const proxy = require('express-http-proxy')
const express = require('express')
const path = require('path')
const port = '20314'

// eslint-disable-next-line import/order
const interfaces = require('os').networkInterfaces()

const LOCAL_IP = getIPAddress()
const app = express()

app.use(express.static(path.join(__dirname, './dist/')))

const localUri = `http://localhost:${port}/`
const ipUri = `http://${LOCAL_IP}:${port}/`

app.listen(port, () => {
  console.log(`应用启动: ${localUri}`)
  console.log(`应用启动: ${ipUri}`)
})

const proxyURI = 'https://iot-api-boss-test.chehejia.com/saos-csp-ir-web'

app.use('/', proxy(proxyURI, {
  proxyReqPathResolver: function (req, res) {
      console.log(res)
    // console.log('proxy is done')
    return req.url
  }
}))

function getIPAddress () {
  for (const devName in interfaces) {
    const iface = interfaces[devName]
    for (let i = 0; i < iface.length; i++) {
      const alias = iface[i]
      if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
        return alias.address
      }
    }
  }
  return 'localhost'
}
