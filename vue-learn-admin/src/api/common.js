/*
 * @description: 公共接口
 * @author: leroy
 * @Compile：2021-01-20 18：00
 * @update: leroy(2021-01-20 18：00)
 */
import service from '@/utils/request'

/**
 * @memo 登录获取左侧菜单
 * @param {}
 */
export function menu_list_path(data) {
  return service({
    url: '/v1/sys/account/getMenuList',
    method: 'post',
    data
  })
}

/**
 * @memo 上传单个图片
 */
export function file_upload_image_path(data) {
  return service({
    url: '/v1/file/fastDFS/uploadImage',
    method: 'post',
    data
  })
}

/**
 * @memo 上传多个图片
 */
export function file_multi_upload_path(data) {
  return service({
    url: '/v1/file/fastDFS/uploadMultiFile',
    method: 'post',
    data
  })
}

/**
 * @memo 上传多个图片
 */
export function file_multi_upload_print_path(data) {
  return service({
    url: '/v1/file/fastDFS/uploadMultiFile',
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data;charset=UTF-8'
    },
    data
  })
}

/**
 * @memo 删除图片 暂时不使用
 */
export function delete_file_path(data) {
  return service({
    url: '/v1/file/fastDFS/deleteFile',
    method: 'post',
    data
  })
}

/**
 * @memo 上传视频文件
 */
export function file_uploadVideo_path(data) {
  return service({
    url: '/v1/file/fastDFS/uploadVideo',
    method: 'post',
    data
  })
}

/**
 * @memo 上传文本文档
 */
export function file_uploadDocument_path(data) {
  return service({
    url: '/v1/file/fastDFS/uploadDocument',
    method: 'post',
    data
  })
}

/**
 * @memo 上传文本文档
 */
export function file_uploadMultiFile_path(data) {
  return service({
    url: '/v1/file/fastDFS/uploadMultiFile',
    method: 'post',
    data
  })
}
