/**
 * @description: 日常运营接口文件
 * @author: leroy
 * @Compile：2021-12-01 10：00
 * @update: lx(2021-12-01 10：00)
 */
import service from '@/utils/request'
/************************* banner图管理 ***************************** */
/**
 * @memo banner图 --- 添加
 * @param {
 *  bannerName: '',                //类型：String  可有字段  备注：banner图名称
 *  imgPath: '',                //类型：String  可有字段  备注：图片存放路径（短）
 *  sort: '',                //类型：String  可有字段  备注：排序 数值越大越靠前
 *  businessId: '',                //类型：String  可有字段  备注：跳转类型的id 或者图片跳转url
 *  type: '',                //类型：String  可有字段  备注：跳转类型：0：不跳转,1-跳转URL,2-客户列表,3-会员卡列表;4-视频详情;5-客户详情,6-会员卡详情;7-活动列表,8-活动详情,
 *  putSite: '',                //类型：String  可有字段  备注：投放位置 0-首页
 *  memo: '',                //类型：String  可有字段  备注：备注
 *  isState: '',                //类型：String  可有字段  备注：是否上线 0是 1否
 * }
 */
export function banner_save_path(data) {
    return service({
        url: '/v1/operation/banner/save',
        method: 'post',
        data
    })
}

/**
 * @memo banner图 --- 修改
 * @param {
 *  id: '',                //类型：String  可有字段  备注：编号
 *  bannerName: '',                //类型：String  可有字段  备注：banner图名称
 *  imgPath: '',                //类型：String  可有字段  备注：图片存放路径（短）
 *  sort: '',                //类型：String  可有字段  备注：排序 数值越大越靠前
 *  businessId: '',                //类型：String  可有字段  备注：跳转类型的id 或者图片跳转url
 *  type: '',                //类型：String  可有字段  备注：跳转类型：0：不跳转,1-跳转URL,2-客户列表,3-会员卡列表;4-视频详情;5-客户详情,6-会员卡详情;7-活动列表,8-活动详情,
 *  putSite: '',                //类型：String  可有字段  备注：投放位置 0-首页
 *  memo: '',                //类型：String  可有字段  备注：备注
 *  isState: '',                //类型：String  可有字段  备注：是否上线 0是 1否
 * }
 */
export function banner_update_path(data) {
    return service({
        url: '/v1/operation/banner/update',
        method: 'post',
        data
    })
}

/**
 * @memo banner图 --- 删除
 * @param {
 *  ids: '',                //类型：String  可有字段  备注：编号集
 * }
 */
export function banner_delete_path(data) {
    return service({
        url: '/v1/operation/banner/delete',
        method: 'post',
        data
    })
}

/**
 * @memo banner图 --- 列表
 * @param {
 *  pageNum: 1,                //类型：Number  可有字段  备注：当前页码
 *  pageSize: 10,                //类型：Number  可有字段  备注：每页的条数
 *  bannerName: '',                //类型：String  可有字段  备注：banner名称
 *  putSite: '',                //类型：Number  可有字段  备注：投放位置 0首页
 *  isState: ''                //类型：Number  可有字段  备注：状态
 * }
 */
export function banner_list_path(data) {
    return service({
        url: '/v1/operation/banner/getList',
        method: 'post',
        data
    })
}

/**
 * @memo banner图 --- 详情
 * @param {
 *  id: '',                //类型：String  可有字段  备注：编号
 * }
 */
 export function banner_info_path(data) {
    return service({
        url: '/v1/operation/banner/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo banner图 --- 批量修改状态
 * @param {
 *  ids: '',                //类型：String  可有字段  备注：编号集
 *  isState: ''                //类型：String  可有字段  备注：状态
 * }
 */
 export function banner_update_state_path(data) {
    return service({
        url: '/v1/operation/banner/updateMultiCategory',
        method: 'post',
        data
    })
}


/************************* 意见反馈分类 ***************************** */
/**
 * @memo 意见反馈分类 --- 添加
 * @param {
 *  cateName: "",                //类型：String  可有字段  备注：分类名称
 *  sort: "",                //类型：String  可有字段  备注：排序 数值越大越靠前
 *  memo: "",                //类型：String  可有字段  备注：备注
 *  isState: "",                //类型：String  可有字段  备注：是否上线 0是 1否
 * }
 */
export function feed_category_save_path(data) {
    return service({
        url: '/v1/user/feedbackcate/save',
        method: 'post',
        data
    })
}

/**
 * @memo 意见反馈分类 --- 修改
 * @param {
 *  id: "",                //类型：String  必有字段  备注：id
 *  cateName: "",                //类型：String  可有字段  备注：分类名称
 *  sort: "",                //类型：String  可有字段  备注：排序 数值越大越靠前
 *  memo: "",                //类型：String  可有字段  备注：备注
 *  isState: "",                //类型：String  可有字段  备注：是否上线 0是 1否
 * }
 */
export function feed_category_update_path(data) {
    return service({
        url: '/v1/user/feedbackcate/update',
        method: 'post',
        data
    })
}

/**
 * @memo 意见反馈分类 --- 删除
 * @param {
 *  ids: ""                //类型：String  必有字段  备注：编号集
 * } 
 */
export function feed_category_delete_path(data) {
    return service({
        url: '/v1/user/feedbackcate/delete',
        method: 'post',
        data
    })
}

/**
 * @memo 意见反馈分类 --- 列表
 * @param {
 *  pageNum: 1,                //类型：Number  可有字段  备注：当前页码
 *  pageSize: 10,                //类型：Number  可有字段  备注：每页的条数
 *  cateName: "",                //类型：String  可有字段  备注：分类名称
 *  isState: "",                //类型：String  可有字段  备注：是否上线 0是 1否
 * }
 */
export function feed_category_list_path(data) {
    return service({
        url: '/v1/user/feedbackcate/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 意见反馈分类 --- 详情
 * @param {
 *  id: ""                //类型：String  必有字段  备注：编号
 * }
 */
export function feed_category_info_path(data) {
    return service({
        url: '/v1/user/feedbackcate/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 意见反馈分类 --- 批量修改状态
 * @param {
 *  ids: "",                //类型：String  必有字段  备注：编号集
 *  isState: "",                //类型：String  必有字段  备注：状态
 * }
 */
export function feed_category_update_state_path(data) {
    return service({
        url: '/v1/user/feedbackcate/updateMultiIsEnabled',
        method: 'post',
        data
    })
}

/**
 * @memo 意见反馈分类 --- 全部
 * @param {}
 */
export function feed_category_all_path(data) {
    return service({
        url: '/v1/user/feedbackcate/findAll',
        method: 'post',
        data
    })
}


/************************* 意见反馈 ***************************** */
/**
 * @memo 意见反馈 --- 列表
 * @param {
 *  pageNum: 1,                //类型：String  必有字段  备注：页码
 *  pageSize: 10,                //类型：String  必有字段  备注：条码
 *  name: "",                //类型：String  可有字段  备注：用户昵称
 *  mobilePhone: "",                //类型：String  可有字段  备注：联系方式
 * }
 */
export function feedback_list_path(data) {
    return service({
        url: '/v1/user/feedback/getList',
        method: 'post',
        data
    })
}
/**
 * @memo 意见反馈 --- 详情
 * @param {
 *  id: ""                //类型：String  必有字段  备注：编号
 * }
 */
export function feedback_info_path(data) {
    return service({
        url: '/v1/user/feedback/getInfo',
        method: 'post',
        data
    })
}
/**
 * @memo 意见反馈 --- 处理意见
 * @param {
 *  id:"efbdcb841bf94550a0749c0bd077b928"                //类型：String  必有字段  备注：id
 *  handingContent":"1",                //类型：String  必有字段  备注：处理结果
 *  memo":"1",                //类型：String  必有字段  备注：备注
 * }
 */
export function feedback_hand_path(data) {
    return service({
        url: '/v1/user/feedback/dealWithResults',
        method: 'post',
        data
    })
}
