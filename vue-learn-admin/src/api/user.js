/**
 * @description: 用户管理接口文件
 * @author: leroy
 * @Compile：2021-12-01 10：00
 * @update: lx(2021-12-01 10：00)
 */
import service from '@/utils/request'

/************************* 用户管理 ********************************* */
/**
 * @memo 用户管理 --- 列表
 * @param {
 *  pageNum: 1,                //类型：String  可有字段  备注：页码 默认第一页
 *  pageSize: 10,                //类型：String  可有字段  备注：条码 默认十条
 *  trueName: '',                //类型：String  可有字段  备注：真实姓名
 * }
 */
export function user_list_path(data) {
    return service({
        url: '/v1/user/user/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 用户管理 --- 详情
 * @param {
 *  id: ''                //类型：String  必有字段  备注：编号
 * }
 */
export function user_info_path(data) {
    return service({
        url: '/v1/user/user/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 用户管理 --- 保存
 */
export function user_save_path(data) {
    return service({
        url: '/v1/user/user/save',
        method: 'post',
        data
    })
}

/**
 * @memo 用户管理 --- 修改附加表信息
 */
export function user_addition_update_path(data) {
    return service({
        url: '/v1/user/user/updateUserAddition',
        method: 'post',
        data
    })
}

/**
 * @memo 用户管理 --- 修改择偶标准信息
 */
export function user_mate_update_path(data) {
    return service({
        url: '/v1/user/user/updateUserMateSelection',
        method: 'post',
        data
    })
}


/**
 * @memo 用户管理 --- 修改
 * @param {
 *  id: '',                //类型：String  可有字段  备注：编号
 *  birthday: '',                //类型：String  可有字段  备注：生日
 *  modilePhone: '',                //类型：String  可有字段  备注：手机号
 *  sex: '',                //类型：String  可有字段  备注：用户的性别
 * }
 */
export function user_update_path(data) {
    return service({
        url: '/v1/user/user/update',
        method: 'post',
        data
    })
}
/**
 * @memo 用户管理 --- 批量修改状态
 * @param {
 *  ids: '',                //类型：String  可有字段  备注：编号集
 *  isState: '',                //类型：Number  可有字段  备注：是否上线 0是 1否
 * }
 */
export function user_update_state_path(data) {
    return service({
        url: '/v1/user/user/updateMultiIsEnabled',
        method: 'post',
        data
    })
}
/**
 * @memo 用户管理 --- 批量推荐状态
 * @param {
 *  ids: '',                //类型：String  可有字段  备注：编号集
 *  isType: '',                //类型：Number  可有字段  备注：是否推荐 0是 1否
 * }
 */
 export function user_update_type_path(data) {
    return service({
        url: '/v1/user/user/updateMultiIsType',
        method: 'post',
        data
    })
}
/**
 * @memo 用户管理 --- 会员卡详情
 * @param {
 *  id: ''                //类型：String  必有字段  备注：编号
 * }
 */
 export function user_card_info_path(data) {
    return service({
        url: '/v1/log/userMemberCard/getVIPInfo',
        method: 'post',
        data
    })
}

/************************* 访客/浏览记录 *************************** */
/**
 * @memo 访客记录 --- 列表
 * @param {
 *  pageNum: 1,                //类型：Number  可有字段  备注：当前页码
 *  pageSize: 10,                //类型：Number  可有字段  备注：每页的条数
 *  userId: userId,                //类型：String  可有字段  备注：用户编号
 * }
 */
export function visitors_list_path(data) {
    return service({
        url: '/v1/user/visitorsOrBrowse/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 浏览记录 --- 列表
 * @param {
 *  pageNum: 1,                //类型：Number  可有字段  备注：当前页码
 *  pageSize: 10,                //类型：Number  可有字段  备注：每页的条数
 *  userId: userId,                //类型：String  可有字段  备注：用户编号
 * }
 */
 export function browse_list_path(data) {
    return service({
        url: '/v1/user/visitorsOrBrowse/getListBrowse',
        method: 'post',
        data
    })
}

/************************* 审核管理 ********************************* */
/**
 * @memo 审核管理 --- 列表
 * @param {
 *  pageNum: 1,                //类型：Number  可有字段  备注：当前页码
 *  pageSize: 10,                //类型：Number  可有字段  备注：每页的条数
 *  nickName: '',                //类型：String  可有字段  备注：用户昵称
 *  isAudit: '',                //类型：Number  可有字段  备注：审核状态：0-待审核，1-审核通过 2-审核不通过
 * }
 */
 export function audit_list_path(data) {
    return service({
        url: '/v1/user/userAudit/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 审核管理 --- 修改审核
 * @param {
 *  id: '',                //类型：String  可有字段  备注：编号
 *  isAudit: '',                //类型：Number  可有字段  备注：审核状态：0-待审核，1-审核通过 2-审核不通过
 *  reason: '',                //类型：String  可有字段  备注：错误原因
 * }
 */
 export function audit_update_audit_path(data) {
    return service({
        url: '/v1/user/userAudit/updateIsAudit',
        method: 'post',
        data
    })
}

/************************* 举报分类管理 ********************************* */
/**
 * @memo 举报分类管理 --- 添加
 * @param {
 *  cateName: '',                //类型：String  可有字段  备注：分类名称
 *  sort: '',                //类型：String  可有字段  备注：排序 数值越大越靠前
 *  isState: '',                //类型：String  可有字段  备注：是否上线 0是 1否
 *  memo: '',                //类型：String  可有字段  备注：备注
 * }
 */
export function report_category_save_path(data) {
    return service({
        url: '/v1/user/reportcate/save',
        method: 'post',
        data
    })
}

/**
 * @memo 举报分类管理 --- 修改
 * @param {
 *  id: '',                //类型：String  可有字段  备注：编号
 *  cateName: '',                //类型：String  可有字段  备注：分类名称
 *  sort: '',                //类型：String  可有字段  备注：排序 数值越大越靠前
 *  isState: '',                //类型：String  可有字段  备注：是否上线 0是 1否
 *  memo: '',                //类型：String  可有字段  备注：备注
 * }
 */
export function report_category_update_path(data) {
    return service({
        url: '/v1/user/reportcate/update',
        method: 'post',
        data
    })
}

/**
 * @memo 举报分类管理 --- 删除
 * @param {
 *  ids: '',                //类型：String  必有字段  备注：编号
 * } 
 */
export function report_category_delete_path(data) {
    return service({
        url: '/v1/user/reportcate/delete',
        method: 'post',
        data
    })
}

/**
 * @memo 举报分类管理 --- 列表
 * @param {
 *  pageNum: 1,                //类型：String  必有字段  备注：页码
 *  pageSize: 10,                //类型：String  必有字段  备注：条码
 *  cateName: '',                //类型：String  可有字段  备注：分类名称
 *  isState: '',                //类型：String  可有字段  备注：是否上线 0是 1否
 * } 
 */
export function report_category_list_path(data) {
    return service({
        url: '/v1/user/reportcate/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 举报分类管理 --- 详情
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编号
 * } 
 */
export function report_category_info_path(data) {
    return service({
        url: '/v1/user/reportcate/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 举报分类管理 --- 批量修改状态
 * @param {
 *  ids: '',                //类型：String  必有字段  备注：编号
 *  isState: '',                //类型：String  必有字段  备注：是否上线 0是 1否
 * } 
 */
export function report_category_update_state_path(data) {
    return service({
        url: '/v1/user/reportcate/updateMultiIsEnabled',
        method: 'post',
        data
    })
}

/**
 * @memo 举报分类管理 --- 全部
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编号
 * } 
 */
export function report_category_all_path(data) {
    return service({
        url: '/v1/user/reportcate/findAll',
        method: 'post',
        data
    })
}


/************************* 举报管理 ********************************* */
/**
 * @memo 举报管理 --- 列表
 * @param {
 *  pageNum: 1,                //类型：String  必有字段  备注：页码
 *  pageSize: 10,                //类型：String  必有字段  备注：条码
 *  name: '',                //类型：String  可有字段  备注：用户昵称
 *  cateId: '',                //类型：String  可有字段  备注：分类id
 * }
 */
export function report_list_path(data) {
    return service({
        url: '/v1/user/report/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 举报管理 --- 详情
 * @param {
 *  id: ''                //类型：String  必有字段  备注：编号
 * }
 */
export function report_info_path(data) {
    return service({
        url: '/v1/user/report/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 举报管理 --- 处理意见
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编号
 *  handingContent: '',                //类型：String  可有字段  备注：处理结果
 *  memo: '',                //类型：String  可有字段  备注：备注
 * }
 */
export function report_hand_path(data) {
    return service({
        url: '/v1/user/report/dealWithResults',
        method: 'post',
        data
    })
}
