/**
 * @description: 系统板块接口文件
 * @author: leroy
 * @Compile：2021-05-31 11：00
 * @update: leroy(2021-06-01 18：00)
 */
import service from '@/utils/request'

/************************ 菜单管理 *************************** */
/**
 * @memo 菜单管理 --- 添加
 * @param {
 *  menuName: '',                //类型：String  必有字段  备注：菜单名称
 *  icon: '',                //类型：String  可有字段  备注：菜单或按钮icon
 *  menuCode: '',                //类型：String  必有字段  备注：菜单标识
 *  menuLevel: '',                //类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
 *  parentCode: '',                //类型：String  可有字段  备注：头像路径
 *  parentID: '',                //类型：String  可有字段  备注：父菜单ID
 *  sort: '',                //类型：String  可有字段  备注：* 排序号 数据越高排序越大 0是最低
 *  menuType: '',                //类型：String  必有字段  备注：类型 0-菜单 1-按钮
 *  isState: '',                //类型：String  可有字段  备注：是否启用 0-是 1-否
 *  buttonType: '',                //类型：String  可有字段  备注：按钮区分 1：增改 2：查询 3：删除
 *  pagePath: '',                //类型：String  可有字段  备注：页面路由
 *  url: '',                //类型：String  可有字段  备注：链接地址
 *  memo: ''                //类型：String  可有字段  备注：备注
 * } 
 */
export function menu_save_path(data) {
  return service({
    url: '/v1/sys/menu/save',
    method: 'post',
    data
  })
}

/**
 * @memo  菜单管理 --- 修改
 * @param {
 *  id: '',                //类型：String  可有字段  备注：编号
 *  menuName: '',                //类型：String  必有字段  备注：菜单名称
 *  icon: '',                //类型：String  可有字段  备注：菜单或按钮icon
 *  menuCode: '',                //类型：String  必有字段  备注：菜单标识
 *  menuLevel: '',                //类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
 *  parentCode: '',                //类型：String  可有字段  备注：头像路径
 *  parentID: '',                //类型：String  可有字段  备注：父菜单ID
 *  sort: '',                //类型：String  可有字段  备注：* 排序号 数据越高排序越大 0是最低
 *  menuType: '',                //类型：String  必有字段  备注：类型 0-菜单 1-按钮
 *  isState: '',                //类型：String  可有字段  备注：是否启用 0-是 1-否
 *  buttonType: '',                //类型：String  可有字段  备注：按钮区分 1：增改 2：查询 3：删除
 *  pagePath: '',                //类型：String  可有字段  备注：页面路由
 *  url: '',                //类型：String  可有字段  备注：链接地址
 *  memo: ''                //类型：String  可有字段  备注：备注
 * } 
 */
export function menu_update_path(data) {
  return service({
    url: '/v1/sys/menu/update',
    method: 'post',
    data
  })
}

/**
 * @memo 菜单管理 --- 删除
 * @param {
 *  ids: '',                //类型：String  可有字段  备注：编号集
 * }
 */
export function menu_delete_path(data) {
  return service({
    url: '/v1/sys/menu/delete',
    method: 'post',
    data
  })
}



/**
 * @memo 菜单管理 --- 详情
 * @param {
 *  id: '',                //类型：String  可有字段  备注：编号
 * }
 */
export function menu_info_path(data) {
  return service({
    url: '/v1/sys/menu/getInfo',
    method: 'post',
    data
  })
}

/**
 * @memo 菜单管理 --- 全量列表
 * @param {}
 */
export function menu_tree_path(data) {
  return service({
    url: '/v1/sys/menu/getTree',
    method: 'post',
    data
  })
}

/**
 * @memo 菜单管理 --- 批量修改状态
 */
export function menu_update_state_path(data) {
  return service({
    url: '/v1/sys/menu/updateMultipleState',
    method: 'post',
    data
  })
}

/************************ 角色管理 ******************************* */
/**
 * @memo  角色管理 --- 添加
 * @param {
 *  roleName: '',                //类型：String  必有字段  备注：角色名称
 *  isUpdate: '',                //类型：Number  可有字段  备注： 是否可以编辑 0-是 1-否
 *  isStatus: '',                //类型：Number  可有字段  备注：账号状态 0:正常/启用 1:不启用
 *  departmentID: '',                //类型：Number  必有字段  备注：部门id
 *  memo: '',                //类型：String  可有字段  备注：备注
 *  menuIdList: '',                //类型：String  必有字段  备注：菜单id
 *  code: ''                //类型：String  必有字段  备注：超管填写ADMIN 普通管理员USER
 * }
 */
export function role_save_path(data) {
  return service({
    url: '/v1/sys/role/save',
    method: 'post',
    data
  })
}

/**
 * @memo  角色管理 --- 更新
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编号
 *  roleName: '',                //类型：String  可有字段  备注：角色名称
 *  isUpdate: '',                //类型：Number  可有字段  备注： 是否可以编辑 0-是 1-否
 *  isStatus: '',                //类型：Number  可有字段  备注：账号状态 0:正常/启用 1:不启用
 *  departmentID: '',                //类型：Number  可有字段  备注：部门id
 *  memo: '',                //类型：String  可有字段  备注：备注
 *  menuIdList: '',                //类型：String  可有字段  备注：菜单id
 *  code: ''                //类型：String  可有字段  备注：角色编号
 * }
 */
export function role_update_path(data) {
  return service({
    url: '/v1/sys/role/update',
    method: 'post',
    data
  })
}

/**
 * @memo  角色管理 --- 删除
 * @param {
 *  ids: ''                //类型：String  必有字段  备注：编号集
 * }
 */
export function role_delete_path(data) {
  return service({
    url: '/v1/sys/role/delete',
    method: 'post',
    data
  })
}

/**
 * @memo  角色管理 --- 详情
 * @param {
 *  id: ''                //类型：String  必有字段  备注：编号
 * }
 */
export function role_info_path(data) {
  return service({
    url: '/v1/sys/role/getInfo',
    method: 'post',
    data
  })
}

/**
 * @memo  角色管理 --- 列表
 * @param {
 *  pageSize: 10,                //类型：String  可有字段  备注：条码 默认十条
 *  pageNum: 1,                //类型：String  可有字段  备注：页码 默认第一页
 *  roleName: '',                //类型：String  可有字段  备注：角色名称
 * } 
 */
export function role_list_path(data) {
  return service({
    url: '/v1/sys/role/getList',
    method: 'post',
    data
  })
}

/**
 * @memo  角色管理 --- 全部
 * @param {
 *  departmentID: '',                //类型：String  可有字段  备注：部门id
 * }
 */
export function role_find_all_path(data) {
  return service({
    url: '/v1/sys/role/findAll',
    method: 'post',
    data
  })
}

/************************** 系统管理员 ********************************* */
/**
 * @memo  系统管理员 --- 添加
 * @param {
 *  loginName: '',                //类型：String  必有字段  备注：登录名
 *  accountName: '',                //类型：String  必有字段  备注： 用户真实姓名
 *  loginPassword: '',                //类型：String  必有字段  备注：密码
 *  photo: '',                //类型：String  必有字段  备注：头像路径
 *  roleID: '',                //类型：String  必有字段  备注：角色ID
 *  phone: '',                //类型：String  必有字段  备注：手机
 *  departmentID: '',                //类型：String  必有字段  备注：部门id
 *  isStatus: '',                //类型：String  可有字段  备注：账号状态 0:正常/启用 1:冻结/禁用
 *  accountCode: '',                //类型：String  可有字段  备注：员工编号
 *  email: '',                //类型：String  可有字段  备注：邮箱
 *  sex: '',                //类型：String  可有字段  备注：性别 0:未知 1:男 2:女
 *  address: '',                //类型：String  可有字段  备注：住址
 *  memo: ''                //类型：String  可有字段  备注：备注
 * }
 */
export function account_save_path(data) {
  return service({
    url: '/v1/sys/account/save',
    method: 'post',
    data
  })
}

/**
 * @memo  系统管理员 --- 更新
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编号
 *  loginName: '',                //类型：String  必有字段  备注：登录名
 *  accountName: '',                //类型：String  必有字段  备注： 用户真实姓名
 *  loginPassword: '',                //类型：String  必有字段  备注：密码
 *  photo: '',                //类型：String  必有字段  备注：头像路径
 *  roleID: '',                //类型：String  必有字段  备注：角色ID
 *  phone: '',                //类型：String  必有字段  备注：手机
 *  departmentID: '',                //类型：String  必有字段  备注：部门id
 *  isStatus: '',                //类型：String  可有字段  备注：账号状态 0:正常/启用 1:冻结/禁用
 *  accountCode: '',                //类型：String  可有字段  备注：员工编号
 *  email: '',                //类型：String  可有字段  备注：邮箱
 *  sex: '',                //类型：String  可有字段  备注：性别 0:未知 1:男 2:女
 *  address: '',                //类型：String  可有字段  备注：住址
 *  memo: ''                //类型：String  可有字段  备注：备注
 * }
 */
export function account_update_path(data) {
  return service({
    url: '/v1/sys/account/update',
    method: 'post',
    data
  })
}

/**
 * @memo  系统管理员 --- 删除
 * @param {
 *  ids: ''                //类型：String  可有字段  备注：编号
 * }
 */
export function account_delete_path(data) {
  return service({
    url: '/v1/sys/account/delete',
    method: 'post',
    data
  })
}

/**
 * @memo  系统管理员 --- 详情
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编号
 * }
 */
export function account_info_path(data) {
  return service({
    url: '/v1/sys/account/getInfo',
    method: 'post',
    data
  })
}

/**
 * @memo  系统管理员 --- 列表
 * @param {
 *  pageNum: 1,                //类型：Number  必有字段  备注：页码 默认第一页
 *  pageSize: 10,                //类型：Number  必有字段  备注：条码 每页默认十条
 *  loginName: '',                //类型：String  可有字段  备注：用户账号 支持模糊搜索
 *  accountName: '',                //类型：String  可有字段  备注：用户名 支持模糊搜索
 *  phone: '',                //类型：String  可有字段  备注：手机号
 * }
 */
export function account_list_path(data) {
  return service({
    url: '/v1/sys/account/getList',
    method: 'post',
    data
  })
}

/*********************** 日志管理 *************************** */
/**
 * @memo  管理员登录日志 --- 列表
 * @param {
 *  pageNum: 1,                //类型：Number  必有字段  备注：页码 默认第一页
 *  pageSize: 10,                //类型：Number  必有字段  备注：条码 每页默认十条
 *  loginName: '',                //类型：String  可有字段  备注：用户账号
 *  phone: '',                //类型：String  可有字段  备注：联系方式
 *  endTime: '',                //类型：String  可有字段  备注：登录开始时间
 *  startTime: '',                //类型：String  可有字段  备注：登录结束时间
 * }
 */
export function account_log_path(data) {
  return service({
    url: '/v1/log/accountLogin/getList',
    method: 'post',
    data
  })
}

/**
 * @memo  小程序登录日志 --- 列表
 * @param {
 *  pageNum: 1,                //类型：Number  必有字段  备注：页码 默认第一页
 *  pageSize: 10,                //类型：Number  必有字段  备注：条码 每页默认十条
 *  nickname: '',                //类型：String  可有字段  备注：用户昵称
 *  startTime: '',                //类型：String  可有字段  备注：登录开始时间
 *  endTime: '',                //类型：String  可有字段  备注：登录结束时间
 * }
 */
export function user_log_path(data) {
  return service({
    url: '/v1/log/userLogin/getList',
    method: 'post',
    data
  })
}

/**
 * @memo 用户支付日志 --- 列表
 * @param {
 *  pageNum: 1,                //类型：Number  必有字段  备注：当前页
 *  pageSize: 10,                //类型：Number  必有字段  备注：每页条数
 *  nickName: '',                //类型：String  可有字段  备注：用户昵称
 *  modilePhone: '',                //类型：String  可有字段  备注：手机号
 *  orderNum: '',                //类型：String  可有字段  备注：订单号
 *  startTime: '',                //类型：String  可有字段  备注：支付开始时间
 *  endTime: '',                //类型：String  可有字段  备注：支付结束时间
 * }
 */
 export function user_pay_path(data) {
  return service({
    url: '/v1/log/userPay/getList',
    method: 'post',
    data
  })
}

/************************* 部门管理 ***************************** */
/**
 * @memo 部门管理 --- 组织架构
 * @param {}
 */
export function organizatioon_tree_path(data) {
  return service({
    url: '/v1/sys/organizatioon/getTree',
    method: 'post',
    data
  })
}

/**
 * @memo 部门管理 --- 添加
 * @param {
 *  organizationName: '',                //类型：String  可有字段  备注：组织名称
 *  deptLevel: '',                //类型：Number  可有字段  备注：组织层级 0:总公司(内置) 1:总公司的部门或下属公司 依次类推...
 *  deptType: '',                //类型：Number  可有字段  备注：组织区分 0:总公司(内置) 1:总公司的部门 2:总公司下属的公司 3:下属公司的部门
 *  parentID: '',                //类型：String  可有字段  备注：父标识
 *  memo: ''                //类型：String  可有字段  备注：备注
 * }
 */
export function organizatioon_save_path(data) {
  return service({
    url: '/v1/sys/organizatioon/save',
    method: 'post',
    data
  })
}

/**
 * @memo 部门管理 --- 修改
 * @param {
 *  id: '',                //类型：String  可有字段  备注：编号
 *  organizationName: '',                //类型：String  可有字段  备注：组织名称
 *  deptLevel: '',                //类型：Number  可有字段  备注：组织层级 0:总公司(内置) 1:总公司的部门或下属公司 依次类推...
 *  deptType: '',                //类型：Number  可有字段  备注：组织区分 0:总公司(内置) 1:总公司的部门 2:总公司下属的公司 3:下属公司的部门
 *  parentID: '',                //类型：String  可有字段  备注：父标识
 *  memo: ''                //类型：String  可有字段  备注：备注
 * }
 */
export function organizatioon_update_path(data) {
  return service({
    url: '/v1/sys/organizatioon/update',
    method: 'post',
    data
  })
}

/**
 * @memo 部门管理 --- 删除
 * @param {
 *  ids: '',                //类型：Number  必有字段  备注：编号集
 * }
 */
export function organizatioon_delete_path(data) {
  return service({
    url: '/v1/sys/organizatioon/delete',
    method: 'post',
    data
  })
}

/**
 * @memo 部门管理 --- 详情
 * @param {
 *  id: ''                //类型：String  必有字段  备注：编号
 * }
 */
export function organizatioon_info_path(data) {
  return service({
    url: '/v1/sys/organizatioon/getInfo',
    method: 'post',
    data
  })
}

/**
 * @deprecated  部门管理 --- 全部
 */
export function organizatioon_all_path(data) {
  return service({
    url: '/v1/sys/organizatioon/findAllCompany',
    method: 'post',
    data
  })
}
