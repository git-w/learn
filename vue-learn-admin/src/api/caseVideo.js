/**
 * @description: 视频案例接口文件
 * @author: leroy
 * @Compile：2021-12-06 10：00
 * @update: lx(2021-12-06 10：00)
 */
import service from '@/utils/request'

/************************* 视频案例 ********************************* */
/**
 * @memo 视频案例 --- 保存
 * @param {
 *  theCover: [],                //类型：String  必有字段  备注：案例封面路径（短）
 *  name: '',                //类型：String  必有字段  备注：标题
 *  type: 1,                //类型：String  必有字段  备注：类型
 *  url: '',                //类型：String  必有字段  备注：视频案例路径
 *  isState: 0,                //类型：String  必有字段  备注：状态
 * }
 */
export function case_save_path(data) {
    return service({
        url: '/v1/vide/whiteheadabout/save',
        method: 'post',
        data
    })
}

/**
 * @memo 视频案例 --- 修改
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编号
 *  theCover: [],                //类型：String  必有字段  备注：案例封面路径（短）
 *  name: '',                //类型：String  必有字段  备注：标题
 *  type: 1,                //类型：String  必有字段  备注：类型
 *  url: '',                //类型：String  必有字段  备注：视频案例路径
 *  isState: 0,                //类型：String  必有字段  备注：状态
 * }
 */
export function case_update_path(data) {
    return service({
        url: '/v1/vide/whiteheadabout/update',
        method: 'post',
        data
    })
}

/**
 * @memo 视频案例 --- 删除
 * @param {
 *  ids: '',                //类型：String  必有字段  备注：编号集
 * }
 */
export function case_delete_path(data) {
    return service({
        url: '/v1/vide/whiteheadabout/delete',
        method: 'post',
        data
    })
}

/**
 * @memo 视频案例 --- 列表
 * @param {
 *  pageSize: 1,                // 类型：Number  必有字段  备注：每页长度
 *  pageNum: 10,                // 类型：Number  必有字段  备注：当前页码
 *  name: '',                //类型：String  必有字段  备注：标题
 * }
 */
export function case_list_path(data) {
    return service({
        url: '/v1/vide/whiteheadabout/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 视频案例 --- 详情
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编号
 * }
 */
export function case_info_path(data) {
    return service({
        url: '/v1/vide/whiteheadabout/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 视频案例 --- 批量修改状态
 * @param {
 *  ids: '',                //类型：String  必有字段  备注：编号集
 *  isState: ''                //类型：String  必有字段  备注：状态 0 正常 1 冻结
 * }
 */
export function case_update_state_path(data) {
    return service({
        url: '/v1/vide/whiteheadabout/updateMultipleState',
        method: 'post',
        data
    })
}
