const bread = {
  aside: '',

  // 日常运营
  operation: '日常运营',
  banner: 'banner图管理',
  feedBack: '意见反馈',
    feedBackCategory: '意见反馈分类管理',
    feedBackManage: '意见反馈管理',

  // 活动管理
  activity: '活动管理',
  activityCategory: '活动分类管理',
  activityManage: '活动管理',
  activityOrder: '活动订单管理',
  activityInfoOrder: '活动订单详情',

  // 视频案例
  caseVideo: '视频案例',
  caseManage: '内容管理',

  // 用户管理
  user: '用户管理',
  userManage: '用户管理',
  userAudit: '用户审核管理',
  report: '举报管理',
    reportCategory: '举报分类管理',
    reportManage: '举报管理',
  
  // 会员卡管理
  memberCard: '会员卡管理',
  cardManage: '会员卡信息管理',
  cardOrder: '订单管理',
  cardLog: '日志管理',

  // 内容管理
  content: '内容管理',
  material: '素材管理',

  // 系统设置
  system: '系统设置',
  menu: '菜单管理',
  authority: '角色管理',
  account: '系统管理员',
  organizatioon: '部门管理',
  adminLog: '日志管理',

  // 系统消息
  message: '系统消息',
  systemMessage: '系统消息管理',
  
  
}
export default bread
