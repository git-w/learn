/**
 * @description: 活动管理接口文件
 * @author: leroy
 * @Compile：2021-12-01 11：00
 * @update: lx(2021-12-01 11：00)
 */
import service from '@/utils/request'

/*************************** 活动分类 ******************************* */
/**
 * @memo 活动分类 --- 添加
 * @param {
 *  cateName: '',                // 类型：String  必有字段  备注：名称
 *  sort: '',                // 类型：Number  可有字段  备注：排序 0是最高 默认0
 *  memo: '',                //类型：String  可有字段  备注：备注
 *  isState: '',                //类型：String  可有字段  备注：是否上线 0是 1否
 * }
 */
export function activity_category_save_path(data) {
    return service({
        url: '/v1/activity/cate/save',
        method: 'post',
        data
    })
}

/**
 * @memo 活动分类 --- 修改
 * @param {
 *  id: '',                // 类型：String  必有字段  备注：编号
 *  cateName: '',                // 类型：String  必有字段  备注：名称
 *  sort: '',                // 类型：Number  可有字段  备注：排序 0是最高 默认0
 *  memo: '',                //类型：String  可有字段  备注：备注
 *  isState: '',                //类型：String  可有字段  备注：是否上线 0是 1否
 * }
 */
export function activity_category_update_path(data) {
    return service({
        url: '/v1/activity/cate/update',
        method: 'post',
        data
    })
}

/**
 * @memo 活动分类 --- 删除
 * @param {
 *  ids:'',                //类型：String  可有字段  备注：编号集
 * }
 */
export function activity_category_delete_path(data) {
    return service({
        url: '/v1/activity/cate/delete',
        method: 'post',
        data
    })
}

/**
 * @memo 活动分类 --- 列表
 * @param {
 *  pageNum:1,                //类型：String  必有字段  备注：页码
 *  pageSize:10,                //类型：String  必有字段  备注：条码
 *  cateName:"",                //类型：String  可有字段  备注：分类名称
 *  isState:""                //类型：String  可有字段  备注：是否上线 0是 1否
 * }
 */
export function activity_category_list_path(data) {
    return service({
        url: '/v1/activity/cate/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 活动分类 --- 详情
 * @param {
 *  id:""                //类型：String  必有字段  备注：编号
 * }
 */
export function activity_category_info_path(data) {
    return service({
        url: '/v1/activity/cate/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 活动分类 --- 批量修改状态
 * @param {
 *  ids:""                //类型：String  必有字段  备注：编号集
 *  isState:0                //类型：Number  必有字段  备注：是否上线 0是 1否
 * }
 */
export function activity_category_update_state_path(data) {
    return service({
        url: '/v1/activity/cate/updateMultiIsEnabled',
        method: 'post',
        data
    })
}

/**
 * @memo 活动分类 --- 全部
 * @param {}
 */
export function activity_category_all_path(data) {
    return service({
        url: '/v1/activity/cate/findAll',
        method: 'post',
        data
    })
}


/************************* 活动管理 ***************************************** */
/**
 * @memo 活动管理 --- 添加
 * @param {
 *  name:"",                //类型：String  可有字段  备注：活动名称
 *  msg:"",                //类型：String  可有字段  备注：活动简介
 *  content:"",                //类型：String  可有字段  备注：活动描述
 *  bookingInformation:"",                //类型：String  可有字段  备注：预定须知
 *  categoryId:"",                //类型：String  可有字段  备注：活动类型ID
 *  shareItPictures:"",                //类型：String  可有字段  备注：分享海报路径（短）
 *  pictures:"",                //类型：String  可有字段  备注：活动封面路径（短）
 *  imgPathList:"",                //类型：String  可有字段  备注：活动照片路径（短），多图用,隔开
 *  address:"",                //类型：String  可有字段  备注：活动地址
 *  longitude:"",                //类型：String  可有字段  备注：经度
 *  latitude:"",                //类型：String  可有字段  备注：纬度
 *  payStartTime:"",                //类型：Number  可有字段  备注：报名开始时间
 *  payEndTime:"",                //类型：Number  可有字段  备注：报名结束时间
 *  stratTime:"",                //类型：Number  可有字段  备注：活动开始时间
 *  endTime:"",                //类型：Number  可有字段  备注：活动结束时间
 *  isFree:"",                //类型：Number  可有字段  备注：是否免费：0-免费；1-不免费
 *  price:"",                //类型：Number  可有字段  备注：活动报名费
 *  activityUserCount:"",                //类型：Number  可有字段  备注：活动名额
 *  isState:1                //类型：Number  可有字段  备注：是否上线 0是 1否
 * }
 */
export function activity_save_path(data) {
    return service({
        url: '/v1/activity/save',
        method: 'post',
        data
    })
}

/**
 * @memo 活动管理 --- 修改
 * @param {
 *  id:"",                //类型：String  必有字段  备注：编号
 *  name:"",                //类型：String  可有字段  备注：活动名称
 *  msg:"",                //类型：String  可有字段  备注：活动简介
 *  content:"",                //类型：String  可有字段  备注：活动描述
 *  bookingInformation:"",                //类型：String  可有字段  备注：预定须知
 *  categoryId:"",                //类型：String  可有字段  备注：活动类型ID
 *  shareItPictures:"",                //类型：String  可有字段  备注：分享海报路径（短）
 *  pictures:"",                //类型：String  可有字段  备注：活动封面路径（短）
 *  imgPathList:"",                //类型：String  可有字段  备注：活动照片路径（短），多图用,隔开
 *  address:"",                //类型：String  可有字段  备注：活动地址
 *  longitude:"",                //类型：String  可有字段  备注：经度
 *  latitude:"",                //类型：String  可有字段  备注：纬度
 *  payStartTime:"",                //类型：Number  可有字段  备注：报名开始时间
 *  payEndTime:"",                //类型：Number  可有字段  备注：报名结束时间
 *  stratTime:"",                //类型：Number  可有字段  备注：活动开始时间
 *  endTime:"",                //类型：Number  可有字段  备注：活动结束时间
 *  isFree:"",                //类型：Number  可有字段  备注：是否免费：0-免费；1-不免费
 *  price:"",                //类型：Number  可有字段  备注：活动报名费
 *  activityUserCount:"",                //类型：Number  可有字段  备注：活动名额
 *  isState:1                //类型：Number  可有字段  备注：是否上线 0是 1否
 * }
 */
export function activity_update_path(data) {
    return service({
        url: '/v1/activity/update',
        method: 'post',
        data
    })
}

/**
 * @memo 活动管理 --- 删除
 * @param {
 *  ids:""                //类型：String  必有字段  备注：编号集
 * }
 */
export function activity_delete_path(data) {
    return service({
        url: '/v1/activity/delete',
        method: 'post',
        data
    })
}

/**
 * @memo 活动管理 --- 列表
 * @param {
 *  pageNum:1,                //类型：String  必有字段  备注：页码
 *  pageSize:10,                //类型：String  必有字段  备注：条码
 *  name:"",                //类型：String  可有字段  备注：活动名称
 *  categoryId:"",                //类型：String  可有字段  备注：活动类型ID
 *  isState:""                //类型：Number  可有字段  备注：是否上线 0是 1否
 * }
 */
export function activity_list_path(data) {
    return service({
        url: '/v1/activity/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 活动管理 --- 详情
 * @param {
 *  id:""                //类型：String  必有字段  备注：编号
 * }
 */
export function activity_info_path(data) {
    return service({
        url: '/v1/activity/getInfo',
        method: 'post',
        data
    })
}

/************************** 活动价格库存 ******************************* */
/**
 * @memo 活动价格库存 --- 列表
 * @param {
 *  activityId:""                //类型：String  必有字段  备注：活动编号
 * }
 */
export function activity_price_list_path(data) {
    return service({
        url: '/v1/activity/price/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 活动价格库存 --- 添加
 * @param {
 *  price: "",                // 类型：String  必有字段  备注：价格
 *  vipPrice: "",                // 类型：String  必有字段  备注：会员价格
 *  stock: "",                // 类型：String  必有字段  备注：库存
 *  startTime: "",                // 类型：String  必有字段  备注：开始时间
 *  endTime: "",                // 类型：String  必有字段  备注：结束时间
 * }
 */
export function activity_price_save_path(data) {
    return service({
        url: '/v1/activity/price/save',
        method: 'post',
        data
    })
}

/**
 * @memo 活动价格库存 --- 修改
 * @param {
 *  id: "",                // 类型：String  必有字段  备注：编号
 *  price: "",                // 类型：String  必有字段  备注：价格
 *  vipPrice: "",                // 类型：String  必有字段  备注：会员价格
 *  stock: "",                // 类型：String  必有字段  备注：库存
 *  startTime: "",                // 类型：String  必有字段  备注：开始时间
 *  endTime: "",                // 类型：String  必有字段  备注：结束时间
 * }
 */
export function activity_price_update_path(data) {
    return service({
        url: '/v1/activity/price/update',
        method: 'post',
        data
    })
}

/**
 * @memo 活动价格库存 --- 删除
 * @param {
 *  ids:"",                // 类型：String  必有字段  备注：编号集
 * }
 */
export function activity_price_delete_path(data) {
    return service({
        url: '/v1/activity/price/delete',
        method: 'post',
        data
    })
}

/**
 * @memo 活动价格库存 --- 详情
 * @param {
 *  id:"",                // 类型：String  必有字段  备注：编号
 * }
 */
export function activity_price_info_path(data) {
    return service({
        url: '/v1/activity/price/getInfo',
        method: 'post',
        data
    })
}

/********************************** 活动订单管理 ************************************** */
/**
 * @memo 活动订单管理 --- 列表
 * @param {
 *   pageNum:1,                //类型：String  必有字段  备注：页码
 *   pageSize:10,                //类型：String  必有字段  备注：条码
 * }
 */
export function activity_order_list_path(data) {
    return service({
        url: '/v1/activity/order/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 活动订单管理 --- 详情
 * @param {
 *  id:"",                // 类型：String  必有字段  备注：编号
 * }
 */
export function activity_order_info_path(data) {
    return service({
        url: '/v1/activity/order/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 活动订单管理 --- 核销订单
 * @param {
 *  id:"",                // 类型：String  必有字段  备注：编号
 * }
 */
export function activity_order_confirm_path(data) {
    return service({
        url: '/v1/activity/order/confirmOrder',
        method: 'post',
        data
    })
}

/**
 * @memo 活动订单管理 --- 订单退款
 * @param {
 *  id:"",                // 类型：String  必有字段  备注：编号
 * }
 */
export function activity_order_refund_path(data) {
    return service({
        url: '/v1/activity/order/confirmRefundOrder',
        method: 'post',
        data
    })
}

/**
 * @memo 活动订单管理 --- 订单拒绝退款
 * @param {
 *  id:"",                // 类型：String  必有字段  备注：编号
 * }
 */
export function activity_order_refuse_refund_path(data) {
    return service({
        url: '/v1/activity/order/refuseRefundOrder',
        method: 'post',
        data
    })
}
