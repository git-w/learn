/**
 * @description: 会员卡接口文件
 * @author: leroy
 * @Compile：2021-12-06 14：00
 * @update: lx(2021-12-06 14：00)
 */
import service from '@/utils/request'

/************************* 会员卡管理 ********************************* */
/**
 * @memo 会员卡管理 --- 保存
 * @param {
 *  name: '',                //类型：String  必有字段  备注：会员卡名称
 *  subtitle: '',                //类型：String  必有字段  备注：会员卡副标题
 *  level: '',                //类型：String  必有字段  备注：会员级别
 *  content: '',                //类型：String  必有字段  备注：会员卡使用说明
 *  introduction: '',                //类型：String  必有字段  备注：会员卡简介
 *  price: '',                //类型：Number  必有字段  备注：价格
 *  isInfinite: '',                //类型：Number  必有字段  备注：库存限制
 *  inventoryCount: '',                //类型：Number  必有字段  备注：库存数量
 *  validTimeUnit: '',                //类型：Number  必有字段  备注：有效时长单位，1-周，2-月，3-季，4-年
 *  validTime: '',                //类型：Number  必有字段  备注：有效时长(周期)，表示几周，几个月
 *  imgUrl: [],                //类型：String  必有字段  备注：会员卡封面路径（短）
 *  sellTime: [],                //类型：String  必有字段  备注：售卖时效
 *  isEnabled: '',                //类型：String  必有字段  备注：是否启用 0-是 1-否
 *  isFree: '',                //类型：String  必有字段  备注：是否免费领取 0-是 1-否
 *  getNum: '',                //类型：String  必有字段  备注：领取次数
 * }
 */
export function card_save_path(data) {
    return service({
        url: '/v1/card/memberCard/save',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡管理 --- 修改
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编号
 *  name: '',                //类型：String  必有字段  备注：会员卡名称
 *  subtitle: '',                //类型：String  必有字段  备注：会员卡副标题
 *  level: '',                //类型：String  必有字段  备注：会员级别
 *  content: '',                //类型：String  必有字段  备注：会员卡使用说明
 *  introduction: '',                //类型：String  必有字段  备注：会员卡简介
 *  price: '',                //类型：Number  必有字段  备注：价格
 *  isInfinite: '',                //类型：Number  必有字段  备注：库存限制
 *  inventoryCount: '',                //类型：Number  必有字段  备注：库存数量
 *  validTimeUnit: '',                //类型：Number  必有字段  备注：有效时长单位，1-周，2-月，3-季，4-年
 *  validTime: '',                //类型：Number  必有字段  备注：有效时长(周期)，表示几周，几个月
 *  imgUrl: [],                //类型：String  必有字段  备注：会员卡封面路径（短）
 *  sellTime: [],                //类型：String  必有字段  备注：售卖时效
 *  isEnabled: '',                //类型：String  必有字段  备注：是否启用 0-是 1-否
 *  isFree: '',                //类型：String  必有字段  备注：是否免费领取 0-是 1-否
 *  getNum: '',                //类型：String  必有字段  备注：领取次数
 * }
 */
export function card_update_path(data) {
    return service({
        url: '/v1/card/memberCard/update',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡管理 --- 删除
 * @param {
 *  ids:'',                //类型：String  必有字段  备注：编号集
 * }
 */
export function card_delete_path(data) {
    return service({
        url: '/v1/card/memberCard/delete',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡管理 --- 列表
 * @param {
 *  pageSize: 1,                // 类型：Number  必有字段  备注：每页长度
 *  pageNum: 10,                // 类型：Number  必有字段  备注：当前页码
 *  name: "",                //类型：String  必有字段  备注：会员卡名称
 * }
 */
export function card_list_path(data) {
    return service({
        url: '/v1/card/memberCard/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡管理 --- 详情
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编码
 * }
 */
export function card_info_path(data) {
    return service({
        url: '/v1/card/memberCard/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡管理 --- 批量修改状态
 * @param {
 *  ids: "",                //类型：String  必有字段  备注：编码集
 *  isEnabled: "",                //类型：String  必有字段  备注：状态 0 正常 1 冻结
 * }
 */
export function card_update_state_path(data) {
    return service({
        url: '/v1/card/memberCard/updateMultiIsEnabled',
        method: 'post',
        data
    })
}


/************************* 会员卡配置管理 ********************************* */
/**
 * @memo 会员卡配置管理 --- 添加
 * @param {
 *  cardId: '',                //类型：String  必有字段  备注：关联会员卡ID
 *  isType: '',                //类型：String  必有字段  备注：关联类型，参数为空
 *  imgUrl: [],                //类型：String  必有字段  备注：图片地址
 *  count: '',                //类型：String  必有字段  备注：数量
 *  isEnabled: '',                //类型：Number  必有字段  备注：是否启用 0-是 1-否
 *  memo: '',                //类型：String  必有字段  备注：备注
 * }
 */
export function card_config_save_path(data) {
    return service({
        url: '/v1/card/memberCardConfig/save',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡配置管理 --- 修改
 * @param {
 *  id: '',                //类型：String  必有字段  备注：编码
 *  cardId: '',                //类型：String  必有字段  备注：关联会员卡ID
 *  isType: '',                //类型：String  必有字段  备注：关联类型，参数为空
 *  imgUrl: [],                //类型：String  必有字段  备注：图片地址
 *  count: '',                //类型：String  必有字段  备注：数量
 *  isEnabled: '',                //类型：Number  必有字段  备注：是否启用 0-是 1-否
 *  memo: '',                //类型：String  必有字段  备注：备注
 * }
 */
export function card_config_update_path(data) {
    return service({
        url: '/v1/card/memberCardConfig/update',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡配置管理 --- 删除
 * @param {
 *  ids: '',                //类型：String  必有字段  备注：编码集
 * }
 */
export function card_config_delete_path(data) {
    return service({
        url: '/v1/card/memberCardConfig/delete',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡配置管理 --- 列表
 * @param {
 *  pageSize: 1,                // 类型：Number  必有字段  备注：每页长度
 *  pageNum: 10,                // 类型：Number  必有字段  备注：当前页码
 *  cardId: "",                //类型：String  必有字段  备注：会员卡id
 * }
 */
export function card_config_list_path(data) {
    return service({
        url: '/v1/card/memberCardConfig/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡配置管理 --- 详情
 * @param {
 *  id: "",                // 类型：String  必有字段  备注：编号
 * }
 */
export function card_config_info_path(data) {
    return service({
        url: '/v1/card/memberCardConfig/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡配置管理 --- 批量修改状态
 * @param {
 *  ids: '',                // 类型：String  必有字段  备注：编号集
 *  isEnabled: ''                // 类型：String  必有字段  备注：状态 0 正常 1 冻结
 * }
 */
export function card_config_update_state_path(data) {
    return service({
        url: '/v1/card/memberCardConfig/updateMultiIsEnabled',
        method: 'post',
        data
    })
}


/************************* 会员卡订单管理 ********************************* */
/**
 * @memo 会员卡订单管理 --- 列表
 * @param {
 *  pageSize: 1,                // 类型：Number  必有字段  备注：每页长度
 *  pageNum: 10,                // 类型：Number  必有字段  备注：当前页码
 *  orderNum: '',                // 类型：String  可有字段  备注：订单号
 *  nickName: '',                // 类型：String  可有字段  备注：用户昵称
 *  createTimeFrom: '',                //类型：Number  可有字段  备注：下单时间（始）
 *  createTimeTo: '',                //类型：Number  可有字段  备注：下单时间（终）
 * }
 */
export function card_order_list_path(data) {
    return service({
        url: '/v1/card/order/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡订单管理 --- 详情
 * @param {
 *  id:"",                // 类型：String  可有字段  备注：编号
 * }
 */
 export function card_order_info_path(data) {
    return service({
        url: '/v1/card/order/getInfo',
        method: 'post',
        data
    })
}

/**
 * @memo 会员卡使用记录 --- 列表
 * @param {
 *  pageSize: 1,                // 类型：Number  必有字段  备注：每页长度
 *  pageNum: 10,                // 类型：Number  必有字段  备注：当前页码
 *  midCardId: '',                // 类型：String  可有字段  备注：关联会员卡id
 * }
 */
export function card_member_list_path(data) {
    return service({
        url: '/v1/log/userMemberCard/getList',
        method: 'post',
        data
    })
}
