import service from '@/utils/request'
import { get_token, get_uid } from '@/utils/auth'

/**
 * @memo  登录接口
 * @param {
 *   username: '',                //类型：String  必有字段  备注：登录账号
 *   password: '',                //类型：String  必有字段  备注：登录密码
 * }
 */
export function login_path(data) {
  return service({
    url: '/login',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded' // 请求头为form表单类型的，那么请求体的参数就为键值对的形式的
    },
    data:"username="+data.username+"&password="+data.password+"&code="+data.code+"&token="+data.token
  })
}

/**
 * @memo  修改密码
 * @param {
 *  loginPassword: '',                //类型：String  必有字段  备注：旧密码
 *  newLoginPassword: '',                //类型：String  必有字段  备注：新秘密
 * }
 */
export function reset_password_path(data) {
  return service({
    url: '/v1/sys/account/resetPassword',
    method: 'post',
    data
  })
}

/**
 * @memo 管理员详情接口
 * @param {}
 */
export function admin_info_path(data) {
  return service({
    url: '/v1/sys/account/getAdministratorInfo',
    method: 'post',
    headers: {
      'Authorization': get_token(),
      'Uid': get_uid()
    },
    data
  })
}


/**
 * @memo 验证码接口
 * @param {}
 */
export function get_captcha_path() {
  return service({
    url: '/v1/sys/code/captcha',
    method: 'post'
  })
}


