/*
 * @description: 内容管理接口文件
 * @author: leroy
 * @Compile：2021-01-29 18：00
 * @update: leroy(2021-01-29 18：00)
 */
import service from '@/utils/request'

/***************************** 素材库分类 ******************************** */
/**
 * @memo  素材库分类---  全部
 * @param {}
 */
export function category_all_path(data) {
  return service({
    url: '/v1/sys/imagesCategory/findAll',
    method: 'post',
    data
  })
}

/**
 * @memo  素材库分类---  保存
 * @param {
 *  categoryName:''               //类型：String  必有字段  备注：分类名称
 * }
 */
export function category_save_path(data) {
  return service({
    url: '/v1/sys/imagesCategory/save',
    method: 'post',
    data
  })
}

/**
 * @memo  素材库分类---  修改
 * @param {
 *   id: '',               //类型：String  必有字段  备注：编号
 *   categoryName: ''               //类型：String  必有字段  备注：分类名称
 * }
 */
export function category_update_path(data) {
  return service({
    url: '/v1/sys/imagesCategory/update',
    method: 'post',
    data
  })
}

/**
 * @memo  素材库分类---  删除
 * @param {
 *   ids: '',               //类型：String  必有字段  备注：编号集
 * }
 */
export function category_delete_path(data) {
  return service({
    url: '/v1/sys/imagesCategory/delete',
    method: 'post',
    data
  })
}


/***************************** 素材库图片 ******************************** */
/**
 * @memo  素材库图片---  列表
 * @param {
 *  pageSize: 1,               //类型：Number  必有字段  备注：每页的数量
 *  pageNum：10,               //类型：Number  必有字段  备注：当前页
 *  categoryID："",               //类型：String  可有字段  备注：分类id
 *  imgName：""               //类型：String  可有字段  备注：图片名称
 * }
 */
export function image_list_path(data) {
  return service({
    url: '/v1/sys/images/getList',
    method: 'post',
    data
  })
}


/**
 * @memo               素材库图片---  添加
 * @param {
 *  imageList: {
 *    categoryID: "",               //类型：String  必有字段  备注：分类Id
 *    imgName: "",               //类型：String  必有字段  备注：图片名称
 *    imgUrl: "",               //类型：String  必有字段  备注：图片地址Url
 *    imgType: "",               //类型：String  必有字段  备注：文件内容类型
 *    imgHeight: "",               //类型：Number  必有字段  备注：图片高 px
 *    imgWidth: "",               //类型：Number  必有字段  备注：图片宽 px
 *    imgSize: "",               //类型：Number  必有字段  备注：图片文件大小 字节
 *  },               //类型：Array  必有字段  备注：图片对象
 */
export function image_save_path(data) {
  return service({
    url: '/v1/sys/images/saveBatch',
    method: 'post',
    data
  })
}


/**
 * @memo  素材库图片 --- 批量修改分组
 * @param {
 *  ids: '',               //类型：String  可有字段  备注：编号集
 *  categoryID: '',               //类型：String  可有字段  备注：分类id
 * }
 */
export function image_update_category_path(data) {
  return service({
    url: '/v1/sys/images/updateMultiCategory',
    method: 'post',
    data
  })
}


/**
 * @memo                      素材库图片--- 删除
 * @param {
 *  ids: '',               //类型：String  必有字段  备注：编号
 * } 
 */
export function image_delete_path(data) {
  return service({
    url: '/v1/sys/images/delete',
    method: 'post',
    data
  })
}
