/**
 * @description: 系统信息接口文件
 * @author: leroy
 * @Compile：2021-12-20 11：00
 * @update: lx(2021-12-20 11：00)
 */
import service from '@/utils/request'

/*********************** 系统消息管理 *************************** */
/**
 * @memo 系统消息管理 --- 保存
 * @param {
 *  title: '',                // 类型：Number  必有字段  备注：消息标题备注
 *  content: '',                // 类型：Number  必有字段  备注：内容
 *  isMessage: '',                // 类型：Number  必有字段  备注：信息类型 0:公告 1:通知
 *  isUserAll: '',                // 类型：Number  必有字段  备注：用户类型 0:通知全部用户 1:部分用户
 *  userIds: '',                // 类型：String  必有字段  备注：用户ids
 *  isType: '',                // 类型：String  必有字段  备注：通知的话选择新消息类型消息类型:1审核成功通知 2审核失通知  3会员卡购买成功通知 4新用户注册成功通知  5免费会员卡领取成功通知
 * }
 */
export function message_save_path(data) {
    return service({
        url: '/v1/messageCenter/message/save',
        method: 'post',
        data
    })
}

/**
 * @memo 系统消息管理 --- 修改
 * @param {
 *  id: '',                // 类型：Number  必有字段  备注：编号
 *  title: '',                // 类型：Number  必有字段  备注：消息标题备注
 *  content: '',                // 类型：Number  必有字段  备注：内容
 *  isMessage: '',                // 类型：Number  必有字段  备注：信息类型 0:公告 1:通知
 *  isUserAll: '',                // 类型：Number  必有字段  备注：用户类型 0:通知全部用户 1:部分用户
 *  userIds: '',                // 类型：String  必有字段  备注：用户ids
 *  isType: '',                // 类型：String  必有字段  备注：通知的话选择新消息类型消息类型:1审核成功通知 2审核失通知  3会员卡购买成功通知 4新用户注册成功通知  5免费会员卡领取成功通知
 * }
 */
 export function message_update_path(data) {
    return service({
        url: '/v1/messageCenter/message/update',
        method: 'post',
        data
    })
}

/**
 * @memo 系统消息管理 --- 列表
 * @param {
 *  pageSize: 1,                // 类型：Number  必有字段  备注：每页长度
 *  pageNum: 10,                // 类型：Number  必有字段  备注：当前页码
 * }
 */
export function message_list_path(data) {
    return service({
        url: '/v1/messageCenter/message/getList',
        method: 'post',
        data
    })
}

/**
 * @memo 系统消息管理 --- 删除
 * @param {
 *  ids: '',                // 类型：Number  必有字段  备注：编号集
 * }
 */
export function message_delete_path(data) {
    return service({
        url: '/v1/messageCenter/message/delete',
        method: 'post',
        data
    })
}

/********************** 消息日志管理 ********************************* */
/**
 * @memo 消息日志管理 --- 列表
 * @param {
 *  pageSize: 1,                // 类型：Number  必有字段  备注：每页长度
 *  pageNum: 10,                // 类型：Number  必有字段  备注：当前页码
 * }
 */
export function message_log_path(data) {
    return service({
        url: '/v1/messageCenter/userMessage/getList',
        method: 'post',
        data
    })
}
