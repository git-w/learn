/**
 * @description: 系统管理-部门管理
 * @author: leroy
 * @Compile：2021-06-02 15：30
 * @update: leroy(2021-06-02 15：30)
 */

import {
    organizatioon_tree_path,  // 树形列表
    organizatioon_save_path,  //添加
    organizatioon_update_path,  //更新
    organizatioon_delete_path,  //删除
} from '../../../api/system'

import { date_format } from '@/utils/date/dateFormat' //时间戳转化成标准时间
export default {
    data() {
        return {
            ids: '',
            date_format, //时间处理器方法 (年-月-日-时-分-秒) 

            treeData: [], // 部门架构 树形数组
            filterText: '', // 部门节点过滤
            expandedKeys: [], // 默认展开的节点

            drawer: false, // 控制抽屜的开合
            title: '', // 控制抽屜的头部

            // 部门管理 表单字段
            addForm: {
                id: '',
                organizationName: '', //类型：String  必有字段  备注：角色名称
                deptLevel: '', //类型：Number  必有字段  备注：组织层级 0:公司(内置) 1:公司的部门
                deptType: '', //类型：String  必有字段  备注：组织类型 0:公司(内置) 1:公司的部门
                parentID: '', //类型：String  可有字段  备注：父标识
                memo: '' //类型：String  可有字段  备注：备注
            },

            // 部门管理 表单验证
            rules: {
                //部门名称
                organizationName: [{
                    required: true,
                    message: '请输入部门名称',
                    trigger: 'blur'
                }]
            }
        }
    },
    mounted() {
        this.to_load() // 进入此页面加载数据
    },

    methods: {
        /**
         * @memo 进入此页面进行加载
         */
        to_load() {
            this.get_organization_tree_data()
        },

        /**
         * @memo  请求部门树的接口
         */
        async get_organization_tree_data() {
            const { data: res } = await organizatioon_tree_path({})
            this.treeData = res
        },

        /*************************** 添加、修改 操作 ******************************** */
        /**
         * @memo 添加-跳往添加界面
         */
        add_btn(data) {
            this.drawer = true
            this.title = "添加部门"
            let deptLevel = data.deptLevel // 为了不改变原来的值，不能直接使用语法糖++。所以需要创建一个新变量来操作
            this.addForm = {
                id: '',
                organizationName: '', //类型：String  必有字段  备注：角色名称
                deptLevel: deptLevel + 1, //类型：Number  必有字段  备注：组织层级 0:公司(内置) 1:公司的部门
                deptType: 1, //类型：String  必有字段  备注：组织类型 0:公司(内置) 1:公司的部门
                parentID: data.id, //类型：String  可有字段  备注：父标识
                memo: '' //类型：String  可有字段  备注：备注
            }
            // 关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /**
         * @memo  修改按钮  点击修改按钮从行内拿数据出来放到表单内显示
         * @param  参数:id   当前修改的数据id
         */
        change_btn(row) {
            if (row) {
                this.addForm = {
                    id: row.id,
                    organizationName: row.organizationName, //类型：String  必有字段  备注：角色名称
                    deptLevel: row.deptLevel, //类型：Number  必有字段  备注：组织层级 0:公司(内置) 1:公司的部门
                    deptType: row.deptType, //类型：String  必有字段  备注：组织类型 0:公司(内置) 1:公司的部门
                    parentID: row.parentID, //类型：String  可有字段  备注：父标识
                    memo: row.memo //类型：String  可有字段  备注：备注
                }
                this.drawer = true
                this.title = '修改部门信息'
            } else {
                this.$message.error('数据错误请刷新页面')
            }
        },

        /**
         * @memo 保存修改方法
         * 根据有无id来判断调用的接口是新增还是修改
         */
        async save_update_data() {
            let param = {
                id: this.addForm.id ? this.addForm.id : '',
                organizationName: this.addForm.organizationName, //类型：String  必有字段  备注：角色名称
                deptLevel: this.addForm.deptLevel, //类型：Number  必有字段  备注：组织层级 0:公司(内置) 1:公司的部门
                deptType: this.addForm.deptType, //类型：String  必有字段  备注：组织类型 0:公司(内置) 1:公司的部门
                parentID: this.addForm.parentID, //类型：String  可有字段  备注：父标识
                memo: this.addForm.memo //类型：String  可有字段  备注：备注
            }
            if (!this.addForm.id) {
                await organizatioon_save_path(param)
                    .then(res => {
                        // 添加成功
                        this.$message.success(res.message)
                        // 添加成功返回列表页
                        this.clear_drawer()
                        this.get_organization_tree_data()
                    })
                    .catch(() => { })
            } else if (this.addForm.id) {
                await organizatioon_update_path(param)
                    .then(res => {
                        // 修改成功
                        this.$message.success(res.message)
                        // 修改成功返回列表页
                        this.clear_drawer()
                        this.get_organization_tree_data()
                    })
                    .catch(() => { })
            }
        },

        /**
         * @memo 提交按钮
         */
        submit_btn(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    this.save_update_data()
                } else {
                    //提交失败
                    return false
                }
            })
        },

        /************************** 删除 操作 ********************************* */
        /**
         * @memo   删除接口
         */
        async delete_data(id) {
            const res = await organizatioon_delete_path({
                ids: id
            })
            return res
        },

        /**
         * @memo  删除部门按钮
         * 点击删除按钮弹出询问框
         * 点击确认之后将进行接口操作
         */
        delete_btn(id) {
            if (id) {
                this.$confirm('是否确定删除?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                })
                    .then(() => {
                        this.delete_data(id)
                            .then(res => {

                                if (res.code === 100000) {
                                    //删除成功
                                    this.$message.success('删除成功')
                                    this.get_organization_tree_data()
                                } else {
                                    this.$message.error(res.message)
                                }
                            }).catch(() => {
                                this.$message({
                                    message: '删除失败!',
                                    type: 'error'
                                })
                            })

                    })
                    .catch(() => { })
            } else {
                this.$message.error('请选择需要删除的内容')
            }

        },

        /************************** 关闭抽屉 操作 ****************************** */
        /**
         * @memo 抽屉关闭前的回调
         */
         handle_close() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },

        /**
         * @memo 点击取消按钮，关闭抽屉
         */
        close_drawer() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },

        /**
         * @memo 关闭抽屉
         */
        clear_drawer() {
            this.drawer = false
            this.addForm = {
                id: '',
                organizationName: '', //类型：String  必有字段  备注：角色名称
                deptLevel: '', //类型：Number  必有字段  备注：组织层级 0:公司(内置) 1:公司的部门
                deptType: '', //类型：String  必有字段  备注：组织类型 0:公司(内置) 1:公司的部门
                parentID: '', //类型：String  可有字段  备注：父标识
                memo: '' //类型：String  可有字段  备注：备注
            }
            //关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /********************** 节点过滤 ****************************** */
        /**
         * @memo   节点过滤
         */
        filter_node(value, data) {
            if (!value) return true
            return data.organizationName.indexOf(value) !== -1
        }
    },
    watch: {
        filterText(val) {
            this.$refs.tree.filter(val)
        }
    }
}
