/**
 * @description: 系统管理-菜单管理
 * @author: leroy
 * @Compile：2020-08-27 15：00
 * @update: leroy(2020-08-27 15：00)
 */

import {
  menu_save_path,  // 添加
  menu_delete_path,  // 删除
  menu_update_path,  // 更新
  menu_info_path,  // 详情
  menu_tree_path,  // 树形列表
  menu_update_state_path  // 批量修改状态
} from '../../../api/system'

import { get_img_url_to_path_list } from '@/utils/img/imageMethod.js'
export default {
  data() {
    return {
      ids: '',

      treeData: [], //菜单管理 列表数组

      // 设置树结构的配置
      defaultProps: {
        children: 'children',
        label: 'name',
        pId: ''
      },

      drawer: false, // 控制抽屉的打开和关闭
      title: '', //控制抽屉的头部
      disabled: true, //表单菜单类型选择框的禁用和启用

      // 菜单管理 表单字段
      addForm: {
        id: '',  // 类型：String  必有字段  备注：编号
        menuName: '',  // 类型：String  必有字段  备注：菜单名称
        icon: '',  // 类型：String  必有字段  备注：菜单或按钮icon
        menuCode: '',  // 类型：String  必有字段  备注：菜单标识
        menuLevel: '',  // 类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
        parentCode: '',  // 类型：String  可有字段  备注：父菜单标识
        parentID: '',  // 类型：String  可有字段  备注：父菜单ID
        sort: '',  // 类型：Number  可有字段  备注：排序号
        menuType: '',  // 类型：Number  必有字段  备注：类型 0-菜单 1-按钮
        buttonType: '',  // 类型：Number  可有字段  备注：按钮区分 1：增改 2：查询 3：删除
        url: '',  // 类型：String  必有字段  备注：链接地址
        pagePath: '',  // 页面路由
        memo: ''  //类型：String  可有字段  备注：备注
      },

      // 目录类型 数组
      menuTypeList: [
        {
          value: 1,
          label: '按钮'
        },
        {
          value: 0,
          label: '菜单'
        },
        {
          value: 2,
          label: '菜单集'
        }
      ],

      // 按钮类型 数组
      buttonTypeList: [
        {
          value: 1,
          label: '添加'
        },
        {
          value: 2,
          label: '查询'
        },
        {
          value: 3,
          label: '删除'
        },
        {
          value: 4,
          label: '修改'
        }
      ],

      // 菜单管理 表单验证
      rules: {
        // 菜单名称
        menuName: [
          {
            required: true,
            message: '请输入菜单名称',
            trigger: 'blur'
          }
        ],
        // 菜单标识
        menuCode: [
          {
            required: true,
            message: '请输入菜单标识',
            trigger: 'blur'
          }
        ],
        // 排序
        sort: [
          {
            required: true,
            message: '请输入排序',
            trigger: 'blur'
          },
          {
            pattern: /^[0-9]+[0-9]*$/,
            message: '请输入正整数'
          }
        ],
        // 菜单类型
        menuType: [
          {
            required: true,
            message: '请选择类型',
            trigger: 'change'
          }
        ],
        // 按钮类型
        buttonType: [
          {
            required: true,
            message: '请选择按钮类型',
            trigger: 'change'
          }
        ],
        // 页面路由
        pagePath: [
          {
            required: true,
            message: '请输入页面路由',
            trigger: 'blur'
          },
        ],
        // 链接地址
        url: [
          {
            required: true,
            message: '请输入链接地址',
            trigger: 'blur'
          }
        ]
      },

    }
  },
  mounted() {
    this.to_load() // 进入此页面进行请求
  },
  methods: {
    /**
     * @memo 进入此页面进行加载
     */
    to_load() {
      this.get_tree_data() // 进入此页面进行请求
    },

    /**
     * @memo  组织树请求
     */
    async get_tree_data() {
      let res = await menu_tree_path({})
      this.treeData = res.data
    },

    /**
     * @memo  下拉框选中的按钮的操作
     */
    handle_command(command) {
      if (command.button == 'add_tree_son_menu_btn') {
        //添加子级目录
        this.add_tree_son_menu_btn(command.row)
      } else if (command.button == 'add_tree_son_menu') {
        //添加子级菜单
        this.add_tree_son_menu(command.row)
      } else if (command.button == 'add_tree_son_btn') {
        //添加按钮
        this.add_tree_son_btn(command.row)
      } else if (command.button == 'change_btn') {
        //修改菜单信息
        this.change_btn(command.row)
      } else if (command.button == 'delete_btn') {
        //删除菜单
        this.delete_btn(command.row)
      } else if (command.button === 'enabled_btn') {
        //启用
        this.update_state_data(command.row, '0')
      } else if (command.button === 'disable_btn') {
        //禁用
        this.update_state_data(command.row, '1')
      }
    },
    /**
     * @memo  下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
     */
    compose_value(item, row) {
      return {
        button: item,
        row: row
      }
    },


    /*************************** 添加、修改菜单操作 ************************************* */
    /**
     * @memo  添加一级目录
     */
    add_tree_parent() {
      this.drawer = true
      this.title = '添加一级目录'
      this.addForm = {
        id: '', //类型：String  必有字段  备注：编号
        menuName: '', // 类型：String  必有字段  备注：菜单名称
        icon: [], // 类型：String  必有字段  备注：菜单或按钮icon
        menuCode: '', // 类型：String  必有字段  备注：菜单标识
        menuLevel: 1, // 类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
        parentCode: 1, // 类型：String  可有字段  备注：父菜单标识
        parentID: '', // 类型：String  可有字段  备注：父菜单ID
        sort: '', // 类型：Number  可有字段  备注：排序号
        menuType: 0, // 类型：Number  必有字段  备注：类型 0-菜单 1-按钮
        buttonType: '', // 类型：Number  可有字段  备注：按钮区分 1：增改 2：查询 3：删除
        url: '', // 类型：String  必有字段  备注：链接地址
        pagePath: '', // 类型：String  必有字段  备注：页面路由
        memo: '' //类型：String  可有字段  备注：备注
      }
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /**
     * @memo  添加子级目录
     */

    add_tree_son_menu_btn(data) {
      let menuLevel = Number(data.attributes.MenuLevel) + 1
      let parentCode = Number(data.attributes.parentCode) + 1

      this.addForm = {
        id: '', //类型：String  必有字段  备注：编号
        menuName: '', // 类型：String  必有字段  备注：菜单名称
        icon: [], // 类型：String  必有字段  备注：菜单或按钮icon
        menuCode: '', // 类型：String  必有字段  备注：菜单标识
        menuLevel: menuLevel, // 类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
        parentCode: parentCode, // 类型：String  可有字段  备注：父菜单标识
        parentID: data.id, // 类型：String  可有字段  备注：父菜单ID
        sort: '', // 类型：Number  可有字段  备注：排序号
        menuType: menuLevel == 2 ? 2 : '', // 类型：Number  必有字段  备注：类型 0-菜单 1-按钮 3 菜单集
        buttonType: '', // 类型：Number  可有字段  备注：按钮区分 1：增改 2：查询 3：删除
        url: '', // 类型：String  必有字段  备注：链接地址
        pagePath: '', // 页面路由
        memo: '' //类型：String  可有字段  备注：备注
      }
      this.disabled = true //禁用菜单类型选择框
      this.imageUrl = '' //清空菜单标识预览路径
      this.drawer = true
      this.title = '添加子级目录'
      //清空表单字段
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /**
     * @memo 添加子级菜单
     */
    add_tree_son_menu(data) {
      let menuLevel = Number(data.attributes.MenuLevel) + 1
      let parentCode = Number(data.attributes.parentCode) + 1

      this.addForm = {
        id: '', //类型：String  必有字段  备注：编号
        menuName: '', // 类型：String  必有字段  备注：菜单名称
        icon: [], // 类型：String  必有字段  备注：菜单或按钮icon
        menuCode: '', // 类型：String  必有字段  备注：菜单标识
        menuLevel: menuLevel, // 类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
        parentCode: parentCode, // 类型：String  可有字段  备注：父菜单标识
        parentID: data.id, // 类型：String  可有字段  备注：父菜单ID
        sort: '', // 类型：Number  可有字段  备注：排序号
        menuType:
          menuLevel == 1 ? '' : menuLevel == 2 ? 0 : menuLevel == 3 ? 0 : '', // 类型：Number  必有字段  备注：类型 0-菜单 1-按钮
        buttonType: '', // 类型：Number  可有字段  备注：按钮区分 1：增改 2：查询 3：删除
        url: '', // 类型：String  必有字段  备注：链接地址
        pagePath: '', // 页面路由
        memo: '' //类型：String  可有字段  备注：备注
      }
      this.disabled = true //禁用菜单类型选择框
      this.imageUrl = '' //清空菜单标识预览路径
      this.drawer = true
      this.title = '添加子级菜单'
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /**
     *  @memo  添加按钮
     */
    add_tree_son_btn(data) {
      let menuLevel = Number(data.attributes.MenuLevel) + 1
      let parentCode = Number(data.attributes.parentCode) + 1

      this.addForm = {
        id: '', //类型：String  必有字段  备注：编号
        menuName: '', // 类型：String  必有字段  备注：菜单名称
        icon: [], // 类型：String  必有字段  备注：菜单或按钮icon
        menuCode: '', // 类型：String  必有字段  备注：菜单标识
        menuLevel: menuLevel, // 类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
        parentCode: parentCode, // 类型：String  可有字段  备注：父菜单标识
        parentID: data.id, // 类型：String  可有字段  备注：父菜单ID
        sort: '', // 类型：Number  可有字段  备注：排序号
        menuType: 1, // 类型：Number  必有字段  备注：类型 0-菜单 1-按钮
        buttonType: '', // 类型：Number  可有字段  备注：按钮区分 1：增改 2：查询 3：删除
        url: '', // 类型：String  必有字段  备注：链接地址
        pagePath: '', // 页面路由
        memo: '' //类型：String  可有字段  备注：备注
      }
      this.disabled = true //禁用菜单类型选择框
      this.imageUrl = '' //清空菜单标识预览路径
      this.drawer = true
      this.title = '添加按钮'
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /**
     * @memo 详情 接口
     */
    async get_info_data(id) {
      const res = await menu_info_path({
        id: id
      })
      return res
    },

    /**
     * @memo  点击修改按钮 打开对话框
     * @param   参数: id  当前操作数据的id
     */
    change_btn(id) {
      if (id) {
        this.get_info_data(id)
          .then(res => {
            this.imageUrl = '' //清空菜单标识预览路径
            this.addForm = {
              id: res.data.id, //类型：String  必有字段  备注：编号
              menuName: res.data.menuName, // 类型：String  必有字段  备注：菜单名称
              icon: get_img_url_to_path_list(
                res.data.icon,
                res.data.iconPath
              ), // 类型：String  必有字段  备注：菜单或按钮icon
              menuCode: res.data.menuCode, // 类型：String  必有字段  备注：菜单标识
              menuLevel: res.data.menuLevel, // 类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
              parentCode: res.data.parentCode, // 类型：String  可有字段  备注：父菜单标识
              parentID: res.data.parentID, // 类型：String  可有字段  备注：父菜单ID
              sort: res.data.sort, // 类型：Number  可有字段  备注：排序号
              menuType: res.data.menuType, // 类型：Number  必有字段  备注：类型 0-菜单 1-按钮
              buttonType: res.data.buttonType, // 类型：Number  可有字段  备注：按钮区分 1：增改 2：查询 3：删除
              url: res.data.url, // 类型：String  必有字段  备注：链接地址
              pagePath: res.data.pagePath, // 页面路由
              memo: res.data.memo //类型：String  可有字段  备注：备注
            }
            this.drawer = true
            this.title = '修改菜单信息'
          })
          .catch(() => { })
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /**
     * @memo 保存修改方法
     * 根据有无id来判断调用的接口是新增还是修改
     */
    async save_update_data() {
      // 菜单标识
      let iconPath =
        this.addForm.icon.length > 0 ? this.addForm.icon[0].imgUrl : ''
      let param = {
        id: this.addForm.id, //类型：String  必有字段  备注：编号
        menuName: this.addForm.menuName, // 类型：String  必有字段  备注：菜单名称  ·
        icon: iconPath, // 类型：String  必有字段  备注：菜单或按钮icon
        menuCode: this.addForm.menuCode, // 类型：String  必有字段  备注：菜单标识  ·
        menuLevel: this.addForm.menuLevel, // 类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
        parentCode: this.addForm.parentCode, // 类型：String  可有字段  备注：父菜单标识
        parentID: this.addForm.parentID, // 类型：String  可有字段  备注：父菜单ID
        sort: this.addForm.sort, // 类型：Number  可有字段  备注：排序号 ·
        menuType: this.addForm.menuType, // 类型：Number  必有字段  备注：类型 0-菜单 1-按钮 ·
        buttonType: this.addForm.buttonType, // 类型：Number  可有字段  备注：按钮区分 1：增改 2：查询 3：删除 ·
        url: this.addForm.url, // 类型：String  必有字段  备注：链接地址 ·
        pagePath: this.addForm.pagePath, // 页面路由
        memo: this.addForm.memo //类型：String  可有字段  备注：备注
      }
      if (!this.addForm.id) {
        await menu_save_path(param)
          .then(res => {
            // 添加成功
            this.$message.success(res.message)
            // 添加成功返回列表页
            this.clear_drawer()
            // 重新加载列表
            this.get_tree_data()
          })
          .catch(() => { })
      } else if (this.addForm.id) {
        await menu_update_path(param)
          .then(res => {
            // 修改成功
            this.$message.success(res.message)
            // 添加成功返回列表页
            this.clear_drawer()
            // 重新加载列表
            this.get_tree_data()
          })
          .catch(() => { })
      }
    },

    /**
     * @memo 提交按钮
     */
    submit_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_update_data()
        } else {
          // 提交失败
          return false
        }
      })
    },

    /********************** 修改状态 操作 ********************************* */
    /**
     * @memo  修改状态 接口
     */
    async update_state_data(id, isState) {
      if (id) {
        const param = {
          ids: id,
          isState: isState
        }
        await menu_update_state_path(param)
          .then(res => {
            this.$message({
              message: res.message,
              type: 'success'
            })
            this.get_tree_data()
          })
          .catch(() => { })
      } else {
        this.$message.error('请选择需要修改状态的内容')
      }
    },


    /*********************** 删除 操作 ************************************** */
    /**
     * @memo  删除接口
     */
    async delete_data(id) {
      const res = await menu_delete_path({
        ids: id
      })
      return res
    },

    /**
     * @memo 删除菜单 按钮
     */
    delete_btn(id) {
      if (id) {
        this.$confirm('是否确定删除?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        })
          .then(() => {
            this.delete_data(id)
              .then(res => {
                // 删除成功后重新加载列表
                this.treeData = []
                this.get_tree_data()
                this.$message.success(res.message)
              })
              .catch(() => { })
          })
          .catch(() => { })
      } else {
        this.$message.error('请选择需要删除的内容!')
      }
    },

    /************************* 关闭抽屉 操作 ********************************* */
    /**
     * @memo 抽屉关闭前的回调
     */
    handle_close() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => { })
    },

    /**
     * @memo 点击取消按钮，关闭弹窗
     */
    close_drawer() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => { })
    },

    /**
     * @memo 清除数据并且关抽屉
     */
    clear_drawer() {
      this.drawer = false
      this.addForm = {
        id: '', //类型：String  必有字段  备注：编号
        menuName: '', // 类型：String  必有字段  备注：菜单名称
        icon: [], // 类型：String  必有字段  备注：菜单或按钮icon
        menuCode: '', // 类型：String  必有字段  备注：菜单标识
        menuLevel: '', // 类型：Number  必有字段  备注：菜单层级 1:一级菜单 2:二级菜单 依次类推...
        parentCode: '', // 类型：String  可有字段  备注：父菜单标识
        parentID: '', // 类型：String  可有字段  备注：父菜单ID
        sort: '', // 类型：Number  可有字段  备注：排序号
        menuType: '', // 类型：Number  必有字段  备注：类型 0-菜单 1-按钮
        buttonType: '', // 类型：Number  可有字段  备注：按钮区分 1：增改 2：查询 3：删除
        url: '', // 类型：String  必有字段  备注：链接地址
        pagePath: '', // 页面路由
        memo: '' //类型：String  可有字段  备注：备注
      }
      this.imageUrl = '' // 清空菜单标识预览路径
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /*********************** 选择 操作 ************************** */
    /**
     * @memo  选择按钮属性
     * @param  参数: buttonType   1:增改 2:详情 3:删除
     */
    select_buttonType(buttonType) {
      if (buttonType == '1') {
        this.addForm.menuName = '添加'
        this.addForm.menuCode = 'add'
        this.addForm.sort = '0'
      } else if (buttonType == '2') {
        this.addForm.menuName = '详情'
        this.addForm.menuCode = 'info'
        this.addForm.sort = '2'
      } else if (buttonType == '3') {
        this.addForm.menuName = '删除'
        this.addForm.menuCode = 'delete'
        this.addForm.sort = '1'
      } else if (buttonType == '4') {
        this.addForm.menuName = '修改'
        this.addForm.menuCode = 'update'
        this.addForm.sort = '3'
      }
    },

    /*********************** 列表judge 操作 *************************** */
    /**
     * @memo  类型
     * @param  参数: MenuType 0:菜单 1:按钮   MenuLevel: 1:一级菜单 2:二级菜单 3:三级菜单   ButtonType: 1:增改按钮 2:查询按钮 3:删除按钮
     */
    menuType_judge(row) {
      if (row.attributes.MenuType == '0') {
        // 菜单
        if (row.attributes.MenuLevel == 1) {
          return '一级菜单'
        } else if (row.attributes.MenuLevel == 2) {
          return '二级菜单'
        } else if (row.attributes.MenuLevel == 3) {
          return '三级菜单'
        }
      } else if (row.attributes.MenuType == '1') {
        // 按钮
        if (row.attributes.ButtonType == 1) {
          return '增改按钮'
        } else if (row.attributes.ButtonType == 2) {
          return '查询按钮'
        } else if (row.attributes.ButtonType == 3) {
          return '删除按钮'
        } else if (row.attributes.ButtonType == 4) {
          return '修改按钮'
        }
      } else if (row.attributes.MenuType == '2') {
        if (row.attributes.MenuLevel == 2) {
          return '二级菜单'
        }
      }
    },

    /**
     * @memo  状态
     * @param isState  //类型：String  可有字段  备注：状态 0启用 1停用
     */
    state_judge(isState) {
      if (isState == '0') {
        return '启用'
      } else if (isState == '1') {
        return '停用'
      }
    },
  }
}
