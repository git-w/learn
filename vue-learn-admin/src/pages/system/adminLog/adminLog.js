/**
 * @description: 系统设置-日志管理
 * @author: leroy
 * @Compile：2020-12-08 11：00
 * @update: leroy(2020-12-08 12：00)
 */
import {
  account_log_path,  // 后台日志管理 列表
  user_log_path,  // 小程序登录日志管理 列表
  user_pay_path,  // 用户支付日志 列表
} from '../../../api/system'

import {
  date_format
} from '../../../utils/date/dateFormat' //时间戳转化成标准时间

export default {
  data() {
    return {
      ids: '',

      date_format, // 时间处理器方法 (年-月-日-时-分-秒) 
    
      activeName: 'backstage',
      tableType: '1', //列表的属性

      // 管理员日志管理 检索表单
      searchForm: {
        loginName: '',  //类型：String  可有字段  备注：用户账号
        phone: '',  //类型：String  可有字段  备注：联系方式
        loginTime: []  //类型：String  可有字段  备注：登录时间
      },
      tableData: [], // 管理员日志管理列表 数组
      
      // 小程序日志管理 检索表单
      searchUserForm: {
        totals: 0,
        pageNum: 1,
        pageSize: 10,
        nickname: '',  //类型：String  可有字段  备注：用户昵称
        ipLocation: '',  //类型：String  可有字段  备注：联系方式
        loginTime: []  //类型：String  可有字段  备注：登录时间
      },
      tableUserData: [],  // 小程序日志管理列表 数组

      // 小程序日志管理 检索表单
      searchPayForm: {
        totals: 0,
        pageNum: 1,
        pageSize: 10,
        nickName: '',  //类型：String  可有字段  备注：用户昵称
        modilePhone: '',  //类型：String  可有字段  备注：联系方式
        orderNum: '',  //类型：String  可有字段  备注：订单号
        createDate: [],  //类型：String  可有字段  备注：支付时间
      },
      tablePayData: [],  // 用户支付日志列表 数组
    }
  },

  mounted() {
    this.to_load()  //进入此页面进行请求
  },

  methods: {

    /**
     * @memo   点击tabs切换
     */
    handle_click(tab, event) {
      if (tab.name == 'backstage') {
        // 管理员登录日志
        this.tableType = '1'  
        this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize) //进入此页面加载列表
      } else if (tab.name == 'smallroutine') {
        // 小程序登录日志
        this.tableType = '2' 
        this.get_tabel_data(this.searchUserForm.pageNum, this.searchUserForm.pageSize) //进入此页面加载列表
      } else if (tab.name == 'userPayLog') {
        // 用户支付日志
        this.tableType = '3' 
        this.get_tabel_data(this.searchPayForm.pageNum, this.searchPayForm.pageSize) //进入此页面加载列表
      }
    },

    /**
     * @memo 进入此页面进行加载
     */
    to_load() {
      this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize) //进入此页面加载列表
    },

    /**
     * @memo  检索搜索按钮
     */
    search_btn() {
      if (this.tableType == 1) {  
        // 管理员登录日志
        this.get_tabel_data(1, this.searchForm.pageSize) // 请求列表数据
      } else if (this.tableType == 2) {  
        // 小程序登录日志
        this.get_tabel_data(1, this.searchUserForm.pageSize)
      } else if (this.tableType == 3) {  
        // 用户支付日志
        this.get_tabel_data(1, this.searchPayForm.pageSize)
      }
    },

    /**
     * @memo  检索重置按钮
     */
    reset_btn() {
      if (this.tableType == 1) {  
        // 管理员登录日志
        this.searchForm.loginName = ''  //类型：String  可有字段  备注：用户账号
        this.searchForm.phone = ''  //类型：String  可有字段  备注：联系方式
        this.searchForm.loginTime = []  //类型：String  可有字段  备注：登录时间
        this.get_tabel_data(1, this.searchForm.pageSize)
      } else if (this.tableType == 2) {
        // 小程序登录日志
        this.searchUserForm.nickname = ''  //类型：String  可有字段  备注：用户昵称
        this.searchUserForm.ipLocation = ''  //类型：String  可有字段  备注：联系方式
        this.searchUserForm.loginTime = []  //类型：String  可有字段  备注：登录时间
        this.get_tabel_data(1, this.searchUserForm.pageSize)
      } else if (this.tableType == 3) {  
        // 用户支付日志
        this.searchPayForm.nickName = ''  //类型：String  可有字段  备注：用户昵称
        this.searchPayForm.modilePhone = ''  //类型：String  可有字段  备注：联系方式
        this.searchPayForm.orderNum = ''  //类型：String  可有字段  备注：订单号
        this.searchPayForm.createDate = []  //类型：String  可有字段  备注：支付时间
        this.get_tabel_data(1, this.searchPayForm.pageSize)
      }
    },

    /**
     * @memo 获取用户列表
     * @param pageNum  //类型：Number  必有字段  备注：当前页
     * @param pageSize  //类型：Number  必有字段  备注：每页条数
     * @param totals  //类型：Number  必有字段  备注：总条数
     */
    async get_tabel_data(pageNum, pageSize) {
      if (this.tableType == 1) {
        // 管理员登录日志
        const param = {
          pageNum: pageNum,  //类型：Number  必有字段  备注：当前页
          pageSize: pageSize,  //类型：Number  必有字段  备注：每页条数
          loginName: this.searchForm.loginName,  //类型：String  可有字段  备注：用户账号
          phone: this.searchForm.phone,  //类型：String  可有字段  备注：联系方式
          startTime: this.searchForm.loginTime != null ? this.searchForm.loginTime[0] : '',  //类型：String  可有字段  备注：登录开始时间
          endTime: this.searchForm.loginTime != null ? this.searchForm.loginTime[1] : ''  //类型：String  可有字段  备注：登录结束时间
        }
        await account_log_path(param)
          .then(res => {
            this.tableData = res.data.resultList
            this.searchForm.totals = res.data.totalRows
            this.searchForm.pageSize = res.data.pageSize // 当前页码条数
            this.searchForm.pageNum = res.data.pageNum // 请求页码
            if (this.searchForm.pageNum > 1) {
              if (res.data.resultList.length == 0) {
                this.get_tabel_data(
                  this.searchForm.pageNum - 1,
                  this.searchForm.pageSize
                )
              }
            } else if (this.searchForm.pageNum == 0) {
              return this.$messag.warning('没有更多数据')
            }
          })
      } else if (this.tableType == 2) {
        // 小程序登录日志
        const param = {
          pageNum: pageNum,  //类型：Number  必有字段  备注：当前页
          pageSize: pageSize,  //类型：Number  必有字段  备注：每页条数
          nickname: this.searchUserForm.nickname,  //类型：String  可有字段  备注：用户昵称
          startTime: this.searchUserForm.loginTime != null ? this.searchUserForm.loginTime[0] : '',  //类型：String  可有字段  备注：登录开始时间
          endTime: this.searchUserForm.loginTime != null ? this.searchUserForm.loginTime[1] : ''  //类型：String  可有字段  备注：登录结束时间
        }
        await user_log_path(param)
          .then(res => {
            this.tableUserData = res.data.resultList
            this.searchUserForm.totals = res.data.totalRows
            this.searchUserForm.pageSize = res.data.pageSize // 当前页码条数
            this.searchUserForm.pageNum = res.data.pageNum // 请求页码
            if (this.searchUserForm.pageNum > 1) {
              if (res.data.resultList.length == 0) {
                this.get_tabel_data(
                  this.searchUserForm.pageNum - 1,
                  this.searchUserForm.pageSize
                )
              }
            } else if (this.searchUserForm.pageNum == 0) {
              return this.$messag.warning('没有更多数据')
            }
          })
      } else if (this.tableType == 3) {
        // 用户支付日志
        const param = {
          pageNum: pageNum,  //类型：Number  必有字段  备注：当前页
          pageSize: pageSize,  //类型：Number  必有字段  备注：每页条数
          nickName: this.searchPayForm.nickName,  //类型：String  可有字段  备注：用户昵称
          modilePhone: this.searchPayForm.modilePhone,  //类型：String  可有字段  备注：手机号
          orderNum: this.searchPayForm.orderNum,  //类型：String  可有字段  备注：订单号
          startTime: this.searchPayForm.createDate != null ? this.searchPayForm.createDate[0] : '',  //类型：String  可有字段  备注：支付开始时间
          endTime: this.searchPayForm.createDate != null ? this.searchPayForm.createDate[1] : ''  //类型：String  可有字段  备注：支付结束时间
        }
        await user_pay_path(param)
          .then(res => {
            this.tablePayData = res.data.resultList
            this.searchPayForm.totals = res.data.totalRows
            this.searchPayForm.pageSize = res.data.pageSize // 当前页码条数
            this.searchPayForm.pageNum = res.data.pageNum // 请求页码
            if (this.searchPayForm.pageNum > 1) {
              if (res.data.resultList.length == 0) {
                this.get_tabel_data(
                  this.searchPayForm.pageNum - 1,
                  this.searchPayForm.pageSize
                )
              }
            } else if (this.searchPayForm.pageNum == 0) {
              return this.$messag.warning('没有更多数据')
            }
          })
      }
    },

    /****************************** 用户支付日志 列表judge ****************************** */
    /**
     * @memo 支付方式 judge
     */
    payTypeJudge(payType) {
      if (payType == 1) {
        return '微信扫码'
      } else if (payType == 2) {
        return '微信公众号内支付'
      } else if (payType == 3) {
        return '微信支付'
      } else if (payType == 4) {
        return '其他'
      }
    },
    
    /**
     * @memo 订单状态 judge
     */
    isStatusJudge(isStatus) {
      if (isStatus == 1) {
        return '未支付'
      } else if (isStatus == 2) {
        return '支付成功'
      }
    },

    /****************************** 列表分页 ****************************** */
    /**
     * @memo 小程序登录日志 列表分页 相关方法
     */
    change_user_size_btn(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_data(1, val)
    },
    change_user_current_btn(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_data(val, this.searchUserForm.pageSize)
    },

    /**
     * @memo 用户支付日志 列表分页 相关方法
     */
    change_pay_size_btn(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_data(1, val)
    },
    change_pay_current_btn(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_data(val, this.searchUserForm.pageSize)
    },
  }
}
