/**
 * @description: 系统管理-角色管理
 * @author: leroy
 * @Compile：2020-08-27 18：00
 * @update: leroy(2020-08-27 18：00)
 */
import {
  role_save_path, // 添加
  role_delete_path, // 删除
  role_update_path, // 更新
  role_info_path, // 详情
  role_list_path, // 列表
  menu_tree_path, // 树形列表
  organizatioon_tree_path //部门管理 全部列表
} from '../../../api/system'

import { date_format } from '../../../utils/date/dateFormat' //时间戳转化成标准时间

export default {
  data() {
    //添加权限校验
    let istreeData = (rule, value, callback) => {
      //判断已选中的权限树是否大于0
      const _that = this
      if (_that.$refs.checked.getCheckedKeys().length > 0) {
        callback()
      } else {
        //权限树小于等于0,提示请选择权限
        return callback(new Error('请选择权限'))
      }
    }
    return {
      // 部门动态加载级联选择的配置
      props: {
        checkStrictly: true,
        value: 'id',
        label: 'organizationName'
      },

      // 角色管理 检索字段
      searchForm: {
        name: '' //类型：String  可有字段  备注：角色名称
      },
      tableData: [], // 角色管理列表数组
      organizatioonList: [], // 部门下拉选择数组
      date_format, //时间处理器方法 (年-月-日-时-分-秒)

      drawer: false, // 控制抽屉打开和隐藏
      title: '', // 控制抽屉的头部
      checkedIds: [], // 默认选中的id数组
      treeData: [], // 树结构的数据

      // 角色管理 表单字段
      addForm: {
        roleName: '', //类型：String  可有字段  备注：角色名称
        id: '', //类型：String  必有字段  备注：无
        isUpdate: '', //类型：Number  可有字段  备注： 是否可以编辑 0-是 1-否
        isStatus: '', //类型：Number  可有字段  备注：账号状态 0:正常/启用 1:不启用
        departmentID: '', //类型：Number  可有字段  备注：部门id
        memo: '', //类型：String  可有字段  备注：备注
        menuIdList: '', //类型：String  可有字段  备注：菜单id
        code: '' //类型：String  可有字段  备注： 角色编号
      },

      // 权限树选择的配置
      defaultProps: {
        children: 'children',
        label: 'menuName'
      },

      //角色管理员 表单验证
      rules: {
        //角色名称
        roleName: [
          {
            required: true,
            message: '请输入角色名称',
            trigger: 'blur'
          }
        ],
        //部门
        // departmentID: [
        //   {
        //     required: true,
        //     message: '请选择部门',
        //     trigger: 'change'
        //   }
        // ],
        //权限树
        treeData: [
          {
            validator: istreeData
          }
        ],
        // 是否可编辑
        isUpdate: [
          {
            required: true,
            message: '请选择是否可编辑',
            trigger: 'change'
          }
        ],
        // 状态
        isStatus: [
          {
            required: true,
            message: '请选择状态',
            trigger: 'change'
          }
        ],
        // 角色编号
        code: [
          {
            required: true,
            message: '请输入角色编号',
            trigger: 'blur'
          }
        ]
      }
    }
  },

  mounted() {
    this.to_load() // 进入此页面进行请求
  },

  methods: {
    /**
     * @memo  进入页面加载数据
     */
    to_load() {
      this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize) // 请求列表数据
      this.get_organizatioon_list() //请求部门列表
    },

    /**
     * @memo  检索重置按钮
     */
    reset_btn() {
      this.searchForm.name = '' //类型：String  可有字段  备注：角色名称
      this.get_tabel_data(1, this.searchForm.pageSize) // 请求列表数据
    },

    /**
     * @memo 获取列表
     */
    async get_tabel_data(pageNum, pageSize) {
      let param = {
        pageNum: pageNum, //类型：Number  必有字段  备注：页码 默认第一页
        pageSize: pageSize, //类型：Number  必有字段  备注：条码 每页默认十条
        roleName: this.searchForm.name //类型：String  可有字段  备注：角色名称
      }
      await role_list_path(param)
        .then(res => {
          this.tableData = res.data.resultList
          this.searchForm.totals = res.data.totalRows
          this.searchForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchForm.pageNum > 1) {
            if (res.data.resultList.length === 0) {
              this.get_tabel_data(
                this.searchForm.pageNum - 1,
                this.searchForm.pageSize
              )
            }
          } else if (this.searchForm.pageNum === 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => { })
    },

    /**
     * @memo  下拉框选中的按钮的操作
     */
    handle_command(command) {
      if (command.button == 'delete_btn') {
        //删除
        this.delete_btn(command.row)
      } else if (command.button == 'change_btn') {
        //修改
        this.change_btn(command.row)
      } else if (command.button == 'info_btn') {
        //详情
        this.info_btn(command.row)
      }
    },

    /**
     * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
     */
    compose_value(item, row) {
      return {
        button: item,
        row: row
      }
    },

    /**
     * @memo 菜单树接口
     */
    async get_menu_tree() {
      const { data: res } = await menu_tree_path({})
      this.treeData = res
    },

    /**
     * @memo 部门下拉选择的数据
     * 调用findAll的接口，返回一个数组，用于下拉列表
     * organizatioonList 下拉数组
     */
    async get_organizatioon_list() {
      const { data: res } = await organizatioon_tree_path({ parentID: '' })
      this.organizatioonList = res
    },

    /**
     * @memo  部门级联选择器的下拉加载配置方法
     */
    async lazy_load_data(node, resolve) {
      if (node.level != '0') {
        await organizatioon_tree_path({ parentID: node.value }).then(res => {
          const nodes = res.data
          // 通过调用resolve将子节点数据返回，通知组件数据加载完成
          resolve(nodes)
        })
      }
    },


    /************************** 添加、修改 操作 ******************************* */
    /**
     * @memo 添加-跳往添加界面
     */
    add_btn() {
      this.drawer = true
      this.title = '添加角色'
      this.addForm = {
        roleName: '', //类型：String  可有字段  备注：角色名称
        id: '', //类型：String  必有字段  备注：无
        isUpdate: 0, //类型：Number  可有字段  备注： 是否可以编辑 0-是 1-否
        isStatus: 0, //类型：Number  可有字段  备注：账号状态 0:正常/启用 1:不启用
        departmentID: '', //类型：Number  可有字段  备注：部门id
        memo: '', //类型：String  可有字段  备注：备注
        menuIdList: '', //类型：String  可有字段  备注：菜单id
        code: 'user' //类型：String  可有字段  备注：角色编号
      }
      this.treeData = [] // 清空树状的数据
      this.checkedIds = [] // 清空树状的勾选数组的数据
      this.get_menu_tree() //请求树形菜单列表数据
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /**
     * @memo  详情接口
     */
    async get_info_data(id) {
      const res = await role_info_path({
        id: id
      })
      return res
    },

    /**
     * @memo 修改 按钮
     */
    change_btn(id) {
      if (id) {
        this.checkedIds = []
        this.get_menu_tree()
        this.get_info_data(id)
          .then(res => {
            this.addForm = {
              id: res.data.id, //类型：String  必有字段  备注：编号
              roleName: res.data.roleName, //类型：String  可有字段  备注：角色名称
              departmentID: res.data.departmentID
                ? res.data.departmentID.split(',')
                : [], //类型：String  可有字段  备注：部门ID
              departmentName: res.data.departmentName, //类型：String  可有字段  备注：部门名称
              menuIdList: [], //类型：String  可有字段  备注：所有菜单id 多个英文逗号隔开
              isUpdate: res.data.isUpdate, //类型：Number  可有字段  备注： 是否可以编辑 0-是 1-否
              isStatus: res.data.isStatus, //类型：Number  可有字段  备注：账号状态 0:正常/启用 1:不启用
              memo: res.data.memo, //类型：String  可有字段  备注：备注
              code: res.data.code //类型：String  可有字段  备注：角色编号
            }

            //回显选择的菜单
            let checkedIds = []
            if (res.data.treeList) {
              let treeData = res.data.treeList
              for (var i in treeData) {
                // 为menuList的下标
                var children = treeData[i].children
                for (var k in children) {
                  var children1 = children[k].children

                  for (var j in children1) {
                    if (!children1[j].children) {
                      checkedIds.push(children1[j].id) // 只过滤到第三级，第一级勾选的话子级都会被勾选。
                    } else {
                      var children2 = children1[j].children
                      for (var f in children2) {
                        checkedIds.push(children2[f].id) // 只过滤到第三级，第一级勾选的话子级都会被勾选。
                      }
                    }
                  }
                }
              }
            }
            this.checkedIds = checkedIds
            this.drawer = true
            this.title = '修改角色'
          })
          .catch(() => { })
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /**
     * @memo 保存修改方法
     * 根据有无id来判断调用的接口是新增还是修改
     */
    async save_update_data() {
      let departmentIDArry = this.addForm.departmentID
      let param = {
        id: this.addForm.id ? this.addForm.id : '', //类型：String  必有字段  备注：编号
        roleName: this.addForm.roleName, //类型：String  可有字段  备注：角色名称
        departmentID: departmentIDArry[departmentIDArry.length - 1], //类型：String  可有字段  备注：部门id
        menuIdList: this.addForm.menuIdList
          ? this.addForm.menuIdList.join(',')
          : '', //类型：String  可有字段  备注：所有菜单id 多个英文逗号隔开
        isUpdate: this.addForm.isUpdate, //类型：Number  可有字段  备注： 是否可以编辑 0-是 1-否
        isStatus: this.addForm.isStatus, //类型：Number  可有字段  备注：账号状态 0:正常/启用 1:不启用
        memo: this.addForm.memo, //类型：String  可有字段  备注：备注
        code: this.addForm.code //类型：String  可有字段  备注：角色编号
      }
      if (!this.addForm.id) {
        await role_save_path(param)
          .then(res => {
            // 添加成功
            this.$message.success(res.message)
            // 添加成功返回列表页
            this.clear_drawer()
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => { })
      } else if (this.addForm.id) {
        await role_update_path(param)
          .then(res => {
            // 修改成功
            this.$message.success(res.message)
            // 修改成功返回列表页
            this.clear_drawer()
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => { })
      }
    },

    /**
     * @memo  提交按钮
     */
    submit_form(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.addForm.menuIdList = this.$refs.checked
            .getCheckedKeys()
            .concat(this.$refs.checked.getHalfCheckedKeys()) // 合并数组   选中的和半选中的
          this.save_update_data()
        } else {
          // 提交失败
          return false
        }
      })
    },

    /************************ 删除 操作 ********************************** */
    /**
     * @memo 删除接口
     */
    async delete_data(id) {
      const res = await role_delete_path({
        ids: id
      })
      return res
    },

    /**
     * @memo 删除 按钮
     */
    delete_btn(id) {
      if (id) {
        this.$confirm('是否确定删除?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        })
          .then(() => {
            this.delete_data(id)
              .then(res => {
                this.$message.success(res.message)
                this.get_tabel_data(
                  this.searchForm.pageNum,
                  this.searchForm.pageSize
                )
              })
              .catch(() => { })
          })
          .catch(() => { })
      } else {
        this.$message.error('请选择需要删除的内容!')
      }
    },


    /************************* 关闭抽屉 操作 ******************************* */
    /**
     * @memo 抽屉关闭前的回调
     */
    handle_close() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => { })
    },

    /**
     * @memo 点击取消按钮，关闭弹窗
     */
    close_drawer() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => { })
    },

    /**
     * @memo 清除数据 - 添加和修改后
     */
    clear_drawer() {
      this.drawer = false
      this.addForm = {
        id: '', // 类型：String  必有字段  备注：编号
        roleName: '', // 类型：String  可有字段  备注：角色名称
        departmentID: '', // 类型：String  可有字段  备注：部门ID
        menuIdList: [] // 类型：String  可有字段  备注：所有菜单id 多个英文逗号隔开
      }
      this.treeData = [] // 清空树状的数据
      this.checkedIds = [] // 清空树状的勾选数组的数据

      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },


    /*********************** 列表judge 操作 *************************** */
    /**
     * @memo 状态 judge
     */
    state_judge(state) {
      if (state == '0') {
        return '启用'
      } else if (state == '1') {
        return '停用'
      }
    },

    /**
     * @memo 是否可编辑 judge
     */
    update_judge(isUpdate) {
      if (isUpdate == '0') {
        return '可编辑'
      } else if (isUpdate == '0') {
        return '不可编辑'
      }
    },
  }
}
