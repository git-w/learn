/**
 * @description: 系统管理-系统管理员
 * @author: leroy
 * @Compile：2021-05-17 10：00
 * @update: lx(2021-05-17 10：00)
 */
import {
  account_save_path, // 添加
  account_delete_path, // 删除
  account_update_path, // 更新
  account_info_path, // 详情
  account_list_path, // 列表
  role_find_all_path, // 角色管理 全部
  organizatioon_tree_path // 部门管理 全部
} from '../../../api/system'

import { date_format } from '../../../utils/date/dateFormat' //时间戳转化成标准时间
import { get_img_url_to_path_list } from '@/utils/img/imageMethod.js'

import {
  validateLogin,  // 账号
  validatePassword, // 密码
  validateLetter, // 数字和字母
  validatemoblie, // 手机号或电话号
  validateEmail, // 邮箱
} from '../../../utils/verify'  // 表单校验规则

export default {
  data() {
    return {
      ids: '',

      date_format, //时间处理器方法 (年-月-日-时-分-秒)

      // 部门 动态加载级联选择的配置
      props: {
        checkStrictly: true,
        value: 'id',
        label: 'organizationName'
      },

      // 系统管理员 检索字段
      searchForm: {
        loginName: '', //类型：String  可有字段  备注：用户账号 支持模糊搜索
        accountName: '', //类型：String  可有字段  备注：用户名 支持模糊搜索
        phone: '' //类型：String  可有字段  备注：手机号
      },
      tableData: [], // 系统管理员 列表数组
      
      authority: [], // 角色下拉列表数组
      organizatioonList: [], // 部门下拉选择数组

      drawer: false, // 控制抽屜的开合
      title: '', // 控制抽屜的头部
      loginNameDisabled: false, // 表单账号禁用启用

      // 系统管理员 表单字段
      addForm: {
        id: '', // 类型：String  必有字段  备注：编号
        loginName: '', // 类型：String  必有字段  备注：用户登录名
        accountName: '', // 类型：String  必有字段  备注： 用户真实姓名
        loginPassword: '', // 类型：String  必有字段  备注：用户密码
        phone: [], // 类型：String  必有字段  备注：用户手机号
        roleID: '', // 类型：String  必有字段  备注：管理员角色id
        accountCode: '', // 类型：String  可有字段  备注：员工编号
        email: '', // 类型：String  可有字段  备注：邮箱
        sex: '', //类型：String  可有字段  备注：性别 0:未知 1:男 2:女
        memo: '', //类型：String  可有字段  备注：备注
        isStatus: '', //类型：String  可有字段  备注：账号状态 0:正常 1:冻结
        photo: '', //类型：String  可有字段  备注：头像路径
        remark: '', //类型：String  可有字段  备注：后台管理员对粉丝的备注
        departmentID: [] //类型：String  必有字段  备注：部门id
      },

      // 系统管理员 表单验证
      rules: {
        //头像
        imgPath: [
          {
            // validator: isImgPath
          }
        ],
        //手机号
        phone: [
          {
            required: true,
            message: '请输入手机号',
            trigger: 'blur'
          },
          {
            validator: validatemoblie
          }
        ],
        //员工编号
        accountCode: [
          {
            required: true,
            message: '请输入员工编号',
            trigger: 'blur'
          },
          {
            validator: validateLetter
          }
        ],
        //邮箱
        email: [
          {
            required: true,
            message: '请输入邮箱',
            trigger: 'blur'
          },
          {
            validator: validateEmail
          }
        ],
        //账号
        loginName: [
          {
            required: true,
            message: '请输入账号',
            trigger: 'blur'
          },
          {
            validator: validateLogin
          }
        ],
        //密码
        loginPassword: [
          {
            required: true,
            message: '请输入密码',
            trigger: 'blur'
          },
          {
            validator: validatePassword
          }
        ],
        //真实姓名
        accountName: [
          {
            required: true,
            message: '请输入真实姓名',
            trigger: 'blur'
          }
        ],
        //部门
        departmentID: [
          {
            required: true,
            message: '请选择部门',
            trigger: 'change'
          }
        ],
        //角色
        roleID: [
          {
            required: true,
            message: '请选择角色',
            trigger: 'change'
          }
        ]
      }
    }
  },
  mounted() {
    this.to_load() // 进入此页面进行请求
  },
  methods: {
    /**
     * @memo  进入此页面进行加载
     */
    to_load() {
      this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize) // 请求列表数据
    },

    /**
     * @memo  检索重置按钮
     */
    reset_btn() {
      this.searchForm.loginName = '' //类型：String  可有字段  备注：用户账号 支持模糊搜索
      this.searchForm.accountName = '' //类型：String  可有字段  备注：用户名 支持模糊搜索
      this.searchForm.phone = '' //类型：String  可有字段  备注：手机号
      this.get_tabel_data(1, this.searchForm.pageSize) // 请求列表数据
    },

    /**
     * @memo 获取列表
     */
    async get_tabel_data(pageNum, pageSize) {
      const param = {
        pageNum: pageNum, // 类型：Number  必有字段  备注：页码 默认第一页
        pageSize: pageSize, // 类型：Number  必有字段  备注：条码 每页默认十条
        loginName: this.searchForm.loginName, // 类型：String  可有字段  备注：用户账号 支持模糊搜索
        accountName: this.searchForm.accountName, // 类型：String  可有字段  备注：用户名 支持模糊搜索
        phone: this.searchForm.phone // 类型：String  可有字段  备注：手机号
      }
      await account_list_path(param)
        .then(res => {
          this.tableData = res.data.resultList //列表的数据
          this.searchForm.totals = res.data.totalRows //总长度
          this.searchForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchForm.pageNum > 1) {
            if (res.data.resultList.length === 0) {
              this.get_tabel_data(
                this.searchForm.pageNum - 1,
                this.searchForm.pageSize
              )
            }
          } else if (this.searchForm.pageNum === 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => { })
    },

    /**
     * @memo 下拉框选中的按钮的操作  更多操作
     */
    handle_command(command) {
      if (command.button === 'delete_btn') {
        //删除
        this.delete_btn(command.row)
      } else if (command.button === 'change_btn') {
        //修改
        this.change_btn(command.row)
      } else if (command.button === 'start_btn') {
        //启用
        this.update_state_data(command.row, '0')
      } else if (command.button === 'end_btn') {
        //停用
        this.update_state_data(command.row, '1')
      }
    },
    /**
     * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
     */
    compose_value(item, row) {
      return {
        button: item,
        row: row
      }
    },

    /************************ 获取角色、部门 操作 ************************* */
    /**
     * @memo  获取角色下拉列表
     */
    async get_authority_data(id) {
      await role_find_all_path({ departmentID: id })
        .then(res => {
          this.authority = res.data
        })
        .catch(() => { })
    },

    /**
     * @memo  选择部门后调用角色列表
     */
    change_department_btn(departmentID) {
      let id = departmentID[departmentID.length - 1]
      this.$set(this.addForm, 'roleID', '')
      this.get_authority_data(id) //请求角色下拉列表
    },

    /**
     * @memo  级联选择器的下拉加载配置方法
     */
    // async lazy_load_data(node, resolve) {
    //   if (node.level != '0') {
    //     await organizatioon_tree_path({ parentID: node.value }).then(res => {
    //       const nodes = res.data
    //       // 通过调用resolve将子节点数据返回，通知组件数据加载完成
    //       resolve(nodes)
    //     })
    //   }
    // },

    /**
     * @memo 部门下拉选择的数据
     * @memo 调用findAll的接口，返回一个数组，用于下拉列表
     * @memo organizatioonList 下拉数组
     */
    async get_organizatioon_list_data() {
      const { data: res } = await organizatioon_tree_path({ parentID: '' })
      this.organizatioonList = res
    },


    /************************ 添加、修改 操作 ******************************** */
    /**
     * @memo  添加系统管理员-弹出添加抽屉
     */
    add_btn() {
      this.drawer = true
      this.title = '添加系统管理员'
      this.departmentList = [] // 清除部门数组重新请求赋值
      this.addForm = {
        loginName: '', // 类型：String  必有字段  备注：用户登录名
        accountName: '', // 类型：String  必有字段  备注： 用户真实姓名
        loginPassword: '', // 类型：String  必有字段  备注：用户密码
        phone: '', // 类型：String  必有字段  备注：用户手机号
        photo: [], // 类型：String  必有字段  备注：图片短路径
        roleID: '', // 类型：String  必有字段  备注：管理员角色id
        accountCode: '', // 类型：String  可有字段  备注：员工编号
        email: '', // 类型：String  可有字段  备注：邮箱
        address: '', // 类型：String  可有字段  备注：住址
        isStatus: '', // 类型：Number  必有字段  备注：账号状态 0:正常 1:冻结
        remark: '', // 类型：Number  必有字段  备注：后台管理员对粉丝的备注
        departmentID: [], // 类型：Number  必有字段  备注：部门的id数组
        sex: '', //  类型：Number  必有字段  备注：性别
        memo: '' //  类型：Number  必有字段  备注：备注
      }
      this.get_organizatioon_list_data() // 请求部门下拉列表
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /**
     * @memo 详情 接口
     */
    async get_info_data(id) {
      const res = await account_info_path({
        id: id
      })
      return res
    },

    /**
     * @memo 修改 按钮
     */
    change_btn(id) {
      if (id) {
        this.get_info_data(id)
          .then(res => {
            this.addForm = {
              id: res.data.id,
              loginName: res.data.loginName, // 类型：String  必有字段  备注：用户登录名
              accountName: res.data.accountName, // 类型：String  必有字段  备注： 用户真实姓名
              loginPassword: res.data.loginPassword, // 类型：String  必有字段  备注：用户密码
              phone: res.data.phone, // 类型：String  必有字段  备注：用户手机号
              photo: get_img_url_to_path_list(
                res.data.photo,
                res.data.photoPath
              ), // 类型：String  必有字段  备注：管理员头像
              roleID: res.data.roleID, // 类型：String  必有字段  备注：管理员角色id
              accountCode: res.data.accountCode, // 类型：String  可有字段  备注：员工编号
              email: res.data.email, // 类型：String  可有字段  备注：邮箱
              address: res.data.address, // 类型：String  可有字段  备注：住址
              isStatus: res.data.isStatus, //  类型：Number  可有字段  备注：账号状态 0:正常 1:冻结
              remark: res.data.remark, // 后台管理员对粉丝的备注
              departmentID: res.data.departmentID
                ? res.data.departmentID.split(',')
                : [], // 部门的id数组
              departmentName: res.data.departmentName, // 部门的名称
              sex: res.data.sex // 性别
            }
            this.get_authority_data(res.data.departmentID) // 请求角色下拉列表
            this.loginNameDisabled = true // 禁用账户输入框
            this.drawer = true
            this.title = '修改系统管理员'
          })
          .catch(() => {
            this.$message.error('查询失败')
          })
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /**
     * @memo 保存修改方法
     * 根据有无id来判断调用的接口是新增还是修改
     */
    async save_update_data() {
      // 部门
      let departmentIDArry = this.addForm.departmentID
      // 管理员头像
      let photoPath =
        this.addForm.photo.length > 0 ? this.addForm.photo[0].imgUrl : ''
      let param = {
        id: this.addForm.id ? this.addForm.id : '', //类型：String  必有字段  备注：编号
        loginName: this.addForm.loginName, // 类型：String  必有字段  备注：用户登录名
        accountName: this.addForm.accountName, // 类型：String  必有字段  备注： 用户真实姓名
        loginPassword: this.addForm.loginPassword, // 类型：String  必有字段  备注：用户密码
        phone: this.addForm.phone, // 类型：String  必有字段  备注：用户手机号
        photo: photoPath, // 类型：String  可有字段  备注：头像路径
        roleID: this.addForm.roleID, // 类型：String  必有字段  备注：管理员角色id
        accountCode: this.addForm.accountCode, // 类型：String  可有字段  备注：员工编号
        email: this.addForm.email, // 类型：String  可有字段  备注：邮箱
        address: this.addForm.address, // 类型：String  可有字段  备注：住址
        isStatus: this.addForm.isStatus, // 类型：String  可有字段  备注：账号状态 0:正常 1:冻结
        remark: this.addForm.remark, //类型：String  可有字段  备注：后台管理员对粉丝的备注
        departmentID: departmentIDArry[departmentIDArry.length - 1], //类型：String  必有字段  备注：部门id
        sex: this.addForm.sex, //类型：String  可有字段  备注：性别 0:未知 1:男 2:女
        memo: this.addForm.memo //类型：String  可有字段  备注：备注
      }
      if (!this.addForm.id) {
        await account_save_path(param)
          .then(res => {
            // 添加成功
            this.$message.success(res.message)
            // 添加成功返回列表页
            this.clear_drawer()
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => { })
      } else if (this.addForm.id) {
        await account_update_path(param)
          .then(res => {
            // 修改成功
            this.$message.success(res.message)
            // 修改成功返回列表页
            this.clear_drawer()
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => { })
      }
    },

    /**
     * @memo  提交按钮
     */
    submit_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_update_data()
        } else {
          // 提交失败
          return false
        }
      })
    },

    /***************************** 删除 操作 ********************************** */
    /**
     * @memo  删除接口
     */
    async delete_data(id) {
      const res = await account_delete_path({
        ids: id
      })
      return res
    },

    /**
     * @memo 删除按钮
     */
    delete_btn(id) {
      if (id) {
        this.$confirm('是否确定删除?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        })
          .then(() => {
            this.delete_data(id)
              .then(res => {
                this.$message.success(res.message)
                this.get_tabel_data(
                  this.searchForm.pageNum,
                  this.searchForm.pageSize
                )
              })
              .catch(() => { })
          })
          .catch(() => { })
      } else {
        this.$message.error('请选择需要删除的内容')
      }
    },

    /************************ 关闭抽屉 操作 *********************************** */
    /**
     * @memo 抽屉关闭前的回调
     */
    handle_close() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => { })
    },

    /**
     * @memo 点击取消按钮，关闭弹窗
     */
    close_drawer() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => { })
    },


    /**
     * @memo 关闭抽屉
     */
    clear_drawer() {
      this.drawer = false
      this.loginNameDisabled = false 
      this.departmentList = [] // 清空部门下拉框数组数据
      this.addForm = {
        loginName: '', // 类型：String  必有字段  备注：用户登录名
        accountName: '', // 类型：String  必有字段  备注： 用户真实姓名
        loginPassword: '', // 类型：String  必有字段  备注：用户密码
        phone: '', // 类型：String  必有字段  备注：用户手机号
        roleID: '', // 类型：String  必有字段  备注：管理员角色id
        accountCode: '', // 类型：String  可有字段  备注：员工编号
        email: '', // 类型：String  可有字段  备注：邮箱
        sex: '', //类型：String  可有字段  备注：性别 0:未知 1:男 2:女
        memo: '', //类型：String  可有字段  备注：备注
        isStatus: '', //类型：String  可有字段  备注：账号状态 0:正常 1:冻结
        photo: [], //类型：String  可有字段  备注：头像路径
        remark: '', //类型：String  可有字段  备注：后台管理员对粉丝的备注
        departmentID: [] //类型：String  必有字段  备注：部门id
      }
      // 关闭表单校验规则
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /*********************** 列表judge 操作 *************************** */
    /**
     * @memo 用户状态 judge
     */
    account_status_judge(accountStatus) {
      if (accountStatus === 0) {
        return '正常/启用'
      } else if (accountStatus === 1) {
        return '冻结/停用'
      }
    },

    /**
     * @memo  管理员类型 judge
     */
    account_type_judge(accountType) {
      if (accountType === 0) {
        return '超级管理员'
      } else if (accountType === 1) {
        return '普通管理员'
      }
    },

    /**
     * @memo  性别 judge
     */
    sex_judge(sex) {
      if (sex === 0) {
        return '未知'
      } else if (sex === 1) {
        return '男'
      } else if (sex === 2) {
        return '女'
      }
    },
  }
}
