/**
 * @description: 日常运营-banner管理
 * @author: leroy
 * @Compile：2021-11-26 11：00
 * @update:lx(2021-11-26 11：00)
 */

import {
    banner_save_path,  // 添加
    banner_update_path,  // 修改
    banner_delete_path,  // 删除
    banner_list_path,  // 列表
    banner_info_path,  // 详情
    banner_update_state_path,  // 批量修改状态
} from '../../../api/operation'

import {
    user_list_path,  // 列表
} from '../../../api/user'

import {
    card_list_path, // 列表
} from '../../../api/card'

import {
    activity_list_path,  // 列表
    activity_category_all_path,  // 分类全部
} from '../../../api/activity'

import { date_format, date_format_ymd, format_date, format_date_ymd } from '../../../utils/date/dateFormat'
import { get_img_url_to_path_list } from '@/utils/img/imageMethod.js'
export default {
    data() {
        // 上传图片验证
        let isImgPath = (rule, value, callback) => {
            //判断banner图是否有值
            if (this.addForm.imgPath) {
                callback()
            } else {
                //banner图没有值,提示请上传图片
                return callback(new Error('请上传图片'))
            }
        }
        // 添加类型验证
        let isBusinessId = (rule, value, callback) => {
            //判断跳转类型是否选择
            if (this.addForm.businessId) {
                callback()
            } else {
                //未选择提示请选择跳转内容或者输入跳转url
                return callback(new Error('请选择跳转内容或者输入跳转url！'))
            }
        }
        return {
            ids: '',

            date_format, // 时间处理器方法 (年-月-日-时-分-秒)
            date_format_ymd, // 时间处理器方法 (年-月-日)
            format_date,
            format_date_ymd,

            //banner图 检索字段
            searchForm: {
                name: '', //搜索名称
                putSite: '', //类型：Number  可有字段  备注：投放位置 0首页
                isState: ''
            },
            tableData: [], //列表的数据

            // 用户列表 检索字段
            searchUserForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0
            },
            tableUserList: [],  // 用户列表 数组数据

            // 会员卡列表 检索字段
            searchCardForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0
            },
            tableCardList: [],  // 会员卡列表 数组数据

            // 活动列表 检索字段
            searchActivityForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0
            },
            tableActivityList: [],  // 活动列表 数组数据
            categoryList: [], // 活动分类下拉框数组

            // 领取会员卡列表 检索字段
            searchFreeCardForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0
            },
            tableFreeCardList: [],  // 领取会员卡列表 数组数据

            drawer: false, // 控制抽屜的开合
            title: '', // 控制抽屜的头部
            add: 1,

            pickerOptions: {
                disabledDate(time) {
                    return time.getTime() < Date.now() - 8.64e7 //如果没有后面的-8.64e7就是不可以选择今天的
                }
            }, // 日期选择器只能选择今天及今天以后的时间

            updateTimeDialog: false, // 控制修改时间弹窗的显示隐藏
            updateTimeForm: {
                timeQuantum: []
            },

            putSiteList: [
                {
                    value: 0,
                    label: '首页'
                }
            ], // 投放位置

            typeClassList: [
                {
                    value: 0,
                    label: '空跳转'
                },
                {
                    value: 1,
                    label: '跳转URL'
                },
                {
                    value: 2,
                    label: '客户列表'
                },
                {
                    value: 3,
                    label: '会员卡列表'
                },
                {
                    value: 4,
                    label: '视频列表'
                },
                {
                    value: 5,
                    label: '客户详情'
                }, {
                    value: 6,
                    label: '会员卡详情'
                }, {
                    value: 7,
                    label: '活动列表'
                }, {
                    value: 8,
                    label: '活动详情'
                }, {
                    value: 9,
                    label: '领取会员卡'
                }
            ], // 类型

            stateList: [
                {
                    value: '0',
                    label: '启用 '
                },
                {
                    value: '1',
                    label: '停用 '
                }
            ], // 状态

            // banner图 表单字段
            addForm: {
                id: '',
                bannerName: '', // 类型：String  必有字段  备注：轮播名称
                imgPath: [], // 类型：String  必有字段  备注：图片存放路径短
                timeQuantum: [], //时间选择器
                startTime: '', //类型：Number  必有字段  备注：开始显示时间
                endTime: '', //类型：Number  必有字段  备注：结束显示时间
                sort: '', // 类型：Number  可有字段  备注：排序 0是最高 默认0
                isState: 0, // 类型：Number  可有字段  备注：是否上线 0是 1否 默认上线
                businessId: '', // 类型：String  可有字段  备注：跳转类型的id 或者图片跳转url
                businessName: '', // 类型：String  可有字段  备注：跳转类型名称
                typeClass: '', //跳转 的类型
                type: '', // 类型：Number  必有字段  备注：type：0：不跳转 1:跳转URL :2跳转内容  3:攻略详情 4商品主页 5商品详情
                putSite: '', //类型：Number  必有字段  备注：投放位置 0首页
                createTime: '', //创建时间
                createOperatorName: '', // 创建人
                updateTime: '', //更新时间
                updateOperatorName: '', // 操作人
                isTime: ''
            },

            // banner图 表单校验
            rules: {
                // banner图片
                imgPath: [
                    {
                        required: true,
                        validator: isImgPath
                    }
                ],
                // 跳转内容的ID或跳转的url
                businessId: [
                    {
                        required: true,
                        validator: isBusinessId
                    }
                ],
                // banner图名称
                bannerName: [
                    {
                        required: true,
                        message: '请输入banner图名称',
                        trigger: 'blur'
                    }
                ],
                // 类型
                typeClass: [
                    {
                        required: true,
                        message: '请选择类型',
                        trigger: 'change'
                    }
                ],
                // 跳转内容
                type: [
                    {
                        required: true,
                        message: '请选择跳转内容',
                        trigger: 'change'
                    }
                ],
                // 排序
                sort: [
                    {
                        required: false,
                        message: '请输入排序',
                        trigger: 'blur'
                    },
                    {
                        pattern: /^[0-9]+[0-9]*$/,
                        message: '请输入正整数'
                    }
                ],
                // 显示时间
                timeQuantum: [
                    {
                        required: true,
                        message: '请选择显示时间',
                        trigger: 'blur'
                    }
                ],
                // 投放位置
                putSite: [
                    {
                        required: true,
                        message: '请选择投放位置',
                        trigger: 'change'
                    }
                ]
            },
        }
    },
    mounted() {
        this.to_load() // 进入此页面进行请求
    },
    methods: {
        /**
         * @memo 进入此页面进行加载
         */
        to_load() {
            this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize) // 请求列表数据
        },

        /**
         * @memo 检索重置按钮
         */
        reset_btn() {
            this.searchForm.name = ''
            this.searchForm.putSite = ''
            this.searchForm.isState = ''
            this.get_tabel_data(1, this.searchForm.pageSize)
        },

        /**
         * @memo 获取列表
         */
        async get_tabel_data(pageNum, pageSize) {
            let param = {
                bannerName: this.searchForm.name, //类型：Number  可有字段  备注：banner名称
                putSite: this.searchForm.putSite, //类型：Number  可有字段  备注：投放位置 0首页
                pageNum: pageNum, //类型：Number  可有字段  备注：当前页码
                pageSize: pageSize, //类型：Number  可有字段  备注：每页的条数
                isState: this.searchForm.isState //类型：Number  可有字段  备注：状态
            }
            await banner_list_path(param)
                .then(res => {
                    this.tableData = res.data.resultList //列表的数据
                    this.searchForm.totals = res.data.totalRows //总长度
                    this.searchForm.pageSize = res.data.pageSize // 当前页码条数                    this.searchForm.pageNum = res.data.pageNum // 请求页码
                    this.searchForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchForm.pageNum > 1) {
                        if (res.data.resultList.length == 0) {
                            this.get_tabel_data(
                                this.searchForm.pageNum - 1,
                                this.searchForm.pageSize
                            )
                        }
                    } else if (this.searchForm.pageNum == 0) {
                        return this.$message.warning('没有更多数据')
                    }
                })
                .catch(() => { })
        },

        /**
         * @memo table的sort-change事件
         * @param 参数 column.order 有两个值：ascending 升序  descending 降序
         */
        change_table_sort(column) {
            //获取字段名称和排序类型
            // var fieldName = column.prop;   // 获取prop指定的字段
            var sortingType = column.order //  ascending 升序   descending 降序
            var sortable = column.column.sortable // 点击的是那一列
            if (sortingType == 'descending') {
                if (sortable == 'isState') {
                    this.searchForm.isState = '0'
                }
            }
            //按照升序排序
            else if (sortingType == 'ascending') {
                if (sortable == 'isState') {
                    this.searchForm.isState = '1'
                }
            }
            //无序
            else if (!sortingType) {
                if (sortable == 'isState') {
                    this.searchForm.isState = ''
                }
            }
            // 请求列表数据
            this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
        },

        /**
         * @memo 下拉框选中的按钮的操作  更多操作
         */
        handle_command(command) {
            if (command.button == 'delete_btn') {
                //删除
                this.delete_btn(command.row)
            } else if (command == 'delete_btn') {
                //多选 删除
                this.delete_btn(this.ids)
            } else if (command == 'more_disable') {
                //多选 停用
                this.update_state_data(this.ids, 1)
            } else if (command == 'more_start_using') {
                //多选 启用
                this.update_state_data(this.ids, 0)
            } else if (command.button == 'disable_btn') {
                //停用
                this.update_state_data(command.row, 1)
            } else if (command.button == 'start_using_btn') {
                //启用
                this.update_state_data(command.row, 0)
            } else if (command.button == 'change_btn') {
                //修改
                this.change_btn(command.row)
            } else if (command.button == 'info_btn') {
                //详情
                this.info_btn(command.row)
            } else if (command == 'more_change_time') {
                //多选修改显示时间
                this.more_change_time()
            } else if (command.button == 'change_time_btn') {
                //单选修改显示时间
                this.change_time_btn(command.row)
            }
        },
        /**
         * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
         */
        compose_value(item, row) {
            return {
                button: item,
                row: row
            }
        },

        /**
         * @memo 获取用户列表
         */
        async get_user_data(pageNum, pageSize) {
            const param = {
                pageNum: pageNum, //当前页码
                pageSize: pageSize, //每页的条数
            }
            await user_list_path(param)
                .then(res => {
                    this.tableUserList = res.data.resultList //列表的数据
                    this.searchUserForm.totals = res.data.totalRows //总长度
                    this.searchUserForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchUserForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchUserForm.pageNum > 1) {
                        if (res.data.resultList.length == 0) {
                            this.get_user_data(
                                this.searchUserForm.pageNum - 1,
                                this.searchUserForm.pageSize
                            )
                        }
                    } else if (this.searchUserForm.pageNum == 0) {
                        return this.$message.warning('没有更多数据')
                    }
                })
                .catch(() => { })
        },

        /**
         * @memo 获取会员卡列表
         */
        async get_card_data(pageNum, pageSize) {
            const param = {
                pageNum: pageNum, //当前页码
                pageSize: pageSize, //每页的条数
            }
            await card_list_path(param)
                .then(res => {
                    this.tableCardList = res.data.resultList //列表的数据
                    this.searchCardForm.totals = res.data.totalRows //总长度
                    this.searchCardForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchCardForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchCardForm.pageNum > 1) {
                        if (res.data.resultList.length == 0) {
                            this.get_card_data(
                                this.searchCardForm.pageNum - 1,
                                this.searchCardForm.pageSize
                            )
                        }
                    } else if (this.searchCardForm.pageNum == 0) {
                        return this.$message.warning('没有更多数据')
                    }
                })
                .catch(() => { })
        },

        /**
         * @memo 获取活动列表
         */
        async get_activity_data(pageNum, pageSize) {
            const param = {
                pageNum: pageNum, //当前页码
                pageSize: pageSize, //每页的条数
            }
            await activity_list_path(param)
                .then(res => {
                    this.tableActivityList = res.data.resultList //列表的数据
                    this.searchActivityForm.totals = res.data.totalRows //总长度
                    this.searchActivityForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchActivityForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchActivityForm.pageNum > 1) {
                        if (res.data.resultList.length == 0) {
                            this.get_activity_data(
                                this.searchActivityForm.pageNum - 1,
                                this.searchActivityForm.pageSize
                            )
                        }
                    } else if (this.searchActivityForm.pageNum == 0) {
                        return this.$message.warning('没有更多数据')
                    }
                })
                .catch(() => { })
        },

        /**
         *  请求分类的下拉列表的数据
         *  点击添加按钮时需要添加一次
         */
        async get_category_list_data() {
            let { data: res } = await activity_category_all_path({})
            this.categoryList = res
        },

        /**
         * @memo 获取会员卡列表
         */
         async get_free_card_data(pageNum, pageSize) {
            const param = {
                pageNum: pageNum, //当前页码
                pageSize: pageSize, //每页的条数
                isFree: 0,
            }
            await card_list_path(param)
                .then(res => {
                    this.tableFreeCardList = res.data.resultList //列表的数据
                    this.searchFreeCardForm.totals = res.data.totalRows //总长度
                    this.searchFreeCardForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchFreeCardForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchFreeCardForm.pageNum > 1) {
                        if (res.data.resultList.length == 0) {
                            this.get_free_card_data(
                                this.searchFreeCardForm.pageNum - 1,
                                this.searchFreeCardForm.pageSize
                            )
                        }
                    } else if (this.searchFreeCardForm.pageNum == 0) {
                        return this.$message.warning('没有更多数据')
                    }
                })
                .catch(() => { })
        },

        /**
         * @memo 添加-跳往添加界面
         */
        add_btn() {
            this.drawer = true
            this.add = 1
            this.title = '添加banner'
            this.addForm = {
                id: '',
                bannerName: '', // 类型：String  必有字段  备注：轮播名称
                imgPath: [], // 类型：String  必有字段  备注：图片存放路径短
                timeQuantum: [], //时间选择器
                startTime: '', //类型：Number  必有字段  备注：开始显示时间
                endTime: '', //类型：Number  必有字段  备注：结束显示时间
                sort: '', // 类型：Number  可有字段  备注：排序 0是最高 默认0
                isState: 0, // 类型：Number  可有字段  备注：是否上线 0是 1否 默认上线
                businessId: '', // 类型：String  可有字段  备注：跳转类型的id 或者图片跳转url
                businessName: '', // 类型：String  可有字段  备注：跳转类型名称
                typeClass: '', //跳转 的类型
                type: '', // 类型：Number  必有字段  备注：跳转类型：0：不跳转 1:跳转URL 2:跳转内容 3房产主页 4房产详情 5餐厅
                putSite: '', //类型：Number  必有字段  备注：投放位置 0首页
                createTime: '', //创建时间
                createOperatorName: '', // 创建人
                updateTime: '', //更新时间
                updateOperatorName: '', // 操作人
                isTime: 1
            }
            //关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /**
         * @memo  详情接口
         */
        async get_info_data(id) {
            const res = await banner_info_path({
                id: id
            })
            return res
        },

        /**
         * @memo 修改
         */
        change_btn(id) {
            if (id) {
                this.get_info_data(id)
                    .then(res => {
                        this.addForm = {
                            id: res.data.id, //类型：String  必有字段  备注：id
                            bannerName: res.data.bannerName, //类型：String  必有字段  备注：轮播名称
                            imgPath: get_img_url_to_path_list(
                                res.data.imgPath,
                                res.data.imgPathUrl
                            ), //类型：String  必有字段  备注：图片存放路径短
                            timeQuantum: [res.data.startTime, res.data.endTime], // 时间段选择器
                            sort: res.data.sort, // 类型：Number  可有字段  备注：排序 0是最高 默认0
                            isState: res.data.isState, // 类型：Number  可有字段  备注：是否上线 0是 1否 默认上线
                            businessId: res.data.businessId, // 类型：String  可有字段  备注：跳转类型的id 或者图片跳转url
                            businessName: res.data.businessName, // 类型：String  可有字段  备注：跳转类型名称
                            isTime: res.data.isTime,
                            typeClass: res.data.type,
                            type: res.data.type, // 类型：Number  必有字段  备注：跳转类型：0：不跳转 1:跳转URL 2:跳转内容  3:攻略详情 4商品主页 5商品详情
                            putSite: res.data.putSite //类型：Number  必有字段  备注：投放位置投放位置 0首页
                        }
                        if (res.data.type == 5) {
                            this.get_user_data(
                                this.searchUserForm.pageNum,
                                this.searchUserForm.pageSize
                            ) //加载用户列表
                        } else if (res.data.type == 6) {
                            this.get_card_data(
                                this.searchCardForm.pageNum,
                                this.searchCardForm.pageSize
                            ) //加载会员卡列表
                        } else if (res.data.type == 8) {
                            this.get_activity_data(
                                this.searchActivityForm.pageNum,
                                this.searchActivityForm.pageSize
                            ) //加载活动列表
                            this.get_category_list_data()
                        } else if (res.data.type == 9) {
                            this.get_free_card_data(
                                this.searchFreeCardForm.pageNum,
                                this.searchFreeCardForm.pageSize
                            )
                        }
                        this.drawer = true
                        this.$nextTick(() => { })
                        this.add = 1
                        this.title = '修改banner区'
                    })
                    .catch(() => { })
            } else {
                this.$message.error('数据错误请刷新页面')
            }
        },

        /**
         * @memo 详情按钮
         */
        info_btn(id) {
            if (id) {
                this.get_info_data(id)
                    .then(res => {
                        this.addForm = {
                            id: res.data.id, //类型：String  必有字段  备注：id
                            bannerName: res.data.bannerName, //类型：String  必有字段  备注：轮播名称
                            imgPath: res.data.imgPath, //类型：String  必有字段  备注：图片存放路径短
                            imgPathUrl: res.data.imgPathUrl,
                            isTime: res.data.isTime,
                            startTime: res.data.startTime, //类型：Number  必有字段  备注：开始显示时间
                            endTime: res.data.endTime, //类型：Number  必有字段  备注：结束显示时间
                            sort: res.data.sort, // 类型：Number  可有字段  备注：排序 0是最高 默认0
                            isState: res.data.isState, // 类型：Number  可有字段  备注：是否上线 0是 1否 默认上线
                            businessId: res.data.businessId, // 类型：String  可有字段  备注：跳转类型的id 或者图片跳转url
                            businessName: res.data.businessName, // 类型：String  可有字段  备注：跳转类型名称
                            // typeClass: "", //跳转 的类型
                            type: res.data.type, // 类型：Number  必有字段  备注：跳转类型：0：不跳转 1:跳转URL 2:跳转内容
                            putSite: res.data.putSite, //类型：Number  必有字段  备注：投放位置 0首页
                            createTime: res.data.createTime, // 创建时间
                            createOperatorName: res.data.createOperatorName, // 创建人
                            updateTime: res.data.updateTime, //更新时间
                            updateOperatorName: res.data.updateOperatorName // 操作人
                        }

                        this.drawer = true
                        this.add = 2
                        this.title = 'Banner图详情'
                    })
                    .catch(() => { })
            } else {
                this.$message.error('数据错误请刷新页面')
            }
        },

        /**
         * @memo 保存修改方法
         * 根据有无id来判断调用的接口是新增还是修改
         */
        async save_update_data() {
            // banner图
            let imgPath =
                this.addForm.imgPath.length > 0 ? this.addForm.imgPath[0].imgUrl : ''
            let param = {
                id: this.addForm.id ? this.addForm.id : '',
                bannerName: this.addForm.bannerName, //类型：String  必有字段  备注：轮播名称
                imgPath: imgPath, //类型：String  必有字段  备注：图片存放路径短
                startTime: this.addForm.timeQuantum[0]
                    ? this.addForm.timeQuantum[0]
                    : '', //类型：Number  必有字段  备注：开始显示时间
                endTime: this.addForm.timeQuantum[1] ? this.addForm.timeQuantum[1] : '', //类型：Number  必有字段  备注：结束显示时间
                sort: this.addForm.sort, // 类型：Number  可有字段  备注：排序 0是最高 默认0
                isState: this.addForm.isState, // 类型：Number  可有字段  备注：是否上线 0是 1否 默认上线
                businessId: this.addForm.businessId, // 类型：String  可有字段  备注：跳转类型的id 或者图片跳转url
                businessName: this.addForm.businessName, // 类型：String  可有字段  备注：跳转类型名称
                // typeClass: "", //跳转 的类型
                type: this.addForm.type, // 类型：Number  必有字段  跳转类型：0：不跳转 1:跳转URL 2:跳转内容 3 攻略  4 商品
                putSite: this.addForm.putSite, //类型：Number  必有字段  备注：投放位置 0首页 1地产 2外卖 3物业 4酒店 5商城
                isTime: this.addForm.isTime
            }
            if (!this.addForm.id) {
                await banner_save_path(param)
                    .then(res => {
                        // 添加成功
                        this.$message.success(res.message)
                        // 添加成功返回列表页
                        this.clear_drawer()
                        this.get_tabel_data(1, this.searchForm.pageSize)
                    })
                    .catch(() => { })
            } else if (this.addForm.id) {
                await banner_update_path(param)
                    .then(res => {
                        // 修改成功
                        this.$message.success(res.message)
                        // 修改成功返回列表页
                        this.clear_drawer()
                        this.get_tabel_data(
                            this.searchForm.pageNum,
                            this.searchForm.pageSize
                        )
                    })
                    .catch(() => { })
            }
        },

        /**
         * @memo 提交按钮
         */
        submit_btn(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    this.save_update_data()
                } else {
                    // 提交失败
                    return false
                }
            })
        },


        /*************************** 删除 操作 ********************************** */
        /**
         * @memo 删除接口
         */
        async delete_data(id) {
            const res = await banner_delete_path({
                ids: id
            })
            return res
        },

        /**
         * @memo 单选删除按钮
         */
        delete_btn(id) {
            if (id) {
                this.$confirm('是否确定删除?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                })
                    .then(() => {
                        this.delete_data(id)
                            .then(res => {
                                this.$message.success(res.message)
                                this.get_tabel_data(
                                    this.searchForm.pageNum,
                                    this.searchForm.pageSize
                                )
                            })
                            .catch(() => { })
                    })
                    .catch(() => { })
            } else {
                this.$message.error('请选择需要删除的内容!')
            }
        },


        /*************************** 修改状态 操作 ********************************** */
        /**
         * @memo  修改多条状态接口
         */
        async update_state_data(id, isState) {
            if (id) {
                const param = {
                    ids: id,
                    isState: isState
                }
                await banner_update_state_path(param)
                    .then(res => {
                        this.$message({
                            message: res.message,
                            type: 'success'
                        })
                        this.get_tabel_data(
                            this.searchForm.pageNum,
                            this.searchForm.pageSize
                        )
                    })
                    .catch(() => { })
            } else {
                this.$message.error('请选择需要修改状态的内容!')
            }
        },

        /************************** 修改显示时间 ****************/
        /**
         * @memo 关闭更改考显示时间弹窗
         */
        reset_updateTime(formName) {
            this.$refs[formName].resetFields()
            this.updateTimeDialog = false
        },

        /**
         * @memo 修改显示时间的接口
         */
        // async change_time_data() {
        //     const res = await banner_update_multi_time_path({
        //         ids: this.updateTimeForm.id,
        //         startTime: this.updateTimeForm.timeQuantum[0],
        //         endTime: this.updateTimeForm.timeQuantum[1]
        //     })
        //     return res
        // },

        /**
         * @memo  提交修改时间
         */
        submit_updateTime(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    this.change_time_data()
                        .then(res => {
                            // 修改成功
                            this.$message.success(res.message)
                            // 清空数据表单
                            this.clear_dialog_btn()
                            // 重新获取列表，更新数据列表
                            this.get_tabel_data(
                                this.searchForm.pageNum,
                                this.searchForm.pageSize
                            )
                        })
                        .catch(() => { })
                } else {
                    return false
                }
            })
        },

        /**
         * @memo 清除弹窗数据
         * 并且关闭弹窗
         */
        clear_dialog() {
            this.updateTimeForm = {
                id: '',
                timeQuantum: []
            } // 清空数据表单
            this.updateTimeDialog = false // 关闭弹窗
        },

        /**
         * @memo 弹窗关闭前的回调
         */
        handle_close_dialog() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_dialog()
                })
                .catch(() => { })
        },

        /**
         * @memo 多选修改显示时间
         */
        more_change_time() {
            if (this.ids) {
                this.updateTimeDialog = true
                this.updateTimeForm = {
                    id: this.ids,
                    timeQuantum: []
                }
                this.$nextTick(() => {
                    this.$refs.updateTimeForm.clearValidate()
                })
            } else {
                this.$message.error('请先选择需要修改的内容！')
            }
        },

        /**
         * @memo 单选修改显示时间
         * 需先请求当前数据的显示时间范围
         * 并且复现
         */
        change_time_btn(id) {
            this.get_info_data(id)
                .then(res => {
                    this.updateTimeForm = {
                        id: res.data.id,
                        timeQuantum:
                            res.data.isTime == 1 ? [] : [res.data.startTime, res.data.endTime]
                    }
                    this.updateTimeDialog = true // 将时间弹窗显示
                    this.$nextTick(() => {
                        this.$refs.updateTimeForm.clearValidate()
                    })
                })
                .catch(() => { })
        },


        /************************* 关闭抽屉 操作 ******************************** */
        /**
         * @memo 抽屉关闭前的回调
         */
        handle_close() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },
        /**
         * @memo 点击取消按钮，关闭抽屉
         */
        close_drawer() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },
        /**
         * @memo 关闭抽屉
         */
        clear_drawer() {
            this.drawer = false
            this.addForm = {
                id: '',
                bannerName: '', // 类型：String  必有字段  备注：轮播名称
                imgPath: [], // 类型：String  必有字段  备注：图片存放路径短
                timeQuantum: [], //时间选择器
                startTime: '', //类型：Number  必有字段  备注：开始显示时间
                endTime: '', //类型：Number  必有字段  备注：结束显示时间
                sort: '', // 类型：Number  可有字段  备注：排序 0是最高 默认0
                isState: 0, // 类型：Number  可有字段  备注：是否上线 0是 1否 默认上线
                businessId: '', // 类型：String  可有字段  备注：跳转类型的id 或者图片跳转url
                businessName: '', // 类型：String  可有字段  备注：跳转类型名称
                typeClass: '', //跳转 的类型
                type: '', // 类型：Number  必有字段  备注：跳转类型：0：不跳转 1:跳转URL 2:跳转内容 3:攻略;5:商品;6:餐厅;
                putSite: '', //类型：Number  必有字段  备注：投放位置 0首页
                createTime: '', //创建时间
                createOperatorName: '', // 创建人
                updateTime: '', //更新时间
                updateOperatorName: '' // 操作人
            }
            this.updateTimeDialog = false
            //关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },


        /*********************** 列表judge 操作 *************************** */
        /**
         * @memo 是否上线  状态
         */
        isStateJudge(isState) {
            if (isState == 0) {
                return '启用'
            } else if (isState == 1) {
                return '停用'
            }
        },
        /**
         * @memo table内跳转目标
         * @param 跳转类型：0：不跳转,1-跳转URL,2-客户列表,3-会员卡列表;4-视频列表;5-客户详情,6-会员卡详情;7-活动列表,8-活动详情,
         */
        type_judge(type) {
            switch (type) {
                case 0:
                    return '不跳转'
                case 1:
                    return '跳转url'
                case 2:
                    return '客户列表'
                case 3:
                    return '会员卡列表'
                case 4:
                    return '视频列表'
                case 5:
                    return '客户详情'
                case 6:
                    return '会员卡详情'
                case 7:
                    return '活动列表'
                case 8:
                    return '活动详情'
                case 9:
                    return '领取会员卡'
            }
        },

        /**
         * @memo 投放位置
         * @param 投放位置 0首页
         */
        putSiteJudge(putSite) {
            if (putSite == '0') {
                return '首页'
            }
        },

        /***************************** 用户列表judge 操作 *********************************** */
        /**
         * @memo 性别
         */
        sexJudge(sex) {
            if (sex == 0) {
                return '未知'
            } else if (sex == 1) {
                return '男'
            } else if (sex == 2) {
                return '女'
            }
        },

        /**
         * @memo 会员等级
         */
        levelJudge(level) {
            if (level == 0) {
                return '普通用户'
            } else if (level == 1) {
                return 'VIP会员'
            }
        },

        /***************************** 会员卡列表judge 操作 *********************************** */
        /**
         * @memo 状态
         */
        isEnabledJudge(isEnabled) {
            if (isEnabled == 0) {
                return '启用'
            } else if (isEnabled == 1) {
                return '停用'
            }
        },

        /***************************** 活动列表judge 操作 *********************************** */
        /**
         * @memo 状态
         */
        stateJudge(isState) {
            if (isState == 0) {
                return '启用'
            } else if (isState == 1) {
                return '停用'
            }
        },

        /**
         * @memo 活动 状态
         */
        activityStateJudge(activityState) {
            if (activityState == 0) {
                return '报名中'
            } else if (activityState == 1) {
                return '进行中'
            } else if (activityState == 2) {
                return '已结束'
            } else if (activityState == 3) {
                return '未开始'
            }
        },

        /**
         * @memo 分类 judge
         */
        cateJudge(cateId) {
            let res = this.categoryList.filter(item => {
                return item.id == cateId
            })
            return res.length > 0 ? res[0].cateName : '无数据'
        },


        /********************************** 切换操作 *********************************** */
        /**
         * @memo 选择类型
         */
        typeClassChange(val) {
            this.addForm.businessId = ''
            // 判断如果选择的类型是0不跳转 or 1跳转url , 则直接给type赋值
            if (val == '0') {
                this.addForm.type = 0
            } else if (val == '1') {
                this.addForm.type = 1
            } else if (val == '2') {
                this.addForm.type = 2
            } else if (val == '3') {
                this.addForm.type = 3
            } else if (val == '4') {
                this.addForm.type = 4
            } else if (val == '5') {
                this.addForm.type = 5
                this.searchUserForm = {
                    pageNum: 1,
                    pageSize: 10,
                    totals: 0
                }
                this.get_user_data(
                    this.searchUserForm.pageNum,
                    this.searchUserForm.pageSize
                )
            } else if (val == '6') {
                this.addForm.type = 6
                this.searchCardForm = {
                    pageNum: 1,
                    pageSize: 10,
                    totals: 0
                }
                this.get_card_data(
                    this.searchCardForm.pageNum,
                    this.searchCardForm.pageSize
                )
            } else if (val == '7') {
                this.addForm.type = 7
            } else if (val == '8') {
                this.addForm.type = 8
                this.searchActivityForm = {
                    pageNum: 1,
                    pageSize: 10,
                    totals: 0
                }
                this.get_activity_data(
                    this.searchActivityForm.pageNum,
                    this.searchActivityForm.pageSize
                )
                this.get_category_list_data()
            } else if (val == '9') {
                this.addForm.type = 9
                this.searchFreeCardForm = {
                    pageNum: 1,
                    pageSize: 10,
                    totals: 0
                }
                this.get_free_card_data(
                    this.searchFreeCardForm.pageNum,
                    this.searchFreeCardForm.pageSize
                )
            }
        },

        /**
         * @memo 切换投放位置
         * @param 投放位置 0首页
         */
        select_putSite(e) {
            this.addForm.type = ''
            this.addForm.typeClass = ''
            if (e == '0') {
                // 首页
                this.typeClassList = [
                    {
                        value: 0,
                        label: '空跳转'
                    },
                    {
                        value: 1,
                        label: '跳转URL'
                    },
                    {
                        value: 2,
                        label: '客户列表'
                    },
                    {
                        value: 3,
                        label: '会员卡列表'
                    },
                    {
                        value: 4,
                        label: '视频列表'
                    },
                    {
                        value: 5,
                        label: '客户详情'
                    }, {
                        value: 6,
                        label: '会员卡详情'
                    }, {
                        value: 7,
                        label: '活动列表'
                    }, {
                        value: 8,
                        label: '活动详情'
                    }, {
                        value: 9,
                        label: '领取会员卡'
                    }
                ]
            }
        },

        /**
         * @memo 客户分页 相关方法
         */
        change_user_size_btn(val) {
            // 分页加载常见问题列表加载
            this.get_user_data(this.searchUserForm.pageNum, val)
        },
        change_user_current_btn(val) {
            // 分页加载常见问题列表加载
            this.get_user_data(val, this.searchUserForm.pageSize)
        },

        /**
         * @memo 会员卡 相关方法
         */
        change_card_size_btn(val) {
            // 分页加载常见问题列表加载
            this.get_card_data(this.searchCardForm.pageNum, val)
        },
        change_card_current_btn(val) {
            // 分页加载常见问题列表加载
            this.get_card_data(val, this.searchCardForm.pageSize)
        },

        /**
         * @memo 活动 相关方法
         */
        change_activity_size_btn(val) {
            // 分页加载常见问题列表加载
            this.get_activity_data(this.searchActivityForm.pageNum, val)
        },
        change_activity_current_btn(val) {
            // 分页加载常见问题列表加载
            this.get_activity_data(val, this.searchActivityForm.pageSize)
        },

        /**
         * @memo 领取会员卡 相关方法
         */
        change_free_card_size_btn(val) {
            // 分页加载常见问题列表加载
            this.get_free_card_data(this.searchFreeCardForm.pageNum, val)
        },
        change_free_card_current_btn(val) {
            // 分页加载常见问题列表加载
            this.get_free_card_data(val, this.searchFreeCardForm.pageSize)
        },
    }
}
