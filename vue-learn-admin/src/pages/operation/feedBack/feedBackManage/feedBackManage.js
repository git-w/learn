/**
 * @description:日常运营-意见反馈-内容管理
 * @author: leroy
 * @Compile：2021-11-26 11：30
 * @update: lx(2021-11-26 11：30)
 */
import {
    feed_category_all_path,  // 分类全部
    feedback_list_path,  // 列表
    feedback_info_path,  // 详情
    feedback_hand_path,  // 处理意见
} from '../../../../api/operation'

import { date_format } from '../../../../utils/date/dateFormat'

export default {
    data() {
        return {
            date_format, // 时间处理器方法 (年-月-日-时-分-秒)

            // 意见反馈 检索字段
            searchForm: {
                nickname: '',   //类型：String  可有字段  备imgPathListUrl注：用户昵称
                cateId: '',   //类型：String  可有字段  备注：分类id
            },

            tableData: [],   //列表的数据

            drawer: false,  // 控制抽屜的开合
            title: '',  // 控制抽屜的头部
            add: '',

            // 意见反馈 表单字段
            addForm: {
                id: '',
                content: '',  //类型：String  必有字段  备注：反馈内容
                createTime: '',  //类型：Number  必有字段  备注：创建时间
                createOperatorID: '',  //类型：String  必有字段  备注：创建者ID（反馈者）
                satisfaction: '',   //类型：Number  必有字段  备注： 满意度 0非常满意 1满意 2一般 3不满意
                imgPathListUrl: '',  //类型：Mixed  必有字段  备注： 图片存放路径（长），多图用,隔开
                nickname: '',  //类型：String  必有字段  备注：用户的昵称
                modilePhone: '',   //类型：String  必有字段  备注：手机号
                headimgurl: '',  //类型：String  必有字段  备注：用户头像
                handingContent: '',  //类型：String  必有字段  备注：处理结果
                memo: '',  //类型：String  必有字段  备注：备注
                isState: ''  //类型：Number  必有字段  备注：处理状态 0未处理 1已处理
            },

            colors: ['#99A9BF', '#F7BA2A', '#FF9900'], // 等同于 { 2: '#99A9BF', 4: { value: '#F7BA2A', excluded: true }, 5: '#FF9900' }

            // 处理反馈 表单字段
            updateForm: {
                memo: '',
                handingContent: ''
            },


            tableDataProps: {
                value: 'id',
                label: 'menuName',
                children: 'children',
                checkStrictly: true,
                emitPath: false,
                expandTrigger: 'click'
            },
            cateList: [],  // 意见反馈分类 数组

            // 处理反馈 表单检索
            rules: {
                // 备注
                memo: [
                    {
                        required: true,
                        message: '请输入备注',
                        trigger: 'blur'
                    }
                ],
                // 处理结果
                handingContent: [
                    {
                        required: true,
                        message: '请输入处理结果',
                        trigger: 'blur'
                    }
                ]
            }
        }
    },
    mounted() {
        this.to_load() // 进入此页面进行加载
    },
    methods: {
        /**
         * @memo 进入此页面进行加载
         */
        to_load() {
            this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize) // 加载列表
            this.get_category_data()
        },

        /**
         * @memo 检索重置
         */
        reset_btn() {
            this.searchForm.nickname = '' //类型：String  可有字段  备注：用户昵称
            this.searchForm.cateId = ''   //类型：Number  可有字段  备注：分类id
            this.get_tabel_data(1, this.searchForm.pageSize)
        },
        /**
         * @memo 获取列表
         */
        async get_tabel_data(pageNum, pageSize) {
            let param = {
                pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
                pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
                nickname: this.searchForm.nickname,
                cateId: this.searchForm.cateId
            }
            await feedback_list_path(param)
                .then(res => {
                    this.tableData = res.data.resultList
                    this.searchForm.totals = res.data.totalRows
                    this.searchForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchForm.pageNum > 1) {
                        if (res.data.resultList.length === 0) {
                            this.get_tabel_data(
                                this.searchForm.pageNum - 1,
                                this.searchForm.pageSize
                            )
                        }
                    } else if (this.searchForm.pageNum === 0) {
                        return this.$message.warning('没有更多数据')
                    }
                })
                .catch(() => { })
        },


        /**
         * @deprecated table的sort-change事件
         * @param 参数 column.order 有两个值：ascending 升序  descending 降序
         */
        change_table_sort(column) {
            //获取字段名称和排序类型
            // var fieldName = column.prop;   // 获取prop指定的字段
            var sortingType = column.order; //  ascending 升序   descending 降序
            var sortable = column.column.sortable; // 点击的是那一列
            if (sortingType == "descending") {
                if (sortable == "isState") {
                    this.searchForm.isState = "0";
                }
            }
            //按照升序排序
            else if (sortingType == "ascending") {
                if (sortable == "isState") {
                    this.searchForm.isState = "1";
                }
            }
            //无序
            else if (!sortingType) {
                if (sortable == "isState") {
                    this.searchForm.isState = "";
                }
            }
            //请求列表
            this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize);
        },

        /**
         * @memo 下拉框选中的按钮的操作  更多操作
         */
        handle_command(command) {
            if (command.button == 'info_btn') {
                this.info_btn(command.row)
            } else if (command.button == 'hand_btn') {
                this.hand_btn(command.row)
            }
        },
        /**
         * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
         */
        compose_value(item, row) {
            return {
                button: item,
                row: row
            }
        },

        /**
         * @memo 获取 意见反馈分类数组
         */
        async get_category_data() {
            let { data: res } = await feed_category_all_path({})
            this.cateList = res
        },

        /************************* 详情 操作 ***************************** */
        /**
         * @memo 详情 接口
         */
         async get_info_data(id) {
            const res = await feedback_info_path({
                id: id
            })
            return res
        },

        /**
         * @memo 详情按钮
         */
        info_btn(id) {
            if (id) {
                this.get_info_data(id)
                    .then(res => {
                        this.addForm = {
                            id: res.data.id,
                            headimgurl: res.data.headimgurl,
                            nickName: res.data.nickName,
                            modilePhone: res.data.modilePhone,
                            content: res.data.content,
                            createOperatorName: res.data.createOperatorName,
                            createTime: res.data.createTime,
                            satisFaction: res.data.satisFaction,
                            imgPathList: res.data.imgPathList,
                            imgPathListUrl: res.data.imgPathListUrl
                                ? res.data.imgPathListUrl.split(',')
                                : [],
                            name: res.data.name,
                            mobilePhone: res.data.mobilePhone,
                            memo: res.data.memo,
                            handingContent: res.data.handingContent,
                            complaintsTime: res.data.complaintsTime,
                            cateId: res.data.cateId,
                            isState: res.data.isState,
                        }
                        this.add = 1
                        this.drawer = true
                        this.title = '反馈详情'
                    })
                    .catch(() => { })
            } else {
                this.$message.error('数据错误请刷新页面')
            }
        },

        
        /************************* 处理反馈 操作 ***************************** */
        /**
         * @memo 处理反馈 接口
         */
        async hand_data() {
            let param = {
                id: this.addForm.id,
                handingContent: this.updateForm.handingContent,
                isState: 1
            }
            await feedback_hand_path(param).then(() => {
                this.$message.success('已处理')
                this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
                this.clear_drawer()
            })
        },

        /**
         * @memo 处理反馈 按钮
         */
        hand_btn(id) {
            if (id) {
                this.get_info_data(id)
                    .then(res => {
                        this.addForm = {
                            id: res.data.id,
                            isState: res.data.isState
                        }
                        this.add = 2
                        this.drawer = true
                        this.title = '处理反馈'
                    })
                    .catch(() => { })
            } else {
                this.$message.error('数据错误请刷新页面')
            }
        },

        /**
         * @memo 提交按钮
         */
        submit_btn(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    this.hand_data()
                } else {
                    return false
                }
            })
        },


        /************************** 关闭抽屉 操作 ****************************** */
        /**
         * @memo 点击取消按钮，关闭抽屉
         */
        close_drawer() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.drawer = false
                })
                .catch(() => { })
        },
        /**
         * @memo 关闭抽屉
         */
        clear_drawer() {
            this.drawer = false
            this.addForm = {
                handingContent: '',
                memo: ''
            }

            //关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /*********************** 列表judge 操作 *************************** */
        /**
         * @memo 分类 judge
         */
        cate_judge(cateId) {
            let res = this.cateList.filter(item => {
                return item.id == cateId
            })
            return res.length > 0 ? res[0].cateName : '暂无分类'
        },
    }
}
