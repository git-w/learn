/*
 * @description: 内容管理之素材管理
 * @author: leroy
 * @Compile：2021-01-28 09：00
 * @update: leroy(2021-01-29 18：00)
 */

import Material from '@/components/content/material/material.vue'
export default {
  components: {
    Material
  },
  data() {
    return {
        materialType:3
    }
  },

  mounted() {},
  methods: {}
}
