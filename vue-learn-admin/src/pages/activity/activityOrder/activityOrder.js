/**
 * @description: 活动管理-订单管理
 * @author: leroy
 * @Compile：2021-08-21 18：00
 * @update:lx(2021-08-25 18：00)
 */
import {
    activity_order_list_path,  // 列表
    activity_order_info_path,  // 详情
    activity_order_confirm_path,  // 确认订单
    activity_order_refund_path, // 退款
    activity_order_refuse_refund_path,  // 拒绝退款
    activity_info_path,  // 活动 详情
} from '../../../api/activity'

import { date_format } from '@/utils/date/dateFormat'

import { price_rule } from '@/utils/verify/replaceMethods'

export default {
    data() {
        // 退款价格 校验
        let isRealCost = (rule, value, callback) => {
            let _that = this

            if (value <= _that.addForm.realCost) {
                callback()
            } else {
                return callback(new Error('退款价格不能大于订单价格!'))
            }
        }
        return {
            ids: '',

            price_rule, // 处理价格的公共方法
            date_format, // 时间处理器方法 (年-月-日-时-分-秒)

            activeName: 'all',
            tableType: '1', //列表的属性

            searchForm: {
                pageNum: 1, // 当前页码
                pageSize: 10, // 每页长度
                totals: 0, // 当页有多少条
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                isOrderStatus: '', //订单状态 0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货，5-完成。 6-退款申请 7-审核通过退款中  8-退款完成，9-超时未支付取消的
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                name: '',  // 类型：String  可有字段  备注：活动名称
                nickName: '',  // 类型：String  可有字段  备注：预约人昵称
            }, // 中间变量用于给接口的param对象中赋值
            searchAllForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0, // 当页有多少条
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                isOrderStatus: '', //订单状态0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货，5-完成。 6-退款申请 7-审核通过退款中  8-退款完成，9-超时未支付取消的
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                name: '',  // 类型：String  可有字段  备注：活动名称
                nickName: '',  // 类型：String  可有字段  备注：预约人昵称
            }, //全部列表检索对象
            searchPaymentForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0, // 当页有多少条
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                isOrderStatus: '', //订单状态0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货，5-完成。 6-退款申请 7-审核通过退款中  8-退款完成，9-超时未支付取消的
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                name: '',  // 类型：String  可有字段  备注：活动名称
                nickName: '',  // 类型：String  可有字段  备注：预约人昵称
            }, //代付款
            searchWaitingForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0, // 当页有多少条
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                isOrderStatus: '', //订单状态0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货，5-完成。 6-退款申请 7-审核通过退款中  8-退款完成，9-超时未支付取消的
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                name: '',  // 类型：String  可有字段  备注：活动名称
                nickName: '',  // 类型：String  可有字段  备注：预约人昵称
            }, //待使用
            searchDoneForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0, // 当页有多少条
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                isOrderStatus: '', //订单状态0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货，5-完成。 6-退款申请 7-审核通过退款中  8-退款完成，9-超时未支付取消的
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                name: '',  // 类型：String  可有字段  备注：活动名称
                nickName: '',  // 类型：String  可有字段  备注：预约人昵称
            }, //已完成
            searchRefundingForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0, // 当页有多少条
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                isOrderStatus: '', //订单状态0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货，5-完成。 6-退款申请 7-审核通过退款中  8-退款完成，9-超时未支付取消的
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                name: '',  // 类型：String  可有字段  备注：活动名称
                nickName: '',  // 类型：String  可有字段  备注：预约人昵称
            }, //退款中
            searchNotPaidForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0, // 当页有多少条
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                isOrderStatus: '', //订单状态0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货，5-完成。 6-退款申请 7-审核通过退款中  8-退款完成，9-超时未支付取消的
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                name: '',  // 类型：String  可有字段  备注：活动名称
                nickName: '',  // 类型：String  可有字段  备注：预约人昵称
            }, //未付款关闭
            searchRefundForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0, // 当页有多少条
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                isOrderStatus: '', //订单状态0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货，5-完成。 6-退款申请 7-审核通过退款中  8-退款完成，9-超时未支付取消的
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                name: '',  // 类型：String  可有字段  备注：活动名称
                nickName: '',  // 类型：String  可有字段  备注：预约人昵称
            }, //退款后关闭
            searchFackOfForm: {
                pageNum: 1,
                pageSize: 10,
                totals: 0, // 当页有多少条
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                isOrderStatus: '', //订单状态0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货，5-完成。 6-退款申请 7-审核通过退款中  8-退款完成，9-超时未支付取消的
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                name: '',  // 类型：String  可有字段  备注：活动名称
                nickName: '',  // 类型：String  可有字段  备注：预约人昵称
            }, //交易关闭

            tableData: [], // 列表表单数据
            orderData: [],  // 订单列表数据

            drawer: false, // 控制抽屜的开合
            title: '', // 控制抽屜的头部
            add: 1,
            activeCollapse: [],  // 详情折叠面板 默认打开

            addForm: {},  //订单管理 表单字段
            infoForm: {},  //活动详情 表单字段
            refundForm: {
                id: '',
                realCost: '',
            },  //退款 表单字段

            // 订单管理 表单校验
            rules: {
                // 修改价格
                // 改后价格
                realCost: [{
                    required: true,
                    message: '请输入改后价格',
                    trigger: 'blur'
                },
                {
                    pattern: /^\d+$|^\d+[.]?\d+$/,
                    message: '请输入正确的价格'
                }],
            },

            // 退款 表单校验
            refundRules: {
                realCost: [{
                    required: true,
                    message: '请输入退款金额',
                    trigger: 'blur'
                },
                {
                    pattern: /^\d+$|^\d+[.]?\d+$/,
                    message: '请输入正确的价格'
                }, {
                    validator: isRealCost
                }],
            }
        }
    },
    watch: {
        tableType(newVal, oldVal) {
            this.get_tabel_btn(newVal)
        }
    },
    mounted() {
        this.to_load() // 进入此页面进行请求
    },
    computed: {
        activityNum() {
            return this.$store.getters['getActivityOrderNum']
        }
    },

    methods: {
        /**
         * @memo  点击tabs切换
         */
        handle_click(tab, event) {
            if (tab.name == 'all') {
                this.tableType = '1' //全部
            } else if (tab.name == 'payment') {
                this.tableType = '2' //待付款
            } else if (tab.name == 'waiting') {
                this.tableType = '3' //待使用
            } else if (tab.name == 'done') {
                this.tableType = '4' //已完成
            } else if (tab.name == 'refunding') {
                this.tableType = '5' //退款中
            } else if (tab.name == 'notPaid') {
                this.tableType = '6' //未付款关闭
            } else if (tab.name == 'refund') {
                this.tableType = '7' //退款后关闭
            } else if (tab.name == 'fackOf') {
                this.tableType = '8' //交易关闭
            }
        },

        /**
         * @memo  获取列表
         */
        get_tabel_btn(tableType) {
            if (tableType == '1') {
                //全部
                this.searchForm = {
                    pageNum: this.searchAllForm.pageNum,
                    pageSize: this.searchAllForm.pageSize,
                    totals: this.searchAllForm.totals,
                    isOrderStatus: '', // 类型：String  可有字段  备注：订单状态
                    orderNum: this.searchForm.orderNum, //类型：String  可有字段  备注：订单号
                    createDate: this.searchForm.createDate,  //类型：Number  可有字段  备注：下单时间
                    name: this.searchForm.name,  // 类型：String  可有字段  备注：活动名称
                    nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：预约人昵称
                }
                this.get_tabel_data(
                    this.searchAllForm.pageNum,
                    this.searchAllForm.pageSize
                )
            } else if (tableType == '2') {
                //代付款
                this.searchForm = {
                    pageNum: this.searchPaymentForm.pageNum,
                    pageSize: this.searchPaymentForm.pageSize,
                    totals: this.searchPaymentForm.totals,
                    isOrderStatus: '1', //订单状态
                    orderNum: this.searchForm.orderNum, //类型：String  可有字段  备注：订单号
                    createDate: this.searchForm.createDate,  //类型：Number  可有字段  备注：下单时间
                    name: this.searchForm.name,  // 类型：String  可有字段  备注：活动名称
                    nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：预约人昵称
                }
                this.get_tabel_data(
                    this.searchPaymentForm.pageNum,
                    this.searchPaymentForm.pageSize
                )
            } else if (tableType == '3') {
                //待使用
                this.searchForm = {
                    pageNum: this.searchWaitingForm.pageNum,
                    pageSize: this.searchWaitingForm.pageSize,
                    totals: this.searchWaitingForm.totals,
                    isOrderStatus: '2', //待使用
                    orderNum: this.searchForm.orderNum, //类型：String  可有字段  备注：订单号
                    createDate: this.searchForm.createDate,  //类型：Number  可有字段  备注：下单时间
                    name: this.searchForm.name,  // 类型：String  可有字段  备注：活动名称
                    nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：预约人昵称
                }
                this.get_tabel_data(
                    this.searchWaitingForm.pageNum,
                    this.searchWaitingForm.pageSize
                )
            } else if (tableType == '4') {
                //已完成
                this.searchForm = {
                    pageNum: this.searchDoneForm.pageNum,
                    pageSize: this.searchDoneForm.pageSize,
                    totals: this.searchDoneForm.totals,
                    isOrderStatus: '3', //订单状态
                    orderNum: this.searchForm.orderNum, //类型：String  可有字段  备注：订单号
                    createDate: this.searchForm.createDate,  //类型：Number  可有字段  备注：下单时间
                    name: this.searchForm.name,  // 类型：String  可有字段  备注：活动名称
                    nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：预约人昵称
                }
                this.get_tabel_data(
                    this.searchDoneForm.pageNum,
                    this.searchDoneForm.pageSize
                )
            } else if (tableType == '5') {
                //申请退款
                this.searchForm = {
                    pageNum: this.searchRefundingForm.pageNum,
                    pageSize: this.searchRefundingForm.pageSize,
                    totals: this.searchRefundingForm.totals,
                    isOrderStatus: '4', //订单状态
                    orderNum: this.searchForm.orderNum, //类型：String  可有字段  备注：订单号
                    createDate: this.searchForm.createDate,  //类型：Number  可有字段  备注：下单时间
                    name: this.searchForm.name,  // 类型：String  可有字段  备注：活动名称
                    nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：预约人昵称
                }
                this.get_tabel_data(
                    this.searchRefundingForm.pageNum,
                    this.searchRefundingForm.pageSize
                )
            } else if (tableType == '6') {
                //未付款关闭
                this.searchForm = {
                    pageNum: this.searchNotPaidForm.pageNum,
                    pageSize: this.searchNotPaidForm.pageSize,
                    totals: this.searchNotPaidForm.totals,
                    isOrderStatus: '7', //订单状态
                    orderNum: this.searchForm.orderNum, //类型：String  可有字段  备注：订单号
                    createDate: this.searchForm.createDate,  //类型：Number  可有字段  备注：下单时间
                    name: this.searchForm.name,  // 类型：String  可有字段  备注：活动名称
                    nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：预约人昵称
                }
                this.get_tabel_data(
                    this.searchNotPaidForm.pageNum,
                    this.searchNotPaidForm.pageSize
                )
            } else if (tableType == '7') {
                //退款完成
                this.searchForm = {
                    pageNum: this.searchRefundForm.pageNum,
                    pageSize: this.searchRefundForm.pageSize,
                    totals: this.searchRefundForm.totals,
                    isOrderStatus: '5', //订单状态
                    orderNum: this.searchForm.orderNum, //类型：String  可有字段  备注：订单号
                    createDate: this.searchForm.createDate,  //类型：Number  可有字段  备注：下单时间
                    name: this.searchForm.name,  // 类型：String  可有字段  备注：活动名称
                    nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：预约人昵称
                }
                this.get_tabel_data(
                    this.searchRefundForm.pageNum,
                    this.searchRefundForm.pageSize
                )
            } else if (tableType == '8') {
                //交易关闭
                this.searchForm = {
                    pageNum: this.searchFackOfForm.pageNum,
                    pageSize: this.searchFackOfForm.pageSize,
                    totals: this.searchFackOfForm.totals,
                    isOrderStatus: '0', //订单状态
                    orderNum: this.searchForm.orderNum, //类型：String  可有字段  备注：订单号
                    createDate: this.searchForm.createDate,  //类型：Number  可有字段  备注：下单时间
                    name: this.searchForm.name,  // 类型：String  可有字段  备注：活动名称
                    nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：预约人昵称
                }
                this.get_tabel_data(
                    this.searchFackOfForm.pageNum,
                    this.searchFackOfForm.pageSize
                )
            }
        },

        /**
         * @memo 进入此页面的加载
         */
        to_load() {
            this.get_tabel_btn(this.tableType) // 请求列表的数据
        },

        /**
         * @memo 检索搜索按钮
         */
        search_btn() {
            this.get_tabel_btn(this.tableType) // 请求列表的数据
        },

        /**
         * @memo 检索重置按钮
         */
        reset_btn() {
            this.searchForm.orderNum = ''  //类型：String  可有字段  备注：订单号
            this.searchForm.createDate = []  //类型：Number  可有字段  备注：下单时间
            this.searchForm.name = ''  // 类型：String  可有字段  备注：活动名称
            this.searchForm.nickName = ''  // 类型：String  可有字段  备注：预约人昵称
            this.get_tabel_btn(this.tableType) // 请求列表的数据
        },

        /**
         * @memo  获取列表
         */
        async get_tabel_data(pageNum, pageSize) {
            const param = {
                pageNum: pageNum, //当前页码
                pageSize: pageSize, //每页的条数
                isOrderStatus: this.searchForm.isOrderStatus, //类型：String  必有字段  备注：状态
                createTimeFrom: this.searchForm.createDate != null ? this.searchForm.createDate[0] : '',  //类型：Number  可有字段  备注：下单时间（始）
                createTimeTo: this.searchForm.createDate != null ? this.searchForm.createDate[1] : '',  //类型：Number  可有字段  备注：下单时间（终）
                orderNum: this.searchForm.orderNum,  //类型：String  可有字段  备注：订单号
                name: this.searchForm.name,  // 类型：String  可有字段  备注：活动名称
                nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：预约人昵称
            }
            await activity_order_list_path(param)
                .then(res => {
                    this.tableData = res.data.resultList //列表的数据
                    this.searchForm.totals = res.data.totalRows //总长度
                    this.searchForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchForm.pageNum = res.data.pageNum // 请求页码
                    if (this.tableType == '1') { // 全部
                        this.searchAllForm.totals = res.data.totalRows //总长度
                        this.searchAllForm.pageSize = res.data.pageSize // 当前页码条数
                        this.searchAllForm.pageNum = res.data.pageNum // 请求页码
                    } else if (this.tableType == '2') {  // 代付款
                        this.searchPaymentForm.totals = res.data.totalRows //总长度
                        this.searchPaymentForm.pageSize = res.data.pageSize // 当前页码条数
                        this.searchPaymentForm.pageNum = res.data.pageNum // 请求页码
                    } else if (this.tableType == '3') {  // 待使用
                        this.searchWaitingForm.totals = res.data.totalRows //总长度
                        this.searchWaitingForm.pageSize = res.data.pageSize // 当前页码条数
                        this.searchWaitingForm.pageNum = res.data.pageNum // 请求页码
                    } else if (this.tableType == '4') {  // 已完成
                        this.searchDoneForm.totals = res.data.totalRows //总长度
                        this.searchDoneForm.pageSize = res.data.pageSize // 当前页码条数
                        this.searchDoneForm.pageNum = res.data.pageNum // 请求页码
                    } else if (this.tableType == '5') {  // 退款中
                        this.searchRefundingForm.totals = res.data.totalRows //总长度
                        this.searchRefundingForm.pageSize = res.data.pageSize // 当前页码条数
                        this.searchRefundingForm.pageNum = res.data.pageNum // 请求页码
                    } else if (this.tableType == '6') {  // 未付款关闭
                        this.searchNotPaidForm.totals = res.data.totalRows //总长度
                        this.searchNotPaidForm.pageSize = res.data.pageSize // 当前页码条数
                        this.searchNotPaidForm.pageNum = res.data.pageNum // 请求页码
                    } else if (this.tableType == '7') {  // 退款后关闭
                        this.searchRefundForm.totals = res.data.totalRows //总长度
                        this.searchRefundForm.pageSize = res.data.pageSize // 当前页码条数
                        this.searchRefundForm.pageNum = res.data.pageNum // 请求页码
                    } else if (this.tableType == '8') {  // 交易关闭
                        this.searchFackOfForm.totals = res.data.totalRows //总长度
                        this.searchFackOfForm.pageSize = res.data.pageSize // 当前页码条数
                        this.searchFackOfForm.pageNum = res.data.pageNum // 请求页码
                    }
                    if (this.searchForm.pageNum > 1) {
                        if (res.data.resultList.length == 0) {
                            this.get_tabel_data(
                                this.searchForm.pageNum - 1,
                                this.searchForm.pageSize
                            )
                        }
                    } else if (this.searchForm.pageNum == 0) {
                        return this.$message.warning('没有更多数据')
                    }
                })
                .catch(() => { })
        },

        /**
         * @memo 下拉框选中的按钮的操作  更多操作
         */
        handle_command(command) {
            if (command.button == 'info_btn') {
                //订单详情
                this.info_btn(command.row)
            } else if (command.button == 'cancel_btn') {
                //核销
                this.cancel_btn(command.row)
            } else if (command.button == 'refund_btn') {
                //退款
                this.refund_btn(command.row)
            } else if (command.button == 'initiative_refund_btn') {
                //主动退款
                this.initiative_refund_btn(command.row)
            }
        },
        /**
         * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
         */
        compose_value(item, row) {
            return {
                button: item,
                row: row
            }
        },

        /********************** 详情 操作 ********************************* */
        /**
         * @memo 详情接口
         */
        async get_info_data(id) {
            const res = await activity_order_info_path({
                id: id
            })
            return res
        },

        /**
         * @memo 活动详情 接口
         */
        async get_activity_info_data(id) {
            const res = await activity_info_path({
                id: id
            })
            this.infoForm = res.data
        },

        /**
         * @memo 详情信息
         */
        info_btn(id) {
            this.get_info_data(id)
                .then(res => {
                    if (res.code === 100000) {
                        this.addForm = {
                            id: res.data.id,
                            activityId: res.data.activityId,
                            payOperatorId: res.data.payOperatorId,
                            orderNum: res.data.orderNum,
                            payTime: res.data.payTime,
                            actCost: res.data.actCost,
                            realCost: res.data.realCost,
                            discountType: res.data.discountType,
                            ardId: res.data.ardId,
                            cardName: res.data.cardName,
                            discountId: res.data.discountId,
                            discountName: res.data.discountName,
                            paymentMethod: res.data.paymentMethod,
                            payState: res.data.payState,
                            refundStartTime: res.data.refundStartTime,
                            refundEndTime: res.data.refundEndTime,
                            activityPeopleNumber: res.data.activityPeopleNumber,
                            isOrderStatus: res.data.isOrderStatus,
                            refundToDescribe: res.data.refundToDescribe,
                            memo: res.data.memo,
                            orderActivityDate: res.data.orderActivityDate,
                            signInTime: res.data.signInTime,
                            isSignIn: res.data.isSignIn,
                            isUserType: res.data.isUserType,
                            createOperatorId: res.data.createOperatorId,
                            createOperatorName: res.data.createOperatorName,
                            createTime: res.data.createTime,
                            updateOperatorId: res.data.updateOperatorId,
                            updateOperatorName: res.data.updateOperatorName,
                            stratTime: res.data.stratTime,
                            endTime: res.data.endTime,
                            nickName: res.data.nickName,
                            headimgurl: res.data.headimgurl
                        }
                        this.get_activity_info_data(res.data.activityId)
                        this.activeCollapse = ['1', '2']
                        this.drawer = true
                        this.title = "订单详情"
                        this.add = 1
                    } else {
                        this.$message.error(res.message)
                    }
                })
                .catch(() => {
                    this.$message.error('查询失败')
                })
        },


        /**************************** 核销 操作 ****************************** */
        /**
         * @memo 核销 按钮
         */
        cancel_btn(id) {
            if (id) {
                this.$confirm("将核销该订单，是否确定?", "提示", {
                    confirmButtonText: "确定",
                    cancelButtonText: "取消",
                    type: "warning",
                })
                    .then(() => {
                        this.cancel_data(id)
                            .then(res => {
                                if (res.code == 100000) {
                                    // 刷新列表
                                    this.get_tabel_data(this.searchForm.currentPage, this.searchForm.numPerPage);
                                    this.$message({
                                        message: "核销成功",
                                        type: "success",
                                    });
                                } else {
                                    this.$message({
                                        message: res.message,
                                        type: "error",
                                    });
                                }

                            })
                    })
            }
        },

        /**
         * @memo 核销 接口
         */
        async cancel_data(id) {
            const res = await activity_order_confirm_path({
                id: id,
            });
            return res;
        },

        /****************************** 退款 操作 ************************************* */
        /**
         * @memo  商家退款
         */
        refund_btn(id) {
            this.get_info_data(id)
                .then(res => {
                    if (res.code === 100000) {
                        this.addForm = {
                            id: res.data.id,
                            activityId: res.data.activityId,
                            payOperatorId: res.data.payOperatorId,
                            orderNum: res.data.orderNum,
                            payTime: res.data.payTime,
                            actCost: res.data.actCost,
                            realCost: res.data.realCost,
                            discountType: res.data.discountType,
                            ardId: res.data.ardId,
                            cardName: res.data.cardName,
                            discountId: res.data.discountId,
                            discountName: res.data.discountName,
                            paymentMethod: res.data.paymentMethod,
                            payState: res.data.payState,
                            refundStartTime: res.data.refundStartTime,
                            refundEndTime: res.data.refundEndTime,
                            activityPeopleNumber: res.data.activityPeopleNumber,
                            isOrderStatus: res.data.isOrderStatus,
                            refundToDescribe: res.data.refundToDescribe,
                            memo: res.data.memo,
                            orderActivityDate: res.data.orderActivityDate,
                            signInTime: res.data.signInTime,
                            isSignIn: res.data.isSignIn,
                            isUserType: res.data.isUserType,
                            createOperatorId: res.data.createOperatorId,
                            createOperatorName: res.data.createOperatorName,
                            createTime: res.data.createTime,
                            updateOperatorId: res.data.updateOperatorId,
                            updateOperatorName: res.data.updateOperatorName,
                            stratTime: res.data.stratTime,
                            endTime: res.data.endTime,
                            nickName: res.data.nickName,
                            headimgurl: res.data.headimgurl
                        }
                        this.refundForm = {
                            id: res.data.id,
                            realCost: res.data.realCost
                        }
                        this.activeCollapse = ['1', '2']
                        this.drawer = true
                        this.title = '商家退款'
                        this.add = 2
                    } else {
                        this.$message.error(res.message)
                    }
                })
        },

        /**
         * @memo 主动退款 按钮
         */
        initiative_refund_btn(id) {
            this.get_info_data(id)
                .then(res => {
                    if (res.code === 100000) {
                        this.addForm = {
                            id: res.data.id,
                            activityId: res.data.activityId,
                            payOperatorId: res.data.payOperatorId,
                            orderNum: res.data.orderNum,
                            payTime: res.data.payTime,
                            actCost: res.data.actCost,
                            realCost: res.data.realCost,
                            discountType: res.data.discountType,
                            ardId: res.data.ardId,
                            cardName: res.data.cardName,
                            discountId: res.data.discountId,
                            discountName: res.data.discountName,
                            paymentMethod: res.data.paymentMethod,
                            payState: res.data.payState,
                            refundStartTime: res.data.refundStartTime,
                            refundEndTime: res.data.refundEndTime,
                            activityPeopleNumber: res.data.activityPeopleNumber,
                            isOrderStatus: res.data.isOrderStatus,
                            refundToDescribe: res.data.refundToDescribe,
                            memo: res.data.memo,
                            orderActivityDate: res.data.orderActivityDate,
                            signInTime: res.data.signInTime,
                            isSignIn: res.data.isSignIn,
                            isUserType: res.data.isUserType,
                            createOperatorId: res.data.createOperatorId,
                            createOperatorName: res.data.createOperatorName,
                            createTime: res.data.createTime,
                            updateOperatorId: res.data.updateOperatorId,
                            updateOperatorName: res.data.updateOperatorName,
                            stratTime: res.data.stratTime,
                            endTime: res.data.endTime,
                            nickName: res.data.nickName,
                            headimgurl: res.data.headimgurl
                        }
                        this.refundForm = {
                            id: res.data.id,
                            realCost: res.data.realCost
                        }
                        this.activeCollapse = ['1', '2']
                        this.drawer = true
                        this.title = '主动退款'
                        this.add = 2
                    } else {
                        this.$message.error(res.message)
                    }
                })
                .catch(() => {
                    this.$message.error('查询失败')
                })
        },

        /**
         * @memo   退款 接口
         * @param  orderNum      //类型：String  必有字段  备注：订单号
         */
        async refund_data() {
            let param = {
                id: this.refundForm.id, //类型：String  必有字段  备注：订单号
            }
            await activity_order_refund_path(param).then(res => {
                this.$message.success(res.message)
                this.clear_drawer()
                this.get_tabel_btn(this.tableType)
            })
        },

        /**
         * @memo  拒绝退款 接口
         */
        async refuseForRefund_refund_data() {
            let param = {
                id: this.refundForm.id //类型：String  必有字段  备注：订单号
            }
            await activity_order_refuse_refund_path(param).then(res => {
                this.$message({
                    duration: 800,
                    message: res.message,
                    type: 'success'
                })
                this.clear_drawer()
            })
        },

        /**
         * @memo  拒绝退款提交按钮
         */
        submit_refuse_refund_btn(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    this.refuseForRefund_refund_data()
                } else {
                    // 提交失败
                    return false
                }
            })
        },

        /**
         * @memo  退款提交按钮
         */
        submit_refund_btn(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    this.refund_data()
                } else {
                    // 提交失败
                    return false
                }
            })
        },


        /*********************** 抽屉关闭 ************************* */
        /**
         * @memo  抽屉关闭前的回调
         */
        handle_close() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },

        /**
         * @memo  点击取消按钮，关闭抽屉
         */
        close_drawer() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },
        /**
         * @memo  关闭抽屉
         */
        clear_drawer() {
            this.drawer = false
            this.addForm = {}
            this.refundForm = {}
            this.get_tabel_data(
                this.searchForm.pageNum,
                this.searchForm.pageSize
            )
            //关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /*********************** 列表judge 操作 *************************** */
        /**
         * @memo 订单状态
         */
        isOrderStatusJudge(isOrderStatus) {
            if (isOrderStatus == 1) {
                //代付款
                return '待付款'
            } else if (isOrderStatus == 2) {
                //待使用
                return '待使用'
            } else if (isOrderStatus == 3) {
                //已完成
                return '已完成'
            } else if (isOrderStatus == 4) {
                //申请退款
                return '申请退款'
            } else if (isOrderStatus == 6) {
                //退款中
                return '退款中'
            } else if (isOrderStatus == 5) {
                //退款后关闭
                return '退款完成'
            } else if (isOrderStatus == 7) {
                //未付款关闭
                return '超时未支付'
            } else if (isOrderStatus == 0) {
                //交易关闭
                return '订单取消'
            }
        },
    }
}
