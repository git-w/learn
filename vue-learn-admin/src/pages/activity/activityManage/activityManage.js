/**
 * @description: 活动管理-活动管理
 * @author: leroy
 * @Compile：2021-11-29 16：00
 * @update:lx(2021-11-29 16：00)
 */
import {
  activity_save_path, // 添加
  activity_update_path, // 修改
  activity_delete_path, // 删除
  activity_list_path, // 列表
  activity_info_path, // 详情
  activity_category_all_path // 分类全部
} from '../../../api/activity'

import { date_format } from '../../../utils/date/dateFormat'

import Amap from '@/components/Amap/Amap.vue'
import { price_rule } from '@/utils/verify/replaceMethods'
import { get_img_url_to_path_list } from '@/utils/img/imageMethod.js'

import { setStore } from '@/mixins/activity/setStore.js'
export default {
  components: {
    Amap
  },
  mixins: [setStore],
  data() {
    // 上传活动封面验证
    let isSharePath = (rule, value, callback) => {
      let _that = this
      // 判断活动封面是否有值
      if (_that.addForm.shareItPictures.length > 0) {
        callback()
      } else {
        // 活动封面没有值,提示请上传图片
        return callback(new Error('请上传图片'))
      }
    }

    // 上传活动分享图片验证
    let isCoverPath = (rule, value, callback) => {
      let _that = this
      // 判断活动分享是否有值
      if (_that.addForm.pictures.length > 0) {
        callback()
      } else {
        // 活动分享没有值,提示请上传图片
        return callback(new Error('请上传图片'))
      }
    }

    // 上传活动图片验证
    let isImgPathList = (rule, value, callback) => {
      let _that = this
      // 判断活动图片是否有值
      if (_that.addForm.imgPathList.length > 0) {
        callback()
      } else {
        // 活动图片没有值,提示请上传图片
        return callback(new Error('请上传活动图片'))
      }
    }
    return {
      ids: '',

      date_format, // 时间处理器方法 (年-月-日-时-分-秒)
      price_rule, // 处理价格的公共方法

      // 活动管理 检索字段
      searchForm: {
        activityName: '', //类型：String  必有字段  备注：活动名称
        categoryID: '', //类型：String  必有字段  备注：活动分类
        isState: '' //类型：String  必有字段  备注：0上架 1下架
      },
      tableData: [], // 列表表单数据

      categoryList: [], // 活动分类下拉框数组

      drawer: false, //控制抽屉的开合
      title: '', //控制抽屉的头部
      add: 1, //判断抽屉显示 添加修改 - 1、详情 - 2
      activeCollapse: [], // 详情折叠面板 默认打开

      // 活动管理 表单数据
      addForm: {
        id: '', //类型：String  必有字段  备注：编号
        name: '', //类型：String  可有字段  备注：活动名称
        msg: '', //类型：String  可有字段  备注：活动简介
        content: '', //类型：String  可有字段  备注：活动描述
        bookingInformation: '', //类型：String  可有字段  备注：预定须知
        pictures: '', //类型：String  可有字段  备注：活动封面路径（短）
        shareItPictures: '', //类型：String  可有字段  备注：分享海报路径（短）
        imgPathList: '', //类型：String  可有字段  备注：活动照片路径（短），多图用,隔开
        stratTime: '', //类型：Number  可有字段  备注：活动开始时间
        endTime: '', //类型：Number  可有字段  备注：活动结束时间
        isFree: '', //类型：Number  可有字段  备注：是否免费：0-免费；1-不免费
        price: '', //类型：Number  可有字段  备注：活动报名费
        activityUserCount: '', //类型：Number  可有字段  备注：活动名额
        categoryId: '', //类型：String  可有字段  备注：活动类型ID
        isState: '', //类型：Number  可有字段  备注：是否上线 0是 1否
        payStartTime: '', //类型：Number  可有字段  备注：报名开始时间
        payEndTime: '', //类型：Number  可有字段  备注：报名结束时间
        circleDot: {
          lng: '',
          lat: '',
          address: '' //类型：String  必有字段  备注：活动地址
        } // 设置地图默认的经纬度为北京市
      },

      // 活动管理 表单校验
      rules: {
        // 活动封面
        shareItPictures: [
          {
            required: true,
            validator: isSharePath
          }
        ],

        // 活动分享
        pictures: [
          {
            required: true,
            validator: isCoverPath
          }
        ],

        // 活动图片
        imgPathList: [
          {
            required: true,
            validator: isImgPathList
          }
        ],

        // 活动名称
        name: [
          {
            required: true,
            message: '请输入活动名称',
            trigger: 'blur'
          }
        ],

        // 活动分类
        categoryId: [
          {
            required: true,
            message: '请选择活动分类',
            trigger: 'change'
          }
        ],

        // 报名时间
        applyTime: [
          {
            required: true,
            message: '请选择报名时间',
            trigger: 'change'
          }
        ],

        // 活动时间
        activityTime: [
          {
            required: true,
            message: '请选择活动时间',
            trigger: 'change'
          }
        ],

        // 活动费用
        price: [
          {
            required: true,
            message: '请选择活动费用',
            trigger: 'blur'
          }
        ],

        // 活动名额
        activityNum: [
          {
            required: true,
            message: '请选择活动名额',
            trigger: 'change'
          }
        ],

        // 活动地址
        'circleDot.address': [
          {
            required: true,
            message: '请输入活动地址',
            trigger: 'blur'
          }
        ]
      }
    }
  },
  mounted() {
    this.to_load() // 进入此页面进行请求
  },

  methods: {
    /**
     * @memo 进入此页面的加载
     */
    to_load() {
      this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
      this.get_category_list_data()
    },

    /**
     * @memo 检索重置按钮
     */
    reset_btn() {
      this.searchForm.activityName = '' //类型：String  可有字段  备注：活动名称
      this.searchForm.categoryID = '' //类型：String  可有字段  备注：活动类型ID
      this.searchForm.isState = '' //类型：Number  可有字段  备注：是否上线 0是 1否
      this.get_tabel_data(1, this.searchForm.pageSize) // 请求列表数据
    },

    /**
     * @memo 获取列表
     */
    async get_tabel_data(pageNum, pageSize) {
      const param = {
        pageSize: pageSize, //当前页码
        pageNum: pageNum, //每页的条数
        activityName: this.searchForm.activityName, //类型：String  必有字段  备注：活动名称
        categoryID: this.searchForm.categoryID, //类型：String  必有字段  备注：活动分类
        isState: this.searchForm.isState //类型：String  必有字段  备注：0上架 1下架
      }
      await activity_list_path(param)
        .then(res => {
          this.tableData = res.data.resultList //列表的数据
          this.searchForm.totals = res.data.totalRows //总长度
          this.searchForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchForm.pageNum > 1) {
            if (res.data.resultList.length === 0) {
              this.get_tabel_data(
                this.searchForm.pageNum - 1,
                this.searchForm.pageSize
              )
            }
          } else if (this.searchForm.pageNum === 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => {})
    },

    /**
     * @memo 下拉框选中的按钮的操作  更多操作
     */
    handle_command(command) {
      if (command.button == 'delete_btn') {
        //删除
        this.delete_btn(command.row)
      } else if (command == 'delete_btn') {
        //多选 删除
        this.delete_btn(this.ids)
      } else if (command.button == 'change_btn') {
        //修改
        this.change_btn(command.row)
      } else if (command.button == 'info_btn') {
        //详情
        this.info_btn(command.row)
      } else if (command.button === 'set_store_btn') {
        //设置库存
        this.set_store_btn(command.row)
      }
    },

    /**
     * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
     */
    compose_value(item, row) {
      return {
        button: item,
        row: row
      }
    },

    /**
     * @memo 请求分类的下拉列表的数据
     * 点击添加按钮时需要添加一次
     */
    async get_category_list_data() {
      let { data: res } = await activity_category_all_path({})
      this.categoryList = res
    },

    /************************* 添加、修改 操作 ******************************* */
    /**
     * @memo 添加-跳往添加界面
     */
    add_btn() {
      this.drawer = true
      this.title = '添加活动'
      this.add = 1
      this.addForm = {
        id: '', //类型：String  必有字段  备注：编号
        name: '', //类型：String  可有字段  备注：活动名称
        msg: '', //类型：String  可有字段  备注：活动简介
        content: '', //类型：String  可有字段  备注：活动描述
        bookingInformation: '', //类型：String  可有字段  备注：预定须知
        pictures: [], //类型：String  可有字段  备注：活动封面路径（短）
        shareItPictures: [], //类型：String  可有字段  备注：分享海报路径（短）
        imgPathList: [], //图片存放路径（短），多图用,隔开
        activityTime: [], //类型：Number  可有字段  备注：活动时间
        isFree: 0, //类型：Number  可有字段  备注：是否免费：0-免费；1-不免费
        price: '', //类型：Number  可有字段  备注：活动报名费
        activityUserCount: '', //类型：Number  可有字段  备注：活动名额
        categoryId: '', //类型：String  可有字段  备注：活动类型ID
        isState: 0, //类型：Number  可有字段  备注：是否上线 0是 1否
        applyTime: [], //类型：Number  可有字段  备注：报名时间
        circleDot: {
          lng: '',
          lat: '',
          address: '' //类型：String  必有字段  备注：活动地址
        } // 设置地图默认的经纬度为北京市
      }
      this.get_category_list_data() // 请求活动分类 下拉列表数据
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /**
     * @memo 详情 接口
     */
    async get_info_data(id) {
      const res = await activity_info_path({
        id: id
      })
      return res
    },

    /**
     * @memo 详情 按钮
     */
    info_btn(id) {
      if (id) {
        this.get_info_data(id)
          .then(res => {
            this.addForm = {
              id: res.data.id, //类型：String  必有字段  备注：编号
              name: res.data.name, //类型：String  可有字段  备注：活动名称
              msg: res.data.msg, //类型：String  可有字段  备注：活动简介
              content: res.data.content, //类型：String  可有字段  备注：活动描述
              bookingInformation: res.data.bookingInformation, //类型：String  可有字段  备注：预定须知
              pictures: res.data.pictures, //类型：String  可有字段  备注：活动封面路径（短）
              picturesUrl: res.data.picturesUrl, //类型：String  可有字段  备注：活动封面路径（长）
              shareItPictures: res.data.shareItPictures, //类型：String  可有字段  备注：分享海报路径（短）
              shareItPicturesUrl: res.data.shareItPicturesUrl, //类型：String  可有字段  备注：分享海报路径（长）
              imgPathList: res.data.imgUrlPath
                ? res.data.imgPathList.split(',')
                : [], //类型：String  可有字段  备注：活动照片路径（短），多图用,隔开
              imgUrlPath: res.data.imgUrlPath
                ? res.data.imgUrlPath.split(',') //类型：String  可有字段  备注：活动照片路径（长），多图用,隔开
                : [],
              stratTime: res.data.stratTime, //类型：Number  可有字段  备注：活动开始时间
              endTime: res.data.endTime, //类型：Number  可有字段  备注：活动结束时间
              isFree: res.data.isFree, //类型：Number  可有字段  备注：是否免费：0-免费；1-不免费
              price: res.data.price, //类型：Number  可有字段  备注：活动报名费
              activityUserCount: res.data.activityUserCount, //类型：Number  可有字段  备注：活动名额
              categoryId: res.data.categoryId, //类型：String  可有字段  备注：活动类型ID
              isState: res.data.isState, //类型：Number  可有字段  备注：是否上线 0是 1否
              payStartTime: res.data.payStartTime, //类型：Number  可有字段  备注：报名开始时间
              payEndTime: res.data.payEndTime, //类型：Number  可有字段  备注：报名结束时间
              circleDot: {
                lng: res.data.longitude,
                lat: res.data.latitude,
                address: res.data.address //类型：String  必有字段  备注：活动地址
              } // 设置地图默认的经纬度为北京市
            }
            this.activeCollapse = ['1']
            this.drawer = true
            this.title = '活动详情'
            this.add = 2
          })
          .catch(() => {})
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /**
     * @memo 修改 按钮
     */
    change_btn(id) {
      if (id) {
        this.get_info_data(id)
          .then(res => {
            this.addForm = {
              id: res.data.id, //类型：String  必有字段  备注：编号
              name: res.data.name, //类型：String  可有字段  备注：活动名称
              msg: res.data.msg, //类型：String  可有字段  备注：活动简介
              content: res.data.content, //类型：String  可有字段  备注：活动描述
              bookingInformation: res.data.bookingInformation, //类型：String  可有字段  备注：预定须知
              pictures: get_img_url_to_path_list(
                res.data.pictures,
                res.data.picturesUrl
              ), //类型：String  可有字段  备注：活动封面路径
              shareItPictures: get_img_url_to_path_list(
                res.data.shareItPictures,
                res.data.shareItPicturesUrl
              ), //类型：String  可有字段  备注：分享海报路径
              imgPathList: get_img_url_to_path_list(
                res.data.imgPathList,
                res.data.imgUrlPath
              ), //类型：String  可有字段  备注：活动照片路径，多图用,隔开
              activityTime: [res.data.stratTime, res.data.endTime], //类型：Number  可有字段  备注：活动时间
              isFree: res.data.isFree, //类型：Number  可有字段  备注：是否免费：0-免费；1-不免费
              price: res.data.price, //类型：Number  可有字段  备注：活动报名费
              activityUserCount: res.data.activityUserCount, //类型：Number  可有字段  备注：活动名额
              categoryId: res.data.categoryId, //类型：String  可有字段  备注：活动类型ID
              isState: res.data.isState, //类型：Number  可有字段  备注：是否上线 0是 1否
              applyTime: [res.data.payStartTime, res.data.payEndTime], //类型：Number  可有字段  备注：报名时间
              circleDot: {
                lng: res.data.longitude,
                lat: res.data.latitude,
                address: res.data.address //类型：String  必有字段  备注：活动地址
              } // 设置地图默认的经纬度为北京市
            }
            this.get_category_list_data() // 请求活动分类 下拉列表数据
            this.drawer = true
            this.title = '修改活动信息'
            this.add = 1
            this.$nextTick(() => {})
          })
          .catch(() => {})
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /**
     * @memo 保存修改方法
     * 根据有无id来判断调用的接口是新增还是修改
     */
    async save_update_data() {
      // 分享图
      let shareItPictures =
        this.addForm.shareItPictures.length > 0
          ? this.addForm.shareItPictures[0].imgUrl
          : ''
      // 封面图
      let pictures =
        this.addForm.pictures.length > 0 ? this.addForm.pictures[0].imgUrl : ''
      // 活动图
      let imgPathList = ''
      if (this.addForm.imgPathList.length > 0) {
        this.addForm.imgPathList.forEach(item => {
          imgPathList += item.imgUrl + ','
        })
      }
      let param = {
        id: this.addForm.id ? this.addForm.id : '', //类型：String  必有字段  备注：id
        shareItPictures: shareItPictures, //类型：String  必有字段  备注：分享海报路径（短）
        sort: Number(this.addForm.sort), //类型：Number  必有字段  备注：排序 数值越大越靠前
        name: this.addForm.name, //类型：String  必有字段  备注：活动名称
        isState: this.addForm.isState, //类型：Number  必有字段  备注：是否上线 0是 1否
        pictures: pictures, //类型：String  必有字段  备注：活动封面路径（短）
        imgPathList: imgPathList, //类型：String  必有字段  备注：活动照片路径（短），多图用,隔开
        activityUserCount: Number(this.addForm.activityUserCount), //类型：Number  必有字段  备注：活动名额
        categoryId: this.addForm.categoryId, //类型：String  必有字段  备注：活动类型ID
        payStartTime: Number(this.addForm.applyTime[0]), //类型：Number  必有字段  备注：报名开始时间
        payEndTime: Number(this.addForm.applyTime[1]), //类型：Number  必有字段  备注：报名结束时间
        stratTime: Number(this.addForm.activityTime[0]), //类型：Number  必有字段  备注：活动开始时间
        endTime: Number(this.addForm.activityTime[1]), //类型：Number  必有字段  备注：活动结束时间
        bookingInformation: this.addForm.bookingInformation, //类型：String  必有字段  备注：活动须知
        content: this.addForm.content, //类型：String  必有字段  备注： 活动详情内容(富文本)
        address: this.addForm.circleDot.address, //类型：String  必有字段  备注：活动地址
        latitude: this.addForm.circleDot.lat, //类型：String  必有字段  备注：纬度
        longitude: this.addForm.circleDot.lng, //类型：String  必有字段  备注：经度
        isFree: this.addForm.isFree, //类型：Number  必有字段  备注：是否免费：0-免费；1-不免费
        price: this.addForm.price //类型：Number  必有字段  备注：活动报名费
      }
      if (!this.addForm.id) {
        await activity_save_path(param)
          .then(res => {
            //添加成功
            this.$message.success(res.message)
            //添加成功返回列表页
            this.clear_drawer()
            this.get_tabel_data(1, this.searchForm.pageSize)
          })
          .catch(() => {})
      } else if (this.addForm.id) {
        await activity_update_path(param)
          .then(res => {
            // 修改成功
            this.$message.success(res.message)
            // 修改成功返回列表页
            this.clear_drawer()
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => {})
      }
    },

    /**
     * @memo 提交按钮
     */
    submit_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_update_data()
        } else {
          // 提交失败
          return false
        }
      })
    },

    /*********************** 删除 操作 ********************************* */
    /**
     * @memo 删除接口
     */
    async delete_data(id) {
      const res = await activity_delete_path({
        ids: id
      })
      return res
    },

    /**
     * @memo 删除按钮
     */
    delete_btn(id) {
      if (id) {
        this.$confirm(
          '【报名中】或【进行中】的活动删除后用户将无法照找到该活动，请确认删除操作。',
          '操作提示',
          {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
          }
        )
          .then(() => {
            this.delete_data(id)
              .then(res => {
                this.$message.success(res.message)
                this.get_tabel_data(
                  this.searchForm.pageNum,
                  this.searchForm.pageSize
                )
              })
              .catch(() => {})
          })
          .catch(() => {})
      } else {
        this.$message.error('请选择需要删除的内容!')
      }
    },

    /********************** 关闭抽屉 操作 ******************************* */
    /**
     * @memo 抽屉关闭前的回调
     */
    handle_close() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => {})
    },

    /**
     * @memo  点击取消按钮，关闭抽屉
     */
    close_drawer() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => {})
    },

    /**
     * @memo 关闭抽屉
     */
    clear_drawer() {
      this.drawer = false
      this.addForm = {
        id: '', //类型：String  必有字段  备注：编号
        name: '', //类型：String  可有字段  备注：活动名称
        msg: '', //类型：String  可有字段  备注：活动简介
        content: '', //类型：String  可有字段  备注：活动描述
        bookingInformation: '', //类型：String  可有字段  备注：预定须知
        pictures: [], //类型：String  可有字段  备注：活动封面路径（短）
        shareItPictures: [], //类型：String  可有字段  备注：分享海报路径（短）
        imgPathList: [], //图片存放路径（短），多图用,隔开
        activityTime: [], //类型：Number  可有字段  备注：活动时间
        isFree: 0, //类型：Number  可有字段  备注：是否免费：0-免费；1-不免费
        price: '', //类型：Number  可有字段  备注：活动报名费
        activityUserCount: '', //类型：Number  可有字段  备注：活动名额
        categoryId: '', //类型：String  可有字段  备注：活动类型ID
        isState: 0, //类型：Number  可有字段  备注：是否上线 0是 1否
        applyTime: [], //类型：Number  可有字段  备注：报名时间
        circleDot: {
          lng: '',
          lat: '',
          address: '' //类型：String  必有字段  备注：活动地址
        } // 设置地图默认的经纬度为北京市
      }
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /*********************** 列表judge 操作 *************************** */
    /**
     * @memo 分类 judge
     */
    cateJudge(cateId) {
      let res = this.categoryList.filter(item => {
        return item.id == cateId
      })
      return res.length > 0 ? res[0].cateName : '无数据'
    },

    /**
     * @memo 状态
     */
    isStateJudge(state) {
      if (state == 0) {
        return '启用'
      } else if (state == 1) {
        return '停用'
      }
    },

    /**
     * @memo 报名状态
     */
    applyStateJudge(applyState) {
      if (applyState == 0) {
        return '启用'
      } else if (applyState == 1) {
        return '停用'
      }
    },

    /**
     * @memo 活动 状态
     */
    activityStateJudge(activityState) {
      if (activityState == 0) {
        return '报名中'
      } else if (activityState == 1) {
        return '进行中'
      } else if (activityState == 2) {
        return '已结束'
      } else if (activityState == 3) {
        return '未开始'
      }
    },

    /**
     * @memo  确认 状态
     */
    confirmStateJudge(confirmState) {
      if (confirmState == 0) {
        return '已确认'
      } else if (confirmState == 1) {
        return '待确认'
      }
    },

    /**
     * @memo  年龄
     */
    applySexJudge(applySex) {
      if (applySex == 0) {
        return '男'
      } else if (applySex == 1) {
        return '女'
      }
    }
  }
}
