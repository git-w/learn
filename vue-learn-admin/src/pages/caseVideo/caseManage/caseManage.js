/**
 * @description: 视频案例-内容管理
 * @author: leroy
 * @Compile：2021-12-03 16：00
 * @update:lx(2021-12-03 16：00)
 */

import {
    case_save_path,  // 添加
    case_update_path,  // 修改
    case_delete_path,  // 删除
    case_list_path,  // 列表
    case_info_path,  // 详情
    case_update_state_path,  // 修改状态
} from '../../../api/caseVideo'
import {
    date_format
} from '../../../utils/date/dateFormat'

import videoUpload from '@/components/VideoUploads/VideoUploads.vue'
import { upload_videos_data, getObjectURL } from '../../../utils/file'

import { get_img_url_to_path_list } from '@/utils/img/imageMethod.js'
export default {
    components: {
        videoUpload
    },
    data() {
        // 上传案例封面验证
        let isUrlPath = (rule, value, callback) => {
            let _that = this
            // 判断案例封面是否有值
            if (_that.addForm.theCover.length > 0) {
                callback()
            } else {
                // 案例封面没有值,提示请上传图片
                return callback(new Error('请上传图片'))
            }
        }

        // 视频或URL地址
        let isVideoPath = (rule, value, callback) => {
            // 判断视频案例是否有值
            if (this.addForm.url != null) {
                callback()
            } else {
                // 视频案例没有值,提示请上传视频
                return callback(new Error('请上传视频/输入URL'))
            }
        }
        return {
            ids: '',
            date_format,

            // 内容管理检索字段
            searchForm: {
                name: '',  //类型：String  必有字段  备注：标题
            },

            tableData: [], // 列表表单数据


            drawer: false, //控制抽屉的开合
            title: '', //控制抽屉的头部
            add: 1, //判断抽屉显示 添加修改 - 1、详情 - 2

            videoList: [], // 视频暂存数组
            accept: 'video/*',
            // 内容管理 表单数据
            addForm: {
                id: '',
                theCover: [],  //类型：String  必有字段  备注：案例封面路径（短）
                name: '',  //类型：String  必有字段  备注：标题
                type: 1, //类型：String  必有字段  备注：类型
                url: '',  //类型：String  必有字段  备注：视频案例路径
                isState: '',  //类型：String  必有字段  备注：状态
            },

            videoDialog: false,  // 控制视频案例弹窗的开合
            // 视频案例 表单数据
            videoForm: {
                videoUrl: '',  //类型：String  必有字段  备注：视频案例路径
            },

            // 内容管理 表单校验
            rules: {
                // 案例封面
                theCover: [{
                    required: true,
                    validator: isUrlPath
                }],
                // 案例标题
                name: [{
                    required: true,
                    message: '请输入标题',
                    trigger: 'blur'
                }],
                // 上传方式
                type: [{
                    required: true,
                    message: '请选择上传方式',
                    trigger: 'change'
                }],
                // 视频或URL地址
                url: [{
                    required: true,
                    validator: isVideoPath
                }]
            }
        }
    },
    mounted() {
        this.to_load()
    },

    methods: {
        /**
         * @memo 进入此页面的加载
         */
        to_load() {
            this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
        },

        /**
         * @memo 检索重置按钮
         */
        reset_btn() {
            this.searchForm.name = '' // 标题
            this.get_tabel_data(1, this.searchForm.pageSize) //获取列表
        },

        /**
         * @memo 内容管理 获取列表
         */
        async get_tabel_data(pageNum, pageSize) {
            const param = {
                pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
                pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
                name: this.searchForm.name, //类型：String  必有字段  备注：标题
            }
            await case_list_path(param)
                .then(res => {
                    this.tableData = res.data.resultList
                    this.searchForm.totals = res.data.totalRows
                    this.searchForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchForm.pageNum > 1) {
                        if (res.data.resultList.length === 0) {
                            this.get_tabel_data(
                                this.searchForm.pageNum - 1,
                                this.searchForm.pageSize
                            )
                        }
                    } else if (this.searchForm.pageNum === 0) {
                        return this.$message.warning('没有更多数据')
                    }
                }).catch(() => { })
        },

        /**
         * @memo 下拉框选中的按钮的操作  更多操作
         */
        handle_command(command) {
            if (command.button == 'delete_btn') {
                //删除
                this.delete_btn(command.row)
            } else if (command == 'delete_btn') {
                //多选删除
                this.delete_btn(this.ids)
            } else if (command.button == 'change_btn') {
                //修改
                this.change_btn(command.row)
            } else if (command.button == 'info_btn') {
                //详情
                this.info_btn(command.row)
            } else if (command == 'more_disable') {
                //多选 停用
                this.update_state_data(this.ids, 1)
            } else if (command == 'more_enabled') {
                //多选 启用
                this.update_state_data(this.ids, 0)
            } else if (command.button === 'enabled_btn') {
                //启用
                this.update_state_data(command.row, 0)
            } else if (command.button === 'disable_btn') {
                //禁用
                this.update_state_data(command.row, 1)
            }
        },

        /**
         * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
         */
        compose_value(item, row) {
            return {
                button: item,
                row: row
            }
        },

        /**
         * @memo 查看视频案例
         */
        infoVideo_btn(videoUrl) {
            this.videoDialog = true
            this.videoForm = {
                videoUrl: videoUrl
            }
            // 关闭表单验证
            this.$nextTick(() => {
                this.$refs.videoForm.clearValidate()
            })
        },

        /**
         * @memo 关闭 观看视频 弹窗
         */
        handle_close_dialog() {
            this.videoDialog = false
            this.videoForm = {
                videoUrl: ''
            }
            // 关闭表单验证
            this.$nextTick(() => {
                this.$refs.videoForm.clearValidate()
            })
        },


        /************************ 添加、修改 操作 ****************************** */
        /**
         * @memo 选择文件 方法
         */
        changeFile(files) {
            this.addForm.url = getObjectURL(files[0])
            for (let i in files) {
                if (files[i].size) {
                    this.videoList = []
                    this.videoList.push(files[i])
                }
            }
        },

        upload_videos(file) {
            if (this.videoList.length > 0) {
                return new Promise(function (resolve, reject) {
                    upload_videos_data(file)
                        .then(ret => {
                            resolve(ret.imgPathUrl)
                        })
                        .catch(() => {
                            reject('上传失败')
                        })
                })
            } else {
                return ''
            }
        },

        /**
         * @memo 添加-跳往添加界面
         */
        add_btn() {
            this.drawer = true
            this.add = 1
            this.title = '添加案例信息'
            this.addForm = {
                id: '',
                theCover: [],  //类型：String  必有字段  备注：案例封面路径（短）
                name: '',  //类型：String  必有字段  备注：标题
                type: 1, //类型：String  必有字段  备注：类型
                url: '',  //类型：String  必有字段  备注：视频案例路径
                isState: 0,  //类型：String  必有字段  备注：状态
            }
            // 关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /**
         * @memo  详情接口
         */
        async get_info_data(id) {
            const res = await case_info_path({
                id: id
            })
            return res
        },

        /**
         * @memo 修改 按钮
         */
        change_btn(id) {
            if (id) {
                this.get_info_data(id)
                    .then(res => {
                        this.addForm = {
                            id: res.data.id,
                            theCover: get_img_url_to_path_list(
                                res.data.theCover,
                                res.data.imgPathUrl
                            ),  //类型：String  必有字段  备注：案例封面路径（短）
                            name: res.data.name,  //类型：String  必有字段  备注：标题
                            url: res.data.url, //类型：String  可有字段  备注：视频存放路径
                            isState: res.data.isState,  //类型：String  必有字段  备注：状态
                        }
                        this.drawer = true
                        this.add = 1
                        this.title = '修改会员卡信息'
                    })
                    .catch(() => { })
            } else {
                this.$message.error('数据错误请刷新页面')
            }
        },

        /**
         * @memo 保存修改方法
         * 根据有无id来判断调用的接口是新增还是修改
         */
        async save_update_data() {
            // 视频封面
            let imgUrl =
                this.addForm.theCover.length > 0 ? this.addForm.theCover[0].imgUrl : ''
            let param = {
                id: this.addForm.id ? this.addForm.id : '',
                theCover: imgUrl,  //类型：String  必有字段  备注：会员卡封面路径（短）
                name: this.addForm.name,  //类型：String  必有字段  备注：标题
                url: this.addForm.url,  //类型：String  必有字段  备注：视频案例路径
                isState: this.addForm.isState,  //类型：String  必有字段  备注：状态
            }
            if (!this.addForm.id) {
                await case_save_path(param)
                    .then(res => {
                        //添加成功
                        this.$message.success(res.message)
                        //添加成功返回列表页
                        this.clear_drawer()
                        this.get_tabel_data(1, this.searchForm.pageSize)
                    })
            } else if (this.addForm.id) {
                await case_update_path(param)
                    .then(res => {
                        //添加成功
                        this.$message.success(res.message)
                        //添加成功返回列表页
                        this.clear_drawer()
                        this.get_tabel_data(1, this.searchForm.pageSize)
                    })
            }
        },

        /**
         * @memo 提交按钮
         */
        submit_btn(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    if (!this.addForm.id && this.addForm.type == 1) {
                        Promise.all([this.upload_videos(this.videoList[0])])
                            .then(res => {
                                if (res[0]) {
                                    this.addForm.url = res[0]
                                }
                                this.save_update_data()
                            })
                            .catch(() => { })
                    } else if (!this.addForm.id && this.addForm.type == 0) {
                        this.save_update_data()
                    } else if (this.addForm.id) {
                        Promise.all([this.upload_videos(this.videoList[0])])
                            .then(res => {
                                if (res[0]) {
                                    this.addForm.url = res[0]
                                }
                                this.save_update_data()
                            })
                            .catch(() => { })
                    }
                } else {
                    // 提交失败
                    return false
                }
            })
        },

        /********************** 修改状态 操作 ********************************* */
        /**
         * @memo  修改 状态接口
         * @param id  //类型：String  必有字段  备注：编号
         * @param isStatus  //状态 0 正常 1 冻结
         */
        async update_state_data(id, isState) {
            if (id) {
                const res = await case_update_state_path({
                    ids: id,
                    isState: isState
                })
                this.$message.success(res.message)
                this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
            } else {
                this.$message.error('请选择需要修改状态的内容!')
            }
        },

        /*********************** 删除 操作 ********************************* */
        /**
         * @memo 删除接口
         */
        async delete_data(id) {
            const res = await case_delete_path({
                ids: id
            })
            return res
        },

        /**
         * @memo 删除按钮
         */
        delete_btn(id) {
            if (id) {
                this.$confirm('是否确定删除?', '操作提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                })
                    .then(() => {
                        this.delete_data(id)
                            .then(res => {
                                this.$message.success(res.message)
                                this.get_tabel_data(
                                    this.searchForm.pageNum,
                                    this.searchForm.pageSize
                                )
                            })
                            .catch(() => { })
                    })
                    .catch(() => { })
            } else {
                this.$message.error('请选择需要删除的内容!')
            }
        },

        /********************** 关闭抽屉 操作 ******************************* */
        /**
         * @memo 抽屉关闭前的回调
         */
        handle_close() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },

        /**
         * @memo  点击取消按钮，关闭抽屉
         */
        close_drawer() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },
        /**
         * @memo 关闭抽屉
         */
        clear_drawer() {
            this.drawer = false
            this.addForm = {
                id: '',
                imgUrl: [],  //类型：String  必有字段  备注：案例封面路径（短）
                title: '',  //类型：String  必有字段  备注：标题
                type: 1, //类型：String  必有字段  备注：类型
                videoUrl: '',  //类型：String  必有字段  备注：视频案例路径
                isState: '',  //类型：String  必有字段  备注：状态
            }
            //关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },


        /*********************** 列表judge 操作 *************************** */
        /**
         * @memo 是否上线  状态
         */
        isStateJudge(isState) {
            if (isState == 0) {
                return '启用'
            } else if (isState == 1) {
                return '停用'
            }
        },

    }
}
