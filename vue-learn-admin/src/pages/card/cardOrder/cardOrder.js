/**
 * @description: 会员卡管理-订单管理
 * @author: leroy
 * @Compile：2021-12-08 14：30
 * @update:lx(2021-12-08 14：30)
 */
import {
    card_order_list_path,  // 列表
    card_order_info_path,  // 详情
    card_member_list_path,  // 使用记录 列表
} from '../../../api/card'
import { date_format, format_date, format_date_ymd } from '@/utils/date/dateFormat'

export default {
    data() {
        return {
            date_format,  //时间处理
            format_date,
            format_date_ymd,

            // 订单管理 检索字段
            searchForm: {
                createDate: [],  //类型：Number  可有字段  备注：下单时间
                orderNum: '',  // 类型：String  可有字段  备注：订单号
                nickName: '',   // 类型：String  可有字段  备注：用户昵称
            },
            tableData: [], // 列表表单数据

            // 使用记录 检索字段
            searchMemberForm: {
                pageNum: 1,  //类型：Number  必有字段  备注：页码 默认第一页
                pageSize: 10,  //类型：Number  必有字段  备注：条码 每页默认十条
                totals: 0,
            },
            tableMemberData: [],  // 使用记录 列表数组

            isTypeList: [{
                value: 0,
                label: '红娘牵线'
            },{
                value: 1,
                label: '活动'
            }],

            title: '', // 控制抽屜的头部
            drawer: false, // 控制抽屜的开合
            add: 1,
            activeCollapse: [],  // 详情折叠面板 默认打开

            addForm: {
                cancelTime: '',
                createOperatorId: '',
                createOperatorName: '',
                createTime: '',
                deliverOperatorId: '',
                deliverTime: '',
                finishOperatorId: '',
                finishTime: '',
                headimgurl: '',
                id: '',
                memo: '',
                modilePhone: '',
                nickName: '',
                orderNum: '',
                orderRemark: '',
                orderStatus: '',
                orderType: '',
                outStoreTime: '',
                payMode: '',
                payOperatorId: '',
                payStatus: '',
                payTime: '',
                realCost: '',
                remark: '',
                totalMoney: '',
                updateOperatorId: '',
                updateOperatorName: '',
                updateTime: '',
                userId: '',
                userMemberCardVO: {
                    cardId: '',
                    cardNum: '',
                    createOperatorId: '',
                    createOperatorName: '',
                    createTime: '',
                    endTime: '',
                    id: '',
                    isStatus: '',
                    memberCardVO: {
                        content: '',
                        createTime: '',
                        endTime: '',
                        extraParams1: '',
                        id: '',
                        imgUrl: '',
                        imgUrlPath: '',
                        introduction: '',
                        inventoryCount: '',
                        isEnabled: '',
                        isInfinite: '',
                        level: '',
                        memberCardConfigVoList: '',
                        memo: '',
                        name: '',
                        price: '',
                        remark: '',
                        startTime: '',
                        subtitle: '',
                        updateTime: '',
                        validTime: '',
                        validTimeUnit: '',

                    },
                    memo: '',
                    orderId: '',
                    orderNum: '',
                    rightsVOList: [],
                    startTime: '',
                    updateOperatorId: '',
                    updateOperatorName: '',
                    updateTime: '',
                    userId: '',
                }
            },  // 订单管理 表单字段
        }
    },
    mounted() {
        this.to_load()
    },
    methods: {
        /**
         * @memo 进入此页面的加载
         */
        to_load() {
            this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
        },

        /**
         * @memo 检索重置按钮
         */
        reset_btn() {
            this.searchForm.createDate = []  //类型：Number  可有字段  备注：下单时间
            this.searchForm.orderNum = ''  // 类型：String  可有字段  备注：订单号
            this.searchForm.nickName = ''   // 类型：String  可有字段  备注：用户昵称
            this.get_tabel_data(1, this.searchForm.pageSize) //获取列表
        },

        /**
         * @memo 订单管理 获取列表
         */
        async get_tabel_data(pageNum, pageSize) {
            const param = {
                pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
                pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
                orderNum: this.searchForm.orderNum,  // 类型：String  可有字段  备注：订单号
                nickName: this.searchForm.nickName,  // 类型：String  可有字段  备注：用户昵称
                createTimeFrom: this.searchForm.createDate != null ? this.searchForm.createDate[0] : '',  //类型：Number  可有字段  备注：下单时间（始）
                createTimeTo: this.searchForm.createDate != null ? this.searchForm.createDate[1] : '',  //类型：Number  可有字段  备注：下单时间（终）
            }
            await card_order_list_path(param)
                .then(res => {
                    this.tableData = res.data.resultList
                    this.searchForm.totals = res.data.totalRows
                    this.searchForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchForm.pageNum > 1) {
                        if (res.data.resultList.length === 0) {
                            this.get_tabel_data(
                                this.searchForm.pageNum - 1,
                                this.searchForm.pageSize
                            )
                        }
                    } else if (this.searchForm.pageNum === 0) {
                        return this.$message.warning('没有更多数据')
                    }
                }).catch(() => { })
        },

        /**
         * @memo 下拉框选中的按钮的操作  更多操作
         */
        handle_command(command) {
            if (command.button == 'info_btn') {
                //详情
                this.info_btn(command.row)
            }
        },

        /**
         * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
         */
        compose_value(item, row) {
            return {
                button: item,
                row: row
            }
        },



        /******************************** 详情 操作 ************************* */
        /**
         * @memo 详情 接口
         */
        async get_info_data(id) {
            const res = await card_order_info_path({
                id: id
            })
            return res
        },

        /**
         * @memo  跳转到订单详情页面
         */
        info_btn(id) {
            if (id) {
                this.get_info_data(id)
                    .then(res => {
                        this.addForm = {
                            cancelTime: res.data.cancelTime,
                            createOperatorId: res.data.createOperatorId,
                            createOperatorName: res.data.createOperatorName,
                            createTime: res.data.createTime,
                            deliverOperatorId: res.data.deliverOperatorId,
                            deliverTime: res.data.deliverTime,
                            finishOperatorId: res.data.finishOperatorId,
                            finishTime: res.data.finishTime,
                            headimgurl: res.data.headimgurl,
                            id: res.data.id,
                            memo: res.data.memo,
                            modilePhone: res.data.modilePhone,
                            nickName: res.data.nickName,
                            orderNum: res.data.orderNum,
                            orderRemark: res.data.orderRemark,
                            orderStatus: res.data.orderStatus,
                            orderType: res.data.orderType,
                            outStoreTime: res.data.outStoreTime,
                            payMode: res.data.payMode,
                            payOperatorId: res.data.payOperatorId,
                            payStatus: res.data.payStatus,
                            payTime: res.data.payTime,
                            realCost: res.data.realCost,
                            remark: res.data.remark,
                            totalMoney: res.data.totalMoney,
                            updateOperatorId: res.data.updateOperatorId,
                            updateOperatorName: res.data.updateOperatorName,
                            updateTime: res.data.updateTime,
                            userId: res.data.userId,
                            userMemberCardVO: {
                                cardId: res.data.userMemberCardVO.cardId,
                                cardNum: res.data.userMemberCardVO.cardNum,
                                createOperatorId: res.data.userMemberCardVO.createOperatorId,
                                createOperatorName: res.data.userMemberCardVO.createOperatorName,
                                createTime: res.data.userMemberCardVO.createTime,
                                endTime: res.data.userMemberCardVO.endTime,
                                id: res.data.userMemberCardVO.id,
                                isStatus: res.data.userMemberCardVO.isStatus,
                                memberCardVO: {
                                    content: res.data.userMemberCardVO.memberCardVO.content,
                                    createTime: res.data.userMemberCardVO.memberCardVO.createTime,
                                    endTime: res.data.userMemberCardVO.memberCardVO.endTime,
                                    extraParams1: res.data.userMemberCardVO.memberCardVO.extraParams1,
                                    getNum: res.data.userMemberCardVO.memberCardVO.getNum,
                                    id: res.data.userMemberCardVO.memberCardVO.id,
                                    imgUrl: res.data.userMemberCardVO.memberCardVO.imgUrl,
                                    imgUrlPath: res.data.userMemberCardVO.memberCardVO.imgUrlPath,
                                    introduction: res.data.userMemberCardVO.memberCardVO.introduction,
                                    inventoryCount: res.data.userMemberCardVO.memberCardVO.inventoryCount,
                                    isEnabled: res.data.userMemberCardVO.memberCardVO.isEnabled,
                                    isFree: res.data.userMemberCardVO.memberCardVO.isFree,
                                    isInfinite: res.data.userMemberCardVO.memberCardVO.isInfinite,
                                    level: res.data.userMemberCardVO.memberCardVO.level,
                                    memberCardConfigVoList: res.data.userMemberCardVO.memberCardVO.memberCardConfigVoList,
                                    memo: res.data.userMemberCardVO.memberCardVO.memo,
                                    name: res.data.userMemberCardVO.memberCardVO.name,
                                    price: res.data.userMemberCardVO.memberCardVO.price,
                                    remark: res.data.userMemberCardVO.memberCardVO.remark,
                                    startTime: res.data.userMemberCardVO.memberCardVO.startTime,
                                    subtitle: res.data.userMemberCardVO.memberCardVO.subtitle,
                                    updateTime: res.data.userMemberCardVO.memberCardVO.updateTime,
                                    validTime: res.data.userMemberCardVO.memberCardVO.validTime,
                                    validTimeUnit: res.data.userMemberCardVO.memberCardVO.validTimeUnit,
                                },
                                memo: res.data.userMemberCardVO.memo,
                                orderId: res.data.userMemberCardVO.orderId,
                                orderNum: res.data.userMemberCardVO.orderNum,
                                rightsVOList: res.data.userMemberCardVO.rightsVOList,
                                startTime: res.data.userMemberCardVO.startTime,
                                updateOperatorId: res.data.userMemberCardVO.updateOperatorId,
                                updateOperatorName: res.data.userMemberCardVO.updateOperatorName,
                                updateTime: res.data.userMemberCardVO.updateTime,
                                userId: res.data.userMemberCardVO.userId,
                            }
                        }
                        this.get_member_tabel_data(this.searchMemberForm.pageNum, this.searchMemberForm.pageSize)
                        this.activeCollapse = ['1','2','3'],
                        this.drawer = true
                        this.title = '订单详情'
                    }).catch(() => { })
            } else {
                this.$message.error('数据错误请刷新页面')
            }
        },


        /********************************* 使用记录 操作 ************************************ */
        /**
         * @memo 使用记录 获取列表
         */
        async get_member_tabel_data(pageNum, pageSize) {
            const param = {
                pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
                pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
                midCardId: this.addForm.userMemberCardVO.id,  // 类型：String  可有字段  备注：关联会员卡id
            }
            await card_member_list_path(param)
                .then(res => {
                    this.tableMemberData = res.data.resultList
                    this.searchMemberForm.totals = res.data.totalRows
                    this.searchMemberForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchMemberForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchMemberForm.pageNum > 1) {
                        if (res.data.resultList.length === 0) {
                            this.get_member_tabel_data(
                                this.searchMemberForm.pageNum - 1,
                                this.searchMemberForm.pageSize
                            )
                        }
                    } else if (this.searchMemberForm.pageNum === 0) {
                        return this.$message.warning('没有更多数据')
                    }
                }).catch(() => { })
        },

        /********************** 关闭抽屉 操作 ******************************* */
        /**
         * @memo 抽屉关闭前的回调
         */
        handle_close() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },

        /**
         * @memo  点击取消按钮，关闭抽屉
         */
        close_drawer() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },
        /**
         * @memo 关闭抽屉
         */
        clear_drawer() {
            this.drawer = false
            this.addForm = {
                cancelTime: '',
                createOperatorId: '',
                createOperatorName: '',
                createTime: '',
                deliverOperatorId: '',
                deliverTime: '',
                finishOperatorId: '',
                finishTime: '',
                headimgurl: '',
                id: '',
                memo: '',
                modilePhone: '',
                nickName: '',
                orderNum: '',
                orderRemark: '',
                orderStatus: '',
                orderType: '',
                outStoreTime: '',
                payMode: '',
                payOperatorId: '',
                payStatus: '',
                payTime: '',
                realCost: '',
                remark: '',
                totalMoney: '',
                updateOperatorId: '',
                updateOperatorName: '',
                updateTime: '',
                userId: '',
                userMemberCardVO: {
                    cardId: '',
                    cardNum: '',
                    createOperatorId: '',
                    createOperatorName: '',
                    createTime: '',
                    endTime: '',
                    id: '',
                    isStatus: '',
                    memberCardVO: {},
                    memo: '',
                    orderId: '',
                    orderNum: '',
                    rightsVOList: [],
                    startTime: '',
                    updateOperatorId: '',
                    updateOperatorName: '',
                    updateTime: '',
                    userId: '',
                }
            }
            //关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /****************************** 列表judge ****************************** */
        /**
         * @memo 订单状态 judge
         */
        orderStatusJudge(orderStatus) {
            if (orderStatus == 0) {
                return '取消订单'
            } else if (orderStatus == 1) {
                return '待支付'
            } else if (orderStatus == 2) {
                return '支付/待使用'
            } else if (orderStatus == 5) {
                return '完成'
            } else if (orderStatus == 6) {
                return '申请退款'
            } else if (orderStatus == 7) {
                return '退款中'
            } else if (orderStatus == 8) {
                return '退款完成'
            } else if (orderStatus == 9) {
                return '超时未支付'
            }
        },

        /**
         * @memo 类型
         */
        isTypejudge(isType) {
            let res = this.isTypeList.filter(item => {
                return item.value == isType
            })
            return res.length > 0 ? res[0].label : '无数据'
        },

        /**************************** 使用记录 列表分页 操作 ************************************ */
        /**
         * @memo 列表分页 相关方法
         */
        change_size_member_btn(val) {
            // 分页加载常见问题列表加载
            this.get_member_tabel_data(1, val)
        },
        change_current_member_btn(val) {
            // 分页加载常见问题列表加载
            this.get_member_tabel_data(val, this.searchForm.pageSize)
        },
    }
}
