/**
 * @description: 会员卡管理-会员卡管理-配置管理
 * @author: leroy
 * @Compile：2021-12-06 18：00
 * @update:lx(2021-12-06 18：00)
 */

import { date_format } from '@/utils/date/dateFormat' //时间格式化
import {
    card_config_save_path,  // 添加
    card_config_update_path,  // 修改
    card_config_delete_path,  // 删除
    card_config_list_path,  // 列表
    card_config_info_path,  // 详情
    card_config_update_state_path  // 修改状态
} from '../../../../api/card'

import { get_img_url_to_path_list } from '@/utils/img/imageMethod.js'
export default {
    data() {
        // 上传配置ICON验证
        let isConfigPath = (rule, value, callback) => {
            let _that = this
            // 判断配置ICON是否有值
            if (_that.addForm.imgUrl.length > 0) {
                callback()
            } else {
                // 配置ICON没有值,提示请上传图片
                return callback(new Error('请上传图片'))
            }
        }
        return {
            cardId: this.$store.getters['getCardNum'], // 会员卡id
            date_format, //时间处理

            // 会员卡配置检索字段
            searchForm: {
                cardId: '', //类型：String  必有字段  备注：会员卡id
            },

            tableData: [], // 列表表单数据

            drawer: false, //控制抽屉的开合
            title: '', //控制抽屉的头部
            add: 1, //判断抽屉显示 添加修改 - 1、详情 - 2\

            isTypeList: [{
                value: 0,
                label: '红娘牵线'
            },{
                value: 1,
                label: '活动'
            }],

            // 会员卡配置 表单数据
            addForm: {
                id: '',
                cardId: '',  //类型：String  必有字段  备注：关联会员卡ID
                isType: '',  //类型：String  必有字段  备注：关联类型，参数为空
                othersId: '',  //类型：String  必有字段  备注：关联ID
                imgUrl: '',  //类型：String  必有字段  备注：图片地址
                count: '',  //类型：String  必有字段  备注：数量
                timeInterval: '',  //类型：Number  必有字段  备注：间隔天数
                validTime: '',  //类型：Number  必有字段  备注：有效天数
                isEnabled: 0,  //类型：Number  必有字段  备注：是否启用 0-是 1-否
                remark: '',  //类型：Number  必有字段  备注：备注
                memo: '',  //类型：String  必有字段  备注：备注
            },

            // 会员卡配置 表单校验
            rules: {
                // 配置ICON
                imgUrl: [{
                    required: true,
                    validator: isConfigPath
                }],

                // 类型
                isType: [{
                    required: true,
                    message: '请选择类型',
                    trigger: 'change'
                }],

                // 数量
                count: [{
                    required: true,
                    message: '请输入数量',
                    trigger: 'blur'
                }, {
                    pattern: /^[0-9]+[0-9]*$/,
                    message: '请输入正整数'
                }]
            }
        }
    },
    mounted() {
        this.to_load()
    },
    methods: {
        /**
         * @memo  返回上一页
         */
        prev() {
            this.$router.go(-1)
        },

        /**
         * @memo 进入此页面的加载
         */
        to_load() {
            this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
        },

        /**
         * @memo 检索重置按钮
         */
        reset_btn() {
            // this.searchForm.name = '' // 会员卡名称
            this.get_tabel_data(1, this.searchForm.pageSize) //获取列表
        },

        /**
         * @memo 会员卡配置  获取列表
         */
        async get_tabel_data(pageNum, pageSize) {
            const param = {
                pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
                pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
                cardId: this.cardId, //类型：String  必有字段  备注：会员卡id
            }
            await card_config_list_path(param)
                .then(res => {
                    this.tableData = res.data.resultList
                    this.searchForm.totals = res.data.totalRows
                    this.searchForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchForm.pageNum > 1) {
                        if (res.data.resultList.length === 0) {
                            this.get_tabel_data(
                                this.searchForm.pageNum - 1,
                                this.searchForm.pageSize
                            )
                        }
                    } else if (this.searchForm.pageNum === 0) {
                        return this.$message.warning('没有更多数据')
                    }
                }).catch(() => { })
        },

        /**
         * @memo 下拉框选中的按钮的操作  更多操作
         */
        handle_command(command) {
            if (command.button == 'delete_btn') {
                //删除
                this.delete_btn(command.row)
            } else if (command == 'delete_btn') {
                //多选删除
                this.delete_btn(this.ids)
            } else if (command.button == 'change_btn') {
                //修改
                this.change_btn(command.row)
            } else if (command == 'more_disable') {
                //多选 停用
                this.update_state_data(this.ids, 1)
            } else if (command == 'more_enabled') {
                //多选 启用
                this.update_state_data(this.ids, 0)
            } else if (command.button === 'enabled_btn') {
                //启用
                this.update_state_data(command.row, '0')
            } else if (command.button === 'disable_btn') {
                //禁用
                this.update_state_data(command.row, '1')
            }
        },

        /**
         * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
         */
        compose_value(item, row) {
            return {
                button: item,
                row: row
            }
        },


        /************************ 添加、修改 操作 ****************************** */
        /**
         * @memo 添加-跳往添加界面
         */
        add_btn() {
            this.drawer = true
            this.add = 1
            this.title = '添加配置'
            this.addForm = {
                id: '',
                cardId: this.cardId,  //类型：String  必有字段  备注：关联会员卡ID
                isType: '',  //类型：String  必有字段  备注：关联类型，参数为空
                imgUrl: [],  //类型：String  必有字段  备注：图片地址
                count: '',  //类型：String  必有字段  备注：数量
                isEnabled: 0,  //类型：Number  必有字段  备注：是否启用 0-是 1-否
                remark: '',  //类型：Number  必有字段  备注：备注
                memo: '',  //类型：String  必有字段  备注：备注
            }
            // 关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /**
         * @memo 详情接口
         */
        async get_info_data(id) {
            const res = await card_config_info_path({
                id: id,
            })
            return res
        },

        /**
         * @memo 修改 按钮
         */
        change_btn(id) {
            if (id) {
                this.get_info_data(id)
                    .then(res => {
                        this.addForm = {
                            id: res.data.id,
                            cardId: res.data.cardId,  //类型：String  必有字段  备注：关联会员卡ID
                            isType: res.data.isType,  //类型：String  必有字段  备注：关联类型，参数为空
                            othersId: res.data.othersId,  //类型：String  必有字段  备注：关联ID
                            imgUrl: get_img_url_to_path_list(
                                res.data.imgUrl,
                                res.data.imgUrlPath
                            ),  //类型：String  必有字段  备注：图片地址
                            count: res.data.count,  //类型：String  必有字段  备注：数量
                            timeInterval: res.data.timeInterval,  //类型：Number  必有字段  备注：间隔天数
                            validTime: res.data.validTime,  //类型：Number  必有字段  备注：有效天数
                            isEnabled: res.data.isEnabled,  //类型：Number  必有字段  备注：是否启用 0-是 1-否
                            remark: res.data.remark,  //类型：Number  必有字段  备注：备注
                            memo: res.data.memo,  //类型：String  必有字段  备注：备注
                        }
                        this.drawer = true
                        this.add = 1
                        this.title = '修改配置信息'
                    })
                    .catch(() => { })
            } else {
                this.$message.error('数据错误请刷新页面')
            }
        },

        /**
         * @memo 保存修改方法
         * 根据有无id来判断调用的接口是新增还是修改
         */
        async save_update_data() {
            // 会员卡封面图
            let imgUrl =
                this.addForm.imgUrl.length > 0 ? this.addForm.imgUrl[0].imgUrl : ''
            let param = {
                id: this.addForm.id,
                cardId: this.addForm.cardId,  //类型：String  必有字段  备注：关联会员卡ID
                isType: this.addForm.isType,  //类型：String  必有字段  备注：关联类型，参数为空
                othersId: this.addForm.othersId,  //类型：String  必有字段  备注：关联ID
                imgUrl: imgUrl,  //类型：String  必有字段  备注：图片地址
                count: this.addForm.count,  //类型：String  必有字段  备注：数量
                timeInterval: this.addForm.timeInterval,  //类型：Number  必有字段  备注：间隔天数
                validTime: this.addForm.validTime,  //类型：Number  必有字段  备注：有效天数
                isEnabled: this.addForm.isEnabled,  //类型：Number  必有字段  备注：是否启用 0-是 1-否
                remark: this.addForm.remark,  //类型：Number  必有字段  备注：备注
                memo: this.addForm.memo,  //类型：String  必有字段  备注：备注
            }
            if (!this.addForm.id) {
                await card_config_save_path(param)
                    .then(res => {
                        //添加成功
                        this.$message.success(res.message)
                        //添加成功返回列表页
                        this.clear_drawer()
                        this.get_tabel_data(1, this.searchForm.pageSize)
                    })
            } else if (this.addForm.id) {
                await card_config_update_path(param)
                    .then(res => {
                        //添加成功
                        this.$message.success(res.message)
                        //添加成功返回列表页
                        this.clear_drawer()
                        this.get_tabel_data(1, this.searchForm.pageSize)
                    })
            }
        },

        /**
         * @memo 提交按钮
         */
        submit_btn(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    this.save_update_data()
                } else {
                    // 提交失败
                    return false
                }
            })
        },

        /********************** 修改状态 操作 ********************************* */
        /**
         * @memo  修改 状态接口
         * @param id  //类型：String  必有字段  备注：编号
         * @param isStatus  //状态 0 正常 1 冻结
         */
        async update_state_data(id, isEnabled) {
            if (id) {
                const res = await card_config_update_state_path({
                    ids: id,
                    isEnabled: isEnabled
                })
                this.$message.success(res.message)
                this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
            } else {
                this.$message.error('请选择需要修改状态的内容!')
            }
        },

        /*********************** 删除 操作 ********************************* */
        /**
         * @memo 删除接口
         */
        async delete_data(id) {
            const res = await card_config_delete_path({
                ids: id
            })
            return res
        },

        /**
         * @memo 删除按钮
         */
        delete_btn(id) {
            if (id) {
                this.$confirm('是否确认删除此会员卡配置。', '操作提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                })
                    .then(() => {
                        this.delete_data(id)
                            .then(res => {
                                this.$message.success(res.message)
                                this.get_tabel_data(
                                    this.searchForm.pageNum,
                                    this.searchForm.pageSize
                                )
                            })
                            .catch(() => { })
                    })
                    .catch(() => { })
            } else {
                this.$message.error('请选择需要删除的内容!')
            }
        },


        /********************** 关闭抽屉 操作 ******************************* */
        /**
         * @memo 抽屉关闭前的回调
         */
        handle_close() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },

        /**
         * @memo  点击取消按钮，关闭抽屉
         */
        close_drawer() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },
        /**
         * @memo 关闭抽屉
         */
        clear_drawer() {
            this.drawer = false
            this.addForm = {
                id: '',
                cardId: '',  //类型：String  必有字段  备注：关联会员卡ID
                isType: '',  //类型：String  必有字段  备注：关联类型，参数为空
                othersId: '',  //类型：String  必有字段  备注：关联ID
                imgUrl: [],  //类型：String  必有字段  备注：图片地址
                count: '',  //类型：String  必有字段  备注：数量
                timeInterval: '',  //类型：Number  必有字段  备注：间隔天数
                validTime: '',  //类型：Number  必有字段  备注：有效天数
                isEnabled: '',  //类型：Number  必有字段  备注：是否启用 0-是 1-否
                remark: '',  //类型：Number  必有字段  备注：备注
                memo: '',  //类型：String  必有字段  备注：备注
            }
            //关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },


        /*********************** 列表judge 操作 *************************** */
        /**
         * @memo 状态
         */
        isEnabledJudge(isEnabled) {
            if (isEnabled == 0) {
                return '启用'
            } else if (isEnabled == 1) {
                return '停用'
            }
        },

        /**
         * @memo 类型
         */
        isTypejudge(isType) {
            let res = this.isTypeList.filter(item => {
                return item.value == isType
            })
            return res.length > 0 ? res[0].label : '无数据'
        },

    }
}
