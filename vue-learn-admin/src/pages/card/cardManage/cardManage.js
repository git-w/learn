/**
 * @description: 会员卡管理-会员卡信息管理
 * @author: leroy
 * @Compile：2021-12-02 18：00
 * @update:lx(2021-12-02 18：00)
 */
import {
  card_save_path, // 添加
  card_update_path, // 修改
  card_delete_path, // 删除
  card_list_path, // 列表
  card_info_path, // 详情
  card_update_state_path // 修改状态
} from '../../../api/card'

import {
  date_format,
  format_date,
  format_date_ymd
} from '../../../utils/date/dateFormat'

import { price_rule } from '@/utils/verify/replaceMethods'
import { get_img_url_to_path_list } from '@/utils/img/imageMethod.js'
export default {
  data() {
    // 上传会员卡封面验证
    let isCardPath = (rule, value, callback) => {
      let _that = this
      // 判断会员卡封面是否有值
      if (_that.addForm.imgUrl.length > 0) {
        callback()
      } else {
        // 会员卡封面没有值,提示请上传图片
        return callback(new Error('请上传图片'))
      }
    }
    return {
      ids: '',

      date_format, // 时间处理（时间戳 ---- yyyy-MM-dd hh:mm:ss）
      format_date, // 时间处理（UTC时间格式 --- yyyy-MM-dd hh:mm:ss）
      format_date_ymd, // 时间处理（UTC时间格式 --- yyyy-MM-dd)
      price_rule, // 处理价格的公共方法

      // 会员卡管理检索字段
      searchForm: {
        name: '' //类型：String  必有字段  备注：会员卡名称
      },

      tableData: [], // 列表表单数据

      drawer: false, //控制抽屉的开合
      title: '', //控制抽屉的头部
      add: 1, //判断抽屉显示 添加修改 - 1、详情 - 2
      activeCollapse: [], // 详情折叠面板 默认打开

      // 会员卡管理 表单数据
      addForm: {
        id: '', //类型：String  必有字段  备注：编号
        name: '', //类型：String  必有字段  备注：会员卡名称
        subtitle: '', //类型：String  必有字段  备注：会员卡副标题
        level: '', //类型：String  必有字段  备注：会员级别
        content: '', //类型：String  必有字段  备注：会员卡使用说明
        introduction: '', //类型：String  必有字段  备注：会员卡简介
        price: '', //类型：Number  必有字段  备注：价格
        isInfinite: '', //类型：Number  必有字段  备注：库存限制
        inventoryCount: '', //类型：Number  必有字段  备注：库存数量
        validTimeUnit: '', //类型：Number  必有字段  备注：有效时长单位，1-周，2-月，3-季，4-年
        validTime: '', //类型：Number  必有字段  备注：有效时长(周期)，表示几周，几个月
        imgUrl: [], //类型：String  必有字段  备注：会员卡封面路径（短）
        sellTime: [], //类型：String  必有字段  备注：售卖时效
        isEnabled: '', //类型：String  必有字段  备注：是否启用 0-是 1-否
        isFree: '', //类型：String  必有字段  备注：是否免费领取 0-是 1-否
        getNum: '' //类型：String  必有字段  备注：领取次数
      },

      // 会员卡管理 表单校验
      rules: {
        // 会员卡封面
        imgUrl: [
          {
            required: true,
            validator: isCardPath
          }
        ],
        // 会员卡名称
        name: [
          {
            required: true,
            message: '请输入会员卡名称',
            trigger: 'blur'
          }
        ],
        // 价格
        price: [
          {
            required: true,
            message: '请输入价格',
            trigger: 'change'
          }
        ],
        // 库存限制
        isInfinite: [
          {
            required: true,
            message: '请选择库存限制',
            trigger: 'change'
          }
        ],
        // 库存数量
        inventoryCount: [
          {
            required: true,
            message: '请输入库存数量',
            trigger: 'blur'
          },
          {
            pattern: /^[0-9]+[0-9]*$/,
            message: '请输入正整数'
          }
        ],
        // 有效时长单位
        validTimeUnit: [
          {
            required: true,
            message: '请选择有效时长单位',
            trigger: 'change'
          }
        ],
        // 有效时长
        validTime: [
          {
            required: true,
            message: '请选择有效时长',
            trigger: 'change'
          }
        ],
        // 售卖时效
        sellTime: [
          {
            required: true,
            message: '请选择售卖时效',
            trigger: 'change'
          }
        ],
        // 领取数量
        getNum: [
          {
            required: true,
            message: '请输入领取数量',
            trigger: 'blur'
          },
          {
            pattern: /^[0-9]+[0-9]*$/,
            message: '请输入正整数'
          }
        ]
      }
    }
  },
  mounted() {
    this.to_load() // 进入此页面进行请求
  },
  methods: {
    /**
     * @memo 进入此页面的加载
     */
    to_load() {
      this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
    },

    /**
     * @memo 检索重置按钮
     */
    reset_btn() {
      this.searchForm.name = '' // 会员卡名称
      this.get_tabel_data(1, this.searchForm.pageSize) //获取列表
    },

    /**
     * @memo 获取列表
     */
    async get_tabel_data(pageNum, pageSize) {
      const param = {
        pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
        pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
        name: this.searchForm.name //类型：String  必有字段  备注：会员卡名称
      }
      await card_list_path(param)
        .then(res => {
          this.tableData = res.data.resultList
          this.searchForm.totals = res.data.totalRows
          this.searchForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchForm.pageNum > 1) {
            if (res.data.resultList.length === 0) {
              this.get_tabel_data(
                this.searchForm.pageNum - 1,
                this.searchForm.pageSize
              )
            }
          } else if (this.searchForm.pageNum === 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => {})
    },

    /**
     * @memo 下拉框选中的按钮的操作  更多操作
     */
    handle_command(command) {
      if (command.button == 'delete_btn') {
        //删除
        this.delete_btn(command.row)
      } else if (command == 'delete_btn') {
        //多选删除
        this.delete_btn(this.ids)
      } else if (command.button == 'change_btn') {
        //修改
        this.change_btn(command.row)
      } else if (command.button == 'info_btn') {
        //详情
        this.info_btn(command.row)
      } else if (command == 'more_disable') {
        //多选 停用
        this.update_state_data(this.ids, 1)
      } else if (command == 'more_enabled') {
        //多选 启用
        this.update_state_data(this.ids, 0)
      } else if (command.button === 'enabled_btn') {
        //启用
        this.update_state_data(command.row, '0')
      } else if (command.button === 'disable_btn') {
        //停用
        this.update_state_data(command.row, '1')
      } else if (command.button === 'config_btn') {
        //配置
        this.config_btn(command.row)
      }
    },
    /**
     * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
     */
    compose_value(item, row) {
      return {
        button: item,
        row: row
      }
    },

    /**
     * @memo 配置按钮 - 跳往会员配置管理
     */
    config_btn(row) {
      this.$router.push({
        path: '/card/cardConfig'
      })
      this.$store.commit('setCardNum', row.id)
    },

    /************************ 添加、修改 操作 ****************************** */
    /**
     * @memo 添加-跳往添加界面
     */
    add_btn() {
      this.drawer = true
      this.add = 1
      this.title = '添加会员卡'
      this.addForm = {
        id: '',
        name: '', //类型：String  必有字段  备注：会员卡名称
        subtitle: '', //类型：String  必有字段  备注：会员卡副标题
        level: '', //类型：String  必有字段  备注：会员级别
        content: '', //类型：String  必有字段  备注：会员卡使用说明
        introduction: '', //类型：String  必有字段  备注：会员卡简介
        price: '', //类型：Number  必有字段  备注：价格
        isInfinite: 0, //类型：Number  必有字段  备注：库存限制
        inventoryCount: '', //类型：Number  必有字段  备注：库存数量
        validTimeUnit: 5, //类型：Number  必有字段  备注：有效时长单位，1-周，2-月，3-季，4-年
        validTime: '', //类型：Number  必有字段  备注：有效时长(周期)，表示几周，几个月
        imgUrl: [], //类型：String  必有字段  备注：会员卡封面路径（短）
        sellTime: [], //类型：String  必有字段  备注：售卖时效
        isEnabled: 0, //类型：String  必有字段  备注：是否启用 0-是 1-否
        isFree: 1, //类型：String  必有字段  备注：是否免费领取 0-是 1-否
        getNum: '' //类型：String  必有字段  备注：领取次数
      }
      // 关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /**
     * @memo 详情 接口
     */
    async get_info_data(id) {
      const res = await card_info_path({
        id: id
      })
      return res
    },

    /**
     * @memo 详情 按钮
     */
    info_btn(id) {
      if (id) {
        this.get_info_data(id)
          .then(res => {
            this.addForm = {
              id: res.data.id,
              name: res.data.name, //类型：String  必有字段  备注：会员卡名称
              subtitle: res.data.subtitle, //类型：String  必有字段  备注：会员卡副标题
              level: res.data.level, //类型：String  必有字段  备注：会员级别
              content: res.data.content, //类型：String  必有字段  备注：会员卡使用说明
              introduction: res.data.introduction, //类型：String  必有字段  备注：会员卡简介
              price: res.data.price, //类型：Number  必有字段  备注：价格
              isInfinite: res.data.isInfinite, //类型：Number  必有字段  备注：库存限制
              inventoryCount: res.data.inventoryCount, //类型：Number  必有字段  备注：库存数量
              validTimeUnit: res.data.validTimeUnit, //类型：Number  必有字段  备注：有效时长单位，1-周，2-月，3-季，4-年
              validTime: res.data.validTime, //类型：Number  必有字段  备注：有效时长(周期)，表示几周，几个月
              imgUrl: res.data.imgUrl, //类型：String  必有字段  备注：会员卡封面路径（短）
              imgUrlPath: res.data.imgUrlPath, //类型：String  必有字段  备注：会员卡封面路径（长）
              startTime: res.data.startTime, //类型：String  必有字段  备注：售卖开始时效
              endTime: res.data.endTime, //类型：String  必有字段  备注：售卖结束时效
              isEnabled: res.data.isEnabled, //类型：String  必有字段  备注：是否启用 0-是 1-否
              isFree: res.data.isFree, //类型：String  必有字段  备注：是否免费领取 0-是 1-否
              getNum: res.data.isFree == 0 ? res.data.getNum : '' //类型：String  必有字段  备注：领取次数
            }
            ;(this.activeCollapse = ['1']), (this.drawer = true)
            this.add = 2
            this.title = '会员卡详情'
          })
          .catch(() => {})
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /**
     * @memo 修改 按钮
     */
    change_btn(id) {
      if (id) {
        this.get_info_data(id)
          .then(res => {
            this.addForm = {
              id: res.data.id,
              name: res.data.name, //类型：String  必有字段  备注：会员卡名称
              subtitle: res.data.subtitle, //类型：String  必有字段  备注：会员卡副标题
              level: res.data.level, //类型：String  必有字段  备注：会员级别
              content: res.data.content, //类型：String  必有字段  备注：会员卡使用说明
              introduction: res.data.introduction, //类型：String  必有字段  备注：会员卡简介
              price: res.data.price, //类型：Number  必有字段  备注：价格
              isInfinite: res.data.isInfinite, //类型：Number  必有字段  备注：库存限制
              inventoryCount: res.data.inventoryCount, //类型：Number  必有字段  备注：库存数量
              validTimeUnit: res.data.validTimeUnit, //类型：Number  必有字段  备注：有效时长单位，1-周，2-月，3-季，4-年
              validTime: res.data.validTime, //类型：Number  必有字段  备注：有效时长(周期)，表示几周，几个月
              imgUrl: get_img_url_to_path_list(
                res.data.imgUrl,
                res.data.imgUrlPath
              ), //类型：String  必有字段  备注：会员卡封面路径（短）
              sellTime: [
                Date.parse(res.data.startTime),
                Date.parse(res.data.endTime)
              ], //类型：String  必有字段  备注：售卖时效
              isEnabled: res.data.isEnabled, //类型：String  必有字段  备注：是否启用 0-是 1-否
              isFree: res.data.isFree, //类型：String  必有字段  备注：是否免费领取 0-是 1-否
              getNum: res.data.isFree == 0 ? res.data.getNum : '' //类型：String  必有字段  备注：领取次数
            }
            this.drawer = true
            this.add = 1
            this.title = '修改会员卡信息'
          })
          .catch(() => {})
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /**
     * @memo 保存修改方法
     * 根据有无id来判断调用的接口是新增还是修改
     */
    async save_update_data() {
      // 会员卡封面图
      let imgUrl =
        this.addForm.imgUrl.length > 0 ? this.addForm.imgUrl[0].imgUrl : ''
      let param = {
        id: this.addForm.id,
        name: this.addForm.name, //类型：String  必有字段  备注：会员卡名称
        subtitle: this.addForm.subtitle, //类型：String  必有字段  备注：会员卡副标题
        level: this.addForm.level, //类型：String  必有字段  备注：会员级别
        content: this.addForm.content, //类型：String  必有字段  备注：会员卡使用说明
        introduction: this.addForm.introduction, //类型：String  必有字段  备注：会员卡简介
        price: this.addForm.price, //类型：Number  必有字段  备注：价格
        isInfinite: this.addForm.isInfinite, //类型：Number  必有字段  备注：库存限制
        inventoryCount:
          this.addForm.isInfinite == 1
            ? this.addForm.inventoryCount
            : this.addForm.isInfinite == 1
            ? ''
            : '', //类型：Number  必有字段  备注：库存数量
        validTimeUnit: this.addForm.validTimeUnit, //类型：Number  必有字段  备注：有效时长单位，1-周，2-月，3-季，4-年
        validTime: this.addForm.validTime, //类型：Number  必有字段  备注：有效时长(周期)，表示几周，几个月
        imgUrl: imgUrl, //类型：String  必有字段  备注：会员卡封面路径（短）
        startTime: this.addForm.sellTime[0], //类型：String  必有字段  备注：售卖开始时效
        endTime: this.addForm.sellTime[1], //类型：String  必有字段  备注：售卖结束时效
        isEnabled: this.addForm.isEnabled, //类型：String  必有字段  备注：是否启用 0-是 1-否
        isFree: this.addForm.isFree, //类型：String  必有字段  备注：是否免费领取 0-是 1-否
        getNum: this.addForm.isFree == 0 ? this.addForm.getNum : '' //类型：String  必有字段  备注：领取次数
      }
      if (!this.addForm.id) {
        await card_save_path(param).then(res => {
          //添加成功
          this.$message.success(res.message)
          //添加成功返回列表页
          this.clear_drawer()
          this.get_tabel_data(1, this.searchForm.pageSize)
        })
      } else if (this.addForm.id) {
        await card_update_path(param).then(res => {
          //添加成功
          this.$message.success(res.message)
          //添加成功返回列表页
          this.clear_drawer()
          this.get_tabel_data(1, this.searchForm.pageSize)
        })
      }
    },

    /**
     * @memo 提交按钮
     */
    submit_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_update_data()
        } else {
          // 提交失败
          return false
        }
      })
    },

    /********************** 修改状态 操作 ********************************* */
    /**
     * @memo  修改状态 接口
     * @param id  //类型：String  必有字段  备注：编号
     * @param isStatus  //状态 0 正常 1 冻结
     */
    async update_state_data(id, isEnabled) {
      if (id) {
        const param = {
          ids: id,
          isEnabled: isEnabled
        }
        await card_update_state_path(param)
          .then(res => {
            this.$message.success(res.message)
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => {})
      } else {
        this.$message.error('请选择需要修改状态的内容!')
      }
    },

    /*********************** 删除 操作 ********************************* */
    /**
     * @memo 删除接口
     */
    async delete_data(id) {
      const res = await card_delete_path({
        ids: id
      })
      return res
    },

    /**
     * @memo 删除按钮
     */
    delete_btn(id) {
      if (id) {
        this.$confirm(
          '已购买的会员卡删除后用户将无法照找到该会员卡，请确认删除操作。',
          '操作提示',
          {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
          }
        )
          .then(() => {
            this.delete_data(id)
              .then(res => {
                this.$message.success(res.message)
                this.get_tabel_data(
                  this.searchForm.pageNum,
                  this.searchForm.pageSize
                )
              })
              .catch(() => {})
          })
          .catch(() => {})
      } else {
        this.$message.error('请选择需要删除的内容!')
      }
    },

    /********************** 关闭抽屉 操作 ******************************* */
    /**
     * @memo 抽屉关闭前的回调
     */
    handle_close() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => {})
    },

    /**
     * @memo  点击取消按钮，关闭抽屉
     */
    close_drawer() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => {})
    },

    /**
     * @memo 关闭抽屉
     */
    clear_drawer() {
      this.drawer = false
      this.addForm = {
        id: '',
        name: '', //类型：String  必有字段  备注：会员卡名称
        subtitle: '', //类型：String  必有字段  备注：会员卡副标题
        level: '', //类型：String  必有字段  备注：会员级别
        content: '', //类型：String  必有字段  备注：会员卡使用说明
        introduction: '', //类型：String  必有字段  备注：会员卡简介
        price: '', //类型：Number  必有字段  备注：价格
        isInfinite: 0, //类型：Number  必有字段  备注：库存限制
        inventoryCount: '', //类型：Number  必有字段  备注：库存数量
        validTimeUnit: 5, //类型：Number  必有字段  备注：有效时长单位，1-周，2-月，3-季，4-年
        validTime: '', //类型：Number  必有字段  备注：有效时长(周期)，表示几周，几个月
        imgUrl: [], //类型：String  必有字段  备注：会员卡封面路径（短）
        sellTime: [], //类型：String  必有字段  备注：售卖时效
        isEnabled: 0, //类型：String  必有字段  备注：是否启用 0-是 1-否
        isFree: 1, //类型：String  必有字段  备注：是否免费领取 0-是 1-否
        getNum: '' //类型：String  必有字段  备注：领取次数
      }
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /*********************** 列表judge 操作 *************************** */
    /**
     * @memo 状态
     */
    isEnabledJudge(isEnabled) {
      if (isEnabled == 0) {
        return '启用'
      } else if (isEnabled == 1) {
        return '停用'
      }
    }
  }
}
