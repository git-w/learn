/**
 * @description: 用户管理-用户审核管理
 * @author: leroy 
 * @Compile：2021-12-01 17：00
 * @update:lx(2021-12-01 17：00)
 */
import {
  audit_list_path, // 列表
  audit_update_audit_path // 审核
} from '../../../api/user'

import { date_format } from '../../../utils/date/dateFormat'

export default {
  data() {
    return {
      ids: '',

      date_format, // 时间处理器方法 (年-月-日-时-分-秒)

      // 用户审核管理 检索字段
      searchForm: {
        nickname: '', //类型：String  可有字段  备注：用户昵称
        isAudit: '' //类型：String  可有字段  备注：审核状态：0-待审核，1-审核通过 2-审核不通过
      },

      // 用户审核管理 列表数组
      tableData: [],

      // 审核状态 下拉框数组
      isAuditList: [
        {
          value: 0,
          label: '待审核'
        },
        {
          value: 1,
          label: '审核通过'
        },
        {
          value: 2,
          label: '审核未通过'
        }
      ],

      dialog: false, // 审核详情 弹窗
      auditDialog: false, // 审核未通过 弹窗

      // 审核详情 表单字段
      addForm: {
        /*** 修改审核的信息 ***/
        headimgurl: '',
        introduce: '',
        pinCodesProsUrl: '',
        pinCodesProsUrlPath: '',
        pinCodesConsUrl: '',
        pinCodesConsUrlPath: '',
        pinCodesHandUrl: '',
        pinCodesHandUrlPath: '',

        /*** 原来的信息 ***/
        originalHeadimgurl: '',
        originalIntroduce: '',
        originalPinCodesProsUrl: '',
        originalPinCodesProsUrlPath: '',
        originalPinCodesConsUrl: '',
        originalPinCodesConsUrlPath: '',
        originalPinCodesHandUrl: '',
        originalPinCodesHandUrlPath: '',

        isAudit: '', // 审核状态
        reason: '' // 错误原因
      },

      // 审核 表单校验
      rules: {
        // 错误原因
        reason: [
          {
            required: true,
            message: '请输入错误原因',
            trigger: 'blur'
          }
        ]
      }
    }
  },
  mounted() {
    this.to_load() // 进入此页面进行请求
  },
  methods: {
    /**
     * @memo 进入此页面进行加载
     */
    to_load() {
      this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize) // 请求列表数据
    },

    /**
     * @memo 检索重置按钮
     */
    reset_btn() {
      this.searchForm.nickName = '' //类型：String  可有字段  备注：用户昵称
      this.searchForm.isAudit = '' //类型：String  可有字段  备注：审核状态：0-待审核，1-审核通过 2-审核不通过
      this.get_tabel_data(1, this.searchForm.pageSize)
    },

    /**
     * @memo 获取列表
     */
    async get_tabel_data(pageNum, pageSize) {
      let param = {
        nickName: this.searchForm.nickName, //类型：String  可有字段  备注：用户昵称
        isAudit: this.searchForm.isAudit, //类型：String  可有字段  备注：审核状态：0-待审核，1-审核通过 2-审核不通过
        pageNum: pageNum, //当前页码
        pageSize: pageSize //每页的条数
      }
      await audit_list_path(param)
        .then(res => {
          this.tableData = res.data.resultList //列表的数据
          this.searchForm.totals = res.data.totalRows //总长度
          this.searchForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchForm.pageNum > 1) {
            if (res.data.resultList.length == 0) {
              this.get_tabel_data(
                this.searchForm.pageNum - 1,
                this.searchForm.pageSize
              )
            }
          } else if (this.searchForm.pageNum == 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => {})
    },

    /**
     * @memo 下拉框选中的按钮的操作  更多操作
     */
    handle_command(command) {
      if (command.button == 'info_btn') {
        //详情
        this.info_btn(command.row)
      } else if (command.button == 'audited_btn') {
        //审核通过
        this.update_audit_btn(command.row, '1')
      } else if (command.button == 'nopass_btn') {
        //审核未通过
        this.update_audit_btn(command.row, '2')
      }
    },
    /**
     * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
     */
    compose_value(item, row) {
      return {
        button: item,
        row: row
      }
    },

    /*********************** 修改审核状态 操作 ******************************* */
    /**
     * @memo 修改审核状态 按钮
     */
    update_audit_btn(id, isAudit) {
      if (isAudit == 1) {
        // 审核状态为审核通过
        this.$confirm('是否确定审核通过?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        })
          .then(() => {
            this.update_audit_data(id, isAudit)
              .then(res => {
                //审核通过
                this.$message.success(res.message)
                //审核通过，刷新列表
                this.get_tabel_data(
                  this.searchForm.pageNum,
                  this.searchForm.pageSize
                )
              })
              .catch(() => {})
          })
          .catch(() => {})
      } else if (isAudit == 2) {
        // 审核状态为审核未通过
        this.auditDialog = true
        this.addForm = {
          id: id,
          isAudit: isAudit,
          reason: ''
        }
        this.$nextTick(() => {})
      }
    },

    /**
     * @memo  修改审核状态 接口
     */
    async update_audit_data(id, isAudit) {
      if (isAudit == 1) {
        // 审核状态为审核通过
        const res = await audit_update_audit_path({
          id: id,
          isAudit: isAudit
        })
        return res
      } else if (isAudit == 2) {
        // 审核状态为审核未通过
        const res = await audit_update_audit_path({
          id: id,
          isAudit: isAudit,
          reason: this.addForm.reason
        })
        // 接收成功的回调，显示成功提醒
        this.$message.success(res.message)
        this.dialog_audit_close()
        // 重新请求列表数据
        this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
      }
    },

    /**
     * @memo  提交审核未通过信息 按钮
     */
    submit_audit_data(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.update_audit_data(this.addForm.id, this.addForm.isAudit)
        } else {
          return false
        }
      })
    },

    /**
     * @memo 关闭审核未通过 弹窗
     */
    dialog_audit_close() {
      this.auditDialog = false
      this.addForm = {
        id: '',
        isAudit: '', // 审核状态
        reason: '' // 错误原因
      }
      this.$nextTick(() => {})
    },

    /*********************** 审核信息 操作 **************************** */
    /**
     * @memo 审核详情 按钮
     */
    audit_btn(row) {
      if (row) {
        this.addForm = {
          /*** 修改审核的信息 ***/
          headimgurl: row.headimgurl,
          introduce: row.introduce,
          pinCodesProsUrl: row.pinCodesProsUrl,
          pinCodesProsUrlPath: row.pinCodesProsUrlPath,
          pinCodesConsUrl: row.pinCodesConsUrl,
          pinCodesConsUrlPath: row.pinCodesConsUrlPath,
          pinCodesHandUrl: row.pinCodesHandUrl,
          pinCodesHandUrlPath: row.pinCodesHandUrlPath,

          /*** 原来的信息 ***/
          trueName: row.trueName,
          pinCodes: row.pinCodes,
          originalHeadimgurl: row.originalHeadimgurl,
          originalIntroduce: row.originalIntroduce,
          originalPinCodesProsUrl: row.originalPinCodesProsUrl,
          originalPinCodesProsUrlPath: row.originalPinCodesProsUrlPath,
          originalPinCodesConsUrl: row.originalPinCodesConsUrl,
          originalPinCodesConsUrlPath: row.originalPinCodesConsUrlPath,
          originalPinCodesHandUrl: row.originalPinCodesHandUrl,
          originalPinCodesHandUrlPath: row.originalPinCodesHandUrlPath,

          isAudit: row.isAudit, // 审核状态
          reason: row.reason // 错误原因
        }
        this.dialog = true
        this.$nextTick(() => {})
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /**
     * @memo 关闭实名认证 弹窗
     */
    handle_close_dialog() {
      this.clear_dialog()
    },

    /**
     * @memo 点击取消按钮，关闭弹窗
     */
    close_dialog() {
        this.clear_dialog()
    },

    /**
     * @memo 关闭抽屉
     */
    clear_dialog() {
      this.dialog = false
      this.addForm = {
        /*** 修改审核的信息 ***/
        headSculpture: '',
        introduce: '',
        pinCodesProsUrl: '',
        pinCodesProsUrlPath: '',
        pinCodesConsUrl: '',
        pinCodesConsUrlPath: '',
        pinCodesHandUrl: '',
        pinCodesHandUrlPath: '',

        /*** 原来的信息 ***/
        originalHeadimgurl: '',
        originalIntroduce: '',
        originalPinCodesProsUrl: '',
        originalPinCodesProsUrlPath: '',
        originalPinCodesConsUrl: '',
        originalPinCodesConsUrlPath: '',
        originalPinCodesHandUrl: '',
        originalPinCodesHandUrlPath: '',

        isAudit: '', // 审核状态
        reason: '' // 错误原因
      }
      this.$nextTick(() => {})
    },

    /*********************** 列表judge 操作 *************************** */
    /**
     * @memo 审核 状态
     */
    isAuditJudge(isAudit) {
      let res = this.isAuditList.filter(item => {
        return item.value == isAudit
      })
      return res.length > 0 ? res[0].label : '暂无数据'
    }
  }
}
