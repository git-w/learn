/**
 * @description: 用户管理-举报管理-分类管理
 * @author: leroy
 * @Compile：2021-11-26 13：00
 * @update:lx(2021-11-26 13：00)
 */
import {
  report_category_save_path, // 添加
  report_category_update_path, // 修改
  report_category_delete_path, // 删除
  report_category_list_path, // 列表
  report_category_info_path, // 详情
  report_category_update_state_path // 批量修改状态
} from '../../../../api/user'

import { date_format, date_format_ymd } from '../../../../utils/date/dateFormat'

export default {
  data() {
    return {
      ids: '',

      date_format, // 时间处理器方法 (年-月-日-时-分-秒)
      date_format_ymd, // 时间处理器方法 (年-月-日)

      // 分类管理 检索字段
      searchForm: {
        cateName: '', //类型：String  可有字段  备注：分类管理
        isState: '' //类型：Number  可有字段  备注：状态
      },
      tableData: [], // 列表的数据

      drawer: false, // 控制抽屜的开合
      title: '', // 控制抽屜的头部
      add: 1,

      // 状态
      stateList: [
        {
          value: 0,
          label: '启用 '
        },
        {
          value: 1,
          label: '停用 '
        }
      ],

      // 分类管理 表单字段
      addForm: {
        id: '',
        cateName: '', // 类型：String  必有字段  备注：名称
        sort: '', // 类型：Number  可有字段  备注：排序 0是最高 默认0
        isState: '', // 类型：Number  可有字段  备注：是否上线 0是 1否 默认上线
        memo: '' // 类型：Number  可有字段  备注：备注
      },

      // 分类管理 表单校验
      rules: {
        // 分类名称
        cateName: [
          {
            required: true,
            message: '请输入分类名称',
            trigger: 'blur'
          }
        ],
        // 排序
        sort: [
          {
            required: true,
            message: '请输入排序',
            trigger: 'blur'
          },
          {
            pattern: /^[0-9]+[0-9]*$/,
            message: '请输入正整数'
          }
        ]
      }
    }
  },
  mounted() {
    this.to_load() // 进入此页面进行请求
  },
  methods: {
    /**
     * @memo 进入此页面进行加载
     */
    to_load() {
      this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize) // 请求列表数据
    },

    /**
     * @memo  检索重置按钮
     */
    reset_btn() {
      this.searchForm.cateName = '' //类型：String  可有字段  备注：分类管理
      this.searchForm.isState = '' //类型：Number  可有字段  备注：是否上线 0是 1否
      this.get_tabel_data(1, this.searchForm.pageSize) // 请求列表数据
    },

    /**
     * @memo 获取列表
     */
    async get_tabel_data(pageNum, pageSize) {
      let param = {
        pageNum: pageNum, //当前页码
        pageSize: pageSize, //每页的条数
        cateName: this.searchForm.cateName, //类型：String  可有字段  备注：分类名称
        isState: this.searchForm.isState //类型：Number  可有字段  备注：状态
      }
      await report_category_list_path(param)
        .then(res => {
          this.tableData = res.data.resultList //列表的数据
          this.searchForm.totals = res.data.totalRows //总长度
          this.searchForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchForm.pageNum > 1) {
            if (res.data.resultList.length == 0) {
              this.get_tabel_data(
                this.searchForm.pageNum - 1,
                this.searchForm.pageSize
              )
            }
          } else if (this.searchForm.pageNum == 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => {})
    },

    /**
     * @memo table的sort-change事件
     * @param 参数 column.order 有两个值：ascending 升序  descending 降序
     */
    change_table_sort(column) {
      //获取字段名称和排序类型
      // var fieldName = column.prop;   // 获取prop指定的字段
      var sortingType = column.order //  ascending 升序   descending 降序
      var sortable = column.column.sortable // 点击的是那一列
      if (sortingType == 'descending') {
        if (sortable == 'isState') {
          this.searchForm.isState = '0'
        }
      }
      //按照升序排序
      else if (sortingType == 'ascending') {
        if (sortable == 'isState') {
          this.searchForm.isState = '1'
        }
      }
      //无序
      else if (!sortingType) {
        if (sortable == 'isState') {
          this.searchForm.isState = ''
        }
      }
      //请求列表
      this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
    },

    /**
     * @memo 下拉框选中的按钮的操作  更多操作
     */
    handle_command(command) {
      if (command.button == 'delete_btn') {
        //删除
        this.delete_btn(command.row)
      } else if (command == 'delete_btn') {
        //多选 删除
        this.delete_btn(this.ids)
      } else if (command == 'more_disable') {
        //多选 停用
        this.update_state_data(this.ids, 1)
      } else if (command == 'more_start_using') {
        //多选 启用
        this.update_state_data(this.ids, 0)
      } else if (command.button == 'disable_btn') {
        //停用
        this.update_state_data(command.row, 1)
      } else if (command.button == 'start_using_btn') {
        //启用
        this.update_state_data(command.row, 0)
      } else if (command.button == 'change_btn') {
        //修改
        this.change_btn(command.row)
      }
    },
    /**
     * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
     */
    compose_value(item, row) {
      return {
        button: item,
        row: row
      }
    },

    /************************** 添加、修改 操作 *************************** */
    /**
     * @memo 添加-跳往添加界面
     */
    add_btn() {
      this.drawer = true
      this.title = '添加分类'
      this.addForm = {
        id: '',
        cateName: '', //类型：String  可有字段  备注：分类名称
        sort: '', //类型：String  可有字段  备注：排序 数值越大越靠前
        isState: 0, //类型：String  可有字段  备注：是否上线 0是 1否
        memo: '' //类型：String  可有字段  备注：备注
      }
      // 关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /**
     * @memo  详情接口
     */
    async get_info_data(id) {
      const res = await report_category_info_path({
        id: id
      })
      return res
    },

    /**
     * @memo 修改
     */
    change_btn(id) {
      if (id) {
        this.get_info_data(id)
          .then(res => {
            this.addForm = {
              id: res.data.id, //类型：String  必有字段  备注：id
              cateName: res.data.cateName, //类型：String  可有字段  备注：分类名称
              sort: res.data.sort, //类型：String  可有字段  备注：排序 数值越大越靠前
              isState: res.data.isState, //类型：String  可有字段  备注：是否上线 0是 1否
              memo: res.data.memo //类型：String  可有字段  备注：备注
            }
            this.drawer = true
            this.title = '修改分类'
            this.$nextTick(() => {})
          })
          .catch(() => {})
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /**
     * @memo 保存修改方法
     * 根据有无id来判断调用的接口是新增还是修改
     */
    async save_update_data() {
      let param = {
        id: this.addForm.id ? this.addForm.id : '', //类型：String  必有字段  备注：id
        cateName: this.addForm.cateName, //类型：String  必有字段  备注：分类名称
        sort: this.addForm.sort, // 类型：Number  可有字段  备注：排序 0是最高 默认0
        isState: this.addForm.isState, // 类型：Number  可有字段  备注：是否上线 0是 1否 默认上线
        memo: this.addForm.memo //类型：String  可有字段  备注：备注
      }
      if (!this.addForm.id) {
        await report_category_save_path(param)
          .then(res => {
            // 添加成功
            this.$message.success(res.message)
            // 添加成功返回列表页
            this.clear_drawer()
            this.get_tabel_data(1, this.searchForm.pageSize)
          })
          .catch(() => {})
      } else if (this.addForm.id) {
        await report_category_update_path(param)
          .then(res => {
            // 修改成功
            this.$message.success(res.message)
            // 修改成功返回列表页
            this.clear_drawer()
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => {})
      }
    },

    /**
     * @memo 提交按钮
     */
    submit_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_update_data()
        } else {
          // 提交失败
          return false
        }
      })
    },

    /************************ 修改状态 操作 ****************************** */
    /**
     * @memo  修改多条状态接口
     */
    async update_state_data(id, isState) {
      if (id) {
        const param = {
          ids: id,
          isState: isState
        }
        await report_category_update_state_path(param)
          .then(res => {
            this.$message({
              message: res.message,
              type: 'success'
            })
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => {})
      } else {
        this.$message.error('请选择需要修改状态的内容!')
      }
    },

    /************************** 删除 操作 *************************** */
    /**
     * @memo 删除接口
     */
    async delete_data(id) {
      const res = await report_category_delete_path({
        ids: id
      })
      return res
    },

    /**
     * @memo 删除按钮
     */
    delete_btn(id) {
      if (id) {
        this.$confirm('是否确定删除?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        })
          .then(() => {
            this.delete_data(id)
              .then(res => {
                this.$message.success(res.message)
                this.get_tabel_data(
                  this.searchForm.pageNum,
                  this.searchForm.pageSize
                )
              })
              .catch(() => {})
          })
          .catch(() => {})
      } else {
        this.$message.error('请选择需要删除的内容!')
      }
    },

    /********************** 关闭抽屉 操作 ******************************* */
    /**
     * @memo 抽屉关闭前的回调
     */
    handle_close() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => {})
    },

    /**
     * @memo 点击取消按钮，关闭抽屉
     */
    close_drawer() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => {})
    },

    /**
     * @memo 关闭抽屉
     */
    clear_drawer() {
      this.drawer = false
      this.addForm = {
        id: '',
        cateName: '', // 类型：String  必有字段  备注：分类名称
        sort: '', // 类型：Number  可有字段  备注：排序 0是最高 默认0
        memo: '', //类型：String  可有字段  备注：备注
        isState: '' // 类型：Number  可有字段  备注：是否上线 0是 1否 默认上线
      }
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /*********************** 列表judge 操作 *************************** */
    /**
     * @memo 是否上线  状态
     */
    isStateJudge(isState) {
      if (isState == 0) {
        return '启用'
      } else if (isState == 1) {
        return '停用'
      }
    }
  }
}
