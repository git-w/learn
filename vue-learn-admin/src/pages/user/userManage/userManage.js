/**
 * @description: 用户管理-用户信息管理
 * @author: leroy 
 * @Compile：2021-11-26 13：00
 * @update:lx(2021-11-26 13：00)
 */

import {
  user_list_path, // 列表
  user_info_path, // 详情
  user_save_path, // 添加
  user_update_path, // 修改
  user_update_state_path, // 批量修改状态
  user_update_type_path, // 批量修改推荐状态
  visitors_list_path, // 访客记录 列表
  browse_list_path, // 浏览记录 列表
  user_card_info_path, // 用户会员卡 详情
  user_addition_update_path,
  user_mate_update_path
} from '../../../api/user'

import {
  card_member_list_path // 使用记录 列表
} from '../../../api/card'

import {
  validatemoblie // 手机号或电话号
} from '../../../utils/verify' // 表单校验规则

import {
  date_format,
  format_date,
  format_date_ymd,
  get_birthday
} from '../../../utils/date/dateFormat'

import { get_img_url_to_path_list } from '@/utils/img/imageMethod.js'

export default {
  data() {
    return {
      ids: '',

      date_format, // 时间处理器方法 (年-月-日 时:分:秒)
      format_date, // 国际时间处理方法 (年-月-日 时:分:秒)
      format_date_ymd, // 国际时间处理方法 (年-月-日)
      get_birthday, // 时间处理器方法 （年龄）

      // 用户管理 检索字段
      searchForm: {
        trueName: '' //类型：String  可有字段  备注：真实姓名
      },
      // 用户管理 列表数组
      tableData: [],

      drawer: false, // 控制抽屜的开合

      title: '', // 控制抽屜的头部
      add: 1,
      active: '', // 添加用户 步骤
      activeCollapse: [], // 详情折叠面板 默认打开

      inputVisible: false, // 用户标签组件相关的变量控制输入框的显示隐藏
      inputValue: '', // 用户标签组件相关的变量

      // 用户管理 表单字段
      addForm: {
        id: '',
        trueName: '', //类型：String  可有字段  备注：真实姓名
        email: '', //类型：String  可有字段  备注：邮箱
        birthday: '', //类型：String  可有字段  备注：生日
        pinCodes: '', //类型：String  可有字段  备注：身份证号
        address: '', //类型：String  可有字段  备注：用户住址
        nativePlace: '', //类型：String  可有字段  备注：籍贯
        userName: '', //类型：String  可有字段  备注：用户注册时使用个的帐号
        password: '', //类型：String  可有字段  备注：密码
        nickName: '', //类型：String  可有字段  备注：用户的昵称
        headimgurl: '', //类型：String  可有字段  备注：用户头像
        modilePhone: '', //类型：String  可有字段  备注：手机号
        routineOpenid: '', //类型：String  可有字段  备注：小程序唯一身份ID
        sex: '', //类型：String  可有字段  备注：用户的性别
        city: '', //类型：String  可有字段  备注：用户所在城市
        language: '', //类型：String  可有字段  备注：用户的语言，简体中文为zh_CN
        province: '', //类型：String  可有字段  备注：用户所在省份
        country: '', //类型：String  可有字段  备注：用户所在国家
        unionid: '', //类型：String  可有字段  备注：只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
        openid: '', //类型：String  可有字段  备注：用户的标识
        otherUid: '', //类型：String  可有字段  备注：第三方平台返回的user_id
        totalIntegral: '', //类型：String  可有字段  备注：累计总积分
        integral: '', //类型：String  可有字段  备注：累计剩余积分
        money: '', //类型：String  可有字段  备注：账户金额
        totalMoney: '', //类型：String  可有字段  备注：总账户金额
        registerTime: '', //类型：String  可有字段  备注：注册时间
        registerIp: '', //类型：String  可有字段  备注：注册ip
        loginTime: '', //类型：String  可有字段  备注：最后一次登录时间
        loginIp: '', //类型：String  可有字段  备注：登录ip
        isState: '', //类型：String  可有字段  备注：账号状态 0:正常 1:冻结 2是禁言 3拉黑
        isAudit: '', //类型：String  可有字段  备注：是否已审核：0-是，1-否
        isType: '', //类型：String  可有字段  备注：用户类型（0:无 ）
        level: '', //类型：String  可有字段  备注：用户等级 1-游客（普通用户）2-会员vip
        consumeAmount: '', //类型：String  可有字段  累计消费金额
        createTime: '', //类型：String  可有字段  备注：添加时间
        updateTime: '', //类型：String  可有字段  备注：更新时间
        memo: '', //类型：String  可有字段  备注：备注
        userMateSelectionVo: {}, //类型：Object  可有字段  备注：择偶标准
        userAdditionVo: {} //类型：Object  可有字段  备注：用户附加信息
      },

      // 择偶标准 表单字段
      userMateSelectionVo: {
        ageOptionalRange: '', // 年龄可选范围：18-99（最小年龄选不限、最多年龄选30，则显示：30岁及以下）
        heightOptionalRange: '', // 身高可选范围：145-250（最低选160cm、最高选不限，则显示：160cm及以上）
        educationBackground: '', // 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
        maritalStatus: '', // 婚姻状况：0-未婚、1-离异、2-丧偶
        childrenStatus: '', // 子女状况：0-无子女、1-有子女
        provinceName: '', // 用户所在省份名称
        cityName: '', // 用户所在城市名称
        provinceId: '', // 用户所在省份id
        cityId: '', // 用户所在城市id
        housingStatus: '', // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
        carStatus: '', // 购车情况：0-已购车、1-有需要购车、2-其他
        userId: ''
      },
      rules: {
        // 性别
        sex: [
          {
            required: true,
            message: '请选择性别',
            trigger: 'change'
          }
        ],
        // 出生年月
        birthday: [
          {
            required: true,
            message: '请选择出生年月',
            trigger: 'change'
          }
        ],
        // 手机号
        modilePhone: [
          {
            required: true,
            message: '请输入手机号',
            trigger: 'blur'
          },
          {
            validator: validatemoblie
          }
        ]
      },

      /*********************** 访客/浏览记录 ************************* */
      tabName: 'visitors', // 访客/浏览记录 tabs
      tableVisitorsData: [], // 访客记录 列表数组
      // 访客记录 列表
      searchVisitorsForm: {
        pageNum: 1,
        pageSize: 10,
        totals: 0
      },
      tableBrowseData: [], // 浏览记录 列表数组
      // 浏览记录 列表
      searchBrowseForm: {
        pageNum: 1,
        pageSize: 10,
        totals: 0
      },

      /************************ 使用记录 *********************** */
      // 使用记录 检索字段
      searchMemberForm: {
        pageNum: 1, //类型：Number  必有字段  备注：页码 默认第一页
        pageSize: 10, //类型：Number  必有字段  备注：条码 每页默认十条
        totals: 0
      },
      tableMemberData: [] // 使用记录 列表数组
    }
  },
  mounted() {
    this.to_load() // 进入此页面进行请求
  },
  methods: {
    /**
     * @memo 进入此页面进行加载
     */
    to_load() {
      this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize) // 请求列表数据
    },

    /**
     * @memo 检索重置
     */
    reset_btn() {
      this.searchForm.trueName = '' //类型：String  可有字段  备注：真实姓名
      this.get_tabel_data(1, this.searchForm.pageSize)
    },

    /**
     * @memo 获取列表
     */
    async get_tabel_data(pageNum, pageSize) {
      let param = {
        trueName: this.searchForm.trueName, //类型：String  可有字段  备注：真实姓名
        pageNum: pageNum, //当前页码
        pageSize: pageSize //每页的条数
      }
      await user_list_path(param)
        .then(res => {
          this.tableData = res.data.resultList // 列表的数据
          this.searchForm.totals = res.data.totalRows // 总长度
          this.searchForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchForm.pageNum > 1) {
            if (res.data.resultList.length == 0) {
              this.get_tabel_data(
                this.searchForm.pageNum - 1,
                this.searchForm.pageSize
              )
            }
          } else if (this.searchForm.pageNum == 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => {})
    },

    /**
     * @memo 下拉框选中的按钮的操作  更多操作
     */
    handle_command(command) {
      if (command.button == 'disable_btn') {
        //停用
        this.update_state_data(command.row, 1)
      } else if (command.button == 'enabled_btn') {
        //启用
        this.update_state_data(command.row, 0)
      } else if (command.button == 'disable_type_btn') {
        //取消推荐
        this.update_type_data(command.row, '1')
      } else if (command.button == 'enabled_type_btn') {
        //推荐
        this.update_type_data(command.row, '0')
      } else if (command.button == 'info_btn') {
        //详情
        this.info_btn(command.row)
      } else if (command.button == 'card_info_btn') {
        //会员卡详情
        this.card_info_btn(command.row)
      } else if (command.button == 'change_btn') {
        //修改资料
        this.change_btn(command.row)
      }
    },
    /**
     * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
     */
    compose_value(item, row) {
      return {
        button: item,
        row: row
      }
    },

    /************************ 添加用户 操作 ******************************* */
    /**
     * @memo 添加用户
     */
    add_btn() {
      this.drawer = true
      this.add = 1
      this.title = '添加用户'
      this.active = 1
      this.addForm = {
        /************ 基本信息 ************* */
        id: '',
        trueName: '', // 真实姓名
        birthday: '', // 生日
        address: '', // 用户住址
        nativePlace: '', // 籍贯
        nickName: '', // 用户的昵称
        headimgurl: [], // 用户头像
        modilePhone: '', // 手机号
        sex: '', // 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
        isState: '', // 账号状态 0:正常 1:冻结 2是禁言 3拉黑
        memo: '', // 备注
        imgPhotoWallUrl: [], // 照片墙面 多个英文逗号短路径
        wechatNumber: '', // 微信号
        pinCodes: '', // 身份证号

        /*************** 详细信息 ************* */
        tags: [], // 用户标签，多个英文逗号隔开
        stature: '', // 身高-身高范围：145-250
        educationBackground: '', // 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
        maritalStatus: '', // 婚姻状况：0-未婚、1-离异、2-丧偶
        childrenStatus: '', // 子女状况：0-无子女、1-有子女
        provinceName: '', // 用户所在省份名称
        cityName: '', // 用户所在城市名称
        provinceId: '', // 用户所在省份id
        cityId: '', // 用户所在城市id
        housingStatus: '', // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
        carStatus: '', // 购车情况：0-已购车、1-有需要购车、2-其他
        coverImgUrl: [], // 封面图
        userId: '' // 用户id
      }

      /**************** 择偶标准 ***************** */
      this.userMateSelectionVo = {
        ageOptionalRange: '', // 年龄可选范围：18-99（最小年龄选不限、最多年龄选30，则显示：30岁及以下）
        heightOptionalRange: '', // 身高可选范围：145-250（最低选160cm、最高选不限，则显示：160cm及以上）
        educationBackground: '', // 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
        maritalStatus: '', // 婚姻状况：0-未婚、1-离异、2-丧偶
        childrenStatus: '', // 子女状况：0-无子女、1-有子女
        provinceName: '', // 用户所在省份名称
        cityName: '', // 用户所在城市名称
        provinceId: '', // 用户所在省份id
        cityId: '', // 用户所在城市id
        housingStatus: '', // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
        carStatus: '', // 购车情况：0-已购车、1-有需要购车、2-其他
        userId: ''
      }
    },

    /**
     * @memo 用户标签输入的关闭事件
     */
    handle_tags_close(tag) {
      if (this.infoDisabled) {
        return
      }
      this.addForm.tags.splice(this.addForm.tags.indexOf(tag), 1)
    },
    /**
     * @memo 用户标签的输入事件
     */
    show_input() {
      this.inputVisible = true
      this.$nextTick(_ => {
        this.$refs.tags.$refs.input.focus()
      })
    },
    /**
     * @memo 用户标签的输入框脱标事件
     */
    handle_input_confirm() {
      let inputValue = this.inputValue
      if (inputValue) {
        this.addForm.tags.push(inputValue)
      }
      this.inputVisible = false
      this.inputValue = ''
    },

    /**
     * @memo 基本信息保存方法
     */
    async save_user_data() {
      // 头像
      let headimgurl =
        this.addForm.headimgurl.length > 0
          ? this.addForm.headimgurl[0].imgUrlPath
          : ''
      // 照片墙
      let imgPhotoWallUrl = ''
      if (this.addForm.imgPhotoWallUrl.length > 0) {
        this.addForm.imgPhotoWallUrl.forEach(item => {
          imgPhotoWallUrl += item.imgUrl + ','
        })
      }

      let param = {
        id: this.addForm.id,
        trueName: this.addForm.trueName, // 真实姓名
        birthday: this.addForm.birthday, // 生日
        address: this.addForm.address, // 用户住址
        nativePlace: this.addForm.nativePlace, // 籍贯
        nickName: this.addForm.nickName, // 用户的昵称
        headimgurl: headimgurl, // 用户头像
        modilePhone: this.addForm.modilePhone, // 手机号
        sex: this.addForm.sex, // 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
        memo: this.addForm.memo, // 备注
        imgPhotoWallUrl: imgPhotoWallUrl, // 照片墙面 多个英文逗号短路径
        wechatNumber: this.addForm.wechatNumber, // 微信号
        pinCodes: this.addForm.pinCodes // 身份证号
      }
      if (!this.addForm.id) {
        await user_save_path(param).then(res => {
          //添加成功
          this.$message.success(res.message)
          //添加成功返回列表页
          this.active = 2
          this.addForm.id = res.data
        })
      } else if (this.addForm.id) {
        await user_update_path(param)
          .then(res => {
            // 修改成功
            this.$message.success(res.message)
            // 修改成功返回列表页
            this.active = 2
          })
          .catch(() => {})
      }
    },
    /**
     * @memo 用户信息-基本信息保存
     */
    submit_user_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_user_data()
        } else {
          // 提交失败
          return false
        }
      })
    },

    /**
     * @memo 详细信息保存方法
     */
    async save_addition_data() {
      // 背景图片
      let coverImgUrl =
        this.addForm.coverImgUrl.length > 0
          ? this.addForm.coverImgUrl[0].imgUrl
          : ''
      let param = {
        tags: this.addForm.tags.length > 0 ? this.addForm.tags.join(',') : '', // 用户标签，多个英文逗号隔开
        stature: this.addForm.stature, // 身高-身高范围：145-250
        weight: this.addForm.weight,  // 体重
        educationBackground: this.addForm.educationBackground, // 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
        maritalStatus: this.addForm.maritalStatus, // 婚姻状况：0-未婚、1-离异、2-丧偶
        childrenStatus: this.addForm.childrenStatus, // 子女状况：0-无子女、1-有子女
        provinceName: this.addForm.provinceName, // 用户所在省份名称
        cityName: this.addForm.cityName, // 用户所在城市名称
        provinceId: this.addForm.provinceId, // 用户所在省份id
        cityId: this.addForm.cityId, // 用户所在城市id
        housingStatus: this.addForm.housingStatus, // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
        carStatus: this.addForm.carStatus, // 购车情况：0-已购车、1-有需要购车、2-其他
        coverImgUrl: coverImgUrl, // 封面图
        introduce: this.addForm.introduce,  // 独白
        userId: this.addForm.id, // 用户id
      }
      if (this.addForm.id) {
        await user_addition_update_path(param).then(res => {
          //添加成功
          this.$message.success(res.message)
          //添加成功返回列表页
          this.active = 3
        })
      }
    },
    /**
     * @memo 用户信息- 详细信息保存
     */
    submit_addition_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_addition_data()
        } else {
          // 提交失败
          return false
        }
      })
    },

    /**
     * @memo 上一页 按钮
     */
    pre_addition_btn(active) {
      console.log(active)
      this.active = active-1
    },

    /**
     * @memo 择偶标准保存方法
     */
    async save_mate_data() {
      let param = {
        ageOptionalRange: this.userMateSelectionVo.ageOptionalRange, // 年龄可选范围：18-99（最小年龄选不限、最多年龄选30，则显示：30岁及以下）
        heightOptionalRange: this.userMateSelectionVo.heightOptionalRange, // 身高可选范围：145-250（最低选160cm、最高选不限，则显示：160cm及以上）
        educationBackground: this.userMateSelectionVo.educationBackground, // 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
        maritalStatus: this.userMateSelectionVo.maritalStatus, // 婚姻状况：0-未婚、1-离异、2-丧偶
        childrenStatus: this.userMateSelectionVo.childrenStatus, // 子女状况：0-无子女、1-有子女
        provinceName: this.userMateSelectionVo.provinceName, // 用户所在省份名称
        cityName: this.userMateSelectionVo.cityName, // 用户所在城市名称
        provinceId: this.userMateSelectionVo.provinceId, // 用户所在省份id
        cityId: this.userMateSelectionVo.cityId, // 用户所在城市id
        housingStatus: this.userMateSelectionVo.housingStatus, // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
        carStatus: this.addForm.carStatus, // 购车情况：0-已购车、1-有需要购车、2-其他
        userId: this.addForm.id // 用户id
      }
      if (this.addForm.id) {
        await user_mate_update_path(param).then(res => {
          //添加成功
          this.$message.success(res.message)
          //添加成功返回列表页
          this.clear_drawer()
          this.get_tabel_data(1, this.searchForm.pageSize)
        })
      }
    },
    /**
     * @memo 用户信息 - 择偶标准保存
     */
    submit_mate_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_mate_data()
        } else {
          // 提交失败
          return false
        }
      })
    },

    /************************ 详情 操作 ******************************* */
    /**
     * @memo 详情接口
     */
    async get_info_data(id) {
      const res = await user_info_path({
        id: id
      })
      return res
    },

    /**
     * @memo 详情按钮
     */
    info_btn(id) {
      if (id) {
        this.get_info_data(id)
          .then(res => {
            this.addForm = {
              id: res.data.id,
              trueName: res.data.trueName, //类型：String  可有字段  备注：真实姓名
              email: res.data.email, //类型：String  可有字段  备注：邮箱
              birthday: res.data.birthday, //类型：String  可有字段  备注：生日
              pinCodes: res.data.pinCodes, //类型：String  可有字段  备注：身份证号
              address: res.data.address, //类型：String  可有字段  备注：用户住址
              nativePlace: res.data.nativePlace, //类型：String  可有字段  备注：籍贯
              userName: res.data.userName, //类型：String  可有字段  备注：用户注册时使用个的帐号
              password: res.data.password, //类型：String  可有字段  备注：密码
              nickName: res.data.nickName, //类型：String  可有字段  备注：用户的昵称
              headimgurl: res.data.headimgurl, //类型：String  可有字段  备注：用户头像
              modilePhone: res.data.modilePhone, //类型：String  可有字段  备注：手机号
              routineOpenid: res.data.routineOpenid, //类型：String  可有字段  备注：小程序唯一身份ID
              sex: res.data.sex, //类型：String  可有字段  备注：用户的性别
              city: res.data.city, //类型：String  可有字段  备注：用户所在城市
              language: res.data.language, //类型：String  可有字段  备注：用户的语言，简体中文为zh_CN
              province: res.data.province, //类型：String  可有字段  备注：用户所在省份
              country: res.data.country, //类型：String  可有字段  备注：用户所在国家
              unionid: res.data.unionid, //类型：String  可有字段  备注：只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
              openid: res.data.openid, //类型：String  可有字段  备注：用户的标识
              otherUid: res.data.otherUid, //类型：String  可有字段  备注：第三方平台返回的user_id
              totalIntegral: res.data.totalIntegral, //类型：String  可有字段  备注：累计总积分
              integral: res.data.integral, //类型：String  可有字段  备注：累计剩余积分
              money: res.data.money, //类型：String  可有字段  备注：账户金额
              totalMoney: res.data.totalMoney, //类型：String  可有字段  备注：总账户金额
              registerTime: res.data.registerTime, //类型：String  可有字段  备注：注册时间
              registerIp: res.data.registerIp, //类型：String  可有字段  备注：注册ip
              loginTime: res.data.loginTime, //类型：String  可有字段  备注：最后一次登录时间
              loginIp: res.data.loginIp, //类型：String  可有字段  备注：登录ip
              isState: res.data.isState, //类型：String  可有字段  备注：账号状态 0:正常 1:冻结 2是禁言 3拉黑
              isAudit: res.data.isAudit, //类型：String  可有字段  备注：是否已审核：0-是，1-否
              isType: res.data.isType, //类型：String  可有字段  备注：用户类型（0:无 ）
              level: res.data.level, //类型：String  可有字段  备注：用户等级 1-游客（普通用户）2-会员vip
              consumeAmount: res.data.consumeAmount, //类型：String  可有字段  累计消费金额
              createTime: res.data.createTime, //类型：String  可有字段  备注：添加时间
              updateTime: res.data.updateTime, //类型：String  可有字段  备注：更新时间
              memo: res.data.memo, //类型：String  可有字段  备注：备注
              wechatNumber: res.data.wechatNumber, //类型：String  可有字段  备注：微信号
              imgPhotoWallUrl: res.data.imgPhotoWallUrl
                ? res.data.imgPhotoWallUrl.split(',')
                : [], //类型：String  可有字段  备注：照片墙短路径
              imgPhotoWallUrlPath: res.data.imgPhotoWallUrlPath
                ? res.data.imgPhotoWallUrlPath.split(',')
                : [], //类型：String  可有字段  备注：照片墙长路径
              userMateSelectionVo: res.data.userMateSelectionVo, //类型：Object  可有字段  备注：择偶对象
              userAdditionVo: {
                carStatus: res.data.userAdditionVo.carStatus,
                childrenStatus: res.data.userAdditionVo.childrenStatus,
                cityId: res.data.userAdditionVo.cityId,
                cityName: res.data.userAdditionVo.cityName,
                coverImgUrl: res.data.userAdditionVo.coverImgUrl,
                coverImgUrlPath: res.data.userAdditionVo.coverImgUrlPath,
                duration: res.data.userAdditionVo.duration,
                educationBackground:
                  res.data.userAdditionVo.educationBackground,
                housingStatus: res.data.userAdditionVo.housingStatus,
                introduce: res.data.userAdditionVo.introduce,
                introduceVoiceUrl: res.data.userAdditionVo.introduceVoiceUrl,
                introduceVoiceUrlPath:
                  res.data.userAdditionVo.introduceVoiceUrlPath,
                maritalStatus: res.data.userAdditionVo.maritalStatus,
                trueName: res.data.trueName,
                pinCodes: res.data.pinCodes,
                pinCodesConsUrl: res.data.userAdditionVo.pinCodesConsUrl,
                pinCodesConsUrlPath:
                  res.data.userAdditionVo.pinCodesConsUrlPath,
                pinCodesHandUrl: res.data.userAdditionVo.pinCodesHandUrl,
                pinCodesHandUrlPath:
                  res.data.userAdditionVo.pinCodesHandUrlPath,
                pinCodesProsUrl: res.data.userAdditionVo.pinCodesProsUrl,
                pinCodesProsUrlPath:
                  res.data.userAdditionVo.pinCodesProsUrlPath,
                provinceId: res.data.userAdditionVo.provinceId,
                provinceName: res.data.userAdditionVo.provinceName,
                stature: res.data.userAdditionVo.stature,
                tags: res.data.userAdditionVo.tags
                  ? res.data.userAdditionVo.tags.split(',')
                  : [],
                userId: res.data.userAdditionVo.userId
              } //类型：Object  可有字段  备注：用户附加信息
            }
            this.tabName = 'visitors' // 访客/浏览记录 tabs
            this.get_tabel_visitors_data(
              this.searchVisitorsForm.pageNum,
              this.searchVisitorsForm.pageSize,
              this.addForm.id
            )
            this.get_tabel_browse_data(
              this.searchBrowseForm.pageNum,
              this.searchBrowseForm.pageSize,
              this.addForm.id
            )
            this.activeCollapse = ['1', '2', '3', '4', '5']
            this.drawer = true
            this.title = '用户详情'
            this.add = 2
            this.$nextTick(() => {})
          })
          .catch(() => {})
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /****************************** 修改资料 操作 ****************************** */
    /**
     * @memo 修改资料按钮
     */
    change_btn(id) {
      if (id) {
        this.get_info_data(id)
          .then(res => {
            this.addForm = {
              id: res.data.id,
              trueName: res.data.trueName, //类型：String  可有字段  备注：真实姓名
              email: res.data.email, //类型：String  可有字段  备注：邮箱
              birthday: res.data.birthday, //类型：String  可有字段  备注：生日
              pinCodes: res.data.pinCodes, //类型：String  可有字段  备注：身份证号
              address: res.data.address, //类型：String  可有字段  备注：用户住址
              nativePlace: res.data.nativePlace, //类型：String  可有字段  备注：籍贯
              userName: res.data.userName, //类型：String  可有字段  备注：用户注册时使用个的帐号
              password: res.data.password, //类型：String  可有字段  备注：密码
              nickName: res.data.nickName, //类型：String  可有字段  备注：用户的昵称
              headimgurl: get_img_url_to_path_list(
                res.data.headimgurl,
                res.data.headimgurl
              ), //类型：String  可有字段  备注：用户头像
              modilePhone: res.data.modilePhone, //类型：String  可有字段  备注：手机号
              routineOpenid: res.data.routineOpenid, //类型：String  可有字段  备注：小程序唯一身份ID
              sex: res.data.sex, //类型：String  可有字段  备注：用户的性别
              city: res.data.city, //类型：String  可有字段  备注：用户所在城市
              language: res.data.language, //类型：String  可有字段  备注：用户的语言，简体中文为zh_CN
              province: res.data.province, //类型：String  可有字段  备注：用户所在省份
              country: res.data.country, //类型：String  可有字段  备注：用户所在国家
              unionid: res.data.unionid, //类型：String  可有字段  备注：只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
              openid: res.data.openid, //类型：String  可有字段  备注：用户的标识
              otherUid: res.data.otherUid, //类型：String  可有字段  备注：第三方平台返回的user_id
              totalIntegral: res.data.totalIntegral, //类型：String  可有字段  备注：累计总积分
              integral: res.data.integral, //类型：String  可有字段  备注：累计剩余积分
              money: res.data.money, //类型：String  可有字段  备注：账户金额
              totalMoney: res.data.totalMoney, //类型：String  可有字段  备注：总账户金额
              registerTime: res.data.registerTime, //类型：String  可有字段  备注：注册时间
              registerIp: res.data.registerIp, //类型：String  可有字段  备注：注册ip
              loginTime: res.data.loginTime, //类型：String  可有字段  备注：最后一次登录时间
              loginIp: res.data.loginIp, //类型：String  可有字段  备注：登录ip
              isState: res.data.isState, //类型：String  可有字段  备注：账号状态 0:正常 1:冻结 2是禁言 3拉黑
              isAudit: res.data.isAudit, //类型：String  可有字段  备注：是否已审核：0-是，1-否
              isType: res.data.isType, //类型：String  可有字段  备注：用户类型（0:无 ）
              level: res.data.level, //类型：String  可有字段  备注：用户等级 1-游客（普通用户）2-会员vip
              consumeAmount: res.data.consumeAmount, //类型：String  可有字段  累计消费金额
              createTime: res.data.createTime, //类型：String  可有字段  备注：添加时间
              updateTime: res.data.updateTime, //类型：String  可有字段  备注：更新时间
              memo: res.data.memo, //类型：String  可有字段  备注：备注
              wechatNumber: res.data.wechatNumber, //类型：String  可有字段  备注：微信号
              imgPhotoWallUrl: get_img_url_to_path_list(
                res.data.imgPhotoWallUrl,
                res.data.imgPhotoWallUrlPath
              ), //类型：String  可有字段  备注：照片墙短路径
              carStatus: res.data.userAdditionVo.carStatus,
              childrenStatus: res.data.userAdditionVo.childrenStatus,
              cityId: res.data.userAdditionVo.cityId,
              cityName: res.data.userAdditionVo.cityName,
              coverImgUrl: get_img_url_to_path_list(
                res.data.userAdditionVo.coverImgUrl,
                res.data.userAdditionVo.coverImgUrl
              ),
              duration: res.data.userAdditionVo.duration,
              educationBackground: res.data.userAdditionVo.educationBackground,
              housingStatus: res.data.userAdditionVo.housingStatus,
              introduce: res.data.userAdditionVo.introduce,
              introduceVoiceUrl: res.data.userAdditionVo.introduceVoiceUrl,
              introduceVoiceUrlPath:
                res.data.userAdditionVo.introduceVoiceUrlPath,
              maritalStatus: res.data.userAdditionVo.maritalStatus,
              pinCodesConsUrl: res.data.userAdditionVo.pinCodesConsUrl,
              pinCodesConsUrlPath: res.data.userAdditionVo.pinCodesConsUrlPath,
              pinCodesHandUrl: res.data.userAdditionVo.pinCodesHandUrl,
              pinCodesHandUrlPath: res.data.userAdditionVo.pinCodesHandUrlPath,
              pinCodesProsUrl: res.data.userAdditionVo.pinCodesProsUrl,
              pinCodesProsUrlPath: res.data.userAdditionVo.pinCodesProsUrlPath,
              provinceId: res.data.userAdditionVo.provinceId,
              provinceName: res.data.userAdditionVo.provinceName,
              stature: res.data.userAdditionVo.stature,
              weight: res.data.userAdditionVo.weight ? res.data.userAdditionVo.weight : 100,
              tags: res.data.userAdditionVo.tags
                ? res.data.userAdditionVo.tags.split(',')
                : [],
              userId: res.data.userAdditionVo.userId
            }

            this.userMateSelectionVo = {
              ageOptionalRange: res.data.userMateSelectionVo.ageOptionalRange, // 年龄可选范围：18-99（最小年龄选不限、最多年龄选30，则显示：30岁及以下）
              heightOptionalRange:
                res.data.userMateSelectionVo.heightOptionalRange, // 身高可选范围：145-250（最低选160cm、最高选不限，则显示：160cm及以上）
              educationBackground:
                res.data.userMateSelectionVo.educationBackground, // 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
              maritalStatus: res.data.userMateSelectionVo.maritalStatus, // 婚姻状况：0-未婚、1-离异、2-丧偶
              childrenStatus: res.data.userMateSelectionVo.childrenStatus, // 子女状况：0-无子女、1-有子女
              provinceName: res.data.userMateSelectionVo.provinceName, // 用户所在省份名称
              cityName: res.data.userMateSelectionVo.cityName, // 用户所在城市名称
              provinceId: res.data.userMateSelectionVo.provinceId, // 用户所在省份id
              cityId: res.data.userMateSelectionVo.cityId, // 用户所在城市id
              housingStatus: res.data.userMateSelectionVo.housingStatus, // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
              carStatus: res.data.userMateSelectionVo.carStatus, // 购车情况：0-已购车、1-有需要购车、2-其他
              userId: res.data.userMateSelectionVo.userId
            }
            this.drawer = true
            this.title = '修改资料'
            this.add = 1
            this.active = 1
          })
          .catch(() => {})
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /****************************** 访客/浏览记录 操作 *********************************** */
    /**
     * @memo 获取访客列表
     */
    async get_tabel_visitors_data(pageNum, pageSize, userId) {
      let param = {
        userId: userId, //类型：String  可有字段  备注：用户编号
        pageNum: pageNum, //当前页码
        pageSize: pageSize //每页的条数
      }
      await visitors_list_path(param)
        .then(res => {
          this.tableVisitorsData = res.data.resultList // 列表的数据
          this.searchVisitorsForm.totals = res.data.totalRows // 总长度
          this.searchVisitorsForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchVisitorsForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchVisitorsForm.pageNum > 1) {
            if (res.data.resultList.length == 0) {
              this.get_tabel_visitors_data(
                this.searchVisitorsForm.pageNum - 1,
                this.searchVisitorsForm.pageSize
              )
            }
          } else if (this.searchVisitorsForm.pageNum == 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => {})
    },

    /**
     * @memo 获取访客列表
     */
    async get_tabel_browse_data(pageNum, pageSize, userId) {
      let param = {
        userId: userId, //类型：String  可有字段  备注：真实姓名
        pageNum: pageNum, //当前页码
        pageSize: pageSize //每页的条数
      }
      await browse_list_path(param)
        .then(res => {
          this.tableBrowseData = res.data.resultList // 列表的数据
          this.searchBrowseForm.totals = res.data.totalRows // 总长度
          this.searchBrowseForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchBrowseForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchBrowseForm.pageNum > 1) {
            if (res.data.resultList.length == 0) {
              this.get_tabel_browse_data(
                this.searchBrowseForm.pageNum - 1,
                this.searchBrowseForm.pageSize
              )
            }
          } else if (this.searchBrowseForm.pageNum == 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => {})
    },

    /***************************** 会员卡详情 操作 ****************************** */
    /**
     * @memo 会员卡详情 接口
     */
    async get_card_info_data(id) {
      const res = await user_card_info_path({
        id: id
      })
      return res
    },

    /**
     * @memo 会员卡详情 按钮
     */
    card_info_btn(id) {
      if (id) {
        this.get_card_info_data(id)
          .then(res => {
            this.cardForm = {
              cardId: res.data.cardId,
              cardNum: res.data.cardNum,
              createOperatorId: res.data.createOperatorId,
              createOperatorName: res.data.createOperatorName,
              createTime: res.data.createTime,
              endTime: res.data.endTime,
              id: res.data.id,
              isStatus: res.data.isStatus,
              memberCardVO: {
                content: res.data.memberCardVO.content,
                createTime: res.data.memberCardVO.createTime,
                endTime: res.data.memberCardVO.endTime,
                extraParams1: res.data.memberCardVO.extraParams1,
                id: res.data.memberCardVO.id,
                imgUrl: res.data.memberCardVO.imgUrl,
                imgUrlPath: res.data.memberCardVO.imgUrlPath,
                introduction: res.data.memberCardVO.introduction,
                inventoryCount: res.data.memberCardVO.inventoryCount,
                isEnabled: res.data.memberCardVO.isEnabled,
                isInfinite: res.data.memberCardVO.isInfinite,
                level: res.data.memberCardVO.level,
                memberCardConfigVoList:
                  res.data.memberCardVO.memberCardConfigVoList,
                memo: res.data.memberCardVO.memo,
                name: res.data.memberCardVO.name,
                price: res.data.memberCardVO.price,
                remark: res.data.memberCardVO.remark,
                startTime: res.data.memberCardVO.startTime,
                subtitle: res.data.memberCardVO.subtitle,
                updateTime: res.data.memberCardVO.updateTime,
                validTime: res.data.memberCardVO.validTime,
                validTimeUnit: res.data.memberCardVO.validTimeUnit
              },
              memo: res.data.memo,
              orderId: res.data.orderId,
              orderNum: res.data.orderNum,
              rightsVOList: res.data.rightsVOList,
              startTime: res.data.startTime,
              updateOperatorId: res.data.updateOperatorId,
              updateOperatorName: res.data.updateOperatorName,
              updateTime: res.data.updateTime,
              userId: res.data.userId
            }
            this.get_member_tabel_data(
              this.searchMemberForm.pageNum,
              this.searchMemberForm.pageSize
            )
            this.activeCollapse = ['1', '2']
            this.drawer = true
            this.title = '会员卡详情'
            this.add = 3
          })
          .catch(() => {})
      } else {
        this.$message.error('数据错误请刷新页面')
      }
    },

    /********************************* 使用记录 操作 ************************************ */
    /**
     * @memo 使用记录 获取列表
     */
    async get_member_tabel_data(pageNum, pageSize) {
      const param = {
        pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
        pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
        midCardId: this.cardForm.id // 类型：String  可有字段  备注：关联会员卡id
      }
      await card_member_list_path(param)
        .then(res => {
          this.tableMemberData = res.data.resultList
          this.searchMemberForm.totals = res.data.totalRows
          this.searchMemberForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchMemberForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchMemberForm.pageNum > 1) {
            if (res.data.resultList.length === 0) {
              this.get_member_tabel_data(
                this.searchMemberForm.pageNum - 1,
                this.searchMemberForm.pageSize
              )
            }
          } else if (this.searchMemberForm.pageNum === 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => {})
    },

    /******************** 修改状态 操作 ************************ */
    /**
     * @memo 修改多条状态接口
     */
    async update_state_data(id, isState) {
      if (id) {
        const param = {
          ids: id,
          isState: isState
        }
        await user_update_state_path(param)
          .then(res => {
            this.$message({
              message: res.message,
              type: 'success'
            })
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => {})
      } else {
        this.$message.error('请选择需要修改状态的用户!')
      }
    },

    /**
     * @memo 修改多条推荐状态接口
     */
    async update_type_data(id, isType) {
      if (id) {
        const param = {
          ids: id,
          isType: isType
        }
        await user_update_type_path(param)
          .then(res => {
            this.$message({
              message: res.message,
              type: 'success'
            })
            this.get_tabel_data(
              this.searchForm.pageNum,
              this.searchForm.pageSize
            )
          })
          .catch(() => {})
      } else {
        this.$message.error('请选择需要推荐的用户!')
      }
    },

    /********************** 关闭抽屉 操作 ******************************* */
    /**
     * @memo 抽屉关闭前的回调
     */
    handle_close() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => {})
    },

    /**
     * @memo 点击取消按钮，关闭抽屉
     */
    close_drawer() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => {})
    },

    /**
     * @memo 关闭抽屉
     */
    clear_drawer() {
      this.drawer = false
      this.activeCollapse = []
      this.addForm = {
        id: '',
        trueName: '', //类型：String  可有字段  备注：真实姓名
        email: '', //类型：String  可有字段  备注：邮箱
        birthday: '', //类型：String  可有字段  备注：生日
        pinCodes: '', //类型：String  可有字段  备注：身份证号
        address: '', //类型：String  可有字段  备注：用户住址
        nativePlace: '', //类型：String  可有字段  备注：籍贯
        userName: '', //类型：String  可有字段  备注：用户注册时使用个的帐号
        password: '', //类型：String  可有字段  备注：密码
        nickName: '', //类型：String  可有字段  备注：用户的昵称
        headimgurl: '', //类型：String  可有字段  备注：用户头像
        modilePhone: '', //类型：String  可有字段  备注：手机号
        routineOpenid: '', //类型：String  可有字段  备注：小程序唯一身份ID
        sex: '', //类型：String  可有字段  备注：用户的性别
        city: '', //类型：String  可有字段  备注：用户所在城市
        language: '', //类型：String  可有字段  备注：用户的语言，简体中文为zh_CN
        province: '', //类型：String  可有字段  备注：用户所在省份
        country: '', //类型：String  可有字段  备注：用户所在国家
        unionid: '', //类型：String  可有字段  备注：只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
        openid: '', //类型：String  可有字段  备注：用户的标识
        otherUid: '', //类型：String  可有字段  备注：第三方平台返回的user_id
        totalIntegral: '', //类型：String  可有字段  备注：累计总积分
        integral: '', //类型：String  可有字段  备注：累计剩余积分
        money: '', //类型：String  可有字段  备注：账户金额
        totalMoney: '', //类型：String  可有字段  备注：总账户金额
        registerTime: '', //类型：String  可有字段  备注：注册时间
        registerIp: '', //类型：String  可有字段  备注：注册ip
        loginTime: '', //类型：String  可有字段  备注：最后一次登录时间
        loginIp: '', //类型：String  可有字段  备注：登录ip
        isState: '', //类型：String  可有字段  备注：账号状态 0:正常 1:冻结 2是禁言 3拉黑
        isAudit: '', //类型：String  可有字段  备注：是否已审核：0-是，1-否
        isType: '', //类型：String  可有字段  备注：用户类型（0:无 ）
        level: '', //类型：String  可有字段  备注：用户等级 1-游客（普通用户）2-会员vip
        consumeAmount: '', //类型：String  可有字段  累计消费金额
        createTime: '', //类型：String  可有字段  备注：添加时间
        updateTime: '', //类型：String  可有字段  备注：更新时间
        memo: '', //类型：String  可有字段  备注：备注
        userMateSelectionVo: {
          ageOptionalRange: '',
          carStatus: '',
          childrenStatus: '',
          cityId: '',
          cityName: '',
          educationBackground: '',
          heightOptionalRange: '',
          housingStatus: '',
          maritalStatus: '',
          provinceId: '',
          provinceName: '',
          userId: ''
        }, //类型：Object  可有字段  备注：择偶标准
        userAdditionVo: {
          carStatus: '',
          childrenStatus: '',
          cityId: '',
          cityName: '',
          coverImgUrl: '',
          coverImgUrlPath: '',
          duration: '',
          educationBackground: '',
          housingStatus: '',
          introduce: '',
          introduceVoiceUrl: '',
          introduceVoiceUrlPath: '',
          maritalStatus: '',
          pinCodesConsUrl: '',
          pinCodesConsUrlPath: '',
          pinCodesHandUrl: '',
          pinCodesHandUrlPath: '',
          pinCodesProsUrl: '',
          pinCodesProsUrlPath: '',
          provinceId: '',
          provinceName: '',
          stature: '',
          tags: '',
          userId: ''
        } //类型：Object  可有字段  备注：用户附加信息
      }
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.addForm.clearValidate()
      })
    },

    /*********************** 列表judge 操作 *************************** */
    /**
     * @memo 是否上线  状态
     */
    isStateJudge(isState) {
      if (isState == 0) {
        return '上线'
      } else if (isState == 1) {
        return '下线'
      }
    },

    /**
     * @memo 是否实名认证  状态
     */
    isAuditJudge(isAudit) {
      if (isAudit == 0) {
        return '已实名'
      } else if (isAudit == 1) {
        return '未认证'
      }
    },

    /**
     * @memo 是否推荐  状态
     */
    isTypeJudge(isType) {
      if (isType == 0) {
        return '推荐'
      } else if (isType == 1) {
        return '不推荐'
      }
    },

    /**
     * @memo 性别
     */
    sexJudge(sex) {
      if (sex == 0) {
        return '未知'
      } else if (sex == 1) {
        return '男'
      } else if (sex == 2) {
        return '女'
      }
    },

    /**
     * @memo 学历
     */
    educationJudge(education) {
      if (education == 0) {
        return '高中及以下'
      } else if (education == 1) {
        return '大专'
      } else if (education == 2) {
        return '本科'
      } else if (education == 3) {
        return '双学位'
      } else if (education == 4) {
        return '硕士'
      } else if (education == 5) {
        return '博士'
      } else {
        return '暂无数据'
      }
    },

    /**
     * @memo 婚姻状况
     */
    marriageStateJudge(maritalStatus) {
      if (maritalStatus == 0) {
        return '未婚'
      } else if (maritalStatus == 1) {
        return '已婚'
      } else if (maritalStatus == 2) {
        return '离异'
      } else if (maritalStatus == 3) {
        return '丧偶'
      } else {
        return '暂无数据'
      }
    },

    /**
     * @memo 子女状况
     */
    familyStateJudge(familyState) {
      if (familyState == 0) {
        return '无子女'
      } else if (familyState == 1) {
        return '有子女'
      } else {
        return '暂无数据'
      }
    },

    /**
     * @memo 住房情况
     */
    housingStateJudge(housingState) {
      if (housingState == 0) {
        return '已购房'
      } else if (housingState == 1) {
        return '租房'
      } else if (housingState == 2) {
        return '有需要购房'
      } else if (housingState == 3) {
        return '其他'
      } else {
        return '暂无数据'
      }
    },

    /**
     * @memo 购车情况
     */
    shoppingStateJudge(shoppingState) {
      if (shoppingState == 0) {
        return '已购车'
      } else if (shoppingState == 1) {
        return '有需要购车'
      } else if (shoppingState == 2) {
        return '其他'
      } else {
        return '暂无数据'
      }
    },

    /**
     * @memo 会员等级
     */
    levelJudge(level) {
      if (level == 0) {
        return '普通用户'
      } else if (level == 1) {
        return 'VIP会员'
      }
    },

    /*********************** 会员卡权益列表judge 操作 *************************** */
    /**
     * @memo 类型
     */
    isTypejudge(isType) {
      if (isType == 0) {
        return '红娘牵线'
      } else if (isType == 1) {
        return '活动'
      } else {
        return '无数据'
      }
    },

    /*********************** 列表分页 操作 *********************************** */
    /**
     * @memo 访问列表分页 相关方法
     */
    change_size_visitors_btn(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_visitors_data(1, val)
    },
    change_current_visitors_btn(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_visitors_data(val, this.searchVisitorsForm.pageSize)
    },

    /**
     * @memo 浏览列表分页 相关方法
     */
    change_size_browse_btn(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_browse_data(1, val)
    },
    change_current_browse_btn(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_browse_data(val, this.searchBrowseForm.pageSize)
    },

    /**
     * @memo 会员卡使用记录列表分页 相关方法
     */
    change_size_member_btn(val) {
      // 分页加载常见问题列表加载
      this.get_member_tabel_data(1, val)
    },
    change_current_member_btn(val) {
      // 分页加载常见问题列表加载
      this.get_member_tabel_data(val, this.searchMemberForm.pageSize)
    }
  }
}
