/**
 * @description: 系统消息-消息日志管理
 * @author: leroy
 * @Compile：2021-12-28 09：30
 * @update:lx(2021-12-28 09：30)
 */
import {
    message_log_path, // 列表
} from '../../../api/message'
import {
    date_format
} from '../../../utils/date/dateFormat'

export default {
    data() {
        return {
            date_format,  //时间处理

            // 日志管理 检索字段
            searchForm: {
                userName: '',   // 类型：String  可有字段  备注：用户昵称
            },
            tableData: [], // 列表表单数据
        }
    },
    mounted() {
        this.to_load()
    },
    methods: {
        /**
         * @memo 进入此页面的加载
         */
        to_load() {
            this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
        },

        /**
         * @memo 日志管理 获取列表
         */
        async get_tabel_data(pageNum, pageSize) {
            const param = {
                pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
                pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
            }
            await message_log_path(param)
                .then(res => {
                    this.tableData = res.data.resultList
                    this.searchForm.totals = res.data.totalRows
                    this.searchForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchForm.pageNum > 1) {
                        if (res.data.resultList.length === 0) {
                            this.get_tabel_data(
                                this.searchForm.pageNum - 1,
                                this.searchForm.pageSize
                            )
                        }
                    } else if (this.searchForm.pageNum === 0) {
                        return this.$message.warning('没有更多数据')
                    }
                }).catch(() => { })
        },
    },

    /*********************** 列表judge 操作 ****************************************** */
    /**
     * @memo 消息类型 judge
     */
    isMessageJudge(isMessage) {
        if (isMessage == 0) {
            return '公告'
        } else if (isMessage == 1) {
            return '通知'
        }
    },
}
