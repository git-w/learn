/**
 * @description: 系统信息-系统信息管理
 * @author: leroy
 * @Compile：2021-12-20 11：00
 * @update:lx(2021-12-20 11：00)
 */
import {
    message_list_path,  // 列表
    message_save_path,  // 添加
    message_update_path,  // 修改
    message_delete_path,  // 删除
} from '../../../api/message'

import {
    user_list_path  // 用户列表
} from '../../../api/user'

import {
    date_format,
} from '../../../utils/date/dateFormat'

import {
    unique_str,
} from '../../../utils/unique'

export default {
    data() {
        // 选择用户 校验
        let isUserId = (rule, value, callback) => {
            if (this.userIds.length > 0) {
                callback();
            } else {
                return callback(new Error("请选择需要发放的用户"));
            }
        }
        return {
            ids: '',
            date_format,  // 时间处理方法
            unique_str,  // 数组去重

            // 系统消息 检索字段
            searchForm: {
                isMessage: '', //类型：Number  必有字段  备注：信息类型 0:公告 1:个人信息
            },

            tableData: [], // 列表表单数据

            // 信息类型
            isMessageList: [{
                value: 0,
                label: '公告'
            }, {
                value: 1,
                label: '通知'
            }],

            drawer: false, //控制抽屉的开合
            title: '', //控制抽屉的头部
            add: 1, //判断抽屉显示 添加修改 - 1

            // 系统消息 表单数据
            addForm: {
                id: '',
                title: '',  // 类型：Number  必有字段  备注：消息标题备注
                content: '',  // 类型：Number  必有字段  备注：内容
                isMessage: '',  // 类型：Number  必有字段  备注：信息类型 0:公告 1:通知
                isUserAll: '',  // 类型：Number  必有字段  备注：用户类型 0:通知全部用户 1:部分用户
                userIds: '',  // 类型：String  必有字段  备注：用户ids
                isType: '',  // 类型：String  必有字段  备注：通知的话选择新消息类型消息类型:1审核成功通知 2审核失通知  3会员卡购买成功通知 4新用户注册成功通知  5免费会员卡领取成功通知
            },
            tableUserData: [],  // 用户列表数组
            // 用户列表 检索字段
            searchUserForm: {
                pageNum: 1,  //类型：Number  必有字段  备注：页码 默认第一页
                pageSize: 10,  //类型：Number  必有字段  备注：条码 每页默认十条
                totals: 0,
            },
            userIds: [], // 选中用户的id数组

            // 系统消息 表单校验
            rules: {
                // 标题
                title: [{
                    required: true,
                    message: '请输入标题',
                    trigger: 'blur'
                }],

                // 内容
                content: [{
                    required: true,
                    message: '请输入内容',
                    trigger: 'blur'
                }],

                // 消息类型
                isMessage: [{
                    required: true,
                    message: '请选择消息类型',
                    trigger: 'change'
                }],

                // 通知类型
                isType: [{
                    required: true,
                    message: '请选择通知类型',
                    trigger: 'change'
                }],

                // 是否发布
                isUndo: [{
                    required: true,
                    message: '请选择发布状态',
                    trigger: 'change'
                }],

                // 发送用户类型
                isUserAll: [{
                    required: true,
                    message: '请选择发送用户类型',
                    trigger: 'change'
                }],

                // 选择用户
                userIds: [{
                    required: true,
                    validator: isUserId
                }]
            }
        }
    },
    mounted() {
        this.to_load()
    },
    methods: {
        /**
         * @memo 进入此页面的加载
         */
        to_load() {
            this.get_tabel_data(this.searchForm.pageNum, this.searchForm.pageSize)
        },

        /**
         * @memo 检索重置按钮
         */
        reset_btn() {
            this.get_tabel_data(1, this.searchForm.pageSize) //获取列表
        },

        /**
         * @memo 会员卡管理  获取列表
         */
        async get_tabel_data(pageNum, pageSize) {
            const param = {
                pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
                pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
            }
            await message_list_path(param)
                .then(res => {
                    this.tableData = res.data.resultList
                    this.searchForm.totals = res.data.totalRows
                    this.searchForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchForm.pageNum > 1) {
                        if (res.data.resultList.length === 0) {
                            this.get_tabel_data(
                                this.searchForm.pageNum - 1,
                                this.searchForm.pageSize
                            )
                        }
                    } else if (this.searchForm.pageNum === 0) {
                        return this.$message.warning('没有更多数据')
                    }
                }).catch(() => { })
        },

        /**
         * @memo 下拉框选中的按钮的操作  更多操作
         */
        handle_command(command) {
            if (command.button == 'delete_btn') {
                //删除
                this.delete_btn(command.row)
            } else if (command == 'delete_btn') {
                //多选删除
                this.delete_btn(this.ids)
            } else if (command.button == 'change_btn') {
                //修改
                this.change_btn(command.row)
            } else if (command.button == 'send_btn') {
                //发送公告
                this.send_btn(command.row)
            }
        },

        /**
         * @memo 下拉框选中的操作按钮，并将此行的数据携带数据回调，进行操作
         */
        compose_value(item, row) {
            return {
                button: item,
                row: row
            }
        },

        /*************************** 添加、修改 操作 ****************************** */
        /**
         * @memo 添加-跳往添加界面
         */
        add_btn() {
            this.drawer = true
            this.add = 1
            this.title = '添加消息'
            this.addForm = {
                id: '',
                title: '',  // 类型：Number  必有字段  备注：消息标题备注
                content: '',  // 类型：Number  必有字段  备注：内容
                isMessage: 0,  // 类型：Number  必有字段  备注：信息类型 0:公告 1:通知
                isUserAll: 1,  // 类型：Number  必有字段  备注：用户类型 0:通知全部用户 1:部分用户
                isUndo: 1,  // 类型：Number  必有字段  备注：是否发布 0:发布 1:未发布
                userIds: '',  // 类型：String  必有字段  备注：用户ids
                isType: 0,  // 类型：String  必有字段  备注：通知的话选择新消息类型消息类型:1审核成功通知 2审核失通知  3会员卡购买成功通知 4新用户注册成功通知  5免费会员卡领取成功通知
            }
            // 关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /**
         * @memo 修改-跳往修改界面
         */
        change_btn(row) {
            this.drawer = true
            this.add = 1
            this.title = '添加消息'
            this.addForm = {
                id: row.id,
                title: row.title,  // 类型：Number  必有字段  备注：消息标题备注
                content: row.content,  // 类型：Number  必有字段  备注：内容
                isMessage: row.isMessage,  // 类型：Number  必有字段  备注：信息类型 0:公告 1:通知
                isUndo: row.isUndo,  // 类型：Number  必有字段  备注：是否发布 0:发布 1:未发布
                isUserAll: 1,  // 类型：Number  必有字段  备注：用户类型 0:通知全部用户 1:部分用户
                userIds: '',  // 类型：String  必有字段  备注：用户ids
                isType: row.isType,  // 类型：String  必有字段  备注：通知的话选择新消息类型消息类型:1审核成功通知 2审核失通知  3会员卡购买成功通知 4新用户注册成功通知  5免费会员卡领取成功通知
            }
            // 关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /**
         * @memo 保存修改方法
         * 根据有无id来判断调用的接口是新增还是修改
         */
        async save_update_data() {
            let param = {
                id: this.addForm.id,
                title: this.addForm.title,  // 类型：Number  必有字段  备注：消息标题备注
                content: this.addForm.content,  // 类型：Number  必有字段  备注：内容
                isMessage: this.addForm.isMessage,  // 类型：Number  必有字段  备注：信息类型 0:公告 1:通知
                isUndo: this.addForm.isUndo,  // 类型：Number  必有字段  备注：是否发布 0:发布 1:未发布
                isType: this.addForm.isType,  // 类型：String  必有字段  备注：通知的话选择新消息类型消息类型:1审核成功通知 2审核失通知  3会员卡购买成功通知 4新用户注册成功通知  5免费会员卡领取成功通知
                isUserAll: this.addForm.isUserAll,  // 类型：Number  必有字段  备注：用户类型 0:通知全部用户 1:部分用户
                userIds: this.addForm.userIds,  // 类型：String  必有字段  备注：用户ids
            }
            if (!this.addForm.id) {
                await message_save_path(param)
                    .then(res => {
                        //添加成功
                        this.$message.success(res.message)
                        //添加成功返回列表页
                        this.clear_drawer()
                        this.get_tabel_data(1, this.searchForm.pageSize)
                    })
            } else if (this.addForm.id) {
                await message_update_path(param)
                    .then(res => {
                        //添加成功
                        this.$message.success(res.message)
                        //添加成功返回列表页
                        this.clear_drawer()
                        this.get_tabel_data(1, this.searchForm.pageSize)
                    })
            }

        },

        /**
         * @memo 提交按钮
         */
        submit_btn(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    this.save_update_data()
                } else {
                    // 提交失败
                    return false
                }
            })
        },

        /************************************ 发送公告 操作 ****************************************** */
        /**
         * @memo 发送公告 按钮
         */
        send_btn(row) {
            this.drawer = true
            this.add = 2
            this.title = '发送公告'
            this.addForm = {
                id: row.id,
                title: row.title,  // 类型：Number  必有字段  备注：消息标题备注
                content: row.content,  // 类型：Number  必有字段  备注：内容
                isMessage: row.isMessage,  // 类型：Number  必有字段  备注：信息类型 0:公告 1:通知
                isType: row.isType,  // 类型：String  必有字段  备注：通知的话选择新消息类型消息类型:1审核成功通知 2审核失通知  3会员卡购买成功通知 4新用户注册成功通知  5免费会员卡领取成功通知
                isUndo: row.isUndo,  // 类型：Number  必有字段  备注：是否发布 0:发布 1:未发布
                isUserAll: 0,  // 类型：Number  必有字段  备注：用户类型 0:通知全部用户 1:部分用户
                userIds: '',  // 类型：String  必有字段  备注：用户ids
            }
            this.get_tabel_user_data(this.searchUserForm.pageNum, this.searchUserForm.pageSize)  // 请求用户列表
            // 关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /**
         * @memo 发送公告方法
         */
        async save_send_data() {
            let param = {
                id: this.addForm.id,
                title: this.addForm.title,  // 类型：Number  必有字段  备注：消息标题备注
                content: this.addForm.content,  // 类型：Number  必有字段  备注：内容
                isMessage: this.addForm.isMessage,  // 类型：Number  必有字段  备注：信息类型 0:公告 1:通知
                isUndo: this.addForm.isUndo,  // 类型：Number  必有字段  备注：是否发布 0:发布 1:未发布
                isUserAll: this.addForm.isUserAll,  // 类型：Number  必有字段  备注：用户类型 0:通知全部用户 1:部分用户
                userIds: this.userIds.length > 0
                    ? this.userIds.join(",")
                    : "",  // 类型：String  必有字段  备注：用户ids
                isType: this.addForm.isType,  // 类型：String  必有字段  备注：通知的话选择新消息类型消息类型:1审核成功通知 2审核失通知  3会员卡购买成功通知 4新用户注册成功通知  5免费会员卡领取成功通知
            }
            await message_update_path(param)
                .then(res => {
                    //添加成功
                    this.$message.success(res.message)
                    //添加成功返回列表页
                    this.clear_drawer()
                    this.get_tabel_data(1, this.searchForm.pageSize)
                })

        },

        /**
         * @memo 发送公告 提交按钮
         */
        submit_send_btn(formName) {
            this.$refs[formName].validate(valid => {
                if (valid) {
                    this.save_send_data()
                } else {
                    // 提交失败
                    return false
                }
            })
        },

        /************************************ 选择用户 操作 ****************************************** */
        /**
         * @memo 用户管理 获取列表
         */
        async get_tabel_user_data(pageNum, pageSize) {
            const param = {
                pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
                pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
            }
            await user_list_path(param)
                .then(res => {
                    this.tableUserData = res.data.resultList
                    this.searchUserForm.totals = res.data.totalRows
                    this.searchUserForm.pageSize = res.data.pageSize // 当前页码条数
                    this.searchUserForm.pageNum = res.data.pageNum // 请求页码
                    if (this.searchUserForm.pageNum > 1) {
                        if (res.data.resultList.length === 0) {
                            this.get_tabel_user_data(
                                this.searchUserForm.pageNum - 1,
                                this.searchUserForm.pageSize
                            )
                        }
                    } else if (this.searchUserForm.pageNum === 0) {
                        return this.$message.warning('没有更多数据')
                    }
                })
        },

        /**
         * @memo 列表分页 相关方法
         */
        change_user_size_btn(val) {
            // 分页加载常见问题列表加载
            this.get_tabel_user_data(1, val)
        },
        change_user_current_btn(val) {
            // 分页加载常见问题列表加载
            this.get_tabel_user_data(val, this.searchUserForm.pageSize)
        },

        /**
         * @memo 用户列表单选
         */
        select_user(selection, row) {
            if (selection.includes(row)) {
                // 如果包含说明选中了
                this.userIds.push(row.id);
            } else {
                // 如果没包含说明取消选中了
                for (let i in this.userIds) {
                    if (this.userIds[i] == row.id) {
                        this.userIds.splice(i, 1);
                    }
                }
            }
            // id数组去重
            this.userIds = this.unique_str(this.userIds);
        },

        /**
         * @memo 用户列表全选
         * selection.length>0说明是全选
         * selection.length<0说明是取消全选
         */
        select_user_all(selection) {
            if (selection.length > 0) {
                for (let i in selection) {
                    this.userIds.push(selection[i].id);
                }
            } else {
                for (let l in this.tableUserData) {
                    for (let j in this.userIds) {
                        if (this.userIds[j] == this.tableUserData[l].id) {
                            this.userIds.splice(j, 1);
                        }
                    }
                }
            }
            // id数组去重
            this.userIds = this.unique_str(this.userIds);
        },



        /************************ 删除 操作 **************************** */
        /**
         * @memo 删除接口
         */
        async delete_data(id) {
            const res = await message_delete_path({
                ids: id
            })
            return res
        },

        /**
         * @memo 删除按钮
         */
        delete_btn(id) {
            if (id) {
                this.$confirm('是否确认删除此消息，请确认删除操作。', '操作提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                })
                    .then(() => {
                        this.delete_data(id)
                            .then(res => {
                                this.$message.success(res.message)
                                this.get_tabel_data(
                                    this.searchForm.pageNum,
                                    this.searchForm.pageSize
                                )
                            })
                            .catch(() => { })
                    })
                    .catch(() => { })
            } else {
                this.$message.error('请选择需要删除的内容!')
            }
        },

        /********************** 关闭抽屉 操作 ******************************* */
        /**
         * @memo 抽屉关闭前的回调
         */
        handle_close() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },

        /**
         * @memo  点击取消按钮，关闭抽屉
         */
        close_drawer() {
            this.$confirm('确认关闭？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            })
                .then(() => {
                    this.clear_drawer()
                })
                .catch(() => { })
        },
        /**
         * @memo 关闭抽屉
         */
        clear_drawer() {
            this.drawer = false
            this.addForm = {
                id: '',
                title: '',  // 类型：Number  必有字段  备注：消息标题备注
                content: '',  // 类型：Number  必有字段  备注：内容
                isMessage: '',  // 类型：Number  必有字段  备注：信息类型 0:公告 1:通知
                isUserAll: '',  // 类型：Number  必有字段  备注：用户类型 0:通知全部用户 1:部分用户
                userIds: '',  // 类型：String  必有字段  备注：用户ids
                isType: '',  // 类型：String  必有字段  备注：通知的话选择新消息类型消息类型:1审核成功通知 2审核失通知  3会员卡购买成功通知
            }
            //关闭表单验证
            this.$nextTick(() => {
                this.$refs.addForm.clearValidate()
            })
        },

        /*********************** 列表judge 操作 ****************************************** */
        /**
         * @memo 消息类型 judge
         */
        isMessageJudge(isMessage) {
            if (isMessage == 0) {
                return '公告'
            } else if (isMessage == 1) {
                return '通知'
            }
        },

        /**
         * @memo 通知类型 judge
         */
        informTypeJudge(isType) {
            if (isType == 1) {
                return '审核成功'
            } else if (isType == 2) {
                return '审核失败'
            } else if (isType == 3) {
                return '会员卡购买成功'
            } else if (isType == 4) {
                return '新用户注册成功'
            } else if (isType == 5) {
                return '免费会员卡领取成功'
            }
        },

        /**
         * @memo 是否发布 judge
         */
        isUndoJudge(isUndo) {
            if (isUndo == 0) {
                return '发布'
            } else if (isUndo == 1) {
                return '不发布'
            }
        },
        

        /*********************** 用户列表judge 操作 *************************** */
        /**
         * @memo 是否上线  状态
         */
        isStateJudge(isState) {
            if (isState == 0) {
                return '上线'
            } else if (isState == 1) {
                return '下线'
            }
        },

        /**
         * @memo 是否推荐  状态
         */
        isTypeJudge(isType) {
            if (isType == 0) {
                return '推荐'
            } else if (isType == 1) {
                return '不推荐'
            }
        },

        /**
         * @memo 性别
         */
        sexJudge(sex) {
            if (sex == 0) {
                return '未知'
            } else if (sex == 1) {
                return '男'
            } else if (sex == 2) {
                return '女'
            }
        },
    }
}
