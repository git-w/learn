/*
 * @description: 登录页面
 * @author: leroy
 * @Compile：2020-11-30 18：00
 * @update: leroy(2020-11-30 19：00)
 * memo：vue-puzzle-vcode为
 */
import Vcode from 'vue-puzzle-vcode'
import md5 from 'js-md5'
import Cookies from "js-cookie";
import Fingerprint2 from 'fingerprintjs2' // 引入
import { mapActions } from 'vuex'
import { get_captcha_path } from '@/api/login.js'
export default {
  components: {
    Vcode
  },
  data() {
    return {
      loading: false,//是否登录中
      loginState: false, // 登录状态
      base64_images: '',//验证码图片
      param: {
        username: '',
        password: '',
        rememberMe: false,
        code: '',
        token: ''
      },
      isShow: false, // 是否显示拼图校验
      isPassing: false, // 是否通过
      loginMessage: '', // 登录后的
      rules: {
        username: [
          {
            required: true,
            message: '请输入用户名',
            trigger: 'blur'
          }
        ],
        password: [
          {
            required: true,
            message: '请输入密码',
            trigger: 'blur'
          }
        ],
        code: [
          {
            required: true,
            message: '请输入验证码',
            trigger: 'blur'
          }
        ]
      },
      isBtnLoading: false
    }
  },
  watch: {
    param: {
      handler(newValue, oldValue) {
        console.log(newValue, oldValue)
        this.loginState = false
      },
      deep: true
    }
  },
  created() {
    this.to_load();
  },
  mounted() {

 },

  methods: {

    /**
         * @memo 进入此页面的加载
         */
    to_load() {
      // 初始化验证码
      this.get_captcha_data();
      this.getCookie();
    },
    /**
     * 获取验证码
     */
    async get_captcha_data() {
    
      await get_captcha_path({}).then(res => {
        //添加成功
        this.param.token = res.token;
        this.base64_images = res.images;
       
      })
        .catch(() => { })
    },
    getCookie() {
      const username = Cookies.get("username");
      const password = Cookies.get("password");
      const rememberMe = Cookies.get('rememberMe')
      this.param = {
        username: username === undefined ? this.param.username : username,
        password: password === undefined ? this.param.password : decrypt(password),
        rememberMe: rememberMe === undefined ? false : Boolean(rememberMe)
      };
    },
    /**
     * 获取设备或浏览器唯一标识
     */
     getvisitorId() {
				let excludes = {};
				let options = {
					excludes: excludes
				}
				console.log(options)
				Fingerprint2.get(options, function(components) {
					// 参数
					const values = components.map(function(component) {
						return component.value
					});
          console.log("components--------------"+components);
					// 指纹
					const murmur = Fingerprint2.x64hash128(values.join(''), 31);
					sessionStorage.setItem("Finger", murmur); //存入标识值
					 console.log("murmur--------------"+murmur);
				});
    },
    /**
     * 用户通过了验证
     */
    on_success() {
      this.loading = true;
      this.isShow = false
      const param = {
        username: this.param.username, // 类型：String  必有字段  备注：无
        password: md5(this.param.password), // 类型：String  必有字段  备注：无
        code: this.param.code, // 类型：String  必有字段  备注：无
        token: this.param.token, // 类型：String  必有字段  备注：无

        $router: this.$router,
        $route: this.$route
      }
      // 调用登录接口
      this.login(param).then(res => {
        console.log(res)
        if (res == "error") {
          this.loading = false;
          this.param.code = '';
          this.get_captcha_data();
        }else{
          if (this.param.rememberMe) {
            Cookies.set("username", this.param.username, { expires: 30 });
            Cookies.set("password", encrypt(this.param.password), { expires: 30 });
            Cookies.set('rememberMe', this.param.rememberMe, { expires: 30 });
          } else {
            Cookies.remove("username");
            Cookies.remove("password");
            Cookies.remove('rememberMe');
          }
    
        }

      })
    },
    /**
     * 用户点击遮罩层，应该关闭模态框
     */
    on_close() {
      this.loginState = true
      this.loginMessage = '验证失败！'
    },

    /**
     * 提交信息登录
     */
    submit_btn() {
      if (!this.param.username) {
        this.loginState = true
        this.loginMessage = '请输入账户！'
      } else if (!this.param.password) {
        this.loginState = true
        this.loginMessage = '请输入密码！'
      } else if (!this.param.code) {
        this.loginState = true
        this.loginMessage = '请输入验证码！'
      } else if (
        this.param.username.length > '4' &&
        this.param.password.length > '5'
      ) {
        this.loginState = false
        this.isShow = true
      } else if (
        this.param.username.length < '5' &&
        this.param.password.length > '5'
      ) {
        this.loginState = true
        this.loginMessage = '请输入最少五个字符的账户！'
      } else if (
        this.param.username.length > '4' &&
        this.param.password.length < '6'
      ) {
        this.loginState = true
        this.loginMessage = '请输入最少六个字符的密码！'
      } else if (
        this.param.username.length < '5' &&
        this.param.password.length < '6'
      ) {
        this.loginState = true
        this.loginMessage = '请输入正确的账户和密码！'
      }
    },

    /**
     * 输入账户把校验规则隐藏
     */
    input_mobile_phone() {
      this.loginState = false
    },

    /**
     * vux 的辅助函数 此方法香的一批
     * 换种写法就是this.$store.commit('user/login',data);
     */
    ...mapActions({
      login: 'user/login'
    })
  }
}
