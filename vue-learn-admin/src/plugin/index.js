/**
 *  基于vue扩展功能(全局组件，原型方法|属性，自定义指令，过滤器)
 *  这里指引入公共的组件，其他组件在使用页面进行按需引入的形式导入，否则会引起首次加载慢的问题
 */
import MyBread from '@/components/common/Bread/Bread.vue'
import pages from '@/components/common/Pages/Pages.vue'
import MyHeader from '@/components/common/MyHeader/MyHeader.vue'
import MySidebar from '@/components/common/Sidebar/Sidebar.vue'
import ImageUpload from '@/components/content/ImageUpload/ImageUpload.vue'
import Editor from '@/components/common/QuillEditor/QuillEditor.vue'
import MyTag from '@/components/common/Tags/Tags.vue'
export default {
  install: Vue => {
    // Vue 使用vue的构造函数  当main.js使用use的使用传入进来的
    Vue.component(MyBread.name, MyBread)
    Vue.component(MyHeader.name, MyHeader)
    Vue.component(MySidebar.name, MySidebar)
    Vue.component(pages.name, pages)
    Vue.component(ImageUpload.name, ImageUpload)
    Vue.component(Editor.name, Editor)
    Vue.component(MyTag.name, MyTag)
  }
}
