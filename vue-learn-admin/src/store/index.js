import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const files = require.context('./modules', false, /\.js$/)
const modules = {}
files.keys().forEach(key => {
  modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default
})
export default new Vuex.Store({
  state: {
    filePathList: [],
    sumbmitLoading: false,
    tableLoading: false,
    activityOrderNum: "",  // 活动订单号
    cardNum: "",  // 会员卡id
  },
  mutations: {
    setFilePathList(state, filePathList) {
      state.filePathList = filePathList
    },
    setSumbmitLoading(state, sumbmitLoading) {
      state.sumbmitLoading = sumbmitLoading
    },
    setTableLoading(state, tableLoading) {
      state.tableLoading = tableLoading
    },
    // set 活动订单号
    setActivityOrderNum(state, activityOrderNum) {
      state.activityOrderNum = activityOrderNum
    },
    // set 会员卡id
    setCardNum(state, cardNum) {
      state.cardNum = cardNum
    },
  },
  actions: {},
  getters: {
    getFilePathList: state => state.filePathList,
    getSumbmitLoading: state => state.sumbmitLoading,
    getTableLoading: state => state.tableLoading,
    getActivityOrderNum: state => state.activityOrderNum, // get 活动订单号
    getCardNum: state => state.cardNum, // get 会员卡id
  },
  modules
})
