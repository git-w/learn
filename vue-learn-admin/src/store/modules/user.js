/**
 * 引入登录接口方法
 */
import { login_path, admin_info_path } from '@/api/login.js'

import { Message } from 'element-ui'
/**
 * 引入cookie的方法存、取、删 （token、uid） 中交建中暂不用uid  setUid,
 */
import {
  get_token,
  set_token,
  remove_token,
  set_uid,
  get_uid,
  remove_uid,
  remove_menu_list
} from '@/utils/auth'
/**
 * 路由重定向
 */
import { resetRouter } from '@/router'

const LOGIN = 'LOGIN' // 获取用户信息 这么命名是为了区分actions里的longin方法
const SetUserData = 'SetUserData' // 获取用户信息
const LOGOUT = 'LOGOUT' // 退出登录、清除用户数据
const USER_DATA = 'adminInfo' // 用户数据

export default {
  namespaced: true, // 的方式使其成为带命名空间的模块。保证在变量名一样的时候，添加一个父级名拼接
  state: {
    token: get_token() || '', // 如果是重新登录那么在mutations更新state中的token，如果不是重新登录的，就从cookie中拿token
    uid: get_uid() || '',
    user: JSON.parse(sessionStorage.getItem(USER_DATA) || null),
    menulist: []
  },
  mutations: {
    // 登录后将token存储起来
    [LOGIN](state, data) {
      debugger
      let userToken = data.data.authorization
      let userId = data.data.userId
      state.token = userToken
      state.uid = userId
      set_token(userToken)
      set_uid(userId)
      // setUid(uid) 暂不用uid
    },
    // 将userdata存储在localStorage
    [SetUserData](state, userData = {}) {
      state.user = userData
      sessionStorage.setItem(USER_DATA, JSON.stringify(userData))
    },

    [LOGOUT](state) {
      state.user = null
      state.token = null
      remove_token()
      remove_uid()
      sessionStorage.removeItem(USER_DATA)
      resetRouter()
      remove_menu_list()
    }
  },
  actions: {
    // state 不是回调传过来的是上边的state
    async login(state, data) {
      
      try {
        let res = await login_path({
          username: data.username, //类型：String  必有字段  备注：无
          password: data.password, //类型：String  必有字段  备注：无
          code: data.code, //类型：String  必有字段  备注：无
          token: data.token //类型：String  必有字段  备注：无
        })

        // 调用 mutations 里的 LOGIN方法

        state.commit(LOGIN, res)
        state.dispatch('admin_info_data') // 调用本actions里的方法
        Message({
          message: res.message,
          type: 'success',
          duration: 5 * 1000
        })

        data.$router.replace({
          path: '/aside'
        })
        return "succees";
      } catch (error) {
        console.log(error)
        return "error";
      }
    },

    // state 不是回调传过来的是上边的state
    async admin_info_data(state) {
      try {
        let res = await admin_info_path({})
        state.commit(SetUserData, res.data)
      } catch (error) {
        console.log(error)
      }
    },

  },
  getters: {
    token(state) {
      return state.token
    },
    uid(state) {
      return state.uid
    },
    grt_user_data(state) {
      return state.user
    }
  }
}
