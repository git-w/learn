/**
 * 商品部分的store
 * 主要解决商品部分的父子传值的问题
 */

export default {
  namespaced: true, // 的方式使其成为带命名空间的模块。保证在变量名一样的时候，添加一个父级名拼接
  state: {
    attrDetailList: [], // 规格明细列表 
  },
  mutations: {},
  actions: {},
  getters: {}
}
