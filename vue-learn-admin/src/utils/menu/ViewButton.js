function is_view_button (states) {
  var isKey = false

  let matched = this.$route.matched

  if (!matched) {
    return isKey
  }
  if (!states) {
    return isKey
  }
  // 获取缓存里面的菜单
  var menuList = JSON.parse(window.sessionStorage.getItem('menuList'))

  var submenu = matched[1].path // 获取当前路由的path

  var level2Submenu = matched[2].path
  for (var i in menuList) {
    var path = menuList[i].attributes.Url

    // 查看是否有一级的权限
    if (path && path.match(submenu)) {
      var children = menuList[i].children
      // 查看是否有二级的权限
      for (var k in children) {
        var level2path = children[k].attributes.Url
        if (level2path && level2path.match(level2Submenu)) {
          // 如果二级分类存在查看是否有三级的权限
          var children1 = children[k].children
          for (var j in children1) {
            var submenu1 = children1[j].attributes.ButtonType
            if (submenu1 === states) {
              // 如果二级分类存在查看是否有三级的权限
              isKey = true
              return isKey
            }
          }
        }
      }
    }
  }
  return isKey
}

export default is_view_button
