import { file_uploadVideo_path } from '../api/common'
/**
 * 此文件内的方法是上传文件的方法
 * 其中包含
 * 上传单张图片和多张图片
 * 上传多个附件
 * 上传多个音频文件
 * 上传多个视频文件
 */
/**
 * 单个音频文件
 */
export async function upload_audios_data(vueObject, file) {
  vueObject.submitLoading = true
  if (file) {
    let formData = new FormData()
    formData.append('files', file)
    const { data: ret } = await vueObject.$http.post(formData)
    return ret
  } else {
    this.$message.error('请选择音频文件（WAV/MP3/AAC/MPEG/WMA 格式）')
  }
}

/**
 * 上传单个附件
 */
export async function upload_others_data(vueObject, file) {
  vueObject.submitLoading = true
  if (file) {
    let formData = new FormData()
    formData.append('files', file)
    const { data: ret } = await vueObject.$http.post(formData)
    return ret
  } else {
    this.$message.error('请选择附件（doc/pdf/xls/xlsx/ppt 格式）')
  }
}

/**
 * 上传单个视频文件
 */
export async function upload_videos_data(file) {
  if (file) {
    let formData = new FormData()
    formData.append('file', file)
    let res = await file_uploadVideo_path(formData)
    return res.data
  } else {
    this.$message.error('请选择视频文件（mp4/avi/avg/mpeg/wmv 格式）')
  }
}

/**
 * 此方法是将file对象转化成图片预览路径调用的是webapi
 * 调用此方法。传入一个file对象，将传入的对象转化成图片本地地址（local的地址）实现预览
 */
export function getObjectURL(file) {
  var url = null
  if (window.createObjectURL != undefined) {
    url = window.createObjectURL(file)
  } else if (window.URL != undefined) {
    url = window.URL.createObjectURL(file)
  } else if (window.webkitURL != undefined) {
    url = window.webkitURL.createObjectURL(file)
  }
  return url
}
export default {
  upload_audios_data,
  upload_others_data,
  upload_videos_data,
  getObjectURL
}
