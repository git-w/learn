/**
 * @description: 存储token以及uid的方法
 * @author: leroy
 * @Compile：2020-11-26 13：00
 * @update: leroy(2020-11-26 13：00)
 */

import Cookies from 'js-cookie'
const TokenKey = 'token'
const Uid = 'uid'
const MenuList = 'menuList'

export function get_token() {
  return sessionStorage.getItem(TokenKey)
}

export function set_token(token) {
  return sessionStorage.setItem(TokenKey, token)
}

export function remove_token() {
  return sessionStorage.removeItem(TokenKey)
}

export function get_uid() {
  return Cookies.get(Uid)
}

export function set_uid(uid) {
  return Cookies.set(Uid, uid)
}

export function remove_uid() {
  return Cookies.remove(Uid)
}
export function get_menu_list() {
  return Cookies.get(MenuList)
}

export function set_menu_list(menuList) {
  return Cookies.set(MenuList, menuList)
}

export function remove_menu_list() {
  return Cookies.remove(MenuList)
}
