/**
 * @memo 格式化日期
 * @param date {Date} 日期
 * @param pattern {string} 格式，例："yyyy-MM-dd HH:mm:ss"
 * @returns {String} 返回格式化后的日期，如："2018-01-22 18:04:30"
 */
export function date_format(row, format) {
  if (!row) return '——'
  let date = new Date(Number(row)) // 时间戳为10位需*1000，时间戳为13位的话不需乘1000
  if (!format) {
    let Y = date.getFullYear() + '-'
    let M =
      (date.getMonth() + 1 < 10
        ? '0' + (date.getMonth() + 1)
        : date.getMonth() + 1) + '-'
    let D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' '
    let h =
      (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':'
    let m =
      (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) +
      ':'
    let s =
      (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()) +
      ' '
    return Y + M + D + h + m + s
  }
  var time = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'H+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds(),
    'q+': Math.floor((date.getMonth() + 3) / 3),
    'S+': date.getMilliseconds()
  }
  if (/(y+)/i.test(format)) {
    format = format.replace(
      RegExp.$1,
      (date.getFullYear() + '').substr(4 - RegExp.$1.length)
    )
  }
  for (var k in time) {
    if (new RegExp('(' + k + ')').test(format)) {
      format = format.replace(
        RegExp.$1,
        RegExp.$1.length == 1
          ? time[k]
          : ('00' + time[k]).substr(('' + time[k]).length)
      )
    }
  }
  return format
}

/**
 * @memo 按照特殊的时间格式返回对应的时间日期 yyyy-mm-dd
 */
export function date_format_ymd(row) {
  if (row) {
    var date = new Date(row) // 时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-'
    var M =
      (date.getMonth() + 1 < 10
        ? '0' + (date.getMonth() + 1)
        : date.getMonth() + 1) + '-'
    var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' '
    return Y + M + D
  } else {
    return '——'
  }
}

/**
 * @memo 国际标准时间 转 yyyy-MM-dd HH:mm:ss
 */
export function format_date(date) {
  if (!date) return '——'
  const time = new Date(Date.parse(date));
  let Y = time.getFullYear() + '-'
  let M =
    (time.getMonth() + 1 < 10
      ? '0' + (time.getMonth() + 1)
      : time.getMonth() + 1) + '-'
  let D = (time.getDate() < 10 ? '0' + time.getDate() : time.getDate()) + ' '
  let h =
    (time.getHours() < 10 ? '0' + time.getHours() : time.getHours()) + ':'
  let m =
    (time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes()) +
    ':'
  let s =
    (time.getSeconds() < 10 ? '0' + time.getSeconds() : time.getSeconds()) +
    ' '
  return Y + M + D + h + m + s
}

/**
 * @memo 国际标准时间 转 yyyy-MM-dd
 */
export function format_date_ymd(date) {
  if (!date) return '——'
  const time = new Date(Date.parse(date));
  let Y = time.getFullYear() + '-'
  let M =
    (time.getMonth() + 1 < 10
      ? '0' + (time.getMonth() + 1)
      : time.getMonth() + 1) + '-'
  let D = (time.getDate() < 10 ? '0' + time.getDate() : time.getDate()) + ' '
  return Y + M + D
}

/**
 * @memo 时间加减法 返回 str 格式 2019-4-10
 */
export function add_date(date, days) {
  var d = new Date(date)
  d.setDate(d.getDate() + days)
  var m = d.getMonth() + 1
  if (m < 10) {
    m = '0' + m
  }
  var day = d.getDate()
  if (day < 10) {
    day = '0' + day
  }
  return d.getFullYear() + '-' + m + '-' + day
}

/**
 * @memo 时间加减法, 返回数组格式 [2019,03,15]
 */
export function add_dates(date, days) {
  var d = new Date(date)
  var arr = []
  d.setDate(d.getDate() + days)
  var m = d.getMonth()
  var day = d.getDate()
  if (day < 10) {
    day = '0' + day
  }
  arr.push(d.getFullYear())
  arr.push(m)
  arr.push(day)
  return arr
}

/**
 * @memo 传入分钟数, 得到具体时间
 */
export function add_minutes(min) {
  var d = new Date()
  d.setMinutes(d.getMinutes() + parseInt(min))
  return d
}

/**
 * @memo 计算 日期相差天数
 */
export function date_difference(sDate1, sDate2) {
  // sDate1和sDate2是2006-12-18格式
  var dateSpan, iDays
  sDate1 = Date.parse(sDate1)
  sDate2 = Date.parse(sDate2)
  dateSpan = sDate2 - sDate1
  dateSpan = Math.abs(dateSpan)
  iDays = Math.floor(dateSpan / (24 * 3600 * 1000))
  return iDays
}

/**
 * @memo 获取指定时间格式的时间格式：yyyy-mm-dd 格式
 */
function get_date(date) {
  var d = new Date(date)
  if (date == null) {
    d = new Date()
  }
  var m = d.getMonth() + 1
  if (m < 10) {
    m = '0' + m
  }
  var day = d.getDate()
  if (day < 10) {
    day = '0' + day
  }
  var minu = d.getMinutes()
  if (minu < 10) {
    minu = '0' + minu
  }

  var sources = d.getSeconds()
  if (sources < 10) {
    sources = '0' + sources
  }
  return (
    d.getFullYear() +
    '-' +
    m +
    '-' +
    day +
    ' ' +
    d.getHours() +
    ':' +
    minu +
    ':' +
    sources
  )
}

/**
 * @memo 获取当月第一天和最后一天
 */
export function get_month_first_last_day(currentMonth) {
  var nowDate = new Date(currentMonth)
  var fullYear = nowDate.getFullYear()
  var month = nowDate.getMonth() + 1 // getMonth 方法返回 0-11，代表1-12月
  var endOfMonth = new Date(fullYear, month, 0).getDate() // 获取本月最后一天

  var endDate = nowDate.setDate(endOfMonth) //当月最后一天
  var startDate = nowDate.setDate(0) //当月第一天
  return [startDate, endDate]
}

/**
 * @memo 获取零点时刻
 * @param {
 * timestamp:"", //现在的时间戳
 * }
 * @returns {number} 返回一个数字代表当天的0点
 */
export function get_time_stat_point(timestamp) {
  let time = new Date(timestamp)
  let stTime =
    timestamp -
    time.getHours() * 60 * 60 * 1000 -
    time.getMinutes() * 60 * 1000 -
    time.getSeconds() * 1000 -
    time.getMilliseconds()
  return stTime
}

/**
 * @memo 获取23:59:59
 * @param {
 * timestamp:"", //现在的时间戳
 * }
 * @returns {number} 返回一个数字代表当天的0点
 */
export function get_time_end_point(timestamp) {
  let time = new Date(timestamp)
  let edTime =
    timestamp -
    time.getHours() * 60 * 60 * 1000 -
    time.getMinutes() * 60 * 1000 -
    time.getSeconds() * 1000 -
    time.getMilliseconds()
  return edTime
}

/**
 * @memo 获取当天往后七天的时间 包含当天
 * @param {
 * startTime:"", //今天的时间戳
 * dayNumber:"", //几天
 * }
 * @returns {Array} 返回一个数组这个数组里装的是规定天数的日期
 */
export function get_today_futures(startTime, dayNumber = 1) {
  let days = [] // 存放当天及之后的n天的数据
  let day = get_time_stat_point(startTime) //获取这一天的0点

  for (let i = 0; i < dayNumber; i++) {
    days[i] = day
    console.log(i)
    day += 24 * 60 * 60 * 1000
  }

  return days
}

/**
 * @memo 获取某个时间段内的每天的时间戳
 * @param {
 * startTime:"", // 开始时间戳
 * endTime:"", // 结束时间戳
 * }
 * @returns {Array} 返回一个数组这个数组里装的是规定时间段内的每一天的时间戳
 */
export function get_time_quantum_days(startTime, endTime) {
  let dateList = []
  let start = get_time_stat_point(startTime) // 获取这一天的

  let end = get_time_stat_point(endTime)
  while (end - start >= 0) {
    dateList.push(start)
    start += 24 * 60 * 60 * 1000
  }
  return dateList
}

/**
 * @memo 传入一个时间戳计算这个时间戳后n天或前n天的，X点X分X秒的时间戳
 * @param {
 * timestamp:"", // 现在的时间戳 number
 * days:"", // 几天  number 负整数或者正证书或0
 * time
 * }
 * @returns {
 *    timestamp:"" //  number
 * } 返回一个数组这个数组里装的是规定时间段内的每一天的时间戳
 */
export function get_appoint_time(timestamp, days, time) {
  let timeStr = get_time_stat_point(timestamp) // 获取当前时间的0点时间戳
  let timeArry = time.split(':') //
  let h = parseInt(timeArry[0])
  let m = parseInt(timeArry[1])
  let s = parseInt(timeArry[2])

  return (
    timeStr +
    days * 24 * 60 * 60 * 1000 +
    h * 60 * 60 * 1000 +
    m * 60 * 1000 +
    s * 1000
  )
}

/**
 * @memo 获取当前的时间是周几
 * @param {
 *    timestamp:"", // 现在的时间戳 number
 * }
 */
export function get_weekday(timestamp) {
  let str = '周' + '日一二三四五六'.charAt(new Date(timestamp).getDay())
  return str
}

/**
 * @memo 获取当前时间的年龄
 * @param {
 *    birthday:"", // 现在的时间戳 number
 * }
 */
export function get_birthday(birthday) {
  //出生时间 毫秒
  let birthDayTime = new Date(birthday).getTime(); 
  //当前时间 毫秒
  let nowTime = new Date().getTime(); 
  //一年毫秒数(365 * 86400000 = 31536000000)
  return Math.ceil((nowTime-birthDayTime)/31536000000) - 1;
}

/**
 * 导出，在页面使用时按需引入
 */
export default {
  add_date,
  add_dates,
  add_minutes,
  get_date,
  date_difference,
  date_format,
  date_format_ymd,
  format_date,
  format_date_ymd,
  get_month_first_last_day,
  get_today_futures,
  get_time_stat_point,
  get_time_quantum_days,
  get_appoint_time,
  get_weekday,
  get_birthday,
}
