/**
 * 当width与show-overflow-tooltip同时存在时，el-tooltip未正确设置width行内样式，主流浏览器均存* 在该情况，而Safari不兼容col列设置的宽度，内容过长时，会导致撑开列宽；
 * 使用方式 <el-table :cell-style="get_cell_style">...</el-table>
 * 另一种方式见https://github.com/ElemeFE/element/issues/10308#issuecomment-686446725
 */
export function get_cell_style({
  column
}) {
  // TODO 针对Safari浏览器 表格width与showOverflowTooltip暂不能共存异常
  const tempWidth = column.realWidth || column.width
  if (column.showOverflowTooltip) {
    return {
      minWidth: tempWidth + 'px',
      maxWidth: tempWidth + 'px'
    }
  }
  return {}
}


export default {
    get_cell_style
};
