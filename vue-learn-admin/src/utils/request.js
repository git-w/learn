/**
 *  提供一个配置好的axios，然后导出，在main.js来使用。
 */
import axios from 'axios'
import JSONBIGINT from 'json-bigint'
import { Message, MessageBox } from 'element-ui'
import store from '@/store'
import { get_token, get_uid } from '@/utils/auth'
import { show_loading, hide_loading } from '@/utils/loading'
import API from '@/utils/config'
/**
 *  1. 默认配置
 */
// create an axios instance
const service = axios.create({
  baseURL: API.BASE_URL, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  // timeout: 10000 // request timeout
})

/**
 * 配置头部的代码不能在此处定义。
 * 这个里的代码之后在刷新页面后只会执行一次。
 * axios.defaults.headers.Authorization = `Bearer ${store.getUser().token}`
 */
service.defaults.transformResponse = [
  data => {
    // data json格式的字符串
    // 当后端没有响应的时候  转换会报错  捕获这个错误  处理：不去转换直接返回
    try {
      return JSONBIGINT.parse(data)
    } catch (e) {
      return data
    }
  }
]

/**
 *  2. 请求拦截器
 */
service.interceptors.request.use(
  config => {
    if (get_token()) {
      debugger
      show_loading()
      // 不做过多拦截，有token就加上之后return，没有就直接return
      config.headers.Authorization = get_token()
      config.headers.Uid = get_uid()
      return config
    }

    return config
  },
  err => {
    hide_loading()
    Promise.reject(err)
  }
)

/**
 *  3. 响应拦截器
 */
service.interceptors.response.use(
  res => {
    // 成功
    if (res.status === 200) {
      if (res.data.code === 1001 || res.data.code === 1002) {
        // token失效跳转到登陆页面
        MessageBox.confirm(
          '登录状态已过期，您可以继续留在该页面，或者重新登录',
          '系统提示',
          {
            confirmButtonText: '重新登录',
            cancelButtonText: '取消',
            type: 'warning'
          }
        ).then(() => {
          // 这一步操作就已经退出了
          store.dispatch('user/LOGOUT').then(() => {
            location.reload() // 为了重新实例化vue-router对象 避免bug
          })
        })
      } else if (
        res.data.code === 0 ||
        res.data.code === 200 ||
        res.data.code === 100000 ||
        !res.data.code
      ) {
        // 请求成功  - 关闭加载状态
        hide_loading()
        return res.data
      } else if (res.data.code === 100000) {
        hide_loading()
        return res.data
      } else {
        // 响应成功但是接口返回错误 - 将错误信息显示出来 - 将加载状态关闭
        hide_loading()
        Message({
          message: res.data.message,
          type: 'error',
          duration: 5 * 1000
        })
        return Promise.reject(new Error(res.data.message || 'Error'))
      }
    } else if (res.status === 401) {
      // token失效跳转到登陆页面
      MessageBox.confirm(
        '权限已过期，您可以继续留在该页面，或者重新登录',
        '系统提示',
        {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }
      ).then(() => {
        // 这一步操作就已经退出了
        store.dispatch('user/LOGOUT').then(() => {
          location.reload() // 为了重新实例化vue-router对象 避免bug
        })
      })
    }
  },
  err => {
    if (err.response.data.code == -1) {
      // token失效跳转到登陆页面
      MessageBox.confirm(
        '权限已过期，您可以继续留在该页面，或者重新登录',
        '系统提示',
        {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }
      ).then(() => {
        // 这一步操作就已经退出了
        console.log(store)
        store.commit('user/LOGOUT').then(() => {
          location.reload() // 为了重新实例化vue-router对象 避免bug
        })
      })
    }
    // 响应失败 接口不通 404/500/501/···
    Message({
      message: err.response.data.code
        ? err.response.data.message
        : err.response.data.status,
      type: 'error',
      duration: 5 * 1000
    })
    hide_loading()
    return Promise.reject(err)
  }
)

export default service
