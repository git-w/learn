/**
 * @memo 处理价格的公共方法
 */
 export function price_rule(value) {
  if (value == '') {
    return ''
  } else {
    var v = value
    if (v === '0') {
      v = '0.00'
    } else if (v === '0.') {
      v = '0.00'
    } else if (/^0+\d+\.?\d*.*$/.test(v)) {
      v = v.replace(/^0+(\d+\.?\d*).*$/, '$1')
    } else if (/^0\.\d$/.test(v)) {
      v = v + '0'
    } else if (!/^\d+\.\d{2}$/.test(v)) {
      if (/^\d+\.\d{2}.+/.test(v)) {
        v = v.replace(/^(\d+\.\d{2}).*$/, '$1')
      } else if (/^\d+$/.test(v)) {
        v = v + '.00'
      } else if (/^\d+\.$/.test(v)) {
        v = v + '00'
      } else if (/^\d+\.\d$/.test(v)) {
        v = v + '0'
      } else if (/^[^\d]+\d+\.?\d*$/.test(v)) {
        v = v.replace(/^[^\d]+(\d+\.?\d*)$/, '$1')
      } else if (/\d+/.test(v)) {
        v = v.replace(/^[^\d]*(\d+\.?\d*).*$/, '$1')
      } else if (/^0+\d+\.?\d*$/.test(v)) {
        v = v.replace(/^0+(\d+\.?\d*)$/, '$1')
      } else {
        v = '0.00'
      }
    }
    return Number(v)
  }
}

/**
 * @memo 处理折扣的公共方法
 */
export function discount_rule(value) {
  if (value == '') {
    return ''
  } else {
    var v = value
    if (v < 0 || v > 9.99) {
      return ''
    } else if (/^(-?\d+)(\.\d+)?$/.test(v) || /^-?([1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0)$/.test(v)) {
      if (v === '0') {
        v = '0.00'
      } else if (v === '0.') {
        v = '0.00'
      } else if (/^0+\d+\.?\d*.*$/.test(v)) {
        v = v.replace(/^0+(\d+\.?\d*).*$/, '$1')
      } else if (/^0\.\d$/.test(v)) {
        v = v + '0'
      } else if (!/^\d+\.\d{2}$/.test(v)) {
        if (/^\d+\.\d{2}.+/.test(v)) {
          v = v.replace(/^(\d+\.\d{2}).*$/, '$1')
        } else if (/^\d+$/.test(v)) {
          v = v + '.00'
        } else if (/^\d+\.$/.test(v)) {
          v = v + '00'
        } else if (/^\d+\.\d$/.test(v)) {
          v = v + '0'
        } else if (/^[^\d]+\d+\.?\d*$/.test(v)) {
          v = v.replace(/^[^\d]+(\d+\.?\d*)$/, '$1')
        } else if (/\d+/.test(v)) {
          v = v.replace(/^[^\d]*(\d+\.?\d*).*$/, '$1')
        } else if (/^0+\d+\.?\d*$/.test(v)) {
          v = v.replace(/^0+(\d+\.?\d*)$/, '$1')
        } else {
          v = '0.00'
        }
      } else if (/^0\.\d$/.test(v)) {
        v = v + '0'
      } else if (/^\.*$\d$/.test(v)){
        return ''
      }
      return Number(v)
    } else {
      return ''
    }
  }
}