/*
 * @description: 校验规则封装，正则表达式
 * @author: leroy
 * @Compile：2020-08-27 18：00
 * @update: leroy(2020-08-27 18：00)
 */

// 账号验证
const loginNameReg = /^([A-Z]|[a-z]|[\d])*$/
export function validateLogin(rule, value, callback) {
    if (!loginNameReg.test(value)) {
        callback(new Error('账号不能输入特殊字符'))
    } else {
        if (value.length >= 5) {
            callback()
        } else {
            return callback(new Error('请输入最短5个字符！'))
        }
    }
}

// 密码验证
const numberReg = /^([A-Z]|[a-z]|[\d])*$/
export function validatePassword(rule, value, callback) {
    if (!numberReg.test(value)) {
        callback(new Error('密码只能输入数字和字母'))
    } else {
        if (value.length >= 6) {
            callback()
        } else {
            return callback(new Error('请输入最短6个字符！'))
        }

    }
}

// 数字和字母验证
const letterReg = /^([A-Z]|[a-z]|[\d])*$/
export function validateLetter(rule, value, callback) {
    if (!letterReg.test(value)) {
        callback(new Error("只能输入英文和数字"));
    } else {
        callback()
    }
}

// 中文验证
const cnReg = /^[\u4e00-\u9fa5]+$/
export function validateCn(rule, value, callback) {
    if (!cnReg.test(value)) {
        callback(new Error("请输入中文"));
    } else {
        callback()
    }
}
// 邮箱验证
const emailReg = /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/
export function validateEmail(rule, value, callback) {
    if (!emailReg.test(value)) {
        callback(new Error('请输入正确的邮箱'))
    } else {
        callback()
    }
}
// 手机号验证
const mobilePhoneReg = /^((0\d{2,3}-\d{7,8})|(1[23456789]\d{9}))$/
export function validatemoblie(rule, value, callback) {
    if (!mobilePhoneReg.test(value)) {
        callback(new Error('请输入正确的手机号码'))
    } else {
        callback()
    }
}
// URL地址验证
const urlReg = /^http[s]?:\/\/.*/
export function validateUrl(rule, value, callback) {
    if (!urlReg.test(value)) {
        callback(new Error('请输入正确的URL地址'))
    } else {
        callback()
    }
}
//身份证验证
const isIdReg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
export function validateIsId(rule, value, callback) {
    if (!isIdReg.test(value)) {
        callback(new Error('请输入正确的身份证'))
    } else {
        callback()
    }
}
// n*70 70取余
export function validateDivide70(rule, value, callback) {
    if (value % 70 != 0) {
        callback(new Error('请输入70的倍数'))
    } else {
        callback()
    }
}
export default {
    validateLogin,
    validatePassword,
    validateLetter,
    validateCn,
    validateEmail,
    validatemoblie,
    validateUrl,
    validateIsId,
    validateDivide70
}
