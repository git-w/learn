import { file_multi_upload_print_path } from '../../api/common'
/**
 *
 * @memo 调用此方法，传入此方法的是图片对象数组，返回的是字符串拼接
 * imageList 是一个数组，用来存储当前的路径
 * 传入一个数组，这个数组可能是一张，也可能是多张图将file.file保存在这个数组中
 * 传入这个数组将这个数组通过formData上传返回一个数组这个数组里包含的每一个对象里有短路径和长路径
 * 将数组重的短路径拼接成字符串然后return
 */
export async function upload_img_data(
  imageList,
  isWaterMarkText,
  waterMarkText
) {
  let formData = new FormData()

  if (imageList.length > 0) {
    // 多张图片上传接口
    for (let imageIndex in imageList) {
      if (imageList[imageIndex]) {
        formData.append('files', imageList[imageIndex])
        // formData.append('isFlag', isWaterMarkText)
        // formData.append('print', waterMarkText)
      }
    }

    let imagePath = await file_multi_upload_print_path(formData)

    return imagePath
  }
}

/**
 * @memo 限制图片大小
 */
export function img_size(file, width, height) {
  return new Promise((resolve, reject) => {
    console.log(reject)
    let url = window.URL || window.webkitURL
    let img = new Image()
    img.onload = function() {
      // 图片比例校验
      // let valid = img.width <=width&& img.height <=height
      // valid ? resolve() : reject();
      let valid = img.width <= 100000 && img.height <= 10000
      valid ? resolve(true) : resolve(true)
    }
    img.src = url.createObjectURL(file)
  })
}

/**
 * @memo 将base64 处理成 file
 * @param {
 *    nowFrame:"" // base64 图片Url
 *    file:"" // 原file对象信息  转化之前的 file
 * }
 */
export function base64_to_file(nowFrame, file) {
  let arr = nowFrame.split(',')
  let mime = arr[0].match(/:(.*?);/)[1]
  let bstr = atob(arr[1])
  let n = bstr.length
  let u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  let newFile = new File([u8arr], file.name, { type: mime })
  newFile.uid = file.uid
  return newFile
}
/**
 * 将以base64的图片url数据转换 Blob 文件流
 * 用url方式表示的base64图片数据
 * @param {
 *    {
 *      dataURL:"",//用url方式表示的base64图片数据
 *      type: 'image/jpeg'//文件类型
 *    }
 * }
 */
export function base64_to_blob(base64, type) {
  let urlData = base64
  let bytes = null
  if (urlData.split(',').length > 1) {
    //是否带前缀
    bytes = window.atob(urlData.split(',')[1]) // 去掉url的头，并转换为byte
  } else {
    bytes = window.atob(urlData)
  }
  // 处理异常,将ascii码小于0的转换为大于0
  let ab = new ArrayBuffer(bytes.length)
  let ia = new Uint8Array(ab)
  for (let i = 0; i < bytes.length; i++) {
    ia[i] = bytes.charCodeAt(i)
  }
  return new Blob([ab], { type: type })
}

/**
 * @memo 此方法是将file对象、blob 转化成图片预览路径调用的是webapi
 * 调用此方法。传入一个file对象，将传入的对象转化成图片本地地址（local的地址）实现预览
 */
export function file_flow_to_url(file) {
  let url = null
  if (window.createObjectURL !== undefined) {
    url = window.createObjectURL(file)
  } else if (window.URL !== undefined) {
    url = window.URL.createObjectURL(file)
  } else if (window.webkitURL !== undefined) {
    url = window.webkitURL.createObjectURL(file)
  }
  return url
}

/**
 * @memo 此方法是将blob对象转化成file
 */
export function blob_to_file(file, name) {
   
  const newFile = new window.File([file], name, { type: file.type })
   
  return newFile
}

/**
 * @memo 长短路径匹配成对象
 * 帮助长路径找到它所属的短路径
 * @param {
 *   imgPath:"",  // Stirng 短路径
 *   imgPathUrl:"",  // Stirng  短路径
 * }
 */
export function get_img_url_to_path_list(imgPath, imgPathUrl) {
  let imgPathArry = imgPath ? imgPath.split(',').filter(item => item) : []
  let imagePathArry = imgPathUrl
    ? imgPathUrl.split(',').filter(item => item)
    : []
  if (imgPathArry.length == 0 || imagePathArry.length == 0) return []
  let imgArry = imgPathArry.map(imgPath => {
    return {
      imgUrl: imgPath,
      imgUrlPath: imagePathArry.filter(imagePath => {
        return imagePath.indexOf(imgPath) != -1
      })[0]
    }
  })
  return imgArry
}
