/**
 * @author Jsj
 * @memo 给图片加水印的工具类
 */
import { base64_to_file, base64_to_blob } from './imageMethod.js'
/**
 * @memo 给图片加水印
 * @param {
 *   file:"", // file 文件对象
 *   settingForm:{
 *      text:"", // 水印的文字
 *      isText:"", // 是否需要水印 0 是 1 否
 *      compress:"", // 压缩比例
 *   }
 * }
 */
export function set_water_mark_data(
  file,
  settingForm = {
    text: '', // 水印的文字
    isText: 1, // 是否需要水印 0 是 1 否
    compress: 50 // 压缩比例
  }
) {
   
  return new Promise((reject, resolve) => {
    let reader = new FileReader()
    reader.onload = function(e) {
      let txt = e.target.result
      let img = document.createElement('img')
      img.src = txt
      img.onload = function() {
         
        //  在页面中创建canvas容器
        let canvas = document.createElement('canvas')
        canvas.width = img.width
        canvas.height = img.height
        let context = canvas.getContext('2d')
        context.fillRect(0, 0, 500, 500)
        //  将图片添加至canvas容器中
        context.drawImage(img, 0, 0, canvas.width, canvas.height)
        //  将文字添加至canvas中
        context.globalAlpha = 0.8
        context.rotate((-20 * Math.PI) / 180) // 水印旋转角度
        context.fillStyle = '#D7D7D7'
        context.textAlign = 'center'
        context.textBaseline = 'Middle'
         
        // if (settingForm.isText == 0) {
        //   context.font = `${(canvas.width / 1024) * 50}px Vedana`
        //   let heightRate = (canvas.width / 1024) * 120
        //   let widthRate = (canvas.width / 1024) * 50
        //   let xRate = (canvas.width / 1024) * 700
        //   let yRate = (canvas.width / 1024) * 200

        //   for (let i = 0; i < canvas.height / heightRate; i++) {
        //     for (let j = 0; j < canvas.width / widthRate; j++) {
        //       context.fillText(
        //         settingForm.text,
        //         i * xRate,
        //         j * yRate,
        //         canvas.width
        //       ) // 水印在画布的位置x，y轴
        //     }
        //   }
        // }

        //  导出base64  可选择是否压缩以及压缩比例
         
        let nowFrame = canvas.toDataURL(
          file.type || 'image/jpeg',
          settingForm.compress / 100 > 0.7 ? 0.7 : settingForm.compress / 100
        )

         
        // 转换成file对象 和 Blob
        let newFile = base64_to_file(nowFrame, file)
        let newBlob = {
          url: base64_to_blob(nowFrame, 'image/jpeg'),
          uid: file.uid
        }
         
        console.log(newFile, newBlob)
        reject({ newFile, newBlob })
      }
    }
    reader.readAsDataURL(file)
  })
}
