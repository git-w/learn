//
const BASE_URL = process.env.VUE_APP_BASE_API // process.env 这里是全局属性
const API = {
  USER_LOGIN: BASE_URL + '/login'
}

export default {
  BASE_URL,
  API
}
