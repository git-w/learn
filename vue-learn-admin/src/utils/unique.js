/**
 * @memo 数组去重不是对象去重 排空
 */
export function unique_str(arr) {
  let arry = Array.from(new Set(arr))
  const target = arry.filter(arry => {
    return arry != null && arry != undefined && arry != ''
  })
  return target
}

/**
 * @memo 对象数组去重
 */
export function unique_obj(arr) {
  const res = new Map()
  return arr.filter(
    arr =>
      (!res.has(arr.id) || !res.has(arr.iD)) &&
      (res.set(arr.id, 1) || res.set(arr.iD, 1))
  )
}

/**
 * @memo 冒泡排序 此方法将此方法将按从小到大的顺序排列 数组中的内容
 */
export function arry_bubbling(arr) {
  var len = arr.length
  for (var i = 0; i < len - 1; i++) {
    for (var j = 0; j < len - 1 - i; j++) {
      // 相邻元素两两对比，元素交换，大的元素交换到后面
      if (arr[j].checkedTime > arr[j + 1].checkedTime) {
        var temp = arr[j]
        arr[j] = arr[j + 1]
        arr[j + 1] = temp
      }
    }
  }
  return arr
}

/**
 * 导出，在页面使用时按需引入
 */
export default {
  unique_str,
  unique_obj,
  arry_bubbling
}
