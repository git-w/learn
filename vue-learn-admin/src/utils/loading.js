import { Loading } from 'element-ui'

let loadingCount = 0
let loading

const start_loading = (options = {}) => {
  loading = Loading.service({
    lock: true,
    text: '加载中……',
    ...options
  })
}

const end_loading = () => {
  loading.close()
}

export const show_loading = options => {
  if (loadingCount === 0)
  {
    start_loading(options)
  }
  loadingCount += 1
}

export const hide_loading = () => {
  if (loadingCount <= 0)
  {
    return
  }
  loadingCount -= 1
  if (loadingCount === 0)
  {
    end_loading()
  }
}

export default function (Vue) {
  // 添加全局API
  Vue.prototype.$loading = {
    show_loading,
    hide_loading,
  }
}
