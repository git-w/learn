export default {
  nameWidth: 150, // 名称
  selectWidth: 60, // 多选
  radioWidth: 100, // 单选
  operationWidth: 100, // 操作
  imgSquareWidth: 200, //图片（长方形）
  imgWidth: 150, // 图片(圆形)
  imgCircleWidth: 100, // 图片(矩形)
  phoneWidth: 150, // 电话
  timeWidth: 200, // 时间点
  timeSlotWidth: 350, // 时间段
  stateWidth: 100, // 状态
  typeWidth: 120, // 类型
  moreWidth: 150, // 其他
  userWidth: 100, // 用户
  emailWidth: 250, // 邮箱
  workNumberWidth: 100, // 工号
  loginNameWidth: 150, // 账号
  studyCountWidth: 150, // 访问量
  taskTypeWidth: 150, // 任务状态
  contentWidth: 280, // 内容
  name300: 300,
  name400: 400,
  width50: 50,
  width100: 100,
  width150: 150,
  width200: 200,
  width250: 250,
  width300: 300,
  width350: 350,
  width400: 400,
}
