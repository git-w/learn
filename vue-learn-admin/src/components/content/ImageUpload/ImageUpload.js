/*
 * @description: 上传图片的组件封装
 * @author: leroy
 * @Compile：2021-01-28 09：00
 * @update: leroy(2021-01-29 18：00)
 * @memo:append-to-body 属性是用来提高dialog的层级 此坑十分的坑
 * @memo:  astrict  对象是设置图片规格的参数  是个对象 在组件中不能直接操作父组件给子组件传递的props值
 *         proportion 宽高比 arry [5,2]   不设置默认显示空数组
 *         size       大小 K为单位  number  500 不设置默认没限制
 *         imgType    图片属性  arry ['image/png','image/PNG'] 不设置默认为空数组
 *         measure    图片尺寸 arry [宽,高] 不设置默认默认为空数组
 */

import Material from '../material/material.vue'
export default {
  name: 'ImageUpload',
  components: {
    Material
  },
  model: {
    prop: 'value', // 用来改变默认的checked事件
    event: 'change' // v-model 的绑定属性和抛出事件的
  },
  props: {
    // 组件调用类型 false 单选 true 多选
    multiple: {
      type: Boolean,
      default() {
        return false
      }
    },
    // 图片的显示路径 { imgUrlPath: '', imgUrl: '' }
    value: {
      type: Array,
      default() {
        return []
      }
    },
    // 图片属性限制
    astrict: {
      type: Object,
      default() {
        return {
          proportion: [], // 宽高比 arry [5,2]   不设置默认显示空数组
          size: 0, //  大小 K为单位  number  500 不设置默认没限制
          imgType: [], // 图片属性  arry ['jpg','JPG'] 不设置默认为空数组
          measure: [] // 图片尺寸 arry [宽,高] 不设置默认默认为空数组
        }
      }
    },
    //隐藏预览功能
    noPreview: {
      type: Boolean,
      default() {
        return false
      }
    }
  },

  data() {
    return {
      imglistDialog: false,
      dialogVisible: false,
      imageUrl: '', // 图片预览路径
      imageAstrict: {} // 图片规格
    }
  },
  computed: {
    imageList(){
        return this.value // 这玩意儿是个数组 父子组件
    }
  },
  watch: {},

  mounted() {
    this.to_load() // 进入此页面先加载
  },
  methods: {
    /**
     * 打开图片列表
     */
    open_img_list() {
      if (this.multiple && this.imageList.length >= 30) {
        this.$message.error('图片数量已达上限！')
        return
      }
      this.imglistDialog = true
    },

    /**
     * @memo 检查当前图片规格是否符合要求
     *         proportion 宽高比 arry [5,2]   不设置默认显示空数组
     *         size       大小 K为单位  number  500 不设置默认没限制
     *         imgType    图片属性  arry ['jpg','JPG'] 不设置默认为空数组
     *         measure    图片尺寸 arry [宽,高] 不设置默认默认为空数组
     */
    is_qualified(img) {
      if (this.imageAstrict.proportion.legnth > 0) {
        if (
          img.imgWidth / img.imgHeight !==
          this.imageAstrict.proportion[0] / this.imageAstrict.proportion[1]
        ) {
          debugger
          return false
        }
      }
      if (this.imageAstrict.size) {
        if (img.imgSize / 1024 > this.imageAstrict.size) {
          debugger
          return false
        }
      }
      if (this.imageAstrict.imgType > 0) {
        if (!this.imageAstrict.imgType.includes(img.imgType)) {
          debugger
          return false
        }
      }
      if (this.imageAstrict.measure.legnth > 0) {
        if (
          img.imgWidth > this.imageAstrict.measure[0] &&
          img.imgHeight > this.imageAstrict.measure[1]
        ) {
          debugger
          return false
        }
      }

      return true
    },

    /**
     * 保存当前选中的图片
     */
    submit_checked_img(arry) {
      if (arry.length == 0) {
        this.$message.error('请选择图片！')
        return
      }
      let qualifiedImgList = []
      let disqualificationList = []
      arry.forEach(item => {
        if (this.is_qualified(item)) {
          qualifiedImgList.push(item)
        } else {
          disqualificationList.push(item)
        }
      })
      if (disqualificationList.length > 0) {
        this.$message.warning(`不合格图片${disqualificationList.length}张`)
      }
      this.$emit('change', this.imageList.concat(qualifiedImgList))
      this.imglistDialog = false
    },

    /**
     * 删除但前选中的图片
     * 给父组件传递一个空的图片
     */
    delete_img(item, index) {
      this.imageList.splice(index, 1)
      this.$emit('change', this.imageList)
    },
    /**
     * 取消选中图片保存按钮
     */
    close_checked_img() {
      this.imglistDialog = false
    },

    /**
     * 预览图片按钮
     * 将这个图片的的路径传递给弹窗的img标签用来显示
     */
    view_img(item) {
      this.imageUrl = item.imgUrlPath
      this.dialogVisible = true
    },

    /**
     * 初始化规格
     */
    init_astrict(astrict) {
      debugger
      this.imageAstrict = {
        proportion:
          astrict.proportion && astrict.proportion.length > 0
            ? astrict.proportion
            : [],
        size: astrict.size && astrict.size > 0 ? astrict.size : 0,
        imgType:
          astrict.imgType && astrict.imgType.length > 0 ? astrict.imgType : [],
        measure:
          astrict.measure && astrict.measure.length > 0 ? astrict.measure : []
      }
    },

    /**
     * 进入此页面进行加载
     */
    to_load() {
      let astrict = {
        ...this.astrict
      }
      this.init_astrict(astrict)
    }
  }
}
