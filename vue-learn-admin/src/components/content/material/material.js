/*
 * @description: 内容管理之素材管理
 * @author: leroy
 * @Compile：2021-01-28 09：00
 * @update: leroy(2021-01-29 18：00)
 * @memo type 的值决定还用当前的组件使用的方式
 */

import {
  category_all_path, // 分类 全部
  category_save_path, // 分类 添加
  category_update_path, // 分类 修改
  category_delete_path, // 分类 删除
  image_list_path, // 列表
  image_save_path, // 添加
  image_delete_path, // 删除
  image_update_category_path // 修改分类
} from '@/api/content'
import { img_size, upload_img_data } from '@/utils/img/imageMethod' // 引入图片处理包;
export default {
  name: 'material',
  props: {
    type: {
      type: Number,
      default() {
        return ''
      }
    },
    //隐藏预览功能
    noPreview: {
      type: Boolean,
      default() {
        return false
      }
    }
  },
  data() {
    //上传图片验证
    let isPhoto = (rule, value, callback) => {
      //判断新闻封面地址是否有值
      var that = this
      if (that.addImageList.length > 0) {
        callback()
      } else {
        //本地图片没有值,提示请上传图片
        return callback(new Error('请上传本地图片'))
      }
    }
    return {
      activeName: 'myImg', //tab名
      dialogImageUrl: '', //上传后的图片路径
      //搜索
      searchForm: {
        categoryID: '', //类型：String  可有字段  备注：无
        imgName: '', //类型：String  可有字段  备注：无
        pageNum: 1,
        pageSize: 14,
        totals: 0
      },
      up: true,
      //添加分组名称
      categoryForm: {
        categoryName: ''
      },
      searchCategory: '', // 检索分组名称
      addCategoryShow: false, //添加分组框显示
      updateCategoryShow: false, // 修改分组框显示
      updateImgCategoryShow: false, // 修改图片分类的显示框
      imgCategoryForm: {
        updateImgCategoryId: '', // 修改图片分类id的显示框
      },
      categoryList: [], // 分组列表
      categoryId: '', // 点击哪个分类应该显示哪个分类
      categoryPower: true, // 点击的分类是都具备删除和修改的权限
      checkAll: false, // 全选的状态
      checkedImageIds: [], // 以选中的图片的id
      isIndeterminate: false, // 不确定状态
      checked: [], // 全选变量   选中后将当前页额的所有图片的id加入当前数组
      imgTableList: [], // 图片列表数组
      addImageForm: {
        categoryID: '' // 分类id
      },
      imageUrl: '', // 弹窗的预览路径
      addImageList: [], // 选中的图片base64 暂存数组，用于上传换取短路径用的数组
      addImageUrlList: [], // 选中的图片的预览路径数组用于表单内的复现
      dialogVisible: false, // 选中的图片是否预览

      imgFile: {}, // 选中的图片对象
      rules: {
        categoryID: [
          {
            required: true,
            message: '请选择分组',
            trigger: 'change'
          }
        ],
        photo: [
          {
            validator: isPhoto
          }
        ],

        categoryName: [
          {
            required: true,
            message: '请输入分组名称',
            trigger: 'change'
          }
        ],

        updateImgCategoryId: [
          {
            required: true,
            message: '请选择分组',
            trigger: 'change'
          }
        ]
      }
    }
  },
  watch: {
    /**
     * @memo 监听新增分组,清除form里的数据项
     */
    addCategoryShow(newVal, oldVal) {
      console.log(newVal, oldVal)
      // 清除分类表单数
      if (newVal) {
        this.categoryForm = {
          categoryName: ''
        }
      }
    },

    /**
     * @memo 监听修改图片分组的弹窗
     */
    updateImgCategoryShow(newVal, oldVal) {
      console.log(newVal, oldVal)
      this.imgCategoryForm.updateImgCategoryId = '' // 清除分类的id
    },

    /**
     * @memo 监听点击的分类，并获取当前点击的分类的id，并且获取改分类是否具备删除的权限，请求分类下属的列表
     */
    categoryId(newVal, oldVal) {
      console.log(newVal, oldVal)
      this.searchForm.categoryID = newVal
      // 请求图片类表的数据
      this.get_image_list_data(1, this.searchForm.pageSize)
      // 清除分类表单数
      this.categoryList.forEach((ele, index) => {
        console.log(ele, index)
        if (ele.id == newVal) {
          if (ele.id == 'df300210e48446c3a6b1f7c8762513a3') {
            console.log(ele.id)
            this.categoryPower = false
          } else {
            this.categoryPower = true
          }
        }
      })
    }
  },
  mounted() {
    this.toload()
  },
  methods: {
    /**
     * @memo 检索分组名称
     */
    search_category() {
      if (this.searchCategory) {
        return this.categoryList.filter(item => {
          return item.categoryName.indexOf(this.searchCategory) !== -1
        })
      } else {
        return this.categoryList
      }
    },
    /**
     * 进入此页面加载
     */
    toload() {
      this.get_category_list_data()
    },
    /** 分类相关代码方法 **/

    /**
     * 请求分类的列表调用分类的findall的接口
     * category_all_path
     */
    async get_category_list_data() {
      let { data: res } = await category_all_path({})
      this.categoryList = res

      this.categoryId = res[0].id
      this.addImageForm.categoryId = res[0].id
      this.searchForm.categoryID = res[0].id
    },
    /**
     * 点击当前分类
     * 将分类的id
     * 并请求当前分类下的图片
     */
    click_category(item) {
      this.categoryId = item.id
      this.categoryForm = {
        id: item.id,
        categoryName: item.categoryName
      }
      this.checkedImageIds = []
      this.checkAll = false
      this.isIndeterminate = false
    },

    /**
     * 新增提交按钮
     */
    submit_category_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_update_data()
        } else {
          return false
        }
      })
    },

    /**
     * 新增和保存接口方法
     * 当categoryForm对象中有id的时候，就是修改。反之就是新增
     */
    async save_update_data() {
      let param = {
        id: this.categoryForm.id ? this.categoryForm.id : '',
        categoryName: this.categoryForm.categoryName
      }
      if (!this.categoryForm.id) {
        await category_save_path(param)
          .then(res => {
            this.$message.success(res.message)
            this.addCategoryShow = false
            this.get_category_list_data()
          })
          .catch(() => {})
      } else {
        await category_update_path(param)
          .then(res => {
            this.$message.success(res.message)
            this.updateCategoryShow = false
            this.get_category_list_data()
          })
          .catch(() => {})
      }
    },

    /**
     * 修改框显示之前的回调
     * 显示当前选中的数据
     */
    update_show() {
      this.categoryList.forEach((ele, index) => {
        console.log(ele, index)
        if (ele.id == this.categoryId) {
          this.$nextTick(() => {
            this.categoryForm = {
              id: ele.id,
              categoryName: ele.categoryName
            }
          })
        }
      })
    },

    /**
     * 点击删除分组
     */
    delete_category_btn() {
      this.categoryList.forEach((ele, index) => {
        console.log(ele, index)
        if (ele.id == this.categoryId) {
          this.categoryForm = {
            id: ele.id,
            categoryName: ele.categoryName
          }
        }
      })
      this.$confirm(
        `此操作将永久删除分类"${this.categoryForm.categoryName}", 是否继续?`,
        '提示',
        {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }
      )
        .then(() => {
          this.delete_category_data(this.categoryForm.id)
            .then(res => {
              this.$message.success(res.message)
              this.get_category_list_data()
            })
            .catch(() => {})
        })
        .catch(() => {
          this.$message({
            type: 'warning',
            message: '已取消删除'
          })
        })
    },

    /**
     * 分类删除的接口
     */
    async delete_category_data(id) {
      let res = await category_delete_path({ ids: id })
      return res
    },

    /** 图片的相关方法 **/
    /**
     * 图片的列表接口方法
     */
    async get_image_list_data(pageNum, pageSize) {
      let param = {
        pageSize: pageSize, //类型：Number  必有字段  备注：每页的数量
        pageNum: pageNum, //类型：Number  必有字段  备注：当前页
        categoryID: this.searchForm.categoryID, //类型：String  可有字段  备注：无
        imgName: this.searchForm.imgName //类型：String  可有字段  备注：无
      }
      await image_list_path(param)
        .then(res => {
          this.imgTableList = res.data.resultList //列表的数据
          this.searchForm.totals = res.data.totalRows //总长度
          this.searchForm.pageSize = res.data.pageSize // 当前页码条数
          this.searchForm.pageNum = res.data.pageNum // 请求页码
          if (this.searchForm.pageNum > 1) {
            if (res.data.resultList.length == 0) {
              this.get_image_list_data(
                this.searchForm.pageNum - 1,
                this.searchForm.pageSize
              )
            }
          } else if (this.searchForm.pageNum == 0) {
            return this.$message.warning('没有更多数据')
          }
        })
        .catch(() => {})
    },

    select_img(val) {
      if (val) {
        let imgTableList = this.imgTableList
        for (let i in imgTableList) {
          this.checkedImageIds.push(imgTableList[i].id)
        }
      }
    },

    /**
     * 全选
     * 监听全选的true或者false
     * 如果是true那么将图片数组中的每一项的id， push 至 checkbox 的id 数组中
     * 并且将id数组绑定到checkbox-group上用来显示被勾选的数据
     */
    checked_img_all(val) {
      if (val) {
        let imgTableList = this.imgTableList
        for (let i in imgTableList) {
          this.checkedImageIds.push(imgTableList[i].id)
        }
      } else {
        this.checkedImageIds = []
        this.isIndeterminate = false
      }
    },

    /**
     * 单选按钮
     * 单选按钮的回调事件
     * 回调的参数是所有已经选中的图片的id数组
     * 将选中的id添加至id数组中
     */
    checked_img(value) {
      if (this.type == '1') {
        this.checkedImageIds = []
        if (value[value.length - 1]) {
          this.checkedImageIds.push(value[value.length - 1])
        } else {
          this.checkedImageIds = []
        }
      }

      // 将已经选中的id数组赋值给data中声明的id数组变量，这里的方式为了深拷贝
      else if (this.type == '2') {
        let checkedCount = value.length

        if (checkedCount < 10) {
          this.checkedImageIds = []

          for (let i in value) {
            if (value[i]) {
              this.checkedImageIds.push(value[i])
            }
          } // 将已经选中的id数组赋值给data中声明的id数组变量，这里的方式为了深拷贝
        } else {
          this.checkedImageIds = []
          for (let i in value) {
            // i 为什么要小于 9 呢，因为这里的
            if (value[i] && i < 9) {
              this.checkedImageIds.push(value[i])
            }
          } // 将已经选中的id数组赋值给data中声明的id数组变量，这里的方式为了深拷贝
          this.$message.error('最多选择9张照片！')
        }
      } else if (this.type == '3') {
        let checkedCount = value.length
        this.checkAll = checkedCount === this.imgTableList.length // 如果单选已选中的id数组长度等于当前列表的数组长度，那么全选装态就变为选中状态，反之就为false
        this.isIndeterminate =
          checkedCount > 0 && checkedCount < this.imgTableList.length // 当选中的数组长度大于0且选中的装态小于列表图片长度的时候半选状态为true
        this.checkedImageIds = []

        for (let i in value) {
          if (value[i]) {
            this.checkedImageIds.push(value[i])
          }
        } // 将已经选中的id数组赋值给data中声明的id数组变量，这里的方式为了深拷贝
      }
    },

    /**
     * 修改图片的分组
     * 提交按钮
     */
    submit_img_category_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.image_update_data()
        } else {
          return false
        }
      })
    },

    /**
     * 图片修改接口
     */
    async image_update_data() {
      let param = {
        ids:
          this.checkedImageIds.length > 0 ? this.checkedImageIds.join(',') : '',
        categoryID: this.imgCategoryForm.updateImgCategoryId
      }
      await image_update_category_path(param)
        .then(res => {
          this.$message.success(res.message)
          this.updateImgCategoryShow = false
          this.checkedImageIds = []
          this.checkAll = false
          this.isIndeterminate = false
          this.get_image_list_data(
            this.searchForm.pageNum,
            this.searchForm.pageSize
          )
        })
        .catch(() => {})
    },

    /**
     * 删除按钮
     */
    delete_img_btn() {
      this.$confirm('确认删除？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          let id = this.checkedImageIds.join(',')
          this.delete_img_path(id)
            .then(res => {
              this.$message.success(res.message)
              this.checkedImageIds = []
              this.checkAll = false
              this.isIndeterminate = false
              this.get_image_list_data(
                this.searchForm.pageNum,
                this.searchForm.pageSize
              )
            })
            .catch(() => {})
        })
        .catch(() => {
          this.$message({
            type: 'warning',
            message: '已取消删除'
          })
        })
    },

    /**
     * 删除接口
     */
    async delete_img_path(id) {
      let res = await image_delete_path({ ids: id })
      return res
    },

    /**
     * 切换tab
     * 切换tabs的时候不对
     * 刷新图片的列表
     */
    handle_click(tab, event) {
      console.log(event)
      if (tab.name == 'myImg') {
        this.get_image_list_data(
          this.searchForm.pageNum,
          this.searchForm.pageSize
        )
      } else if (tab.name == 'upload') {
        debugger
        this.addImageList = [] // 清除图片本地路径的数组
        this.addImageForm.categoryID = 'df300210e48446c3a6b1f7c8762513a3'
      }
    },

    /**
     * 上传图片的接口(换取短地址)
     * upload_img
     */
    upload_img() {
      if (this.addImageList.length > 0) {
        console.log(this.addImageList)
        let _that = this
        return new Promise(function(resolve, reject) {
          upload_img_data(_that.addImageList)
            .then(res => {
              resolve(res.data)
            })
            .catch(() => {
              _that.$message.error(reject.message)
            })
        })
      } else {
        return ''
      }
    },

    /**
     * 上传图片的接口(换取本地路径)
     * 如果不写async的话图片右上角的小三角已经不会显示
     */
    async upload_img_url(file) {
      this.addImageList.push(file.file) // 将base64的路径push至数组中，存储起来用于走接口换取短路径
      this.$refs.addImageForm.clearValidate()
    },

    /**
     * 上传图片的标准
     */
    async before_avatar_upload(file) {
      debugger
      var testmsg = file.name.substring(file.name.lastIndexOf('.') + 1)
      const extension =
        testmsg === 'jpg' ||
        testmsg === 'JPG' ||
        testmsg === 'png' ||
        testmsg === 'PNG'
      const isLt2M = file.size / 1024 / 1024 < 2
      console.log(isLt2M)
      if (!extension) {
        this.$message.error('上传头像图片只能是 JPG 或 PNG 格式!')
      }
      if (!isLt2M) {
        this.$message.error('上传头像图片大小不能超过 2MB!')
      }
      const isSize = img_size(file, 750, 600, this)
      return extension && isLt2M && isSize
    },

    /**
     * 移除当前的图片
     */
    img_remove_btn(file, fileListAdd) {
      debugger
      this.addImageUrlList.forEach((item, index) => {
        if (item.uid == file.uid) {
          this.addImageUrlList.splice(index, 1)
          this.addImageList.splice(index, 1)
        }
      })
    },

    /**
     * 图片的预览
     */
    img_preview_btn(file) {
      this.imageUrl = file.url
      this.dialogVisible = true
    },

    /**
     * 提交图片新增的方法
     */
    submit_img_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          Promise.all([this.upload_img()]).then(res => {
            let imgPathArry = []
            if (res) {
              let imgArry = res[0]
              if (imgArry.length > 0) {
                for (let i = 0; i < imgArry.length; i++) {
                  let imgObj = {
                    categoryID: this.addImageForm.categoryID, //类型：String  必有字段  备注：分类Id
                    imgName: imgArry[i].fileName, //类型：String  必有字段  备注：图片名称
                    imgUrl: imgArry[i].imgPath, //类型：String  必有字段  备注：图片地址Url
                    imgType: imgArry[i].fileContentType, //类型：String  必有字段  备注：文件内容类型
                    imgHeight: imgArry[i].height, //类型：String  必有字段  备注：图片高 px
                    imgWidth: imgArry[i].width, //类型：String  必有字段  备注：图片宽 px
                    imgSize: imgArry[i].size //类型：String  必有字段  备注：图片文件大小 字节
                  }
                  imgPathArry.push(imgObj)
                }
              }
            } else {
              this.$message.error('请上传图片！')
              return false
            }
            // 获取到图片地址后，执行请求接口
            this.save_img_data(imgPathArry)
          })
        } else {
          return false
        }
      })
    },

    /**
     * 保存方法
     */
    async save_img_data(imgPathArry) {
      let param = {
        imageList: imgPathArry
      }
      await image_save_path(param)
        .then(res => {
          this.$message.success(res.message)
          // 清除图片上传的数据
          this.clear_img_form()
        })
        .catch(() => {})
    },

    /**
     * 清除图片表单的数据
     */
    clear_img_form() {
      this.addImageForm.categoryID = '' // 清空分类的id
      this.addImageUrlList = []
    },

    /**
     * 检索按钮
     */
    search_btn() {
      this.get_image_list_data(1, this.searchForm.pageSize)
    },
    /**
     * 检索重置
     */
    reset_btn() {
      this.searchForm.pageNum = 1
      this.searchForm.pageSize = 14
      this.searchForm.imgName = ''
      this.get_image_list_data(1, this.searchForm.pageSize)
    },
    /**
     * 列表分页 相关方法
     */
    change_size_btn(val) {
      // 分页加载常见问题列表加载
      this.get_image_list_data(1, val)
    },
    change_current_btn(val) {
      // 分页加载常见问题列表加载
      this.get_image_list_data(val, this.searchForm.pageSize)
    },

    /**
     * 将选中的图片的地址传值给父组件
     * 选中的图片往外的时候需要传递的是整个对象
     * 通过选中的id数组来循环遍历当前页的图片列表数组，找到对应的那个对象并把对象传递出来
     */
    submit_checked_img() {
      let imgArry = []
      this.checkedImageIds.forEach((idEle, idIndex) => {
        this.imgTableList.forEach((ele, index) => {
          console.log(ele, index)
          if (idEle == ele.id) {
            imgArry.push(ele)
          }
        })
      })

      this.$emit('submit_checked_img', imgArry)
      this.checkedImageIds = []
    },

    /**
     * 取消选中图片保存按钮
     */
    close_checked_img() {
      this.$emit('close_checked_img')
      this.checkedImageIds = []
    }
  }
}
