export default {
  name: "SelectFileBtn",
  props: {
    multiple: {
      type: String,
      default () {
        return false;
      }
    },
    accept: {
      type: String,
      default () {
        return false;
      }
    },

  },
  watch: {
    // 新增的时候是多选，修改的时候是单选
    multiple(val) {
      this.multiple = val
    },
    accept(val) {
      this.accept = val
    },
  },
  mounted() {},
  methods: {
    changeFile(event) {
      var f = document.getElementById("files");
    
      this.$emit("changeFile", f.files)
      event.target.value = ""
    },
  }
}