export default {
  name: 'Amap',
  model: {
    prop: 'value', // 用来改变默认的checked事件
    event: 'change' // v-model 的绑定属性和抛出事件的
  },
  props: {
    value: {
      type: Object,
      default: {
        lng: 116.403959,
        lat: 39.915136
      }
    }
  },
  data() {
    return {
      MAps: null, // 地图实例
      geolocation: null, // 定位实例
      circle: null, // 画圆实例
      site: '' // 地址
    }
  },
  mounted() {
    this.initMaps()  // 进入此页面，刷新地图
  },
  beforeUpdate() {
    this.initMaps()  // 数据改变，刷新地图
  },
  methods: {
    /**
     * 初始化地图
     */
    initMaps() {
      var marker
      let that = this
      if (!that.value.lng) that.value.lng = 116.3975 // 保险措施 如果没有默认值直接给默认值
      if (!that.value.lat) that.value.lat = 39.909145 // 保险措施 如果没有默认值直接给默认值
      //初始化地图
      var map = new AMap.Map('MAps', {
        center: [that.value.lng, that.value.lat],
        resizeEnable: true,
        zoom: 15 // 缩放比
      })
      if (that.value.lng && that.value.lat) {
        if (marker) {
          marker.setMap(null)
          marker = null
        }
        marker = new AMap.Marker({
          icon: new AMap.Icon({
            image:
              '//a.amap.com/jsapi_demos/static/demo-center/icons/poi-marker-default.png',
            size: new AMap.Size(50, 50), //图标大小
            imageSize: new AMap.Size(20, 35)
          }),
          position: [that.value.lng, that.value.lat],
          offset: new AMap.Pixel(-10, -30)
        })
        that.$emit('change', that.value)
        marker.setMap(map)
      }

      //输入提示
      var auto = new AMap.Autocomplete({
        input: 'tipinput'
      })
      //构造地点查询类
      var placeSearch = new AMap.PlaceSearch({
        map: map
      })

      //注册监听，当选中某条记录时会触发
      AMap.event.addListener(
        auto,
        'select', //关键字查询查询
        function select(e) {
          placeSearch.setCity(e.poi.adcode)
          placeSearch.search(e.poi.name)
        }
      )
      AMap.event.addListener(placeSearch, 'markerClick', function (e) {
        that.value.lng = e.data.location.lng
        that.value.lat = e.data.location.lat
        var coordinateB = e.data.location.lng + ',' + e.data.location.lat
        show_info_click(that.value, coordinateB)
      })

      // 给地图绑定点击事件
      map.on('click', function (e) {
        that.value.lng = e.lnglat.getLng()
        that.value.lat = e.lnglat.getLat()
        var coordinateB = e.lnglat.getLng() + ',' + e.lnglat.getLat()
        show_info_click(that.value, coordinateB)
      })

      // 添加覆盖物至点击的点
      function show_info_click(value, coordinateB) {
        if (marker) {
          marker.setMap(null) // 清除现在标记的地方
          marker = null // 清空这个实例
        }
        marker = new AMap.Marker({
          icon: new AMap.Icon({
            image:
              '//a.amap.com/jsapi_demos/static/demo-center/icons/poi-marker-default.png',
            size: new AMap.Size(50, 50), //图标大小
            imageSize: new AMap.Size(20, 35)
          }),
          position: [value.lng, value.lat],
          offset: new AMap.Pixel(-10, -30)
        })
        regeo_code(coordinateB) // 抓取地图名称组件定位的
        marker.setMap(map)
      }

      // 构建查询地址
      var geocoder = new AMap.Geocoder({
        // city: "010", //城市设为北京，默认：“全国”
        // radius: 1000 //范围，默认：500
      })
      function regeo_code(lnglat) {
        geocoder.getAddress(lnglat, function (status, result) {
          if (status === 'complete' && result.regeocode) {
            var address = result.regeocode.formattedAddress
            that.value.address = address
            that.$emit('change', that.value)
          } else {
            this.$message.error('根据经纬度查询地址失败')
          }
        })
      }
    }
  }
}
