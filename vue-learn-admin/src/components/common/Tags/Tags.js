import bus from '../bus'
export default {
    name: 'my-tags',
    data() {
        return {
            tagsList: [],
        }
    },
    methods: {
        isActive(path) {
            return path === this.$route.fullPath
        },
        // 关闭单个标签
        closeTags(index) {
            const delItem = this.tagsList.splice(index, 1)[0]
            const item = this.tagsList[index]
                ? this.tagsList[index]
                : this.tagsList[index - 1]
            if (item) {
                delItem.path === this.$route.fullPath && this.$router.push(item.path)
            } else {
                this.$router.push('/')
            }
        },
        // 关闭全部标签
        closeAll() {
            this.tagsList = []
            this.$router.push('/')
        },
        // 关闭其他标签
        closeOther() {
            const curItem = this.tagsList.filter((item) => {
                return item.path === this.$route.fullPath
            })
            this.tagsList = curItem
        },
        // 设置标签
        setTags(route) {
            const isExist = this.tagsList.some((item) => {
                return item.path === route.fullPath
            })
            if (!isExist) {
                if (this.tagsList.length >= 10) {
                    this.tagsList.shift()
                }
                if (route.name != 'aside') {
                    this.tagsList.push({
                        title: route.meta.title,
                        path: route.fullPath,
                        name: route.name,
                    })
                }
            }
            bus.$emit('tags', this.tagsList)
        },
        handleTags(command) {
            command === 'other' ? this.closeOther() : this.closeAll()
        },
    },
    computed: {
        showTags() {
            return this.tagsList.length > 0
        },
    },
    watch: {
        $route(newValue, oldValue) {
            this.setTags(newValue)
        },
    },
    created() {
        this.setTags(this.$route)
        // 监听关闭当前页面的标签页
        bus.$on('close_current_tags', () => {
            for (let i = 0, len = this.tagsList.length; i < len; i++) {
                const item = this.tagsList[i]
                if (item.path === this.$route.fullPath) {
                    if (i < len - 1) {
                        this.$router.push(this.tagsList[i + 1].path)
                    } else if (i > 0) {
                        this.$router.push(this.tagsList[i - 1].path)
                    } else {
                        this.$router.push('/')
                    }
                    this.tagsList.splice(i, 1)
                    break
                }
            }
        })
    },
}