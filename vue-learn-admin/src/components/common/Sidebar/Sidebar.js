import { menu_list_path } from '../../../api/common'
import bus from '../bus'
export default {
  name: 'my-sidebar',
  data() {
    return {
      collapse: false,
      menuList: [],
      path: '',
      // sysName: '优享文旅',
      // sysNameCollapse: '文旅',
    }
  },
  mounted() {
    this.getMenuList()
  },
  computed: {
    onRoutes() {
      return this.$route.path.replace('/', '')
    },
  },
  methods: {
    async getMenuList() {
      const res = await menu_list_path({})
      this.menuList = res.data
      const matched = this.$route.matched // 一个数组，包含当前路由的所有嵌套路径片段的路由记录
      if (matched.length > 0 && matched[matched.length - 1].path != '/aside') {
        this.$router.replace({
          path: matched[matched.length - 1].path,
        })
        this.path = matched
      } else {
        this.$router.push(this.menuList[0].children[0].attributes.PagePath) // 默认跳转第一个路由
      }
    },
  },
  created() {
    // 通过 Event Bus 进行组件间通信，来折叠侧边栏
    bus.$on('collapse', (msg) => {
      this.collapse = msg
      bus.$emit('collapse-content', msg)
    })
  },
}