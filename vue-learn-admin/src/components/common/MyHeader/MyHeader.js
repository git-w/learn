import bus from '../bus' // 非父子传值用bus中间件来传值
import { reset_password_path } from '../../../api/login'
import { account_update_path } from '../../../api/system'
import store from '@/store'
import { date_format } from '../../../utils/date/dateFormat'
import { get_img_url_to_path_list } from '@/utils/img/imageMethod.js'
export default {
  name: 'my-header',
  data() {
    return {
      date_format,
      collapse: false,
      fullscreen: false,
      dialog: false, // 控制详情弹窗的开合
      drawer: false, // 控制修改信息抽屉的开合
      title: '', // 控制抽屉的头部

      resetDialog: false, // 重置密码弹窗

      userForm: {
        loginPassword: '',
        newLoginPassword: '',
        newLoginPasswordA: ''
      }, // 修改密码的弹窗数据表单
      addForm: {
        id: '',
        accountName: '',
        phone: [],
        email: '',
        loginName: '',
        departmentName: '',
        departmentID: ''
      }, // 修改用户的数据表单

      rules: {
        loginPassword: [
          {
            required: true,
            message: '请输入旧密码',
            trigger: 'blur'
          }
        ],
        newLoginPassword: [
          {
            required: true,
            message: '请输入新密码',
            trigger: 'blur'
          }
        ],
        newLoginPasswordA: [
          {
            required: true,
            message: '请再次输入新密码',
            trigger: 'blur'
          }
        ],
        accountName: [
          {
            required: true,
            message: '请输入真实姓名',
            trigger: 'blur'
          }
        ],
        phone: [
          {
            required: true,
            message: '请输入手机号',
            trigger: 'blur'
          },
          {
            pattern: /^1[23456789]\d{9}$/,
            message: '请输入正确的手机号码'
          }
        ],
        email: [
          {
            required: true,
            message: '请输入邮箱',
            trigger: 'blur'
          },
          {
            pattern: /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/,
            message: '请输入正确的邮箱'
          }
        ]
      }
    }
  },
  mounted() {
    if (document.body.cliewntWidth < 1500) {
      this.collapse_change()
    }
  },
  computed: {
    adminInfo() {
      return this.$store.getters['user/grt_user_data']
    }
  },
  methods: {
    /**
     * 抽屉关闭前回调
     */
    handle_close() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_drawer()
        })
        .catch(() => { })
    },

    /**
     *  关闭弹窗
     */
    close_drawer() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          // 调用关闭抽屉的方法
          this.clear_drawer()
        })
        .catch(() => { })
    },
    /**
     * 清除数据
     * 关闭抽屉清空图片相关的变量
     */
    clear_drawer() {
      this.drawer = false
      this.addForm = {
        id: '',
        accountName: '',
        phone: [],
        email: '',
        loginName: '',
        departmentName: '',
        departmentID: ''
      }
    },
    /**
     * 管理员的修改接口
     */
    async account_update_data() {
      let phoneImg =
        this.addForm.photo.length > 0 ? this.addForm.photo[0].imgUrl : ''
      let param = {
        id: this.addForm.id,
        accountName: this.addForm.accountName,
        email: this.addForm.email,
        loginName: this.addForm.loginName,
        photo: phoneImg
      }
      await account_update_path(param)
        .then(res => {
          // 修改成功
          this.$message.success(res.message)
          // 修改成功返回列表页
          this.clear_drawer()
          this.$store.dispatch('user/admin_info_data')
        })
        .catch(() => { })
    },


    /**
     * 退出
     */
    login_out_btn() {
      this.$confirm('是否确认退出?', '提示', {
        confirmButtonText: '是',
        cancelButtonText: '否',
        type: 'warning'
      })
        .then(() => {
          store.commit('user/LOGOUT')
        })
        .catch(() => { })
    },

    /**
     * 启用状态
     */
    state_judge(accountStatus) {
      if (accountStatus === 0) {
        return '正常/启用'
      } else if (accountStatus === 1) {
        return '冻结/停用'
      }
    },

    /**
     * 个人详情 按钮
     */
    info_btn() {
      this.dialog = true
      this.title = '基本信息'
    },
    /**
     * 重置
     */
    reset_btn(formName) {
      this.$refs[formName].resetFields()
    },

    /**
     *  修改密码接口
     */
    async reset_password_data() {
      await reset_password_path({
        loginPassword: this.userForm.loginPassword,
        newLoginPassword: this.userForm.newLoginPassword
      })
        .then(() => {
          // 修改成功后将推出登录
          this.$message.success('修改成功')
          store.commit('user/LOGOUT')
        })
        .catch(() => { })
    },

    /**
     * 提交密码修改
     */
    submit_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          if (this.resetDialog) {
            if (
              this.userForm.loginPassword === this.userForm.newLoginPassword
            ) {
              this.$message.error('旧密码与新密码一致，请重新输入！')
            } else if (
              this.userForm.loginPassword === this.userForm.newLoginPasswordA
            ) {
              this.$message.error('旧密码与新密码一致，请重新输入！')
            } else if (
              this.userForm.newLoginPassword !== this.userForm.newLoginPasswordA
            ) {
              this.$message.error('两次输入密码不一致，请重新输入')
            } else if (
              this.userForm.loginPassword !== this.userForm.newLoginPassword &&
              this.userForm.loginPassword !== this.userForm.newLoginPasswordA &&
              this.userForm.newLoginPassword === this.userForm.newLoginPasswordA
            ) {
              this.reset_password_data()
            }
          } else if (this.drawer) {
            this.account_update_data()
            // Promise.all([this.upload_img()]).then(res => {
            //     if (res[0]) {
            //         this.addForm.photo = res[0]
            //     }

            // })
          }
        } else {
          return false
        }
      })
    },

    /**
     * 校验密码不能输入汉字
     */
    input_filtration(e) {
      e.replace(/[\u4E00-\u9FA5]|[\uFE30-\uFFA0]/g, '')
    },

    /*
     * 点击下拉事件
     * 包含重置密码 退出功能
     */
    handle_command(command) {
      if (command == 'reset_btn') {
        // 点击的是重置密码,显示弹窗并且清空表单对象内的所有数据
        this.resetDialog = true
        this.userForm = {
          loginPassword: '',
          newLoginPassword: '',
          newLoginPasswordA: ''
        }
      } else if (command == 'login_out_btn') {
        // 退出登录，直接调用store里的退出登录的方法，同时将浏览器内的关于用户的数据群全部清空
        this.login_out_btn()
      } else if (command == 'info_btn') {
        // 详情按钮，直接显示从store里拿到的管理员详情信息
        this.info_btn()
      } else if (command == 'change_btn') {
        // 点击了信息编辑按钮，将登陆人的信息展示在抽屉中
        this.drawer = true
        this.title = '信息编辑'
        this.addForm = {
          id: this.adminInfo.id,
          accountName: this.adminInfo.accountName,
          email: this.adminInfo.email,
          loginName: this.adminInfo.loginName,
          departmentName: this.adminInfo.departmentName,
          departmentID: this.adminInfo.departmentID,
          photo: get_img_url_to_path_list(
            this.adminInfo.photo,
            this.adminInfo.photoPath
          ),
        }
      } else if (command == 'clear_cache_btn') {
        this.clear_cache_btn()
      }
    },

    /**
     *  侧边栏折叠
     */
    collapse_change() {
      this.collapse = !this.collapse
      bus.$emit('collapse', this.collapse)
    },

    /**
     * 全屏事件
     */
    handle_full_screen() {
      let element = document.documentElement
      if (this.fullscreen) {
        if (document.exitFullscreen) {
          document.exitFullscreen()
        } else if (document.webkitCancelFullScreen) {
          document.webkitCancelFullScreen()
        } else if (document.mozCancelFullScreen) {
          document.mozCancelFullScreen()
        } else if (document.msExitFullscreen) {
          document.msExitFullscreen()
        }
      } else {
        if (element.requestFullscreen) {
          element.requestFullscreen()
        } else if (element.webkitRequestFullScreen) {
          element.webkitRequestFullScreen()
        } else if (element.mozRequestFullScreen) {
          element.mozRequestFullScreen()
        } else if (element.msRequestFullscreen) {
          // IE11
          element.msRequestFullscreen()
        }
      }
      this.fullscreen = !this.fullscreen
    },

    /**
     * 清空缓存
     */
    // clear_cache_btn() {
    //   localStorage.clear();
    //   this.$message.success('清除缓存成功！')
    // },
  }
}
