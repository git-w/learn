export default {
    name: 'my-pages',
    // 获取从父元素传递过来的数据
    props: {
        currentPage: {
            type: Number,
            default: () => { }
        },
        pageSizes: {
            type: Array,
            default: () => { }
        },
        pageSize: {
            type: Number,
            default: () => { }
        },
        total: {
            type: Number,
            default: () => { }
        }
    },
    data() {
        return {}
    },
    methods: {
        /**
         * 改变每页显示多少条数时，将数据传递给父元素
         */
        change_size_btn(val) {
            this.$emit('sizeChange', val)
        },
        /**
         * 改变当前第几页，将数据传递给父元素
         */
        change_current_btn(val) {
            this.$emit('currentChange', val)
        }
    }
}