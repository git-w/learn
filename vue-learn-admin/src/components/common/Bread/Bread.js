import bread from '../../../api/bread.js'
export default {
  name: 'my-bread',
  data() {
    return {
      breadsName: bread,
      breadList: [],
    }
  },
  methods: {
    getBreadList() {
      this.breadList = []
      for (const routeInfo of this.$route.matched) {
        if (typeof routeInfo.name === 'string' && routeInfo.name !== 'home') {
          if (routeInfo.name !== 'aside') {
            this.breadList.push({ name: routeInfo.name, path: routeInfo.path })
          }
        }
      }
    },
  },
  mounted() {
    this.getBreadList()
  },
  watch: {
    $route() {
      this.getBreadList()
    }
  },
}