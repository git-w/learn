/*
 * @description: 富文本的上传图片问题
 * @author: leroy
 * @Compile：2020-11-23 18：00
 * @update: leroy(2020-11-23 18：00)
 */
import { quillEditor } from 'vue-quill-editor'
import Quill from 'quill'
import quillConfig from './QuillConfig.js'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
import { addQuillTitle } from './QuillTitle.js'
import { img_size } from '@/utils/img/imageMethod' // 引入图片处理包;
import { file_upload_image_path } from '../../../api/common'
export default {
  name: 'Editor',
  props: {
    // 富文本内容
    value: String,
    // 富文本的名字 同一个页面的多个富文本name不能重复的
    name: String,
    // 图片类型
    imgType: {
      type: Array,
      default: () => [
        'image/jpeg',
        'image/jpg',
        'image/png',
        'image/gif',
        'image/bmp'
      ]
    },
    // 图片限制大小 单位 M
    limitSize: {
      type: Number,
      default: 1
    }
  },
  components: {
    quillEditor
  },
  mounted() {
    addQuillTitle()
    quillConfig.initButton()
    var vm = this
    // 当点击图书上传是，触发elementui的upload点击
    var imgHandler = async function(image) {
      vm.addImgRange = vm.$refs.myTextEditor.quill.getSelection()
      if (image) {
        document.querySelector(`.${vm.name} div`).click()
      }
    }
    vm.$refs.myTextEditor.quill
      .getModule('toolbar')
      .addHandler('image', imgHandler)
  },
  model: {
    props: 'content',
    // 必须的change事件不然是不会响应的
    event: 'change'
  },
  computed: {
    content: {
      get: function() {
        return this.value
      },
      set: function() {}
    }
  },
  data() {
    return {
      // 配置文件
      quillOption: quillConfig,
      imageLoading: false,
      elUpload: 'elUpload'
    }
  },
  methods: {
    /**
     * 当富文本内容发生变化的时候
     */
    onEditorChange(e) {
      this.$emit('change', e.html)
    },

    /**
     * 当点击的时候
     */
    onEditorFocus(e) {
      this.$emit('focus', e)
    },

    /**
     * 当富文本准备的时候
     */
    onEditorReady(e) {
      this.$emit('ready', e)
    },

    /**
     * 图片上传成功
     */
    handleAvatarSuccess(ret) {
      let url = ret.imgPathUrl,
        vm = this
      if (url !== null && url.length > 0) {
        var value = url
        // ***主要的东西就是这里：上传成功回显
        vm.addImgRange = vm.$refs.myTextEditor.quill.getSelection()
        value = value.indexOf('http') != -1 ? value : 'http:' + value
        vm.$refs.myTextEditor.quill.insertEmbed(
          vm.addImgRange !== null ? vm.addImgRange.index : 0,
          'image',
          value,
          Quill.sources.USER
        )
      } else {
        vm.$message.warning('图片增加失败')
      }

      this.imageLoading = false
    },

    /**
     * 图片上传前
     */
    beforeAvatarUpload(file) {
      var testmsg = file.name.substring(file.name.lastIndexOf('.') + 1)
      const extension =
        testmsg === 'jpg' ||
        testmsg === 'JPG' ||
        testmsg === 'png' ||
        testmsg === 'PNG'
      /* 限制格式：jpg、png*/
      if (!extension) {
        this.$message.error({
          message: '上传文件只能是jpg或者png格式!',
          type: 'error'
        })
        return false /*必须加上return false; 才能阻止*/
      }
      /* 限制大小 */
      const isLt50M = file.size / 1024 / 1024 < 2
      if (!isLt50M) {
        this.$message({
          message: '上传文件大小不能超过 2MB!',
          type: 'error'
        })
        return false
      }
      let isSize = img_size(file, 750, 642, this)
      return extension && isLt50M && isSize
    },

    /**
     * 上传时
     */
    onProgress() {
      this.imageLoading = true
    },
    /**
     * 上传接口
     */
    async upload_img(file) {
      this.imageLoading = true
      let imageList = []
      imageList.push(file.file)
      this.submitLoading = true
      let formData = new FormData()
      if (imageList.length > 0) {
        for (let imageIndex in imageList) {
          if (imageList[imageIndex]) {
            formData.append('file', imageList[imageIndex])
            const { data: ret } = await file_upload_image_path(formData)
            return ret
          } else {
            this.$message.error('图片上传错误')
          }
        }
      } else {
        this.$message.error('请上传图片')
      }
    }
  }
}
