import Vue from 'vue'
import App from './App'
import router from './router'
import is_view_button from './utils/menu/ViewButton' // 查看列表页面增删改查
import tableWidth from './utils/jsonData/tableWidth' // 列表单元格宽度
import Loading from './utils/loading.js'  // 全局加载
import ElementUI from 'element-ui'  // 使用element组件库
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/icon/iconfont.css'
import './assets/css/main.css'
import './assets/css/color-dark.css'

import store from './store' // 使用vuexs
import 'lib-flexible' // 自动转化rem
import get_cell_style from './utils/tableStyle' // 处理table的列内设置宽度和悬浮显示时不兼容问题
import plugin from '@/plugin' // 全局组件
import VueParticles from 'vue-particles' //  粒子插件
import { table } from './mixins/common/table.js';  // 公共列表方法混入


Vue.use(VueParticles) // 使用动效粒子效果
Vue.use(ElementUI) // 挂载注册element-ui
Vue.use(plugin)  // 挂载全局组件
Vue.use(Loading)  // 挂载全局加载
Vue.prototype.$tableWidth = tableWidth // 挂载列表单元格宽度
Vue.prototype.$is_view_button = is_view_button // 权限按钮
Vue.prototype.$get_cell_style = get_cell_style // 处理悬浮提示和设置列宽的不兼容
Vue.config.devtools = true // 生产环境中需要设置为false
Vue.mixin(table);  // 混入列表公共方法


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
