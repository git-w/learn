/**
 * @description: 全局列表mixins文件
 * @author: leroy
 * @compile：2021-11-30 11：00
 * @update: lx(2021-11-30 11：00)
 */
export const table = {
    data() {
        return {
            // 列表检索
            searchForm: {
                pageNum: 1,  //类型：Number  必有字段  备注：页码 默认第一页
                pageSize: 10,  //类型：Number  必有字段  备注：条码 每页默认十条
                totals: 0,
            }
        }
    },
    methods: {
        /*********************** 检索按钮 操作 *************************** */
        /**
         * @memo  检索搜索按钮
         */
        search_btn() {
            this.get_tabel_data(1, this.searchForm.pageSize) // 请求列表数据
        },

        
        /*********************** 多选、分页 操作 *************************** */
        /**
         * @memo 多选功能
         */
        handle_selection_change(val) {
            if (val.length > 0) {
                var x
                var ids = ''
                for (x in val) {
                    ids += val[x].id + ','
                }
                this.ids = ids
            } else {
                this.ids = ''
            }
        },

        /**
         * @memo 列表分页 相关方法
         */
        change_size_btn(val) {
            // 分页加载常见问题列表加载
            this.get_tabel_data(1, val)
        },
        change_current_btn(val) {
            // 分页加载常见问题列表加载
            this.get_tabel_data(val, this.searchForm.pageSize)
        },
    }
}
