/**
 * @description: 活动库存管理mixins文件
 * @author: leroy
 * @compile：2021-12-28 15：00
 * @update: (2021-12-28 15：00)
 */
import {
  activity_price_list_path, // 列表
  activity_price_save_path, // 添加
  activity_price_update_path, // 修改
  activity_price_delete_path, // 删除
  activity_price_info_path // 详情
} from '../../api/activity'

export const setStore = {
  data() {
    return {
      addPriceDisabled: false, // 添加售价的启用状态

      // 价格库存列表 检索字段
      searchPriceForm: {
        pageNum: 1, //类型：Number  必有字段  备注：页码 默认第一页
        pageSize: 10, //类型：Number  必有字段  备注：条码 每页默认十条
        totals: 0
      },
      tablePriceData: [], // 价格库存 列表数组

      priceDialog: false, // 修改价格库存 弹窗开合
      // 修改价格库存 表单字段
      priceForm: {
        id: '',
        price: '', // 价格
        stock: '', // 库存
        activityTime: ['', ''] // 时间范围
      },

      // 修改价格库存 表单校验
      priceRules: {
        // 售卖时间
        activityTime: [
          {
            required: true,
            message: '请选择日期',
            trigger: 'change'
          }
        ],

        // 库存
        stock: [
          {
            required: true,
            message: '请输入库存',
            trigger: 'blur'
          },
          {
            pattern: /^[0-9]+[0-9]*$/,
            message: '请输入整数'
          }
        ],

        // 价格
        price: [
          {
            required: true,
            message: '请输入价格',
            trigger: 'blur'
          }
        ],

        // 会员价格
        vipPrice: [
          {
            required: true,
            message: '请输入会员价格',
            trigger: 'blur'
          }
        ]
      }
    }
  },
  methods: {
    /*********************** 设置库存 操作 ************************************* */
    /**
     * @memo 设置价格库存 按钮
     */
    set_store_btn(row) {
      this.addForm = {
        id: row.id, //活动主键ID
        name: row.name, //活动名称
        msg: row.msg, //活动简介
        content: row.content, //活动描述(富文本)
        bookingInformation: row.bookingInformation, //预定须知
        activityTime: [row.stratTime, row.endTime], //活动时间
        isFree: row.isFree, //是否免费：0-免费；1-不免费
        price: row.price, //活动报名费
        activityUserCount: row.activityUserCount, //活动名额
        categoryId: row.categoryId, //活动分类
        isState: row.isState, //状态 0在线 1下线
        applyTime: [row.payStartTime, row.payEndTime], //售卖时间
        circleDot: {
          lng: row.longitude,
          lat: row.latitude,
          address: row.address //类型：String  必有字段  备注：活动地址
        } // 设置地图默认的经纬度为北京市
      }
      this.priceDialog = true
      this.priceForm = {
        id: '',
        activityId: row.id, // 活动id
        price: row.isFree == 0 ? 0 : '', // 价格
        vipPrice: row.isFree == 0 ? 0 : '', // 会员价格
        stock: '', // 库存
        activityTime: ['', ''] // 时间范围
      }
      this.addPriceDisabled = false
      this.get_tabel_price_data(
        this.searchPriceForm.pageNum,
        this.searchPriceForm.pageSize,
        row.id
      )
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.priceForm.clearValidate()
      })
    },

    /**
     * @memo 设置时间的禁用状态
     */
    pickerOptions(index) {
      const option = {
        disabledDate: time => {
          const times = this.tablePriceData
            .map(it => it)
            .filter((it, i) => it && i !== index)
          const startTime = this.addForm.activityTime[0]
          const endTime = this.addForm.activityTime[1]
          let timeScope = time.getTime() < startTime || time.getTime() > endTime
          times.forEach((item, indexs) => {
            timeScope =
              timeScope ||
              (time.getTime() >= new Date(item.startTime).getTime() &&
                time.getTime() <= new Date(item.endTime).getTime())
          })
          return timeScope
        }
      }
      return option
    },

    /**
     * @memo 选择时间--如果开始时间在任意一个时间段内出现，那么就提示时间重复，并且清空选择的时间
     */
    selectDay() {
      const startTime = this.priceForm.activityTime[0]
      const endTime = this.priceForm.activityTime[1]
      this.tablePriceData.forEach((item, index) => {
        console.log(item)
        if (item.id != this.priceForm.id) {
          if (
            (startTime >= item.startTime && startTime <= item.endTime) ||
            (endTime >= item.startTime && endTime <= item.endTime)
          ) {
            this.$message({
              message: '售卖时间重复请重新选择',
              type: 'error'
            })
            this.priceForm.activityTime = []
          }
        }
      })
    },

    /******************************* 修改价格库存 操作 ***************************************** */
    /**
     * @memo 列表接口
     */
    async get_tabel_price_data(pageNum, pageSize, id) {
      const res = await activity_price_list_path({
        pageSize: pageSize, // 类型：Number  必有字段  备注：每页长度
        pageNum: pageNum, // 类型：Number  必有字段  备注：当前页码
        activityId: id
      })
      if (res.code == 100000) {
        this.tablePriceData = res.data.resultList
        this.searchPriceForm.totals = res.data.totalRows
        this.searchPriceForm.pageSize = res.data.pageSize // 当前页码条数
        this.searchPriceForm.pageNum = res.data.pageNum // 请求页码
        if (this.searchPriceForm.pageNum > 1) {
          if (res.data.resultList.length === 0) {
            this.get_tabel_price_data(
              this.searchPriceForm.pageNum - 1,
              this.searchPriceForm.pageSize,
              id
            )
          }
        } else if (this.searchForm.pageNum === 0) {
          return this.$message.warning('没有更多数据')
        }
      } else {
        // 查询失败
        this.$message.error(res.message)
      }
    },

    /**
     * @memo 添加一行
     */
    add_table() {
      this.addPriceDisabled = true
      this.priceForm.id = ''
      this.priceForm.vipPrice = ''
      this.priceForm.price = ''
      this.priceForm.stock = ''
      this.priceForm.activityTime = []
      this.tablePriceData.push({
        id: '',
        price: '', // 价格
        vipPrice: this.addForm.isFree == 0 ? 0 : '', // 会员价格
        stock: this.addForm.isFree == 0 ? 0 : '', // 库存
        startTime: '', // 开始时间
        endTime: '' // 结束时间
      })
      this.$nextTick(function() {
        this.$refs.priceForm.resetFields() // 关闭校验
      })
    },

    /**
     * @memo 添加价格库存 按钮
     */
    add_table_btn() {
      this.isDisable = true
      this.add_table()
    },

    /**
     * @memo 价格库存 详情接口
     */
    async get_info_price_data(id) {
      const res = await activity_price_info_path({
        id: id
      })
      return res
    },

    /**
     * @memo 修改价格库存 按钮
     */
    update_price_btn(row) {
      if (row.id) {
        this.add_table()
        this.get_info_price_data(row.id)
          .then(res => {
            this.priceForm = {
              id: res.data.id,
              activityId: res.data.activityId, // 活动id
              price: res.data.price, // 价格
              vipPrice: res.data.vipPrice, // 会员价格
              stock: res.data.stock, // 库存
              activityTime: [res.data.startTime, res.data.endTime] // 开始时间
            }
          })
          .catch(() => {})
      }
    },

    /**
     * @memo 保存修改 接口
     */
    async save_update_price_data() {
      let param = {
        id: this.priceForm.id ? this.priceForm.id : '',
        activityId: this.priceForm.activityId, // 活动id
        price: this.priceForm.price, // 价格
        vipPrice: this.priceForm.vipPrice, // 会员价格
        stock: this.priceForm.stock, // 库存
        startTime: this.priceForm.activityTime[0], // 开始时间
        endTime: this.priceForm.activityTime[1] // 结束时间
      }
      if (!this.priceForm.id) {
        await activity_price_save_path(param)
          .then(res => {
            //添加成功
            this.$message.success(res.message)
            //添加成功返回列表页
            this.close_price_btn()
            this.get_tabel_price_data(
              this.searchPriceForm.pageNum,
              this.searchPriceForm.pageSize,
              this.priceForm.activityId
            )
          })
          .catch(() => {})
      } else if (this.priceForm.id) {
        await activity_price_update_path(param)
          .then(res => {
            // 修改成功
            this.$message.success(res.message)
            // 修改成功返回列表页
            this.close_price_btn()
            this.get_tabel_price_data(
              this.searchPriceForm.pageNum,
              this.searchPriceForm.pageSize,
              this.priceForm.activityId
            )
          })
          .catch(() => {})
      }
    },

    /**
     * @memo 新增提交
     */
    submit_price_btn(formName) {
      this.$refs[formName].validate(valid => {
        if (valid) {
          this.save_update_price_data()
        } else {
          //提交失败
          return false
        }
      })
    },

    /**
     * @memo 删除价格库存 接口
     */
    async delete_price_data(id) {
      const ret = activity_price_delete_path({
        ids: id
      })
      return ret
    },

    /**
     * @memo 删除价格库存 按钮
     */
    delete_price_btn(id) {
      if (!id) {
        return
      }
      this.$confirm('将删除该价格信息, 是否确定?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.delete_price_data(id)
            .then(res => {
              // 删除成功后重新加载列表
              this.get_tabel_price_data(
                this.searchPriceForm.pageNum,
                this.searchPriceForm.pageSize,
                this.priceForm.activityId
              )
              this.$message({
                message: '删除成功',
                type: 'success'
              })
            })
            .catch(() => {})
        })
        .catch(() => {})
    },

    /**
     * @memo 取消价格库存 按钮
     */
    close_price_btn() {
      this.addPriceDisabled = false
      this.get_tabel_price_data(
        this.searchPriceForm.pageNum,
        this.searchPriceForm.pageSize,
        this.addForm.id
      )
    },

    /**
     * @memo 关闭修改价格库存 按钮
     */
    handle_close_dialog() {
      this.$confirm('确认关闭？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(() => {
          this.clear_dialog()
        })
        .catch(() => {})
    },

    /**
     * @memo 关闭修改价格库存 弹窗
     */
    clear_dialog() {
      this.priceForm.id = ''
      this.priceForm.price = '' // 价格
      this.priceForm.vipPrice = '' // 会员价格
      this.priceForm.stock = '' // 库存
      this.priceForm.activityTime = [] // 开始时间
      this.priceDialog = false
      //关闭表单验证
      this.$nextTick(() => {
        this.$refs.priceForm.clearValidate()
      })
    },

    /************************ 分页 操作 ***************************** */
    /**
     * @memo 列表分页 相关方法
     */
    change_price_size_change(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_price_data(1, val, this.addForm.id)
    },
    change_price_current_change(val) {
      // 分页加载常见问题列表加载
      this.get_tabel_price_data(
        val,
        this.searchPriceForm.pageSize,
        this.addForm.id
      )
    }
  }
}
