/*
 * @description: 会员卡管理路由文件
 * @author: leroy
 * @Compile：2021-12-02 17：30
 * @update: lx(2021-12-02 17：30)
 */

export default [
    {
        // 会员卡信息管理
        path: 'cardManage',
        component: resolve => require(['../pages/card/cardManage/cardManage.vue'], resolve),
        name: 'cardManage',
        meta: {
            title: '会员卡信息管理'
        }
    },
    {
        // 会员卡配置
        path: 'cardConfig',
        component: resolve => require(['../pages/card/cardManage/cardConfig/cardConfig.vue'], resolve),
        name: 'cardConfig',
        meta: {
            title: '会员卡配置'
        }
    },
    {
        // 会员卡订单管理
        path: 'cardOrder',
        component: resolve => require(['../pages/card/cardOrder/cardOrder.vue'], resolve),
        name: 'cardOrder',
        meta: {
            title: '会员卡订单管理'
        }
    },
]
