/*
 * @description: 系统设置路由文件
 * @author: leroy
 * @Compile：2020-12-07 11：00
 * @update: leroy(2020-12-08 11：00)
 */

export default [
  {
    // 菜单管理
    path: 'menu',
    component: resolve => require(['../pages/system/menu/menu.vue'], resolve),
    name: 'menu',
    meta: {
      title: '菜单管理'
    }
  },
  {
    // 角色管理
    path: 'authority',
    component: resolve =>
      require(['../pages/system/authority/authority.vue'], resolve),
    name: 'authority',
    meta: {
      title: '角色管理'
    }
  },
  {
    // 系统管理员
    path: 'account',
    component: resolve =>
      require(['../pages/system/account/account.vue'], resolve),
    name: 'account',
    meta: {
      title: '系统管理员管理'
    }
  },
  {
    // 日志管理
    path: 'adminLog',
    component: resolve =>
      require(['../pages/system/adminLog/adminLog.vue'], resolve),
    name: 'adminLog',
    meta: {
      title: '日志管理'
    }
  },
  {
    // 部门管理
    path: 'organizatioon',
    component: resolve =>
      require(['../pages/system/organizatioon/organizatioon.vue'], resolve),
    name: 'organizatioon',
    meta: {
      title: '部门管理'
    }
  },
]
