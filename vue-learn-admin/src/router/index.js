import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
import { routers } from './router'
import { get_token } from '@/utils/auth' // getMenuList
// 此VueRouter是自己自定义引入暴露出来的，即是自定义的，以下的VueRouter同样是这样
// 解决两次访问相同路由地址报错
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)
// 路由配置
const RouterConfig = {
  mode: 'history',
  //   mode: 'hash',
  routes: routers
}
/**
 * 重写路由的push方法
 */

const router = new Router(RouterConfig)

/**
 * 导航守卫
 * 使用 router.beforeEach 注册一个全局前置守卫，判断用户是否登陆
 * 可以在导航守卫处理判断逻辑，通过判断是否有权限来判断是next(),还是跳转至403.
 */
router.beforeEach(async (to, from, next) => {
  if (!to.name) {
    next('/404')
  } else {
    let token = get_token()

    if (token) {
         
      // 当有token时，需要对路由进行判断，如果跳转路径是"/"或者"/aside"时直接跳转aside，其余的都放行且，请求用户详情
      if (to.path == '/') {
        //  请求用户详情接口，调用的是store里的方法，讲用户信息保存至loca中
        next({
            path: '/aside'
        })
      } else if (to.path == '/aside') {
        await store.dispatch('user/getAdminInfo')

        next()
      } else {
        next()
      }
    } else if (!token) {
      // 如果没有token那么不管怎么跳转都要跳转至登录页

      if (to.path === '/') {
        next()
      } else {
        next({
          path: '/'
        })
      }
    }
  }
})
export function resetRouter() {
  router.replace('/')
}
export default router
