/*
 * @description: 活动管理路由文件
 * @author: leroy
 * @Compile：2021-11-29 13：30
 * @update: lx(2021-11-29 13：30)
 */

export default [
    {
        // 活动分类
        path: 'activityCategory',
        component: resolve => require(['../pages/activity/activityCategory/activityCategory.vue'], resolve),
        name: 'activityCategory',
        meta: {
            title: '活动分类'
        }
    },
    {
        // 活动管理
        path: 'activityManage',
        component: resolve => require(['../pages/activity/activityManage/activityManage.vue'], resolve),
        name: 'activityManage',
        meta: {
            title: '活动管理'
        }
    },
    {
        // 活动订单管理
        path: 'activityOrder',
        component: resolve => require(['../pages/activity/activityOrder/activityOrder.vue'], resolve),
        name: 'activityOrder',
        meta: {
            title: '活动订单管理'
        }
    },
]
