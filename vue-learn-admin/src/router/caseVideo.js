/**
 * @description: 视频案例路由文件
 * @author: leroy
 * @Compile：2021-12-03 15：30
 * @update: lx(2021-12-03 15：30)
 */

export default [
    {
        // 视频案例
        path: 'caseManage',
        component: resolve => require(['../pages/caseVideo/caseManage/caseManage.vue'], resolve),
        name: 'caseManage',
        meta: {
            title: '视频案例'
        }
    }
]
