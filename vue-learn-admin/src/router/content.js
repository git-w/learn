/*
 * @description: 内容管理 路由文件
 * @author: leroy
 * @Compile：2021-01-27 13：00
 * @update: leroy(2021-01-27 13：00)
 */
export default [
  {
    path: 'material',
    component: resolve =>
      require(['../pages/content/material/material.vue'], resolve),
    name: 'material',
    title: '素材管理',
    meta: {
      title: '素材管理'
    }
  }
]
