import aside from '@/pages/aside/aside.vue'
import operation from './operation'  // 日常运营
import activity from './activity'  // 活动管理
import caseVideo from './caseVideo'  // 视频案例
import user from './user'  // 用户管理
import card from './card'  // 会员卡管理
import content from './content' // 内容管理
import system from './system' // 系统设置
import message from './message'  // 系统消息
// 作为Main组件的子页面展示并且在左侧菜单显示的路由写在appRouter里
export const appRouter = [
  // 模板页
  {
    path: '/aside',
    name: 'aside',
    component: aside
  },
  // 日常运营
  {
    path: '/operation',
    component: aside,
    name: 'operation',
    redirect: 'noRedirect',
    title: '日常运营',
    children: operation
  },
  // 活动管理
  {
    path: '/activity',
    component: aside,
    name: 'activity',
    redirect: 'noRedirect',
    title: '活动管理',
    children: activity
  },
  // 视频案例
  {
    path: '/caseVideo',
    component: aside,
    name: 'caseVideo',
    redirect: 'noRedirect',
    title: '视频案例',
    children: caseVideo
  },
  // 用户管理
  {
    path: '/user',
    component: aside,
    name: 'user',
    redirect: 'noRedirect',
    title: '用户管理',
    children: user
  },
  // 会员卡管理
  {
    path: '/card',
    component: aside,
    name: 'card',
    redirect: 'noRedirect',
    title: '会员卡管理',
    children: card
  },
  // 内容管理
  {
    path: '/content',
    component: aside,
    name: 'content',
    redirect: 'noRedirect',
    title: '内容管理',
    children: content
  },
  // 系统设置
  {
    path: '/system',
    component: aside,
    name: 'system',
    redirect: 'noRedirect',
    title: '系统设置',
    children: system
  },
  // 系统消息
  {
    path: '/message',
    component: aside,
    name: 'message',
    redirect: 'noRedirect',
    title: '系统消息',
    children: message
  },
  {
    path: '/404',
    component: resolve => require(['../pages/error/404.vue'], resolve),
    name: 'error-404',
    meta: {
      title: '404-页面不存在'
    }
  },
  {
    path: '/403',
    component: resolve => require(['../pages/error/403.vue'], resolve),
    meta: {
      title: '403-权限不足'
    },
    name: 'error-403'
  },
  {
    path: '/500',
    component: resolve => require(['../pages/error/500.vue'], resolve),
    meta: {
      title: '500-服务端错误'
    },
    name: 'error-500'
  },
  {
    path: '/',
    component: resolve => require(['../pages/login/login.vue'], resolve),
    name: '登录',
    hidden: true
  }
]

// 所有上面定义的路由都要写在下面的routers里
export const routers = [...appRouter]
