/*
 * @description: 用户管理路由文件
 * @author: leroy
 * @Compile：2021-11-25 17：50
 * @update: lx(2021-11-25 17：50)
 */

export default [
    {
        // 用户管理
        path: 'userManage',
        component: resolve => require(['../pages/user/userManage/userManage.vue'], resolve),
        name: 'userManage',
        meta: {
            title: '用户管理'
        }
    },
    {
        // 用户审核管理
        path: 'userAudit',
        component: resolve => require(['../pages/user/userAudit/userAudit.vue'], resolve),
        name: 'userAudit',
        meta: {
            title: '用户审核管理'
        }
    },
    {
        // 举报管理
        path: 'report',
        component: resolve => require(['../pages/Main.vue'], resolve),
        name: 'report',
        redirect: 'reportCategory',
        meta: {
            title: '举报管理'
        },
        children: [
            {
                // 举报分类管理
                path: 'reportCategory',
                component: resolve =>
                    require([
                        '../pages/user/report/reportCategory/reportCategory.vue'
                    ], resolve),
                name: 'reportCategory',
                title: '举报分类管理',
                meta: {
                    title: '举报分类管理'
                },
            },
            {
                // 举报管理
                path: 'reportManage',
                component: resolve =>
                    require([
                        '../pages/user/report/reportManage/reportManage.vue'
                    ], resolve),
                name: 'reportManage',
                title: '举报管理',
                meta: {
                    title: '举报管理'
                },
            }
        ]
    }
]
