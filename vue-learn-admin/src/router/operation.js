/*
 * @description: 日常运营路由文件
 * @author: leroy
 * @Compile：2021-11-25 17：30
 * @update: lx(2021-11-25 17：30)
 */

export default [
    {
        // banner图
        path: 'banner',
        component: resolve => require(['../pages/operation/banner/banner.vue'], resolve),
        name: 'banner',
        meta: {
            title: 'banner图管理'
        }
    },
    {
        // 意见反馈
        path: 'feedBack',
        component: resolve => require(['../pages/Main.vue'], resolve),
        name: 'feedBack',
        redirect: 'feedBackCategory',
        meta: {
            title: '意见反馈'
        },
        children: [
            {
                path: 'feedBackCategory',
                component: resolve => require(['../pages/operation/feedBack/feedBackCategory/feedBackCategory.vue'], resolve),
                name: 'feedBackCategory',
                meta: {
                    title: '意见反馈分类'
                }
            },
            {
                path: 'feedBackManage',
                component: resolve => require(['../pages/operation/feedBack/feedBackManage/feedBackManage.vue'], resolve),
                name: 'feedBackManage',
                meta: {
                    title: '意见反馈管理'
                }
            }
        ]
    }
]
