/*
 * @description: 系统消息 路由文件
 * @author: leroy
 * @Compile：2021-12-03 18：00
 * @update: lx(2021-12-03 18：00)
 */
export default [
    {
      path: 'systemMessage',
      component: resolve =>
        require(['../pages/message/systemMessage/systemMessage.vue'], resolve),
      name: 'systemMessage',
      title: '系统消息管理',
      meta: {
        title: '系统消息管理'
      }
    },
    {
      path: 'messageLog',
      component: resolve =>
        require(['../pages/message/messageLog/messageLog.vue'], resolve),
      name: 'messageLog',
      title: '消息通知日志',
      meta: {
        title: '消息通知日志'
      }
    }
  ]
