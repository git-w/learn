## 开发

```bash
# 克隆项目
git clone https://gitee.com/y_project/jingxuan-Vue

# 进入项目目录
cd jingxuan-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 一 命名
### 1. 项目命名
```
全部采用小写方式，以下划线分隔。例：sqy-marry_admin
1.后台管理端命名：project_admin
2.pc端命名：project_pc
3.h5端命名：project_h5
```

### 2. 结构目录命名

> 目录结构需与原型图的业务模块划分清晰，禁止A业务版块下创建B业务版块的文件 同时创建时应以版块下的模块业务名称去创建文件，如图所示：

```
后台管理端目录：
newProject/src文件目录
├─api  接口文件
    ├─bread.js 面包屑文件：导出一个大对象
    ├─system.js 接口文件：导出各接口的方法主要是为了在页面内进行按需引入的作用
├─assets  静态资源
    ├─css 样式资源
    ├─img 图片资源
├─components  公用级别组件 
├─mixins 混入
├─router  路由
    ├─index.js 入口文件，对路由进行的拦截封装
    ├─router.js 路由文件主要内容是对路由的模块化引入
    ├─system.js 模块路由文件
├─store   vuex的使用存储用户相关信息
├─plugin   组件管理
    ├─index.js   全局组件的注册
├─utils   工具类
    ├─’*’   各模块抽取的公共方法
    ├─request.js   请求封装函数
    ├─auth.js  cookie的封装用来保存客户的信息
    ├─loading.js  加载层
├─pages  路由级别组件
    ├─system  系统设置板块
        ├─account  系统管理员模块
            ├─account.vue  页面
     	    ├─account.js  逻辑
    ├─account.less  样式
├─App.vue 根组件
├─main.js 入口文件
```