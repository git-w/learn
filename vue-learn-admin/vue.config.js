const path = require('path') //加载path模块
const TerserPlugin = require('terser-webpack-plugin') //去除console.log和debugger
const CompressionWebpackPlugin = require('compression-webpack-plugin') //压缩Webpack插件
const { HashedModuleIdsPlugin } = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin //打包分析
const webpack = require('webpack') //引入webpack
function resolve(dir) {
  return path.join(__dirname, dir)
}

const isProduction = process.env.NODE_ENV === 'production'

// cdn预加载使用   externals:外部扩展  从输出的 bundle 中排除依赖
const externals = {
  vue: 'Vue',
  'vue-router': 'VueRouter',
  vuex: 'Vuex',
  axios: 'axios',
  'element-ui': 'ELEMENT'
}

const cdn = {
  // 开发环境
  dev: {
    css: ['https://cdn.jsdelivr.net/npm/element-ui/lib/theme-chalk/index.css'],
    js: []
  },
  // 生产环境
  build: {
    css: ['https://cdn.jsdelivr.net/npm/element-ui/lib/theme-chalk/index.css'],
    js: [
      'https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.min.js',
      'https://cdn.jsdelivr.net/npm/vuex@3.6.0/dist/vuex.min.js',
      'https://cdn.jsdelivr.net/npm/vue-router@3.1.3/dist/vue-router.min.js',
      'https://cdn.jsdelivr.net/npm/element-ui@2.13.0/lib/index.js',
      'https://cdn.jsdelivr.net/npm/axios@0.19.0/dist/axios.min.js'
    ]
  }
}

module.exports = {
  lintOnSave: false, // 关闭eslint
  productionSourceMap: false, //关闭sourceMap映射
  publicPath: '/', // 基础路径 ./ 或者  /

  chainWebpack: config => {
    config.resolve.alias.set('@', resolve('src')) //alias别名
    config.plugins.delete('preload') //浏览器页面必定需要的资源，浏览器一定会加载这些资源
    config.plugins.delete('prefetch') //浏览器页面可能需要的资源，浏览器不一定会加载这些资源
    //分环境配置cdn
    config.plugin('html').tap(args => {
      if (process.env.NODE_ENV === 'production') {
        args[0].cdn = cdn.build
      }
      if (process.env.NODE_ENV === 'development') {
        args[0].cdn = cdn.dev
      }
      return args
    })
  },

  configureWebpack: config => {
    const plugins = []
    if (isProduction) {
      // 开启分离js
      config.optimization = {
        splitChunks: {
          chunks: 'all', //拆分模块的范围  all表示以上两者都包括
          maxAsyncRequests: 6, // 最大的异步请求数量,也就是同时加载的模块最大模块数量
          maxInitialRequests: 4, //允许入口并行加载的最大请求数
          cacheGroups: {
            vendor: {
              minChunks: 2, //最少引用次数
              chunks: 'all', //
              test: /[\\/]node_modules[\\/]/, //打包的位置
              name: 'vendor',

              reuseExistingChunk: true, //如果一个模块已经被打包过了,那么再打包时就忽略这个上模块
              priority: -10 //根据优先级决定打包到哪个组里，
            },
            common: {
              chunks: 'all',
              test: /[\\/]src[\\/]/,
              name: 'bag',
              minChunks: 2, // 引入的次数大于等于3时才进行代码分割
              reuseExistingChunk: true, //如果一个模块已经被打包过了,那么再打包时就忽略这个上模块
              priority: -20 //根据优先级决定打包到哪个组里，
            }
          }
        }
      }
      // 取消webpack警告的性能提示
      config.performance = {
        hints: 'warning',
        //入口起点的最大体积
        maxEntrypointSize: 1000 * 500,
        //生成文件的最大体积
        maxAssetSize: 1000 * 1000,
        //只给出 js 文件的性能提示
        assetFilter: function(assetFilename) {
          return assetFilename.endsWith('.js')
        }
      }

      plugins.push(
        new TerserPlugin({
          terserOptions: {
            compress: {
              drop_console: true, //去除console
              drop_debugger: true, //去除debugger
              pure_funcs: ['console.log']
            }
          }
        })
      ) // 去除console.log和debugger

      plugins.push(
        new CompressionWebpackPlugin({
          algorithm: 'gzip', // 压缩格式
          test: /\.(js|css)$/, // 匹配文件名
          threshold: 10240, // 对超过10k的数据压缩
          deleteOriginalAssets: false, // 不删除源文件
          minRatio: 0.8 // 压缩比
        })
      ) // 服务器也要相应开启gzip

      plugins.push(new HashedModuleIdsPlugin()) // 用于根据模块的相对路径生成 hash 作为模块 id, 一般用于生产环境
      plugins.push(
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/) // 会将所有本地化内容和核心功能一起打包,IgnorePlugin 在打包时忽略本地化内容:
      )
      plugins.push(new BundleAnalyzerPlugin()) // 开启打包分析

      plugins.push(
        new webpack.optimize.LimitChunkCountPlugin({
          maxChunks: 20 //使用大于或等于的值限制最大块数1。使用1将防止添加任何其他大块，因为条目/主大块也包括在计数中。
        })
      )
      plugins.push(
        new webpack.optimize.MinChunkSizePlugin({
          minChunkSize: 10240 // 每个包的最小体积
        })
      )
      // 打包时npm包转CDN
      config.externals = externals
    }

    return { plugins }
  },

  pluginOptions: {
    // 配置全局less
    'style-resources-loader': {
      preProcessor: 'less',
      patterns: [resolve('./src/style/theme.less')]
    }
  },
  devServer: {
    open: false, // 自动启动浏览器
    port: 9000, // 端口号
    hotOnly: false // 热更新
  }
}
