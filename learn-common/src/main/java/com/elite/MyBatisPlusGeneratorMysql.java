package com.elite;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Objects;


/**
 * @Title :MyBatisPlusGeneratorMysql.java
 * @Package: com.elite
 * @Description:代码生成器
 * @author: leroy
 * @data: 2020/4/25 11:47
 * @update: leroy
 */
public class MyBatisPlusGeneratorMysql {




    public static void main(String[] args) {

        AutoGenerator mpg = new AutoGenerator();

        //全局配置
        GlobalConfig gc = new GlobalConfig();
        //项目路径
        String projectPath = System.getProperty("user.dir");

        //生成文件工程设置
        gc.setOutputDir(projectPath + "/common/src/main/java/");





        gc.setIdType(IdType.UUID);
        gc.setFileOverride(true);//是否覆盖文件
        gc.setSwagger2(false);//是否自动生成接口文档
        gc.setActiveRecord(false);
        gc.setBaseResultMap(true);
        gc.setBaseColumnList(true);
        gc.setAuthor("leroy");//作者
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("elite2020");
        dsc.setUrl("jdbc:mysql://localhost:3306/learn?serverTimezone=Asia/Shanghai&characterEncoding=utf8&autoReconnect=true&zeroDateTimeBehavior=convertToNull");
        //如果有是使用下面未判断的数据库类型默认为String,  可以单独配置
        dsc.setTypeConvert(new ITypeConvert() {
            @Override
            public IColumnType processTypeConvert(GlobalConfig globalConfig, String s) {
                IColumnType iColumnType = null;
                if (s.contains("text")) {
                    iColumnType = DbColumnType.STRING;
                }
                if (s.contains("varchar")) {
                    iColumnType = DbColumnType.STRING;
                }
                if (s.contains("char")) {
                    iColumnType = DbColumnType.STRING;
                }
                if (s.contains("decimal")) {
                    iColumnType = DbColumnType.BIG_DECIMAL;
                }
                if (s.contains("int")) {
                    iColumnType = DbColumnType.INTEGER;
                }
                if (s.contains("timestamp")) {
                    iColumnType = DbColumnType.TIMESTAMP;
                }
                if (s.contains("datetime")) {
                    iColumnType = DbColumnType.LOCAL_DATE_TIME;
                }
                if (s.contains("date")) {
                    iColumnType = DbColumnType.DATE;
                }
                if (s.contains("float")) {
                    iColumnType = DbColumnType.FLOAT;
                }
                if (s.contains("boolean")) {
                    iColumnType = DbColumnType.BOOLEAN;
                }
                if (s.contains("bigint")) {
                    iColumnType = DbColumnType.LONG;
                }
                if (!Objects.nonNull(iColumnType)) {
                    iColumnType = DbColumnType.STRING;
                }
                return iColumnType;
            }
        });
        mpg.setDataSource(dsc);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setTablePrefix("t_");//去掉表前缀 t_
        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setInclude("t_activity_price");
        mpg.setStrategy(strategy);
        // 包配置
        PackageConfig pc = new PackageConfig();
        String packeg = "com.elite.learn.activityManage";
        pc.setParent(packeg);
        mpg.setPackageInfo(pc);
        // 执行生成
        mpg.execute();
        String path = "./common/src/main/java/" + packeg.replace(".", "/") + "/controller";
        deleteFile(new File(path));
        String copyAndDeletePath = "./common/src/main/java/" + packeg.replace(".", "/") + "/mapper/xml/";
        try {
            boolean flag = copyFile(copyAndDeletePath, "./common/src/main/resources/mapper/");
            if (flag) {
                deleteFile(new File(copyAndDeletePath));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static boolean copyFile(String oldFile, String newFile) throws Exception {
        File file = new File(oldFile);
        File file2 = new File(newFile);
        file2.mkdir();
        String[] fileName = file.list();
        File temp = null;

        if (oldFile.equals(newFile)) {
            System.out.println("已存在相同文件名");
        } else {
            for (int i = 0; i < fileName.length; i++) {
                if (oldFile.endsWith(File.separator)) {// \
                    temp = new File(oldFile + fileName[i]);
                } else {
                    temp = new File(oldFile + File.separator + fileName[i]);
                }
                if (temp.isFile()) {
                    FileInputStream in = new FileInputStream(temp);
                    FileOutputStream ou = new FileOutputStream(newFile + "/" + temp.getName());

                    byte[] bs = new byte[1024];
                    int count = 0;
                    while ((count = in.read(bs, 0, bs.length)) != -1) {
                        ou.write(bs, 0, count);
                    }
                    ou.flush();
                    ou.close();
                    in.close();
                }
                if (temp.isDirectory()) {
                    copyFile(oldFile + "/" + fileName[i], newFile + "/" + fileName[i]);
                }
            }
        }
        return true;
    }

    public static boolean deleteFile(File dirFile) {
        // 如果dir对应的文件不存在，则退出
        if (!dirFile.exists()) {
            return false;
        }
        if (dirFile.isFile()) {
            return dirFile.delete();
        } else {
            for (File file : dirFile.listFiles()) {
                deleteFile(file);
            }
        }
        return dirFile.delete();
    }

}