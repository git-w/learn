package com.elite.learn.messageManage.bll;

import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.messageManage.dto.UserMessageDto;
import com.elite.learn.messageManage.entity.UserMessage;
import com.elite.learn.messageManage.query.UserMessageQuery;
import com.elite.learn.messageManage.vo.UserMessageVo;

public interface IUserMessageServiceBLL extends ICommonServiceBLL<UserMessage, UserMessageDto, UserMessageVo, UserMessageQuery> {


    /**
     * 获取用户未读的条数
     *
     * @param userId
     * @return
     */
    Integer getCount(String userId);


    /**
     * 修改成已读
     *
     * @param operUserId
     * @return
     */
    boolean updateRead(String operUserId, BaseParams params);


    /**
     * 获取小程序端用户未读消息
     *
     * @param params
     * @return
     */
    PageResult<UserMessageVo> getWebList(UserMessageQuery params);


    /**
     * 发送小程序消息
     *
     * @param userId 用户id
     * @param type   消息类型
     * @return
     */
    boolean send(Integer type, String userId);


    /**
     * 发送小程序消息
     *
     * @param userId 和用户发送系统通告
     * @return
     */
    boolean sendSysMessage(String userId);
}
