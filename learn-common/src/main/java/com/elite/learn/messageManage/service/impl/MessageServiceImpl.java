package com.elite.learn.messageManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.messageManage.entity.Message;
import com.elite.learn.messageManage.mapper.MessageMapper;
import com.elite.learn.messageManage.service.IMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.messageManage.vo.MessageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统消息-通告表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/18 14:05
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<MessageVo> getLists(Page<MessageVo> pageParams, @Param(Constants.WRAPPER) Wrapper<MessageVo> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));
        return pageParams;
    }
}
