package com.elite.learn.messageManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.messageManage.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.messageManage.vo.MessageVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统消息-通告表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
public interface MessageMapper extends BaseMapper<Message> {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/18 14:07
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<MessageVo> getLists(Page<MessageVo> pageParams, @Param(Constants.WRAPPER) Wrapper<MessageVo> queryWrapper);
}
