package com.elite.learn.messageManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.messageManage.bll.IUserMessageServiceBLL;
import com.elite.learn.messageManage.dto.UserMessageDto;
import com.elite.learn.messageManage.entity.Message;
import com.elite.learn.messageManage.entity.UserMessage;
import com.elite.learn.messageManage.query.UserMessageQuery;
import com.elite.learn.messageManage.service.IMessageService;
import com.elite.learn.messageManage.service.IUserMessageService;
import com.elite.learn.messageManage.vo.UserMessageVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 系统消息-用户消息表
 * @Title: UserMessageServiceBLL
 * @Package com.elite.learn.messageCenterManage.bll.imp
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/18 10:38
 */
@Service
public class UserMessageServiceBLL implements IUserMessageServiceBLL {


    @Resource
    private IUserMessageService service;
    //系统消息-通告表
    @Resource
    private IMessageService messageService;


    /**
     * 主键id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/20 10:11
     * @update
     * @updateTime
     */
    @Override
    public UserMessage get(String id) {
        return this.service.getById(id);
    }


    @Override
    public UserMessageVo getInfo(String id) {
        return null;
    }


    /**
     * 保存
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/20 10:12
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, UserMessageDto bean) {
        //创建实体对象
        UserMessage info = new UserMessage();
        //编号
        info.setId(IdUtils.simpleUUID());
        //用户 0:未读 1:已读
        info.setUserId(bean.getUserId());
        info.setMessageId(bean.getMessageId());
        //额外参数
        info.setExtraParams1(bean.getExtraParams1());

        //阅读时间
        info.setCreateTime(System.currentTimeMillis());
        boolean result = this.service.save(info);

        return result ? info.getId() : null;
    }

    @Override
    public boolean update(String operUserId, UserMessageDto bean) {
        return false;
    }

    @Override
    public boolean remove(String operUserId, String ids) {
        return false;
    }

    @Override
    public List<UserMessageVo> findAll(UserMessageQuery bean) {
        return null;
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/18 14:58
     * @update
     * @updateTime
     */
    @Override
    public PageResult<UserMessageVo> getList(UserMessageQuery params) {
        //构建条件选择器
        QueryWrapper<UserMessageVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNotNull("tium.user_id");
        queryWrapper.orderByDesc("tium.create_time");
        Page<UserMessageVo> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<UserMessageVo> page = this.service.getLists(PageParams, queryWrapper);
        return new PageResult<>(page);
    }


    /**
     * 查询未读的条数
     *
     * @param userId
     * @return
     */
    @Override
    public Integer getCount(String userId) {
        //构建条件选择器
        QueryWrapper<UserMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_read", 0);
        queryWrapper.eq("user_id", userId);
        return this.service.count(queryWrapper);
    }


    /**
     * 修改已读状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean updateRead(String operUserId, BaseParams params) {
        boolean flag = true;
        //每个id已逗号分割
        String[] idArr = params.getIds().split(",");
        //遍历id
        UserMessage infoNew = null;
        for (String id : idArr) {
            //获取id判断是否为空
            UserMessage info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            //创建删除实体
            infoNew = new UserMessage();
            //设置删除状态
            infoNew.setId(info.getId());
            infoNew.setIsRead(1);
            infoNew.setReadTime(System.currentTimeMillis());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     *  获取小程序端用户未读消息
     * @author: leroy
     * @date 2022/1/14 14:43
     * @param params
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public PageResult<UserMessageVo> getWebList(UserMessageQuery params) {
        //构建条件选择器
        QueryWrapper<UserMessageVo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(params.getUserId())) {
            queryWrapper.eq("tium.user_id", params.getUserId());
        }
        queryWrapper.orderByDesc("tium.create_time");
        Page<UserMessageVo> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<UserMessageVo> page = this.service.getWebLists(PageParams, queryWrapper);
        return new PageResult<>(page);
    }


    /**
     * 发送消息通知的话选择新消息类型消息类型:1审核成功通知 2审核失败通  3会员卡购买成功知 4新用户注册成功通知  5免费会员卡领取成功通知
     * @param type 消息类型
     * @param userId 用户id
     * @return
     */
    @Override
    public boolean send(Integer type, String userId) {
        //构建条件选择器
        QueryWrapper<Message> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq(" is_type", type);
        queryWrapper.eq(" is_message", 1);
        Message MessageInfo = this.messageService.getOne(queryWrapper);
        if (Objects.nonNull(MessageInfo)) {
            //创建实体对象
            UserMessage info = new UserMessage();
            //编号
            info.setId(IdUtils.simpleUUID());
            //用户 0:未读 1:已读
            info.setUserId(userId);
            info.setMessageId(MessageInfo.getId());
            info.setIsRead(0);
            info.setCreateTime(System.currentTimeMillis());
            //阅读时间
            info.setCreateTime(System.currentTimeMillis());
            boolean result = this.service.save(info);
        }
        return false;
    }

    /**
     * 和系统用户发送系统通告
     * @param userId 和用户发送系统通告
     * @return
     */
    @Override
    public boolean sendSysMessage(String userId) {
        //构建条件选择器
        QueryWrapper<Message> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq(" is_user_all", 0);
        queryWrapper.eq(" is_undo", 0);
        queryWrapper.eq(" is_message", 0);
        List<Message> list = this.messageService.list(queryWrapper);
        if(list.size()>0){
            UserMessage UserMessageInfo =null;
            for(Message info:list){
                //构建条件选择器
                QueryWrapper<UserMessage> queryMessageWrapper = new QueryWrapper<>();
                queryMessageWrapper.eq("message_id", info.getId());
                queryMessageWrapper.eq("user_id", userId);
                 if(service.count(queryMessageWrapper)<=0){
                     //创建实体对象
                      UserMessageInfo = new UserMessage();
                     //编号
                     UserMessageInfo.setId(IdUtils.simpleUUID());
                     //用户 0:未读 1:已读
                     UserMessageInfo.setUserId(userId);
                     UserMessageInfo.setMessageId(info.getId());
                     UserMessageInfo.setIsRead(0);
                     UserMessageInfo.setCreateTime(System.currentTimeMillis());
                     //阅读时间
                     UserMessageInfo.setCreateTime(System.currentTimeMillis());
                     this.service.save(UserMessageInfo);
                 }
            }
        }
        return false;
    }
}