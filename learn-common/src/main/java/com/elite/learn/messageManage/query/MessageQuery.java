package com.elite.learn.messageManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统消息-通告表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
@Data
public class MessageQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;



    /**
     * 用户类型 0:通知全部用户 1:部分用户
     */
    private Integer isUserId;





}
