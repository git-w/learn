package com.elite.learn.messageManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统消息-通告表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_message")
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 消息标题备注
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 是否撤销 0:未撤销 1:撤销
     */
    private Integer isUndo;

    /**
     * 撤销时间
     */
    private Long undoTime;

    /**
     * 系统消息
     */
    private String Message;

    /**
     * 用户类型 0:通知全部用户 1:部分用户
     */
    private Integer isUserAll;

    /**
     * 信息类型 0:公告 1:个人信息
     */
    private Integer isMessage;
    /**
     * 信息类型 0:公告 1:个人信息
     */
    private Integer isType;
    /**
     * 发布者
     */
    private String createOperatorId;

    /**
     * 发布时间
     */
    private Long createTime;



    /**
     * 额外参数1
     */
    private String extraParams1;

    /**
     * 额外参数2
     */
    @TableField("extra_Params2")
    private String extraParams2;

    /**
     * 额外参数3
     */
    @TableField("extra_Params3")
    private String extraParams3;


}
