package com.elite.learn.messageManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统消息-用户消息表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_user_message")
public class UserMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     *用户ID
     */
    private String userId;

    /**
     *用户ID
     */
    private String messageId;




    /**
     * 用户 0:未读 1:已读
     */
    private Integer isRead;

    /**
     * 阅读时间
     */
    private Long createTime;
    /**
     * 额外参数1
     */
    private Long readTime;

    /**
     * 额外参数2
     */
    private String extraParams1;




}
