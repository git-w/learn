package com.elite.learn.messageManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.core.page.PageResult;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.messageManage.bll.IMessageServiceBLL;
import com.elite.learn.messageManage.contants.MessageCenterManageExceptionCodeEnum;
import com.elite.learn.messageManage.dto.MessageDto;
import com.elite.learn.messageManage.entity.Message;
import com.elite.learn.messageManage.entity.UserMessage;
import com.elite.learn.messageManage.query.MessageQuery;
import com.elite.learn.messageManage.service.IMessageService;
import com.elite.learn.messageManage.service.IUserMessageService;
import com.elite.learn.messageManage.vo.MessageVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 系统消息-通告表
 * @Title: MessageServiceBLL
 * @Package com.elite.learn.messageCenterManage.bll
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/18 10:35
 */
@Service
public class MessageServiceBLL implements IMessageServiceBLL {

    @Resource
    private IMessageService service;
    //系统消息-用户消息表 服务类
    @Resource
    private IUserMessageService userMessageService;


    /**
     * 主键id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/18 14:11
     * @update
     * @updateTime
     */
    @Override
    public Message get(String id) {
        return this.service.getById(id);
    }


    @Override
    public MessageVo getInfo(String id) {
        return null;
    }


    /**
     * 保存
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/18 13:03
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, MessageDto bean) {
        //创建实体对象
        Message info = new Message();
        //用户生成的id
        info.setId(IdUtils.simpleUUID());
        //消息标题备注
        info.setTitle(bean.getTitle());
        //内容
        info.setContent(bean.getContent());
        //是否撤销 0:未撤销 1:撤销
        info.setIsUndo(bean.getIsUndo());
        //撤销时间
        info.setUndoTime(bean.getUndoTime());
        //系统消息
        info.setMessage(bean.getMessage());
        // 用户类型 0:通知全部用户 1:部分用户
        info.setIsUserAll(bean.getIsUserAll());
        if (bean.getIsUserAll() == 1) {
            if (StringUtils.isNotEmpty(bean.getUserIds())) {
                //每个id已逗号分割
                String[] idsArr = bean.getUserIds().split(",");
                List<UserMessage> list = new ArrayList<>();
                UserMessage message = null;
                //遍历id并判断是否为空
                for (String id : idsArr) {
                    message = new UserMessage();
                    message.setUserId(id);
                    message.setMessageId(info.getId());
                    message.setIsRead(0);
                    message.setCreateTime(System.currentTimeMillis());
                    message.setExtraParams1(bean.getExtraParams1());
                    list.add(message);
                }
                this.userMessageService.saveBatch(list);
            }
        }
        //信息类型 0:公告 1:个人信息
        info.setIsMessage(bean.getIsMessage());
        //信息类型 0:公告 1:个人信息
        info.setIsType(bean.getIsType());
        //额外参数
        info.setExtraParams1(bean.getExtraParams1());
        info.setExtraParams2(bean.getExtraParams2());
        info.setExtraParams3(bean.getExtraParams3());
        //发布者
        info.setCreateOperatorId(operUserId);
        //发布时间
        info.setCreateTime(System.currentTimeMillis());

        boolean result = this.service.save(info);
        return result ? info.getId() : null;
    }


    @Override
    public boolean update(String operUserId, MessageDto bean) {
        // 判断通过ID获取的对象是否为空
        Message info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(MessageCenterManageExceptionCodeEnum.SelectFail.code,
                    MessageCenterManageExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        Message infoNew = new Message();
        //用户生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //消息标题备注
        if (StringUtil.notBlank(bean.getTitle())) {
            infoNew.setTitle(bean.getTitle());
        }
        //内容
        if (StringUtil.notBlank(bean.getContent())) {
            infoNew.setContent(bean.getContent());
        }
        //是否撤销 0:未撤销 1:撤销
        infoNew.setIsUndo(bean.getIsUndo());
        //撤销时间
        infoNew.setUndoTime(bean.getUndoTime());
        //系统消息
        infoNew.setMessage(bean.getMessage());
        // 用户类型 0:通知全部用户 1:部分用户
        info.setIsUserAll(bean.getIsUserAll());
        if (bean.getIsUserAll() == 1 && bean.getIsUndo() == 0) {
            if (StringUtils.isNotEmpty(bean.getUserIds())) {
                //每个id已逗号分割
                String[] idsArr = bean.getUserIds().split(",");
                List<UserMessage> list = new ArrayList<>();
                UserMessage message = null;
                //遍历id并判断是否为空
                for (String id : idsArr) {
                    message = new UserMessage();
                    message.setUserId(id);
                    message.setMessageId(info.getId());
                    message.setIsRead(0);
                    message.setCreateTime(System.currentTimeMillis());
                    message.setExtraParams1(bean.getExtraParams1());
                    list.add(message);
                }
                this.userMessageService.saveBatch(list);
            }
        }

        //信息类型 0:公告 1:个人信息
        infoNew.setIsMessage(bean.getIsMessage());
        //信息类型 0:公告 1:个人信息
        infoNew.setIsType(bean.getIsType());
        //额外参数
        infoNew.setExtraParams1(bean.getExtraParams1());
        infoNew.setExtraParams2(bean.getExtraParams2());
        infoNew.setExtraParams3(bean.getExtraParams3());

        return this.service.updateById(infoNew);
    }

    @Override
    public boolean remove(String operUserId, String ids) {

        return false;
    }

    @Override
    public List<MessageVo> findAll(MessageQuery bean) {
        return null;
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/18 13:20
     * @update
     * @updateTime
     */
    @Override
    public PageResult<MessageVo> getList(MessageQuery params) {
        //构建条件选择器
        QueryWrapper<MessageVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNotNull("title");
        //过滤条件
        //queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("create_time");

        Page<MessageVo> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<MessageVo> page = this.service.getLists(PageParams, queryWrapper);
        return new PageResult<>(page);
    }
}