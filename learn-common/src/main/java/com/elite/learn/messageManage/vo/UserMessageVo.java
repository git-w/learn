package com.elite.learn.messageManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统消息-用户消息表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
@Data

public class UserMessageVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 用户 0:未读 1:已读
     */
    private Integer isRead;


    /**
     * 阅读时间
     */
    private Long readTime;

    /**
     * 阅读时间
     */
    private Long createTime;


    /**
     * 消息标题备注
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 是否撤销 0:未撤销 1:撤销
     */
    private Integer isUndo;

    /**
     * 撤销时间
     */
    private Long undoTime;
    /**
     * 系统消息
     */
    private String Message;
    /**
     * 信息类型 0:公告 1:个人信息
     */
    private Integer isUserAll;
    /**
     * 信息类型 0:公告 1:个人信息
     */
    private Integer isMessage;
    /**
     * 用户的昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String headimgurl;
}