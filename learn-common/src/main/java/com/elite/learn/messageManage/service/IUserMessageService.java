package com.elite.learn.messageManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.messageManage.entity.UserMessage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.messageManage.vo.UserMessageVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统消息-用户消息表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
public interface IUserMessageService extends IService<UserMessage> {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/18 15:02
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    IPage<UserMessageVo> getLists(Page<UserMessageVo> pageParams, @Param(Constants.WRAPPER) Wrapper<UserMessageVo> queryWrapper);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/18 15:02
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    IPage<UserMessageVo> getWebLists(Page<UserMessageVo> pageParams, @Param(Constants.WRAPPER) Wrapper<UserMessageVo> queryWrapper);

}
