package com.elite.learn.messageManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.messageManage.entity.UserMessage;
import com.elite.learn.messageManage.mapper.UserMessageMapper;
import com.elite.learn.messageManage.service.IUserMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.messageManage.vo.UserMessageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统消息-用户消息表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
@Service
public class UserMessageServiceImpl extends ServiceImpl<UserMessageMapper, UserMessage> implements IUserMessageService {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/18 15:06
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<UserMessageVo> getLists(Page<UserMessageVo> pageParams, @Param(Constants.WRAPPER) Wrapper<UserMessageVo> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));
        return pageParams;
    }


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/18 15:06
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<UserMessageVo> getWebLists(Page<UserMessageVo> pageParams, @Param(Constants.WRAPPER) Wrapper<UserMessageVo> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));
        return pageParams;
    }
}
