package com.elite.learn.messageManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.messageManage.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.messageManage.vo.MessageVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统消息-通告表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
public interface IMessageService extends IService<Message> {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/18 13:18
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    IPage<MessageVo> getLists(Page<MessageVo> pageParams, @Param(Constants.WRAPPER) Wrapper<MessageVo> queryWrapper);

}
