package com.elite.learn.messageManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统消息-用户消息表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
@Data
public class UserMessageQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;


    /**
     * 编号
     */
    private String id;

    /**
     * 用户 0:未读 1:已读
     */
    private Integer isUser;

    /**
     * 阅读时间
     */

    private Long createTime;
    /**
     * 用户id
     */
    private String userId;


}
