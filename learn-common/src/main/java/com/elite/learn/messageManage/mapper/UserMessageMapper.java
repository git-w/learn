package com.elite.learn.messageManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.messageManage.entity.UserMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.messageManage.vo.UserMessageVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统消息-用户消息表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
public interface UserMessageMapper extends BaseMapper<UserMessage> {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/18 15:08
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<UserMessageVo> getLists(Page<UserMessageVo> pageParams, @Param(Constants.WRAPPER) Wrapper<UserMessageVo> queryWrapper);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/18 15:08
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<UserMessageVo> getWebLists(Page<UserMessageVo> pageParams, @Param(Constants.WRAPPER) Wrapper<UserMessageVo> queryWrapper);

}
