package com.elite.learn.messageManage.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 系统消息-通告表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-18
 */
@Data

public class MessageDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * 消息标题备注
     */
    @NotBlank(message = "消息标题,参数不能为空")
    private String title;

    /**
     * 内容
     */
    @NotBlank(message = "消息内容,参数不能为空")
    private String content;

    /**
     * 是否撤销 0:未撤销 1:撤销
     */
    private Integer isUndo;

    /**
     * 撤销时间
     */
    private Long undoTime;

    /**
     * 系统消息
     */
    private String Message;

    /**
     * 用户类型 0:通知全部用户 1:部分用户
     */
    private Integer isUserAll;

    /**
     * 信息类型 0:公告 1:个人信息
     */
    private Integer isMessage;
    /**
     * 信息类型 0:公告 1:个人信息
     */
    private Integer isType;
    /**
     * 发布者
     */
    private String createOperatorId;

    /**
     * 发布时间
     */
    private Long createTime;



    /**
     * 额外参数1
     */
    private String extraParams1;

    /**
     * 额外参数2
     */
    private String extraParams2;

    /**
     * 额外参数3
     */
    private String extraParams3;


    /**
     * 额外参数3
     */
    private String userIds;


}
