package com.elite.learn.messageManage.bll;

import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.messageManage.dto.MessageDto;
import com.elite.learn.messageManage.entity.Message;
import com.elite.learn.messageManage.query.MessageQuery;
import com.elite.learn.messageManage.vo.MessageVo;

public interface IMessageServiceBLL extends ICommonServiceBLL<Message, MessageDto, MessageVo, MessageQuery> {


}
