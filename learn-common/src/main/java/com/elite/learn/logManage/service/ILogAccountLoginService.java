package com.elite.learn.logManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.logManage.entity.LogAccountLogin;
import com.elite.learn.logManage.vo.LogAccountLoginVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统管理-管理员登录日志 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-24
 */
public interface ILogAccountLoginService extends IService<LogAccountLogin> {


    /**
     * 登录日志分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<LogAccountLoginVo> selectLists(Page<LogAccountLoginVo> page, @Param(Constants.WRAPPER) Wrapper<LogAccountLogin> queryWrapper);

}
