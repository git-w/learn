package com.elite.learn.logManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.logManage.entity.UserPay;
import com.elite.learn.logManage.vo.UserPayVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 支付日志记录表---用户支付记录表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
public interface IUserPayService extends IService<UserPay> {


    /**
     * 登录日志分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<UserPayVO> getList(Page<UserPayVO> page, @Param(Constants.WRAPPER) Wrapper<UserPay> queryWrapper);

}
