package com.elite.learn.logManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.logManage.entity.LogUserLogin;
import com.elite.learn.logManage.vo.LogUserLoginVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 小程序用户登录日志表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
public interface LogUserLoginMapper extends BaseMapper<LogUserLogin> {


    /**
     * 自定义分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    List<LogUserLoginVo> selectLists(Page<LogUserLoginVo> page, @Param(Constants.WRAPPER) Wrapper<LogUserLogin> queryWrapper);

}
