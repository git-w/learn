package com.elite.learn.logManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.logManage.entity.LogAccountLogin;
import com.elite.learn.logManage.vo.LogAccountLoginVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * <p>
 * 系统管理-管理员登录日志 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-05-24
 */
public interface LogAccountLoginMapper extends BaseMapper<LogAccountLogin> {


    /**
     * 自定义分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    List<LogAccountLoginVo> selectLists(Page<LogAccountLoginVo> page, @Param(Constants.WRAPPER) Wrapper<LogAccountLogin> queryWrapper);

}
