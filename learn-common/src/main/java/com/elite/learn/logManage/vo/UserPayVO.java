package com.elite.learn.logManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 支付日志记录表---用户支付记录表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
@Data
public class UserPayVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 关联订单id
     */
    private String orderId;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 支付方式 1.微信扫码2.微信公众号内支付3.微信h5支付4.其他
     */
    private String payType;

    /**
     * 订单类型 0-购买会员卡
     */
    private Integer orderType;

    /**
     * 签名
     */
    private String appSign;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 发生的金额 单位 分
     */
    private Integer payMoney;

    /**
     * 状态 1.未支付2.支付成功
     */
    private Integer isStatus;

    /**
     * 错误原因
     */
    private String reason;

    /**
     * 交易号
     */
    private String payNo;

    /**
     * 支付完成时间
     */
    private String endTime;

    /**
     * 用户对微信公众号的唯一标识
     */
    private String openId;

    /**
     * 商户号
     */
    private String mchId;

    /**
     * appid
     */
    private String appId;

    /**
     * 微信支付url
     */
    private String codeUrl;

    /**
     * 备注
     */
    private String extraParams1;



    /**
     * 用户的昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String headimgurl;

    /**
     * 手机号
     */
    private String modilePhone;

}
