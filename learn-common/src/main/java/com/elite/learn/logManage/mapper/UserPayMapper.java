package com.elite.learn.logManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.logManage.entity.UserPay;
import com.elite.learn.logManage.vo.UserPayVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 支付日志记录表---用户支付记录表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
public interface UserPayMapper extends BaseMapper<UserPay> {


    /**
     * 自定义分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    List<UserPayVO> getList(Page<UserPayVO> page, @Param(Constants.WRAPPER) Wrapper<UserPay> queryWrapper);

}
