package com.elite.learn.logManage.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 用户收藏表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
@Data
public class UserCollectDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 收藏表 主键ID
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * 用户ID
     */

    private String userId;

    /**
     * 收藏ID
     */

    private String collectId;

    /**
     * 收藏类型 0—活动 1-情缘
     */

    private Integer isType;


    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;
    /**
     * 收藏 0收藏 1未收藏
     */
    private Integer iscollect;

}
