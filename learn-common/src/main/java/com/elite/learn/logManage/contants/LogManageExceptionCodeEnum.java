package com.elite.learn.logManage.contants;

public enum LogManageExceptionCodeEnum {
    SaveSuccess(100000, "保存成功"),
    SelectSuccess(100000, "查询成功"),
    DeleteSuccess(100000, "删除成功"),
    CollectSuccess(100000, "收藏或者取消收藏成功"),
    ParamsError(100001, "参数为空"),
    SaveFail(100005, "保存失败"),
    DeleteFail(100005, "删除失败"),
    SelectFail(100005, "查询失败"),
    REPAIRERROR(700001,"退款失败"),
    SYSTEMERROR(700001,"系统超时等"),
    CollectFail(100005, "已经被收藏，不能重复收藏"),
    CollectErrorFail(100005, "收藏或者取消收藏失败"),
    UpdateFail(100005, "修改失败");

    public Integer code;
    public String msg;

    LogManageExceptionCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
