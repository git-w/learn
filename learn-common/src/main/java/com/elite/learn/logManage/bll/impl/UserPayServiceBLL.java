package com.elite.learn.logManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.logManage.bll.IUserPayServiceBLL;
import com.elite.learn.logManage.entity.UserPay;
import com.elite.learn.logManage.query.UserPayQuery;
import com.elite.learn.logManage.service.IUserPayService;
import com.elite.learn.logManage.vo.UserPayVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @create 2021-01-19 17:11
 * 支付日志记录表
 */

@Service
public class UserPayServiceBLL implements IUserPayServiceBLL {


    @Resource
    private IUserPayService service;


    /**
     * 添加
     *
     * @param params
     */
    @Override
    public void save(UserPay params) {
        if (Objects.nonNull(params)) {
            this.service.save(params);
        }
    }


    /**
     * 更新
     * @author: leroy
     * @date 2022/1/13 13:48
     * @param params
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public void update(UserPay params) {
        if (Objects.nonNull(params) && null != params.getId()) {
            this.service.updateById(params);
        }
    }


    /**
     * 详情
     * @author: leroy
     * @date 2022/1/13 13:49
     * @param orderNum
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public UserPay getInfo(String orderNum) {
        QueryWrapper<UserPay> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("order_num", orderNum);
        List<UserPay> list = this.service.list(queryWrapper);
        if (null != list && list.size() > 0) {
            return list.get(0);
        }
        return null;
    }


    /**
     * <p>
     * 列表
     * </p>
     *
     * @param query
     * @return com.elite.learn.tools.base.common.entity.page.PageResult<com.elite.learn.base.operation.order.common.entity.dto.UserPayDTO>
     * @author: leroy
     * @date 2021/7/15 11:26
     */
    @Override
    public PageResult<UserPayVO> getList(UserPayQuery query) {
        //构建条件选择器
        QueryWrapper<UserPay> queryWrapper = new QueryWrapper<>();
        //订单号
        if (Objects.nonNull(query.getOrderNum())) {
            queryWrapper.like("lup.order_num", query.getOrderNum());
        }
        // 用户名
        if (Objects.nonNull(query.getNickName())) {
            queryWrapper.like("iu.nick_name", query.getNickName());
        }

        // 用户名
        if (Objects.nonNull(query.getModilePhone())) {
            queryWrapper.like("iu.modile_phone", query.getModilePhone());
        }

        if (Objects.nonNull(query.getStartTime()) && Objects.nonNull(query.getEndTime())) {
            queryWrapper.between("lup.create_time", query.getStartTime(), query.getEndTime());
        } else if (Objects.nonNull(query.getStartTime())) {
            queryWrapper.ge("lup.create_time", query.getStartTime());
        } else if (Objects.nonNull(query.getEndTime())) {
            queryWrapper.le("lup.create_time", query.getEndTime());
        }
        queryWrapper.eq("iu.is_delete",0);
        //时间
        queryWrapper.orderByDesc("lup.create_time");
        //分页
        Page<UserPayVO> pageVo = new Page<>(query.getPageNum(), query.getPageSize());
        //分页和搜索
        IPage<UserPayVO> page = this.service.getList(pageVo, queryWrapper);
        //param集合
        return new PageResult<>(page);
    }
}
