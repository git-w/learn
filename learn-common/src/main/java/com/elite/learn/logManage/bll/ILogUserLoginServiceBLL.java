package com.elite.learn.logManage.bll;

import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.logManage.query.LogUserLoginQuery;
import com.elite.learn.logManage.vo.LogUserLoginVo;

public interface ILogUserLoginServiceBLL {


    /**
     * 记录登录日志
     *
     * @param userId  用户id
     */
    void save(String userId);


    /**
     * 分页查看 登录日志
     *
     * @param params
     * @return
     */
    PageResult<LogUserLoginVo> getList(LogUserLoginQuery params);
}
