package com.elite.learn.logManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.logManage.entity.UserCollect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.logManage.vo.UserCollectActivityVo;
import com.elite.learn.logManage.vo.UserCollectVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户收藏表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
public interface UserCollectMapper extends BaseMapper<UserCollect> {


    /**
     * 活动
     * @author: leroy
     * @date 2021/12/13 13:26
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<UserCollectVo> getListsActivity(Page<UserCollectVo> pageParams,@Param(Constants.WRAPPER) Wrapper<UserCollect> queryWrapper);


    /**
     * 情缘
     * @author: leroy
     * @date 2021/12/13 13:27
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<UserCollectVo> getLists(Page<UserCollectVo> pageParams, @Param(Constants.WRAPPER)Wrapper<UserCollect> queryWrapper);
}
