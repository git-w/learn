package com.elite.learn.logManage.bll;

import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.logManage.dto.UserCollectDTO;
import com.elite.learn.logManage.entity.UserCollect;
import com.elite.learn.logManage.query.UserCollectquery;
import com.elite.learn.logManage.vo.UserCollectVo;

public interface IUserCollectServiceBLL   extends ICommonServiceBLL<UserCollect, UserCollectDTO, UserCollectVo,UserCollectquery> {


    /**
     * 收藏或者取消收藏
     * @param operUserId
     * @param params
     * @return
     */
    boolean saveOrDelecte(String operUserId, UserCollectDTO params);


    /**
     *活动分页
     * @author: leroy
     * @date 2021/12/13 13:21
     * @param params
     * @return null
     * @update
     * @updateTime
     */
    PageResult<UserCollectVo> getListActivity(UserCollectquery params);


    /**
     * 查看用户是否有收藏
     * @param operUserId
     * @param id
     * @return
     */
    boolean isCollect(String operUserId, String id);


    /**
     * 查看用户收藏总数
     * @param operUserId
     * @param
     * @return
     */
    int getCount(String operUserId);
}
