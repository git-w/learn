package com.elite.learn.logManage.bll;

import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.logManage.entity.UserRefundPay;
import com.elite.learn.logManage.query.UserRefundPayQuery;
import com.elite.learn.logManage.vo.UserRefundPayVO;

import java.math.BigDecimal;

/**
 * <p>
 * 订单退款记录表BLL 服务类
 * </p>
 *
 * @author: leroy
 * @date 2021/7/15 13:51
 */
public interface IUserRefundPayServiceBLL {


    /**
     * <p>
     * 列表
     * </p>
     *
     * @param query
     * @return com.elite.learn.tools.base.common.entity.page.PageResult<com.elite.learn.base.operation.order.common.entity.dto.UserPayDTO>
     * @author: leroy
     * @date 2021/7/15 13:54
     */
    PageResult<UserRefundPayVO> getList(UserRefundPayQuery query);


    /**
     * <p>
     * 记录退款成功日志
     * </p>
     *
     * @param logOrderRefund
     * @author: leroy
     * @date 2021/6/24 11:39
     */
    void saveLog(UserRefundPay logOrderRefund);


    /**
     * <p>
     * 修改退款日志状态
     * </p>
     *
     * @param logOrderRefund
     * @author: leroy
     * @date 2021/6/24 11:39
     */
    void updateLog(UserRefundPay logOrderRefund);


    /**
     * <p>
     * 记录退款成功日志
     * </p>
     *
     * @param logOrderRefund
     * @return com.elite.learn.base.operation.order.pay.service.autogenerator.entity.LogOrderRefund
     * @author: leroy
     * @date 2021/6/24 11:40
     */
    UserRefundPay getInfo(UserRefundPay logOrderRefund);


    /**
     * <p>
     * 订单退款
     * </p>
     *
     * @param orderNum
     * @param orderType
     * @param totalFee
     * @param userId
     * @param descript
     * @return boolean
     * @author: leroy
     * @date 2021/6/24 11:40
     */
    boolean OrderRepair(String orderId,String orderNum, Integer orderType, BigDecimal totalFee, String userId, String descript);

}
