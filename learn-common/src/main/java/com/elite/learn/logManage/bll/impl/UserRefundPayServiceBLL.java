package com.elite.learn.logManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.core.page.PageResult;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.wx.wxpay.WechatHttpClient;
import com.elite.learn.common.utils.wx.wxpay.WechatRefund;
import com.elite.learn.logManage.bll.IUserRefundPayServiceBLL;
import com.elite.learn.logManage.contants.LogManageExceptionCodeEnum;
import com.elite.learn.logManage.entity.UserRefundPay;
import com.elite.learn.logManage.query.UserRefundPayQuery;
import com.elite.learn.logManage.service.IUserRefundPayService;
import com.elite.learn.logManage.vo.UserRefundPayVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 订单退款记录表BLL 业务服务类
 * </p>
 *
 * @author: leroy
 * @date 2021/7/15 13:51
 */
@Service
public class UserRefundPayServiceBLL implements IUserRefundPayServiceBLL {

    @Resource
    private IUserRefundPayService service;


    /**
     * <p>
     * 列表
     * </p>
     *
     * @param query
     * @return com.elite.learn.tools.base.common.entity.page.PageResult<com.elite.learn.base.operation.order.common.entity.dto.UserRefundPayVO>
     * @author: leroy
     * @date 2021/7/15 13:56
     */
    @Override
    public PageResult<UserRefundPayVO> getList(UserRefundPayQuery query) {

        //构建条件选择器
        QueryWrapper<UserRefundPay> queryWrapper = new QueryWrapper<>();
        //时间
        queryWrapper.orderByDesc("lup.create_time");

        //分页
        Page<UserRefundPayVO> pageVo = new Page<>(query.getPageNum(), query.getPageSize());
        //分页和搜索
        IPage<UserRefundPayVO> page = this.service.getList(pageVo, queryWrapper);
        //param集合

        return new PageResult<>(page);
    }


    /**
     * 记录退款成功日志
     * @author: leroy
     * @date 2022/1/13 13:55
     * @param info
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public void saveLog(UserRefundPay info) {
        if (Objects.nonNull(info)) {
            service.save(info);
        }
    }

    @Override
    public void updateLog(UserRefundPay info) {
        service.updateById(info);
    }


    @Override
    public UserRefundPay getInfo(UserRefundPay params) {
        QueryWrapper<UserRefundPay> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        if (StringUtils.isNotEmpty(params.getOrderNum())) {
            queryWrapper.eq("order_num", params.getOrderNum());
            List<UserRefundPay> list = service.list(queryWrapper);
            if (null != list && list.size() > 0) {
                return list.get(0);
            }
        }
        return null;
    }


    /**
     * 订单退款
     * @author: leroy
     * @date 2022/1/14 14:27
     * @param orderId
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public boolean OrderRepair(String orderId, String orderNum, Integer orderType, BigDecimal totalFee, String userId, String descript) {
        String id = IdUtils.simpleUUID();
        // 支付记录
        UserRefundPay repair = new UserRefundPay();
        repair.setOrderNum(orderNum);
        repair.setOrderId(orderId);
        repair.setId(id);
        repair.setCreateOperatorId(userId);
        repair.setUserId(userId);
        repair.setCreateTime(System.currentTimeMillis());
        repair.setStatus("1");
        saveLog(repair);

        // 金额由元转换成分
        totalFee = totalFee.multiply(new BigDecimal(100));
        // 金额换成int类型
        int total = totalFee.intValue();
        WechatRefund pay = WechatHttpClient.setWechatRefund(id, orderNum, total, total, descript, null);
        // 根据支付参数生成签名并下单
        Map<String, String> payMap = null;
        try {
            System.out.println("I am ----- PayLog");
            System.out.println("I am ----- PayLog");
            System.out.println("I am ----- PayLog");
            System.out.println("I am ----- PayLog");
            System.out.println("I am ----- PayLog");
            System.out.println(pay.toString());
            System.out.println("I am ----- PayLog");
            System.out.println("I am ----- PayLog");
            System.out.println("I am ----- PayLog");
            System.out.println("I am ----- PayLog");
            System.out.println("I am ----- PayLog");
            System.out.println("I am ----- PayLog");
            payMap = WechatHttpClient.WxRefund(pay);
            System.out.println("I am PayLog");
            System.out.println("I am PayLog");
            System.out.println("I am PayLog");
            System.out.println("I am PayLog");
            System.out.println(payMap);
            System.out.println("I am PayLog");
            System.out.println("I am PayLog");
            System.out.println("I am PayLog");
            System.out.println("I am PayLog");
            // 判断result_code是否为SUCCESS
            if (payMap.get("result_code").equals("SUCCESS")) {

                repair.setAppSign((payMap.get("sign")));
                System.out.println("------------111---------------");

                repair.setMchId(payMap.get("mch_id"));
                System.out.println("------------222---------------");
                repair.setAppId(payMap.get("appid"));
                System.out.println("------------333---------------");
                repair.setUserId(userId);
                System.out.println("------------444---------------");
                repair.setOrderNum(orderNum);
                System.out.println("------------555---------------");
                repair.setRefundMoney(total);
                System.out.println("------------666---------------");
                System.out.println("------------777---------------");
                System.out.println("------------888---------------");
                repair.setExtraParams1(descript);
                System.out.println("------------999---------------");
                repair.setCreateTime(System.currentTimeMillis());
                System.out.println("------------101010---------------");
                System.out.println("------------111111---------------");
                this.updateLog(repair);
                return true;
            } else {
                // 保存支付错误信息
                repair.setRefundReason(payMap.toString());
                this.updateLog(repair);
                throw new CommonException(LogManageExceptionCodeEnum.SYSTEMERROR.code,
                        LogManageExceptionCodeEnum.SYSTEMERROR.msg);
            }
        } catch (Exception e) {
            throw new CommonException(LogManageExceptionCodeEnum.REPAIRERROR.code,
                    LogManageExceptionCodeEnum.REPAIRERROR.msg);
        }
    }
}
