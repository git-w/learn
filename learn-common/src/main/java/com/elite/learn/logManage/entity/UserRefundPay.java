package com.elite.learn.logManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 支付日志记录表---用户退款支付记录表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_log_user_refund_pay")
public class UserRefundPay implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 关联订单id
     */
    private String orderId;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 退款方式 5.小程序支付6.其他
     */
    private String payType;

    /**
     * 签名
     */
    private String appSign;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 退款的金额 单位 分
     */
    private Integer refundMoney;

    /**
     * 状态 1.未退款2.已退款
     */
    private String status;

    /**
     * 错误原因
     */
    private String reason;

    /**
     * 交易号
     */
    private String payNo;

    /**
     * 退款完成时间
     */
    private String timeEnd;

    /**
     * 退款原因
     */
    private String refundReason;

    /**
     * 商户号
     */
    private String mchId;

    /**
     * appid
     */
    private String appId;

    /**
     * 微信退款单号
     */
    private String refundPayNo;

    /**
     * 备用字段，如果占用，请备注做什么用。
     */
    private String extraParams1;

    /**
     * 备用字段，如果占用，请备注做什么用。
     */
    private String extraParams2;

    /**
     * 备用字段，如果占用，请备注做什么用。
     */
    private String extraParams3;

    /**
     * 备用字段，如果占用，请备注做什么用。
     */
    private String extraParams4;

    /**
     * 备用字段，如果占用，请备注做什么用。
     */
    private String extraParams5;


}
