package com.elite.learn.logManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统管理-管理员登录日志
 * </p>
 *
 * @author: leroy
 * @since 2021-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_log_account_login")
public class LogAccountLogin implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 登录者id
     */
    @TableField("AccountId")
    private String AccountId;

    /**
     * 创建时间
     */
    @TableField("LoginTime")
    private Long LoginTime;

    /**
     * 浏览器信息
     */
    @TableField("BrowserDetails")
    private String BrowserDetails;

    /**
     * 登陆ip
     */
    @TableField("IpAddr")
    private String IpAddr;

    /**
     * 登录信息
     */
    @TableField("Message")
    private String Message;

    /**
     * 登录状态 0-成功 1-失败
     */
    @TableField("IsStatus")
    private Integer IsStatus;

    /**
     * 登录密码
     */
    @TableField("Password")
    private String Password;
}
