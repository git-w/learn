package com.elite.learn.logManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.core.page.PageResult;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.logManage.bll.IUserCollectServiceBLL;
import com.elite.learn.logManage.contants.LogManageExceptionCodeEnum;
import com.elite.learn.logManage.dto.UserCollectDTO;
import com.elite.learn.logManage.entity.UserCollect;
import com.elite.learn.logManage.query.UserCollectquery;
import com.elite.learn.logManage.service.IUserCollectService;
import com.elite.learn.logManage.vo.UserCollectVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 收藏表
 * @Title: UserCollectServiceBLLImpl
 * @Package com.elite.learn.logManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/11 10:22
 */
@Service
public class UserCollectServiceBLLImpl implements IUserCollectServiceBLL {

    @Resource
    private IUserCollectService service;


    /**
     * 主键id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/11 10:23
     * @update
     * @updateTime
     */
    @Override
    public UserCollect get(String id) {
        return this.service.getById(id);
    }


    /**
     * 详情
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/11 10:24
     * @update
     * @updateTime
     */
    @Override
    public UserCollectVo getInfo(String id) {
        return null;
    }


    @Override
    public String save(String operUserId, UserCollectDTO bean) {
        return null;
    }


    @Override
    public boolean update(String operUserId, UserCollectDTO bean) {

        return false;
    }


    /**
     * 删除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2021/12/11 11:03
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {


        return false;
    }


    @Override
    public List<UserCollectVo> findAll(UserCollectquery bean) {
        return null;
    }


    /**
     * 活动分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/13 13:19
     * @update
     * @updateTime
     */
    @Override
    public PageResult<UserCollectVo> getListActivity(UserCollectquery params) {

        QueryWrapper<UserCollect> queryWrapper = new QueryWrapper<UserCollect>();

        if (StringUtil.notBlank(params.getCreateOperatorId())) {
            queryWrapper.eq("tluc.create_operator_id", params.getCreateOperatorId());
        }
        if (Objects.nonNull(params.getIsType())) {
            queryWrapper.eq("tluc.is_type", params.getIsType());
        }
        queryWrapper.orderByDesc("tluc.create_time");
        // 收藏类型 0  queryWrapper.orderByDesc("tluc.create_time");—活动
        Page<UserCollectVo> PageParamsActivity = new Page<>(params.getPageNum(), params.getPageSize());

        IPage<UserCollectVo> page = this.service.getListsActivity(PageParamsActivity, queryWrapper);
        page.getRecords().forEach(item->{
            if (StringUtil.notBlank(item.getPictures())) {
                //拼接主图长地址
                item.setPicturesUrlPath(FileUploadUtil.getImgRootPath() + item.getPictures());
            }
        });
        return new PageResult<>(page);

    }


    /**
     * 查看用户是否收藏用户
     *
     * @param operUserId
     * @param id
     * @return
     */
    @Override
    public boolean isCollect(String operUserId, String id) {
        boolean flag = false;
        //查看是否已经收藏
        QueryWrapper<UserCollect> queryWrapper = new QueryWrapper<UserCollect>();
        queryWrapper.eq("create_operator_id", operUserId);
        queryWrapper.eq("collect_id", id);
        int count = this.service.count(queryWrapper);
        //条件查询数据的数量大于0 说明数据已经存在
        if (count > 0) {
            flag = true;
        }
        return flag;
    }


    /**
     * 查看用户收藏总数
     *
     * @param operUserId
     * @return
     */
    @Override
    public int getCount(String operUserId) {
        //查看是否已经收藏
        QueryWrapper<UserCollect> queryWrapper = new QueryWrapper<UserCollect>();
        queryWrapper.eq("create_operator_id", operUserId);
        int count = this.service.count(queryWrapper);
        return count;
    }


    /**
     * 分页
     * 情缘
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/11 11:11
     * @update
     * @updateTime
     */
    @Override
    public PageResult<UserCollectVo> getList(UserCollectquery params) {
        QueryWrapper<UserCollect> queryWrapper = new QueryWrapper<UserCollect>();
        queryWrapper.isNotNull("tluc.create_time");
        queryWrapper.orderByDesc("tluc.create_time");
        if (StringUtil.notBlank(params.getCreateOperatorId())) {
            queryWrapper.eq("tluc.create_operator_id", params.getCreateOperatorId());
        }

        if (Objects.nonNull(params.getIsType())) {
            queryWrapper.eq("tluc.is_type", params.getIsType());
        }

        Page<UserCollectVo> PageParams = new Page<>(params.getPageNum(), params.getPageSize());

        IPage<UserCollectVo> page = this.service.getLists(PageParams, queryWrapper);

        return new PageResult<>(page);
    }


    /**
     * 收藏或者取消收藏
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean saveOrDelecte(String operUserId, UserCollectDTO params) {
        boolean flag = false;
        // 取消收藏
        if (params.getIscollect() == 0) {
            //查看是否已经收藏
            QueryWrapper<UserCollect> queryWrapper = new QueryWrapper<UserCollect>();
            queryWrapper.eq("create_operator_id", operUserId);
            queryWrapper.eq("collect_id", params.getCollectId());
            int count = service.count(queryWrapper);
            //条件查询数据的数量大于0 说明数据已经存在
            if (count > 0) {
                throw new CommonException(LogManageExceptionCodeEnum.CollectFail.code, LogManageExceptionCodeEnum.CollectFail.msg);
            }

            UserCollect info = new UserCollect();
            //创建主键id
            info.setId(IdUtils.simpleUUID());
            //用户id
            info.setUserId(params.getUserId());
            //收藏ID
            info.setCollectId(params.getCollectId());
            //收藏类型 0—活动 1-情缘
            info.setIsType(params.getIsType());
            //创建者信息
            info.setCreateOperatorId(operUserId);
            info.setCreateTime(System.currentTimeMillis());
            boolean result = this.service.save(info);
            if (result) {
                flag = true;
            }
        }
        //取消收藏
        if (params.getIscollect() == 1) {
            //条件删除已经被收藏的数据源
            QueryWrapper<UserCollect> queryWrapper = new QueryWrapper<UserCollect>();
            queryWrapper.eq("create_operator_id", operUserId);
            queryWrapper.eq("collect_id", params.getCollectId());
            boolean result = this.service.remove(queryWrapper);
            if (result) {
                flag = true;
            }
        }

        return flag;
    }
}