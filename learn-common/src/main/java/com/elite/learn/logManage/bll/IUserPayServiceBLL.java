package com.elite.learn.logManage.bll;


import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.logManage.entity.UserPay;
import com.elite.learn.logManage.query.UserPayQuery;
import com.elite.learn.logManage.vo.UserPayVO;

/**
 * @author 花賊
 * @create 2021-01-19 17:11
 */
public interface IUserPayServiceBLL {


    /**
     * 保存支付记录
     *
     * @param params
     */
    void save(UserPay params);


    /**
     * 修改支付记录
     *
     * @param params
     */
    void update(UserPay params);


    /**
     * 获取支付记录
     *
     * @param orderNum
     */
    UserPay getInfo(String orderNum);


    /**
     * <p>
     * 列表
     * </p>
     *
     * @param query
     * @return com.elite.learn.tools.base.common.entity.page.PageResult<com.elite.learn.base.operation.order.common.entity.dto.UserPayDTO>
     * @author: leroy
     * @date 2021/7/15 11:25
     */
    PageResult<UserPayVO> getList(UserPayQuery query);
}
