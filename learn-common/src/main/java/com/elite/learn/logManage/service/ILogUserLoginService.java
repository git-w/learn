package com.elite.learn.logManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.logManage.entity.LogUserLogin;
import com.elite.learn.logManage.vo.LogUserLoginVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 小程序用户登录日志表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
public interface ILogUserLoginService extends IService<LogUserLogin> {


    /**
     * 登录日志分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<LogUserLoginVo> selectLists(Page<LogUserLoginVo> page, @Param(Constants.WRAPPER) Wrapper<LogUserLogin> queryWrapper);

}
