package com.elite.learn.logManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.logManage.entity.UserRefundPay;
import com.elite.learn.logManage.vo.UserRefundPayVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 支付日志记录表---用户退款支付记录表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
public interface UserRefundPayMapper extends BaseMapper<UserRefundPay> {


    /**
     * 自定义分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    List<UserRefundPayVO> getList(Page<UserRefundPayVO> page, @Param(Constants.WRAPPER) Wrapper<UserRefundPay> queryWrapper);

}
