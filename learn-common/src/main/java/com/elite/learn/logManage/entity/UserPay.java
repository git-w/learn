package com.elite.learn.logManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 支付日志记录表---用户支付记录表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_log_user_pay")
public class UserPay implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 关联订单id
     */
    private String orderId;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 支付方式 1.微信扫码2.微信公众号内支付3.微信h5支付4.其他
     */
    private String payType;

    /**
     * 订单类型 0-其他 1-活动 2-班车 3-商城  4-外卖 5-酒店/民宿 6-会员卡  8店铺
     */
    private Integer orderType;

    /**
     * 签名
     */
    private String appSign;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 发生的金额 单位 分
     */
    private Integer payMoney;

    /**
     * 状态 1.未支付2.支付成功
     */
    private Integer isStatus;

    /**
     * 错误原因
     */
    private String reason;

    /**
     * 交易号
     */
    private String payNo;

    /**
     * 支付完成时间
     */
    private String endTime;

    /**
     * 用户对微信公众号的唯一标识
     */
    private String openId;

    /**
     * 商户号
     */
    private String mchId;

    /**
     * appid
     */
    private String appId;

    /**
     * 微信支付url
     */
    private String codeUrl;

    /**
     * 备注
     */
    private String extraParams1;


}
