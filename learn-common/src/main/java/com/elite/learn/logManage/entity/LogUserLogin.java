package com.elite.learn.logManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 小程序用户登录日志表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_log_user_login")
public class LogUserLogin implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "ID", type = IdType.UUID)
    private String id;

    /**
     * 用户id
     */
    @TableField("UserId")
    private String UserId;

    /**
     * 登陆ip
     */
    @TableField("IpAddr")
    private String IpAddr;

    /**
     * ip所在地
     */
    @TableField("IpLocation")
    private String IpLocation;

    /**
     * 创建时间
     */
    @TableField("CreateTime")
    private Long CreateTime;


}
