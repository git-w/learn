package com.elite.learn.logManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.logManage.entity.LogAccountLogin;
import com.elite.learn.logManage.entity.LogUserLogin;
import com.elite.learn.logManage.mapper.LogUserLoginMapper;
import com.elite.learn.logManage.service.ILogUserLoginService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.logManage.vo.LogAccountLoginVo;
import com.elite.learn.logManage.vo.LogUserLoginVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 小程序用户登录日志表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Service
public class LogUserLoginServiceImpl extends ServiceImpl<LogUserLoginMapper, LogUserLogin> implements ILogUserLoginService {


    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<LogUserLoginVo> selectLists(Page<LogUserLoginVo> page, @Param(Constants.WRAPPER) Wrapper<LogUserLogin> queryWrapper) {
        page.setRecords(this.baseMapper.selectLists(page, queryWrapper));
        return page;
    }
}
