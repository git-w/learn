package com.elite.learn.logManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.logManage.entity.UserRefundPay;
import com.elite.learn.logManage.vo.UserRefundPayVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 支付日志记录表---用户退款支付记录表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
public interface IUserRefundPayService extends IService<UserRefundPay> {


    /**
     * 登录日志分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<UserRefundPayVO> getList(Page<UserRefundPayVO> page, @Param(Constants.WRAPPER) Wrapper<UserRefundPay> queryWrapper);

}
