package com.elite.learn.logManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 支付日志记录表---用户退款支付记录表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
@Data
public class UserRefundPayQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 关联订单id
     */
    private String orderId;

    /**
     * 订单号
     */
    private String orderNum;



}
