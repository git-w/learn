package com.elite.learn.logManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.utils.ip.RequestUtil;
import com.elite.learn.common.utils.string.StringUtils;
import com.elite.learn.logManage.bll.ILogUserLoginServiceBLL;
import com.elite.learn.logManage.entity.LogUserLogin;
import com.elite.learn.logManage.query.LogUserLoginQuery;
import com.elite.learn.logManage.service.ILogUserLoginService;
import com.elite.learn.logManage.vo.LogUserLoginVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Service
public class LogUserLoginServiceBLLImpl implements ILogUserLoginServiceBLL {

    @Resource
    private ILogUserLoginService service;
    //请求
    @Resource
    private HttpServletRequest request;


    /**
     * 记录登录日志
     *
     * @param userId 用户id
     */
    @Override
    public void save(String userId) {
        String ip = RequestUtil.getIP(request);
        LogUserLogin bean = new LogUserLogin();
        bean.setUserId(userId);
       // bean.setIpLocation(AddressUtil.getAddress(ip));
        bean.setCreateTime(System.currentTimeMillis());
        bean.setIpAddr(ip);
        this.service.save(bean);
    }


    /**
     * 查看用户登录日志
     * @param params
     * @return
     */
    @Override
    public PageResult<LogUserLoginVo> getList(LogUserLoginQuery params) {
        QueryWrapper<LogUserLogin> queryWrapper = new QueryWrapper<>();
        // 判断是否时间搜索
        if (Objects.nonNull(params.getStartTime())) {
            queryWrapper.ge("lul.CreateTime", params.getStartTime());
        }
        if (Objects.nonNull(params.getEndTime())) {
            queryWrapper.le("lul.CreateTime", params.getEndTime());
        }

        //根据地址模糊查询
        if (StringUtils.isNotEmpty(params.getIpLocation())) {
            queryWrapper.like("lul.IpLocation", params.getIpLocation());
        }
        //用户昵称模糊查询
        if (StringUtils.isNotEmpty(params.getNickname())) {
            queryWrapper.like("iu.nick_name", params.getNickname());
        }
        queryWrapper.eq("iu.is_delete",0);
        //时间
        queryWrapper.orderByDesc("lul.CreateTime");
        Page<LogUserLoginVo> pageInfo = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<LogUserLoginVo> page = this.service.selectLists(pageInfo, queryWrapper);
        return new PageResult<>(page);
    }


}
