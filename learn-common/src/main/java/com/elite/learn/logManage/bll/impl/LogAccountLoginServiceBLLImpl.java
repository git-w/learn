package com.elite.learn.logManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.ip.RequestUtil;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.logManage.bll.ILogAccountLoginServiceBLL;
import com.elite.learn.logManage.entity.LogAccountLogin;
import com.elite.learn.logManage.query.LogAccountLoginQuery;
import com.elite.learn.logManage.service.ILogAccountLoginService;
import com.elite.learn.logManage.vo.LogAccountLoginVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Service
public class LogAccountLoginServiceBLLImpl implements ILogAccountLoginServiceBLL {

    @Resource
    private ILogAccountLoginService service;
    //请求
    @Resource
    private HttpServletRequest request;


    /**
     * 记录登录日志
     *
     * @param AccountId
     * @param Message   登录信息
     * @param IsStatus  登录状态 0-成功 1-失败
     * @param Password  登录密码
     */
    @Override
    public void save(String AccountId, String Message, Integer IsStatus, String Password) {
        LogAccountLogin bean = new LogAccountLogin();
        bean.setAccountId(AccountId);
        bean.setMessage(Message);
        bean.setIsStatus(IsStatus);
        bean.setPassword(Password);
        bean.setLoginTime(System.currentTimeMillis());
        bean.setIpAddr(RequestUtil.getIP(request));
        String UserAgent = request.getHeader("User-Agent");
        if (StringUtil.isNotEmpty(UserAgent)) {
            bean.setBrowserDetails(request.getHeader("User-Agent").toLowerCase());
        }
        this.service.save(bean);
    }


    /**
     * 查看用户登录日志
     *
     * @param params
     * @return
     */
    @Override
    public PageResult<LogAccountLoginVo> getList(LogAccountLoginQuery params) {

        QueryWrapper<LogAccountLogin> queryWrapper = new QueryWrapper<>();
//        //判断是否时间搜索
//        if (params.getEndTime() != null && params.getStartTime() != null) {
//            // wrapper.between("LoginTime", CommonHelper.getDateLong(params.getStartTime()),CommonHelper.getDateLong(params.getEndTime()));
//            queryWrapper.ge("a.LoginTime", CommonHelper.getDateLong(params.getStartTime()));
//            queryWrapper.le("a.LoginTime", CommonHelper.getDateLong(params.getEndTime()));
//        }

        // 判断是否时间搜索
        if (Objects.nonNull(params.getStartTime())){
            queryWrapper.ge("a.LoginTime",params.getStartTime());
        }
        if (Objects.nonNull(params.getEndTime())){
            queryWrapper.le("a.LoginTime",params.getEndTime());
        }

        //账号搜索
        if (StringUtil.isNotEmpty(params.getLoginName())) {
            queryWrapper.like("b.LoginName", "%" + params.getLoginName() + "%");
        }
        //手机搜索
        if (StringUtil.isNotEmpty(params.getPhone())) {
            queryWrapper.like("b.Phone", "%" + params.getPhone() + "%");
        }
        //账号搜索
        if (StringUtil.isNotEmpty(params.getIpAddr())) {
            queryWrapper.like("b.IpAddr", "%" + params.getIpAddr() + "%");
        }
        queryWrapper.eq("b.IsDelete", DeleteType.NOTDELETE.getCode());
        //时间
        queryWrapper.orderByDesc("a.LoginTime");

        Page<LogAccountLoginVo> pageInfo = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<LogAccountLoginVo> page = this.service.selectLists(pageInfo, queryWrapper);
        // 总记录数
        for (LogAccountLoginVo info : page.getRecords()) {
            if (StringUtil.isNotEmpty(info.getPhoto())) {
                info.setPhotoPath(FileUploadUtil.getImgRootPath() + info.getPhoto());
            }
        }
        return new PageResult<>(page);
    }


}
