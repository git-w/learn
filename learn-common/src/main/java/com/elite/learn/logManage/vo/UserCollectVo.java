package com.elite.learn.logManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户收藏表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
@Data
public class UserCollectVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 收藏表 主键ID
     */
    private String id;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 收藏ID
     */
    private String collectId;
    /**
     * 收藏ID
     */
    private String pictures;

    /**
     * 收藏ID
     */
    private String picturesUrlPath;


    /**
     * 收藏类型 0—活动 1-情缘
     */
    private Integer isType;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 用户的昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String headimgurl;
    /**
     * 收藏 0收藏 1未收藏
     */
    private Integer iscollect;



    /**
     * 身高-身高范围：145-250
     */
    private String stature;

    /**
     * az：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
     */
    private Integer educationBackground;

    /**
     * 婚姻状况：0-未婚、1-离异、2-丧偶
     */
    private Integer maritalStatus;

    /**
     * 子女状况：0-无子女、1-有子女
     */
    private Integer childrenStatus;

    /**
     * 用户所在省份名称
     */
    private String provinceName;

    /**
     * 用户所在城市名称
     */
    private String cityName;

    /**
     * 用户所在省份id
     */
    private String provinceId;

    /**
     * 用户所在城市id
     */
    private String cityId;
    /**
     * 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
     */
    private Integer housingStatus;

    /**
     * 购车情况：0-已购车、1-有需要购车、2-其他
     */
    private Integer carStatus;




    /**
     * 生日
     */
    private Long birthday;

    /**
     * 是否已审核：0-是，1-否
     */
    private Integer isAudit;

    /**
     * 用户等级 1-游客（普通用户）2-会员vip
     */
    private Integer level;

    /**
     * 生日
     */
    private Long stratTime;


    /**
     * 生日
     */
    private Long endTime;
    /**
     * 活动名称
     */
    private String name;
    /**
     * 活动简介
     */
    private String msg;
}
