package com.elite.learn.logManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户收藏表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_log_user_collect")
public class UserCollect implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 收藏表 主键ID
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 收藏ID
     */
    private String collectId;
    /**
     * 收藏 0收藏 1未收藏
     */
    private Integer iscollect;

    /**
     * 收藏类型 0—活动 1-情缘
     */
    private Integer isType;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;


}
