package com.elite.learn.logManage.bll;

import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.logManage.query.LogAccountLoginQuery;
import com.elite.learn.logManage.vo.LogAccountLoginVo;

public interface ILogAccountLoginServiceBLL {


    /**
     * 记录登录日志
     *
     * @param AccountId
     * @param Message      登录信息
     * @param IsStatus    登录状态 0-成功 1-失败
     * @param Password   登录密码
     */
    void save(String AccountId,String Message,Integer IsStatus,String Password);


    /**
     * 分页查看 登录日志
     *
     * @param params
     * @return
     */
     PageResult<LogAccountLoginVo> getList(LogAccountLoginQuery params);
}
