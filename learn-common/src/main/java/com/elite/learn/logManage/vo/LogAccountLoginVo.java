package com.elite.learn.logManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统管理-管理员登录日志
 * </p>
 *
 * @author: leroy
 * @since 2021-05-24
 */
@Data
public class LogAccountLoginVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 登录者id
     */
    private String id;
    /**
     * 登录者id
     */
    private String AccountId;

    /**
     * 登陆时间
     */
    private Long LoginTime;

    /**
     * 登陆ip
     */
    private String IpAddr;


    /**
     * 用户登录名
     */
    private String LoginName;

    /**
     * 用户真实姓名
     */
    private String AccountName;

    /**
     * 账号状态 0:正常/启用 1:冻结/禁用
     */
    private Integer IsStatus;

    /**
     * 头像路径 短
     */
    private String Photo;

    /**
     * 员工编号
     */
    private String AccountCode;


    /**
     * 邮箱
     */
    private String Email;

    /**
     * 手机
     */
    private String Phone;


    /**
     * 管理员类型 0:超级管理员 1普通管理员
     */
    private Integer Type;

    /**
     * 头像路径 长
     */
    private String PhotoPath;

    /**
     * 浏览器信息
     */
    private String BrowserDetails;
    /**
     * 登录信息
     */
    private String Message;


}
