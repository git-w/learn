package com.elite.learn.logManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.logManage.entity.UserCollect;
import com.elite.learn.logManage.mapper.UserCollectMapper;
import com.elite.learn.logManage.service.IUserCollectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.logManage.vo.UserCollectActivityVo;
import com.elite.learn.logManage.vo.UserCollectVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户收藏表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
@Service
public class UserCollectServiceImpl extends ServiceImpl<UserCollectMapper, UserCollect> implements IUserCollectService {


    /**
     * 活动 分页
     * @author: leroy
     * @date 2021/12/13 13:23
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<UserCollectVo> getListsActivity(Page<UserCollectVo> pageParams,@Param(Constants.WRAPPER)Wrapper<UserCollect> queryWrapper) {
         pageParams.setRecords(this.baseMapper.getListsActivity(pageParams,queryWrapper));
        return pageParams;
    }


    /**
     * 情缘分页
     * @author: leroy
     * @date 2021/12/13 13:27
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<UserCollectVo> getLists(Page<UserCollectVo> pageParams, @Param(Constants.WRAPPER)Wrapper<UserCollect> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));
        return  pageParams;
    }
}
