package com.elite.learn.logManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.logManage.entity.LogAccountLogin;
import com.elite.learn.logManage.mapper.LogAccountLoginMapper;
import com.elite.learn.logManage.service.ILogAccountLoginService;
import com.elite.learn.logManage.vo.LogAccountLoginVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统管理-管理员登录日志 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-24
 */
@Service
public class LogAccountLoginServiceImpl extends ServiceImpl<LogAccountLoginMapper, LogAccountLogin> implements ILogAccountLoginService {


    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<LogAccountLoginVo> selectLists(Page<LogAccountLoginVo> page, @Param(Constants.WRAPPER) Wrapper<LogAccountLogin> queryWrapper) {
        page.setRecords(this.baseMapper.selectLists(page, queryWrapper));
        return page;
    }
}
