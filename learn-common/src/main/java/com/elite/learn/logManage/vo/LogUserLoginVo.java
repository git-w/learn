package com.elite.learn.logManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 小程序用户登录日志表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Data
public class LogUserLoginVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 用户id
     */
    private String UserId;

    /**
     * 登陆ip
     */
    private String IpAddr;

    /**
     * ip所在地
     */
    private String IpLocation;

    /**
     * 创建时间
     */
    private Long CreateTime;
    /**
     * 用户的昵称
     */
    private String Nickname;

    /**
     * 用户头像
     */
    private String Headimgurl;

}
