package com.elite.learn.logManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.logManage.entity.UserPay;
import com.elite.learn.logManage.mapper.UserPayMapper;
import com.elite.learn.logManage.service.IUserPayService;
import com.elite.learn.logManage.vo.UserPayVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付日志记录表---用户支付记录表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
@Service
public class UserPayServiceImpl extends ServiceImpl<UserPayMapper, UserPay> implements IUserPayService {


    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<UserPayVO> getList(Page<UserPayVO> page, @Param(Constants.WRAPPER) Wrapper<UserPay> queryWrapper) {
        page.setRecords(this.baseMapper.getList(page, queryWrapper));
        return page;
    }
}
