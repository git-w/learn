package com.elite.learn.logManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户收藏表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
@Data
public class UserCollectActivityVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 收藏表 主键ID
     */
    private String id;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 收藏ID
     */
    private String collectId;

    /**
     * 收藏类型 0—活动 1-情缘
     */
    private Integer isType;


    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 活动名称
     */
    private String name;

    /**
     * 活动简介
     */
    private String msg;
    /**
     * 收藏 0收藏 1未收藏
     */
    private Integer iscollect;


}
