package com.elite.learn.logManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.logManage.entity.UserCollect;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.logManage.vo.UserCollectActivityVo;
import com.elite.learn.logManage.vo.UserCollectVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户收藏表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
public interface IUserCollectService extends IService<UserCollect> {


    /**
     * 收藏活动分页
     * @author: leroy
     * @date 2021/12/13 11:28
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    IPage<UserCollectVo> getListsActivity(Page<UserCollectVo> pageParams,@Param(Constants.WRAPPER)Wrapper<UserCollect> queryWrapper);


    /**
     * 收藏情缘分页
     * @author: leroy
     * @date 2021/12/13 11:45
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */

    IPage<UserCollectVo> getLists(Page<UserCollectVo> pageParams,@Param(Constants.WRAPPER)Wrapper<UserCollect> queryWrapper);



}
