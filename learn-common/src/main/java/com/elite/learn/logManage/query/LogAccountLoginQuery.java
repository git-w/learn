package com.elite.learn.logManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统管理-管理员登录日志
 * </p>
 *
 * @author: leroy
 * @since 2021-05-24
 */
@Data
public class LogAccountLoginQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 登录ip
     */
    private String IpAddr;



    /**
     * 用户名
     */
    private String LoginName;

    /**
     * 联系方式
     */
    private String Phone;

    /**
     * 开始时间
     */
    private Long StartTime;

    /**
     * 结束时间
     */
    private Long EndTime;





}
