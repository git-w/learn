package com.elite.learn.logManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 支付日志记录表---用户支付记录表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-13
 */
@Data
public class UserPayQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;


    /**
     * 手机号
     */
    private String modilePhone;
    /**
     * 用户名
     */
    private String nickName;
    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 开始时间
     */
    private Long startTime;

    /**
     * 结束时间
     */
    private Long  endTime;
}
