package com.elite.learn.logManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 小程序用户登录日志表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Data
public class LogUserLoginQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;


    /**
     * 登录ip
     */
    private String IpAddr;



    /**
     * 开始时间
     */
    private Long StartTime;

    /**
     * 结束时间
     */
    private Long EndTime;

    /**
     * ip所在地
     */
    private String IpLocation;

    /**
     * 用户的昵称
     */
    private String Nickname;


}
