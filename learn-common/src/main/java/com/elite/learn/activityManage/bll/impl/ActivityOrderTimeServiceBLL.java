package com.elite.learn.activityManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.bll.IActivityOrderTimeServiceBLL;
import com.elite.learn.activityManage.dto.ActivityOrderTimeDTO;
import com.elite.learn.activityManage.entity.ActivityOrderTime;
import com.elite.learn.activityManage.query.ActivityOrderTimeQuery;
import com.elite.learn.activityManage.service.IActivityOrderTimeService;
import com.elite.learn.activityManage.vo.ActivityOrderTimeVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 活动订单时间区间表模块
 * @Title: ActivityOrderTimeServiceBLL
 * @Package com.elite.learn.activityManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/20 15:45
 */

@Service
public class ActivityOrderTimeServiceBLL implements IActivityOrderTimeServiceBLL {

    @Resource
    private IActivityOrderTimeService service;


    /**
     * 创建对象id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/20 15:47
     * @update
     * @updateTime
     */
    @Override
    public ActivityOrderTime get(String id) {
        return this.service.getById(id);
    }


    /**
     * 详情
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/20 15:48
     * @update
     * @updateTime
     */
    @Override
    public ActivityOrderTimeVO getInfo(String id) {
        return this.service.getInfo(id);
    }


    /**
     * 保存
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/20 15:53
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, ActivityOrderTimeDTO bean) {
        //创建实体对象
        ActivityOrderTime info = new ActivityOrderTime();
        //主键ID
        info.setId(IdUtils.simpleUUID());
        //活动id
        info.setActivityId(bean.getActivityId());
        //活动名称
        info.setActivityName(bean.getActivityName());
        //用户id
        info.setUserId(bean.getUserId());
        //分类id
        info.setClassifyId(bean.getClassifyId());
        //关联订单id
        info.setOrderId(bean.getOrderId());
        //订单号
        info.setOrderNum(bean.getOrderNum());
        //当日价格
        info.setToDayPrice(bean.getToDayPrice());
        //当日时间
        info.setToDayTime(bean.getToDayTime());
        //活动日期
        info.setActivityDate(bean.getActivityDate());
        //参与日期
        info.setJoinDate(bean.getJoinDate());
        //0未使用  1已使用  2已过期
        info.setIsUse(0);

        //删除标识 0:未删除 1:已删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());

        //创建者信息
        info.setCreateOperatorId(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateTime(System.currentTimeMillis());

        //添加数据
        boolean result = this.service.save(info);

        return result ? info.getId() : null;
    }


    /**
     * 更新
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/20 16:07
     * @update
     * @updateTime
     */
    @Override
    public boolean update(String operUserId, ActivityOrderTimeDTO bean) {
        ActivityOrderTime info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        ActivityOrderTime infoNew = new ActivityOrderTime();
        //主键ID
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //活动id
        infoNew.setActivityId(bean.getActivityId());
        //活动名称
        infoNew.setActivityName(bean.getActivityName());
        //用户id
        infoNew.setUserId(bean.getUserId());
        //分类id
        infoNew.setClassifyId(bean.getClassifyId());
        //关联订单id
        infoNew.setOrderId(bean.getOrderId());
        //订单号
        infoNew.setOrderNum(bean.getOrderNum());
        //当日价格
        infoNew.setToDayPrice(bean.getToDayPrice());
        //当日时间
        infoNew.setToDayTime(bean.getToDayTime());
        //活动日期
        infoNew.setActivityDate(bean.getActivityDate());
        //参与日期
        infoNew.setJoinDate(bean.getJoinDate());
        //0未使用  1已使用  2已过期
        infoNew.setIsUse(bean.getIsUse());

        //更新者信息
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        //更新数据
        return this.service.updateById(infoNew);

    }


    /**
     * 删除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2021/12/20 16:42
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        ActivityOrderTime infoDel = null;
        //分割id
        String[] idArr = ids.split(",");
        //遍历id判断是否为空，弹出本次循环
        for (String id : idArr) {
            ActivityOrderTime info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new ActivityOrderTime();
            infoDel.setId(info.getId());
            //设置删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            //删除id
            infoDel.setDeleteOperatorId(operUserId);

            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    @Override
    public List<ActivityOrderTimeVO> findAll(ActivityOrderTimeQuery bean) {
        return null;
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/20 16:43
     * @update
     * @updateTime
     */
    @Override
    public PageResult<ActivityOrderTimeVO> getList(ActivityOrderTimeQuery params) {
        QueryWrapper<ActivityOrderTime> queryWrapper = new QueryWrapper<ActivityOrderTime>();
        queryWrapper.eq("taot.is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("taot.create_time");

        // 根据活动id过滤
        if (StringUtil.notBlank(params.getActivityId())) {
            queryWrapper.eq("taot.activity_id", params.getActivityId());
        }

        // 根据用户id过滤
        if (StringUtil.notBlank(params.getUserId())) {
            queryWrapper.eq("taot.user_id", params.getUserId());
        }

        // 根据分类id过滤
        if (StringUtil.notBlank(params.getClassifyId())) {
            queryWrapper.eq("taot.classify_id", params.getClassifyId());
        }


        // 根据关联订单id过滤
        if (StringUtil.notBlank(params.getOrderId())) {
            queryWrapper.eq("taot.order_id", params.getOrderId());
        }


        // 根据使用状态过滤
        if (Objects.nonNull(params.getIsUse())) {
            queryWrapper.eq("taot.is_use", params.getIsUse());
        }

        Page<ActivityOrderTimeVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<ActivityOrderTimeVO> page = this.service.getLists(PageParams, queryWrapper);
        return new PageResult<>(page);

    }
}