package com.elite.learn.activityManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 活动分类表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
@Data
public class  ActivityCateQuery  extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 分类名称
     */
    private String cateName;


    /**
     * 是否上线 0是 1否
     */
    private Integer isState;





    /**
     * 创建时间
     */
    private Long createTime;





}
