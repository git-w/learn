package com.elite.learn.activityManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.ActivityPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.activityManage.vo.ActivityPriceVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动时间价格区间表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-28
 */
public interface ActivityPriceMapper extends BaseMapper<ActivityPrice> {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/21 9:13
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityPriceVO getInfo(String id);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/21 9:14
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<ActivityPriceVO> getLists(Page<ActivityPriceVO> pageParams, @Param(Constants.WRAPPER)Wrapper<ActivityPriceVO> queryWrapper);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/21 9:14
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<ActivityPriceVO> findAll(@Param(Constants.WRAPPER) Wrapper<ActivityPriceVO> queryWrapper);
}
