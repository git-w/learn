package com.elite.learn.activityManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.ActivityOrderTime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.activityManage.vo.ActivityOrderTimeVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动订单时间区间表模块 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
public interface ActivityOrderTimeMapper extends BaseMapper<ActivityOrderTime> {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/20 16:50
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityOrderTimeVO getInfo(String id);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/20 16:51
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<ActivityOrderTimeVO> getLists(Page<ActivityOrderTimeVO> pageParams, @Param(Constants.WRAPPER)  Wrapper<ActivityOrderTime> queryWrapper);
}
