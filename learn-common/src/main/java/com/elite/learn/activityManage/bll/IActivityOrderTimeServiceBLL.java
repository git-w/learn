package com.elite.learn.activityManage.bll;

import com.elite.learn.activityManage.dto.ActivityOrderTimeDTO;
import com.elite.learn.activityManage.entity.ActivityOrderTime;
import com.elite.learn.activityManage.query.ActivityOrderTimeQuery;
import com.elite.learn.activityManage.vo.ActivityOrderTimeVO;
import com.elite.learn.common.core.service.ICommonServiceBLL;

public interface IActivityOrderTimeServiceBLL extends ICommonServiceBLL<ActivityOrderTime, ActivityOrderTimeDTO, ActivityOrderTimeVO, ActivityOrderTimeQuery> {
}
