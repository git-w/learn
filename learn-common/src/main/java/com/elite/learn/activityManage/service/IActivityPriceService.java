package com.elite.learn.activityManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.ActivityPrice;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.activityManage.vo.ActivityPriceVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动时间价格区间表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-28
 */
public interface IActivityPriceService extends IService<ActivityPrice> {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/20 18:14
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityPriceVO getInfo(String id);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/21 9:06
     * @return null
     * @update
     * @updateTime
     */
    List<ActivityPriceVO> findAll(@Param(Constants.WRAPPER) Wrapper<ActivityPriceVO> queryWrapper);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/21 9:06
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    IPage<ActivityPriceVO> getLists(Page<ActivityPriceVO> pageParams, @Param(Constants.WRAPPER) Wrapper<ActivityPriceVO> queryWrapper);
}
