package com.elite.learn.activityManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.bll.IActivityOrderUserServiceBLL;
import com.elite.learn.activityManage.dto.ActivityOrderUserDTO;
import com.elite.learn.activityManage.entity.ActivityOrderUser;
import com.elite.learn.activityManage.query.ActivityOrderUserQuery;
import com.elite.learn.activityManage.service.IActivityOrderUserService;
import com.elite.learn.activityManage.vo.ActivityOrderUserVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 活动用户中心表
 * @Title: ActivityOrderUserServiceBLL
 * @Package com.elite.learn.activityManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/20 17:01
 */

@Service
public class ActivityOrderUserServiceBLL implements IActivityOrderUserServiceBLL {


    @Resource
    private IActivityOrderUserService service;


    /**
     * 主键id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/20 17:04
     * @update
     * @updateTime
     */
    @Override
    public ActivityOrderUser get(String id) {
        return this.service.getById(id);
    }


    /**
     * 详情
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/20 17:04
     * @update
     * @updateTime
     */
    @Override
    public ActivityOrderUserVO getInfo(String id) {
        return this.service.getInfo(id);
    }


    /**
     * 保存
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/20 17:02
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, ActivityOrderUserDTO bean) {
        //创建实体对象
        ActivityOrderUser info = new ActivityOrderUser();
        //主键ID
        info.setId(IdUtils.simpleUUID());
        //活动ID
        info.setActivityId(bean.getActivityId());
        //用户ID
        info.setUserId(bean.getUserId());
        //订单ID
        info.setOrderId(bean.getOrderId());
        //是否签到 0未签到 1已签到
        info.setIsState(0);
        //签到时间
        info.setSignInTime(bean.getSignInTime());
        //名字
        info.setUserName(bean.getUserName());
        //身份证号
        info.setPinCodes(bean.getPinCodes());
        //手机号
        info.setMobilePhone(bean.getMobilePhone());
        //衣服尺码
        info.setClothesSize(bean.getClothesSize());
        //性别
        info.setSex(bean.getSex());
        //年龄
        info.setAge(bean.getAge());
        //问题1的答案
        info.setAnswerOne(bean.getAnswerOne());
        //问题2的答案
        info.setAnswerTwo(bean.getAnswerTwo());
        //问题3的答案
        info.setAnswerThree(bean.getAnswerThree());
        //问题4的答案
        info.setAnswerFour(bean.getAnswerFour());
        //问题5的答案
        info.setAnswerFive(bean.getAnswerFive());
        //问题6的答案
        info.setAnswerSix(bean.getAnswerSix());


        //删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());

        //创建者信息
        info.setCreateOperatorId(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateTime(System.currentTimeMillis());

        //添加数据
        boolean result = this.service.save(info);
        return result ? info.getId() : null;
    }


    /**
     * 更新
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/21 16:52
     * @update
     * @updateTime
     */
    @Override
    public boolean update(String operUserId, ActivityOrderUserDTO bean) {
        ActivityOrderUser info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        ActivityOrderUser infoNew = new ActivityOrderUser();
        //主键ID
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //活动ID
        if (StringUtil.notBlank(bean.getActivityId())) {
            infoNew.setActivityId(bean.getActivityId());
        }
        //用户ID
        if (StringUtil.notBlank(bean.getUserId())) {
            infoNew.setUserId(bean.getUserId());
        }
        //订单ID
        if (StringUtil.notBlank(bean.getOrderId())) {
            infoNew.setOrderId(bean.getOrderId());
        }
        //是否签到 0未签到 1已签到
        if (Objects.nonNull(bean.getIsState())) {
            infoNew.setIsState(bean.getIsState());
        }
        //签到时间
        if (Objects.nonNull(bean.getSignInTime())) {
            infoNew.setSignInTime(bean.getSignInTime());
        }
        //名字
        if (StringUtil.notBlank(bean.getUserName())) {
            infoNew.setUserName(bean.getUserName());
        }
        //身份证号
        if (StringUtil.notBlank(bean.getPinCodes())) {
            infoNew.setPinCodes(bean.getPinCodes());
        }
        //手机号
        if (StringUtil.notBlank(bean.getMobilePhone())) {
            infoNew.setMobilePhone(bean.getMobilePhone());
        }
        //衣服尺码
        if (StringUtil.notBlank(bean.getClothesSize())) {
            infoNew.setClothesSize(bean.getClothesSize());
        }
        //性别
        if (Objects.nonNull(bean.getSex())) {
            infoNew.setSex(bean.getSex());
        }
        //年龄
        if (Objects.nonNull(bean.getAge())) {
            infoNew.setAge(bean.getAge());
        }
        //问题1的答案
        infoNew.setAnswerOne(bean.getAnswerOne());
        //问题2的答案
        infoNew.setAnswerTwo(bean.getAnswerTwo());
        //问题3的答案
        infoNew.setAnswerThree(bean.getAnswerThree());
        //问题4的答案
        infoNew.setAnswerFour(bean.getAnswerFour());
        //问题5的答案
        infoNew.setAnswerFive(bean.getAnswerFive());
        //问题6的答案
        infoNew.setAnswerSix(bean.getAnswerSix());

        //更新者信息
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        //更新数据
        return this.service.updateById(infoNew);
    }


    /**
     * 删除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2021/12/20 17:39
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        ActivityOrderUser infoDel = null;
        //分割id
        String[] idArr = ids.split(",");
        //遍历id判断是否为空，弹出本次循环
        for (String id : idArr) {
            ActivityOrderUser info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new ActivityOrderUser();
            infoDel.setId(info.getId());
            //设置删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            //删除id
            infoDel.setDeleteOperatorId(operUserId);

            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    @Override
    public List<ActivityOrderUserVO> findAll(ActivityOrderUserQuery bean) {
        return null;
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/22 10:48
     * @update
     * @updateTime
     */
    @Override
    public PageResult<ActivityOrderUserVO> getList(ActivityOrderUserQuery params) {
        QueryWrapper<ActivityOrderUser> queryWrapper = new QueryWrapper<ActivityOrderUser>();
        queryWrapper.eq("taou.is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("taou.create_time");
        // 根据活动id过滤
        if (StringUtil.notBlank(params.getActivityId())) {
            queryWrapper.eq("taou.activity_id", params.getActivityId());
        }
        // 根据用户ID过滤
        if (StringUtil.notBlank(params.getUserId())) {
            queryWrapper.eq("taou.user_id", params.getUserId());
        }
        // 根据订单ID过滤
        if (StringUtil.notBlank(params.getOrderId())) {
            queryWrapper.eq("taou.order_id", params.getOrderId());
        }
        Page<ActivityOrderUserVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<ActivityOrderUserVO> page = this.service.getLists(PageParams, queryWrapper);
        return new PageResult<>(page);

    }
}