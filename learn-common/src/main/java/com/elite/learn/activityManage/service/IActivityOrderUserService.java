package com.elite.learn.activityManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.ActivityOrderUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.activityManage.vo.ActivityOrderUserVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 活动用户中间表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
public interface IActivityOrderUserService extends IService<ActivityOrderUser> {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/20 17:05
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityOrderUserVO getInfo(String id);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/20 17:43
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    IPage<ActivityOrderUserVO> getLists(Page<ActivityOrderUserVO> pageParams, @Param(Constants.WRAPPER) Wrapper<ActivityOrderUser> queryWrapper);
}
