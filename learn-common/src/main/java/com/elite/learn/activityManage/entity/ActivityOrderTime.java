package com.elite.learn.activityManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 活动订单时间区间表模块
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_activity_order_time")
public class ActivityOrderTime implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单时间区间表 主键ID
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 活动id
     */
    private String activityId;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 分类id
     */
    private String classifyId;

    /**
     * 关联订单id
     */
    private String orderId;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 当日价格
     */
    private String toDayPrice;

    /**
     * 当日时间
     */
    private String toDayTime;

    /**
     * 活动日期
     */
    private Date activityDate;

    /**
     * 参与日期
     */
    private Date joinDate;

    /**
     * 0未使用  1已使用  2已过期
     */
    private Integer isUse;

    /**
     * 删除标识 0:未删除 1:已删除
     */
    private Integer isDelete;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;

    /**
     * 备用字段1
     */
    private String extraParams1;

    /**
     * 备用字段2
     */
    private String extraParams2;

    /**
     * 备用字段3
     */
    private String extraParams3;

    /**
     * 备用字段4
     */
    private String extraParams4;


}
