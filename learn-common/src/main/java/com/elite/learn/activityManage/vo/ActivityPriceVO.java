package com.elite.learn.activityManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 活动时间价格区间表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
public class ActivityPriceVO implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 活动时间价格区间表 主键ID
     */
    private String id;

    /**
     * 活动id
     */
    private String activityId;

    /**
     * 1在线 2下架
     */
    private Integer isState;

    /**
     * 开始时间
     */
    private Long startTime;

    /**
     * 结束时间
     */
    private Long endTime;

    /**
     * 会员折扣
     */
    private BigDecimal vipDiscount;

    /**
     * 会员价格
     */
    private BigDecimal vipPrice;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 访客价格
     */
    private String visitorsPrice;

    /**
     * 业主库存
     */
    private String ownerInventory;

    /**
     * 访客库存
     */
    private String visitorsInventory;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;



    /**
     * 备用字段1
     */
    private String extraParams1;

    /**
     * 备用字段2
     */
    private String extraParams2;

    /**
     * 备用字段3
     */
    private String extraParams3;

    /**
     * 备用字段4
     */
    private String extraParams4;

}
