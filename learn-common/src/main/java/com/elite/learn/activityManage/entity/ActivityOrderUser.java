package com.elite.learn.activityManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 活动用户中间表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_activity_order_user")
public class ActivityOrderUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动用户中间表 线下管理 主键ID
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 活动ID
     */
    private String activityId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 是否签到 0未签到 1已签到
     */
    private Integer isState;

    /**
     * 签到时间
     */
    private Long signInTime;

    /**
     * 名字
     */
    private String userName;

    /**
     * 身份证号
     */
    private String pinCodes;

    /**
     * 手机号
     */
    private String mobilePhone;

    /**
     * 衣服尺码
     */
    private String clothesSize;

    /**
     * 1男  2女
     */
    private Integer sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 问题1的答案
     */
    private String answerOne;

    /**
     * 问题2的答案
     */
    private String answerTwo;

    /**
     * 问题3的答案
     */
    private String answerThree;

    /**
     * 问题4的答案
     */
    private String answerFour;

    /**
     * 问题5的答案
     */
    private String answerFive;

    /**
     * 问题6的答案
     */
    private String answerSix;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建者购买时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 删除标识 0:未删除 1:删除
     */
    private Integer isDelete;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;

    /**
     * 备用字段1
     */
    private String extraParams1;

    /**
     * 备用字段2
     */
    private String extraParams2;

    /**
     * 备用字段3
     */
    private String extraParams3;

    /**
     * 备用字段4
     */
    private String extraParams4;


}
