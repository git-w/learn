package com.elite.learn.activityManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.vo.ActivityVO;
import com.elite.learn.activityManage.entity.Activity;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 订单表---活动表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-08
 */
public interface IActivityService extends IService<Activity> {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/8 16:05
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     */
    IPage<ActivityVO> getLists(Page<ActivityVO> pageParams, @Param(Constants.WRAPPER) Wrapper<Activity> queryWrapper);


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/8 16:06
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityVO getInfo(String id);
}
