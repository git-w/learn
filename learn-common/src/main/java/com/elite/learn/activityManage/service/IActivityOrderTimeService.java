package com.elite.learn.activityManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.ActivityOrderTime;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.activityManage.vo.ActivityOrderTimeVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 活动订单时间区间表模块 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
public interface IActivityOrderTimeService extends IService<ActivityOrderTime> {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/20 15:49
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityOrderTimeVO getInfo(String id);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/20 16:45
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    IPage<ActivityOrderTimeVO> getLists(Page<ActivityOrderTimeVO> pageParams, @Param(Constants.WRAPPER) Wrapper<ActivityOrderTime> queryWrapper);
}
