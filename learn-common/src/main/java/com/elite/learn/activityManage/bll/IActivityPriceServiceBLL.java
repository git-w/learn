package com.elite.learn.activityManage.bll;

import com.elite.learn.activityManage.dto.ActivityPriceDTO;
import com.elite.learn.activityManage.entity.ActivityPrice;
import com.elite.learn.activityManage.query.ActivityPriceQuery;
import com.elite.learn.activityManage.vo.ActivityPriceVO;
import com.elite.learn.common.core.service.ICommonServiceBLL;

import java.util.List;

public interface IActivityPriceServiceBLL extends ICommonServiceBLL<ActivityPrice, ActivityPriceDTO, ActivityPriceVO, ActivityPriceQuery> {


    /**
     * 添加 活动库存
     * @param operUserId
     * @param params
     * @return
     */
    boolean saveBatch(String operUserId,List<ActivityPriceDTO> params);

}
