package com.elite.learn.activityManage.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 活动时间价格区间表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
public class ActivityPriceDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动时间价格区间表 主键ID
     */
    @NotBlank(groups = Update.class, message = "id参数不能为空")
    private String id;

    /**
     * 活动id
     */
    @NotBlank(message = "活动id，参数不能为空")
    private String activityId;

    /**
     * 开始时间
     */
    @NotNull(message = "开始时间，参数不能为空")
    private Long startTime;

    /**
     * 结束时间
     */
    @NotNull(message = "结束时间，参数不能为空")
    private Long endTime;


    /**
     * 会员折扣
     */
    private BigDecimal vipDiscount;

    /**
     * 会员价格
     */
    private BigDecimal vipPrice;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 库存
     */
    private Integer stock;




    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;


    /**
     * 备用字段1
     */
    private String extraParams1;

    /**
     * 备用字段2
     */
    private String extraParams2;

    /**
     * 备用字段3
     */
    private String extraParams3;

    /**
     * 备用字段4
     */
    private String extraParams4;


}
