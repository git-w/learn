package com.elite.learn.activityManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.vo.ActivityCateVO;
import com.elite.learn.activityManage.entity.ActivityCate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动分类表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
public interface ActivityCateMapper extends BaseMapper<ActivityCate> {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/7 15:14
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityCateVO getInfo(String id);


    /**
     *查詢所有
     * @author: leroy
     * @date 2021/12/7 15:15
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<ActivityCateVO> findAll(@Param(Constants.WRAPPER) Wrapper<ActivityCate> queryWrapper);


    /**
     *
     * @author: leroy
     * @date 2021/12/7 15:15
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<ActivityCateVO> getLists(Page<ActivityCateVO> pageParams, @Param(Constants.WRAPPER) Wrapper<ActivityCate> queryWrapper);
}
