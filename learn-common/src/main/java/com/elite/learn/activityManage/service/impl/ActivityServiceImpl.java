package com.elite.learn.activityManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.vo.ActivityVO;
import com.elite.learn.activityManage.entity.Activity;
import com.elite.learn.activityManage.mapper.ActivityMapper;
import com.elite.learn.activityManage.service.IActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表---活动表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-08
 */
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity> implements IActivityService {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/8 16:07
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<ActivityVO> getLists(Page<ActivityVO> pageParams, @Param(Constants.WRAPPER) Wrapper<Activity> queryWrapper) {
         pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));
        return  pageParams;
    }


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/8 16:09
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public ActivityVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }
}
