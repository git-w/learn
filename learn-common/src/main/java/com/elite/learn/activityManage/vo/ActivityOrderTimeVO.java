package com.elite.learn.activityManage.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 活动订单时间区间表模块
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
public class ActivityOrderTimeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单时间区间表 主键ID
     */

    private String id;

    /**
     * 活动id
     */
    private String activityId;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 分类id
     */
    private String classifyId;

    /**
     * 关联订单id
     */
    private String orderId;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 当日价格
     */
    private String toDayPrice;

    /**
     * 当日时间
     */
    private String toDayTime;

    /**
     * 活动日期
     */
    private Date activityDate;

    /**
     * 参与日期
     */
    private Date joinDate;

    /**
     * 0未使用  1已使用  2已过期
     */
    private Integer isUse;


    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;


    /**
     * 订单号
     */
    private String ordern;

    /**
     * 付款时间
     */
    private Long payTime;


    /**
     * 活动付款金额
     */
    private BigDecimal actCost;


    /**
     * 实际付款金额
     */
    private BigDecimal realCost;

    /**
     * 退款发起时间
     */
    private Long refundStartTime;

    /**
     * 退款成功时间
     */
    private Long refundEndTime;


    /**
     * 用户的昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String headimgurl;



}
