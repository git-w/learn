package com.elite.learn.activityManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 活动售卖订单表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
public class OrderActivityQuery  extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动售卖订单表主键ID
     */
    private String id;

    /**
     * 活动ID
     */
    private String activityId;

    /**
     * 付款用户ID
     */
    private String payOperatorId;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 优惠类别  对应会员卡
     */
    private String discountType;

    /**
     * 支付方式 0微信 1会员卡
     */
    private Integer paymentMethod;

    /**
     * 是否支付成功 0未成功 1已成功
     */
    private Integer payState;


    /**
     * 订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款  7已退款
     */
    private Integer isOrderStatus;


    /**
     * 创建者ID
     */
    private String createOperatorId;

    private Integer refundOrderStatus;
}
