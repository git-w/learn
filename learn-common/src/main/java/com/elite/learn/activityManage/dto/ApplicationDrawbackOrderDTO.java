package com.elite.learn.activityManage.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Title :ConfirmOrderDTO.java
 * @Package: com.elite.learn.activityManage.dto
 * @Description:
 * @author: leroy
 * @data: 2021/12/29 16:45
 */
@Data
public class ApplicationDrawbackOrderDTO {
    /**
     * 活动售卖订单表主键ID
     */
    @NotBlank(message = "id参数不能为空")
    private String id;

    /**
     * 退款备注
     */
    private String refundToDescribe;
}