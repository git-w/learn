package com.elite.learn.activityManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 活动用户中间表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
public class ActivityOrderUserVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动用户中间表 线下管理 主键ID
     */
    private String id;

    /**
     * 活动ID
     */
    private String activityId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 是否签到 0未签到 1已签到
     */

    private Integer isState;

    /**
     * 签到时间
     */
    private Long signInTime;

    /**
     * 名字
     */
    private String userName;

    /**
     * 身份证号
     */
    private String pinCodes;

    /**
     * 手机号
     */
    private String mobilePhone;

    /**
     * 衣服尺码
     */
    private String clothesSize;

    /**
     * 1男  2女
     */
    private Integer sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 问题1的答案
     */
    private String answerOne;

    /**
     * 问题2的答案
     */
    private String answerTwo;

    /**
     * 问题3的答案
     */
    private String answerThree;

    /**
     * 问题4的答案
     */
    private String answerFour;

    /**
     * 问题5的答案
     */
    private String answerFive;

    /**
     * 问题6的答案
     */
    private String answerSix;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建者购买时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;


    /**
     * 活动ID
     */
    private String aid;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 付款时间
     */
    private Long payTime;

    /**
     * 活动付款金额
     */
    private BigDecimal actCost;

    /**
     * 实际付款金额
     */
    private BigDecimal realCost;

    /**
     * 优惠类别  对应会员卡
     */
    private String discountType;

    /**
     * 会员卡id
     */
    private String cardId;

    /**
     * 会员卡号
     */
    private String cardName;

    /**
     * 折扣id
     */
    private String discountId;


    /**
     * 折扣
     */
    private String discountName;

    /**
     * 支付方式 0微信 1会员卡
     */
    private Integer paymentMethod;

    /**
     * 是否支付成功 0未成功 1已成功
     */
    private Integer payState;

    /**
     * 退款发起时间
     */
    private Long refundStartTime;

    /**
     * 退款成功时间
     */
    private Long refundEndTime;

    /**
     * 活动人数
     */
    private Integer activityPeopleNumber;

    /**
     * 订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款  7已退款
     */
    private Integer isOrderStatus;

    /**
     * 退款描述
     */
    private String refundToDescribe;

    /**
     * 备注
     */
    private String memo;

    /**
     * 下单活动日期的拼接
     */
    private String  orderActivityDate;

    /**
     * 签到时间
     */
    private String sit;

    /**
     * 0未签到  1签到
     */
    private String isSignIn;

    /**
     * 类型  2业主  1访客
     */
    private String isUserType;


    /**
     * 用户的昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String headimgurl;
}
