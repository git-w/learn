package com.elite.learn.activityManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.OrderActivity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.activityManage.vo.OrderActivityVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动售卖订单表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
public interface OrderActivityMapper extends BaseMapper<OrderActivity> {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/21 10:19
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    OrderActivityVO getInfo(String id);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/21 10:19
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<OrderActivityVO> getLists(Page<OrderActivityVO> pageParams, @Param(Constants.WRAPPER) Wrapper<OrderActivity> queryWrapper);
}
