package com.elite.learn.activityManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 订单表---活动表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-08
 */
@Data
public class ActivityQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动主键ID
     */
    private String id;

    /**
     * 活动名称
     */
    private String name;



    /**
     * 活动地址
     */
    private String address;



    /**
     * 活动分类
     */
    private String categoryId;
    /**
     * 状态 0在线 1下线
     */
    private Integer isState;





}
