package com.elite.learn.activityManage.bll;

import com.elite.learn.activityManage.vo.ActivityVO;
import com.elite.learn.activityManage.dto.ActivityDTO;
import com.elite.learn.activityManage.entity.Activity;
import com.elite.learn.activityManage.query.ActivityQuery;
import com.elite.learn.common.core.service.ICommonServiceBLL;

public interface IActivityServiceBLL extends ICommonServiceBLL<Activity, ActivityDTO, ActivityVO, ActivityQuery> {
}
