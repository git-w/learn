package com.elite.learn.activityManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.bll.IOrderActivityServiceBLL;
import com.elite.learn.activityManage.dto.OrderActivityDTO;
import com.elite.learn.activityManage.entity.Activity;
import com.elite.learn.activityManage.entity.ActivityPrice;
import com.elite.learn.activityManage.entity.OrderActivity;
import com.elite.learn.activityManage.query.OrderActivityQuery;
import com.elite.learn.activityManage.service.IActivityPriceService;
import com.elite.learn.activityManage.service.IActivityService;
import com.elite.learn.activityManage.service.IOrderActivityService;
import com.elite.learn.activityManage.vo.OrderActivityVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import com.elite.learn.common.core.type.OrderType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.generate.order.GetOrderCodeUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.logManage.bll.IUserRefundPayServiceBLL;
import com.elite.learn.userManage.entity.User;
import com.elite.learn.userManage.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 活动售卖订单表
 * @Title: OrderActivityServiceBLL
 * @Package com.elite.learn.activityManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/21 9:33
 */
@Service
public class OrderActivityServiceBLL implements IOrderActivityServiceBLL {
    //活动售卖订单表
    @Resource
    private IOrderActivityService service;
    // 活动表
    @Resource
    private IActivityService activityService;
    // 活动时间价格区间表
    @Resource
    private IActivityPriceService activityPriceService;
    //订单退款记录表
    @Resource
    private IUserRefundPayServiceBLL userRefundPayServiceBLL;
    //用户
    @Resource
    private IUserService userService;


    /**
     * 主键id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/21 9:34
     * @update
     * @updateTime
     */
    @Override
    public OrderActivity get(String id) {
        return this.service.getById(id);
    }


    /**
     * 详情
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/21 9:35
     * @update
     * @updateTime
     */
    @Override
    public OrderActivityVO getInfo(String id) {
        OrderActivityVO vo = this.service.getInfo(id);
        //有查询结果
        if (Objects.nonNull(vo)) {
            if (StringUtil.notBlank(vo.getPictures())) {
                //拼接主图长地址
                vo.setPicturesUrlPath(FileUploadUtil.getImgRootPath() + vo.getPictures());
            }
        }
        return vo;
    }


    /**
     * 保存
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/21 9:36
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, OrderActivityDTO bean) {
        //创建对象
        OrderActivity info = new OrderActivity();
        //对象生成的id
        info.setId(IdUtils.simpleUUID());
        //订单号
        String orderNum = GetOrderCodeUtil.getOrderCodeByStr("HD");
        //订单号
        info.setOrderNum(orderNum);
        //活动ID
        info.setActivityId(bean.getActivityId());
        //付款用户ID
        info.setPayOperatorId(bean.getPayOperatorId());
        //活动库存ID
        info.setActivityPriceId(bean.getActivityPriceId());
        //订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款  7已退款
        info.setIsOrderStatus(OrderType.UNPAY.getType());

        Activity activityInfo = this.activityService.getById(bean.getActivityId());
        //如果活动为空的话
        if (Objects.isNull(activityInfo)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        // 如果是免费的话
        if (activityInfo.getIsFree() == 1) {
            //减去库存
            ActivityPrice price = this.activityPriceService.getById(bean.getActivityPriceId());
            if (price.getStock() <= 0) {
            /*    throw new CommonException(BasePojectExceptionCodeEnum.UnderStockFail.code,
                        BasePojectExceptionCodeEnum.UnderStockFail.msg);*/
            }
            User user = this.userService.getById(operUserId);
            //用户等级 1-游客（普通用户）2-会员vip
            if (user.getLevel().equals(0)) {
                //活动付款金额
                info.setActCost(price.getPrice());
                //实际付款金额
                info.setRealCost(price.getPrice());
            } else if (user.getLevel().equals(1)) {
                //活动付款金额
                info.setActCost(price.getVipPrice());
                //实际付款金额
                info.setRealCost(price.getVipPrice());
            }
            //支付方式 0微信 1会员卡 2免费
            info.setPaymentMethod(OrderType.WeiXin.getType());

        } else {
            //活动付款金额
            info.setActCost(new BigDecimal(0.00));
            //实际付款金额
            info.setRealCost(new BigDecimal(0.00));
            //支付方式 0微信 0微信 2免费
            info.setPaymentMethod(OrderType.Mianfei.getType());
        }


        //活动人数
        info.setActivityPeopleNumber(bean.getActivityPeopleNumber());

        //备注
        info.setMemo(bean.getMemo());
        //下单活动日期的拼接
        info.setOrderActivityDate(bean.getOrderActivityDate());

        // 0未签到  1签到
        info.setIsSignIn(0);
        //类型  2业主  1访客
        info.setIsUserType(bean.getIsUserType());

        // 删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //创建者信息
        info.setCreateOperatorId(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateTime(System.currentTimeMillis());

        //添加数据
        boolean data = this.service.save(info);
        if (activityInfo.getIsFree() == 0) {
            this.updatePaySuccessOrderStatus(operUserId, orderNum, 2);
        }
        // 判断是否添加成功
        return data ? info.getId() + "," + orderNum : null;
    }


    /**
     * 更新
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/21 9:53
     * @update
     * @updateTime
     */
    @Override
    public boolean update(String operUserId, OrderActivityDTO bean) {
        //查询数据库是否有此数据
        OrderActivity info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建对象
        OrderActivity infoNew = new OrderActivity();
        //对象生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //活动ID
        if (StringUtil.notBlank(bean.getActivityId())) {
            infoNew.setActivityId(bean.getActivityId());
        }
        //付款用户ID
        if (StringUtil.notBlank(bean.getPayOperatorId())) {
            infoNew.setPayOperatorId(bean.getPayOperatorId());
        }
        if (StringUtil.notBlank(bean.getActivityPriceId())) {
            infoNew.setActivityPriceId(bean.getActivityPriceId());
        }

        //订单号
        infoNew.setOrderNum(bean.getOrderNum());
        //付款时间
        infoNew.setPayTime(bean.getPayTime());
        //活动付款金额
        infoNew.setActCost(bean.getActCost());
        //实际付款金额
        infoNew.setRealCost(bean.getRealCost());
        //优惠类别  对应会员卡
        infoNew.setDiscountType(bean.getDiscountType());
        //会员卡id
        infoNew.setCardId(bean.getCardId());
        //会员卡号
        infoNew.setCardName(bean.getCardName());
        //折扣id
        infoNew.setDiscountId(bean.getDiscountId());
        //折扣
        infoNew.setDiscountName(bean.getDiscountName());
        //支付方式 0微信  1会员卡
        infoNew.setPaymentMethod(bean.getPaymentMethod());
        // 是否支付成功 0未成功 1已成功
        infoNew.setPayState(bean.getPayState());
        //退款发起时间
        infoNew.setRefundStartTime(bean.getRefundStartTime());
        //退款成功时间
        infoNew.setRefundEndTime(bean.getRefundEndTime());
        //活动人数
        infoNew.setActivityPeopleNumber(bean.getActivityPeopleNumber());
        //订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款  7已退款
        infoNew.setIsOrderStatus(bean.getIsOrderStatus());
        //退款描述
        infoNew.setRefundToDescribe(bean.getRefundToDescribe());
        //备注
        infoNew.setMemo(bean.getMemo());
        //下单活动日期的拼接
        infoNew.setOrderActivityDate(bean.getOrderActivityDate());

        //签到时间
        infoNew.setSignInTime(bean.getSignInTime());

        // 0未签到  1签到
        infoNew.setIsSignIn(bean.getIsSignIn());

        //类型  2业主  1访客
        infoNew.setIsUserType(bean.getIsUserType());


        //更新者数据
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        //判断数据是否更新成功
        return this.service.updateById(infoNew);
    }


    /**
     * 删除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2021/12/21 10:03
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        OrderActivity infoDel = null;
        //分割id
        String[] idArr = ids.split(",");
        //遍历id判断是否为空，弹出本次循环
        for (String id : idArr) {
            OrderActivity info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new OrderActivity();
            infoDel.setId(info.getId());
            //设置删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            //删除id
            infoDel.setDeleteOperatorId(operUserId);

            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    @Override
    public List<OrderActivityVO> findAll(OrderActivityQuery bean) {
        return null;
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/22 10:16
     * @update
     * @updateTime
     */
    @Override
    public PageResult<OrderActivityVO> getList(OrderActivityQuery params) {
        QueryWrapper<OrderActivity> queryWrapper = new QueryWrapper<OrderActivity>();
        queryWrapper.eq("toa.is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("toa.create_time");

        // 根据活动ID过滤
        if (StringUtil.notBlank(params.getActivityId())) {
            queryWrapper.eq("toa.activity_id", params.getActivityId());
        }
        // 根据活动ID过滤
        if (StringUtil.notBlank(params.getCreateOperatorId())) {
            queryWrapper.eq("toa.create_operator_id", params.getCreateOperatorId());
        }
        // 根据订单号过滤
        if (StringUtil.notBlank(params.getOrderNum())) {
            queryWrapper.eq("toa.order_num", params.getOrderNum());
        }
        // 根据订单状态过滤
        if (Objects.nonNull(params.getIsOrderStatus())) {
            queryWrapper.eq("toa.is_order_status", params.getIsOrderStatus());
        }

        Page<OrderActivityVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<OrderActivityVO> page = this.service.getLists(PageParams, queryWrapper);
        //有查询结果
        if (null != page.getRecords() && page.getRecords().size() > 0) {
            for (OrderActivityVO info : page.getRecords()) {
                if (StringUtil.notBlank(info.getPictures())) {
                    //拼接主图长地址
                    info.setPicturesUrlPath(FileUploadUtil.getImgRootPath() + info.getPictures());
                }
            }
        }
        return new PageResult<>(page);
    }


    /**
     * <p>
     * 支付前检查库存等
     * </p>
     *
     * @param operUserId
     * @param orderNum
     * @return java.math.BigDecimal
     * @author Winder
     * @date 2021/8/19 下午3:01
     */
    @Override
    public BigDecimal getCost(String operUserId, String orderNum) {
        //订单号
        QueryWrapper<OrderActivity> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq("order_num", orderNum);
        OrderActivity one = this.service.getOne(productQueryWrapper);
        if (Objects.isNull(one)) {
            return null;
        }
        return one.getRealCost();
    }


    /**
     * <p>
     * 订单支付成功减去库存
     * 订单状态 0-取消订单 1-未支付/待支付 2-已支付/待确认使用、3-已核销/已使用/完成
     * * 4-申请退款 5-退款中， 6-退款完成 7-超时未支付取消的已关闭
     * </p>
     *
     * @param operUserId
     * @param orderNum
     * @param payment_method 支付方式 0微信 1会员卡 2 免费
     * @return boolean
     * @author: leroy
     * @date 2021/6/24 10:49
     */
    @Override
    public boolean updatePaySuccessOrderStatus(String operUserId, String orderNum, Integer payment_method) {
        //订单号
        QueryWrapper<OrderActivity> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq("order_num", orderNum);
        OrderActivity one = this.service.getOne(productQueryWrapper);
        if (Objects.isNull(one)) {
        }
        OrderActivity infoNew = new OrderActivity();
        infoNew.setId(one.getId());
        infoNew.setPaymentMethod(OrderType.WeiXin.getType());
        infoNew.setPayState(1);
        infoNew.setPayTime(System.currentTimeMillis());
        infoNew.setIsOrderStatus(OrderType.PAY.getType());

        //减去库存
        ActivityPrice price = this.activityPriceService.getById(one.getActivityPriceId());
        if (price.getStock() <= 0) {
          /*  throw new CommonException(BasePojectExceptionCodeEnum.UnderStockFail.code,
                    BasePojectExceptionCodeEnum.UnderStockFail.msg);*/
        }

        price.setStock(price.getStock() - 1);
        this.activityPriceService.updateById(price);

        return this.service.updateById(infoNew);
    }


    /**
     * 订单退款成功增加库存
     *
     * @param operUserId
     * @param orderNum
     * @return
     */
    @Override
    public boolean updateRefundOrderStatus(String operUserId, String orderNum) {
        //订单号
        QueryWrapper<OrderActivity> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq("order_num", orderNum);
        OrderActivity one = this.service.getOne(productQueryWrapper);
        if (Objects.isNull(one)) {
        }
        OrderActivity infoNew = new OrderActivity();
        infoNew.setId(one.getId());
        infoNew.setRefundEndTime(System.currentTimeMillis());
        infoNew.setIsOrderStatus(OrderType.WRITEOFF.getType());
        //减去库存
        ActivityPrice price = this.activityPriceService.getById(one.getActivityPriceId());
        price.setStock(price.getStock() + 1);
        this.activityPriceService.updateById(price);

        return this.service.updateById(infoNew);
    }


    /**
     * 申请退款
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean applicationDrawbackOrder(String operUserId, OrderActivityDTO bean) {
        //查询数据库是否有此数据
        OrderActivity info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }

        //创建对象
        OrderActivity infoNew = new OrderActivity();
        //对象生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //活动id
        if (StringUtil.notBlank(bean.getActivityId())) {
            infoNew.setActivityId(bean.getActivityId());
        }
        //活动库存ID
        if (StringUtil.notBlank(bean.getActivityPriceId())) {
            infoNew.setActivityPriceId(bean.getActivityPriceId());
        }
        //付款用户ID
        if (StringUtil.notBlank(bean.getPayOperatorId())) {
            infoNew.setPayOperatorId(bean.getPayOperatorId());
        }
        //付款时间
        infoNew.setPayTime(bean.getPayTime());
        //优惠类别  对应会员卡
        infoNew.setDiscountType(bean.getDiscountType());
        //会员卡id
        infoNew.setCardId(bean.getCardId());
        //会员卡号
        infoNew.setCardName(bean.getCardName());
        //折扣id
        infoNew.setDiscountId(bean.getDiscountId());
        //折扣
        infoNew.setDiscountName(bean.getDiscountName());
        //支付方式 0微信  1会员卡
        infoNew.setPaymentMethod(bean.getPaymentMethod());
        // 是否支付成功 0未成功 1已成功
        infoNew.setPayState(bean.getPayState());
        //退款发起时间
        infoNew.setRefundStartTime(bean.getRefundStartTime());
        //退款成功时间
        infoNew.setRefundEndTime(bean.getRefundEndTime());
        //活动人数
        infoNew.setActivityPeopleNumber(bean.getActivityPeopleNumber());
        //订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款  7已退款
        infoNew.setIsOrderStatus(bean.getIsOrderStatus());
        //退款描述
        infoNew.setRefundToDescribe(bean.getRefundToDescribe());


        infoNew.setRefundOrderStatus(info.getIsOrderStatus());
        //更新者数据
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        //判断数据是否更新成功
        return this.service.updateById(infoNew);
    }


    /**
     * 拒绝申请退款
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean refuseRefundOrder(String operUserId, OrderActivityDTO bean) {
        //查询数据库是否有此数据
        OrderActivity info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }


        //创建对象
        OrderActivity infoNew = new OrderActivity();
        //对象生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }

        infoNew.setIsOrderStatus(info.getRefundOrderStatus());
        //更新者数据
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        //判断数据是否更新成功
        return this.service.updateById(infoNew);
    }


    /**
     * 确定申请退卷
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean confirmRefundOrder(String operUserId, OrderActivityDTO bean) {
        //查询数据库是否有此数据
        OrderActivity info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        boolean returnFlag = this.userRefundPayServiceBLL.OrderRepair(info.getId(), info.getOrderNum(), OrderType.ACTIVITY.getType(), info.getRealCost(), operUserId, "订单退款");
        if (!returnFlag) {
            // throw new CommonException(BasePojectExceptionCodeEnum.ReturnFail.code, BasePojectExceptionCodeEnum.ReturnFail.msg);
        }

        //创建对象
        OrderActivity infoNew = new OrderActivity();
        //对象生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }

        infoNew.setIsOrderStatus(OrderType.RETURN.getType());
        //更新者数据
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        //判断数据是否更新成功
        return this.service.updateById(infoNew);
    }
}