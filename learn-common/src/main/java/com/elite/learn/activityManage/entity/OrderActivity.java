package com.elite.learn.activityManage.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 活动售卖订单表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_order_activity")
public class OrderActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动售卖订单表主键ID
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 活动ID
     */
    private String activityId;
    /**
     * 活动库存ID
     */
    private String activityPriceId;
    /**
     * 付款用户ID
     */
    private String payOperatorId;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 付款时间
     */
    private Long payTime;

    /**
     * 活动付款金额
     */
    private BigDecimal actCost;

    /**
     * 实际付款金额
     */
    private BigDecimal realCost;

    /**
     * 优惠类别  对应会员卡
     */
    private String discountType;

    /**
     * 会员卡id
     */
    private String cardId;

    /**
     * 会员卡号
     */
    private String cardName;

    /**
     * 折扣id
     */
    private String discountId;

    /**
     * 折扣
     */
    private String discountName;

    /**
     * 支付方式 0微信 1会员卡
     */
    private Integer paymentMethod;

    /**
     * 是否支付成功 0未成功 1已成功
     */
    private Integer payState;

    /**
     * 退款发起时间
     */
    private Long refundStartTime;

    /**
     * 退款成功时间
     */
    private Long refundEndTime;

    /**
     * 活动人数
     */
    private Integer activityPeopleNumber;

    /**
     * 订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款  7已退款
     */
    private Integer isOrderStatus;
    private Integer refundOrderStatus;

    /**
     * 退款描述
     */
    private String refundToDescribe;

    /**
     * 备注
     */
    private String memo;

    /**
     * 下单活动日期的拼接
     */
    private String  orderActivityDate;

    /**
     * 签到时间
     */
    private String signInTime;

    /**
     * 0未签到  1签到
     */
    private Integer isSignIn;

    /**
     * 类型  2业主  1访客
     */
    private Integer isUserType;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 删除标识 0:未删除 1:删除
     */
    private Integer isDelete;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;

    /**
     * 备用字段1
     */
    private String extraParams1;

    /**
     * 备用字段2
     */
    private String extraParams2;

    /**
     * 备用字段3
     */
    private String extraParams3;

    /**
     * 备用字段4
     */
    private String extraParams4;


}
