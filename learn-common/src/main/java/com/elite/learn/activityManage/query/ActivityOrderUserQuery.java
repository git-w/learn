package com.elite.learn.activityManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 活动用户中间表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
public class ActivityOrderUserQuery  extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动用户中间表 线下管理 主键ID
     */
    private String id;

    /**
     * 活动ID
     */
    private String activityId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 订单ID
     */
    private String orderId;





}
