package com.elite.learn.activityManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.ActivityOrderTime;
import com.elite.learn.activityManage.mapper.ActivityOrderTimeMapper;
import com.elite.learn.activityManage.service.IActivityOrderTimeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.activityManage.vo.ActivityOrderTimeVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 活动订单时间区间表模块 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Service
public class ActivityOrderTimeServiceImpl extends ServiceImpl<ActivityOrderTimeMapper, ActivityOrderTime> implements IActivityOrderTimeService {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/20 16:49
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public ActivityOrderTimeVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/20 16:49
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<ActivityOrderTimeVO> getLists(Page<ActivityOrderTimeVO> pageParams, @Param(Constants.WRAPPER)  Wrapper<ActivityOrderTime> queryWrapper) {

        pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));
        return pageParams;
    }
}
