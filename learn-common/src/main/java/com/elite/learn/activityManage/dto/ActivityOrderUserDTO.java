package com.elite.learn.activityManage.dto;

import lombok.Data;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 活动用户中间表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
public class ActivityOrderUserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动用户中间表 线下管理 主键ID
     */
    @NotBlank(groups = Update.class, message = "id参数不能为空")
    private String id;

    /**
     * 活动ID
     */
    @NotBlank(message = "活动id，参数不能为空")
    private String activityId;

    /**
     * 用户ID
     */
    @NotBlank(message = "用户ID，参数不能为空")
    private String userId;

    /**
     * 订单ID
     */
    @NotBlank(message = "订单ID，参数不能为空")
    private String orderId;

    /**
     * 是否签到 0未签到 1已签到
     */
    @NotNull(message = " 是否签到，参数不能为空")
    private Integer isState;

    /**
     * 签到时间
     */
    @NotNull(message = " 签到时间，参数不能为空")
    private Long signInTime;

    /**
     * 名字
     */
    @NotBlank(message = " 名字，参数不能为空")
    private String userName;

    /**
     * 身份证号
     */
    @NotBlank(message = " 身份证号，参数不能为空")
    private String pinCodes;

    /**
     * 手机号
     */
    @NotBlank(message = " 手机号，参数不能为空")
    private String mobilePhone;

    /**
     * 衣服尺码
     */
    @NotBlank(message = " 衣服尺码，参数不能为空")
    private String clothesSize;

    /**
     * 1男  2女
     */
    @NotNull(message = " 性別，参数不能为空")
    private Integer sex;

    /**
     * 年龄
     */
    @NotNull(message = " 年龄，参数不能为空")
    private Integer age;

    /**
     * 问题1的答案
     */
    private String answerOne;

    /**
     * 问题2的答案
     */
    private String answerTwo;

    /**
     * 问题3的答案
     */
    private String answerThree;

    /**
     * 问题4的答案
     */
    private String answerFour;

    /**
     * 问题5的答案
     */
    private String answerFive;

    /**
     * 问题6的答案
     */
    private String answerSix;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建者购买时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;


}
