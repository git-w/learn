package com.elite.learn.activityManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.bll.IActivityCateServiceBLL;
import com.elite.learn.activityManage.dto.ActivityCateDTO;
import com.elite.learn.activityManage.entity.ActivityCate;
import com.elite.learn.activityManage.query.ActivityCateQuery;
import com.elite.learn.activityManage.service.IActivityCateService;
import com.elite.learn.activityManage.vo.ActivityCateVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 活动分类表
 * @Title: ActivityCateServiceBLL
 * @Package com.elite.learn.activityManagementManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/7 14:41
 */
@Service
public class ActivityCateServiceBLL implements IActivityCateServiceBLL {


    @Resource
    private IActivityCateService service;


    /**
     * 主键id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/7 14:42
     * @update
     * @updateTime
     */
    @Override
    public ActivityCate get(String id) {
        return this.service.getById(id);
    }


    /**
     * 详情
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/7 14:43
     * @update
     * @updateTime
     */
    @Override
    public ActivityCateVO getInfo(String id) {
        return this.service.getInfo(id);
    }


    /**
     * 保存
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/7 14:44
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, ActivityCateDTO bean) {
        //创建实体对象
        ActivityCate info = new ActivityCate();
        //获取用户生成的id
        info.setId(IdUtils.simpleUUID());
        //分类名称
        info.setCateName(bean.getCateName());
        //父分类ID
        info.setParentId(bean.getParentId());
        //排序 数值越大越靠前
        info.setSort(bean.getSort());
        //备注
        info.setMemo(bean.getMemo());
        //是否上线 0是 1否
        info.setIsState(bean.getIsState());
        //额外参数1
        info.setExtraParams1(bean.getExtraParams1());
        //删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //创建者信息
        info.setCreateOperatorId(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateTime(System.currentTimeMillis());

        boolean result = this.service.save(info);
        return result ? info.getId() : null;
    }


    /**
     * 更新
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/7 14:50
     * @update
     * @updateTime
     */
    @Override
    public boolean update(String operUserId, ActivityCateDTO bean) {
        // 判断通过ID获取的对象是否为空
        ActivityCate info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }

        //创建实体对象
        ActivityCate infoNew = new ActivityCate();
        //获取用户生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //分类名称
        if (StringUtil.notBlank(bean.getCateName())) {
            infoNew.setCateName(bean.getCateName());
        }
        //父分类ID
        infoNew.setParentId(bean.getParentId());
        //排序 数值越大越靠前
        infoNew.setSort(bean.getSort());
        //备注
        infoNew.setMemo(bean.getMemo());
        //是否上线 0是 1否
        infoNew.setIsState(bean.getIsState());
        //额外参数1
        infoNew.setExtraParams1(bean.getExtraParams1());


        //设置修改者信息
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        return this.service.updateById(infoNew);
    }


    /**
     * 删除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2021/12/7 14:57
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        ActivityCate infoDel = null;
        //分割id
        String[] idArr = ids.split(",");
        //遍历id判断是否为空，弹出本次循环
        for (String id : idArr) {
            ActivityCate info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new ActivityCate();
            infoDel.setId(info.getId());
            //设置删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            //删除id
            infoDel.setDeleteOperatorId(operUserId);

            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     * 查询所有
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/7 14:58
     * @update
     * @updateTime
     */
    @Override
    public List<ActivityCateVO> findAll(ActivityCateQuery bean) {
        QueryWrapper<ActivityCate> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("is_state", DeleteType.NOTDELETE.getCode());
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        return this.service.findAll(queryWrapper);
    }


    /**
     * 分页
     *
     * @param params
     * @return pageResult
     * @author: leroy
     * @date 2021/12/8 15:19
     * @update
     * @updateTime
     */
    @Override
    public PageResult<ActivityCateVO> getList(ActivityCateQuery params) {
        QueryWrapper<ActivityCate> queryWrapper = new QueryWrapper<ActivityCate>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("create_time");
        // 根据分组查询
        if (StringUtil.notBlank(params.getCateName())) {
            queryWrapper.eq("cate_name", params.getCateName());
        }
        Page<ActivityCateVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<ActivityCateVO> page = this.service.getLists(PageParams, queryWrapper);
        return new PageResult<>(page);
    }


    /**
     * 批量上下线
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean updateMultiIsEnabled(String operUserId, BaseParams params) {
        //判断端状态码是否为空
        if (Objects.isNull(params.getIsState())) {
            throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
        }
        boolean flag = true;
        ActivityCate infoNew = null;
        String[] idsArr = params.getIds().split(",");
        for (String id : idsArr) {
            ActivityCate info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoNew = new ActivityCate();
            infoNew.setId(info.getId());
            // 是否上线 0是 1否
            infoNew.setIsState(params.getIsState());

            infoNew.setUpdateTime(System.currentTimeMillis());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }
}