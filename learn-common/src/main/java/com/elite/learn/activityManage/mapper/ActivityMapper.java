package com.elite.learn.activityManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.vo.ActivityVO;
import com.elite.learn.activityManage.entity.Activity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 订单表---活动表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-08
 */
public interface ActivityMapper extends BaseMapper<Activity> {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/8 16:09
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     */
    List<ActivityVO> getLists(Page<ActivityVO> pageParams, @Param(Constants.WRAPPER) Wrapper<Activity> queryWrapper);


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/8 16:10
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityVO getInfo(String id);
}
