package com.elite.learn.activityManage.dto;

import lombok.Data;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 活动分类表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
@Data
public class ActivityCateDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * 分类名称
     */
    @NotBlank(message = "分类名称,参数不能为空")
    private String cateName;

    /**
     * 父分类ID
     */
    private String parentId;

    /**
     * 排序 数值越大越靠前
     */
    private Integer sort;

    /**
     * 备注
     */
    private String memo;

    /**
     * 是否上线 0是 1否
     */
    private Integer isState;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;

    /**
     * 额外参数1
     */
    private String extraParams1;


}
