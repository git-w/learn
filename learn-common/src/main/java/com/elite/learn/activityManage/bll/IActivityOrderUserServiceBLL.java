package com.elite.learn.activityManage.bll;


import com.elite.learn.activityManage.dto.ActivityOrderUserDTO;
import com.elite.learn.activityManage.entity.ActivityOrderUser;
import com.elite.learn.activityManage.query.ActivityOrderUserQuery;
import com.elite.learn.activityManage.vo.ActivityOrderUserVO;
import com.elite.learn.common.core.service.ICommonServiceBLL;

public interface IActivityOrderUserServiceBLL  extends ICommonServiceBLL<ActivityOrderUser, ActivityOrderUserDTO, ActivityOrderUserVO, ActivityOrderUserQuery> {

}
