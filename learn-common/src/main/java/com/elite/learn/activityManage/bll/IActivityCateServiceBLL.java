package com.elite.learn.activityManage.bll;

import com.elite.learn.activityManage.vo.ActivityCateVO;
import com.elite.learn.activityManage.dto.ActivityCateDTO;
import com.elite.learn.activityManage.entity.ActivityCate;
import com.elite.learn.activityManage.query.ActivityCateQuery;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.ICommonServiceBLL;

public interface IActivityCateServiceBLL extends ICommonServiceBLL<ActivityCate, ActivityCateDTO, ActivityCateVO, ActivityCateQuery> {


    /**
     *
     * @author: leroy
     * @date 2021/12/7 15:05
     * @param params
     * @return null
     * @update
     * @updateTime
     */
     boolean updateMultiIsEnabled(String operUserId, BaseParams params);
}
