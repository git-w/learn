package com.elite.learn.activityManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.vo.ActivityCateVO;
import com.elite.learn.activityManage.entity.ActivityCate;
import com.elite.learn.activityManage.mapper.ActivityCateMapper;
import com.elite.learn.activityManage.service.IActivityCateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 活动分类表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
@Service
public class ActivityCateServiceImpl extends ServiceImpl<ActivityCateMapper, ActivityCate> implements IActivityCateService {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/7 15:11
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public ActivityCateVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     * 查询所有
     * @author: leroy
     * @date 2021/12/7 15:12
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public List<ActivityCateVO> findAll(@Param(Constants.WRAPPER) Wrapper<ActivityCate> queryWrapper) {
        return this.baseMapper.findAll(queryWrapper);
    }


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/7 15:13
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<ActivityCateVO> getLists(Page<ActivityCateVO> pageParams, @Param(Constants.WRAPPER) Wrapper<ActivityCate> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));

        return pageParams;
    }
}
