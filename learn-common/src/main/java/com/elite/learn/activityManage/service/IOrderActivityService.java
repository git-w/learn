package com.elite.learn.activityManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.OrderActivity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.activityManage.vo.OrderActivityVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 活动售卖订单表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
public interface IOrderActivityService extends IService<OrderActivity> {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/21 9:36
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    OrderActivityVO getInfo(String id);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/21 10:13
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    IPage<OrderActivityVO> getLists(Page<OrderActivityVO> pageParams, @Param(Constants.WRAPPER) Wrapper<OrderActivity> queryWrapper);
}
