package com.elite.learn.activityManage.bll;

import com.elite.learn.activityManage.dto.OrderActivityDTO;
import com.elite.learn.activityManage.entity.OrderActivity;
import com.elite.learn.activityManage.query.OrderActivityQuery;
import com.elite.learn.activityManage.vo.OrderActivityVO;
import com.elite.learn.common.core.service.ICommonServiceBLL;

import java.math.BigDecimal;

public interface IOrderActivityServiceBLL extends ICommonServiceBLL<OrderActivity, OrderActivityDTO, OrderActivityVO, OrderActivityQuery> {


    /**
     * <p>
     * 支付前检查库存等
     * </p>
     *
     * @param operUserId
     * @param orderNum
     * @return java.math.BigDecimal
     * @author Winder
     * @date 2021/8/19 下午3:01
     */
    BigDecimal getCost(String operUserId, String orderNum);


    /**
     * <p>
     * 订单支付成功减去库存
     * </p>
     *
     * @param operUserId
     * @param orderNum
     * @return boolean
     * @author: leroy
     * @date 2021/6/24 10:49
     */
    boolean updatePaySuccessOrderStatus(String operUserId, String orderNum, Integer payment_method);


    /**
     * <p>
     * 订单退款成功增加库存
     * </p>
     *
     * @param operUserId
     * @param orderNum
     * @return boolean
     * @author: leroy
     * @date 2021/6/24 10:49
     */
    boolean updateRefundOrderStatus(String operUserId, String orderNum);


    /**
     * 申请退款
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    boolean applicationDrawbackOrder(String operUserId, OrderActivityDTO bean);


    /**
     * 拒绝退款
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    boolean refuseRefundOrder(String operUserId, OrderActivityDTO bean);


    /**
     * 确定退款
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    boolean confirmRefundOrder(String operUserId, OrderActivityDTO bean);
}
