package com.elite.learn.activityManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.ActivityOrderUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.activityManage.vo.ActivityOrderUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动用户中间表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
public interface ActivityOrderUserMapper extends BaseMapper<ActivityOrderUser> {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/20 17:48
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityOrderUserVO getInfo(String id);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/20 17:49
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<ActivityOrderUserVO> getLists(Page<ActivityOrderUserVO> pageParams, @Param(Constants.WRAPPER)Wrapper<ActivityOrderUser> queryWrapper);
}
