package com.elite.learn.activityManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 活动时间价格区间表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data
public class ActivityPriceQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动时间价格区间表 主键ID
     */
    private String id;

    /**
     * 活动id
     */
    private String activityId;

    /**
     * 1在线 2下架
     */
    private Integer isState;





}
