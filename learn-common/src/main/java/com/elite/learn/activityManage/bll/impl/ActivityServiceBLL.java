package com.elite.learn.activityManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.bll.IActivityServiceBLL;
import com.elite.learn.activityManage.dto.ActivityDTO;
import com.elite.learn.activityManage.entity.Activity;
import com.elite.learn.activityManage.query.ActivityQuery;
import com.elite.learn.activityManage.service.IActivityService;
import com.elite.learn.activityManage.vo.ActivityVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 活动表
 * @Title: ActivityServiceBLL
 * @Package com.elite.learn.activityManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/8 13:52
 */
@Service
public class ActivityServiceBLL implements IActivityServiceBLL {


    @Resource
    private IActivityService service;


    /**
     * 主键id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/8 13:56
     * @update
     * @updateTime
     */
    @Override
    public Activity get(String id) {
        return this.service.getById(id);
    }


    /**
     * 详情
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/8 13:57
     * @update
     * @updateTime
     */
    @Override
    public ActivityVO getInfo(String id) {
        ActivityVO info = this.service.getInfo(id);
        if (Objects.nonNull(info)) {
            //活动封面
            info.setPicturesUrl(FileUploadUtil.getImgRootPath() + info.getPictures());
            //拼接主图长地址
            if (StringUtil.isNotEmpty(info.getImgPathList())) {
                String[] arrPic = info.getImgPathList().split(",");
                StringBuffer sb = new StringBuffer();
                for (String pic : arrPic) {
                    sb.append(FileUploadUtil.getImgRootPath() + pic).append(",");
                }
                String keywordStr = sb.deleteCharAt(sb.length() - 1).toString();
                info.setImgUrlPath(keywordStr);
            }
            //分享图片
            info.setShareItPicturesUrl(FileUploadUtil.getImgRootPath() + info.getShareItPictures());
        }

        return info;
    }


    /**
     * @param
     * @return null
     * @author: leroy
     * @date 2021/12/8 14:30
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, ActivityDTO bean) {
        //创建对象实体
        Activity info = new Activity();
        //获取主键生成id
        info.setId(IdUtils.simpleUUID());
        //活动名称
        info.setName(bean.getName());
        //活动简介
        info.setMsg(bean.getMsg());
        //活动描述(富文本)
        info.setContent(bean.getContent());
        //预定须知
        info.setBookingInformation(bean.getBookingInformation());
        //活动封面
        info.setPictures(bean.getPictures());
        //分享图片
        info.setShareItPictures(bean.getShareItPictures());
        //活动轮播图最多九张
        info.setBanner(bean.getBanner());
        //活动地址
        info.setAddress(bean.getAddress());
        //图片存放路径（短），多图用,隔开
        info.setImgPathList(bean.getImgPathList());
        //经度
        info.setLongitude(bean.getLongitude());
        //纬度
        info.setLatitude(bean.getLatitude());
        //活动开始时间
        info.setStratTime(bean.getStratTime());
        //活动结束时间
        info.setEndTime(bean.getEndTime());
        //是否免费：0-免费；1-不免费
        info.setIsFree(bean.getIsFree());
        //活动报名费
        info.setPrice(bean.getPrice());
        //活动名额
        info.setActivityUserCount(bean.getActivityUserCount());
        //活动分类
        info.setCategoryId(bean.getCategoryId());
        //状态 0在线 1下线
        info.setIsState(bean.getIsState());
        // 售卖开始时间
        info.setPayStartTime(bean.getPayStartTime());
        //售卖结束时间
        info.setPayEndTime(bean.getPayEndTime());
        //额外参数1
        info.setExtraParams1(bean.getExtraParams1());
        //额外参数2
        info.setExtraParams2(bean.getExtraParams2());
        //额外参数3
        info.setExtraParams3(bean.getExtraParams3());
        //删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //创建者信息
        info.setCreateOperatorId(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateTime(System.currentTimeMillis());

        boolean result = this.service.save(info);
        return result ? info.getId() : null;
    }


    /**
     * 更新
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/8 14:47
     * @update
     * @updateTime
     */
    @Override
    public boolean update(String operUserId, ActivityDTO bean) {
        // 判断通过ID获取的对象是否为空
        Activity info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建对象实体
        Activity infoNew = new Activity();
        //获取主键生成id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //活动名称
        if (StringUtil.notBlank(bean.getName())) {
            infoNew.setName(bean.getName());
        }
        //活动地址
        if (StringUtil.notBlank(bean.getAddress())) {
            infoNew.setAddress(bean.getAddress());
        }
        //活动简介
        infoNew.setMsg(bean.getMsg());
        //活动描述(富文本)
        infoNew.setContent(bean.getContent());
        //预定须知
        infoNew.setBookingInformation(bean.getBookingInformation());
        //活动封面
        infoNew.setPictures(bean.getPictures());
        //分享图片
        infoNew.setShareItPictures(bean.getShareItPictures());
        //活动轮播图最多九张
        infoNew.setBanner(bean.getBanner());
        //图片存放路径（短），多图用,隔开
        infoNew.setImgPathList(bean.getImgPathList());
        //经度
        infoNew.setLongitude(bean.getLongitude());
        //纬度
        infoNew.setLatitude(bean.getLatitude());
        //活动开始时间
        infoNew.setStratTime(bean.getStratTime());
        //活动结束时间
        infoNew.setEndTime(bean.getEndTime());
        //是否免费：0-免费；1-不免费
        infoNew.setIsFree(bean.getIsFree());
        //活动报名费
        infoNew.setPrice(bean.getPrice());
        //活动名额
        infoNew.setActivityUserCount(bean.getActivityUserCount());
        //活动分类
        if (StringUtil.notBlank(bean.getCategoryId())) {
            infoNew.setCategoryId(bean.getCategoryId());
        }
        //状态 0在线 1下线
        infoNew.setIsState(bean.getIsState());
        // 售卖开始时间
        infoNew.setPayStartTime(bean.getPayStartTime());
        //售卖结束时间
        infoNew.setPayEndTime(bean.getPayEndTime());
        //额外参数1
        infoNew.setExtraParams1(bean.getExtraParams1());
        //额外参数2
        infoNew.setExtraParams2(bean.getExtraParams2());
        //额外参数3
        infoNew.setExtraParams3(bean.getExtraParams3());

        //更新者信息
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        return this.service.updateById(infoNew);
    }


    /**
     * 删除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2021/12/8 15:15
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        //每个id已逗号分割
        String[] idArr = ids.split(",");
        //遍历id
        for (String id : idArr) {
            //获取id判断是否为空
            Activity info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            //创建删除实体
            Activity infoDel = new Activity();
            //设置删除状态
            infoDel.setId(info.getId());
            infoDel.setDeleteOperatorId(operUserId);
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            infoDel.setDeleteTime(System.currentTimeMillis());

            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    @Override
    public List<ActivityVO> findAll(ActivityQuery bean) {
        return null;
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/8 15:17
     * @update
     * @updateTime
     */
    @Override
    public PageResult<ActivityVO> getList(ActivityQuery params) {
        QueryWrapper<Activity> queryWrapper = new QueryWrapper<Activity>();
        queryWrapper.eq("tia.is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("tia.create_time");
        // 根据活动名称
        if (StringUtil.notBlank(params.getName())) {
            queryWrapper.eq("tia.name", params.getName());
        }
        //活动地址
        if (StringUtil.notBlank(params.getAddress())) {
            queryWrapper.eq("tia.address", params.getAddress());
        }
        //活动分类
        if (StringUtil.notBlank(params.getCategoryId())) {
            queryWrapper.eq("tia.category_id", params.getCategoryId());
        }
        // 根据活动名称
        if (Objects.nonNull(params.getIsState())) {
            queryWrapper.eq("tia.is_state", params.getIsState());
        }

        Page<ActivityVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<ActivityVO> page = this.service.getLists(PageParams, queryWrapper);

        //有查询结果
        if (null != page.getRecords() && page.getRecords().size() > 0) {
            for (ActivityVO info : page.getRecords()) {
                if (StringUtil.notBlank(info.getImgPathList())) {
                    //拼接主图长地址
                    info.setImgUrlPath(FileUploadUtil.getImgRootPath() + info.getImgPathList());
                }
                //   分享图片
                if (StringUtil.notBlank(info.getShareItPictures())) {
                    //拼接主图长地址
                    info.setImgUrlPath(FileUploadUtil.getImgRootPath() + info.getShareItPictures());
                }
                //  活动封面
                if (StringUtil.notBlank(info.getPictures())) {
                    //拼接主图长地址
                    info.setImgUrlPath(FileUploadUtil.getImgRootPath() + info.getPictures());
                }
            }
        }

        return new PageResult<>(page);
    }


}