package com.elite.learn.activityManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.bll.IActivityPriceServiceBLL;
import com.elite.learn.activityManage.dto.ActivityPriceDTO;
import com.elite.learn.activityManage.entity.ActivityPrice;
import com.elite.learn.activityManage.query.ActivityPriceQuery;
import com.elite.learn.activityManage.service.IActivityPriceService;
import com.elite.learn.activityManage.vo.ActivityPriceVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.common.utils.string.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 活动时间价格区间表
 * @Title: ActivityPriceServiceBLL
 * @Package com.elite.learn.activityManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/20 17:59
 */
@Service
public class ActivityPriceServiceBLL implements IActivityPriceServiceBLL {


    @Resource
    private IActivityPriceService service;


    /**
     * 主键 id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/20 18:12
     * @update
     * @updateTime
     */
    @Override
    public ActivityPrice get(String id) {
        return this.service.getById(id);
    }


    /**
     * 详情
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/20 18:13
     * @update
     * @updateTime
     */
    @Override
    public ActivityPriceVO getInfo(String id) {
        return this.service.getInfo(id);
    }


    /**
     * 保存
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/20 18:14
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, ActivityPriceDTO bean) {
        //创建实体对象
        ActivityPrice info = new ActivityPrice();
        //主键ID
        info.setId(IdUtils.simpleUUID());
        //活动id
        info.setActivityId(bean.getActivityId());
        //开始时间
        info.setStartTime(bean.getStartTime());
        //结束时间
        info.setEndTime(bean.getEndTime());
        //业主价格
        info.setPrice(bean.getPrice());
        //库存
        info.setStock(bean.getStock());

        //会员价格
        info.setVipDiscount(bean.getVipDiscount());
        //价格
        info.setVipPrice(bean.getVipPrice());

        //删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());

        //创建者信息
        info.setCreateOperatorId(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateTime(System.currentTimeMillis());

        //添加数据
        boolean result = this.service.save(info);
        //判断是否添加成功
        return result ? info.getId() : null;
    }


    /**
     * 更新
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/20 18:28
     * @update
     * @updateTime
     */
    @Override
    public boolean update(String operUserId, ActivityPriceDTO bean) {
        ActivityPrice info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        ActivityPrice infoNew = new ActivityPrice();
        //主键ID
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //活动id
        if (StringUtil.notBlank(bean.getActivityId())) {
            infoNew.setActivityId(bean.getActivityId());
        }
        //开始时间
        if (Objects.nonNull(bean.getStartTime())) {
            infoNew.setStartTime(bean.getStartTime());
        }
        //结束时间
        if (Objects.nonNull(bean.getEndTime())) {
            infoNew.setEndTime(bean.getEndTime());
        }
        //业主价格
        infoNew.setPrice(bean.getPrice());
        //库存
        infoNew.setStock(bean.getStock());

        //库存
        infoNew.setVipPrice(bean.getVipPrice());

        //更新数据判断是否成功
        return this.service.updateById(infoNew);
    }


    /**
     * 刪除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2021/12/21 9:01
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        ActivityPrice infoDel = null;
        //分割id
        String[] idArr = ids.split(",");
        //遍历id判断是否为空，弹出本次循环
        for (String id : idArr) {
            ActivityPrice info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new ActivityPrice();
            infoDel.setId(info.getId());
            //设置删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            //删除id
            infoDel.setDeleteOperatorId(operUserId);

            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    @Override
    public List<ActivityPriceVO> findAll(ActivityPriceQuery params) {
        QueryWrapper<ActivityPriceVO> queryWrapper = new QueryWrapper<ActivityPriceVO>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("create_time");

        // 根据1在线,2下架过滤
        if (StringUtils.isNotEmpty(params.getActivityId())) {
            queryWrapper.eq("activity_id", params.getActivityId());
        }
        return this.service.findAll(queryWrapper);
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/21 9:06
     * @update
     * @updateTime
     */
    @Override
    public PageResult<ActivityPriceVO> getList(ActivityPriceQuery params) {
        QueryWrapper<ActivityPriceVO> queryWrapper = new QueryWrapper<ActivityPriceVO>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("create_time");


        // 根据1在线,2下架过滤
        if (StringUtils.isNotEmpty(params.getActivityId())) {
            queryWrapper.eq("activity_id", params.getActivityId());
        }

        Page<ActivityPriceVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<ActivityPriceVO> page = this.service.getLists(PageParams, queryWrapper);
        return new PageResult<>(page);

    }

    /**
     * 添加 活动库存
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2022/1/14 11:48
     * @update
     * @updateTime
     */
    @Override
    public boolean saveBatch(String operUserId, List<ActivityPriceDTO> params) {
        List<ActivityPrice> list = new ArrayList<>();
        //创建实体对象
        ActivityPrice info = null;
        if (Objects.nonNull(params) && params.size() > 0) {
            for (ActivityPriceDTO bean : params) {
                info = new ActivityPrice();
                //主键ID
                info.setId(IdUtils.simpleUUID());
                //活动id
                info.setActivityId(bean.getActivityId());

                //开始时间
                info.setStartTime(bean.getStartTime());
                //结束时间
                info.setEndTime(bean.getEndTime());
                //业主价格
                info.setPrice(bean.getPrice());
                //库存
                info.setStock(bean.getStock());

                //删除标识 0:未删除 1:删除
                info.setIsDelete(DeleteType.NOTDELETE.getCode());
                //创建者信息
                info.setCreateOperatorId(operUserId);
                info.setCreateTime(System.currentTimeMillis());
                list.add(info);
            }

        }
        //添加数据
        return this.service.saveBatch(list);
    }
}