package com.elite.learn.activityManage.contants;

public enum ActivityManageExceptionCodeEnum {

;

    public Integer code;
    public String msg;

    private ActivityManageExceptionCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
