package com.elite.learn.activityManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.ActivityOrderUser;
import com.elite.learn.activityManage.mapper.ActivityOrderUserMapper;
import com.elite.learn.activityManage.service.IActivityOrderUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.activityManage.vo.ActivityOrderUserVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 活动用户中间表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Service
public class ActivityOrderUserServiceImpl extends ServiceImpl<ActivityOrderUserMapper, ActivityOrderUser> implements IActivityOrderUserService {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/20 17:46
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public ActivityOrderUserVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/20 17:47
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<ActivityOrderUserVO> getLists(Page<ActivityOrderUserVO> pageParams, @Param(Constants.WRAPPER)Wrapper<ActivityOrderUser> queryWrapper) {

        pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));
        return pageParams;
    }
}
