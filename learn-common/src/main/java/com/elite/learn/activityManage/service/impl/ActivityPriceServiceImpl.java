package com.elite.learn.activityManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.ActivityPrice;
import com.elite.learn.activityManage.mapper.ActivityPriceMapper;
import com.elite.learn.activityManage.service.IActivityPriceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.activityManage.vo.ActivityPriceVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 活动时间价格区间表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-28
 */
@Service
public class ActivityPriceServiceImpl extends ServiceImpl<ActivityPriceMapper, ActivityPrice> implements IActivityPriceService {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/21 9:09
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public ActivityPriceVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     *分页
     * @author: leroy
     * @date 2021/12/21 9:10
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<ActivityPriceVO> getLists(Page<ActivityPriceVO> pageParams, @Param(Constants.WRAPPER)Wrapper<ActivityPriceVO> queryWrapper) {
            pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));

        return pageParams;
    }

    /**
     * 查询全部
     * @author: leroy
     * @date 2022/1/14 13:26
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public List<ActivityPriceVO> findAll(@Param(Constants.WRAPPER) Wrapper<ActivityPriceVO> queryWrapper) {
        return this.baseMapper.findAll(queryWrapper);
    }

}
