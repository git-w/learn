package com.elite.learn.activityManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.entity.OrderActivity;
import com.elite.learn.activityManage.mapper.OrderActivityMapper;
import com.elite.learn.activityManage.service.IOrderActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.activityManage.vo.OrderActivityVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 活动售卖订单表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Service
public class OrderActivityServiceImpl extends ServiceImpl<OrderActivityMapper, OrderActivity> implements IOrderActivityService {


    /**
     * 详情
     * @author: leroy
     * @date 2021/12/21 10:17
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public OrderActivityVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/21 10:18
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<OrderActivityVO> getLists(Page<OrderActivityVO> pageParams, @Param(Constants.WRAPPER) Wrapper<OrderActivity> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));
        return pageParams;
    }
}
