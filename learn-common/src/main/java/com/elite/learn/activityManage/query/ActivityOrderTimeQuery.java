package com.elite.learn.activityManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 活动订单时间区间表模块
 * </p>
 *
 * @author: leroy
 * @since 2021-12-20
 */
@Data

public class ActivityOrderTimeQuery  extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单时间区间表 主键ID
     */
    private String id;

    /**
     * 活动id
     */
    private String activityId;



    /**
     * 用户id
     */
    private String userId;

    /**
     * 分类id
     */
    private String classifyId;

    /**
     * 关联订单id
     */
    private String orderId;


    /**
     * 0未使用  1已使用  2已过期
     */
    private Integer isUse;





}
