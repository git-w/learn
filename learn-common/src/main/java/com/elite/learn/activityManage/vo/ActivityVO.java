package com.elite.learn.activityManage.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单表---活动表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-08
 */
@Data
public class ActivityVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动主键ID
     */
    private String id;

    /**
     * 活动名称
     */
    private String name;

    /**
     * 活动简介
     */
    private String msg;

    /**
     * 活动描述(富文本)
     */
    private String content;

    /**
     * 预定须知
     */
    private String bookingInformation;

    /**
     * 活动封面
     */
    private String pictures;
    /**
     * 活动封面长路径
     */
    private String picturesUrl;

    /**
     * 分享图片
     */
    private String shareItPictures;


    /**
     * 分享图片长
     */
    private String shareItPicturesUrl;


    /**
     * 活动轮播图最多九张
     */
    private String banner;

    /**
     * 活动地址
     */
    private String address;

    /**
     * 图片存放路径（短），多图用,隔开
     */
    private String imgPathList;

    /**
     * 图片地址长路径
     */
    private String imgUrlPath;


    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 活动开始时间
     */
    private Long stratTime;

    /**
     * 活动结束时间
     */
    private Long endTime;

    /**
     * 是否免费：0-免费；1-不免费
     */
    private Integer isFree;

    /**
     * 活动报名费
     */
    private BigDecimal price;

    /**
     * 活动名额
     */
    private Integer activityUserCount;

    /**
     * 活动分类
     */
    private String categoryId;

    /**
     * 状态 0在线 1下线
     */
    private Integer isState;

    /**
     * 售卖开始时间
     */
    private Long payStartTime;

    /**
     * 售卖结束时间
     */
    private Long payEndTime;



    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;



    /**
     * 额外参数1
     */
    private String extraParams1;

    /**
     * 额外参数2
     */
    private String extraParams2;

    /**
     * 额外参数3
     */
    private String extraParams3;
    /**
     *是否收藏
     */
    private Boolean isCollect;

}
