package com.elite.learn.activityManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.activityManage.vo.ActivityCateVO;
import com.elite.learn.activityManage.entity.ActivityCate;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动分类表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
public interface IActivityCateService extends IService<ActivityCate> {


    /**
     *详情
     * @author: leroy
     * @date 2021/12/7 14:44
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    ActivityCateVO getInfo(String id);


    /**
     * 查询所有
     * @author: leroy
     * @date 2021/12/7 15:00
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<ActivityCateVO> findAll(@Param(Constants.WRAPPER) Wrapper<ActivityCate> queryWrapper);


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/7 15:23
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    IPage<ActivityCateVO> getLists(Page<ActivityCateVO> pageParams, @Param(Constants.WRAPPER) Wrapper<ActivityCate> queryWrapper);
}
