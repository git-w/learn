package com.elite.learn.cardManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 使用会员卡权益日志表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
@Data
public class LogUserMemberCardQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 会员
     */
    private String midCardId;

    /**
     * 开始时间
     */
    private Long startTime;

    /**
     * 结束时间
     */
    private Long  endTime;

}
