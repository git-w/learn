package com.elite.learn.cardManage.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单表---会员卡订单表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_order_card")
public class OrderCard implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 订单编号
     */
    private String orderNum;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货 5-完成 6-申请退款 7-退款中 8-退款完成 9-超时未支付取消的
     */
    private Integer orderStatus;

    /**
     * 收款总金额
     */
    private BigDecimal totalMoney;

    /**
     * 实付金额(元)
     */
    private BigDecimal realCost;

    /**
     *  1微信支付
     */
    private Integer payMode;

    /**
     * 支付状态，0未支付，1已支付，2已支付定金
     */
    @TableField("Pay_status")
    private Integer payStatus;

    /**
     * 备注
     */
    private String remark;

    /**
     * 订单备注
     */
    private String orderRemark;

    /**
     * 订单支付时间
     */
    private Date payTime;

    /**
     * 订单支付创建者ID
     */
    private String payOperatorId;

    /**
     * 完成时间
     */
    private Date finishTime;

    /**
     * 完成创建者ID
     */
    private String finishOperatorId;

    /**
     * 订单申请退货时间
     */
    @TableField("out_store_Time")
    private Date outStoreTime;

    /**
     * 订单发货时间
     */
    private Date deliverTime;

    /**
     * 订单发货创建者ID
     */
    private String deliverOperatorId;

    /**
     * 订单取消时间
     */
    private Date cancelTime;

    /**
     * 0.正常订单
     */
    private String orderType;

    /**
     * 备注
     */
    private String memo;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 删除标识 0:未删除 1:删除
     */
    private Integer isDelete;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;


}
