package com.elite.learn.cardManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 使用会员卡权益日志表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
@Data
public class LogUserMemberCardVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 来源任务ID 如：用户会员卡ID
     */
    private String cardId;

    /**
     * 来源任务ID 如：用户会员卡ID
     */
    private String cardConfigId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 关联类型 0-红娘牵线
     */
    private Integer isType;

    /**
     * 关联ID
     */
    private String othersId;

    /**
     * 领取数量
     */
    private Integer count;

    /**
     * 备注
     */
    private String memo;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;


    /**
     * 用户的昵称
     */
    private String coverNickName;

    /**
     * 用户头像
     */
    private String coverHeadimgurl;

    /**
     * 手机号
     */
    private String coverModilePhone;

    /**
     * 会员卡名字
     */
    private String name;
}
