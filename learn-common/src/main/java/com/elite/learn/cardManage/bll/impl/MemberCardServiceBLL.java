package com.elite.learn.cardManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.bll.IMemberCardConfigServiceBLL;
import com.elite.learn.cardManage.bll.IMemberCardServiceBLL;
import com.elite.learn.cardManage.dto.MemberCardDto;
import com.elite.learn.cardManage.entity.MemberCard;
import com.elite.learn.cardManage.query.MemberCardConfigQuery;
import com.elite.learn.cardManage.query.MemberCardQuery;
import com.elite.learn.cardManage.service.IMemberCardService;
import com.elite.learn.cardManage.vo.MemberCardConfigVO;
import com.elite.learn.cardManage.vo.MemberCardVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 会员卡信息表
 * @Title: InfoMemberCardServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/29 14:12
 */
@Service
public class MemberCardServiceBLL implements IMemberCardServiceBLL {

    @Resource
    private IMemberCardService service;
    //会员卡配置表
    @Resource
    private IMemberCardConfigServiceBLL memberCardConfigServuceBLL;


    /*
     *
     * @author: leroy
     * @date 2021/11/29 15:30
     * @param null
     * @return null
     * @update
     * @updateTime
     * 获取对象id
     */
    @Override
    public MemberCard get(String id) {
        return this.service.getById(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 15:32
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public MemberCardVO getInfo(String id) {
        MemberCardVO info = this.service.getInfo(id);
        if (Objects.nonNull(info)) {
            if (StringUtil.notBlank(info.getImgUrl())) {
                //拼接主图长地址
                info.setImgUrlPath(FileUploadUtil.getImgRootPath() + info.getImgUrl());
            }
        }
        return info;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 15:34
     * @param null
     * @return null
     * @update
     * @updateTime
     * 添加
     */
    @Override
    public String save(String operUserId, MemberCardDto bean) {
        //创建实体
        MemberCard info = new MemberCard();
        //创建用户生成的id
        info.setId(IdUtils.simpleUUID());
        //会员卡名称
        info.setName(bean.getName());
        //会员卡副标题
        info.setSubtitle(bean.getSubtitle());
        //商品主图
        info.setImgUrl(bean.getImgUrl());
        //价格
        info.setPrice(bean.getPrice());
        //会员级别 数字越大级别越大
        info.setLevel(bean.getLevel());
        //会员卡使用说明
        info.setContent(bean.getContent());
        //会员卡简介
        info.setIntroduction(bean.getIntroduction());
        //库存是否无限 0—是 1-否
        info.setIsInfinite(bean.getIsInfinite());
        //库存数量
        info.setInventoryCount(bean.getInventoryCount());
        // 有效时长单位，1-周，2-月，3-季，4-年
        info.setValidTimeUnit(bean.getValidTimeUnit());
        // 有效时长(周期)，表示几周，几个月
        info.setValidTime(bean.getValidTime());
        //有效期开始日期
        info.setStartTime(bean.getStartTime());
        //有效期结束日期
        info.setEndTime(bean.getEndTime());
        //是否启用 0-是 1-否
        info.setIsEnabled(bean.getIsEnabled());
        //备注
        info.setRemark(bean.getRemark());
        //备用字段
        info.setExtraParams1(bean.getExtraParams1());
        // 是否免费领取 0-是 1-否
        info.setIsFree(bean.getIsFree());
        // 是否免费领取 0-是 1-否
        info.setGetNum(bean.getGetNum());

        //删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //添加时间
        info.setCreateTime(System.currentTimeMillis());
        //创建者ID
        info.setCreateOperatorId(operUserId);
        boolean flag = this.service.save(info);
        return flag ? info.getId() : null;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 16:00
     * @param null
     * @return null
     * @update
     * @updateTime
     * 更新
     */
    @Override
    public boolean update(String operUserId, MemberCardDto bean) {

        // 判断通过ID获取的对象是否为空
        MemberCard info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体
        MemberCard infoNew = new MemberCard();
        //创建用户生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //会员卡名称
        if (StringUtils.isNotEmpty(bean.getName())) {
            infoNew.setName(bean.getName());
        }
        //会员卡副标题
        infoNew.setSubtitle(bean.getSubtitle());
        //商品主图
        if (StringUtil.notBlank(bean.getImgUrl())) {
            infoNew.setImgUrl(bean.getImgUrl());
        }
        //价格
        infoNew.setPrice(bean.getPrice());
        //会员级别 数字越大级别越大
        infoNew.setLevel(bean.getLevel());
        //会员卡使用说明
        infoNew.setContent(bean.getContent());
        //会员卡简介
        infoNew.setIntroduction(bean.getIntroduction());
        //库存是否无限 0—是 1-否
        if (Objects.nonNull(bean.getIsInfinite())) {
            infoNew.setIsInfinite(bean.getIsInfinite());
        }
        //库存数量
        infoNew.setInventoryCount(bean.getInventoryCount());
        // 有效时长单位，1-周，2-月，3-季，4-年
        if (Objects.nonNull(bean.getValidTimeUnit())) {
            infoNew.setValidTimeUnit(bean.getValidTimeUnit());
        }
        // 有效时长(周期)，表示几周，几个月
        if (Objects.nonNull(bean.getValidTime())) {
            infoNew.setValidTime(bean.getValidTime());
        }
        //有效期开始日期
        infoNew.setStartTime(bean.getStartTime());
        //有效期结束日期
        infoNew.setEndTime(bean.getEndTime());
        //是否启用 0-是 1-否
        infoNew.setIsEnabled(bean.getIsEnabled());
        //备注
        infoNew.setRemark(bean.getRemark());
        //备用字段
        infoNew.setExtraParams1(bean.getExtraParams1());
        // 是否免费领取 0-是 1-否
        infoNew.setIsFree(bean.getIsFree());
        // 是否免费领取 0-是 1-否
        infoNew.setGetNum(bean.getGetNum());
        //删除标识 0:未删除 1:删除
        //infoNew.setIsDelete(DeleteType.ISDELETE.getCode());

        //更新者状态
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateTime(System.currentTimeMillis());
        return this.service.updateById(infoNew);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 16:17
     * @param null
     * @return null
     * @update
     * @updateTime
     * 删除
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        MemberCard infoDel = null;
        boolean flag = true;
        //每个id已逗号分割
        String[] idsArr = ids.split(",");
        //遍历id并判断是否为空
        for (String id : idsArr) {
            MemberCard info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new MemberCard();
            //设置删除状态
            infoDel.setId(info.getId());
            infoDel.setDeleteOperatorId(operUserId);
            infoDel.setDeleteOperatorName(info.getDeleteOperatorName());
            infoDel.setDeleteTime(System.currentTimeMillis());
            //设置状态为删除
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
            if (!flag) {
                //修改失败
                throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                        BasePojectExceptionCodeEnum.SelectFail.msg);
            }
        }
        return flag;
    }


    @Override
    public List<MemberCardVO> findAll(MemberCardQuery bean) {
        return null;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 16:21
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    @Override
    public PageResult<MemberCardVO> getList(MemberCardQuery params) {
        QueryWrapper<MemberCard> queryWrapper = new QueryWrapper<MemberCard>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        if (Objects.nonNull(params.getIsFree())) {
            queryWrapper.eq("is_free", params.getIsFree());
        }
        if (Objects.nonNull(params.getIsEnabled())) {
            queryWrapper.eq("is_enabled", params.getIsEnabled());
        }
        queryWrapper.orderByDesc("create_time");
        Page<MemberCardVO> pageVo = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<MemberCardVO> page = this.service.getLists(pageVo, queryWrapper);
        page.getRecords().forEach(item->{
            if (StringUtil.notBlank(item.getImgUrl())) {
                //拼接主图长地址
                item.setImgUrlPath(FileUploadUtil.getImgRootPath() + item.getImgUrl());
            }
        });
        return new PageResult<>(page);
    }


    /**
     * 会员卡多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean updateMultiIsEnabled(String operUserId, BaseParams params) {
        boolean flag = true;
        String ids = params.getIds();
        String[] idArr = ids.split(",");
        MemberCard infoNew = null;
        for (String id : idArr) {
            MemberCard info = this.get(id.trim());
            if (Objects.isNull(info)) {
                continue;
            }
            infoNew = new MemberCard();
            infoNew.setId(id.trim());
            infoNew.setIsEnabled(params.getIsEnabled());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
            infoNew.setUpdateTime(System.currentTimeMillis());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        // 判断 菜单是否删除成功 如果删除成功直接抛出异常
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code,
                    BasePojectExceptionCodeEnum.UpdateFail.msg);
        }
        return flag;
    }


    /**
     * 分页列表
     *
     * @param params
     * @return
     */
    @Override
    public PageResult<MemberCardVO> getWebList(MemberCardQuery params) {
        QueryWrapper<MemberCard> queryWrapper = new QueryWrapper<MemberCard>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        if (Objects.nonNull(params.getIsEnabled())) {
            queryWrapper.eq("is_enabled", params.getIsEnabled());
        }
        queryWrapper.orderByDesc("create_time");
        Page<MemberCardVO> pageVo = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<MemberCardVO> page = this.service.getLists(pageVo, queryWrapper);
        //有查询结果
        if (null != page.getRecords() && page.getRecords().size() > 0) {
            MemberCardConfigQuery cardConfigParams = null;
            for (MemberCardVO info : page.getRecords()) {
                if (StringUtil.notBlank(info.getImgUrl())) {
                    //拼接主图长地址
                    info.setImgUrlPath(FileUploadUtil.getImgRootPath() + info.getImgUrl());
                }

                cardConfigParams = new MemberCardConfigQuery();
                cardConfigParams.setIsEnabled(0);
                cardConfigParams.setCardId(info.getId());
                List<MemberCardConfigVO> MemberCardConfigVoList = this.memberCardConfigServuceBLL.findAll(cardConfigParams);

                if (null != MemberCardConfigVoList && MemberCardConfigVoList.size() > 0) {
                    for (MemberCardConfigVO vo : MemberCardConfigVoList) {
                        if (StringUtil.notBlank(vo.getImgUrl())) {
                            //拼接主图长地址
                            vo.setImgUrlPath(FileUploadUtil.getImgRootPath() + vo.getImgUrl());
                        }
                    }
                }
                info.setMemberCardConfigVoList(MemberCardConfigVoList);
            }
        }
        return new PageResult<>(page);
    }

    /**
     * 详情
     *
     * @param id
     * @return
     */
    @Override
    public MemberCardVO getWebInfo(String id) {
        MemberCardVO info = this.service.getInfo(id);
        if (Objects.nonNull(info)) {
            if (StringUtil.notBlank(info.getImgUrl())) {
                //拼接主图长地址
                info.setImgUrlPath(FileUploadUtil.getImgRootPath() + info.getImgUrl());
            }
        }
        MemberCardConfigQuery params = new MemberCardConfigQuery();
        params.setIsEnabled(0);
        params.setCardId(info.getId());
        List<MemberCardConfigVO> MemberCardConfigVoList = this.memberCardConfigServuceBLL.findAll(params);
        if (null != MemberCardConfigVoList && MemberCardConfigVoList.size() > 0) {
            for (MemberCardConfigVO vo : MemberCardConfigVoList) {
                if (StringUtil.notBlank(vo.getImgUrl())) {
                    //拼接主图长地址
                    vo.setImgUrlPath(FileUploadUtil.getImgRootPath() + vo.getImgUrl());
                }
            }
        }
        info.setMemberCardConfigVoList(MemberCardConfigVoList);
        return info;
    }
}