package com.elite.learn.cardManage.vo;

import lombok.Data;

/**
 * @Title :WechatPayDTO.java
 * @Package: com.elite.learn.base.order.common.entity.dto
 * @Description:
 * @author: leroy
 * @data: 2021/3/11 0:03
 */
@Data
public class WechatPayVO {
    /**
     * 小程序appid
     */
    private String appId;

    /**
     * 时间戳
     */
    private String timeStamp;

    /**
     * 随机字符串
     */
    private String nonceStr;
    private String pay_package;
    private String signType;
    private String paySign;
}