package com.elite.learn.cardManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.cardManage.entity.LogUserMemberCard;
import com.elite.learn.cardManage.mapper.LogUserMemberCardMapper;
import com.elite.learn.cardManage.service.ILogUserMemberCardService;
import com.elite.learn.cardManage.vo.LogUserMemberCardVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 使用会员卡权益日志表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
@Service
public class LogUserMemberCardServiceImpl extends ServiceImpl<LogUserMemberCardMapper, LogUserMemberCard> implements ILogUserMemberCardService {


    /**
     * @param pageVo
     * @return null
     * @author: leroy
     * @date 2021/11/29 17:53
     * @update
     * @updateTime 分页
     */
    @Override
    public IPage<LogUserMemberCardVO> getLists(Page<LogUserMemberCardVO> pageVo, @Param(Constants.WRAPPER) Wrapper<LogUserMemberCard> queryWrapper) {
        pageVo.setRecords(this.baseMapper.getLists(pageVo, queryWrapper));
        return pageVo;
    }
}
