package com.elite.learn.cardManage.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 会员卡信息表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Data
public class MemberCardDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * 会员卡名称
     */
    @NotNull(message = "会员卡名称，参数为空")
    private String name;

    /**
     * 会员卡副标题
     */
    private String subtitle;

    /**
     * 商品主图
     */
    @NotNull(message = "会员卡主图，参数为空")
    private String imgUrl;

    /**
     * 价格
     */
   // @NotNull(message = "会员卡价格，参数为空")
    private BigDecimal price;

    /**
     * 会员级别 数字越大级别越大
     */
    private Integer level;

    /**
     * 会员卡使用说明
     */
    private String content;

    /**
     * 会员卡简介
     */
    private String introduction;

    /**
     * 库存是否无限 0—是 1-否
     */
    @NotNull(message = "库存设置，参数为空")
    private Integer isInfinite;

    /**
     * 库存数量
     */
    private Integer inventoryCount;

    /**
     * 有效时长单位，1-周，2-月，3-季，4-年
     */
    @NotNull(message = "有效时长单位，参数为空")
    private Integer validTimeUnit;

    /**
     * 有效时长(周期)，表示几周，几个月
     */
    @NotNull(message = "有效时长，参数为空")
    private Integer validTime;

    /**
     * 有效期开始日期
     */
    private Date startTime;

    /**
     * 有效期结束日期
     */
    private Date endTime;

    /**
     * 是否启用 0-是 1-否
     */
    private Integer isEnabled;

    /**
     * 备注
     */
    private String remark;

    /**
     * 备注
     */
    private String memo;

    /**
     * 创建者ID
     */
    private String createOperatorId;



    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 创建者ID
     */
    private String createOperatorName;



    /**
     * 更新者ID
     */
    private String updateOperatorName;

    /**
     * 备用字段
     */
    private String extraParams1;

    /**
     * 是否免费领取 0-是 1-否
     */
    private Integer isFree;

    /**
     * 领取次数
     */
    private Integer getNum;
}
