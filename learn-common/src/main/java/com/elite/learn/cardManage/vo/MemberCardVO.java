package com.elite.learn.cardManage.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 会员卡信息表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Data
public class MemberCardVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 会员卡名称
     */
    private String name;

    /**
     * 会员卡副标题
     */
    private String subtitle;

    /**
     * 商品主图
     */
    private String imgUrl;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 会员级别 数字越大级别越大
     */
    private Integer level;

    /**
     * 会员卡使用说明
     */
    private String content;

    /**
     * 会员卡简介
     */
    private String introduction;

    /**
     * 库存是否无限 0—是 1-否
     */
    private Integer isInfinite;

    /**
     * 库存数量
     */
    private Integer inventoryCount;

    /**
     * 有效时长单位，1-周，2-月，3-季，4-年
     */
    private Integer validTimeUnit;

    /**
     * 有效时长(周期)，表示几周，几个月
     */
    private Integer validTime;

    /**
     * 有效期开始日期
     */
    private Date startTime;

    /**
     * 有效期结束日期
     */
    private Date endTime;

    /**
     * 是否启用 0-是 1-否
     */
    private Integer isEnabled;

    /**
     * 备注
     */
    private String remark;

    /**
     * 备注
     */
    private String memo;

    /**
     * 添加时间
     */
    private Long createTime;



    /**
     * 更新时间
     */
    private Long updateTime;



    /**
     * 备用字段
     */
    private String extraParams1;


    /**
        * 封面图长路径
     */
    private String imgUrlPath;
    /**
     * 配置信息
     */
    private   List<MemberCardConfigVO>  memberCardConfigVoList;
    /**
     * 是否免费领取 0-是 1-否
     */
    private Integer isFree;

    /**
     * 领取次数
     */
    private Integer getNum;
}
