package com.elite.learn.cardManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.cardManage.entity.OrderCard;
import com.elite.learn.cardManage.vo.OrderCardVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 订单表---会员卡订单表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
public interface IOrderCardService extends IService<OrderCard> {


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/11/29 15:33
     * @update
     * @updateTime 详情
     */
    OrderCardVO getInfo(String id);


    /**
     * @param pageVo
     * @return null
     * @author: leroy
     * @date 2021/11/29 16:24
     * @update
     * @updateTime 分页
     */
    IPage<OrderCardVO> getLists(Page<OrderCardVO> pageVo, @Param(Constants.WRAPPER) Wrapper<OrderCard> queryWrapper);

}
