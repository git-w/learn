package com.elite.learn.cardManage.mapper;

import com.elite.learn.cardManage.entity.UserMemberCardRights;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户注册会员权益表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
public interface UserMemberCardRightsMapper extends BaseMapper<UserMemberCardRights> {

}
