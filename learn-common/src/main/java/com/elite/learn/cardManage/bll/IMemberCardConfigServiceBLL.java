package com.elite.learn.cardManage.bll;

import com.elite.learn.cardManage.dto.MemberCardConfigDTO;
import com.elite.learn.cardManage.entity.MemberCardConfig;
import com.elite.learn.cardManage.query.MemberCardConfigQuery;
import com.elite.learn.cardManage.vo.MemberCardConfigVO;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.ICommonServiceBLL;

public interface IMemberCardConfigServiceBLL extends ICommonServiceBLL<MemberCardConfig, MemberCardConfigDTO, MemberCardConfigVO, MemberCardConfigQuery> {


    /**
     * 多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateMultiIsEnabled(String operUserId, BaseParams params);
}
