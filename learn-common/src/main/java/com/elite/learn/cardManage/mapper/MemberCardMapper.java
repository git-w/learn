package com.elite.learn.cardManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.entity.MemberCard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.cardManage.vo.MemberCardVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 会员卡信息表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
public interface MemberCardMapper extends BaseMapper<MemberCard> {


    /**
     *
     * @author: leroy
     * @date 2021/11/29 17:52
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     *
     */
    MemberCardVO getInfo(String id);


    /**
     *
     * @author: leroy
     * @date 2021/11/29 17:53
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<MemberCardVO> getLists(Page<MemberCardVO> pageVo, @Param(Constants.WRAPPER)Wrapper<MemberCard> queryWrapper);
}
