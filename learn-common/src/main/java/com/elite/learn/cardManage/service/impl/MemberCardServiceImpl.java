package com.elite.learn.cardManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.entity.MemberCard;
import com.elite.learn.cardManage.mapper.MemberCardMapper;
import com.elite.learn.cardManage.service.IMemberCardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.cardManage.vo.MemberCardVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员卡信息表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Service
public class MemberCardServiceImpl extends ServiceImpl<MemberCardMapper, MemberCard> implements IMemberCardService {


    /*
     *
     * @author: leroy
     * @date 2021/11/29 17:53
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public MemberCardVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 17:53
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    @Override
    public IPage<MemberCardVO> getLists(Page<MemberCardVO> pageVo, @Param(Constants.WRAPPER) Wrapper<MemberCard> queryWrapper) {
        pageVo.setRecords(this.baseMapper.getLists(pageVo, queryWrapper));
        return pageVo;
    }
}
