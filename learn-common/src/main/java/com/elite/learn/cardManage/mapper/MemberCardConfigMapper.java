package com.elite.learn.cardManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.entity.MemberCardConfig;
import com.elite.learn.cardManage.vo.MemberCardConfigVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 会员卡配置表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
public interface MemberCardConfigMapper extends BaseMapper<MemberCardConfig> {


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/11/29 17:54
     * @update
     * @updateTime 详情
     */
    MemberCardConfigVO getInfo(String id);


    /**
     * @return null
     * @author: leroy
     * @date 2021/11/29 18:02
     * @update
     * @updateTime 分页
     */
    List<MemberCardConfigVO> findAll(@Param(Constants.WRAPPER) Wrapper<MemberCardConfig> queryWrapper);


    /**
     * @param pageVo
     * @return null
     * @author: leroy
     * @date 2021/11/29 18:02
     * @update
     * @updateTime 分页
     */
    List<MemberCardConfigVO> getLists(Page<MemberCardConfigVO> pageVo, @Param(Constants.WRAPPER) Wrapper<MemberCardConfig> queryWrapper);
}
