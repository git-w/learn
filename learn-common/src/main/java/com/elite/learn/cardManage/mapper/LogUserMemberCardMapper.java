package com.elite.learn.cardManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.entity.LogUserMemberCard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.cardManage.entity.OrderCard;
import com.elite.learn.cardManage.vo.LogUserMemberCardVO;
import com.elite.learn.cardManage.vo.OrderCardVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 使用会员卡权益日志表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
public interface LogUserMemberCardMapper extends BaseMapper<LogUserMemberCard> {


    /**
     * @param pageVo
     * @return null
     * @author: leroy
     * @date 2021/11/29 17:53
     * @update
     * @updateTime 分页
     */
    List<LogUserMemberCardVO> getLists(Page<LogUserMemberCardVO> pageVo, @Param(Constants.WRAPPER) Wrapper<LogUserMemberCard> queryWrapper);

}
