package com.elite.learn.cardManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.entity.OrderCard;
import com.elite.learn.cardManage.vo.OrderCardVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 订单表---会员卡订单表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
public interface OrderCardMapper extends BaseMapper<OrderCard> {


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/11/29 17:52
     * @update
     * @updateTime 详情
     */
    OrderCardVO getInfo(String id);


    /**
     * @param pageVo
     * @return null
     * @author: leroy
     * @date 2021/11/29 17:53
     * @update
     * @updateTime 分页
     */
    List<OrderCardVO> getLists(Page<OrderCardVO> pageVo, @Param(Constants.WRAPPER) Wrapper<OrderCard> queryWrapper);

}
