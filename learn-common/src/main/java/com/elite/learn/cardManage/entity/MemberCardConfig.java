package com.elite.learn.cardManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员卡配置表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_member_card_config")
public class MemberCardConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 关联会员卡ID
     */
    private String cardId;

    /**
     * 关联类型 0-红娘牵线
     */
    private Integer isType;

    /**
     * 关联ID
     */
    private String othersId;

    /**
     * 图片地址
     */
    private String imgUrl;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 间隔天数
     */
    private Integer timeInterval;

    /**
     * 有效天数
     */
    private Integer validTime;

    /**
     * 是否启用 0-是 1-否
     */
    private Integer isEnabled;

    /**
     * 备注
     */
    private String remark;

    /**
     * 备注
     */
    private String memo;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 删除标识 0:未删除 1:删除
     */
    private Integer isDelete;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;


}
