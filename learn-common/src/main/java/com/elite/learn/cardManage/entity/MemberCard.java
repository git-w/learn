package com.elite.learn.cardManage.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员卡信息表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_member_card")
public class MemberCard implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 会员卡名称
     */
    private String name;

    /**
     * 会员卡副标题
     */
    private String subtitle;

    /**
     * 商品主图
     */
    private String imgUrl;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 会员级别 数字越大级别越大
     */
    private Integer level;

    /**
     * 会员卡使用说明
     */
    private String content;

    /**
     * 会员卡简介
     */
    private String introduction;

    /**
     * 库存是否无限 0—是 1-否
     */
    private Integer isInfinite;

    /**
     * 库存数量
     */
    private Integer inventoryCount;

    /**
     * 有效时长单位，1-周，2-月，3-季，4-年
     */
    private Integer validTimeUnit;

    /**
     * 有效时长(周期)，表示几周，几个月
     */
    private Integer validTime;

    /**
     * 有效期开始日期
     */
    private Date startTime;

    /**
     * 有效期结束日期
     */
    private Date endTime;

    /**
     * 是否启用 0-是 1-否
     */
    private Integer isEnabled;

    /**
     * 备注
     */
    private String remark;

    /**
     * 备注
     */
    private String memo;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 删除标识 0:未删除 1:删除
     */
    private Integer isDelete;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;

    /**
     * 备用字段
     */
    private String extraParams1;
    /**
     * 是否免费领取 0-是 1-否
     */
    private Integer isFree;

    /**
     * 领取次数
     */
    private Integer getNum;

}
