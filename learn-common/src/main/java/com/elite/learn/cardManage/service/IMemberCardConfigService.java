package com.elite.learn.cardManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.entity.MemberCardConfig;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.cardManage.vo.MemberCardConfigVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 会员卡配置表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
public interface IMemberCardConfigService extends IService<MemberCardConfig> {


    /*
     *
     * @author: leroy
     * @date 2021/11/29 14:34
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    MemberCardConfigVO getInfo(String id);


    /*
     *
     * @author: leroy
     * @date 2021/11/29 15:20
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    IPage<MemberCardConfigVO> getLists(Page<MemberCardConfigVO> pageVo, @Param(Constants.WRAPPER) Wrapper<MemberCardConfig> queryWrapper);


    /**
     *
     * @author: leroy
     * @date 2021/11/29 15:20
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<MemberCardConfigVO> findAll(@Param(Constants.WRAPPER) Wrapper<MemberCardConfig> queryWrapper);

}
