package com.elite.learn.cardManage.bll;

import com.elite.learn.cardManage.dto.OrderCardDTO;
import com.elite.learn.cardManage.entity.OrderCard;
import com.elite.learn.cardManage.query.OrderCardQuery;
import com.elite.learn.cardManage.vo.OrderCardVO;
import com.elite.learn.common.core.service.ICommonServiceBLL;

import java.math.BigDecimal;


public interface IOrderCardServiceBLL extends ICommonServiceBLL<OrderCard, OrderCardDTO, OrderCardVO, OrderCardQuery> {


    /**
     * <p>
     * 支付前检查库存等
     * </p>
     *
     * @param operUserId
     * @param orderNum
     * @return java.math.BigDecimal
     * @author Winder
     * @date 2021/8/19 下午3:01
     */
    BigDecimal getCost(String operUserId, String orderNum);


    /**
     * <p>
     * 订单支付成功减去库存
     * </p>
     *
     * @param operUserId
     * @param orderNum
     * @return boolean
     * @author: leroy
     * @date 2021/6/24 10:49
     */
    boolean updatePaySuccessOrderStatus(String operUserId, String orderNum);
}
