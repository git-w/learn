package com.elite.learn.cardManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 会员卡配置表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Data
public class MemberCardConfigQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 关联会员卡ID
     */
    private String cardId;
    /**
     * 是否启用 0-是 1-否
     */
    private Integer isEnabled;


}
