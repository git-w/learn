package com.elite.learn.cardManage.dto;

import lombok.Data;

/**
 * @Title :OrderPayParams.java
 * @Package: com.elite.learn.base.order.common.entity.params
 * @Description:
 * @author: leroy
 * @data: 2021/3/10 23:51
 */
@Data
public class OrderPayDTO {
    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 订单类型 0-店铺
     */
    private Integer orderType;

    /**
     * 小程序用户id
     */
    private String openId;
}
