package com.elite.learn.cardManage.service.impl;

import com.elite.learn.cardManage.entity.UserMemberCard;
import com.elite.learn.cardManage.mapper.UserMemberCardMapper;
import com.elite.learn.cardManage.service.IUserMemberCardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员卡和用户中间表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
@Service
public class UserMemberCardServiceImpl extends ServiceImpl<UserMemberCardMapper, UserMemberCard> implements IUserMemberCardService {

}
