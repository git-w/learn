package com.elite.learn.cardManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.entity.MemberCard;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.cardManage.vo.MemberCardVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 会员卡信息表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
public interface IMemberCardService extends IService<MemberCard> {


    /*
     *
     * @author: leroy
     * @date 2021/11/29 15:33
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    MemberCardVO getInfo(String id);


    /*
     *
     * @author: leroy
     * @date 2021/11/29 16:24
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    IPage<MemberCardVO> getLists(Page<MemberCardVO> pageVo, @Param(Constants.WRAPPER) Wrapper<MemberCard> queryWrapper);
}
