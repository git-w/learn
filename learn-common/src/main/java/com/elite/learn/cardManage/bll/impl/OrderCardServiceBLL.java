package com.elite.learn.cardManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.bll.IMemberCardServiceBLL;
import com.elite.learn.cardManage.bll.IOrderCardServiceBLL;
import com.elite.learn.cardManage.contants.CardManageExceptionCodeEnum;
import com.elite.learn.cardManage.dto.OrderCardDTO;
import com.elite.learn.cardManage.entity.*;
import com.elite.learn.cardManage.query.OrderCardQuery;
import com.elite.learn.cardManage.service.*;
import com.elite.learn.cardManage.vo.MemberCardConfigVO;
import com.elite.learn.cardManage.vo.OrderCardVO;
import com.elite.learn.cardManage.vo.UserMemberCardRightsVO;
import com.elite.learn.cardManage.vo.UserMemberCardVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import com.elite.learn.common.core.type.OrderType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.date.DateCalculationUtil;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.generate.order.GetOrderCodeUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.messageManage.bll.IUserMessageServiceBLL;
import com.elite.learn.userManage.entity.User;
import com.elite.learn.userManage.service.IUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 会员卡信息表
 * @Title: InfoOrderCardServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/29 14:12
 */
@Service
public class OrderCardServiceBLL implements IOrderCardServiceBLL {

    @Resource
    private IOrderCardService service;
    //会员卡和用户中间表
    @Resource
    private IUserMemberCardService userMemberCardService;
    //用户注册会员权益表
    @Resource
    private IUserMemberCardRightsService userMemberCardRightsService;
    //会员卡信息表
    @Resource
    private IMemberCardService memberCardService;
    //会员卡配置表
    @Resource
    private IMemberCardConfigService memberCardConfigService;
    //用户
    @Resource
    private IUserService userService;
    //会员卡信息表
    @Resource
    private IMemberCardServiceBLL memberCardServiceBLL;
    //系统消息-用户消息表
    @Resource
    private IUserMessageServiceBLL userMessageServiceBLL;


    /*
     *
     * @author: leroy
     * @date 2021/11/29 15:30
     * @param null
     * @return null
     * @update
     * @updateTime
     * 获取对象id
     */
    @Override
    public OrderCard get(String id) {
        return this.service.getById(id);
    }


    /**
     * 订单详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public OrderCardVO getInfo(String id) {
        OrderCardVO vo = this.service.getInfo(id);
        //订单号
        QueryWrapper<UserMemberCard> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id", id);
        UserMemberCard userMemberCardInfo = this.userMemberCardService.getOne(wrapper);
        UserMemberCardVO userCardVo = new UserMemberCardVO();
        BeanUtils.copyProperties(userMemberCardInfo, userCardVo);
        //存入会员卡的信息
        userCardVo.setMemberCardVO(this.memberCardServiceBLL.getInfo(userCardVo.getCardId()));
        QueryWrapper<UserMemberCardRights> wrapperRights = new QueryWrapper<>();
        wrapperRights.eq("mid_user_card_id", userMemberCardInfo.getId());
        List<UserMemberCardRights> list = this.userMemberCardRightsService.list(wrapperRights);
        List<UserMemberCardRightsVO> rightsListVo = new ArrayList<>();
        UserMemberCardRightsVO rightsVO = null;
        if (Objects.nonNull(list) && list.size() > 0) {
            for (UserMemberCardRights rightsInfo : list) {
                rightsVO = new UserMemberCardRightsVO();
                BeanUtils.copyProperties(rightsInfo, rightsVO);
                MemberCardConfigVO ConfigVo = this.memberCardConfigService.getInfo(rightsInfo.getConfigId());
                if (StringUtil.notBlank(ConfigVo.getImgUrl())) {
                    //拼接主图长地址
                    ConfigVo.setImgUrlPath(FileUploadUtil.getImgRootPath() + ConfigVo.getImgUrl());
                }
                rightsVO.setConfigVo(ConfigVo);
                rightsListVo.add(rightsVO);
            }
        }
        userCardVo.setRightsVOList(rightsListVo);
        vo.setUserMemberCardVO(userCardVo);
        return vo;
    }


    /**
     * 创建订单
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public String save(String operUserId, OrderCardDTO bean) {
        //创建实体
        OrderCard info = new OrderCard();
        //创建用户生成的id
        info.setId(IdUtils.simpleUUID());
        String orderNum = GetOrderCodeUtil.getOrderCodeByStr("VIP");//订单号
        info.setOrderNum(orderNum);//订单号
        //0-取消订单 1-未支付、2-支付/待出库、3-出库/待发货、4-已发货 5-完成 6-申请退款 7-退款中 8-退款完成 9-超时未支付取消的
        info.setOrderStatus(1);
        //用户ID
        info.setUserId(operUserId);
        //查看会员卡列表
        MemberCard memberCardInfo = this.memberCardService.getById(bean.getCardId());
        // 查看领取次数
        if (memberCardInfo.getIsFree() == 0) {
            QueryWrapper<UserMemberCard> wrapper = new QueryWrapper<>();
            wrapper.eq("card_id", memberCardInfo.getId());
            wrapper.eq("create_operator_id", operUserId);
            int count = this.userMemberCardService.count(wrapper);
            if (count >= memberCardInfo.getGetNum()) {
                throw new CommonException(CardManageExceptionCodeEnum.NotUserGetBumFail.code,
                        CardManageExceptionCodeEnum.NotUserGetBumFail.msg);
            }
        }
        // 会员卡id查询失败
        if (Objects.isNull(memberCardInfo)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        // 如果库存不是无限的话
        if (memberCardInfo.getIsInfinite() == 1) {
            if (memberCardInfo.getInventoryCount() <= 0) {
                throw new CommonException(CardManageExceptionCodeEnum.InventoryCountInsufficientFail.code,
                        CardManageExceptionCodeEnum.InventoryCountInsufficientFail.msg);
            }
        }
        // 用户购买会员卡信息
        UserMemberCard userMemberCardInfo = new UserMemberCard();
        //创建用户生成的id
        String userMemberCardId = IdUtils.simpleUUID();
        userMemberCardInfo.setId(userMemberCardId);
        userMemberCardInfo.setCardId(memberCardInfo.getId());
        String VIPNum = GetOrderCodeUtil.getOrderCodeByStr("VIP");//订单号
        userMemberCardInfo.setCardNum(VIPNum);
        userMemberCardInfo.setUserId(operUserId);
        userMemberCardInfo.setOrderId(info.getId());
        userMemberCardInfo.setOrderNum(info.getOrderNum());
        //会员卡状态 0-是 1-否
        userMemberCardInfo.setIsStatus(1);
        //添加时间
        userMemberCardInfo.setCreateTime(System.currentTimeMillis());
        //创建者ID
        userMemberCardInfo.setCreateOperatorId(operUserId);
        //有效时长单位，1-周，2-月，3-季，4-年
        Integer ValidTimeUnit = 0;
        if (memberCardInfo.getValidTimeUnit() == 5) {
            ValidTimeUnit = 1;
        } else if (memberCardInfo.getValidTimeUnit() == 1) {
            ValidTimeUnit = 7;
        } else if (memberCardInfo.getValidTimeUnit() == 2) {
            ValidTimeUnit = 30;
        } else if (memberCardInfo.getValidTimeUnit() == 3) {
            ValidTimeUnit = 90;
        } else if (memberCardInfo.getValidTimeUnit() == 4) {
            ValidTimeUnit = 365;
        }
        //有效时长(周期)，表示几周，几个月
        Integer ValidTime = memberCardInfo.getValidTime() * ValidTimeUnit;
        //开始时间
        userMemberCardInfo.setStartTime(System.currentTimeMillis());
        //结束时间
        userMemberCardInfo.setEndTime(DateCalculationUtil.additionDate(System.currentTimeMillis(), ValidTime));
        this.userMemberCardService.save(userMemberCardInfo);
        // 查看会员卡配置
        QueryWrapper<MemberCardConfig> queryWrapper = new QueryWrapper<MemberCardConfig>();
        queryWrapper.eq("card_id", bean.getCardId());
        queryWrapper.eq("is_enabled", 0);
        //查看会员卡列表
        List<MemberCardConfig> memberCardList = this.memberCardConfigService.list(queryWrapper);
        UserMemberCardRights userMemberCardRightsInfo = null;
        for (MemberCardConfig cardConfigInfo : memberCardList) {
            userMemberCardRightsInfo = new UserMemberCardRights();
            userMemberCardRightsInfo.setId(IdUtils.simpleUUID());
            userMemberCardRightsInfo.setMidUserCardId(userMemberCardId);
            userMemberCardRightsInfo.setCardId(cardConfigInfo.getCardId());
            userMemberCardRightsInfo.setConfigId(cardConfigInfo.getId());
            userMemberCardRightsInfo.setUserId(operUserId);
            userMemberCardRightsInfo.setCount(cardConfigInfo.getCount());
            userMemberCardRightsInfo.setValidDays(ValidTime);
            userMemberCardRightsInfo.setStartTime(userMemberCardInfo.getStartTime());
            userMemberCardRightsInfo.setEndTime(userMemberCardInfo.getEndTime());
            userMemberCardRightsInfo.setTotalCount(cardConfigInfo.getCount());
            userMemberCardRightsInfo.setIsType(cardConfigInfo.getIsType());
            //添加时间
            userMemberCardRightsInfo.setCreateTime(System.currentTimeMillis());
            //创建者ID
            userMemberCardRightsInfo.setCreateOperatorId(operUserId);
            this.userMemberCardRightsService.save(userMemberCardRightsInfo);
        }

        //收款总金额
        info.setTotalMoney(memberCardInfo.getPrice());
        //收款总金额
        info.setRealCost(memberCardInfo.getPrice());
        //支付状态，0未支付，1已支付，2已支付定金
        info.setPayStatus(0);
        info.setOrderType("0");
        //删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //添加时间
        info.setCreateTime(System.currentTimeMillis());
        //创建者ID
        info.setCreateOperatorId(operUserId);
        boolean flag = this.service.save(info);
        //是否免费领取 0-是 1-否
        if (flag && memberCardInfo.getIsFree() == 0) {
            this.updatePaySuccessOrderStatus(operUserId, orderNum);

        }
        return flag ? orderNum : null;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 16:00
     * @param null
     * @return null
     * @update
     * @updateTime
     * 更新
     */
    @Override
    public boolean update(String operUserId, OrderCardDTO bean) {

        // 判断通过ID获取的对象是否为空
        OrderCard info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体
        OrderCard infoNew = new OrderCard();
        //创建用户生成的id
        infoNew.setId(bean.getId());
    /*    //会员卡名称
        if (StringUtils.isNotEmpty(bean.getName())) {
            infoNew.setName(bean.getName());
        }
        //会员卡副标题
        if (StringUtil.notBlank(bean.getSubtitle())) {
            infoNew.setSubtitle(bean.getSubtitle());
        }
        //商品主图
        if (StringUtil.notBlank(bean.getImgUrl())) {
            infoNew.setImgUrl(bean.getImgUrl());
        }
        //价格
        if (Objects.nonNull((bean.getPrice()))) {
            infoNew.setPrice(bean.getPrice());
        }
        //会员级别 数字越大级别越大
        if (Objects.nonNull(bean.getLevel())) {
            infoNew.setLevel(bean.getLevel());
        }
        //会员卡使用说明
        infoNew.setContent(bean.getContent());
        //会员卡简介
        infoNew.setIntroduction(bean.getIntroduction());
        //库存是否无限 0—是 1-否
        if (Objects.nonNull(bean.getIsInfinite())) {
            infoNew.setIsInfinite(bean.getIsInfinite());
        }
        //库存数量
        if (Objects.nonNull(bean.getInventoryCount())) {
            infoNew.setInventoryCount(bean.getInventoryCount());
        }
        // 有效时长单位，1-周，2-月，3-季，4-年
        if (Objects.nonNull(bean.getValidTimeUnit())) {
            infoNew.setValidTimeUnit(bean.getValidTimeUnit());
        }
        // 有效时长(周期)，表示几周，几个月
        if (Objects.nonNull(bean.getValidTime())) {
            infoNew.setValidTime(bean.getValidTime());
        }
        //有效期开始日期
        if (Objects.nonNull(bean.getStartTime())) {
            infoNew.setStartTime(bean.getStartTime());
        }
        //有效期结束日期
        if (Objects.nonNull(bean.getEndTime())) {
            infoNew.setEndTime(bean.getEndTime());
        }
        //是否启用 0-是 1-否
        if (Objects.nonNull(bean.getIsEnabled())) {
            infoNew.setIsEnabled(bean.getIsEnabled());
        }
        //备注
        infoNew.setRemark(bean.getRemark());
        //备用字段
        infoNew.setExtraParams1(bean.getExtraParams1());
*/
        //更新者状态
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateTime(System.currentTimeMillis());
        return this.service.updateById(infoNew);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 16:17
     * @param null
     * @return null
     * @update
     * @updateTime
     * 删除
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        OrderCard infoDel = null;
        boolean flag = true;
        //每个id已逗号分割
        String[] idsArr = ids.split(",");
        //遍历id并判断是否为空
        for (String id : idsArr) {
            OrderCard info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new OrderCard();
            //设置删除状态
            infoDel.setId(info.getId());
            infoDel.setDeleteOperatorId(operUserId);
            infoDel.setDeleteOperatorName(info.getDeleteOperatorName());
            infoDel.setDeleteTime(System.currentTimeMillis());
            //设置状态为删除
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());

            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
            if (!flag) {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code,
                        BasePojectExceptionCodeEnum.UpdateFail.msg);
            }
        }
        return flag;
    }


    @Override
    public List<OrderCardVO> findAll(OrderCardQuery bean) {
        return null;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 16:21
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    @Override
    public PageResult<OrderCardVO> getList(OrderCardQuery params) {
        QueryWrapper<OrderCard> queryWrapper = new QueryWrapper<OrderCard>();
        queryWrapper.eq("tioc.is_delete", DeleteType.NOTDELETE.getCode());
        // 0-取消订单 1-未支付、2-支付/待出库、3-出库/待发货、4-已发货 5-完成 6-申请退款 7-退款中 8-退款完成 9-超时未支付取消的
        if (Objects.nonNull(params.getOrderStatus())) {
            queryWrapper.eq("tioc.order_status", params.getOrderStatus());
        }
        // 订单号
        if (Objects.nonNull(params.getOrderNum())) {
            queryWrapper.like("tioc.order_num", params.getOrderNum());
        }
        queryWrapper.orderByDesc("tioc.create_time");
        Page<OrderCardVO> pageVo = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<OrderCardVO> page = this.service.getLists(pageVo, queryWrapper);
        if (page.getRecords().size() > 0) {
            for (OrderCardVO info : page.getRecords()) {
                //订单号
                QueryWrapper<UserMemberCard> wrapper = new QueryWrapper<>();
                wrapper.eq("order_id", info.getId());
                UserMemberCard userMemberCardInfo = this.userMemberCardService.getOne(wrapper);
                UserMemberCardVO userCardVo = new UserMemberCardVO();
                BeanUtils.copyProperties(userMemberCardInfo, userCardVo);
                //存入会员卡的信息
                userCardVo.setMemberCardVO(this.memberCardServiceBLL.getInfo(userCardVo.getCardId()));
                QueryWrapper<UserMemberCardRights> wrapperRights = new QueryWrapper<>();
                wrapperRights.eq("mid_user_card_id", userMemberCardInfo.getId());
                List<UserMemberCardRights> list = this.userMemberCardRightsService.list(wrapperRights);

                List<UserMemberCardRightsVO> rightsListVo = new ArrayList<>();
                UserMemberCardRightsVO rightsVO = null;
                if (Objects.nonNull(list) && list.size() > 0) {
                    for (UserMemberCardRights rightsInfo : list) {
                        rightsVO = new UserMemberCardRightsVO();
                        BeanUtils.copyProperties(rightsInfo, rightsVO);
                        MemberCardConfigVO ConfigVo = this.memberCardConfigService.getInfo(rightsInfo.getConfigId());
                        if (Objects.nonNull(ConfigVo)) {
                            if (StringUtil.notBlank(ConfigVo.getImgUrl())) {
                                //拼接主图长地址
                                ConfigVo.setImgUrlPath(FileUploadUtil.getImgRootPath() + ConfigVo.getImgUrl());
                            }
                        }
                        rightsVO.setConfigVo(ConfigVo);
                        rightsListVo.add(rightsVO);
                    }
                }
                userCardVo.setRightsVOList(rightsListVo);
                info.setUserMemberCardVO(userCardVo);
            }
        }
        return new PageResult<>(page);
    }


    /**
     * <p>
     * 支付前检查库存等
     * </p>
     *
     * @param operUserId
     * @param orderNum
     * @return java.math.BigDecimal
     * @author Winder
     * @date 2021/8/19 下午3:02
     */
    @Override
    public BigDecimal getCost(String operUserId, String orderNum) {
        //订单号
        QueryWrapper<OrderCard> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq("order_num", orderNum);
        OrderCard one = this.service.getOne(productQueryWrapper);
        if (Objects.isNull(one)) {
            return null;
        }
        return one.getRealCost();
    }


    /**
     * <p>
     * 支付成功减去库存
     * </p>
     *
     * @param operUserId
     * @param orderNum
     * @return boolean
     * @author Winder
     * @date 2021/9/10 下午1:56
     */
    @Override
    public boolean updatePaySuccessOrderStatus(String operUserId, String orderNum) {
        boolean flag = true;
        //订单号
        QueryWrapper<OrderCard> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq("order_num", orderNum);
        OrderCard one = this.service.getOne(productQueryWrapper);
        if (Objects.isNull(one)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //更新订单状态 只有未支付订单可以取消订单
        if (one.getOrderStatus().equals(OrderType.UNPAY.getType())) {
            OrderCard infoNew = new OrderCard();
            infoNew.setOrderStatus(OrderType.COMPLETE.getType());
            infoNew.setId(one.getId());
            infoNew.setPayTime(new Date());//记录订单支付时间
            infoNew.setUpdateTime(System.currentTimeMillis());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setPayStatus(1);
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
            }
            //订单号
            QueryWrapper<UserMemberCard> CardWrapper = new QueryWrapper<>();
            CardWrapper.eq("order_num", orderNum);
            List<UserMemberCard> memberCardList = this.userMemberCardService.list(CardWrapper);
            for (UserMemberCard memberCarInfo : memberCardList) {
                //查看会员卡列表
                MemberCard memberCardInfo = this.memberCardService.getById(memberCarInfo.getCardId());
                // 如果库存不是无限的话
                if (memberCardInfo.getIsInfinite() == 1) {
                    memberCardInfo.setInventoryCount(memberCardInfo.getInventoryCount() - 1);
                    boolean flagUpdate = this.memberCardService.updateById(memberCardInfo);
                    if (!flagUpdate) {
                        flag = false;
                    }
                }
                if (memberCardInfo.getIsFree() == 0) {
                    //通知的话选择新消息类型消息类型:1审核成功通知 2审核失败通  3会员卡购买成功知 4新用户注册成功通知  5免费会员卡领取成功通知
                    this.userMessageServiceBLL.send(5, one.getCreateOperatorId());
                } else {
                    //通知的话选择新消息类型消息类型:1审核成功通知 2审核失败通  3会员卡购买成功知 4新用户注册成功通知  5免费会员卡领取成功通知
                    this.userMessageServiceBLL.send(3, one.getCreateOperatorId());
                }
                // 修改为可以启用
                memberCarInfo.setIsStatus(0);
                this.userMemberCardService.updateById(memberCarInfo);
            }

            //更新用户身份
            User user = new User();
            user.setId(one.getCreateOperatorId());
            user.setLevel(1);
            this.userService.updateById(user);

            return flag;
        } else {
            throw new CommonException(CardManageExceptionCodeEnum.OrderStateParamsError.code, CardManageExceptionCodeEnum.OrderStateParamsError.msg);
        }
    }


}