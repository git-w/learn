package com.elite.learn.cardManage.mapper;

import com.elite.learn.cardManage.entity.UserMemberCard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员卡和用户中间表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
public interface UserMemberCardMapper extends BaseMapper<UserMemberCard> {

}
