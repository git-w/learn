package com.elite.learn.cardManage.contants;

public enum CardManageExceptionCodeEnum {


    SaveSuccess(100000, "保存成功"),
    SelectSuccess(100000, "查询成功"),
    DeleteSuccess(100000, "删除成功"),
    UpdateSuccess(100000, "修改成功"),
    SaveFail(100005, "保存失败"),
    DeleteFail(100005, "删除失败"),
    SelectFail(100005, "查询失败"),
    NotUserGetBumFail(100005, "领取次数已经用完！"),

    OrderTypeLackParamsError(100001, "订单类型错误"),
    OrderTypeParamsError(100001, "缺少订单类型"),
    TotalParamsError(100001, "商品支付金额错误，支付金额小于等于0"),
    PayFail(100005, "支付失败"),
    OrderStateParamsError(100001, "订单状态错误"),
    InventoryCountInsufficientFail(100005, "库存数量不足"),
    UpdateFail(100005, "修改失败");

    public Integer code;
    public String msg;


    CardManageExceptionCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
