package com.elite.learn.cardManage.service.impl;

import com.elite.learn.cardManage.entity.UserMemberCardRights;
import com.elite.learn.cardManage.mapper.UserMemberCardRightsMapper;
import com.elite.learn.cardManage.service.IUserMemberCardRightsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户注册会员权益表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
@Service
public class UserMemberCardRightsServiceImpl extends ServiceImpl<UserMemberCardRightsMapper, UserMemberCardRights> implements IUserMemberCardRightsService {

}
