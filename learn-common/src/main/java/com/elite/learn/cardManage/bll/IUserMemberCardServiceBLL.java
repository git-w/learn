package com.elite.learn.cardManage.bll;


import com.elite.learn.cardManage.query.LogUserMemberCardQuery;
import com.elite.learn.cardManage.vo.LogUserMemberCardVO;
import com.elite.learn.cardManage.vo.UserMemberCardVO;
import com.elite.learn.common.core.page.PageResult;

/**
 * @author 花賊
 * @create 2021-01-19 17:11
 */
public interface IUserMemberCardServiceBLL {


    /**
     * <p>
     * 列表
     * </p>
     *
     * @param query
     * @return com.elite.learn.tools.base.common.entity.page.PageResult<com.elite.learn.base.operation.order.common.entity.dto.UserPayDTO>
     * @author: leroy
     * @date 2021/7/15 11:25
     */
    PageResult<LogUserMemberCardVO> getList(LogUserMemberCardQuery query);


    /**
     * 获取用户的vid
     * @param operUserId
     * @return
     */
    UserMemberCardVO getVIPInfo(String operUserId);
}
