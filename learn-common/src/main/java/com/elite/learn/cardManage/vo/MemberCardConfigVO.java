package com.elite.learn.cardManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 会员卡配置表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Data
public class MemberCardConfigVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 关联会员卡ID
     */
    private String cardId;

    /**
     * 关联类型 0-红娘牵线
     */
    private Integer isType;

    /**
     * 关联ID
     */
    private String othersId;

    /**
     * 图片地址
     */
    private String imgUrl;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 间隔天数
     */
    private Integer timeInterval;

    /**
     * 有效天数
     */
    private Integer validTime;

    /**
     * 是否启用 0-是 1-否
     */
    private Integer isEnabled;

    /**
     * 备注
     */
    private String remark;

    /**
     * 备注
     */
    private String memo;

    /**
     * 添加时间
     */
    private Long createTime;


    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 图片地址长路径
     */
    private String imgUrlPath;


}
