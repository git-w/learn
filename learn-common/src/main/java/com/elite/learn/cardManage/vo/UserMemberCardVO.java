package com.elite.learn.cardManage.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 会员卡和用户中间表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
@Data
public class UserMemberCardVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 关联会员卡ID
     */
    private String cardId;

    /**
     * 会员卡编号
     */
    private String cardNum;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 有效期开始日期
     */
    private Long startTime;

    /**
     * 有效期结束日期
     */
    private Long endTime;

    /**
     * 关联订单id
     */
    private String orderId;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 会员卡状态 0-是 1-否
     */
    private Integer isStatus;

    /**
     * 备注
     */
    private String memo;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 购买会员卡权益
     */
    private List<UserMemberCardRightsVO> rightsVOList;
    /**
     * 会员卡的基本信息
     */
    private MemberCardVO memberCardVO;
}
