package com.elite.learn.cardManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.entity.OrderCard;
import com.elite.learn.cardManage.entity.OrderCard;
import com.elite.learn.cardManage.mapper.OrderCardMapper;
import com.elite.learn.cardManage.service.IOrderCardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.cardManage.vo.OrderCardVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表---会员卡订单表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
@Service
public class OrderCardServiceImpl extends ServiceImpl<OrderCardMapper, OrderCard> implements IOrderCardService {


    /*
     *
     * @author: leroy
     * @date 2021/11/29 17:53
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public OrderCardVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 17:53
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    @Override
    public IPage<OrderCardVO> getLists(Page<OrderCardVO> pageVo, @Param(Constants.WRAPPER) Wrapper<OrderCard> queryWrapper) {
        pageVo.setRecords(this.baseMapper.getLists(pageVo, queryWrapper));
        return pageVo;
    }
}
