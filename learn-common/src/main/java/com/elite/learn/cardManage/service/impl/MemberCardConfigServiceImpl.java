package com.elite.learn.cardManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.cardManage.entity.MemberCardConfig;
import com.elite.learn.cardManage.mapper.MemberCardConfigMapper;
import com.elite.learn.cardManage.service.IMemberCardConfigService;
import com.elite.learn.cardManage.vo.MemberCardConfigVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 会员卡配置表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Service
public class MemberCardConfigServiceImpl extends ServiceImpl<MemberCardConfigMapper, MemberCardConfig> implements IMemberCardConfigService {


    /**
     *
     * @author: leroy
     * @date 2021/11/29 17:53
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public MemberCardConfigVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     *
     * @author: leroy
     * @date 2021/11/29 17:54
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    @Override
    public IPage<MemberCardConfigVO> getLists(Page<MemberCardConfigVO> pageVo, @Param(Constants.WRAPPER) Wrapper<MemberCardConfig> queryWrapper) {
        pageVo.setRecords(this.baseMapper.getLists(pageVo, queryWrapper));
        return pageVo;
    }

    @Override
    public List<MemberCardConfigVO> findAll(@Param(Constants.WRAPPER)Wrapper<MemberCardConfig> queryWrapper) {
        return this.baseMapper.findAll(queryWrapper);
    }
}
