package com.elite.learn.cardManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.bll.IMemberCardConfigServiceBLL;
import com.elite.learn.cardManage.dto.MemberCardConfigDTO;
import com.elite.learn.cardManage.entity.MemberCardConfig;
import com.elite.learn.cardManage.query.MemberCardConfigQuery;
import com.elite.learn.cardManage.service.IMemberCardConfigService;
import com.elite.learn.cardManage.vo.MemberCardConfigVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 会员卡配置表
 * @Title: InfoMemberCardConfigServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/29 14:16
 */

@Service
public class MemberCardConfigServiceBLL implements IMemberCardConfigServiceBLL {

    @Resource
    private IMemberCardConfigService service;


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/11/29 14:33
     * @update
     * @updateTime 根据主键查找id
     */
    @Override
    public MemberCardConfig get(String id) {
        return this.service.getById(id);
    }


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/11/29 14:33
     * @update
     * @updateTime 详情
     */
    @Override
    public MemberCardConfigVO getInfo(String id) {
        MemberCardConfigVO info = this.service.getInfo(id);
        if (Objects.nonNull(info)) {
            if (StringUtil.notBlank(info.getImgUrl())) {
                //拼接主图长地址
                info.setImgUrlPath(FileUploadUtil.getImgRootPath() + info.getImgUrl());
            }
        }
        return info;
    }


    /**
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/11/29 14:34
     * @update
     * @updateTime 添加
     */
    @Override
    public String save(String operUserId, MemberCardConfigDTO bean) {
        //获取实体对象
        MemberCardConfig info = new MemberCardConfig();
        //创建用户生成的id
        info.setId(IdUtils.simpleUUID());
        //关联会员卡ID
        info.setCardId(bean.getCardId());
        //关联类型 0-红娘牵线
        info.setIsType(bean.getIsType());
        //关联ID
        info.setOthersId(bean.getOthersId());
        //图片地址
        info.setImgUrl(bean.getImgUrl());
        //数量
        info.setCount(bean.getCount());
        //间隔天数
        info.setTimeInterval(bean.getTimeInterval());
        //有效天数
        info.setValidTime(bean.getValidTime());
        //是否启用 0-是 1-否
        info.setIsEnabled(bean.getIsEnabled());
        //备注
        info.setRemark(bean.getRemark());
        //删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //创建者信息
        info.setCreateOperatorId(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateTime(System.currentTimeMillis());

        boolean result = this.service.save(info);
        return result ? info.getId() : null;
    }


    /**
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/11/29 14:42
     * @update
     * @updateTime 更新
     */
    @Override
    public boolean update(String operUserId, MemberCardConfigDTO bean) {
        // 判断通过ID获取的对象是否为空
        MemberCardConfig info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //获取实体对象
        MemberCardConfig infoNew = new MemberCardConfig();
        //创建用户生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //关联会员卡ID
        if (StringUtil.notBlank(bean.getCardId())) {
            infoNew.setCardId(bean.getCardId());
        }
        //关联类型 0-红娘牵线
        if (Objects.nonNull(bean.getIsType())) {
            infoNew.setIsType(bean.getIsType());
        }
        //关联ID
        infoNew.setOthersId(bean.getOthersId());
        //图片地址
        infoNew.setImgUrl(bean.getImgUrl());
        //数量
        infoNew.setCount(bean.getCount());
        //间隔天数
        infoNew.setTimeInterval(bean.getTimeInterval());
        //有效天数
        infoNew.setValidTime(bean.getValidTime());
        //是否启用 0-是 1-否
        infoNew.setIsEnabled(bean.getIsEnabled());
        //备注
        infoNew.setRemark(bean.getRemark());
        //删除标识 0:未删除 1:删除
        infoNew.setIsDelete(DeleteType.ISDELETE.getCode());

        //更新者信息
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());
        return this.service.updateById(infoNew);
    }


    /**
     * @param ids
     * @return null
     * @author: leroy
     * @date 2021/11/29 14:51
     * @update
     * @updateTime 删除
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        //每个id已逗号分割
        String[] idsArr = ids.split(",");
        MemberCardConfig infoDel = null;
        //遍历id并判断是否为空
        for (String id : idsArr) {
            MemberCardConfig info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new MemberCardConfig();
            //设置删除状态
            infoDel.setId(info.getId());
            infoDel.setDeleteOperatorId(operUserId);
            //设置状态为删除
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            infoDel.setDeleteOperatorName(info.getDeleteOperatorName());
            infoDel.setDeleteTime(System.currentTimeMillis());
            //删除状态
            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
            if (!flag) {
                //修改失败
                throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                        BasePojectExceptionCodeEnum.SelectFail.msg);
            }
        }
        return flag;
    }


    /**
     * 查询所有
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/20 10:44
     * @update
     * @updateTime
     */
    @Override
    public List<MemberCardConfigVO> findAll(MemberCardConfigQuery params) {
        QueryWrapper<MemberCardConfig> queryWrapper = new QueryWrapper<MemberCardConfig>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        if (Objects.nonNull(params.getIsEnabled())) {
            queryWrapper.eq("is_enabled", params.getIsEnabled());
        }
        if (Objects.nonNull(params.getCardId())) {
            queryWrapper.eq("card_id", params.getCardId());
        }
        queryWrapper.orderByDesc("create_time");
        List<MemberCardConfigVO> list = this.service.findAll(queryWrapper);
        return list;
    }



    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/20 10:44
     * @update
     * @updateTime
     */
    @Override
    public PageResult<MemberCardConfigVO> getList(MemberCardConfigQuery params) {
        QueryWrapper<MemberCardConfig> queryWrapper = new QueryWrapper<MemberCardConfig>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        if (Objects.nonNull(params.getIsEnabled())) {
            queryWrapper.eq("is_enabled", params.getIsEnabled());
        }
        if (Objects.nonNull(params.getCardId())) {
            queryWrapper.eq("card_id", params.getCardId());
        }
        queryWrapper.orderByDesc("create_time");

        Page<MemberCardConfigVO> pageVo = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<MemberCardConfigVO> page = this.service.getLists(pageVo, queryWrapper);
        //有查询结果
            for (MemberCardConfigVO info : page.getRecords()) {
                if (StringUtil.notBlank(info.getImgUrl())) {
                    //拼接主图长地址
                    info.setImgUrlPath(FileUploadUtil.getImgRootPath() + info.getImgUrl());
                }

            }

        return new PageResult<>(page);
    }


    /**
     * 会员卡多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean updateMultiIsEnabled(String operUserId, BaseParams params) {
        boolean flag = true;
        String ids = params.getIds();
        String[] idArr = ids.split(",");
        MemberCardConfig infoNew = null;
        for (String id : idArr) {
            MemberCardConfig info = this.get(id.trim());
            if (Objects.isNull(info)) {
                continue;
            }
            infoNew = new MemberCardConfig();
            infoNew.setId(id.trim());
            infoNew.setIsEnabled(params.getIsEnabled());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
            infoNew.setUpdateTime(System.currentTimeMillis());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code,
                    BasePojectExceptionCodeEnum.UpdateFail.msg);
        }
        return flag;
    }
}

