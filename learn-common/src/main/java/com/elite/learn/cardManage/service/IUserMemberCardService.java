package com.elite.learn.cardManage.service;

import com.elite.learn.cardManage.entity.UserMemberCard;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员卡和用户中间表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
public interface IUserMemberCardService extends IService<UserMemberCard> {

}
