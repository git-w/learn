package com.elite.learn.cardManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户注册会员权益表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
@Data
public class UserMemberCardRightsVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 会员卡id
     */
    private String midUserCardId;

    /**
     * 会员卡id
     */
    private String cardId;

    /**
     * 会员卡权益配置id
     */
    private String configId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 会员卡状态 0-正常 1-失效 2-使用 3-过期
     */
    private Integer isStatus;

    /**
     * 批次
     */
    private Integer batchnum;

    /**
     * 有效天数
     */
    private Integer validDays;

    /**
     * 有效开始时间
     */
    private Long startTime;

    /**
     * 有效结束时间
     */
    private Long endTime;

    /**
     * 备注
     */
    private String memo;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 关联类型 0-红娘牵线
     */
    private Integer isType;
    /**
     * 总数
     */
    private Integer totalCount;

    private MemberCardConfigVO ConfigVo;
}
