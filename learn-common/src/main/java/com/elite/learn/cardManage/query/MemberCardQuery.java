package com.elite.learn.cardManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 会员卡信息表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Data
public class MemberCardQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 会员卡名称
     */
    private String name;
    /**
     * 是否启用 0-是 1-否
     */
    private Integer isEnabled;
    /**
     *是否免费领取 0-是 1-否
     */
    private Integer isFree;


}
