package com.elite.learn.cardManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 订单表---会员卡订单表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-09
 */
@Data
public class OrderCardQuery extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 订单编号
     */
    private String orderNum;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货 5-完成 6-申请退款 7-退款中 8-退款完成 9-超时未支付取消的
     */
    private Integer orderStatus;


}
