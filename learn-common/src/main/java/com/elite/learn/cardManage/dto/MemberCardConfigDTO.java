package com.elite.learn.cardManage.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 会员卡配置表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-29
 */
@Data
public class MemberCardConfigDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * 关联会员卡ID
     */
    @NotNull(message = "关联会员卡ID，参数为空")
    private String cardId;

    /**
     * 关联类型 0-红娘牵线
     */
    @NotNull(message = "关联类型，参数为空")
    private Integer isType;

    /**
     * 关联ID
     */

    private String othersId;

    /**
     * 图片地址
     */
    private String imgUrl;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 间隔天数
     */
    private Integer timeInterval;

    /**
     * 有效天数
     */
    private Integer validTime;

    /**
     * 是否启用 0-是 1-否
     */
    private Integer isEnabled;

    /**
     * 备注
     */
    private String remark;
    /**
     * 备注
     */
    private String memo;

    /**
     * 创建者ID
     */
    private String createOperatorId;
    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 创建者ID
     */
    private String createOperatorName;
    /**
     * 更新者ID
     */
    private String updateOperatorName;


}
