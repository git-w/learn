package com.elite.learn.cardManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.bll.IMemberCardServiceBLL;
import com.elite.learn.cardManage.bll.IUserMemberCardServiceBLL;
import com.elite.learn.cardManage.entity.LogUserMemberCard;
import com.elite.learn.cardManage.entity.UserMemberCard;
import com.elite.learn.cardManage.entity.UserMemberCardRights;
import com.elite.learn.cardManage.query.LogUserMemberCardQuery;
import com.elite.learn.cardManage.service.ILogUserMemberCardService;
import com.elite.learn.cardManage.service.IMemberCardConfigService;
import com.elite.learn.cardManage.service.IUserMemberCardRightsService;
import com.elite.learn.cardManage.service.IUserMemberCardService;
import com.elite.learn.cardManage.vo.LogUserMemberCardVO;
import com.elite.learn.cardManage.vo.MemberCardConfigVO;
import com.elite.learn.cardManage.vo.UserMemberCardRightsVO;
import com.elite.learn.cardManage.vo.UserMemberCardVO;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.string.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author 张建
 * @create 2021-01-19 17:11
 */

@Service
public class UserMemberCardServiceBLL implements IUserMemberCardServiceBLL {

    @Resource
    private ILogUserMemberCardService service;
    //会员卡和用户中间表
    @Resource
    private IUserMemberCardService userMemberCardService;
    //用户注册会员权益表
    @Resource
    private IUserMemberCardRightsService userMemberCardRightsService;
    //会员卡配置表
    @Resource
    private IMemberCardConfigService memberCardConfigService;
    //会员卡信息表
    @Resource
    private IMemberCardServiceBLL memberCardServiceBLL;


    /**
     * <p>
     * 列表
     * </p>
     *
     * @param query
     * @return com.elite.learn.tools.base.common.entity.page.PageResult<com.elite.learn.base.operation.order.common.entity.dto.UserPayDTO>
     * @author: leroy
     * @date 2021/7/15 11:26
     */
    @Override
    public PageResult<LogUserMemberCardVO> getList(LogUserMemberCardQuery query) {
        //构建条件选择器
        QueryWrapper<LogUserMemberCard> queryWrapper = new QueryWrapper<>();
        //订单号
        if (StringUtils.isNotEmpty(query.getMidCardId())) {
            queryWrapper.like("lumc.mid_card_id", query.getMidCardId());
        }

        if (Objects.nonNull(query.getStartTime()) && Objects.nonNull(query.getEndTime())) {
            queryWrapper.between("lumc.create_time", query.getStartTime(), query.getEndTime());
        } else if (Objects.nonNull(query.getStartTime())) {
            queryWrapper.ge("lumc.create_time", query.getStartTime());
        } else if (Objects.nonNull(query.getEndTime())) {
            queryWrapper.le("lumc.create_time", query.getEndTime());
        }
        queryWrapper.orderByDesc("lumc.create_time");
        //分页
        Page<LogUserMemberCardVO> pageVo = new Page<>(query.getPageNum(), query.getPageSize());
        //分页和搜索
        IPage<LogUserMemberCardVO> page = this.service.getLists(pageVo, queryWrapper);
        return new PageResult<>(page);
    }


    /**
     * 获取用户的会员
     *
     * @param operUserId
     * @return
     */
    @Override
    public UserMemberCardVO getVIPInfo(String operUserId) {
        //订单号
        QueryWrapper<UserMemberCard> wrapper = new QueryWrapper<>();
        wrapper.eq("create_operator_id", operUserId);
        wrapper.eq("is_status", 0);
        UserMemberCard userMemberCardInfo = this.userMemberCardService.getOne(wrapper);
        if (Objects.nonNull(userMemberCardInfo)) {
            UserMemberCardVO userCardVo = new UserMemberCardVO();
            BeanUtils.copyProperties(userMemberCardInfo, userCardVo);
            //存入会员卡的信息
            userCardVo.setMemberCardVO(this.memberCardServiceBLL.getInfo(userCardVo.getCardId()));
            QueryWrapper<UserMemberCardRights> wrapperRights = new QueryWrapper<>();
            wrapperRights.eq("mid_user_card_id", userMemberCardInfo.getId());
            List<UserMemberCardRights> list = this.userMemberCardRightsService.list(wrapperRights);
            List<UserMemberCardRightsVO> rightsListVo = new ArrayList<>();
            UserMemberCardRightsVO rightsVO = null;
            if (Objects.nonNull(list) && list.size() > 0) {
                for (UserMemberCardRights rightsInfo : list) {
                    rightsVO = new UserMemberCardRightsVO();
                    BeanUtils.copyProperties(rightsInfo, rightsVO);
                    MemberCardConfigVO ConfigVo = this.memberCardConfigService.getInfo(rightsInfo.getConfigId());
                    if (Objects.nonNull(ConfigVo)) {
                        if (StringUtil.notBlank(ConfigVo.getImgUrl())) {
                            //拼接主图长地址
                            ConfigVo.setImgUrlPath(FileUploadUtil.getImgRootPath() + ConfigVo.getImgUrl());
                        }
                    }
                    rightsVO.setConfigVo(ConfigVo);
                    rightsListVo.add(rightsVO);
                }
            }
            userCardVo.setRightsVOList(rightsListVo);
            return userCardVo;
        }
        return null;
    }
}
