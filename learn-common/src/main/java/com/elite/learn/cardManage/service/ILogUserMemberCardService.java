package com.elite.learn.cardManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.cardManage.entity.LogUserMemberCard;
import com.elite.learn.cardManage.vo.LogUserMemberCardVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 使用会员卡权益日志表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
public interface ILogUserMemberCardService extends IService<LogUserMemberCard> {


    /**
     * @param pageVo
     * @return null
     * @author: leroy
     * @date 2021/11/29 16:24
     * @update
     * @updateTime 分页
     */
    IPage<LogUserMemberCardVO> getLists(Page<LogUserMemberCardVO> pageVo, @Param(Constants.WRAPPER) Wrapper<LogUserMemberCard> queryWrapper);

}
