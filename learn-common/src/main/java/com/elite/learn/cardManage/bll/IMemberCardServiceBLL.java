package com.elite.learn.cardManage.bll;

import com.elite.learn.cardManage.dto.MemberCardDto;
import com.elite.learn.cardManage.entity.MemberCard;
import com.elite.learn.cardManage.query.MemberCardQuery;
import com.elite.learn.cardManage.vo.MemberCardVO;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.ICommonServiceBLL;

public interface IMemberCardServiceBLL extends ICommonServiceBLL<MemberCard, MemberCardDto, MemberCardVO, MemberCardQuery> {


    /**
     * 多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateMultiIsEnabled(String operUserId, BaseParams params);


    /**
     * 分页查询数据
     *
     * @param params
     */
    PageResult<MemberCardVO> getWebList(MemberCardQuery params);


    /**
     * 分页查询数据
     *
     * @param id
     */
    MemberCardVO getWebInfo(String id);
}
