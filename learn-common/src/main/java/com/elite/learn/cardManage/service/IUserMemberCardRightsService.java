package com.elite.learn.cardManage.service;

import com.elite.learn.cardManage.entity.UserMemberCardRights;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户注册会员权益表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-11
 */
public interface IUserMemberCardRightsService extends IService<UserMemberCardRights> {

}
