package com.elite.learn.excelManage.annotation;

import java.lang.annotation.*;

/**
 * @description:
 * @projectName:learn
 * @see:com.elite.learn.excelManage.annotation
 * @author: leroy
 * @createTime:2020/11/16 13:59
 * @version:1.0
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExcelColumn {
    /**
     * Excel标题
     *
     * @return
     * @author Lynch
     */
    String value() default "";

    /**
     * Excel从左往右排列位置
     *
     * @return
     * @author Lynch
     */
    int col() default 0;
}
