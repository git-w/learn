package com.elite.learn.excelManage.util;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * @Package: com.elite.learn.excelManage.util
 * @Description:
 * @author: leroy
 * @data: 2020/11/16 14:01
 */
public class ExcelUtils {

    private static Logger logger = LoggerFactory.getLogger(ExcelUtils.class);

    /**
     * 课程excel
     *
     * @param in
     * @param fileName
     * @return
     * @throws Exception
     */
    public static List getCourseListByExcel(InputStream in, String fileName) throws Exception {

        List list = new ArrayList<>();

        // 创建excel工作簿
        Workbook work = getWorkbook(in, fileName);
        if (null == work) {
            throw new Exception("创建Excel工作薄为空！");
        }

        Sheet sheet = null;
        Row row = null;
        Cell cell = null;

        for (int i = 0; i < work.getNumberOfSheets(); i++) {

            sheet = work.getSheetAt(i);
            if (sheet == null) {
                continue;
            }

            // 滤过第一行标题
            for (int j = sheet.getFirstRowNum(); j <= sheet.getLastRowNum(); j++) {
                row = sheet.getRow(j);
                if (row == null || row.getFirstCellNum() == j) {
                    continue;
                }

                List<Object> li = new ArrayList<>();

                for (int y = row.getFirstCellNum(); y < row.getLastCellNum(); y++) {
                    cell = row.getCell(y);
                    // 日期类型转换
                    if (y == 12 && HSSFDateUtil.isCellDateFormatted(row.getCell(12)) && Objects.nonNull(cell)) {
                        //cell.setCellType(CellType.STRING);
                        //  SimpleDateFormat sdf = null;
                        //sdf = new SimpleDateFormat("yyyy-MM-dd");  //设置转成的时间格式
                        Date date = row.getCell(12).getDateCellValue();
                        //  String shijian = sdf.format(date);
                        // System.out.println("-----------------"+cell.getDateCellValue());
                        // Date date = HSSFDateUtil.getJavaDate(s1);
                        li.add(date);
                        continue;

                    }

                    // 工号
                    if (y == 0 && Objects.nonNull(cell)) {
                        //cell.setCellType(CellType.STRING);
                        //  SimpleDateFormat sdf = null;
                        //sdf = new SimpleDateFormat("yyyy-MM-dd");  //设置转成的时间格式
                        DecimalFormat df = new DecimalFormat("#");
                        //System.out.println(    df.format(cell.getNumericCellValue()));
                        String WorkNumber = df.format(cell.getNumericCellValue());
                        if (!WorkNumber.equals("0")) {
                            li.add(WorkNumber);
                            continue;
                        }
                        //  String shijian = sdf.format(date);
                        // System.out.println("-----------------"+cell.getDateCellValue());
                        // Date date = HSSFDateUtil.getJavaDate(s1);

                    }


                    // 手机号
                    if (y == 3 && Objects.nonNull(cell)) {
                        //cell.setCellType(CellType.STRING);
                        //  SimpleDateFormat sdf = null;
                        //sdf = new SimpleDateFormat("yyyy-MM-dd");  //设置转成的时间格式
                        DecimalFormat df = new DecimalFormat("#");
                        //System.out.println(    df.format(cell.getNumericCellValue()));
                        String ModilePhone = df.format(cell.getNumericCellValue());
                        //  String shijian = sdf.format(date);
                        // System.out.println("-----------------"+cell.getDateCellValue());
                        // Date date = HSSFDateUtil.getJavaDate(s1);
                        li.add(ModilePhone);
                        continue;
                    }

                    li.add(cell);
                }
                list.add(li);
            }
        }
        work.close();
        return list;
    }

    /**
     * excel
     *
     * @param in
     * @param fileName
     * @return
     * @throws Exception
     */
    public static List getCourseListByExcel2(InputStream in, String fileName) throws Exception {

        List list = new ArrayList<>();

        // 创建excel工作簿
        Workbook work = getWorkbook(in, fileName);
        if (null == work) {
            throw new Exception("创建Excel工作薄为空！");
        }

        Sheet sheet = null;
        Row row = null;
        Cell cell = null;

        for (int i = 0; i < work.getNumberOfSheets(); i++) {

            sheet = work.getSheetAt(i);
            if (sheet == null) {
                continue;
            }

            // 滤过第一行标题
            for (int j = sheet.getFirstRowNum(); j <= sheet.getLastRowNum(); j++) {
                row = sheet.getRow(j);
                if (row == null || row.getFirstCellNum() == j) {
                    continue;
                }

                List<Object> li = new ArrayList<>();

                for (int y = row.getFirstCellNum(); y < row.getLastCellNum(); y++) {
                    cell = row.getCell(y);

                    li.add(cell);
                }
                list.add(li);
            }
        }
        work.close();
        return list;
    }

    /**
     * 判断文件格式
     *
     * @param in
     * @param fileName
     * @return
     */
    private static Workbook getWorkbook(InputStream in, String fileName) throws Exception {

        Workbook book = null;
        String filetype = fileName.substring(fileName.lastIndexOf("."));

        if (".xls".equals(filetype)) {
            book = new HSSFWorkbook(in);
        } else if (".xlsx".equals(filetype)) {
            book = new XSSFWorkbook(in);
        } else {
            throw new Exception("请上传excel文件！");
        }

        return book;
    }


    /**
     * 课程excel
     *
     * @param in
     * @param fileName
     * @return
     * @throws Exception
     */
    public static List getTopicListByExcel(InputStream in, String fileName) throws Exception {

        List list = new ArrayList<>();

        // 创建excel工作簿
        Workbook work = getWorkbook(in, fileName);
        if (null == work) {
            throw new Exception("创建Excel工作薄为空！");
        }

        Sheet sheet = null;
        Row row = null;
        Cell cell = null;

        for (int i = 0; i < work.getNumberOfSheets(); i++) {

            sheet = work.getSheetAt(i);
            if (sheet == null) {
                continue;
            }

            // 滤过第一行标题
            for (int j = sheet.getFirstRowNum(); j <= sheet.getLastRowNum(); j++) {
                row = sheet.getRow(j);
                if (row == null || row.getFirstCellNum() == j) {
                    continue;
                }

                List<Object> li = new ArrayList<>();


                for (int y = row.getFirstCellNum(); y < row.getLastCellNum(); y++) {
                    cell = row.getCell(y);

                    String spec = "";
                    if (cell.getCellStyle().getDataFormatString().indexOf("%") != -1) {
                        spec = cell.getNumericCellValue() * 100 + "%";
                        cell.setCellValue(spec);//此处因为我要将错误数据导出，所以把值写回到列中。
                    } else {
                        spec = cell.toString();
                    }
                    cell.setCellType(CellType.STRING);

                    li.add(cell);
                }
                list.add(li);
            }
        }
        work.close();
        return list;
    }


    public static boolean isFloat(String str) {
        try {
            Float.parseFloat(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 导出Excel
     * @author: leroy
     * @date 2021/8/18 14:44
     * @param sheetName
     * @param title
     * @param values
     * @param wb
     * @return org.apache.poi.hssf.usermodel.HSSFWorkbook
     */
    public static HSSFWorkbook getHSSFWorkbook(String sheetName, String[] title, String[][] values, HSSFWorkbook wb) {

        // 第一步，创建一个HSSFWorkbook，对应一个Excel文件
        if (wb == null) {
            wb = new HSSFWorkbook();
        }

        // 第二步，在workbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet sheet = wb.createSheet(sheetName);

        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制
        HSSFRow row = sheet.createRow(0);

        // 第四步，创建单元格，并设置值表头 设置表头居中
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);


        // 声明列对象
        HSSFCell cell = null;

        // 创建标题
        for (int i = 0; i < title.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(title[i]);
            cell.setCellStyle(style);
            sheet.autoSizeColumn(i);
            sheet.setColumnWidth(i, sheet.getColumnWidth(i) * 17 / 10);
        }

        // 创建内容
        for (int i = 0; i < values.length; i++) {
            row = sheet.createRow(i + 1);
            for (int j = 0; j < values[i].length; j++) {
                // 将内容按顺序赋给对应的列对象
                row.createCell(j).setCellValue(values[i][j]);
            }
        }
        return wb;
    }
}