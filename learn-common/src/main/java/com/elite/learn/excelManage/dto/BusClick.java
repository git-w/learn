package com.elite.learn.excelManage.dto;/**
 * @description:
 * @projectName:learn
 * @see:com.elite.learn.excelManage.dto
 * @author: leroy
 * @createTime:2020/11/16 14:06
 * @version:1.0
 */

import com.elite.learn.excelManage.annotation.ExcelColumn;

/**
 *
 * @Title  :BusClick.java  
 * @Package: com.elite.learn.excelManage.dto
 * @Description:
 * @author: leroy
 * @data: 2020/11/16 14:06 
 */
public class BusClick {
    @ExcelColumn(value = "地区编码", col = 1)
    private String cityCode;

    @ExcelColumn(value = "标志", col = 2)
    private String markId;

    @ExcelColumn(value = "toaluv", col = 3)
    private String toaluv;

    @ExcelColumn(value = "date", col = 4)
    private String date;

    @ExcelColumn(value = "clientVer", col = 5)
    private String clientVer;

    @Override
    public String toString() {
        return "BusClick [cityCode=" + cityCode + ", markId=" + markId + ", toaluv=" + toaluv + ", date=" + date
                + ", clientVer=" + clientVer + "]";
    }


}