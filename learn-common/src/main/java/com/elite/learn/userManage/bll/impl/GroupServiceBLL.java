package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IGroupServiceBLL;
import com.elite.learn.userManage.dto.GroupDTO;
import com.elite.learn.userManage.entity.Group;
import com.elite.learn.userManage.query.GroupQuery;
import com.elite.learn.userManage.service.IGroupService;
import com.elite.learn.userManage.vo.GroupVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
public class GroupServiceBLL implements IGroupServiceBLL {
    @Resource
    private IGroupService service;


    /**
     * 查看详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public Group get(String id) {
        if (StringUtil.isNotEmpty(id)) {
            Group info = this.service.getById(id);
            return info;
        }
        return null;
    }


    /**
     * 查看详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public GroupVO getInfo(String id) {
        if (StringUtil.isNotEmpty(id)) {
            QueryWrapper<Group> wrapper = new QueryWrapper<>();
            wrapper.orderByDesc("ig.CreateTime");
            wrapper.eq("ig.IsDelete", DeleteType.NOTDELETE.getCode());
            wrapper.like("ig.ID", id);
            GroupVO info = this.service.selectInfo(wrapper);
            return info;
        }
        return null;
    }

    /**
     * 添加
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2022/1/14 15:59
     * @update
     * @updateTime
     */
    public String isSave(String operGroupId, Group bean) {
        boolean flag = this.isExist(bean.getModilePhone(), bean.getUserName(), bean.getGroupName());
        if (!flag) {
            // 获取uuid
            Group info = new Group();
            info.setId(IdUtils.simpleUUID());
            info.setGroupName(bean.getGroupName()); //用户注册时使用个的帐号
            info.setUserName(bean.getUserName()); //客户姓名,参数为空
            info.setMembers(bean.getMembers());//成员
            info.setModilePhone(bean.getModilePhone()); //手机号
            info.setTotalOrderNum(0);
            info.setIsDelete(DeleteType.NOTDELETE.getCode());//查询没有被删除的
            info.setCreateTime(System.currentTimeMillis());//创建时间
            info.setCreateOperatorName(bean.getCreateOperatorName());
            info.setCreateOperatorID(operGroupId);
            flag = this.service.save(info);
            if (!flag) {
                throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code, BasePojectExceptionCodeEnum.SaveFail.msg);
            }
            return info.getId();
        }
        return null;
    }


    /**
     * 查看团队是否存在
     * 存在的话返回true  不存在的话返回false
     *
     * @return
     */
    public boolean isExist(String modilephone, String userName, String groupName) {
        QueryWrapper<Group> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        queryWrapper.eq("ModilePhone", modilephone);
        queryWrapper.eq("GroupName", groupName);
        queryWrapper.eq("UserName", userName);
        Group group = this.service.getOne(queryWrapper);
        if (Objects.nonNull(group)) {
            return true;
        }
        return false;
    }


    /**
     * 添加
     *
     * @param operGroupId 操作人员标识
     * @param bean        对象
     * @return
     */
    @Override
    public String save(String operGroupId, GroupDTO bean) {
        // 获取uuid

        Group info = new Group();
        info.setId(IdUtils.simpleUUID());
        info.setGroupName(bean.getGroupName()); //用户注册时使用个的帐号
        info.setUserName(bean.getUserName()); //客户姓名,参数为空
        info.setMembers(bean.getMembers());//成员
        info.setModilePhone(bean.getModilePhone()); //手机号
        info.setTotalOrderNum(0);//订单总数量
        info.setIsDelete(DeleteType.NOTDELETE.getCode());//查询没有被删除的
        info.setCreateTime(System.currentTimeMillis());//创建时间
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateOperatorID(operGroupId);
        boolean flag = this.service.save(info);
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code, BasePojectExceptionCodeEnum.SaveFail.msg);
        }
        return info.getId();
    }


    /**
     * 修改
     *
     * @param operGroupId 操作人员标识
     * @param bean        对象
     * @return
     */
    @Override
    public boolean update(String operGroupId, GroupDTO bean) {
        // 判断通过ID获取的对象是否为空
        Group info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        // 创建对象
        Group infoNew = new Group();
        //编号
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        // 修改团队姓名
        if (StringUtil.notBlank(bean.getGroupName())) {
            infoNew.setGroupName(bean.getGroupName());
        }
        // 修改用户注册时使用个的帐号
        if (StringUtil.notBlank(bean.getUserName())) {
            infoNew.setUserName(bean.getUserName());
        }
        //用户注册时使用个的帐号
        if (StringUtil.notBlank(bean.getMembers())) {
            infoNew.setMembers(bean.getMembers());
        }
        // 修改手机号
        if (StringUtil.notBlank(bean.getModilePhone())) {
            infoNew.setModilePhone(bean.getModilePhone());
        }
        infoNew.setMemo(bean.getMemo());
        // 设置修改者信息
        infoNew.setUpdateOperatorID(operGroupId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        return this.service.updateById(infoNew);
    }


    /**
     * 删除
     *
     * @param operGroupId 操作人员标识
     * @param ids         对象标识
     * @return
     */
    @Override
    public boolean remove(String operGroupId, String ids) {
        boolean flag = true;
        String[] idsArr = ids.split(",");
        for (String id : idsArr) {
            Group info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            Group infoDel = new Group();
            infoDel.setId(info.getId());
            // 删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            // 删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            // 删除者id
            infoDel.setDeleteOperatorID(operGroupId);
            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     * 查看所有
     *
     * @param params
     * @return
     */
    @Override
    public List<Group> findAll2(GroupQuery params) {
        QueryWrapper<Group> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("CreateTime");
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());

        if (StringUtil.isNotEmpty(params.getGroupName())) {
            queryWrapper.like("GroupName", params.getGroupName());
        }

        List<Group> groupList = this.service.list(queryWrapper);
        return groupList;
    }


    @Override
    public List<GroupVO> findAll(GroupQuery params) {
        return null;
    }


    /**
     * 分页
     *
     * @param params
     * @return
     */
    @Override
    public PageResult<GroupVO> getList(GroupQuery params) {
        QueryWrapper<Group> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("ig.CreateTime");
        queryWrapper.eq("ig.IsDelete", DeleteType.NOTDELETE.getCode());

        if (StringUtil.isNotEmpty(params.getGroupName())) {
            queryWrapper.like("ig.GroupName", params.getGroupName());
        }
        if (StringUtil.isNotEmpty(params.getUserName())) {
            queryWrapper.like("ig.UserName", params.getUserName());
        }
        if (null != params.getCreateTimeStart()) {
            queryWrapper.gt("ig.CreateTime", params.getCreateTimeStart());
        }
        if (null != params.getCreateTimeEnd()) {
            queryWrapper.lt("ig.CreateTime", params.getCreateTimeEnd());
        }
        if (StringUtil.isNotEmpty(params.getModilePhone())) {
            queryWrapper.eq("ig.ModilePhone", params.getModilePhone());
        }
        Page<GroupVO> PageParams = new Page<GroupVO>(params.getPageNum(), params.getPageSize());
        IPage<GroupVO> page = this.service.selectLists(PageParams, queryWrapper);
        // 总记录数
        return new PageResult<>(page);
    }
}
