package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.BlacklistDTO;
import com.elite.learn.userManage.entity.Blacklist;
import com.elite.learn.userManage.query.BlacklistQuery;
import com.elite.learn.userManage.vo.BlacklistVO;

public interface IBlacklistServiceBLL extends ICommonServiceBLL<Blacklist, BlacklistDTO, BlacklistVO, BlacklistQuery> {

    /**
     * 查看用户是否在黑名单已经存在
     * true是已经存在  false-用户不存在
     * @param params
     * @return
     */
    boolean isModilePhone(String params);
}
