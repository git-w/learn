package com.elite.learn.userManage.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-30
 */
@Data
public class UserLoginVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 真实姓名真实姓名
     */
    private String trueName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 生日
     */
    private Long birthday;







    /**
     * 用户的昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String headimgurl;

    /**
     * 手机号
     */
    private String modilePhone;

    /**
     * 小程序唯一身份ID
     */
    private String routineOpenid;

    /**
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    private Integer sex;

    /**
     * 用户所在城市
     */
    private String city;

    /**
     * 用户的语言，简体中文为zh_CN
     */
    private String language;

    /**
     * 用户所在省份
     */
    private String province;

    /**
     * 用户所在国家
     */
    private String country;


    /**
     * 注册时间
     */
    private Date registerTime;

    /**
     * 注册ip
     */
    private String registerIp;

    /**
     * 最后一次登录时间
     */
    private Date loginTime;

    /**
     * 登录ip
     */
    private String loginIp;


    /**
     * 用户等级 1-游客（普通用户）2-会员vip
     */
    private Integer level;







    /**
     * 获取用户手机验证码
     */
    private String session_key;


    /**
     * token
     */
    private String authorization;

}
