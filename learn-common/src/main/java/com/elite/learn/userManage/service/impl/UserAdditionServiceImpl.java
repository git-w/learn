package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.entity.UserAddition;
import com.elite.learn.userManage.mapper.UserAdditionMapper;
import com.elite.learn.userManage.service.IUserAdditionService;
import com.elite.learn.userManage.vo.UserAdditionVO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息---附加表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Service
public class UserAdditionServiceImpl extends ServiceImpl<UserAdditionMapper, UserAddition> implements IUserAdditionService {

    @Override
    public UserAdditionVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }
}
