package com.elite.learn.userManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 民宿预定-黑名单表
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_blacklist")
public class Blacklist implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */

    private String id;

    /**
     * 用户名称
     */

    private String UserName;

    /**
     * 手机号
     */

    private String ModilePhone;

    /**
     * 更新者ID
     */

    private String UpdateOperatorID;

    /**
     * 更新者名称
     */

    private String UpdateOperatorName;

    /**
     * 添加时间
     */

    private Long CreateTime;

    /**
     * 更新时间
     */

    private Long UpdateTime;

    /**
     * 删除标识 0:未删除 1:删除
     */

    private Integer IsDelete;

    /**
     * 删除者ID
     */

    private String DeleteOperatorID;

    /**
     * 删除者名称
     */

    private String DeleteOperatorName;

    /**
     * 删除时间
     */

    private Long DeleteTime;

    /**
     * 备注
     */

    private String Memo;

    /**
     * 备用字段，如果占用，请备注做什么用
     */

    private String ExtraParams1;

    /**
     * 备用字段，如果占用，请备注做什么用
     */

    private String ExtraParams2;

    /**
     * 备用字段，如果占用，请备注做什么用
     */

    private String ExtraParams3;

    /**
     * 创建者ID
     */


    /**
     * 创建者名称
     */

    private String CreateOperatorName;

    private String CreateOperatorID;

}
