package com.elite.learn.userManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 民宿预定-黑名单表
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
@Data
public class BlacklistQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 用户名称
     */
    private String UserName;

    /**
     * 手机号
     */
    private String ModilePhone;

    /**
     * 添加时间
     */
    private Long CreateTimeStart;
    private Long CreateTimeEnd;
}
