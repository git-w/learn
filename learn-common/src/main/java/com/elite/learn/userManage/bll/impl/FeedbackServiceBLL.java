package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IFeedbackServiceBLL;
import com.elite.learn.userManage.dto.FeedbackDTO;
import com.elite.learn.userManage.entity.Feedback;
import com.elite.learn.userManage.query.FeedbackQuery;
import com.elite.learn.userManage.service.IFeedbackService;
import com.elite.learn.userManage.vo.FeedbackVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 意见反馈表
 * @Title: InfoFeedbackServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/26 8:56
 */
@Service
public class FeedbackServiceBLL implements IFeedbackServiceBLL {


    @Resource
    private IFeedbackService service;


    /*
     *
     * @author: leroy
     * @date 2021/11/26 11:03
     * @param null
     * @return null
     * @update
     * @updateTime
     * 主键id
     */
    @Override
    public Feedback get(String id) {
        return this.service.getById(id);
    }


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/7 13:24
     * @update
     * @updateTime
     */
    @Override
    public FeedbackVO getInfo(String id) {
        FeedbackVO info = this.service.getInfo(id);
        //拼接多图长地址
        if (Objects.nonNull(info) && StringUtil.notBlank(info.getImgPathList())) {
            List<String> strList = new ArrayList<String>();
            String[] pictures = info.getImgPathList().split(",");
            String path = FileUploadUtil.getImgRootPath();
            for (String pic : pictures) {
                strList.add(path.concat(pic));
            }
            info.setImgPathListUrl(StringUtils.join(strList, ","));
        }
        return info;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 11:23
     * @param null
     * @return null
     * @update
     * @updateTime
     * 添加
     */
    @Override
    public String save(String operUserId, FeedbackDTO bean) {
        //创建实体对象
        Feedback info = new Feedback();
        //获取用户生成的id
        info.setId(IdUtils.simpleUUID());
        //用户昵称
        info.setName(bean.getName());
        //联系方式
        info.setMobilePhone(bean.getMobilePhone());
        //投诉时间
        info.setComplaintsTime(bean.getComplaintsTime());
        //投诉内容
        info.setContent(bean.getContent());
        //图片存放路径（短），多图用,隔开
        info.setImgPathList(bean.getImgPathList());
        //满意度 0非常满意 1满意 2一般 3不满意
        info.setSatisFaction(bean.getSatisFaction());
        //分类id
        info.setCateId(bean.getCateId());
        //处理结果
        info.setHandingContent(bean.getHandingContent());
        //备注
        info.setMemo(bean.getMemo());
        //处理状态 0:未处理 1:已处理
        info.setIsState(0);
        //额外参数1
        info.setExtraParams1(bean.getExtraParams1());
        //额外参数2
        info.setExtraParams2(bean.getExtraParams2());
        //额外参数3
        info.setExtraParams3(bean.getExtraParams3());
        //删除标识 0:未删除 1:已删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //创建者ID
        info.setCreateOperatorid(operUserId);
        //创建时间
        info.setCreateTime(System.currentTimeMillis());
        info.setCreateOperatorName(bean.getCreateOperatorName());
        //回调数据
        boolean save = this.service.save(info);
        return save ? info.getId() : null;
    }


    /**
     * 处理结果
     *
     * @param operUserId
     * @param dto
     * @return
     */
    @Override
    public boolean dealWithResults(String operUserId, FeedbackDTO dto) {
        Feedback infoNew = null;
        String[] idArr = dto.getId().split(",");
        boolean flag = true;
        for (String id : idArr) {
            Feedback info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }

            infoNew = new Feedback();
            infoNew.setId(info.getId());
            //处理状态 0:未处理 1:已处理
            if (Objects.nonNull(dto.getIsState())) {
                infoNew.setIsState(1);
            }
            //处理结果
            if (Objects.nonNull(dto.getHandingContent())) {
                infoNew.setHandingContent(dto.getHandingContent());
            } else {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
            }
            infoNew.setUpdateTime(System.currentTimeMillis());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setUpdateOperatorName(dto.getUpdateOperatorName());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 13:07
     * @param null
     * @return null
     * @update
     * @updateTime
     * 更新
     */
    @Override
    public boolean update(String operUserId, FeedbackDTO bean) {
        // 判断通过ID获取的对象是否为空
        Feedback info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        Feedback infoNew = new Feedback();
        //获取用户生成的id
        infoNew.setId(bean.getId());
        //用户昵称
        if (StringUtil.notBlank(bean.getName())) {
            infoNew.setName(bean.getName());
        }
        //联系方式
        infoNew.setMobilePhone(bean.getMobilePhone());
        //投诉时间
        infoNew.setComplaintsTime(bean.getComplaintsTime());
        //投诉内容
        infoNew.setContent(bean.getContent());
        //图片存放路径（短），多图用,隔开
        infoNew.setImgPathList(bean.getImgPathList());
        //满意度 0非常满意 1满意 2一般 3不满意
        infoNew.setSatisFaction(bean.getSatisFaction());
        //分类id
        infoNew.setCateId(bean.getCateId());
        //处理结果
        infoNew.setHandingContent(bean.getHandingContent());
        //备注
        infoNew.setMemo(bean.getMemo());
        //处理状态 0:未处理 1:已处理
        infoNew.setIsState(bean.getIsState());
        //额外参数1
        infoNew.setExtraParams1(bean.getExtraParams1());
        //额外参数2
        infoNew.setExtraParams2(bean.getExtraParams2());
        //额外参数3
        infoNew.setExtraParams3(bean.getExtraParams3());

        //更新者ID
        infoNew.setUpdateOperatorId(operUserId);
        //更新时间
        infoNew.setUpdateTime(System.currentTimeMillis());
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        return this.service.updateById(infoNew);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 13:11
     * @param null
     * @return null
     * @update
     * @updateTime
     * 删除
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        //每个id已逗号分割
        String[] idArr = ids.split(",");
        //遍历id
        for (String id : idArr) {
            //获取id判断是否为空
            Feedback info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            //创建删除实体
            Feedback infoDel = new Feedback();
            //设置删除状态
            infoDel.setId(info.getId());
            infoDel.setDeleteOperatorId(operUserId);
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            infoDel.setDeleteTime(System.currentTimeMillis());


            boolean result = service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }

    @Override
    public List<FeedbackVO> findAll(FeedbackQuery bean) {
        return null;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 13:18
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    @Override
    public PageResult<FeedbackVO> getList(FeedbackQuery params) {
        QueryWrapper<Feedback> queryWrapper = new QueryWrapper<Feedback>();
        queryWrapper.eq("tif.is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("tif.create_time");

        // 根据分组查询
        if (StringUtil.notBlank(params.getName())) {
            queryWrapper.eq("tif.name", params.getName());
        }
        // 根据分组查询
        if (StringUtil.notBlank(params.getCreateOperatorid())) {
            queryWrapper.eq("tif.create_operatorid", params.getCreateOperatorid());
        }

        Page<FeedbackVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<FeedbackVO> page = this.service.getLists(PageParams, queryWrapper);
        //有查询结果
        if (null != page.getRecords() && page.getRecords().size() > 0) {
            for (FeedbackVO info : page.getRecords()) {
                //拼接多图长地址
                if (StringUtil.notBlank(info.getImgPathList())) {
                    List<String> strList = new ArrayList<String>();
                    String[] pictures = info.getImgPathList().split(",");
                    String path = FileUploadUtil.getImgRootPath();
                    for (String pic : pictures) {
                        strList.add(path.concat(pic));
                    }
                    info.setImgPathListUrl(StringUtils.join(strList, ","));
                }
            }
        }
        return new PageResult<>(page);

    }


}


