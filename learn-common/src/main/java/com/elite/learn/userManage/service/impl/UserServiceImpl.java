package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.User;
import com.elite.learn.userManage.mapper.UserMapper;
import com.elite.learn.userManage.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.vo.UserLoginVO;
import com.elite.learn.userManage.vo.UserVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public UserVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }

    @Override
    public IPage<UserVO> getLists(Page<UserVO> pageVo, @Param(Constants.WRAPPER)Wrapper<User> queryWrapper) {
        pageVo.setRecords(this.baseMapper.getLists(pageVo, queryWrapper));
        return pageVo;
    }



    @Override
    public UserLoginVO getConditionInfo(@Param(Constants.WRAPPER) Wrapper<User> queryWrapper) {
        return this.baseMapper.getConditionInfo(queryWrapper);
    }


    /**
     * 条件搜索分页
     * @param pageVo
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<UserVO> getConditionList(Page<UserVO> pageVo, @Param(Constants.WRAPPER)Wrapper<User> queryWrapper) {
        pageVo.setRecords(this.baseMapper.getConditionList(pageVo, queryWrapper));
        return pageVo;
    }
}
