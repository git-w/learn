package com.elite.learn.userManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Feedback;

import com.elite.learn.userManage.vo.FeedbackVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 意见反馈表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
public interface FeedbackMapper extends BaseMapper<Feedback> {

    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:04
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    FeedbackVO getInfo(String id);

    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:05
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<FeedbackVO> getLists(Page<FeedbackVO> pageVo, @Param(Constants.WRAPPER) Wrapper<Feedback> queryWrapper);
}
