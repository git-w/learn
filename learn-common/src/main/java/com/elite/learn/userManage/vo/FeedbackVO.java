package com.elite.learn.userManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 意见反馈表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Data
public class FeedbackVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 用户昵称
     */
    private String name;

    /**
     * 联系方式
     */
    private Integer mobilePhone;


    /**
     * 投诉时间
     */
    private Long complaintsTime;

    /**
     * 投诉内容
     */
    private String content;

    /**
     * 图片存放路径（短），多图用,隔开
     */
    private String imgPathList;


    /**
     * 图片地址长路径
     */
    private String imgUrlPath;

    /**
     * 满意度 0非常满意 1满意 2一般 3不满意
     */
    private Integer satisFaction;

    /**
     * 分类id
     */
    private String cateId;

    /**
     * 处理结果
     */
    private String handingContent;

    /**
     * 备注
     */
    private String memo;

    /**
     * 处理状态 0:未处理 1:已处理
     */
    private Integer isState;



    /**
     * 创建者ID（反馈者）
     */
    private String createOperatorid;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;



    /**
     * 额外参数1
     */
    private String extraParams1;

    /**
     * 额外参数2
     */

    private String extraParams2;

    /**
     * 额外参数3
     */

    private String extraParams3;

    /**
     * 图片长路径
     */
    private String imgPathListUrl;

    /**
     * 分类名称
     */
    private String cateName;

    /**
     * 分类名称
     */
    private String modilePhone;

    /**
     * 分类名称
     */
    private String headimgurl;

    /**
     * 分类名称
     */
    private String nickName;

}
