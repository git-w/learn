package com.elite.learn.userManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户模块---用户信息审核表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Data
public class UserAuditQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 审核状态：0-待审核，1-审核通过 2-审核不通过
     */
    private Integer isAudit;


    /**
     * 错误原因
     */
    private String reason;

}
