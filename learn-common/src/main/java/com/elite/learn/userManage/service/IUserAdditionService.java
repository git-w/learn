package com.elite.learn.userManage.service;

import com.elite.learn.userManage.entity.UserAddition;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.UserAdditionVO;

/**
 * <p>
 * 用户信息---附加表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
public interface IUserAdditionService extends IService<UserAddition> {

    /*
     *
     * @author: leroy
     * @date 2021/12/1 11:52
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    UserAdditionVO getInfo(String id);
}
