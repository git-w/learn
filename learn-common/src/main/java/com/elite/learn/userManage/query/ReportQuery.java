package com.elite.learn.userManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 举报反馈表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Data
public class ReportQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 用户id
     */
    private String userId;
    /**
     * 分类id
     */
    private String cateId;

    /**
     * 用户昵称
     */
    private String name;

    /**
     * 创建时间
     */
    private Long createTime;


    /**
     * 处理状态 0:未处理 1:已处理
     */
    private Integer isState;
}
