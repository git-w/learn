package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IUserAdditionServiceBLL;
import com.elite.learn.userManage.dto.UserAdditionDTO;
import com.elite.learn.userManage.entity.User;
import com.elite.learn.userManage.entity.UserAddition;
import com.elite.learn.userManage.query.UserAdditionQuery;
import com.elite.learn.userManage.service.IUserAdditionService;
import com.elite.learn.userManage.service.IUserService;
import com.elite.learn.userManage.vo.UserAdditionVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description:
 * @Title: UserAdditionServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/6 11:32
 */
@Service
public class UserAdditionServiceBLL implements IUserAdditionServiceBLL {


    @Resource
    private IUserAdditionService service;

    @Resource
    private IUserService userservice;

    @Override
    public UserAddition get(String id) {
        return null;
    }


    @Override
    public UserAdditionVO getInfo(String id) {
        return null;
    }


    @Override
    public String save(String operUserId, UserAdditionDTO bean) {
        return null;
    }


    /**
     * 更新
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2022/1/14 16:08
     * @update
     * @updateTime
     */
    @Override
    public boolean update(String operUserId, UserAdditionDTO bean) {
        QueryWrapper<UserAddition> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", bean.getUserId());
        //获取实体对象
        UserAddition infoNew = new UserAddition();
        User info = new User();
        //调用用户的id
        infoNew.setUserId(bean.getUserId());
        //用户标签，多个英文逗号隔开
        infoNew.setTags(bean.getTags());

        //身份证正面照片地址
        infoNew.setPinCodesConsUrl(bean.getPinCodesConsUrl());
        //身份证反面照片地址
        infoNew.setPinCodesProsUrl(bean.getPinCodesProsUrl());
        //手持身份证
        infoNew.setPinCodesHandUrl(bean.getPinCodesHandUrl());

        //手持身份证
        infoNew.setStature(bean.getStature());

        //学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
        infoNew.setEducationBackground(bean.getEducationBackground());

        //婚姻状况：0-未婚、1-离异、2-丧偶
        infoNew.setMaritalStatus(bean.getMaritalStatus());
        //子女状况：0-无子女、1-有子女
        infoNew.setChildrenStatus(bean.getChildrenStatus());
        //用户所在省份名称
        infoNew.setProvinceName(bean.getProvinceName());
        //用户所在省份名称
        if (StringUtil.isEmpty(infoNew.getProvinceName())) {
            info.setId(infoNew.getUserId());
            info.setProvince(infoNew.getProvinceName());
        }
        //用户所在城市名称
        infoNew.setCityName(bean.getCityName());

        if (StringUtil.isEmpty(infoNew.getCityName())) {
            info.setId(infoNew.getUserId());
            info.setCity(infoNew.getCityName());
        }
        //用户所在省份id
        infoNew.setProvinceId(bean.getProvinceId());
        //用户所在城市id
        infoNew.setCityId(bean.getCityId());
        // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
        infoNew.setHousingStatus(bean.getHousingStatus());
        //购车情况：0-已购车、1-有需要购车、2-其他
        infoNew.setCarStatus(bean.getCarStatus());
        //时长
        infoNew.setDuration(bean.getDuration());
        //头像、独白、语音独白在上传/修改时需要审核
        infoNew.setIntroduce(bean.getIntroduce());
        //语音独白最长60秒，最短5秒
        infoNew.setIntroduceVoiceUrl(bean.getIntroduceVoiceUrl());
        //封面图
        infoNew.setCoverImgUrl(bean.getCoverImgUrl());

        //体重(后加字段)
        infoNew.setWeight(bean.getWeight());

        if (Objects.nonNull(info.getId())) {
            this.userservice.updateById(info);
        }

        return this.service.update(infoNew, wrapper);


    }

    @Override
    public boolean remove(String operUserId, String ids) {
        return false;
    }

    @Override
    public List<UserAdditionVO> findAll(UserAdditionQuery bean) {
        return null;
    }

    @Override
    public PageResult<UserAdditionVO> getList(UserAdditionQuery params) {
        return null;
    }
}