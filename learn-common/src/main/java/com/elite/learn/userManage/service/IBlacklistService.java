package com.elite.learn.userManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Blacklist;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.BlacklistVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 民宿预定-黑名单表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
public interface IBlacklistService extends IService<Blacklist> {
    /**
     * 用户分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<BlacklistVO> selectLists(Page<BlacklistVO> page, @Param(Constants.WRAPPER) Wrapper<Blacklist> queryWrapper);


    /**
     * 用户详情
     *
     * @param queryWrapper
     * @return
     */
    BlacklistVO selectInfo(@Param(Constants.WRAPPER) Wrapper<Blacklist> queryWrapper);
}
