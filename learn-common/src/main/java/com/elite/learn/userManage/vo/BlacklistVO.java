package com.elite.learn.userManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 民宿预定-黑名单表
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
@Data
public class BlacklistVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 用户名称
     */
    private String UserName;

    /**
     * 手机号
     */
    private String ModilePhone;

    /**
     * 更新者ID
     */
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    private String UpdateOperatorName;

    /**
     * 添加时间
     */
    private Long CreateTime;

    /**
     * 更新时间
     */
    private Long UpdateTime;


    /**
     * 备注
     */
    private String Memo;

    /**
     * 备用字段，如果占用，请备注做什么用
     */
    private String ExtraParams1;

    /**
     * 备用字段，如果占用，请备注做什么用
     */
    private String ExtraParams2;

    /**
     * 备用字段，如果占用，请备注做什么用
     */
    private String ExtraParams3;

    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;

}
