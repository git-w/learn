package com.elite.learn.userManage.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Title :AddressLocationDTO.java
 * @Package: com.elite.learn.userManage.dto
 * @Description:
 * @author: leroy
 * @data: 2021/12/14 17:06
 */
@Data
public class AddressLocationDTO {
    /**
     * 经度
     */
    @NotNull(message = "经度,参数为空")
    private String lng;

    /**
     * 纬度
     */
    @NotNull(message = "纬度,参数为空")
    private String lat;
}