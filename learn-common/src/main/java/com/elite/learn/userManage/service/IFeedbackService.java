package com.elite.learn.userManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Feedback;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.FeedbackVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 意见反馈表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
public interface IFeedbackService extends IService<Feedback> {

    /**
     *
     * @author: leroy
     * @date 2021/11/26 11:22
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    FeedbackVO getInfo(String id);

    /**
     *
     * @author: leroy
     * @date 2021/11/26 13:21
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    IPage<FeedbackVO> getLists(Page<FeedbackVO> pageParams, @Param(Constants.WRAPPER) Wrapper<Feedback> queryWrapper);
}
