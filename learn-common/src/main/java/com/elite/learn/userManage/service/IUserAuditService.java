package com.elite.learn.userManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.UserAudit;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.UserAuditVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户模块---用户信息审核表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
public interface IUserAuditService extends IService<UserAudit> {


    /**
     * 分页
     * @author: leroy
     * @date 2021/12/9 11:44
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     */

    IPage<UserAuditVO> getLists(Page<UserAuditVO> pageParams, @Param(Constants.WRAPPER) Wrapper<UserAuditVO> queryWrapper);




    /**
     * 详情
     * @author: leroy
     * @date 2021/12/9 11:48
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    UserAuditVO getInfo(String id);
}
