package com.elite.learn.userManage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户信息---附加表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Data
public class UserMateSelectionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 年龄可选范围：18-99（最小年龄选不限、最多年龄选30，则显示：30岁及以下）
     */
    private String ageOptionalRange;

    /**
     * 身高可选范围：145-250（最低选160cm、最高选不限，则显示：160cm及以上）
     */
    private String heightOptionalRange;

    /**
     * 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
     */
    private Integer educationBackground;

    /**
     * 婚姻状况：0-未婚、1-离异、2-丧偶
     */
    private Integer maritalStatus;

    /**
     * 子女状况：0-无子女、1-有子女
     */
    private Integer childrenStatus;

    /**
     * 用户所在省份名称
     */
    private String provinceName;

    /**
     * 用户所在城市名称
     */
    private String cityName;

    /**
     * 用户所在省份id
     */
    private String provinceId;

    /**
     * 用户所在城市id
     */
    private String cityId;

    /**
     * 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
     */
    private Integer housingStatus;

    /**
     * 购车情况：0-已购车、1-有需要购车、2-其他
     */
    private Integer carStatus;


}
