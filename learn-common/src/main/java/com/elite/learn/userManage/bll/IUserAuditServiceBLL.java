package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.UserAuditDTO;
import com.elite.learn.userManage.entity.UserAudit;
import com.elite.learn.userManage.query.UserAuditQuery;
import com.elite.learn.userManage.vo.UserAuditVO;

public interface IUserAuditServiceBLL extends ICommonServiceBLL<UserAudit, UserAuditDTO, UserAuditVO, UserAuditQuery> {


    /**
     * 修改审核状态 isAudit
     * 审核状态：0-待审核，1-审核通过 2-审核不通过
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateIsAudit(String operUserId, UserAuditDTO params);


    /**
     *查看用户最新审核状态
     *
     * @param operUserId
     * @return
     */
    UserAuditVO getUserAudit(String operUserId);
}
