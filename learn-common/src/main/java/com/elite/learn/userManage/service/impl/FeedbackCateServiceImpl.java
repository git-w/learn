package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.FeedbackCate;
import com.elite.learn.userManage.mapper.FeedbackCateMapper;
import com.elite.learn.userManage.service.IFeedbackCateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.vo.FeedbackCateVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户意见反馈分类表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Service
public class FeedbackCateServiceImpl extends ServiceImpl<FeedbackCateMapper, FeedbackCate> implements IFeedbackCateService {


    /**
     *详情
     * @author: leroy
     * @date 2021/12/5 19:46
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public FeedbackCateVO getInfo(String id) {
        return baseMapper.getInfo(id);
    }



    /**
     *分页
     * @author: leroy
     * @date 2021/12/5 19:46
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<FeedbackCateVO> getLists(Page<FeedbackCateVO> pageParams, @Param((Constants.WRAPPER))Wrapper<FeedbackCate> queryWrapper) {

        pageParams.setRecords(this.baseMapper.getLists(pageParams, queryWrapper));
        return pageParams;
    }


    /**
     *
     * @author: leroy
     * @date 2021/12/6 19:21
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public List<FeedbackCateVO> findAll(@Param(Constants.WRAPPER)Wrapper<FeedbackCate> queryWrapper) {
        return this.baseMapper.findAll(queryWrapper);
    }


}
