package com.elite.learn.userManage.query;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户信息---附加表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Data
public class UserMateSelectionQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 年龄可选范围：18-99（最小年龄选不限、最多年龄选30，则显示：30岁及以下）
     */
    private String ageOptionalRange;




}
