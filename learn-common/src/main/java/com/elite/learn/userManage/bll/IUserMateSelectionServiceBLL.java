package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.UserMateSelectionDTO;
import com.elite.learn.userManage.entity.UserMateSelection;
import com.elite.learn.userManage.query.UserMateSelectionQuery;
import com.elite.learn.userManage.vo.UserMateSelectionVO;

public interface IUserMateSelectionServiceBLL extends ICommonServiceBLL<UserMateSelection, UserMateSelectionDTO, UserMateSelectionVO, UserMateSelectionQuery> {
}
