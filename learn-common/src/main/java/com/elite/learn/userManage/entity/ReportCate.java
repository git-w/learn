package com.elite.learn.userManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 用户举报反馈分类表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_report_cate")
public class ReportCate implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */

    private String id;

    /**
     * 分类名称
     */

    private String cateName;

    /**
     * 排序 数值越大越靠前
     */
    private Integer sort;

    /**
     * 备注
     */
    private String memo;

    /**
     * 是否上线 0是 1否
     */
    private Integer isState;

    /**
     * 删除标识 0:未删除 1:删除
     */
    private Integer isDelete;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;

    /**
     * 额外参数1
     */
    private String extraParams1;


}
