package com.elite.learn.userManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户意见反馈分类表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Data
public class FeedbackCateQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 分类名称
     */
    private String cateName;
    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 是否上线 0是 1否
     */
    private Integer isState;



}
