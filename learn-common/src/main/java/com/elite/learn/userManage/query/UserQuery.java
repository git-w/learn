package com.elite.learn.userManage.query;


import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;


/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-30
 */
@Data
public class UserQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 真实姓名
     */
    private String trueName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 是否已审核：0-是，1-否
     */
    private Integer IsAudit;

    /**
     * 账号状态 0:正常 1:冻结 2是禁言 3拉黑
     */
    private Integer IsState;

    /**
     *用户类型
     */
    private Integer IsType;

    /**
     *用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    private Integer sex;


    /**
     * 真实姓名
     */
    private String province;

    /**
     * 市区名称
     */
    private String city;

    /**
     * 用户昵称
     */
    private String nickName;


    /**
     * 是否已审核：0-是，1-否
     */
    private Integer isAudit;
    /**
     * 用户等级 1-游客（普通用户）2-会员vip
     */
    private Integer level;





    /**
     * 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
     */
    private Integer housingStatus;

    /**
     * 婚姻状况：0-未婚、1-离异、2-丧偶
     */
    private Integer maritalStatus;

    /**
     * 子女状况：0-无子女、1-有子女
     */
    private Integer childrenStatus;

    /**
     * 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
     */
    private Integer educationBackground;

    /**
     * 身高最大
     */
    private Integer bigStature;
    /**
     * 身高最小
     */
    private Integer smallStature;
    /**
     * 年龄最大
     */
    private Integer bigAge;
    /**
     * 年龄最大
     */
    private Integer smallAge;
    /**
     * 浏览量
     */
    private Integer viewsNum;
}
