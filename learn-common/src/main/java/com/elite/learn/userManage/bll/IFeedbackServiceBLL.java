package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.service.ICommonServiceBLL;;
import com.elite.learn.userManage.dto.FeedbackDTO;
import com.elite.learn.userManage.entity.Feedback;
import com.elite.learn.userManage.query.FeedbackQuery;
import com.elite.learn.userManage.vo.FeedbackVO;

public interface IFeedbackServiceBLL extends ICommonServiceBLL<Feedback, FeedbackDTO, FeedbackVO, FeedbackQuery> {

    /**
     * 反馈处理结果
     * @author: leroy
     * @date 2021/12/7 17:52
     * @param dto
     * @return null
     * @update
     * @updateTime
     */
    boolean dealWithResults(String operUserId, FeedbackDTO dto);
}
