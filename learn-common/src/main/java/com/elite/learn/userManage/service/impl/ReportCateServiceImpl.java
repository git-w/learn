package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.elite.learn.userManage.entity.ReportCate;
import com.elite.learn.userManage.mapper.ReportCateMapper;
import com.elite.learn.userManage.service.IReportCateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.vo.ReportCateVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户举报反馈分类表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Service
public class ReportCateServiceImpl extends ServiceImpl<ReportCateMapper, ReportCate> implements IReportCateService {


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:06
     * @update
     * @updateTime 详情
     */
    @Override
    public ReportCateVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     * @param pageParams
     * @return null
     * @author: leroy
     * @date 2021/12/5 21:22
     * @update
     * @updateTime
     */
    @Override
    public IPage<ReportCateVO> getLists(Page<ReportCateVO> pageParams, @Param(Constants.WRAPPER) Wrapper<ReportCate> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getLists(pageParams, queryWrapper));
        return pageParams;
    }



    /**
     *查询全部
     * @author: leroy
     * @date 2021/12/6 16:14
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public List<ReportCateVO> findAll(@Param(Constants.WRAPPER) Wrapper<ReportCate> queryWrapper) {
        return this.baseMapper.findAll(queryWrapper);
    }
}