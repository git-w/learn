package com.elite.learn.userManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.elite.learn.userManage.entity.Report;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.ReportVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 举报反馈表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
public interface IReportService extends IService<Report> {

    /**
     *
     * @author: leroy
     * @date 2021/11/26 14:03
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    ReportVO getInfo(String id);


    /**
     *
     * @author: leroy
     * @date 2021/11/26 14:59
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    IPage<ReportVO> getLists(Page<ReportVO> pageVo, @Param(Constants.WRAPPER) Wrapper<Report> queryWrapper);

}
