package com.elite.learn.userManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.ReportCate;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.ReportCateVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户举报反馈分类表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
public interface IReportCateService extends IService<ReportCate> {

    /**
     *
     * @author: leroy
     * @date 2021/11/26 13:24
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    ReportCateVO getInfo(String id);

    /**
     *
     * @author: leroy
     * @date 2021/11/26 13:58
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    IPage<ReportCateVO> getLists(Page<ReportCateVO> pageVo, @Param(Constants.WRAPPER) Wrapper<ReportCate> queryWrapper);

    /**
     *c查询所有
     * @author: leroy
     * @date 2021/12/6 17:19
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */

    List<ReportCateVO> findAll(@Param(Constants.WRAPPER)Wrapper<ReportCate> queryWrapper);
}
