package com.elite.learn.userManage.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Title :WxPhoneNumberDto.java
 * @Package: com.elite.learn.userManage.dto
 * @Description:
 * @author: leroy
 * @data: 2021/12/1 18:57
 */
@Data
public class WxPhoneNumberDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * EncryptedData
     */
    @NotBlank(message = "EncryptedData,参数为空")
    private String EncryptedData;

    /**
     * Iv
     */
    @NotBlank(message = "Iv,参数为空")
    private String Iv;
    /**
     * Session_key
     */
    @NotBlank(message = "Session_key,参数为空")
    private String Session_key;
}