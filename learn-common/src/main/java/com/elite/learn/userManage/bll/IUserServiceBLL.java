package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.UserDTO;
import com.elite.learn.userManage.entity.User;
import com.elite.learn.userManage.query.UserQuery;
import com.elite.learn.userManage.vo.UserLoginVO;
import com.elite.learn.userManage.vo.UserVO;

public interface IUserServiceBLL extends ICommonServiceBLL<User, UserDTO, UserVO, UserQuery> {

    /**
     * 条件搜索 分页
     *
     * @param params
     * @return
     */
    PageResult<UserVO> getConditionList(UserQuery params);

    /**
     * 修改用户登录时间
     *
     * @param id
     */
    void updateLoginTime(String id);

    /**
     * 微信小程序查看用户
     *
     * @param routineOpenid
     * @return UserLoginVo
     */
    UserLoginVO login(String routineOpenid);

    /**
     * 修改用户手机号
     *
     * @param UserId
     */
    boolean updateModilePhone(String UserId, String mobilePhone);


    /**
     * 多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateMultiIsEnabled(String operUserId, BaseParams params);


    /**
     * 多选修改状态 是否推荐 0是 1否
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateMultiIsType(String operUserId, BaseParams params);


    /**
     * 鹊桥牵线
     *
     * @param UserId
     */
    boolean getFrequency(String operUserId, String UserId);


    /**
     * 鹊桥牵线
     *
     * @param UserId
     */
    boolean saveFrequency(String operUserId, String UserId);

    /**
     * 修改浏览次数
     *
     * @param id
     */
    void updateViewsNum(String id, Integer viewsNum);
}
