package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.date.DateUtil;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.IDCardUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.messageManage.bll.IUserMessageServiceBLL;
import com.elite.learn.userManage.bll.IUserAuditServiceBLL;
import com.elite.learn.userManage.bll.IUserServiceBLL;
import com.elite.learn.userManage.dto.UserAuditDTO;
import com.elite.learn.userManage.entity.User;
import com.elite.learn.userManage.entity.UserAddition;
import com.elite.learn.userManage.entity.UserAudit;
import com.elite.learn.userManage.query.UserAuditQuery;
import com.elite.learn.userManage.service.IUserAdditionService;
import com.elite.learn.userManage.service.IUserAuditService;
import com.elite.learn.userManage.service.IUserService;
import com.elite.learn.userManage.vo.UserAdditionVO;
import com.elite.learn.userManage.vo.UserAuditVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description:
 * @Title: UserAuditServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/6 11:32
 */
@Service
public class UserAuditServiceBLL implements IUserAuditServiceBLL {


    @Resource
    private IUserAuditService service;
    //用户
    @Resource
    private IUserServiceBLL userServiceBLL;
    @Resource
    private IUserService userService;
    @Resource
    private IUserAdditionService userAdditionService;
    // 系统消息-用户消息表
    @Resource
    private IUserMessageServiceBLL userMessageServiceBLL;


    /**
     * 主键id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/9 11:47
     * @update
     * @updateTime
     */
    @Override
    public UserAudit get(String id) {
        return this.service.getById(id);
    }


    /**
     * 详情
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/9 11:48
     * @update
     * @updateTime
     */
    @Override
    public UserAuditVO getInfo(String id) {
        return this.service.getInfo(id);
    }


    /**
     * 添加
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2022/1/14 16:11
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, UserAuditDTO bean) {
        //获取实体对象
        UserAudit info = new UserAudit();
        info.setId(IdUtils.simpleUUID());
        // 审核状态：0-待审核，1-审核通过 2-审核不通过
        info.setIsAudit(0);
        //身份证正面照片地址
        info.setPinCodesConsUrl(bean.getPinCodesConsUrl());
        //身份证反面照片地址
        info.setPinCodesProsUrl(bean.getPinCodesProsUrl());
        //手持身份证
        info.setPinCodesHandUrl(bean.getPinCodesHandUrl());
        //头像、独白、语音独白在上传/修改时需要审核
        info.setIntroduce(bean.getIntroduce());
        //用户头像
        info.setHeadimgurl(bean.getHeadimgurl());

        //身份证
        info.setPinCodes(bean.getPinCodes());
        //名称
        info.setTrueName(bean.getTrueName());
      /*  //身份证正面照片地址
        info.setOriginalPinCodesProsUrl(bean.getOriginalPinCodesProsUrl());
        //身份证反面照片地址
        info.setOriginalPinCodesConsUrl(bean.getOriginalPinCodesConsUrl());
        //手持身份证
        info.setOriginalPinCodesHandUrl(bean.getOriginalPinCodesHandUrl());
        //头像、独白、语音独白在上传/修改时需要审核
        info.setOriginalIntroduce(bean.getOriginalIntroduce());
        //用户头像
        info.setOriginalHeadimgurl(bean.getOriginalHeadimgurl());
        //错误原因
        info.setReason(bean.getReason());*/

        //创建者信息
        info.setCreateTime(System.currentTimeMillis());
        info.setCreateOperatorId(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        //返回数据
        boolean result = this.service.save(info);
        //判断是否成功
        return result ? info.getId() : null;
    }

    @Override
    public boolean update(String operUserId, UserAuditDTO bean) {


        return false;
    }


    @Override
    public boolean remove(String operUserId, String ids) {

        return false;
    }

    @Override
    public List<UserAuditVO> findAll(UserAuditQuery bean) {
        return null;
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/11 12:11
     * @update
     * @updateTime
     */
    @Override
    public PageResult<UserAuditVO> getList(UserAuditQuery params) {
        QueryWrapper<UserAuditVO> queryWrapper = new QueryWrapper<UserAuditVO>();
        queryWrapper.isNotNull("tiua.create_time");
        queryWrapper.orderByDesc("tiua.create_time");


        Page<UserAuditVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<UserAuditVO> page = service.getLists(PageParams, queryWrapper);
        List<UserAuditVO> list = page.getRecords();
        if (list != null && list.size() > 0) {
            for (UserAuditVO bean : list) {
                if (StringUtil.isNotEmpty(bean.getPinCodesProsUrl())) {
                    bean.setPinCodesProsUrlPath(FileUploadUtil.getImgRootPath() + bean.getPinCodesProsUrl());
                }
                if (StringUtil.isNotEmpty(bean.getPinCodesConsUrl())) {
                    bean.setPinCodesConsUrlPath(FileUploadUtil.getImgRootPath() + bean.getPinCodesConsUrl());
                }
                if (StringUtil.isNotEmpty(bean.getPinCodesHandUrl())) {
                    bean.setPinCodesHandUrlPath(FileUploadUtil.getImgRootPath() + bean.getPinCodesHandUrl());
                }

                if (StringUtil.isNotEmpty(bean.getOriginalPinCodesProsUrl())) {
                    bean.setOriginalPinCodesProsUrlPath(FileUploadUtil.getImgRootPath() + bean.getOriginalPinCodesProsUrl());
                }
                if (StringUtil.isNotEmpty(bean.getOriginalPinCodesConsUrl())) {
                    bean.setOriginalPinCodesConsUrlPath(FileUploadUtil.getImgRootPath() + bean.getOriginalPinCodesConsUrl());
                }
                if (StringUtil.isNotEmpty(bean.getOriginalPinCodesHandUrl())) {
                    bean.setOriginalPinCodesHandUrlPath(FileUploadUtil.getImgRootPath() + bean.getOriginalPinCodesHandUrl());
                }
            }
        }
        return new PageResult<>(page);
    }

    /**
     * 修改审核状态 isAudit
     * 审核状态：0-待审核，1-审核通过 2-审核不通过
     *
     * @param operUserId
     * @param bean
     * @return
     */
    @Override
    public boolean updateIsAudit(String operUserId, UserAuditDTO bean) {
        // 判断通过ID获取的对象是否为空
        UserAudit info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        UserAudit infoNew = new UserAudit();
        //用户生成id
        infoNew.setId(bean.getId());
        //处理结果
        if (StringUtil.notBlank(bean.getReason())) {
            infoNew.setReason(bean.getReason());
        }
        // 审核状态：0-待审核，1-审核通过 2-审核不通过
        if (Objects.nonNull(bean.getIsAudit())) {
            infoNew.setIsAudit(bean.getIsAudit());
            if (bean.getIsAudit() == 1) {
                //审核通过修改用户表数据
                User user = userServiceBLL.get(info.getCreateOperatorId());
                UserAdditionVO userAdditionVo = userAdditionService.getInfo(info.getCreateOperatorId());
                UserAddition userAddition = new UserAddition();
                BeanUtils.copyProperties(userAdditionVo, userAddition);
                //交换头像信息
                if (StringUtils.isNotEmpty(info.getHeadimgurl())) {
                    infoNew.setOriginalHeadimgurl(user.getHeadimgurl());
                    user.setHeadimgurl(info.getHeadimgurl());

                }
                //交换信息
                if (StringUtils.isNotEmpty(info.getPinCodesConsUrl())) {
                    infoNew.setOriginalPinCodesConsUrl(userAddition.getPinCodesConsUrl());
                    userAddition.setPinCodesConsUrl(info.getPinCodesConsUrl());
                }

                //交换信息
                if (StringUtils.isNotEmpty(info.getPinCodesProsUrl())) {
                    infoNew.setOriginalPinCodesProsUrl(userAddition.getPinCodesProsUrl());
                    userAddition.setPinCodesProsUrl(info.getPinCodesProsUrl());
                }

                //交换信息
                if (StringUtils.isNotEmpty(info.getPinCodesHandUrl())) {
                    infoNew.setOriginalPinCodesHandUrl(userAddition.getPinCodesHandUrl());
                    userAddition.setPinCodesHandUrl(info.getPinCodesHandUrl());
                }

                //交换信息
                if (StringUtils.isNotEmpty(info.getIntroduce())) {
                    infoNew.setOriginalIntroduce(userAddition.getIntroduce());
                    userAddition.setIntroduce(info.getIntroduce());
                }

                //身份证
                if (StringUtils.isNotEmpty(info.getPinCodes())) {
                    user.setPinCodes(info.getPinCodes());

                    //获取身份证上生日
                    String birthday = IDCardUtil.getBirthday(info.getPinCodes());
                    //获取时间戳
                    long birth = DateUtil.getMillionSecondLong(birthday);
                    //对象赋值
                    user.setBirthday(birth);
                    //用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
                    Integer sex = IDCardUtil.getSex(info.getPinCodes());
                    user.setSex(sex);

                }


                //真实姓名
                if (StringUtils.isNotEmpty(info.getTrueName())) {
                    user.setTrueName(info.getTrueName());
                }
                user.setIsAudit(0);
                this.userService.updateById(user);

                QueryWrapper<UserAddition> wrapper = new QueryWrapper<>();
                wrapper.eq("user_id", info.getCreateOperatorId());
                this.userAdditionService.update(userAddition, wrapper);
                //通知的话选择新消息类型消息类型:1审核成功通知 2审核失败通  3会员卡购买成功知 4新用户注册成功通知  5免费会员卡领取成功通知
                this.userMessageServiceBLL.send(1, info.getCreateOperatorId());
            } else if (bean.getIsAudit() == 2) {
                //通知的话选择新消息类型消息类型:1审核成功通知 2审核失败通  3会员卡购买成功知 4新用户注册成功通知  5免费会员卡领取成功通知
                this.userMessageServiceBLL.send(2, info.getCreateOperatorId());
            }
        }
        //更新者ID
        infoNew.setAuditOperatorId(operUserId);
        infoNew.setAuditOperatorName(bean.getAuditOperatorName());
        infoNew.setAuditTime(System.currentTimeMillis());
        return this.service.updateById(infoNew);
    }


    /**
     * 查看用户最新审核信息+
     *
     * @param operUserId
     * @return
     */
    @Override
    public UserAuditVO getUserAudit(String operUserId) {
        QueryWrapper<UserAudit> queryWrapper = new QueryWrapper<UserAudit>();
        queryWrapper.eq("create_operator_id", operUserId);
        queryWrapper.orderByDesc("create_time");
        List<UserAudit> list = this.service.list(queryWrapper);
        UserAuditVO bean = null;
        if (list != null && list.size() > 0) {
            UserAudit info = list.get(0);
            bean = new UserAuditVO();
            BeanUtils.copyProperties(info, bean);
            if (StringUtil.isNotEmpty(bean.getPinCodesProsUrl())) {
                bean.setPinCodesProsUrlPath(FileUploadUtil.getImgRootPath() + bean.getPinCodesProsUrl());
            }
            if (StringUtil.isNotEmpty(bean.getPinCodesConsUrl())) {
                bean.setPinCodesConsUrlPath(FileUploadUtil.getImgRootPath() + bean.getPinCodesConsUrl());
            }
            if (StringUtil.isNotEmpty(bean.getPinCodesHandUrl())) {
                bean.setPinCodesHandUrlPath(FileUploadUtil.getImgRootPath() + bean.getPinCodesHandUrl());
            }

            if (StringUtil.isNotEmpty(bean.getOriginalPinCodesProsUrl())) {
                bean.setOriginalPinCodesProsUrlPath(FileUploadUtil.getImgRootPath() + bean.getOriginalPinCodesProsUrl());
            }
            if (StringUtil.isNotEmpty(bean.getOriginalPinCodesConsUrl())) {
                bean.setOriginalPinCodesConsUrlPath(FileUploadUtil.getImgRootPath() + bean.getOriginalPinCodesConsUrl());
            }
            if (StringUtil.isNotEmpty(bean.getOriginalPinCodesHandUrl())) {
                bean.setOriginalPinCodesHandUrlPath(FileUploadUtil.getImgRootPath() + bean.getOriginalPinCodesHandUrl());
            }
        }

        return bean;
    }
}