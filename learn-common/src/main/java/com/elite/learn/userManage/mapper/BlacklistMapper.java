package com.elite.learn.userManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Blacklist;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.userManage.vo.BlacklistVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 民宿预定-黑名单表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
public interface BlacklistMapper extends BaseMapper<Blacklist> {
    /**
     * 用户自定义分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    List<BlacklistVO> selectLists(Page<BlacklistVO> page, @Param(Constants.WRAPPER) Wrapper<Blacklist> queryWrapper);

    /**
     * 用户详情
     *
     * @param queryWrapper
     * @return
     */
    BlacklistVO selectInfo(@Param(Constants.WRAPPER) Wrapper<Blacklist> queryWrapper);
}
