package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IFeedbackCateServiceBLL;
import com.elite.learn.userManage.dto.FeedbackCateDTO;
import com.elite.learn.userManage.entity.FeedbackCate;
import com.elite.learn.userManage.query.FeedbackCateQuery;
import com.elite.learn.userManage.service.IFeedbackCateService;
import com.elite.learn.userManage.vo.FeedbackCateVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 用户意见反馈分类表
 * @Title: InfoFeedbackCateServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/26 8:55
 */
@Service
public class FeedbackCateServiceBLL implements IFeedbackCateServiceBLL {


    @Resource
    private IFeedbackCateService service;


    /*
     *
     * @author: leroy
     * @date 2021/11/26 9:42
     * @param null
     * @return null
     * @update
     * @updateTime
     * d 主键
     */
    @Override
    public FeedbackCate get(String id) {
        return this.service.getById(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 9:56
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public FeedbackCateVO getInfo(String id) {

        return this.service.getInfo(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 10:02
     * @param null
     * @return null
     * @update
     * @updateTime
     * 添加数据
     */
    @Override
    public String save(String operUserId, FeedbackCateDTO bean) {
        //创建实体对象
        FeedbackCate info = new FeedbackCate();
        //获取用户生成的id
        info.setId(IdUtils.simpleUUID());
        //分类名称
        info.setCateName(bean.getCateName());
        //父分类ID
        info.setParentId(bean.getParentId());
        //排序 数值越大越靠前
        info.setSort(bean.getSort());
        //备注
        info.setMemo(bean.getMemo());
        //是否上线 0是 1否
        info.setIsState(bean.getIsState());
        //额外参数1
        info.setExtraParams1(bean.getExtraParams1());
        //删除标识 0:未删除 1:删除
        info.setIsDelete(0);
        //更新者ID
        info.setCreateOperatorId(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        //更新时间
        info.setCreateTime(System.currentTimeMillis());


        //数据返回业务层

        boolean flag = this.service.save(info);
        return flag ? info.getId() : null;

    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 10:15
     * @param null
     * @return null
     * @update
     * @updateTime
     * 更新数据
     */
    @Override
    public boolean update(String operUserId, FeedbackCateDTO bean) {
        // 判断通过ID获取的对象是否为空
        FeedbackCate info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        FeedbackCate infoNew = new FeedbackCate();
        //获取用户生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //分类名称
        if (StringUtil.notBlank(bean.getCateName())) {
            infoNew.setCateName(bean.getCateName());
        }
        //父分类ID
        infoNew.setParentId(bean.getParentId());
        //排序 数值越大越靠前
        infoNew.setSort(bean.getSort());
        //备注
        infoNew.setMemo(bean.getMemo());
        //是否上线 0是 1否
        infoNew.setIsState(bean.getIsState());
        //额外参数1
        infoNew.setExtraParams1(bean.getExtraParams1());

        //更新者ID
        infoNew.setUpdateOperatorId(operUserId);
        //更新者名称
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        //更新时间
        infoNew.setUpdateTime(System.currentTimeMillis());

        //数据返回业务层
        return this.service.updateById(infoNew);

    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 10:20
     * @param null
     * @return null
     * @update
     * @updateTime
     * 删除
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        FeedbackCate infoDel = null;
        //分割id
        String[] idArr = ids.split(",");
        //遍历id判断是否为空，弹出本次循环
        for (String id : idArr) {
            FeedbackCate info = this.get(id);
            if (!Objects.nonNull(info)) {
                continue;
            }
            infoDel = new FeedbackCate();
            infoDel.setId(info.getId());
            //设置删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            //删除id
            infoDel.setDeleteOperatorId(operUserId);

            boolean result = service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /*
     * 查询所有
     * @author: leroy
     * @date 2021/12/6 17:13
     * @param null
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public List<FeedbackCateVO> findAll(FeedbackCateQuery bean) {
        QueryWrapper<FeedbackCate> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("is_state", DeleteType.NOTDELETE.getCode());
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        List<FeedbackCateVO> list = this.service.findAll(queryWrapper);
        return list;
    }


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/6 8:41
     * @update
     * @updateTime 分页
     */
    @Override
    public PageResult<FeedbackCateVO> getList(FeedbackCateQuery params) {

        QueryWrapper<FeedbackCate> queryWrapper = new QueryWrapper<FeedbackCate>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("create_time");
        // 根据分组查询
        if (StringUtil.notBlank(params.getCateName())) {
            queryWrapper.eq("cate_name", params.getCateName());
        }

        Page<FeedbackCateVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<FeedbackCateVO> page = this.service.getLists(PageParams, queryWrapper);
        return new PageResult<>(page);
    }

    /**
     * 批量上下线
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean updateMultiIsEnabled(String operUserId, BaseParams params) {
        FeedbackCate infoNew = null;
        String[] idsArr = params.getIds().split(",");
        boolean flag = true;
        for (String id : idsArr) {
            FeedbackCate info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }

            infoNew = new FeedbackCate();
            infoNew.setId(info.getId());
            //状态   0:启用   1:停用
            if (Objects.nonNull(params.getIsState())) {
                infoNew.setIsState(params.getIsState());
            } else {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
            }
            infoNew.setUpdateTime(System.currentTimeMillis());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }
}
