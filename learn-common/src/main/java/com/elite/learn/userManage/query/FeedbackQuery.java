package com.elite.learn.userManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 意见反馈表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Data
public class FeedbackQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 用户昵称
     */
    private String name;


    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 处理状态 0:未处理 1:已处理
     */
    private Integer isState;
    /**
     * 创建者ID（反馈者）
     */
    private String createOperatorid;
}
