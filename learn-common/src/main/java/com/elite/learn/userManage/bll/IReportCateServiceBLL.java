package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.ReportCateDTO;
import com.elite.learn.userManage.entity.ReportCate;
import com.elite.learn.userManage.query.ReportCateQuery;
import com.elite.learn.userManage.vo.ReportCateVO;

public interface IReportCateServiceBLL extends ICommonServiceBLL<ReportCate, ReportCateDTO, ReportCateVO, ReportCateQuery> {
    /**
     * 多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateMultiIsEnabled(String operUserId, BaseParams params);
}
