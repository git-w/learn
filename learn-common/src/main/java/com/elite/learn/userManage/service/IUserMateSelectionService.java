package com.elite.learn.userManage.service;

import com.elite.learn.userManage.entity.UserMateSelection;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.UserMateSelectionVO;

/**
 * <p>
 * 用户信息---附加表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
public interface IUserMateSelectionService extends IService<UserMateSelection> {

    /*
     *
     * @author: leroy
     * @date 2021/12/1 14:10
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    UserMateSelectionVO getInfo(String id);
}
