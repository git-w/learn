package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IBlacklistServiceBLL;
import com.elite.learn.userManage.dto.BlacklistDTO;
import com.elite.learn.userManage.entity.Blacklist;
import com.elite.learn.userManage.query.BlacklistQuery;
import com.elite.learn.userManage.service.IBlacklistService;
import com.elite.learn.userManage.vo.BlacklistVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
public class BlacklistServiceBLL implements IBlacklistServiceBLL {


    @Resource
    private IBlacklistService service;


    /**
     * 查看详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public Blacklist get(String id) {
        if (StringUtil.isNotEmpty(id)) {
            Blacklist info = this.service.getById(id);
            return info;
        }
        return null;
    }


    /**
     * 查看详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public BlacklistVO getInfo(String id) {
        if (StringUtil.isNotEmpty(id)) {
            QueryWrapper<Blacklist> wrapper = new QueryWrapper<>();
            wrapper.orderByDesc("ib.CreateTime");
            wrapper.eq("ib.IsDelete", DeleteType.NOTDELETE.getCode());
            wrapper.like("ib.ID", id);
            BlacklistVO info = this.service.selectInfo(wrapper);
            return info;
        }
        return null;
    }


    /**
     * 添加
     *
     * @param operBlacklistId 操作人员标识
     * @param bean            对象
     * @return
     */
    @Override
    public String save(String operBlacklistId, BlacklistDTO bean) {
        // 获取uuid
        String id = IdUtils.simpleUUID();
        Blacklist info = new Blacklist();
        info.setId(id);
        info.setModilePhone(bean.getModilePhone()); //手机号
        info.setUserName(bean.getUserName());//用户名称

        info.setIsDelete(DeleteType.NOTDELETE.getCode());//查询没有被删除的
        info.setCreateTime(System.currentTimeMillis());//创建时间
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateOperatorID(operBlacklistId);
        boolean flag = this.service.save(info);
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code, BasePojectExceptionCodeEnum.SaveFail.msg);
        }
        return info.getId();
    }


    /**
     * 修改
     *
     * @param operBlacklistId 操作人员标识
     * @param bean            对象
     * @return
     */
    @Override
    public boolean update(String operBlacklistId, BlacklistDTO bean) {
        // 判断通过ID获取的对象是否为空
        Blacklist info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        // 创建对象
        Blacklist infoNew = new Blacklist();
        infoNew.setId(bean.getId());
        // 修改用户注册时使用个的帐号
        if (StringUtil.notBlank(bean.getUserName())) {
            infoNew.setUserName(bean.getUserName());
        }
        // 修改手机号
        if (StringUtil.notBlank(bean.getModilePhone())) {
            infoNew.setModilePhone(bean.getModilePhone());
        }

        // 设置修改者信息
        infoNew.setUpdateOperatorID(operBlacklistId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        return   this.service.updateById(infoNew);

    }

    /**
     * 删除
     *
     * @param operBlacklistId 操作人员标识
     * @param ids             对象标识
     * @return
     */
    @Override
    public boolean remove(String operBlacklistId, String ids) {
        boolean flag = true;
        String[] idsArr = ids.split(",");
        for (String id : idsArr) {
            Blacklist info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            Blacklist infoDel = new Blacklist();
            infoDel.setId(info.getId());
            // 删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            // 删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            // 删除者id
            infoDel.setDeleteOperatorID(operBlacklistId);
            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     * 查看所有
     *
     * @param bean
     * @return
     */
    @Override
    public List<BlacklistVO> findAll(BlacklistQuery bean) {
        return null;
    }


    /**
     * 分页
     *
     * @param params
     * @return
     */
    @Override
    public PageResult<BlacklistVO> getList(BlacklistQuery params) {
        QueryWrapper<Blacklist> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("ib.CreateTime");
        queryWrapper.eq("ib.IsDelete", DeleteType.NOTDELETE.getCode());

        if (StringUtil.isNotEmpty(params.getModilePhone())) {
            queryWrapper.eq("ib.ModilePhone", params.getModilePhone());
        }
        if (StringUtil.isNotEmpty(params.getUserName())) {
            queryWrapper.like("ib.UserName", params.getUserName());
        }
        if (null != params.getCreateTimeStart()) {
            queryWrapper.gt("ib.CreateTime", params.getCreateTimeStart());
        }
        if (null != params.getCreateTimeEnd()) {
            queryWrapper.lt("ib.CreateTime", params.getCreateTimeEnd());
        }
        Page<BlacklistVO> PageParams = new Page<BlacklistVO>(params.getPageNum(), params.getPageSize());
        IPage<BlacklistVO> page = this.service.selectLists(PageParams, queryWrapper);
        return new PageResult<>(page);
    }

    /**
     * 查看用户是否已经存在
     * true-已经存在  false-用户不存在
     *
     * @param params
     * @return
     */
    @Override
    public boolean isModilePhone(String params) {
        QueryWrapper<Blacklist> queryWrapper = new QueryWrapper<>();
        //queryWrapper.orderByDesc("CreateTime");
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        queryWrapper.eq("ModilePhone", params);
        Integer count = this.service.count(queryWrapper);
        return count > 0;
    }
}
