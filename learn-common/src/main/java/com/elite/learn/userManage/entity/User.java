package com.elite.learn.userManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author: leroy
 * @since 2021-11-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */

    private String id;

    /**
     * 真实姓名
     */
    private String trueName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 生日
     */
    private Long birthday;

    /**
     * 身份证号
     */
    private String pinCodes;

    /**
     * 用户住址
     */
    private String address;

    /**
     * 籍贯
     */
    private String nativePlace;

    /**
     * 用户注册时使用个的帐号
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户的昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String headimgurl;

    /**
     * 手机号
     */
    private String modilePhone;

    /**
     * 小程序唯一身份ID
     */
    private String routineOpenid;

    /**
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    private Integer sex;

    /**
     * 用户所在城市
     */
    private String city;

    /**
     * 用户的语言，简体中文为zh_CN
     */
    private String language;

    /**
     * 用户所在省份
     */
    private String province;

    /**
     * 用户所在国家
     */
    private String country;

    /**
     * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
     */
    private String unionid;

    /**
     * 用户的标识，对当前公众号唯一
     */
    private String openid;

    /**
     * 第三方平台返回的user_id
     */
    private String otherUid;

    /**
     * 累计总积分
     */
    private BigDecimal totalIntegral;

    /**
     * 累计剩余积分
     */
    private BigDecimal integral;

    /**
     * 账户金额
     */
    private BigDecimal money;

    /**
     * 总账户金额
     */
    private BigDecimal totalMoney;

    /**
     * 注册时间
     */
    private Date registerTime;

    /**
     * 注册ip
     */
    private String registerIp;

    /**
     * 最后一次登录时间
     */
    private Date loginTime;

    /**
     * 登录ip
     */
    private String loginIp;

    /**
     * 账号状态 0:正常 1:冻结 2是禁言 3拉黑
     */
    private Integer isState;

    /**
     * 是否已审核：0-是，1-否
     */
    private Integer isAudit;

    /**
     * 用户类型（0:无 ）
     */
    private Integer isType;

    /**
     * 用户等级 1-游客（普通用户）2-会员vip
     */
    private Integer level;

    /**
     * 累计消费金额
     */
    private BigDecimal consumeAmount;

    /**
     * 添加时间
     */
    private Long createTime;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 删除标识 0:未删除 1:删除
     */
    private Integer isDelete;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;

    /**
     * 备注
     */
    private String memo;

    /**
     * 备注
     */
    private String extraParams1;

    /**
     * 备注
     */
    private String extraParams2;

    /**
     * 备注
     */
    private String extraParams3;
    /**
     * 浏览量
     */
    private Integer viewsNum;

    /**
     * 照片墙面
     */
    private String imgPhotoWallUrl;
    /**
     * 微信号
     */
    private String wechatNumber;


}
