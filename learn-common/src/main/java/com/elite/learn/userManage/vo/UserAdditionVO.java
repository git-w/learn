package com.elite.learn.userManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户信息---附加表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Data
public class UserAdditionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户标签，多个英文逗号隔开
     */
    private String tags;

    /**
     * 身份证正面照片地址
     */
    private String pinCodesProsUrl;

    /**
     * 身份证反面照片地址
     */
    private String pinCodesConsUrl;

    /**
     * 手持身份证
     */
    private String pinCodesHandUrl;

    /**
     * 身高-身高范围：145-250
     */
    private String stature;

    /**
     * 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
     */
    private Integer educationBackground;

    /**
     * 婚姻状况：0-未婚、1-离异、2-丧偶
     */
    private Integer maritalStatus;

    /**
     * 子女状况：0-无子女、1-有子女
     */
    private Integer childrenStatus;

    /**
     * 用户所在省份名称
     */
    private String provinceName;

    /**
     * 用户所在城市名称
     */
    private String cityName;

    /**
     * 用户所在省份id
     */
    private String provinceId;

    /**
     * 用户所在城市id
     */
    private String cityId;

    /**
     * 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
     */
    private Integer housingStatus;

    /**
     * 购车情况：0-已购车、1-有需要购车、2-其他
     */
    private Integer carStatus;

    /**
     * 头像、独白、语音独白在上传/修改时需要审核
     */
    private String introduce;

    /**
     * 语音独白最长60秒，最短5秒
     */
    private String introduceVoiceUrl;

    /**
     * 身份证正面照片地址
     */
    private String pinCodesProsUrlPath;

    /**
     * 身份证反面照片地址
     */
    private String pinCodesConsUrlPath;

    /**
     * 手持身份证
     */
    private String pinCodesHandUrlPath;
    /**
     * 语音独白最长60秒，最短5秒
     */
    private String introduceVoiceUrlPath;

    /**
     * 时长
     */
    private Integer duration;

    /**
     * 封面图（短）
     */
    private String coverImgUrl;

    /**
     * 封面图（长）
     */
    private String coverImgUrlPath;


    /**
     * 体重
     */
    private  Integer weight;

}
