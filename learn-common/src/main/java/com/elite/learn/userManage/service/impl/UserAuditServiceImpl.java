package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.UserAudit;
import com.elite.learn.userManage.mapper.UserAuditMapper;
import com.elite.learn.userManage.service.IUserAuditService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.vo.UserAuditVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户模块---用户信息审核表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Service
public class UserAuditServiceImpl extends ServiceImpl<UserAuditMapper, UserAudit> implements IUserAuditService {



    /**
     * 分页
     * @author: leroy
     * @date 2021/12/9 17:33
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<UserAuditVO> getLists(Page<UserAuditVO> pageParams, @Param(Constants.WRAPPER)Wrapper<UserAuditVO> queryWrapper) {
        return pageParams.setRecords(this.baseMapper.getLists(pageParams,queryWrapper));
    }




    /**
     * 详情
     * @author: leroy
     * @date 2021/12/9 17:33
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public UserAuditVO getInfo(String id){
     return   this.baseMapper.getInfo(id);
    }
}
