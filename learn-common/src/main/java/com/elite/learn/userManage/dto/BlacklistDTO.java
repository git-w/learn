package com.elite.learn.userManage.dto;

import lombok.Data;
import org.apache.ibatis.annotations.Update;


import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 民宿预定-黑名单表
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
@Data
public class BlacklistDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * 用户名称
     */
    @NotBlank(message = "用户名称,参数为空")
    private String UserName;

    /**
     * 手机号
     */
    @NotBlank(message = "手机号,参数为空")
    private String ModilePhone;

    /**
     * 更新者ID
     */
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    private String UpdateOperatorName;

    /**
     * 添加时间
     */
    private Long CreateTime;

    /**
     * 更新时间
     */
    private Long UpdateTime;


    /**
     * 备注
     */
    private String Memo;

    /**
     * 备用字段，如果占用，请备注做什么用
     */
    private String ExtraParams1;

    /**
     * 备用字段，如果占用，请备注做什么用
     */
    private String ExtraParams2;

    /**
     * 备用字段，如果占用，请备注做什么用
     */
    private String ExtraParams3;

    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;
}
