package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.GroupDTO;
import com.elite.learn.userManage.entity.Group;
import com.elite.learn.userManage.query.GroupQuery;
import com.elite.learn.userManage.vo.GroupVO;

import java.util.List;

public interface IGroupServiceBLL extends ICommonServiceBLL<Group, GroupDTO, GroupVO, GroupQuery> {

    /**
     * 获取所有
     */
    List<Group> findAll2(GroupQuery bean);
}
