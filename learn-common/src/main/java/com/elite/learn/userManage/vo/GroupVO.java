package com.elite.learn.userManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 民宿预定-团队客户表
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
@Data
public class GroupVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 团队姓名
     */
    private String GroupName;

    /**
     * 用户注册时使用个的帐号
     */
    private String UserName;
    private String Members;

    /**
     * 手机号
     */
    private String ModilePhone;

    /**
     * 订单总数量
     */
    private Integer TotalOrderNum;

    /**
     * 更新者ID
     */
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    private String UpdateOperatorName;

    /**
     * 添加时间
     */
    private Long CreateTime;

    /**
     * 更新时间
     */
    private Long UpdateTime;


    /**
     * 备注
     */
    private String Memo;

    /**
     * 备用字段，如果占用，请备注做什么用
     */
    private String ExtraParams1;

    /**
     * 备用字段，如果占用，请备注做什么用
     */
    private String ExtraParams2;

    /**
     * 备用字段，如果占用，请备注做什么用
     */
    private String ExtraParams3;

    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;
}
