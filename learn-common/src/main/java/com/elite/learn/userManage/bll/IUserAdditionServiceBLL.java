package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.UserAdditionDTO;
import com.elite.learn.userManage.entity.UserAddition;
import com.elite.learn.userManage.query.UserAdditionQuery;
import com.elite.learn.userManage.vo.UserAdditionVO;

public interface IUserAdditionServiceBLL extends ICommonServiceBLL<UserAddition, UserAdditionDTO, UserAdditionVO, UserAdditionQuery> {
}
