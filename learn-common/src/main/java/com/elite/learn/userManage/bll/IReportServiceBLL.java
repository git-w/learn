package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.ReportDTO;
import com.elite.learn.userManage.entity.Report;
import com.elite.learn.userManage.query.ReportQuery;
import com.elite.learn.userManage.vo.ReportVO;

public interface IReportServiceBLL extends ICommonServiceBLL<Report, ReportDTO, ReportVO, ReportQuery> {

    /**
     * 反馈处理结果
     * @author: leroy
     * @date 2021/12/7 17:52
     * @param dto
     * @return null
     * @update
     * @updateTime
     */
    boolean dealWithResults(String operUserId, ReportDTO dto);
}
