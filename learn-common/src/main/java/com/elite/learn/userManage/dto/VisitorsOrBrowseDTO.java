package com.elite.learn.userManage.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 浏览访客
 * </p>
 *
 * @author: leroy
 * @since 2021-12-15
 */
@Data

public class VisitorsOrBrowseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建时间
     */
    private Long createTime;


}
