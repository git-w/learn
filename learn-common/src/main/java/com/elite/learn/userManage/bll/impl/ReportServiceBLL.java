package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IReportServiceBLL;
import com.elite.learn.userManage.dto.ReportDTO;
import com.elite.learn.userManage.entity.Report;
import com.elite.learn.userManage.query.ReportQuery;
import com.elite.learn.userManage.service.IReportService;
import com.elite.learn.userManage.vo.ReportVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 举报反馈表
 * @Title: InfoReportServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/26 8:57
 */

@Service
public class ReportServiceBLL implements IReportServiceBLL {

    @Resource
    private IReportService service;


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/7 13:34
     * @update
     * @updateTime 根据主键id
     */
    @Override
    public Report get(String id) {
        return this.service.getById(id);
    }


    /**
     * 详情
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/7 13:34
     * @update
     * @updateTime
     */
    @Override
    public ReportVO getInfo(String id) {
        ReportVO info = this.service.getInfo(id);
        //拼接多图长地址
        if (Objects.nonNull(info) && StringUtil.notBlank(info.getImgPathList())) {
            List<String> strList = new ArrayList<String>();
            String[] pictures = info.getImgPathList().split(",");
            String path = FileUploadUtil.getImgRootPath();
            for (String pic : pictures) {
                strList.add(path.concat(pic));
            }
            info.setImgPathListUrl(StringUtils.join(strList, ","));
        }
        return info;
    }


    /**
     * 添加
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2022/1/14 16:03
     * @update
     * @updateTime
     */
    @Override
    public String save(String operUserId, ReportDTO bean) {
        //创建实体对象
        Report info = new Report();
        //用户生产id
        info.setId(IdUtils.simpleUUID());
        //用户id
        info.setUserId(bean.getUserId());
        //分类id
        info.setCateId(bean.getCateId());
        //用户昵称
        info.setName(bean.getName());
        //联系方式
        info.setMobilePhone(bean.getMobilePhone());
        //举报时间
        info.setComplaintsTime(bean.getComplaintsTime());
        //举报内容
        info.setContent(bean.getContent());
        //图片存放路径（短），多图用,隔开
        info.setImgPathList(bean.getImgPathList());
        //满意度 0非常满意 1满意 2一般 3不满意
        info.setSatisFaction(bean.getSatisFaction());
        //备注
        info.setMemo(bean.getMemo());
        //处理结果
        info.setHandingContent(bean.getHandingContent());
        //备用字段
        info.setExtraParams1(bean.getExtraParams1());
        info.setExtraParams2(bean.getExtraParams2());
        info.setExtraParams3(bean.getExtraParams3());
        //处理状态 0:未处理 1:已处理
        info.setIsState(0);


        //删除标识 0:未删除 1:已删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //创建者ID（反馈者）
        info.setCreateOperatorid(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        //创建时间
        info.setCreateTime(System.currentTimeMillis());
        //数据回调
        boolean save = this.service.save(info);
        return save ? info.getId() : null;
    }


    /**
     * 更新
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2022/1/14 10:36
     * @update
     * @updateTime
     */
    @Override
    public boolean update(String operUserId, ReportDTO bean) {
        // 判断通过ID获取的对象是否为空
        Report info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        Report infoNew = new Report();
        //用户生成id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //用户id
        info.setUserId(bean.getUserId());
        //分类id
        if (StringUtil.notBlank(bean.getCateId())) {
            infoNew.setCateId(bean.getCateId());
        }
        //用户昵称
        if (StringUtil.notBlank(bean.getName())) {
            infoNew.setName(bean.getName());

        }
        //联系方式
        infoNew.setMobilePhone(bean.getMobilePhone());
        //举报时间
        infoNew.setComplaintsTime(bean.getComplaintsTime());
        //举报内容
        infoNew.setContent(bean.getContent());
        //图片存放路径（短），多图用,隔开
        infoNew.setImgPathList(bean.getImgPathList());
        //满意度 0非常满意 1满意 2一般 3不满意
        infoNew.setSatisFaction(bean.getSatisFaction());
        //备注
        infoNew.setMemo(bean.getMemo());
        //处理结果
        infoNew.setHandingContent(bean.getHandingContent());
        //处理状态 0:未处理 1:已处理
        infoNew.setIsState(bean.getIsState());
        //额外参数
        infoNew.setExtraParams1(bean.getExtraParams1());
        infoNew.setExtraParams2(bean.getExtraParams2());
        infoNew.setExtraParams3(bean.getExtraParams3());

        //更新者ID
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());

        return this.service.updateById(infoNew);
    }


    /**
     * 删除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2022/1/14 16:04
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        Report infoDel = null;
        boolean flag = true;
        //分割id
        String[] idArr = ids.split(",");
        //循环遍历所以id
        for (String id : idArr) {
            Report info = this.get(id);
            //判断id是否为空
            if (Objects.isNull(info)) {
                //跳出本次 执行下一次
                continue;
            }
            //创建删除实体
            infoDel = new Report();
            //获取修改后的id
            infoDel.setId(info.getId());
            //设置删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除者ID
            infoDel.setDeleteOperatorId(operUserId);
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());

            //数据回调业务层
            boolean result = this.service.updateById(infoDel);

            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    @Override
    public List<ReportVO> findAll(ReportQuery bean) {
        return null;
    }


    @Override
    public PageResult<ReportVO> getList(ReportQuery params) {

        QueryWrapper<Report> queryWrapper = new QueryWrapper<Report>();
        queryWrapper.eq("tir.is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("tir.create_time");
        // 根据分组查询
        if (StringUtil.notBlank(params.getName())) {
            queryWrapper.eq("tir.name", params.getName());
        }
        if (StringUtil.notBlank(params.getCateId())) {
            queryWrapper.eq("tir.cate_id", params.getCateId());
        }
        //处理状态 0:未处理 1:已处理
        if (Objects.nonNull(params.getIsState())) {
            queryWrapper.eq("tir.is_state", params.getIsState());
        }

        Page<ReportVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<ReportVO> page = service.getLists(PageParams, queryWrapper);

        List<ReportVO> result = page.getRecords();
        //有查询结果
        if (null != result && result.size() > 0) {
            for (ReportVO info : result) {
                //拼接多图长地址
                if (StringUtil.notBlank(info.getImgPathList())) {
                    List<String> strList = new ArrayList<String>();
                    String[] pictures = info.getImgPathList().split(",");
                    String path = FileUploadUtil.getImgRootPath();
                    for (String pic : pictures) {
                        strList.add(path.concat(pic));
                    }
                    info.setImgPathListUrl(StringUtils.join(strList, ","));
                }
            }
        }

        return new PageResult<>(page);
    }


    /**
     * 处理结果
     *
     * @param operUserId
     * @param dto
     * @return
     */
    @Override
    public boolean dealWithResults(String operUserId, ReportDTO dto) {
        Report infoNew = null;
        String[] idArr = dto.getId().split(",");
        boolean flag = true;
        for (String id : idArr) {
            Report info = this.get(id);
            if (null == info) {
                continue;
            }

            infoNew = new Report();
            infoNew.setId(info.getId());
            //处理状态 0:未处理 1:已处理
            if (Objects.nonNull(dto.getIsState())) {
                infoNew.setIsState(1);
            }
            //处理结果
            if (Objects.nonNull(dto.getHandingContent())) {
                infoNew.setHandingContent(dto.getHandingContent());
            } else {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
            }
            infoNew.setUpdateTime(System.currentTimeMillis());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setUpdateOperatorName(dto.getUpdateOperatorName());
            boolean result = service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }
}