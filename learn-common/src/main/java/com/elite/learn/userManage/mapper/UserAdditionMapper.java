package com.elite.learn.userManage.mapper;

import com.elite.learn.userManage.entity.UserAddition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.userManage.vo.UserAdditionVO;

/**
 * <p>
 * 用户信息---附加表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
public interface UserAdditionMapper extends BaseMapper<UserAddition> {
    /**
     * @return null
     * @author: leroy
     * @date 2021/11/30 11:43
     * @update
     * @updateTime 查看附加表详情
     */
    UserAdditionVO getInfo(String id);
}
