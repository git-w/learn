package com.elite.learn.userManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.UserAudit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.userManage.vo.UserAuditVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户模块---用户信息审核表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
public interface UserAuditMapper extends BaseMapper<UserAudit> {



    /**
     * 分页
     * @author: leroy
     * @date 2021/12/9 17:33
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<UserAuditVO> getLists(Page<UserAuditVO> pageParams, @Param(Constants.WRAPPER) Wrapper<UserAuditVO> queryWrapper);



    /**
     * 详情
     * @author: leroy
     * @date 2021/12/9 17:33
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    UserAuditVO getInfo(String id);
}
