package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.entity.Feedback;
import com.elite.learn.userManage.mapper.FeedbackMapper;
import com.elite.learn.userManage.service.IFeedbackService;
import com.elite.learn.userManage.vo.FeedbackVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 意见反馈表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements IFeedbackService {


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:04
     * @update
     * @updateTime 详情
     */
    @Override
    public FeedbackVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     * @param pageParams
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:04
     * @update
     * @updateTime 分页
     */
    @Override
    public IPage<FeedbackVO> getLists(Page<FeedbackVO> pageParams, @Param(Constants.WRAPPER) Wrapper<Feedback> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getLists(pageParams, queryWrapper));
        return pageParams;
    }
}
