package com.elite.learn.userManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户意见反馈分类表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Data
public class FeedbackCateVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 分类名称
     */
    private String cateName;

    /**
     *父分类
     */
    private String parentId;

    /**
     * 排序 数值越大越靠前
     */
    private Integer sort;

    /**
     * 备注
     */
    private String memo;

    /**
     * 是否上线 0是 1否
     */
    private Integer isState;



    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;


    /**
     * 额外参数1
     */
    private String extraParams1;



}
