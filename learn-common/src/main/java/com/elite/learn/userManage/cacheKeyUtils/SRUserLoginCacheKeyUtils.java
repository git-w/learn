package com.elite.learn.userManage.cacheKeyUtils;

import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.common.utils.redis.InnoPlatformConstantsUtil;

/**
 * 小程序端用户登录
 *
 * @Title :LoginAccountCacheKeyUtils.java
 * @Package: com.elite.learn.base.system.entity.cacheKeyUtils
 * @Description:
 * @author: leroy
 * @data: 2020/12/4 11:27 SRUserLoginCacheKeyUtils
 */
public class SRUserLoginCacheKeyUtils {

    private static final String PRE = InnoPlatformConstantsUtil.CACHE_PREX + "SRUSERLOGIN_";

    /**
     * 对象缓存key
     *
     * @param id
     * @return
     */
    public static String getKey(String id) {
        if (StringUtil.isNotEmpty(id)) {
            return PRE + id;
        }
        return null;
    }
}