package com.elite.learn.userManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.User;
import com.elite.learn.userManage.vo.UserLoginVO;
import com.elite.learn.userManage.vo.UserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-11-30
 */
public interface UserMapper extends BaseMapper<User> {


    /*
     *
     * @author: leroy
     * @date 2021/11/30 11:43
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    UserVO getInfo(String id);


    /*
     *
     * @author: leroy
     * @date 2021/11/30 11:45
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<UserVO> getLists(Page<UserVO> pageVo, @Param(Constants.WRAPPER) Wrapper<User> queryWrapper);


    /**
     * @param queryWrapper
     * @return null
     * @author: leroy
     * @date 2021/11/30 11:45
     * @update
     * @updateTime 条件查询单个数据
     */
    UserLoginVO getConditionInfo(@Param(Constants.WRAPPER) Wrapper<User> queryWrapper);



    /**搜索
     * @author: leroy
     * @date 2021/11/30 11:45
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<UserVO> getConditionList(Page<UserVO> pageVo, @Param(Constants.WRAPPER) Wrapper<User> queryWrapper);

}
