package com.elite.learn.userManage.query;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户信息---附加表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Data
public class UserAdditionQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户标签，多个英文逗号隔开
     */
    private String tags;




}
