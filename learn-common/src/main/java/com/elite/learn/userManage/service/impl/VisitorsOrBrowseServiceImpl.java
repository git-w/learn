package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.VisitorsOrBrowse;
import com.elite.learn.userManage.mapper.VisitorsOrBrowseMapper;
import com.elite.learn.userManage.service.IVisitorsOrBrowseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.vo.VisitorsOrBrowseVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 浏览访客 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-15
 */
@Service
public class VisitorsOrBrowseServiceImpl extends ServiceImpl<VisitorsOrBrowseMapper, VisitorsOrBrowse> implements IVisitorsOrBrowseService {


    /**
     * 浏览分页
     * @author: leroy
     * @date 2021/12/15 10:11
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<VisitorsOrBrowseVO> getListBrowse(Page<VisitorsOrBrowseVO> pageParams, @Param(Constants.WRAPPER)Wrapper<VisitorsOrBrowse> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getListBrowse(pageParams,queryWrapper));
        return pageParams;
    }


    /**
     * 访客分页
     * @author: leroy
     * @date 2021/12/15 10:11
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<VisitorsOrBrowseVO> getList(Page<VisitorsOrBrowseVO> pageParams, @Param(Constants.WRAPPER)Wrapper<VisitorsOrBrowse> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getList(pageParams,queryWrapper));
        return pageParams;
    }
}
