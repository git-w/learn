package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.FeedbackCateDTO;
import com.elite.learn.userManage.entity.FeedbackCate;
import com.elite.learn.userManage.query.FeedbackCateQuery;
import com.elite.learn.userManage.vo.FeedbackCateVO;

public interface IFeedbackCateServiceBLL extends ICommonServiceBLL<FeedbackCate, FeedbackCateDTO, FeedbackCateVO, FeedbackCateQuery> {
    /**
     * 多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateMultiIsEnabled(String operUserId, BaseParams params);
}
