package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Group;
import com.elite.learn.userManage.mapper.GroupMapper;
import com.elite.learn.userManage.service.IGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.vo.GroupVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 民宿预定-团队客户表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
@Service
public class GroupServiceImpl extends ServiceImpl<GroupMapper, Group> implements IGroupService {
    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<GroupVO> selectLists(Page<GroupVO> page, @Param(Constants.WRAPPER) Wrapper<Group> queryWrapper) {
        page.setRecords(this.baseMapper.selectLists(page, queryWrapper));
        return page;
    }

    /**
     * 查看用户详情
     *
     * @param queryWrapper
     * @return
     */
    @Override
    public GroupVO selectInfo(@Param(Constants.WRAPPER)Wrapper<Group> queryWrapper) {
        return this.baseMapper.selectInfo(queryWrapper);
    }

}
