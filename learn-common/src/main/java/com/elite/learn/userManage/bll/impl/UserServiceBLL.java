package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.cardManage.entity.LogUserMemberCard;
import com.elite.learn.cardManage.entity.UserMemberCardRights;
import com.elite.learn.cardManage.service.ILogUserMemberCardService;
import com.elite.learn.cardManage.service.IUserMemberCardRightsService;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.ip.RequestUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.common.utils.string.StringUtils;
import com.elite.learn.logManage.bll.ILogUserLoginServiceBLL;
import com.elite.learn.messageManage.bll.IUserMessageServiceBLL;
import com.elite.learn.userManage.bll.IUserServiceBLL;
import com.elite.learn.userManage.contants.UserManageExceptionCodeEnum;
import com.elite.learn.userManage.dto.UserDTO;
import com.elite.learn.userManage.entity.User;
import com.elite.learn.userManage.entity.UserAddition;
import com.elite.learn.userManage.entity.UserMateSelection;
import com.elite.learn.userManage.query.UserQuery;
import com.elite.learn.userManage.service.IUserAdditionService;
import com.elite.learn.userManage.service.IUserMateSelectionService;
import com.elite.learn.userManage.service.IUserService;
import com.elite.learn.userManage.vo.UserAdditionVO;
import com.elite.learn.userManage.vo.UserLoginVO;
import com.elite.learn.userManage.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 用户信息表
 * @Title: InfoUserServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/30 9:42
 */
@Service
public class UserServiceBLL implements IUserServiceBLL {

    @Resource
    private IUserService service;
    //用户日志
    @Resource
    private ILogUserLoginServiceBLL logUserLoginServiceBLL;
    //用户附件表
    @Resource
    private IUserAdditionService userAdditionService;
    //用户信息---附加
    @Resource
    private IUserMateSelectionService userMateSelectionService;
    //请求
    @Resource
    private HttpServletRequest request;
    //用户注册会员权益表 服务类
    @Resource
    private IUserMemberCardRightsService userMemberCardRightsService;
    //使用会员卡权益日志表 服务类
    @Resource
    private ILogUserMemberCardService logUserMemberCardService;
    //系统消息-用户消息表
    @Resource
    private IUserMessageServiceBLL userMessageServiceBLL;


    /*
     *
     * @author: leroy
     * @date 2021/11/30 9:43
     * @param null
     * @return null
     * @update
     * @updateTime
     * 获取对象id
     */
    @Override
    public User get(String id) {
        return this.service.getById(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/30 9:44
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public UserVO getInfo(String id) {
        UserVO info = this.service.getInfo(id);
        UserAdditionVO userAdditionVo = this.userAdditionService.getInfo(id);
        if (StringUtils.isNotEmpty(userAdditionVo.getIntroduceVoiceUrl())) {
            userAdditionVo.setIntroduceVoiceUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getIntroduceVoiceUrl());
        }
        if (StringUtils.isNotEmpty(userAdditionVo.getPinCodesConsUrl())) {
            userAdditionVo.setPinCodesConsUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getPinCodesConsUrl());
        }
        if (StringUtils.isNotEmpty(userAdditionVo.getPinCodesHandUrl())) {
            userAdditionVo.setPinCodesHandUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getPinCodesHandUrl());
        }
        if (StringUtils.isNotEmpty(userAdditionVo.getPinCodesProsUrl())) {
            userAdditionVo.setPinCodesProsUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getPinCodesProsUrl());
        }
        //封面图
        if (StringUtils.isNotEmpty(userAdditionVo.getCoverImgUrl())) {
            userAdditionVo.setCoverImgUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getCoverImgUrl());
        }

        //拼接多图长地址
        if (Objects.nonNull(info) && StringUtil.notBlank(info.getImgPhotoWallUrl())) {
            List<String> strList = new ArrayList<String>();
            String[] pictures = info.getImgPhotoWallUrl().split(",");
            String path = FileUploadUtil.getImgRootPath();
            for (String pic : pictures) {
                strList.add(path.concat(pic));
            }
            info.setImgPhotoWallUrlPath(org.apache.commons.lang.StringUtils.join(strList, ","));
        }

        info.setUserAdditionVo(userAdditionVo);
        info.setUserMateSelectionVo(this.userMateSelectionService.getInfo(id));


        return info;
    }


    /**
     * @param operUserId
     * @return null
     * @author: leroy
     * @date 2021/11/30 9:44
     * @update
     * @updateTime 保存
     */
    @Override
    public String save(String operUserId, UserDTO bean) {
        //创建实体对象
        User info = new User();
        //创建用户生成的实体
        info.setId(IdUtils.simpleUUID());
        //真实姓名
        info.setTrueName(bean.getTrueName());

        //生日
        info.setBirthday(bean.getBirthday());
        //身份证号
        info.setPinCodes(bean.getPinCodes());
        //用户住址
        info.setAddress(bean.getAddress());
        //籍贯
        info.setNativePlace(bean.getNativePlace());

        //用户的昵称
        info.setNickName(bean.getNickName());
        //用户头像
        info.setHeadimgurl(bean.getHeadimgurl());
        //手机号
        info.setModilePhone(bean.getModilePhone());

        //用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
        info.setSex(bean.getSex());


        //注册时间
        info.setRegisterTime(new Date());
        //注册ip
        info.setRegisterIp(RequestUtil.getIP(request));//登录ip
        //最后一次登录时间
        info.setLoginTime(new Date());
        //登录ip
        info.setLoginIp(RequestUtil.getIP(request));
        //账号状态 0:正常 1:冻结 2是禁言 3拉黑
        info.setIsState(bean.getIsState());
        //是否已审核：0-是，1-否
        info.setIsAudit(0);

        //用户等级 1-游客（普通用户）2-会员vip
        info.setLevel(0);

        //备注
        info.setMemo(bean.getMemo());
        //备用1
        info.setExtraParams1(bean.getExtraParams1());
        //备用2
        info.setExtraParams2(bean.getExtraParams2());
        //备用3
        info.setExtraParams3(bean.getExtraParams3());

        //照片墙面
        info.setImgPhotoWallUrl(bean.getImgPhotoWallUrl());
        //微信号
        info.setWechatNumber(bean.getWechatNumber());

        //删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //添加时间
        info.setCreateTime(System.currentTimeMillis());
        //创建者id
        info.setCreateOperatorId(operUserId);

        boolean save = this.service.save(info);
        //添加用户信息附加表
        UserAddition userAdditionInfo = new UserAddition();
        userAdditionInfo.setUserId(info.getId());
        this.userAdditionService.save(userAdditionInfo);

        //添加用户择偶标准表
        UserMateSelection userMateSelectionInfo = new UserMateSelection();
        userMateSelectionInfo.setUserId(info.getId());
        this.userMateSelectionService.save(userMateSelectionInfo);
        return save ? info.getId() : null;
    }

    /*
     *
     * @author: leroy
     * @date 2021/11/30 10:11
     * @param null
     * @return null
     * @update
     * @updateTime
     * 更新
     */
    @Override
    public boolean update(String operUserId, UserDTO bean) {
        // 判断通过ID获取的对象是否为空
        User info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        User infoNew = new User();
        //创建用户生成的实体
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //真实姓名
        if (StringUtil.notBlank(bean.getTrueName())) {
            infoNew.setTrueName(bean.getTrueName());
        }
        //邮箱
        infoNew.setEmail(bean.getEmail());
        //生日
        infoNew.setBirthday(bean.getBirthday());
        //身份证号
        if (StringUtil.notBlank(bean.getPinCodes())) {
            infoNew.setPinCodes(bean.getPinCodes());
        }
        //用户住址
        infoNew.setAddress(bean.getAddress());

        //籍贯
        infoNew.setNativePlace(bean.getNativePlace());
        //用户注册时使用个的帐号
        if (StringUtil.notBlank(bean.getUserName())) {
            infoNew.setUserName(bean.getUserName());
        }
        //密码
        if (StringUtil.notBlank(bean.getPassword())) {
            infoNew.setPassword(bean.getPassword());
        }
        //用户的昵称
        if (StringUtil.notBlank(bean.getNickName())) {
            infoNew.setNickName(bean.getNickName());
        }
        //用户头像
        if (StringUtil.notBlank(bean.getHeadimgurl())) {
            infoNew.setHeadimgurl(bean.getHeadimgurl());
        }
        //手机号
        infoNew.setModilePhone(bean.getModilePhone());
        //用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
        infoNew.setSex(bean.getSex());


        //账号状态 0:正常 1:冻结 2是禁言 3拉黑
        infoNew.setIsState(bean.getIsState());

        //备注
        infoNew.setMemo(bean.getMemo());
        //备用1
        infoNew.setExtraParams1(bean.getExtraParams1());
        //备用2
        infoNew.setExtraParams2(bean.getExtraParams2());
        //备用3
        infoNew.setExtraParams3(bean.getExtraParams3());
        //照片墙面
        infoNew.setImgPhotoWallUrl(bean.getImgPhotoWallUrl());
        //微信号
        infoNew.setWechatNumber(bean.getWechatNumber());

        //更新者状态
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateTime(System.currentTimeMillis());

        //更新状态
        return service.updateById(infoNew);

    }


    /*
     *
     * @author: leroy
     * @date 2021/11/30 10:56
     * @param null
     * @return null
     * @update
     * @updateTime
     * 删除
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        User infoDel = null;
        //分割id
        String[] idArr = ids.split(",");
        //循环遍历所以id
        for (String id : idArr) {
            //读取id
            User info = this.get(id);
            //判断id是否为空
            if (Objects.isNull(info)) {
                //跳出本次 执行下一次
                continue;
            }
            //创建删除实体
            infoDel = new User();
            //获取修改后的id
            infoDel.setId(info.getId());
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除者ID
            infoDel.setDeleteOperatorId(operUserId);
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());

            boolean result = service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;

    }

    @Override
    public List<UserVO> findAll(UserQuery bean) {
        return null;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/30 10:59
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    @Override
    public PageResult<UserVO> getList(UserQuery params) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());

        // 根据分组查询
        if (StringUtil.notBlank(params.getTrueName())) {
            queryWrapper.eq("true_name", params.getTrueName());
        }

        // 用户昵称
        if (StringUtil.notBlank(params.getNickName())) {
            queryWrapper.eq("nick_name", params.getNickName());
        }

        // 账号状态 0:正常 1:冻结 2是禁言 3拉黑
        if (Objects.nonNull(params.getIsState())) {
            queryWrapper.eq("is_state", params.getIsState());
        }
        // 是否已审核：0-是，1-否
        if (Objects.nonNull(params.getIsAudit())) {
            queryWrapper.eq("is_audit", params.getIsAudit());
        }
        // 用户类型
        if (Objects.nonNull(params.getIsType())) {
            queryWrapper.eq("is_type", params.getIsType());
        }
        // 真实姓名
        if (Objects.nonNull(params.getProvince())) {
            queryWrapper.like("province", params.getProvince());
        }
        // 市区名称
        if (Objects.nonNull(params.getCity())) {
            queryWrapper.like("city", params.getCity());
        }


        // 用户等级 1-游客（普通用户）2-会员vip
        if (Objects.nonNull(params.getLevel())) {
            queryWrapper.like("level", params.getLevel());
        }

        // 用户类型
        if (Objects.nonNull(params.getSex())) {
            queryWrapper.eq("sex", params.getSex());
        }
        queryWrapper.orderByDesc("register_time");
        Page<UserVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<UserVO> page = this.service.getLists(PageParams, queryWrapper);

        //有查询结果
        if (null != page.getRecords() && page.getRecords().size() > 0) {
            for (UserVO info : page.getRecords()) {
                UserAdditionVO userAdditionVo = this.userAdditionService.getInfo(info.getId());
                if (StringUtils.isNotEmpty(userAdditionVo.getIntroduceVoiceUrl())) {
                    userAdditionVo.setIntroduceVoiceUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getIntroduceVoiceUrl());
                }
                if (StringUtils.isNotEmpty(userAdditionVo.getPinCodesConsUrl())) {
                    userAdditionVo.setPinCodesConsUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getPinCodesConsUrl());
                }
                if (StringUtils.isNotEmpty(userAdditionVo.getPinCodesHandUrl())) {
                    userAdditionVo.setPinCodesHandUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getPinCodesHandUrl());
                }
                if (StringUtils.isNotEmpty(userAdditionVo.getPinCodesProsUrl())) {
                    userAdditionVo.setPinCodesProsUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getPinCodesProsUrl());
                }
                //封面图
                if (StringUtils.isNotEmpty(userAdditionVo.getCoverImgUrl())) {
                    userAdditionVo.setCoverImgUrl(FileUploadUtil.getImgRootPath() + userAdditionVo.getCoverImgUrl());
                }
                info.setUserAdditionVo(userAdditionVo);
                info.setUserMateSelectionVo(this.userMateSelectionService.getInfo(info.getId()));

            }
        }
        return new PageResult<>(page);
    }


    /**
     * 条件搜索分页
     *
     * @param params
     * @return
     */
    @Override
    public PageResult<UserVO> getConditionList(UserQuery params) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.eq("iu.is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("iu.create_time");
        // 根据分组查询
        if (StringUtil.notBlank(params.getTrueName())) {
            queryWrapper.eq("iu.true_name", params.getTrueName());
        }

        // 根据分组查询
        if (StringUtil.notBlank(params.getNickName())) {
            queryWrapper.eq("iu.nick_name", params.getNickName());
        }

        // 账号状态 0:正常 1:冻结 2是禁言 3拉黑
        if (Objects.nonNull(params.getIsState())) {
            queryWrapper.eq("iu.is_state", params.getIsState());
        }
        // 是否已审核：0-是，1-否
        if (Objects.nonNull(params.getIsAudit())) {
            queryWrapper.eq("iu.is_audit", params.getIsAudit());
        }
        // 用户类型
        if (Objects.nonNull(params.getIsType())) {
            queryWrapper.eq("iu.is_type", params.getIsType());
        }
        // 用户类型
        if (Objects.nonNull(params.getProvince())) {
            queryWrapper.like("iu.province", params.getProvince());
        }
        // 用户类型
        if (Objects.nonNull(params.getCity())) {
            queryWrapper.like("iu.city", params.getCity());
        }

        // 是否已审核：0-是，1-否
        if (Objects.nonNull(params.getIsAudit())) {
            queryWrapper.like("iu.is_audit", params.getIsAudit());
        }

        // 用户等级 1-游客（普通用户）2-会员vip
        if (Objects.nonNull(params.getLevel())) {
            queryWrapper.like("iu.level", params.getLevel());
        }

        // 用户类型
        if (Objects.nonNull(params.getSex())) {
            queryWrapper.eq("iu.sex", params.getSex());
        }


        // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
        if (Objects.nonNull(params.getHousingStatus())) {
            queryWrapper.eq("iua.housing_status", params.getHousingStatus());
        }
        // 婚姻状况：0-未婚、1-离异、2-丧偶
        if (Objects.nonNull(params.getMaritalStatus())) {
            queryWrapper.eq("iua.marital_status", params.getMaritalStatus());
        }
        // 子女状况：0-无子女、1-有子女
        if (Objects.nonNull(params.getChildrenStatus())) {
            queryWrapper.eq("iua.children_status", params.getChildrenStatus());
        }

        // 学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
        if (Objects.nonNull(params.getEducationBackground())) {
            queryWrapper.eq("iua.education_background", params.getEducationBackground());
        }

        // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
        if (Objects.nonNull(params.getHousingStatus())) {
            queryWrapper.eq("iua.housing_atatus", params.getHousingStatus());
        }
        //身高最大
        if (Objects.nonNull(params.getBigStature()) && Objects.nonNull(params.getSmallStature())) {
            queryWrapper.between("iua.stature", params.getSmallStature(), params.getBigStature());
        }
        //年龄
        if (Objects.nonNull(params.getBigAge()) && Objects.nonNull(params.getSmallAge())) {
            queryWrapper.between("iu.birthday", params.getSmallAge(), params.getBigAge());
        }


        Page<UserVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<UserVO> page = this.service.getConditionList(PageParams, queryWrapper);

        List<UserVO> result = page.getRecords();
        //有查询结果
        if (null != result && result.size() > 0) {
            for (UserVO info : result) {
                UserAdditionVO userAdditionVo = this.userAdditionService.getInfo(info.getId());
                if (StringUtils.isNotEmpty(userAdditionVo.getIntroduceVoiceUrl())) {
                    userAdditionVo.setIntroduceVoiceUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getIntroduceVoiceUrl());
                }
                if (StringUtils.isNotEmpty(userAdditionVo.getPinCodesConsUrl())) {
                    userAdditionVo.setPinCodesConsUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getPinCodesConsUrl());
                }
                if (StringUtils.isNotEmpty(userAdditionVo.getPinCodesHandUrl())) {
                    userAdditionVo.setPinCodesHandUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getPinCodesHandUrl());
                }
                if (StringUtils.isNotEmpty(userAdditionVo.getPinCodesProsUrl())) {
                    userAdditionVo.setPinCodesProsUrlPath(FileUploadUtil.getImgRootPath() + userAdditionVo.getPinCodesProsUrl());
                }
                //封面图
                if (StringUtils.isNotEmpty(userAdditionVo.getCoverImgUrl())) {
                    userAdditionVo.setCoverImgUrl(FileUploadUtil.getImgRootPath() + userAdditionVo.getCoverImgUrl());
                }
                info.setUserAdditionVo(userAdditionVo);
                info.setUserMateSelectionVo(userMateSelectionService.getInfo(info.getId()));

            }
        }
        return new PageResult<>(page);
    }


    /**
     * <p>描述:更新用户登录时间 和登录ip</p>
     *
     * @param id
     */
    @Override
    public void updateLoginTime(String id) {
        if (StringUtil.isNotEmpty(id)) {
            String ip = RequestUtil.getIP(request);
            User user = new User();
            user.setId(id);
            user.setLoginTime(new Date());//用户最后一次登录时间
            user.setLoginIp(ip);//登录ip
            this.service.updateById(user);
            // 记录用户登录日志
            this.logUserLoginServiceBLL.save(id);
        }
    }


    /**
     * 日志
     *
     * @param routineOpenid
     * @return null
     * @author: leroy
     * @date 2022/1/14 16:38
     * @update
     * @updateTime
     */
    @Override
    public UserLoginVO login(String routineOpenid) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("routine_openid", routineOpenid);
        UserLoginVO info = this.service.getConditionInfo(wrapper);
        if (Objects.nonNull(info)) {
          /*  if (info.getIsDelete().equals(DeleteType.ISDELETE.getCode())) {
                throw new CommonException(BasePojectExceptionCodeEnum.UserLoginFailDelete.code,
                        BasePojectExceptionCodeEnum.UserLoginFailDelete.msg);
            }*/
            if (Objects.isNull(this.userMateSelectionService.getInfo(info.getId()))) {
                //添加用户择偶标准表
                UserMateSelection userMateSelectionInfo = new UserMateSelection();
                userMateSelectionInfo.setUserId(info.getId());
                this.userMateSelectionService.save(userMateSelectionInfo);
            }
            if (Objects.isNull(this.userAdditionService.getInfo(info.getId()))) {
                //添加用户择偶标准表
                //添加用户信息附加表
                UserAddition userAdditionInfo = new UserAddition();
                userAdditionInfo.setUserId(info.getId());
                this.userAdditionService.save(userAdditionInfo);
            }
            return info;
        } else {
            //开始创建新用户
            User user = new User();
            user.setId(IdUtils.simpleUUID());
            user.setRoutineOpenid(routineOpenid);
            // 累计总积分
            user.setTotalIntegral(new BigDecimal(0));
            user.setIntegral(new BigDecimal(0));

            user.setMoney(new BigDecimal(0));
            user.setRegisterTime(new Date());//注册时间
            user.setRegisterIp(RequestUtil.getIP(request));//登录ip
            ///是否上下线 0-是 1-否
            user.setIsState(1);
            //是否上下线 0-是 1-否
            user.setIsAudit(1);
            // 是否已审核：0-是，1-否
            user.setIsType(1);
            // 用户等级 1-游客（普通用户）2-会员vi
            user.setLevel(0);
            //未删除
            user.setIsDelete(DeleteType.NOTDELETE.getCode());
            user.setCreateTime(System.currentTimeMillis());
            this.service.save(user);

            //添加用户信息附加表
            UserAddition userAdditionInfo = new UserAddition();
            userAdditionInfo.setUserId(user.getId());
            this.userAdditionService.save(userAdditionInfo);

            //添加用户择偶标准表
            UserMateSelection userMateSelectionInfo = new UserMateSelection();
            userMateSelectionInfo.setUserId(user.getId());
            this.userMateSelectionService.save(userMateSelectionInfo);

            //通知的话选择新消息类型消息类型:1审核成功通知 2审核失败通  3会员卡购买成功知 4新用户注册成功通知  5免费会员卡领取成功通知
            this.userMessageServiceBLL.send(4, user.getId());

            UserLoginVO vo = new UserLoginVO();
            BeanUtils.copyProperties(user, vo);
            return vo;
        }
    }

    /**
     * 修改用户手机号
     *
     * @param UserId
     * @param mobilePhone
     * @return
     */
    @Override
    public boolean updateModilePhone(String UserId, String mobilePhone) {
        boolean flag = false;
        if (StringUtil.isNotEmpty(UserId)) {
            User user = new User();
            user.setId(UserId);
            user.setUpdateOperatorId(UserId);
            user.setModilePhone(mobilePhone);
            user.setUpdateTime(System.currentTimeMillis());
            // 判断是否修改成功
            return service.updateById(user);
        }
        return flag;
    }


    /**
     * 批量上下线
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean updateMultiIsEnabled(String operUserId, BaseParams params) {
        User infoNew = null;
        String[] idsArr = params.getIds().split(",");

        for (String id : idsArr) {
            User info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }

            infoNew = new User();
            infoNew.setId(info.getId());
            //账号状态 0:正常 1:冻结 2是禁言 3拉黑
            if (Objects.nonNull(params.getIsState())) {
                infoNew.setIsState(params.getIsState());
            } else {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
            }
        }

        infoNew.setUpdateTime(System.currentTimeMillis());
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
        return this.service.updateById(infoNew);

    }


    /**
     * 批量上下线 是否推荐 0是 1否
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean updateMultiIsType(String operUserId, BaseParams params) {
        User infoNew = null;
        String[] idsArr = params.getIds().split(",");
        boolean flag = true;
        for (String id : idsArr) {
            User info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }

            infoNew = new User();
            infoNew.setId(info.getId());
            //账号状态 是否推荐 0是 1否
            if (null != params.getIsType()) {
                infoNew.setIsType(params.getIsType());
            } else {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
            }
            infoNew.setUpdateTime(System.currentTimeMillis());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }

    @Override
    public boolean getFrequency(String operUserId, String UserId) {
        //查看当前用户会员卡次数
        QueryWrapper<UserMemberCardRights> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", operUserId);
        //关联类型 0-红娘牵线
        wrapper.eq("is_type", 0);
        // 开始时间小于历史时间
        wrapper.le("start_time", System.currentTimeMillis());
        // 结束时间大于当前时间
        wrapper.ge("end_time", System.currentTimeMillis());
        wrapper.orderByDesc("create_time");
        List<UserMemberCardRights> CardRightsList = userMemberCardRightsService.list(wrapper);
        if (Objects.nonNull(CardRightsList) && CardRightsList.size() > 0) {
            UserMemberCardRights memberCardRightsinfo = CardRightsList.get(0);


            //查看当前用户会员卡次数 查看是否已经和当前用户聊过
            QueryWrapper<LogUserMemberCard> MemberWrapperTwo = new QueryWrapper<>();
            MemberWrapperTwo.eq("mid_card_id", memberCardRightsinfo.getMidUserCardId());
            MemberWrapperTwo.eq("mid_card_config_id", memberCardRightsinfo.getId());
            MemberWrapperTwo.eq("user_id", UserId);
            List<LogUserMemberCard> list = logUserMemberCardService.list(MemberWrapperTwo);
            if (list.size() > 0) {
                throw new CommonException(UserManageExceptionCodeEnum.AlreadyNotMemberError.code, UserManageExceptionCodeEnum.AlreadyNotMemberError.msg);

            }
            //查看当前用户会员卡次数
      /*     QueryWrapper<LogUserMemberCard> MemberWrapper = new QueryWrapper<>();
           MemberWrapper.eq("card_id", operUserId);
           MemberWrapper.eq("card_config_id", operUserId);
           List<LogUserMemberCard> listTwo= logUserMemberCardService.list(MemberWrapper);*/
            if (memberCardRightsinfo.getCount() > 0) {
                return true;
            }
            throw new CommonException(UserManageExceptionCodeEnum.NotMemberOverError.code, UserManageExceptionCodeEnum.NotMemberOverError.msg);

        }
        throw new CommonException(UserManageExceptionCodeEnum.NotMemberError.code, UserManageExceptionCodeEnum.NotMemberError.msg);

    }


    @Override
    public boolean saveFrequency(String operUserId, String UserId) {
        //查看当前用户会员卡次数
        QueryWrapper<UserMemberCardRights> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", operUserId);
        wrapper.eq("is_type", 0);
        // 开始时间小于历史时间
        wrapper.le("start_time", System.currentTimeMillis());
        // 结束时间大于当前时间
        wrapper.ge("end_time", System.currentTimeMillis());
        wrapper.orderByDesc("create_time");
        List<UserMemberCardRights> CardRightsList = this.userMemberCardRightsService.list(wrapper);
        if (Objects.nonNull(CardRightsList) && CardRightsList.size() > 0) {
            UserMemberCardRights memberCardRightsinfo = CardRightsList.get(0);
            LogUserMemberCard logUserMemberCardVo = new LogUserMemberCard();


            //创建用户生成的实体
            logUserMemberCardVo.setId(IdUtils.simpleUUID());

            logUserMemberCardVo.setCardId(memberCardRightsinfo.getCardId());
            logUserMemberCardVo.setCardConfigId(memberCardRightsinfo.getConfigId());
            logUserMemberCardVo.setMidCardId(memberCardRightsinfo.getMidUserCardId());
            logUserMemberCardVo.setMidCardConfigId(memberCardRightsinfo.getId());

            logUserMemberCardVo.setIsType(1);
            logUserMemberCardVo.setUserId(UserId);
            logUserMemberCardVo.setCount(0);
            //添加时间
            logUserMemberCardVo.setCreateTime(System.currentTimeMillis());
            //创建者id
            logUserMemberCardVo.setCreateOperatorId(operUserId);

            boolean flag = this.logUserMemberCardService.save(logUserMemberCardVo);

            memberCardRightsinfo.setCount(memberCardRightsinfo.getCount() - 1);
            this.userMemberCardRightsService.updateById(memberCardRightsinfo);
            return flag;

        }
        throw new CommonException(UserManageExceptionCodeEnum.NotMemberError.code, UserManageExceptionCodeEnum.NotMemberError.msg);

    }


    /**
     * 修改浏览次数
     *
     * @param id
     * @param viewsNum
     */
    @Override
    public void updateViewsNum(String id, Integer viewsNum) {
        User infoNew = new User();
        infoNew.setId(id);
        if (Objects.nonNull(viewsNum)) {
            infoNew.setViewsNum(viewsNum + 1);
        } else {
            infoNew.setViewsNum(1);
        }
        this.service.updateById(infoNew);
    }

}
