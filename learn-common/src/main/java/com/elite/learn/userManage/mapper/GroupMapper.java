package com.elite.learn.userManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Group;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.userManage.vo.GroupVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 民宿预定-团队客户表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
public interface GroupMapper extends BaseMapper<Group> {
    /**
     * 用户自定义分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    List<GroupVO> selectLists(Page<GroupVO> page, @Param(Constants.WRAPPER) Wrapper<Group> queryWrapper);

    /**
     * 用户详情
     *
     * @param queryWrapper
     * @return
     */
    GroupVO selectInfo(@Param(Constants.WRAPPER) Wrapper<Group> queryWrapper);
}
