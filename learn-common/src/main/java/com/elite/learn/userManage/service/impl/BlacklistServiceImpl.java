package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Blacklist;
import com.elite.learn.userManage.mapper.BlacklistMapper;
import com.elite.learn.userManage.service.IBlacklistService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.vo.BlacklistVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 民宿预定-黑名单表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
@Service
public class BlacklistServiceImpl extends ServiceImpl<BlacklistMapper, Blacklist> implements IBlacklistService {
    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<BlacklistVO> selectLists(Page<BlacklistVO> page, @Param(Constants.WRAPPER) Wrapper<Blacklist> queryWrapper) {
        page.setRecords(this.baseMapper.selectLists(page, queryWrapper));
        return page;
    }

    /**
     * 查看用户详情
     *
     * @param queryWrapper
     * @return
     */
    @Override
    public BlacklistVO selectInfo(@Param(Constants.WRAPPER)Wrapper<Blacklist> queryWrapper) {
        return this.baseMapper.selectInfo(queryWrapper);
    }
}
