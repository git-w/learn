package com.elite.learn.userManage.bll;

import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.userManage.dto.VisitorsOrBrowseDTO;
import com.elite.learn.userManage.entity.VisitorsOrBrowse;
import com.elite.learn.userManage.query.VisitorsOrBrowseQuery;
import com.elite.learn.userManage.vo.VisitorsOrBrowseVO;


public interface IVisitorsOrBrowseServiceBLL extends ICommonServiceBLL<VisitorsOrBrowse, VisitorsOrBrowseDTO, VisitorsOrBrowseVO, VisitorsOrBrowseQuery> {

    /**
     * 浏览分页
     * @author: leroy
     * @date 2021/12/15 13:43
     * @param params
     * @return null
     * @update
     * @updateTime
     */
    PageResult<VisitorsOrBrowseVO> getListBrowse(VisitorsOrBrowseQuery params);



    /**
     * 保存或删除
     * @param operUserId 是创建者
     * @param userId    浏览者id
     * @author: leroy
     * @date 2021/12/15 15:25
     * @return null
     * @update
     * @updateTime
     */
    boolean saveOrDelecte(String operUserId,String userId);

}
