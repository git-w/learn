package com.elite.learn.userManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 民宿预定-团队客户表
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_group")
public class Group implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */

    private String id;

    /**
     * 团队姓名
     */

    private String GroupName;

    /**
     * 用户注册时使用个的帐号
     */

    private String UserName;

    private String Members;
    /**
     * 手机号
     */


    /**
     * 订单总数量
     */

    private Integer TotalOrderNum;

    /**
     * 更新者ID
     */

    private String UpdateOperatorID;

    /**
     * 更新者名称
     */

    private String UpdateOperatorName;

    /**
     * 添加时间
     */

    private Long CreateTime;

    /**
     * 更新时间
     */

    private Long UpdateTime;

    /**
     * 删除标识 0:未删除 1:删除
     */

    private Integer IsDelete;

    /**
     * 删除者ID
     */

    private String DeleteOperatorID;

    /**
     * 删除者名称
     */

    private String DeleteOperatorName;

    /**
     * 删除时间
     */

    private Long DeleteTime;

    /**
     * 备注
     */

    private String Memo;

    /**
     * 备用字段，如果占用，请备注做什么用
     */

    private String ExtraParams1;

    /**
     * 备用字段，如果占用，请备注做什么用
     */

    private String ExtraParams2;

    /**
     * 备用字段，如果占用，请备注做什么用
     */

    private String ExtraParams3;

    /**
     * 创建者ID
     */

    private String CreateOperatorID;

    /**
     * 创建者名称
     */

    private String CreateOperatorName;

    private  String  ModilePhone;

}
