package com.elite.learn.userManage.dto;

import lombok.Data;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 用户模块---用户信息审核表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Data
public class UserAuditDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * 审核状态：0-待审核，1-审核通过 2-审核不通过
     */
    private Integer isAudit;

    /**
     * 身份证正面照片地址
     */
    private String pinCodesProsUrl;


    /**
     * 身份证反面照片地址（长）
     */
    private String pinCodesConsUrl;

    /**
     * 手持身份证（长）
     */
    private String pinCodesHandUrl;

    /**
     * 头像、独白、语音独白在上传/修改时需要审核
     */
    private String introduce;

    /**
     * 用户头像
     */
    private String headimgurl;

    /**
     * 身份证正面照片地址
     */
    private String originalPinCodesProsUrl;

    /**
     * 身份证反面照片地址
     */
    private String originalPinCodesConsUrl;

    /**
     * 手持身份证
     */
    private String originalPinCodesHandUrl;

    /**
     * 头像、独白、语音独白在上传/修改时需要审核
     */
    private String originalIntroduce;

    /**
     * 用户头像
     */
    private String originalHeadimgurl;

    /**
     * 错误原因
     */
    private String reason;

     private String createOperatorName;

    /**
     * 创建者ID
     */
    private String createOperatorId;



    /**
     * 更新者ID
     */
    private String auditOperatorId;
    /**
     * 更新者名称
     */
    private String auditOperatorName;
    /**
     * 身份证
     */
    private String pinCodes;

    /**
     * 名称
     */
    private String trueName;

}
