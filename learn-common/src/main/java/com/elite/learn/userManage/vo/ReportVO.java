package com.elite.learn.userManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 举报反馈表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Data
public class ReportVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 用户id
     */
    private String userId;
    /**
     * 分类id
     */
    private String cateId;
    /**
     * 用户昵称
     */
    private String name;

    /**
     * 联系方式
     */
    private Integer mobilePhone;

    /**
     * 举报时间
     */
    private Long complaintsTime;

    /**
     * 举报内容
     */
    private String content;

    /**
     * 图片存放路径（短），多图用,隔开
     */
    private String imgPathList;
    /**
     *  图片存放路径（长），多图用,隔开
     */
    private  String imgPathListUrl;

    /**
     * 满意度 0非常满意 1满意 2一般 3不满意
     */
    private Integer satisFaction;

    /**
     * 备注
     */
    private String memo;

    /**
     * 处理结果
     */
    private String handingContent;

    /**
     * 处理状态 0:未处理 1:已处理
     */
    private Integer isState;


    /**
     * 创建者ID（反馈者）
     */
    private String createOperatorid;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;


    /**
     * 额外参数1
     */
    private String extraParams1;

    /**
     * 额外参数2
     */

    private String extraParams2;

    /**
     * 额外参数3
     */

    private String extraParams3;


    /**
     * 用户的昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String headimgurl;

    /**
     * 手机号
     */
    private String modilePhone;



    /**
     * 用户的昵称
     */
    private String coverNickName;

    /**
     * 用户头像
     */
    private String coverHeadimgurl;

    /**
     * 手机号
     */
    private String coverModilePhone;

    /**
     * 分类名称
     */
    private String cateName;
}