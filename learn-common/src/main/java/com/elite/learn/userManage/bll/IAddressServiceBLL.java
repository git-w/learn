package com.elite.learn.userManage.bll;

import com.elite.learn.userManage.dto.AddressLocationDTO;
import com.elite.learn.userManage.vo.AddressLocationVO;

public interface IAddressServiceBLL {

    /**
     * <p>
     * 根据经纬度获取位置数据
     * </p>
     *
     * @param params
     * @return com.elite.learn.base.system.entity.dto.AddressLocationDTO
     * @author: leroy
     * @date 2021/6/17 下午7:04
     */
    AddressLocationVO getLocation(AddressLocationDTO params);
}
