package com.elite.learn.userManage.service.impl;

import com.elite.learn.userManage.entity.UserMateSelection;
import com.elite.learn.userManage.mapper.UserMateSelectionMapper;
import com.elite.learn.userManage.service.IUserMateSelectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.vo.UserMateSelectionVO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息---附加表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-01
 */
@Service
public class UserMateSelectionServiceImpl extends ServiceImpl<UserMateSelectionMapper, UserMateSelection> implements IUserMateSelectionService {

    @Override
    public UserMateSelectionVO getInfo(String id) {
            return this.baseMapper.getInfo(id);
    }
}
