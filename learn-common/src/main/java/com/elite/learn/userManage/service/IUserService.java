package com.elite.learn.userManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.entity.User;
import com.elite.learn.userManage.vo.UserLoginVO;
import com.elite.learn.userManage.vo.UserVO;
import org.apache.ibatis.annotations.Param;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-30
 */
public interface IUserService extends IService<User> {

    /*
     *
     * @author: leroy
     * @date 2021/11/30 11:41
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    UserVO getInfo(String id);



    /*
     *
     * @author: leroy
     * @date 2021/11/30 11:42
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    IPage<UserVO> getLists(Page<UserVO> pageVo, @Param(Constants.WRAPPER)Wrapper<User> queryWrapper);

    /**
     *
     * @author: leroy
     * @date 2021/11/30 11:42
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    UserLoginVO getConditionInfo(@Param(Constants.WRAPPER)Wrapper<User> queryWrapper);


    /**
     * @author: leroy
     * @date 2021/11/30 11:42
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 条件搜索分页
     */
    IPage<UserVO> getConditionList(Page<UserVO> pageVo, @Param(Constants.WRAPPER)Wrapper<User> queryWrapper);
}
