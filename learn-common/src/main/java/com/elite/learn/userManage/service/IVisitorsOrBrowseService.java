package com.elite.learn.userManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.VisitorsOrBrowse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.VisitorsOrBrowseVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 浏览访客 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-15
 */
public interface IVisitorsOrBrowseService extends IService<VisitorsOrBrowse> {


    /**
     * 浏览分页
     *
     * @param queryWrapper
     * @return null
     * @author: leroy
     * @date 2021/12/15 10:05
     * @update
     * @updateTime
     */
    IPage<VisitorsOrBrowseVO> getListBrowse(Page<VisitorsOrBrowseVO> pageParams, @Param(Constants.WRAPPER) Wrapper<VisitorsOrBrowse> queryWrapper);


    /**
     * 访客分页
     *
     * @param queryWrapper
     * @return null
     * @author: leroy
     * @date 2021/12/15 10:10
     * @update
     * @updateTime
     */
    IPage<VisitorsOrBrowseVO> getList(Page<VisitorsOrBrowseVO> pageParams, @Param(Constants.WRAPPER) Wrapper<VisitorsOrBrowse> queryWrapper);
}
