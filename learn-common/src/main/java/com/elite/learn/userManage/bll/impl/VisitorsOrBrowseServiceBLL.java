package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.common.utils.string.StringUtils;
import com.elite.learn.userManage.bll.IVisitorsOrBrowseServiceBLL;
import com.elite.learn.userManage.dto.VisitorsOrBrowseDTO;
import com.elite.learn.userManage.entity.VisitorsOrBrowse;
import com.elite.learn.userManage.query.VisitorsOrBrowseQuery;
import com.elite.learn.userManage.service.IVisitorsOrBrowseService;
import com.elite.learn.userManage.vo.VisitorsOrBrowseVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 浏览访客表
 * @Title: VisitorsOrBrowseServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/15 9:54
 */
@Service
public class VisitorsOrBrowseServiceBLL implements IVisitorsOrBrowseServiceBLL {

    @Resource
    private IVisitorsOrBrowseService service;


    @Override
    public VisitorsOrBrowse get(String id) {
        return null;
    }

    @Override
    public VisitorsOrBrowseVO getInfo(String id) {
        return null;
    }



    @Override
    public String save(String operUserId, VisitorsOrBrowseDTO bean) {
        return null;
    }

    @Override
    public boolean update(String operUserId, VisitorsOrBrowseDTO bean) {
        return false;
    }

    @Override
    public boolean remove(String operUserId, String ids) {
        return false;
    }

    @Override
    public List<VisitorsOrBrowseVO> findAll(VisitorsOrBrowseQuery bean) {
        return null;
    }


    /**
     * 访客分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/15 10:07
     * @update
     * @updateTime
     */
    @Override
    public PageResult<VisitorsOrBrowseVO> getList(VisitorsOrBrowseQuery params) {
        QueryWrapper<VisitorsOrBrowse> queryWrapper = new QueryWrapper<VisitorsOrBrowse>();
        queryWrapper.isNotNull("tivob.create_time");
        queryWrapper.orderByDesc("tivob.create_time");



        // 根据查询
        if (Objects.nonNull(params.getUserId())) {
            queryWrapper.eq("tivob.user_id", params.getUserId());
        }

        Page<VisitorsOrBrowseVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<VisitorsOrBrowseVO> page = this.service.getList(PageParams, queryWrapper);


        List<VisitorsOrBrowseVO> result = page.getRecords();
        //有查询结果
        if (null != result && result.size() > 0) {
            for (VisitorsOrBrowseVO info : result) {
                if (StringUtils.isNotEmpty(info.getIntroduceVoiceUrl())) {
                    info.setIntroduceVoiceUrlPath(FileUploadUtil.getImgRootPath() + info.getIntroduceVoiceUrl());
                }

            }
        }

        return new PageResult<>(page);
    }


    /**
     * 浏览分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/15 10:00
     * @update
     * @updateTime
     */
    @Override
    public PageResult<VisitorsOrBrowseVO> getListBrowse(VisitorsOrBrowseQuery params) {
        QueryWrapper<VisitorsOrBrowse> queryWrapper = new QueryWrapper<VisitorsOrBrowse>();
        queryWrapper.isNotNull("tivob.create_time");
        queryWrapper.orderByDesc("tivob.create_time");

        // 根据查询
        if (StringUtil.notBlank(params.getCreateOperatorId())) {
            queryWrapper.eq("tivob.create_operator_id", params.getCreateOperatorId());
        }

        Page<VisitorsOrBrowseVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
         IPage<VisitorsOrBrowseVO> page = this.service.getListBrowse(PageParams, queryWrapper);



        List<VisitorsOrBrowseVO> result = page.getRecords();
        //有查询结果
        if (null != result && result.size() > 0) {
            for (VisitorsOrBrowseVO info : result) {
                if (StringUtils.isNotEmpty(info.getIntroduceVoiceUrl())) {
                    info.setIntroduceVoiceUrlPath(FileUploadUtil.getImgRootPath() + info.getIntroduceVoiceUrl());
                }

            }
        }
        return new PageResult<>(page);

    }

    /**
     * @param operUserId 是创建者
     * @param userId     浏览者id
     * @return
     */
    @Override
    public boolean saveOrDelecte(String operUserId, String userId) {
        //先去条件删除一次
        QueryWrapper<VisitorsOrBrowse> queryWrapper = new QueryWrapper<VisitorsOrBrowse>();
        queryWrapper.eq("user_id", userId);

        queryWrapper.eq("create_operator_id", operUserId);
        this.service.remove(queryWrapper);
        //创建实体对象
        VisitorsOrBrowse info = new VisitorsOrBrowse();
        //用户id
        info.setUserId(userId);
        //创建者id
        info.setCreateOperatorId(operUserId);
        info.setCreateTime(System.currentTimeMillis());
        return this.service.save(info);

    }


}
