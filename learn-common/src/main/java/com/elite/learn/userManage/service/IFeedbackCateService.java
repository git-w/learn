package com.elite.learn.userManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.FeedbackCate;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.FeedbackCateVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户意见反馈分类表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
public interface IFeedbackCateService extends IService<FeedbackCate> {

    /**
     *
     * @author: leroy
     * @date 2021/11/26 10:49
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    FeedbackCateVO getInfo(String id);


    /**
     *
     * @author: leroy
     * @date 2021/11/26 10:49
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    IPage<FeedbackCateVO> getLists(Page<FeedbackCateVO> pageParams, @Param(Constants.WRAPPER) Wrapper<FeedbackCate> queryWrapper);


    /**
     *
     * @author: leroy
     * @date 2021/11/26 10:49
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<FeedbackCateVO> findAll(@Param(Constants.WRAPPER)Wrapper<FeedbackCate> queryWrapper);
}
