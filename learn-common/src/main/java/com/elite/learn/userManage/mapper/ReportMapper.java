package com.elite.learn.userManage.mapper;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Report;
import com.elite.learn.userManage.vo.ReportVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 举报反馈表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
public interface ReportMapper extends BaseMapper<Report> {

    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:11
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    ReportVO getInfo(String id);





    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:12
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     * 分页
     */

    List<ReportVO> getLists(Page<ReportVO> pageParams, @Param(Constants.WRAPPER) Wrapper<Report> queryWrapper);
}
