package com.elite.learn.userManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.ReportCate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.userManage.vo.ReportCateVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户举报反馈分类表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
public interface ReportCateMapper extends BaseMapper<ReportCate> {

    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:07
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    ReportCateVO getInfo(String id);

    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:08
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<ReportCateVO> getLists(Page<ReportCateVO> pageVo, @Param(Constants.WRAPPER) Wrapper<ReportCate> queryWrapper);

    /**
     * 查询所有
     * @author: leroy
     * @date 2021/12/6 16:15
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<ReportCateVO> findAll(@Param(Constants.WRAPPER) Wrapper<ReportCate> queryWrapper);
}
