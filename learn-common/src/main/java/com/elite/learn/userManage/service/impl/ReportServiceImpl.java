package com.elite.learn.userManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Report;
import com.elite.learn.userManage.mapper.ReportMapper;
import com.elite.learn.userManage.service.IReportService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.userManage.vo.ReportVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 举报反馈表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Service
public class ReportServiceImpl extends ServiceImpl<ReportMapper, Report> implements IReportService {


    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:10
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public ReportVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }








    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:12
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    @Override
    public IPage<ReportVO> getLists(Page<ReportVO> pageParams, @Param(Constants.WRAPPER) Wrapper<Report> queryWrapper) {
        pageParams.setRecords(this.baseMapper.getLists(pageParams, queryWrapper));
        return pageParams;
    }

}
