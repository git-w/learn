package com.elite.learn.userManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.Group;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.userManage.vo.GroupVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 民宿预定-团队客户表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-06-03
 */
public interface IGroupService extends IService<Group> {
    /**
     * 用户分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<GroupVO> selectLists(Page<GroupVO> page, @Param(Constants.WRAPPER) Wrapper<Group> queryWrapper);


    /**
     * 用户详情
     *
     * @param queryWrapper
     * @return
     */
    GroupVO selectInfo(@Param(Constants.WRAPPER) Wrapper<Group> queryWrapper);
}
