package com.elite.learn.userManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.VisitorsOrBrowse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.userManage.vo.VisitorsOrBrowseVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 浏览访客 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-15
 */
public interface VisitorsOrBrowseMapper extends BaseMapper<VisitorsOrBrowse> {


    /**
     * 浏览分页
     * @author: leroy
     * @date 2021/12/15 10:13
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<VisitorsOrBrowseVO> getListBrowse(Page<VisitorsOrBrowseVO> pageParams, @Param(Constants.WRAPPER)Wrapper<VisitorsOrBrowse> queryWrapper);


    /**
     * 访客分页
     * @author: leroy
     * @date 2021/12/15 10:13
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<VisitorsOrBrowseVO> getList(Page<VisitorsOrBrowseVO> pageParams, @Param(Constants.WRAPPER) Wrapper<VisitorsOrBrowse> queryWrapper);
}
