package com.elite.learn.userManage.vo;

import lombok.Data;

import java.util.List;

/**
 * @Title :AddressLocationVO.java
 * @Package: com.elite.learn.userManage.vo
 * @Description:
 * @author: leroy
 * @data: 2021/12/14 17:09
 */
@Data
public class AddressLocationVO {
    /**
     * 编号
     */
    private String id;

    /**
     * 城市名称
     */
    private String CityName;

/*    *//**
     * 区、县集合
     *//*
    private List<AddressDistrict> AddressDistrict;*/

    /**
     * 详细地址
     */
    private String detailedAddress;
}