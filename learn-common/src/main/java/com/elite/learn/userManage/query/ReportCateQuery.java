package com.elite.learn.userManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户举报反馈分类表
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
@Data

public class ReportCateQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 分类名称
     */
    private String cateName;

    /**
     * 排序 数值越大越靠前
     */
    private Integer sort;

    /**
     * 备注
     */
    private String memo;

    /**
     * 是否上线 0是 1否
     */
    private Integer isState;
    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 额外参数1
     */
    private String extraParams1;

    private String  createOperatorName;
    private String updateOperatorName;

}
