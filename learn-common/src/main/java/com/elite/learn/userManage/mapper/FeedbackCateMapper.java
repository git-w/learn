package com.elite.learn.userManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.userManage.entity.FeedbackCate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.userManage.vo.FeedbackCateVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户意见反馈分类表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-11-26
 */
public interface FeedbackCateMapper extends BaseMapper<FeedbackCate> {

    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:03
     * @param id
     *
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    FeedbackCateVO getInfo(String id);

    /**
     *
     * @author: leroy
     * @date 2021/11/26 15:03
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<FeedbackCateVO> getLists(Page<FeedbackCateVO> pageVo, @Param(Constants.WRAPPER) Wrapper<FeedbackCate> queryWrapper);

    /**
     *查询所有
     * @author: leroy
     * @date 2021/12/6 19:22
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    List<FeedbackCateVO> findAll(@Param(Constants.WRAPPER)Wrapper<FeedbackCate> queryWrapper);
}
