package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.userManage.bll.IUserMateSelectionServiceBLL;
import com.elite.learn.userManage.dto.UserMateSelectionDTO;
import com.elite.learn.userManage.entity.UserMateSelection;
import com.elite.learn.userManage.query.UserMateSelectionQuery;
import com.elite.learn.userManage.service.IUserMateSelectionService;
import com.elite.learn.userManage.vo.UserMateSelectionVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 用户附属表
 * @Title: UserMateSelectionServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/1 14:05
 */
@Service
public class UserMateSelectionServiceBLL implements IUserMateSelectionServiceBLL {


    @Resource
    private IUserMateSelectionService service;


    /*
     *
     * @author: leroy
     * @date 2021/12/1 14:09
     * @param null
     * @return null
     * @update
     * @updateTime
     * 对象id
     */
    @Override
    public UserMateSelection get(String id) {
        return this.service.getById(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/12/1 14:10
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public UserMateSelectionVO getInfo(String id) {
        return this.service.getInfo(id);
    }



    @Override
    public String save(String operUserId, UserMateSelectionDTO bean) {
        return null;
    }


    /*
     *
     * @author: leroy
     * @date 2021/12/1 14:10
     * @param null
     * @return null
     * @update
     * @updateTime
     * 更新
     */
    @Override
    public boolean update(String operUserId, UserMateSelectionDTO bean) {
        UserMateSelectionVO info = this.getInfo(bean.getUserId());
        if (Objects.isNull(info)){
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        QueryWrapper<UserMateSelection> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", bean.getUserId());
        //获取实体对象
        UserMateSelection infoNew = new UserMateSelection();
        //调用用户的id
        infoNew.setUserId(bean.getUserId());
        //年龄可选范围：18-99（最小年龄选不限、最多年龄选30，则显示：30岁及以下）
        infoNew.setAgeOptionalRange(bean.getAgeOptionalRange());
        //身高可选范围：145-250（最低选160cm、最高选不限，则显示：160cm及以上）
        infoNew.setAgeOptionalRange(bean.getAgeOptionalRange());
        //学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士
        infoNew.setEducationBackground(bean.getEducationBackground());
        //婚姻状况：0-未婚、1-离异、2-丧偶
        infoNew.setMaritalStatus(bean.getMaritalStatus());
        //子女状况：0-无子女、1-有子女
        infoNew.setChildrenStatus(bean.getChildrenStatus());
        //用户所在省份名称
        infoNew.setProvinceName(bean.getProvinceName());
        //用户所在城市名称
        infoNew.setCityName(bean.getCityName());
        //用户所在省份id
        infoNew.setProvinceId(bean.getProvinceId());
        //用户所在城市id
        infoNew.setCityId(bean.getCityId());
        // 住房情况：0-已购房、1-租房、2-有需要购房、3-其他
        infoNew.setHousingStatus(bean.getHousingStatus());


        infoNew.setHeightOptionalRange(bean.getHeightOptionalRange());

        //购车情况：0-已购车、1-有需要购车、2-其他
        infoNew.setCarStatus(bean.getCarStatus());
        return this.service.update(infoNew, wrapper);

    }

    @Override
    public boolean remove(String operUserId, String ids) {
        return false;
    }

    @Override
    public List<UserMateSelectionVO> findAll(UserMateSelectionQuery bean) {
        return null;
    }

    @Override
    public PageResult<UserMateSelectionVO> getList(UserMateSelectionQuery params) {
        return null;
    }
}