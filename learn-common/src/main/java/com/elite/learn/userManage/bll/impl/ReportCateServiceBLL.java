package com.elite.learn.userManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IReportCateServiceBLL;
import com.elite.learn.userManage.dto.ReportCateDTO;
import com.elite.learn.userManage.entity.ReportCate;
import com.elite.learn.userManage.query.ReportCateQuery;
import com.elite.learn.userManage.service.IReportCateService;
import com.elite.learn.userManage.vo.ReportCateVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 用戶举报反馈表
 * @Title: InfoReportCateServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/26 8:56
 */
@Service
public class ReportCateServiceBLL implements IReportCateServiceBLL {


    @Resource
    private IReportCateService service;


    /*
     *
     * @author: leroy
     * @date 2021/11/26 13:23
     * @param null
     * @return null
     * @update
     * @updateTime
     * 主键id
     */
    @Override
    public ReportCate get(String id) {
        return this.service.getById(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 13:24
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public ReportCateVO getInfo(String id) {
        return this.service.getInfo(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 13:25
     * @param null
     * @return null
     * @update
     * @updateTime
     * 添加
     */
    @Override
    public String save(String operUserId, ReportCateDTO bean) {
        //创建实体对象
        ReportCate info = new ReportCate();
        //获取对象id
        info.setId(IdUtils.simpleUUID());
        //分类名称
        info.setCateName(bean.getCateName());
        //排序 数值越大越靠前
        info.setSort(bean.getSort());
        //备注
        info.setMemo(bean.getMemo());
        //是否上线 0是 1否
        info.setIsState(bean.getIsState());
        //预留字段
        info.setExtraParams1(bean.getExtraParams1());

        //删除标识 0:未删除 1:删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //创建者ID
        info.setCreateOperatorId(operUserId);
        //创建者姓名
        info.setCreateOperatorName(bean.getCreateOperatorName());
        //创建时间
        info.setCreateTime(System.currentTimeMillis());

        boolean save = this.service.save(info);
        return save ? info.getId() : null;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/26 13:41
     * @param null
     * @return null
     * @update
     * @updateTime
     * 更新
     */
    @Override
    public boolean update(String operUserId, ReportCateDTO bean) {
        // 判断通过ID获取的对象是否为空
        ReportCate info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //创建实体对象
        ReportCate infoNew = new ReportCate();
        //获取对象id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        //分类名称
        if (StringUtil.notBlank(bean.getCateName())) {
            infoNew.setCateName(bean.getCateName());
        }
        //排序 数值越大越靠前
        infoNew.setSort(bean.getSort());
        //备注
        infoNew.setMemo(bean.getMemo());
        //是否上线 0是 1否
        infoNew.setIsState(bean.getIsState());
        //额外参数1
        infoNew.setExtraParams1(bean.getExtraParams1());


        //更新者ID
        infoNew.setUpdateOperatorId(operUserId);
        //更新时间
        infoNew.setUpdateTime(System.currentTimeMillis());
        //更新者姓名
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        return this.service.updateById(infoNew);

    }


    /**
     * 刪除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2022/1/14 10:30
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        //分割id
        String[] idArr = ids.split(",");
        //遍历id判断是否为空，弹出本次循环
        for (String id : idArr) {
            ReportCate info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            ReportCate infoDel = new ReportCate();
            infoDel.setId(info.getId());
            //设置删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            //删除id
            infoDel.setDeleteOperatorId(operUserId);

            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /*
     * 查询所有
     * @author: leroy
     * @date 2021/12/6 17:13
     * @param null
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public List<ReportCateVO> findAll(ReportCateQuery bean) {
        QueryWrapper<ReportCate> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.orderByDesc("is_state");
        // 根据分组查询
        if (Objects.nonNull(bean.getIsState())) {
            queryWrapper.eq("is_state", bean.getIsState());
        }

        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        List<ReportCateVO> list = this.service.findAll(queryWrapper);
        return list;
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2022/1/14 10:31
     * @update
     * @updateTime
     */
    @Override
    public PageResult<ReportCateVO> getList(ReportCateQuery params) {
        QueryWrapper<ReportCate> queryWrapper = new QueryWrapper<ReportCate>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("create_time");
        // 根据分组查询
        if (StringUtil.notBlank(params.getCateName())) {
            queryWrapper.eq("cate_name", params.getCateName());
        }

        Page<ReportCateVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<ReportCateVO> page = this.service.getLists(PageParams, queryWrapper);

        return new PageResult<>(page);
    }


    /**
     * 批量上下线
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/7 9:33
     * @update
     * @updateTime
     */
    @Override
    public boolean updateMultiIsEnabled(String operUserId, BaseParams params) {
        ReportCate infoNew = null;
        String[] idsArr = params.getIds().split(",");
        boolean flag = true;
        for (String id : idsArr) {
            ReportCate info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }

            infoNew = new ReportCate();
            infoNew.setId(info.getId());
            //状态   0:启用   1:停用
            if (Objects.nonNull(params.getIsState())) {
                infoNew.setIsState(params.getIsState());
            } else {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
            }
            infoNew.setUpdateTime(System.currentTimeMillis());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }
}