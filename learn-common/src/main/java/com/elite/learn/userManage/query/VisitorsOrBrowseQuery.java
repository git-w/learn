package com.elite.learn.userManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 浏览访客
 * </p>
 *
 * @author: leroy
 * @since 2021-12-15
 */
@Data

public class VisitorsOrBrowseQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建时间
     */
    private Long createTime;


}
