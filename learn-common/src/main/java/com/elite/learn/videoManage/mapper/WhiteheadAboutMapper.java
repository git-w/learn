package com.elite.learn.videoManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.videoManage.entity.WhiteheadAbout;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.videoManage.vo.WhiteheadAboutVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 白首约表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
public interface WhiteheadAboutMapper extends BaseMapper<WhiteheadAbout> {




    /**
     *
     * @author: leroy
     * @date 2021/12/7 11:20
     * @param pageParams
     * @return null
     * @update
     * @updateTime
     */
    List<WhiteheadAboutVO> getLists(Page<WhiteheadAboutVO> pageParams, @Param(Constants.WRAPPER) Wrapper<WhiteheadAbout> queryWrapper);


    /**
     *
     * @author: leroy
     * @date 2021/12/7 11:20
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    WhiteheadAboutVO getInfo(String id);


}
