package com.elite.learn.videoManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 白首约表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-05
 */
@Data

public class WhiteheadAboutVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */

    private String id;

    /**
     * 分类id
     */
    private String cardId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 名称
     */
    private String name;

    /**
     * 封面
     */
    private String theCover;

    /**
     * url短
     */
    private String url;

    /**
     * 图片存放路径（长）
     */
    private String imgPathUrl;

    /**
     * 备注
     */
    private String memo;

    /**
     * 状态 0:上线 1:下线
     */
    private Integer isState;


    /**
     * 创建者ID
     */
    private String createOperatorid;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;


    /**
     * 额外参数1
     */
    private String extraParams1;

}
