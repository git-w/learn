package com.elite.learn.videoManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 白首约表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_whitehead_about")
public class WhiteheadAbout implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 分类id  
     */
    private String cardId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 名称
     */
    private String name;

    /**
     * 封面
     */
    private String theCover;

    /**
     * url
     */
    private String url;

    /**
     * 备注
     */
    private String memo;

    /**
     * 状态 0:上线 1:下线
     */
    private Integer isState;

    /**
     * 删除标识 0:未删除 1:已删除
     */
    private Integer isDelete;

    /**
     * 创建者ID
     */
    private String createOperatorid;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 删除者名称
     */
    private String deleteOperatorId;

    /**
     * 删除时间
     */
    private Long deleteTime;

    /**
     * 额外参数1
     */
    private String extraParams1;


}
