package com.elite.learn.videoManage.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 白首约表
 * </p>
 *
 * @author: leroy
 * @since 2021-12-05
 */
@Data

public class WhiteheadAboutQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;


    /**
     * 名称
     */
    private String name;

    /**
     * 创建时间
     */
    private Long createTime;


    /**
     * 状态 0:上线 1:下线
     */
    private Integer isState;
}
