package com.elite.learn.videoManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.videoManage.entity.WhiteheadAbout;
import com.elite.learn.videoManage.mapper.WhiteheadAboutMapper;
import com.elite.learn.videoManage.service.IWhiteheadAboutService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.videoManage.vo.WhiteheadAboutVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 白首约表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
@Service
public class WhiteheadAboutServiceImpl extends ServiceImpl<WhiteheadAboutMapper, WhiteheadAbout> implements IWhiteheadAboutService {


    /**
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/7 11:18
     * @update
     * @updateTime
     */
    @Override
    public WhiteheadAboutVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     * 分页
     *
     * @param pageParams
     * @return null
     * @author: leroy
     * @date 2021/12/7 11:18
     * @update
     * @updateTime
     */
    @Override
    public IPage<WhiteheadAboutVO> getLists(Page<WhiteheadAboutVO> pageParams, @Param(Constants.WRAPPER) Wrapper<WhiteheadAbout> queryWrapper) {
        return pageParams.setRecords(this.baseMapper.getLists(pageParams, queryWrapper));

    }
}