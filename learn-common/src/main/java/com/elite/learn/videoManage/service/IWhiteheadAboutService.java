package com.elite.learn.videoManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.videoManage.entity.WhiteheadAbout;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.videoManage.vo.WhiteheadAboutVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 白首约表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
public interface IWhiteheadAboutService extends IService<WhiteheadAbout> {


    /**
     * 主键id
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/7 11:05
     * @update
     * @updateTime
     */
    WhiteheadAboutVO getInfo(String id);


    /**
     * 分页
     *
     * @param queryWrapper
     * @return null
     * @author: leroy
     * @date 2021/12/7 11:08
     * @update
     * @updateTime
     */
    IPage<WhiteheadAboutVO> getLists(Page<WhiteheadAboutVO> pageParams, @Param(Constants.WRAPPER) Wrapper<WhiteheadAbout> queryWrapper);
}
