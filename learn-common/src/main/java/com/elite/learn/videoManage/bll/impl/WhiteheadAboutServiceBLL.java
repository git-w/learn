package com.elite.learn.videoManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.videoManage.bll.IWhiteheadAboutServiceBLL;
import com.elite.learn.videoManage.contants.VideoManageExceptionCodeEnum;
import com.elite.learn.videoManage.dto.WhiteheadAboutDTO;
import com.elite.learn.videoManage.entity.WhiteheadAbout;
import com.elite.learn.videoManage.query.WhiteheadAboutQuery;
import com.elite.learn.videoManage.service.IWhiteheadAboutService;
import com.elite.learn.videoManage.vo.WhiteheadAboutVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 白首约表
 * @Title: CommonServiceBLL
 * @Package com.elite.learn.videoManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/5 22:14
 */
@Service
public class WhiteheadAboutServiceBLL implements IWhiteheadAboutServiceBLL {

    @Resource
    private IWhiteheadAboutService service;


    /**
     * 主键ID
     *
     * @param id
     * @return null
     * @author: leroy
     * @date 2021/12/5 22:16
     * @update
     * @updateTime
     */
    @Override
    public WhiteheadAbout get(String id) {
        return this.service.getById(id);
    }


    /**
     * @param id
     * @return null
     * @详情author Charon
     * @date 2021/12/5 22:16
     * @update
     * @updateTime
     */
    @Override
    public WhiteheadAboutVO getInfo(String id) {
        WhiteheadAboutVO info = this.service.getInfo(id);
        if (Objects.isNull(info)&&StringUtil.notBlank(info.getTheCover())) {
            //拼接主图长地址
            info.setImgPathUrl(FileUploadUtil.getImgRootPath() + info.getTheCover());

        }
        return info;
    }


    /**
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/5 22:18
     * @update
     * @updateTime 保存
     */
    @Override
    public String save(String operUserId, WhiteheadAboutDTO bean) {
        //获取实体对象
        WhiteheadAbout info = new WhiteheadAbout();
        //用户生成id
        info.setId(IdUtils.simpleUUID());
        //分类id
        info.setCardId(bean.getCardId());
        //用户ID
        info.setUserId(bean.getUserId());
        //名称
        info.setName(bean.getName());
        //封面
        info.setTheCover(bean.getTheCover());
        //url
        info.setUrl(bean.getUrl());
        //备注
        info.setMemo(bean.getMemo());
        //状态 0:上线 1:下线
        info.setIsState(bean.getIsState());

        //删除标识 0:未删除 1:已删除
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        //创建者ID
        info.setCreateOperatorid(operUserId);
        //创建时间
        info.setCreateTime(System.currentTimeMillis());
        info.setCreateOperatorName(bean.getCreateOperatorName());
        //额外参数1
        info.setExtraParams1(bean.getExtraParams1());

        boolean save = this.service.save(info);
        return save ? info.getId() : null;
    }


    /**
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/5 22:26
     * @update
     * @updateTime
     */
    @Override
    public boolean update(String operUserId, WhiteheadAboutDTO bean) {
        // 判断通过ID获取的对象是否为空
        WhiteheadAbout info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //获取实体对象
        WhiteheadAbout infoNew = new WhiteheadAbout();
        //用户生成id
        infoNew.setId(bean.getId());
        //分类id
        if (StringUtil.notBlank(bean.getCardId())) {
            infoNew.setCardId(bean.getCardId());
        }
        //用户ID
        if (StringUtil.notBlank(bean.getUserId())) {
            infoNew.setUserId(bean.getUserId());
        }
        //名称
        if (StringUtil.notBlank(bean.getName())) {
            infoNew.setName(bean.getName());
        }
        //封面
        if (Objects.nonNull(bean.getTheCover())) {
            infoNew.setTheCover(bean.getTheCover());
        }
        //url
        if (StringUtil.notBlank(bean.getUrl())) {
            infoNew.setUrl(bean.getUrl());
        }
        //备注
        if (StringUtil.notBlank(bean.getMemo())) {
            infoNew.setMemo(bean.getMemo());
        }
        //状态 0:上线 1:下线
        if (Objects.nonNull(bean.getIsState())) {
            infoNew.setIsState(bean.getIsState());
        }
        //备用
        if (StringUtil.notBlank(bean.getExtraParams1())) {
            infoNew.setExtraParams1(bean.getExtraParams1());
        }

        //更新者状态
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateOperatorId(operUserId);

        infoNew.setUpdateTime(System.currentTimeMillis());
        return this.service.updateById(infoNew);

    }


    /**
     * 删除
     *
     * @param ids
     * @return null
     * @author: leroy
     * @date 2021/12/5 22:32
     * @update
     * @updateTime
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        WhiteheadAbout infoDel=null;
        //分割id
        String[] idArr = ids.split(",");
        //遍历id判断是否为空，弹出本次循环
        for (String id : idArr) {
            WhiteheadAbout info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new WhiteheadAbout();
            infoDel.setId(info.getId());
            //设置删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            //删除时间
            infoDel.setDeleteTime(System.currentTimeMillis());
            //删除id
            infoDel.setDeleteOperatorId(operUserId);

            boolean result  = service.updateById(infoDel);
            if (!result){
                flag=false;
            }
        }
        if (!flag){
            //修改失败
            throw new CommonException(VideoManageExceptionCodeEnum.SelectFail.code,
                    VideoManageExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    @Override
    public List<WhiteheadAboutVO> findAll(WhiteheadAboutQuery bean) {
        return null;
    }



    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/5 22:37
     * @update
     * @updateTime 分页
     */
    @Override
    public PageResult<WhiteheadAboutVO> getList(WhiteheadAboutQuery params) {

        QueryWrapper<WhiteheadAbout> queryWrapper = new QueryWrapper<WhiteheadAbout>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("create_time");


        if (StringUtil.notBlank(params.getName())) {
            queryWrapper.eq("name", params.getName());

        }

        if (Objects.nonNull(params.getIsState())) {
            queryWrapper.eq("is_state", params.getIsState());
        }

        Page<WhiteheadAboutVO> PageParams = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<WhiteheadAboutVO> page = this.service.getLists(PageParams, queryWrapper);
        //有查询结果
        if (null != page.getRecords() && page.getRecords().size() > 0) {
            for (WhiteheadAboutVO info : page.getRecords()) {
                if (StringUtil.notBlank(info.getTheCover())) {
                    //拼接主图长地址
                    info.setImgPathUrl(FileUploadUtil.getImgRootPath() + info.getTheCover());
                }

            }
        }

        return new PageResult<>(page);
    }


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/6 7:08
     * @update
     * @updateTime
     */
    @Override
    public boolean updateMultiState(String operUserId, BaseParams params) {
        WhiteheadAbout infoNew = null;
        String[] idsArr = params.getIds().split(",");
        boolean flag = true;
        for (String id : idsArr) {
            WhiteheadAbout info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoNew = new WhiteheadAbout();
            infoNew.setId(info.getId());
            //状态   0:启用   1:停用
            if (Objects.nonNull(params.getIsState())) {
                infoNew.setIsState(params.getIsState());
            }
            infoNew.setUpdateTime(System.currentTimeMillis());
            infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
            boolean result = this.service.updateById(infoNew);

            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(VideoManageExceptionCodeEnum.SelectFail.code, VideoManageExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }
}