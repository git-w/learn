package com.elite.learn.videoManage.bll;

import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.videoManage.dto.WhiteheadAboutDTO;
import com.elite.learn.videoManage.entity.WhiteheadAbout;
import com.elite.learn.videoManage.query.WhiteheadAboutQuery;
import com.elite.learn.videoManage.vo.WhiteheadAboutVO;

public interface IWhiteheadAboutServiceBLL extends ICommonServiceBLL<WhiteheadAbout, WhiteheadAboutDTO, WhiteheadAboutVO, WhiteheadAboutQuery> {



    /**
     * 多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateMultiState(String operUserId, BaseParams params);
}
