package com.elite.learn.videoManage.contants;

public enum VideoManageExceptionCodeEnum {


    SaveSuccess(100000, "保存成功"),
    SelectSuccess(100000, "查询成功"),
    DeleteSuccess(100000, "删除成功"),
    UpdateSuccess(100000, "修改成功"),
    LoginError(100001, "登录错误"),
    IdParamsError(100001, "id编号，参数为空"),
    UserLoginFailDelete(-1, "登录失败，您已被删除！"),
    AuthorizationFail(100005, "授权失败"),
    SaveFail(100005, "保存失败"),
    DeleteFail(100005, "删除失败"),
    SelectFail(100005, "查询失败"),
    UpdateFail(100005, "修改失败");

    public Integer code;
    public String msg;

    private VideoManageExceptionCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
