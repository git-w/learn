package com.elite.learn.systemManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.elite.learn.systemManage.entity.ImagesCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elite.learn.systemManage.vo.ImagesCategoryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 素材库--分类表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
public interface ImagesCategoryMapper extends BaseMapper<ImagesCategory> {


    /**
     * 查全部
     *
     * @return
     */
    List<ImagesCategoryVO> findAll(@Param(Constants.WRAPPER) Wrapper<ImagesCategory> queryWrapper);
}
