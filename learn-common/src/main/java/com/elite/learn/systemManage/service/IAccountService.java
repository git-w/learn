package com.elite.learn.systemManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.systemManage.dto.AccountDTO;
import com.elite.learn.systemManage.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统管理-管理员表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
public interface IAccountService extends IService<Account> {


    /**
     * 管理员分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<AccountDTO> selectLists(Page<AccountDTO> page, @Param(Constants.WRAPPER) Wrapper<Account> queryWrapper);


    /**
     * 用户详情
     *
     * @param queryWrapper
     * @return
     */
    AccountDTO selectInfo(@Param(Constants.WRAPPER) Wrapper<Account> queryWrapper);
}
