package com.elite.learn.systemManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.elite.learn.systemManage.entity.ImagesCategory;
import com.elite.learn.systemManage.mapper.ImagesCategoryMapper;
import com.elite.learn.systemManage.service.IImagesCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.systemManage.vo.ImagesCategoryVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 素材库--分类表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Service
public class ImagesCategoryServiceImpl extends ServiceImpl<ImagesCategoryMapper, ImagesCategory> implements IImagesCategoryService {

    /**
     * 全部
     * @author: leroy
     * @date 2022/1/14 15:46
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public List<ImagesCategoryVO> findAll(Wrapper<ImagesCategory> queryWrapper) {
        return baseMapper.findAll(queryWrapper);
    }
}
