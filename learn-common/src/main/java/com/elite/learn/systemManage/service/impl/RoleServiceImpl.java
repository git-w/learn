package com.elite.learn.systemManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.systemManage.dto.RoleDTO;
import com.elite.learn.systemManage.entity.Role;
import com.elite.learn.systemManage.mapper.RoleMapper;
import com.elite.learn.systemManage.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统管理-角色管理表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {


    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<RoleDTO> selectLists(Page<RoleDTO> page, @Param(Constants.WRAPPER) Wrapper<Role> queryWrapper) {
        page.setRecords(this.baseMapper.selectLists(page, queryWrapper));
        return page;
    }


    /**
     * 查看用户详情
     *
     * @param queryWrapper
     * @return
     */
    @Override
    public RoleDTO selectInfo(Wrapper<Role> queryWrapper) {
        return this.baseMapper.selectInfo(queryWrapper);
    }

}
