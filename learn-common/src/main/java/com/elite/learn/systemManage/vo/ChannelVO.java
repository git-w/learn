package com.elite.learn.systemManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Title ChannelVo
 * @Package com.elite.learn.systemManage.vo
 * @Description
 * @author: leroy
 * @Date 2021-07-20 13:56
 */
@Data
public class ChannelVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 管理账号名
     */
    private String accountNumber;

    /**
     * 联系人名称
     */
    private String contactsName;

    /**
     * 联系人手机
     */
    private String contactsPhone;

    /**
     * 头像路径(短)
     */
    private String channelPhoto;

    /**
     * 房间id(逗号分隔)
     */
    private String houseId;

    /**
     * 期区id(逗号分隔)
     */
    private String areaId;

    /**
     * 角色ID
     */
    private String roleId;

    /**
     * 用户单独设置权限
     */
    private String accountAuthority;

    /**
     * 管理的房源个数
     */
    private Integer houseCount;

    /**
     * 账号状态 0:正常/启用 1:冻结/禁用
     */
    private Integer isState;

    /**
     * 备注
     */
    private String memo;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 关联房屋类型0:期区; 1:房型
     */
    private Integer relevanceType;
}
