package com.elite.learn.systemManage.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 系统管理-菜单表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
public class MenuDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 菜单名称
     */
    private String MenuName;

    /**
     * 菜单或按钮icon
     */
    private String Icon;

    /**
     * 菜单标识
     */
    private String MenuCode;

    /**
     * 菜单层级 1:一级菜单 2:二级菜单 依次类推...
     */
    private Integer MenuLevel;

    /**
     * 父菜单标识
     */
    private String ParentCode;

    /**
     * 父菜单ID
     */
    private String ParentID;

    /**
     * 排序号 数据越高排序越大 0是最低
     */
    private Integer Sort;

    /**
     * 类型 0-菜单 1-按钮
     */
    private Integer MenuType;

    /**
     * 是否启用 0-是 1-否
     */
    private Integer IsState;

    /**
     * 按钮区分 1：增改  2：查询 3：删除
     */
    private Integer ButtonType;

    /**
     * 链接地址
     */
    private String Url;

    /**
     * 备注
     */
    private String Memo;

    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    private Long CreateTime;

    /**
     * 更新者ID
     */
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    private String UpdateOperatorName;

    /**
     * 更新时间
     */
    private Long UpdateTime;

    /**
     * 额外参数1
     */
    private String ExtraParams1;

    /**
     * 额外参数2
     */
    private String ExtraParams2;

    /**
     * 额外参数3
     */
    private String ExtraParams3;



    /**
     * 菜单或按钮icon  长路径
     */
    private String IconPath;

    /**
     * 页面路由
     */
    private String PagePath;
}
