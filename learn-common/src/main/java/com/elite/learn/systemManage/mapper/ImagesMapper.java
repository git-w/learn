package com.elite.learn.systemManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.systemManage.entity.Images;
import com.elite.learn.systemManage.vo.ImagesVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 素材库--图片 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
public interface ImagesMapper extends BaseMapper<Images> {


    /**
     * 获取分页
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    List<ImagesVO> selectLists(Page<ImagesVO> page, @Param(Constants.WRAPPER) Wrapper<Images> queryWrapper);


    /**
     * 查看全部
     *
     * @param queryWrapper
     * @return
     */
    List<ImagesVO> findAll(@Param(Constants.WRAPPER) Wrapper<Images> queryWrapper);
}
