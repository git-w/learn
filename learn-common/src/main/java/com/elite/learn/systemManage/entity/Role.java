package com.elite.learn.systemManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统管理-角色管理表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_role")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "ID", type = IdType.UUID)
    private String id;

    /**
     * 角色名称
     */
    @TableField("RoleName")
    private String RoleName;

    /**
     * 是否可以编辑 0-是   1-否
     */
    @TableField("IsUpdate")
    private Integer IsUpdate;

    /**
     * 账号状态 0:正常/启用 1:不启用
     */
    @TableField("IsStatus")
    private Integer IsStatus;

    /**
     * 是否是超管 0-是 1-否
     */
    @TableField("IsSuperTube")
    private Integer IsSuperTube;

    /**
     * 部门id
     */
    @TableField("DepartmentID")
    private String DepartmentID;

    /**
     * 备注
     */
    @TableField("Memo")
    private String Memo;

    /**
     * 删除标识 0:未删除 1:删除
     */
    @TableField("IsDelete")
    private Integer IsDelete;

    /**
     * 创建者ID
     */
    @TableField("CreateOperatorID")
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    @TableField("CreateOperatorName")
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    @TableField("CreateTime")
    private Long CreateTime;

    /**
     * 更新者ID
     */
    @TableField("UpdateOperatorID")
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    @TableField("UpdateOperatorName")
    private String UpdateOperatorName;

    /**
     * 更新时间
     */
    @TableField("UpdateTime")
    private Long UpdateTime;

    /**
     * 删除者ID
     */
    @TableField("DeleteOperatorID")
    private String DeleteOperatorID;

    /**
     * 删除者名称
     */
    @TableField("DeleteOperatorName")
    private String DeleteOperatorName;

    /**
     * 删除时间
     */
    @TableField("DeleteTime")
    private Long DeleteTime;

    /**
     * 额外参数1
     */
    @TableField("ExtraParams1")
    private String ExtraParams1;

    /**
     * 额外参数2
     */
    @TableField("ExtraParams2")
    private String ExtraParams2;

    /**
     * 额外参数3
     */
    @TableField("ExtraParams3")
    private String ExtraParams3;
    /**
     * 角色编码
     */
    @TableField("Code")
    private String Code;

}
