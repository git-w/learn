package com.elite.learn.systemManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.elite.learn.systemManage.dto.MenuDTO;
import com.elite.learn.systemManage.entity.Account;
import com.elite.learn.systemManage.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统管理-菜单表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
public interface MenuMapper extends BaseMapper<Menu> {


    /**
     * 自定义查询左侧菜单
     *
     * @param queryWrapper
     * @return
     */
    List<MenuDTO> selectLists(@Param(Constants.WRAPPER) Wrapper<Account> queryWrapper);
}
