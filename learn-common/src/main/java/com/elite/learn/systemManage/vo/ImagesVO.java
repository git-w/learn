package com.elite.learn.systemManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 素材库--图片
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Data
public class ImagesVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 分类Id
     */
    private String CategoryID;

    /**
     * 图片名称
     */
    private String ImgName;

    /**
     * 图片地址Url
     */
    private String ImgUrl;

    /**
     * 文件内容类型
     */
    private String ImgType;

    /**
     * 图片高 px
     */
    private Integer ImgHeight;

    /**
     * 图片宽 px
     */
    private Integer ImgWidth;

    /**
     * 图片文件大小 字节
     */
    private Integer ImgSize;

    /**
     * 备注
     */
    private String Memo;


    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建时间
     */
    private Long CreateTime;

    /**
     * 更新者ID
     */
    private String UpdateOperatorID;

    /**
     * 更新时间
     */
    private Long UpdateTime;


    /**
     * 额外参数1
     */
    private String ExtraParams1;
    /**
     * 图片长路径
     */
    private String ImgUrlPath;

    /**
     * 分类名称
     */
    private String CategoryName;

}
