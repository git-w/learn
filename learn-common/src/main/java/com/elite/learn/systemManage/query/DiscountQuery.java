package com.elite.learn.systemManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * @Title DiscountQuery
 * @Package com.elite.learn.systemManage.query
 * @Description
 * @Author Axe
 * @Date 2021-10-11 2:51 下午
 */
@Data
public class DiscountQuery extends PageParams implements Serializable {


    /**
     * 折扣名称
     */
    private String discountName;
}
