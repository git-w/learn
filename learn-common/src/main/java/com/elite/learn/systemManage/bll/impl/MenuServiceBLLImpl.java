package com.elite.learn.systemManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.tree.NodeTree;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IMenuServiceBLL;
import com.elite.learn.systemManage.contants.SystemManageExceptionCodeEnum;
import com.elite.learn.systemManage.entity.Menu;
import com.elite.learn.systemManage.entity.MidRoleAndMenu;
import com.elite.learn.systemManage.params.MenuParams;
import com.elite.learn.systemManage.service.IMenuService;
import com.elite.learn.systemManage.service.IMidRoleAndMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class MenuServiceBLLImpl implements IMenuServiceBLL {


    Map<String, NodeTree> mapNodeTree = new HashMap<String, NodeTree>();
    @Resource
    private IMenuService service;
    @Resource
    private IMidRoleAndMenuService midRoleAndMenuService; //菜单和角色中间表


    /**
     * 查看菜单详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public Menu get(String id) {
        if (StringUtil.isNotEmpty(id)) {
            return this.service.getById(id);
        }
        return null;
    }


    /**
     * 添加菜单
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public String save(String operUserId, Menu bean) {
        String id = IdUtils.simpleUUID();
        Menu info = new Menu();
        info.setId(id);

        //菜单名称
        if (!StringUtil.isNotEmpty(bean.getMenuName())) {
            throw new CommonException(SystemManageExceptionCodeEnum.MenuNameParamsError.code, SystemManageExceptionCodeEnum.MenuNameParamsError.msg);
        }

        //菜单标识
        if (!StringUtil.isNotEmpty(bean.getMenuCode())) {
            throw new CommonException(SystemManageExceptionCodeEnum.MenuCodeParamsError.code, SystemManageExceptionCodeEnum.MenuCodeParamsError.msg);
        }

        //菜单层级 1:一级菜单 2:二级菜单 依次类推...
        if (null == bean.getMenuLevel()) {
            throw new CommonException(SystemManageExceptionCodeEnum.MenuLevelParamsError.code, SystemManageExceptionCodeEnum.MenuLevelParamsError.msg);
        }
        //类型 0-菜单 1-按钮
        if (null == bean.getMenuType()) {
            throw new CommonException(SystemManageExceptionCodeEnum.MenuTypeParamsError.code, SystemManageExceptionCodeEnum.MenuTypeParamsError.msg);
        }

        //排序号
        if (null != bean.getSort()) {
            info.setSort(bean.getSort());
        } else {
            info.setSort(0);
        }
        //是否启用 0是 1否
        if (Objects.nonNull(bean.getIsState())) {
            info.setIsState(bean.getIsState());
        } else {
            info.setIsState(0);
        }
        info.setPagePath(bean.getPagePath());
        info.setMenuName(bean.getMenuName()); //菜单名称
        info.setIcon(bean.getIcon());//菜单或按钮icon
        info.setMenuCode(bean.getMenuCode());//菜单标识
        info.setMenuLevel(bean.getMenuLevel());//菜单层级 1:一级菜单 2:二级菜单 依次类推...
        info.setParentCode(bean.getParentCode());//父菜单标识
        info.setParentID(bean.getParentID());//父菜单ID
        info.setMenuType(bean.getMenuType());//类型 0-菜单 1-按钮
        info.setButtonType(bean.getButtonType());//按钮区分 1：增改 2：查询 3：删除
        info.setUrl(bean.getUrl());//链接地址
        info.setExtraParams1(bean.getExtraParams1());
        info.setExtraParams2(bean.getExtraParams2());
        info.setExtraParams3(bean.getExtraParams3());
        info.setMemo(bean.getMemo());//备注
        info.setCreateTime(System.currentTimeMillis());
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateOperatorID(operUserId);
        boolean flag = this.service.save(info);
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code, BasePojectExceptionCodeEnum.SaveFail.msg);
        }
        return info.getId();
    }


    /**
     * 修改菜单
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean update(String operUserId, Menu bean) {
        // 查询菜单是否存在
        Menu info = this.get(bean.getId());
        // 如果菜单不存在的话直接抛出异常
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        // new一个新的对象 接受修改的值
        Menu infoNew = new Menu();
        infoNew.setId(bean.getId());
        // 判断菜单名称是否存在 如果存在则修改
        if (StringUtil.isNotEmpty(bean.getMenuName())) {
            infoNew.setMenuName(bean.getMenuName());
        }
        // 判断icon是否存在 如果存在的话则修改
        if (StringUtil.isNotEmpty(bean.getIcon())) {
            infoNew.setIcon(bean.getIcon());
        }
        // 页面路由改
        if (StringUtil.isNotEmpty(bean.getPagePath())) {
            infoNew.setPagePath(bean.getPagePath());
        }

        // 判断菜单标识是否存在 如果存在的话 则修改
        if (StringUtil.isNotEmpty(bean.getMenuCode())) {
            infoNew.setMenuCode(bean.getMenuCode());
        }

        //菜单层级 修改
        if (null != bean.getMenuLevel()) {
            infoNew.setMenuLevel(bean.getMenuLevel());
        }

        // 父菜单标识修改
        if (StringUtil.isNotEmpty(bean.getParentCode())) {
            infoNew.setParentCode(bean.getParentCode());
        }

        // 是否启用 0是 1否
        if (null != bean.getIsState()) {
            infoNew.setIsState(bean.getIsState());
        }

        // 父菜单ID 修改
        if (StringUtil.isNotEmpty(bean.getParentID())) {
            infoNew.setParentID(bean.getParentID());
        }
        if (null != bean.getSort()) {
            infoNew.setSort(bean.getSort());
        }

        //类型 0-菜单 1-按钮
        if (null != bean.getMenuType()) {
            infoNew.setMenuType(bean.getMenuType());
        }

        //按钮区分 1：增改 2：查询 3：删除 修改
        if (null != bean.getButtonType()) {
            infoNew.setButtonType(bean.getButtonType());
        }
        // 链接地址 修改
        if (StringUtil.isNotEmpty(bean.getUrl())) {
            infoNew.setUrl(bean.getUrl());
        }


        // 链接地址 修改
        if (StringUtil.isNotEmpty(bean.getMemo())) {
            infoNew.setMemo(bean.getMemo());
        }

        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());
        infoNew.setUpdateOperatorID(operUserId);

        return this.service.updateById(infoNew);
    }


    /**
     * 删除 菜单
     *
     * @param operUserId 操作人员标识
     * @param ids        对象标识
     * @return
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        String[] idsArr = ids.split(",");
        for (String id : idsArr) {
            Menu info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            Menu infoNew = new Menu();
            // 删除状态
            infoNew.setIsDelete(DeleteType.ISDELETE.getCode());
            infoNew.setDeleteTime(System.currentTimeMillis());
            infoNew.setDeleteOperatorID(operUserId);
            infoNew.setId(info.getId());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     * 前端用户请求的权限
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public List<NodeTree> getMenuList(String operUserId, MenuParams params) {
        this.mapNodeTree.clear();
        Map<String, NodeTree> map = this.mapNodeTree;
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("CreateTime");
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        List<Menu> list = this.service.list(queryWrapper);
        List<NodeTree> NodeTreeList = toNodeTreeList(list);
        List<NodeTree> dtoList = new ArrayList<NodeTree>();
        Map<String, NodeTree> map2 = new HashMap<String, NodeTree>();
        if (null == NodeTreeList || NodeTreeList.size() == 0) {
            return dtoList;
        }
        // 遍历NodeTreeList 依次放入map
        for (NodeTree bean : NodeTreeList) {
            map2.put(bean.getId(), bean);
        }
        // 遍历NodeTreeList与map对比
        for (NodeTree bean : NodeTreeList) {
            if (!map.containsKey(bean.getId())) {
                continue;
            }
            if (map2.containsKey(bean.getParentID())) {
                // 获取父节点
                NodeTree parent = map2.get(bean.getParentID());
                List<NodeTree> children = parent.getChildren();
                if (Objects.isNull(children)) {
                    children = new ArrayList<>();
                }
                children.add(bean);
                parent.setIsParent(true);
                parent.setChildren(children);
            } else {
                dtoList.add(bean);
            }
        }
        return dtoList;
    }


    /**
     * 多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean updateMultiState(String operUserId, BaseParams params) {
        Menu infoNew = null;
        String[] idsArr = params.getIds().split(",");
        boolean flag = true;
        for (String id : idsArr) {
            Menu info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }

            infoNew = new Menu();
            infoNew.setId(info.getId());
            //状态   0:启用   1:停用
            if (Objects.nonNull(params.getIsState())) {
                infoNew.setIsState(params.getIsState());
            } else {
                throw new CommonException(SystemManageExceptionCodeEnum.IsStateParamsError.code, SystemManageExceptionCodeEnum.IsStateParamsError.msg);
            }
            infoNew.setUpdateTime(System.currentTimeMillis());
            infoNew.setUpdateOperatorID(operUserId);
            infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        if (!flag) {
            //修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     * 普通用户请求的权限  根据管理员权限获取全量菜单
     *
     * @param params
     * @return
     */
    @Override
    public List<NodeTree> getAccountTree(MenuParams params) {
        List<NodeTree> treeList = new ArrayList<NodeTree>();
        if (StringUtil.isNotEmpty(params.getRoleId())) {
            //查询用户的权限
            QueryWrapper<MidRoleAndMenu> wrapper = new QueryWrapper<MidRoleAndMenu>();
            wrapper.eq("RoleID", params.getRoleId());
            List<MidRoleAndMenu> listMenuId = this.midRoleAndMenuService.list(wrapper);
            if (null != listMenuId && listMenuId.size() > 0) {
                List<String> listId = new ArrayList<>();
                for (MidRoleAndMenu roleAndMenu : listMenuId) {
                    listId.add(roleAndMenu.getMenuID());
                }

                QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
                queryWrapper.orderByAsc("Sort");
                queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
                queryWrapper.in("ID", listId);
                List<Menu> list = this.service.list(queryWrapper);
                if (Objects.nonNull(list)) {
                    List<NodeTree> NodeTreeList = this.toNodeTreeList(list);
                    treeList = this.toMenuTree(NodeTreeList);
                }
            }

        }
        return treeList;
    }


    @Override
    public boolean saveAllMenu() {
        return false;
    }


    /**
     * 获取全量菜单
     *
     * @param params
     * @return
     */
    @Override
    public List<NodeTree> getTree(MenuParams params) {
        List<NodeTree> treeList = new ArrayList<NodeTree>();
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("Sort");
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        List<Menu> list = this.service.list(queryWrapper);
        if (Objects.nonNull(list)) {
            // 转换集合
            List<NodeTree> nodeTreeList = this.toNodeTreeList(list);
            // 转换成树结构
            treeList = this.toMenuTree(nodeTreeList);
        }
        return treeList;
    }

    @Override
    public void addCache(Menu bean) {


    }

    @Override
    public void updateCache(Menu oldBean, Menu newBean) {


    }

    @Override
    public void removeCache(Menu bean) {


    }

    @Override
    public void initCache() {


    }

    @Override
    public List<Menu> findAll(Menu bean) {
        return null;
    }


    /**
     * 转成 treeList
     *
     * @param
     * @return
     */
    private List<NodeTree> toNodeTreeList(List<Menu> list) {
        List<NodeTree> treeList = new ArrayList<>();
        // 判断是否为空
        if (Objects.nonNull(list) && list.size() > 0) {
            // 遍历列表 转成NodeTree对象
            for (Menu menu : list) {
                NodeTree info = toInfoForFullTree(menu);
                treeList.add(info);
            }
        }
        return treeList;
    }


    /**
     * 菜单集合对象转换Map
     *
     * @param menu
     * @return
     */
    private NodeTree toInfoForFullTree(Menu menu) {
        NodeTree info = new NodeTree();
        info.setId(menu.getId());
        info.setMenuName(menu.getMenuName());
        info.setParentID(menu.getParentID());
        info.setMenuLevel(menu.getMenuLevel());

        Map<String, String> attr = new HashMap<String, String>();
        if (null != menu.getIsState()) {
            attr.put("IsState", menu.getIsState().toString());
        } else {
            attr.put("IsState", "1");
        }
        attr.put("MenuName", menu.getMenuName());
        attr.put("Icon", menu.getIcon());
        if (StringUtil.isNotEmpty(menu.getIcon())) {
            attr.put("IconPath", FileUploadUtil.getImgRootPath() + menu.getIcon());
            info.setIconPath(FileUploadUtil.getImgRootPath() + menu.getIcon());
        } else {
            attr.put("IconPath", "");
        }
        attr.put("MenuCode", menu.getMenuCode());
        attr.put("MenuLevel", menu.getMenuLevel().toString());
        attr.put("ParentCode", menu.getParentCode());
        attr.put("ParentID", menu.getParentID());
        attr.put("MenuType", menu.getMenuType().toString());
        if (null != menu.getButtonType()) {
            attr.put("ButtonType", menu.getButtonType().toString());
        } else {
            attr.put("ButtonType", null);
        }
        attr.put("Url", menu.getUrl());
        attr.put("PagePath", menu.getPagePath());

        attr.put("extraParams1", menu.getExtraParams1());
        attr.put("memo", menu.getMemo());
        info.setAttributes(attr);
        return info;
    }


    /**
     * 判断组织数结构
     *
     * @param NodeTreeList
     * @return
     */
    private List<NodeTree> toMenuTree(List<NodeTree> NodeTreeList) {
        List<NodeTree> dtoList = new ArrayList<NodeTree>();
        Map<String, NodeTree> map = new HashMap<String, NodeTree>();
        if (null == NodeTreeList || NodeTreeList.size() == 0) {
            return dtoList;
        }
        // 遍历NodeTreeList 依次放入map
        for (NodeTree NodeTree : NodeTreeList) {
            map.put(NodeTree.getId(), NodeTree);
        }
        // 遍历NodeTreeList与map对比
        for (NodeTree NodeTree : NodeTreeList) {
            if (map.containsKey(NodeTree.getParentID())) {
                //if(NodeTree.getAttributes().get("MenuType"))
                // 获取父节点
                NodeTree parent = map.get(NodeTree.getParentID());
                List<NodeTree> children = parent.getChildren();
                if (Objects.isNull(children)) {
                    children = new ArrayList<>();
                }
                children.add(NodeTree);
                parent.setIsParent(true);
                parent.setChildren(children);

            } else {
                dtoList.add(NodeTree);
            }
        }
        List<NodeTree> dtoListNew = new ArrayList<NodeTree>();
        for (NodeTree NodeTree : dtoList) {
            if (NodeTree.getMenuLevel() == 1) {
                dtoListNew.add(NodeTree);

            }
        }
        return dtoListNew;
    }

}
