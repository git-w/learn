package com.elite.learn.systemManage.bll;


import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.systemManage.dto.ImagesDTO;
import com.elite.learn.systemManage.dto.ImagesUpdateDTO;
import com.elite.learn.systemManage.entity.Images;
import com.elite.learn.systemManage.query.ImagesQuery;
import com.elite.learn.systemManage.vo.ImagesVO;

import java.util.List;

public interface IImagesServiceBLL extends ICommonServiceBLL<Images, ImagesDTO, ImagesVO, ImagesQuery> {


    /**
     * 批量修改素材库图片分类
     *
     * @param params
     * @return
     */
    boolean updateMultiCategory(String operUserId, ImagesUpdateDTO params);


    /**
     * 批量添加素材库图片分类
     *
     * @param beans
     * @return
     */
    boolean saveBatch(String operUserId, List<ImagesDTO> beans);

}
