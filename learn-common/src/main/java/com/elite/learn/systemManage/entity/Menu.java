package com.elite.learn.systemManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统管理-菜单表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "ID", type = IdType.UUID)
    private String id;

    /**
     * 菜单名称
     */
    @TableField("MenuName")
    private String MenuName;

    /**
     * 菜单或按钮icon
     */
    @TableField("Icon")
    private String Icon;

    /**
     * 菜单标识
     */
    @TableField("MenuCode")
    private String MenuCode;

    /**
     * 菜单层级 1:一级菜单 2:二级菜单 依次类推...
     */
    @TableField("MenuLevel")
    private Integer MenuLevel;

    /**
     * 父菜单标识
     */
    @TableField("ParentCode")
    private String ParentCode;

    /**
     * 父菜单ID
     */
    @TableField("ParentID")
    private String ParentID;

    /**
     * 排序号 数据越高排序越大 0是最低
     */
    @TableField("Sort")
    private Integer Sort;

    /**
     * 类型 0-菜单 1-按钮
     */
    @TableField("MenuType")
    private Integer MenuType;

    /**
     * 是否启用 0-是 1-否
     */
    @TableField("IsState")
    private Integer IsState;

    /**
     * 按钮区分 1：增改  2：查询 3：删除
     */
    @TableField("ButtonType")
    private Integer ButtonType;

    /**
     * 链接地址
     */
    @TableField("Url")
    private String Url;

    /**
     * 备注
     */
    @TableField("Memo")
    private String Memo;

    /**
     * 删除标识 0:未删除 1:删除
     */
    @TableField("IsDelete")
    private Integer IsDelete;

    /**
     * 创建者ID
     */
    @TableField("CreateOperatorID")
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    @TableField("CreateOperatorName")
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    @TableField("CreateTime")
    private Long CreateTime;

    /**
     * 更新者ID
     */
    @TableField("UpdateOperatorID")
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    @TableField("UpdateOperatorName")
    private String UpdateOperatorName;

    /**
     * 更新时间
     */
    @TableField("UpdateTime")
    private Long UpdateTime;

    /**
     * 删除者ID
     */
    @TableField("DeleteOperatorID")
    private String DeleteOperatorID;

    /**
     * 删除者名称
     */
    @TableField("DeleteOperatorName")
    private String DeleteOperatorName;

    /**
     * 删除时间
     */
    @TableField("DeleteTime")
    private Long DeleteTime;

    /**
     * 额外参数1
     */
    @TableField("ExtraParams1")
    private String ExtraParams1;

    /**
     * 额外参数2
     */
    @TableField("ExtraParams2")
    private String ExtraParams2;

    /**
     * 额外参数3
     */
    @TableField("ExtraParams3")
    private String ExtraParams3;

    /**
     * 页面路由
     */
    @TableField("PagePath")
    private String PagePath;

}
