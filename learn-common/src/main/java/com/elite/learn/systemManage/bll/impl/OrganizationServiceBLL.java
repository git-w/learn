package com.elite.learn.systemManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IOrganizationServiceBLL;
import com.elite.learn.systemManage.contants.SystemManageExceptionCodeEnum;
import com.elite.learn.systemManage.dto.OrganizationDTO;
import com.elite.learn.systemManage.entity.Organization;
import com.elite.learn.systemManage.query.OrganizationQuery;
import com.elite.learn.systemManage.service.IOrganizationService;
import com.elite.learn.systemManage.tree.OrganizationTree;
import com.elite.learn.systemManage.vo.OrganizationVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Title :IAccountServiceBLL.java
 * @Package: com.elite.learn.systemManage.bll.impl
 * @Description:
 * @author: leroy
 * @data: 2020/5/12 10:50
 */
@Service
public class OrganizationServiceBLL implements IOrganizationServiceBLL {


    @Resource
    private IOrganizationService service;


    /**
     * 查看详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public Organization get(String id) {
        if (StringUtil.isNotEmpty(id)) {
            return this.service.getById(id);
        }
        return null;
    }


    @Override
    public OrganizationVO getInfo(String id) {
        return null;
    }


    /**
     * 添加公司部门
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public String save(String operUserId, OrganizationDTO bean) {
        String id = IdUtils.simpleUUID();
        Organization info = new Organization();
        info.setId(id);
        //总公司直属部门
        if (bean.getDeptType() != 0 && bean.getDeptLevel() != 0) {
            //父标识
            if (!StringUtil.isNotEmpty(bean.getParentID())) {
                throw new CommonException(SystemManageExceptionCodeEnum.ParentIDParamsError.code, SystemManageExceptionCodeEnum.ParentIDParamsError.msg);
            }
        }
        info.setOrganizationName(bean.getOrganizationName());
        info.setDeptLevel(bean.getDeptLevel());
        info.setDeptType(bean.getDeptType());
        info.setParentID(bean.getParentID());
        info.setMemo(bean.getMemo());
        info.setCreateTime(System.currentTimeMillis());
        info.setCreateOperatorID(operUserId);
        boolean flag = this.service.save(info);
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code, BasePojectExceptionCodeEnum.SaveFail.msg);
        }
        return info.getId();
    }


    /**
     * 修改公司部门
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean update(String operUserId, OrganizationDTO bean) {
        Organization info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        Organization infoNew = new Organization();
        infoNew.setId(info.getId());

        //组织名称
        if (StringUtil.isNotEmpty(bean.getOrganizationName())) {
            infoNew.setOrganizationName(bean.getOrganizationName());
        }

        if (StringUtil.notBlank(bean.getMemo())) {
            infoNew.setMemo(bean.getMemo());
        }
        infoNew.setUpdateTime(System.currentTimeMillis());
        infoNew.setUpdateOperatorID(operUserId);
        return this.service.updateById(infoNew);

    }


    /**
     * 删除部门
     *
     * @param operUserId 操作人员标识
     * @param ids        对象标识
     * @return
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        String[] idsArr = ids.split(",");
        for (String id : idsArr) {
            Organization info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            // 删除状态
            info.setIsDelete(DeleteType.ISDELETE.getCode());
            info.setDeleteTime(System.currentTimeMillis());
            info.setDeleteOperatorID(operUserId);
            boolean result = this.service.updateById(info);
            if (result) {
            } else {
                flag = false;
            }
        }
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     * 查看全部
     *
     * @param bean
     * @return
     */
    public List<OrganizationVO> findAll(OrganizationQuery bean) {
        QueryWrapper<Organization> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("CreateTime");
        queryWrapper.eq("isDelete", DeleteType.NOTDELETE.getCode());
        List<Organization> list = this.service.list(queryWrapper);
        List<OrganizationVO> autlist = new ArrayList<>();
        OrganizationVO vo = null;
        for (Organization info : list) {
            vo = new OrganizationVO();
            vo.setId(info.getId());
            vo.setOrganizationName(info.getOrganizationName());
            autlist.add(vo);
        }
        return autlist;
    }

    @Override
    public PageResult<OrganizationVO> getList(OrganizationQuery params) {
        return null;
    }


    /**
     * 组织架构树
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2022/1/14 15:38
     * @update
     * @updateTime
     */
    @Override
    public List<OrganizationTree> getTree(OrganizationQuery params) {
        List<OrganizationTree> treeList = new ArrayList<OrganizationTree>();
        List<OrganizationTree> treeListTwo = new ArrayList<OrganizationTree>();
        List<OrganizationTree> listTree = new ArrayList<OrganizationTree>();
        QueryWrapper<Organization> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("DeptType,CreateTime");
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        List<Organization> list = this.service.list(queryWrapper);
        if (list != null) {
            List<OrganizationTree> NodeTreeList = toNodeTreeList(list);
            treeList = toTree(NodeTreeList);
            if (treeList != null && treeList.size() > 0) {
                List<OrganizationTree> Listtwo = treeList.get(0).getChildren();
                for (OrganizationTree tree : treeList) {
                    if (tree.getDeptType() == 0) {
                        treeListTwo.add(tree);
                    }
                }
                if (Listtwo != null && Listtwo.size() > 0) {
                    for (OrganizationTree treeTwo : Listtwo) {
                        if (treeTwo.getDeptType() == 1 || treeTwo.getDeptType() == 2) {
                            listTree.add(treeTwo);
                        }
                    }
                    treeListTwo.get(0).setChildren(listTree);
                }
            }
        }
        return treeListTwo;
    }


    /**
     * 转成 treeList
     *
     * @param
     * @return
     */
    private List<OrganizationTree> toNodeTreeList(List<Organization> list) {

        List<OrganizationTree> treeList = new ArrayList<OrganizationTree>();

        if (null == list || list.size() == 0) {
            return treeList;
        }

        // 遍历列表 转成NodeTree对象
        for (Organization Organization : list) {
            if (null == Organization) {
                continue;
            }
            OrganizationTree tree = toNodeTreeForFullTree(Organization);
            treeList.add(tree);
        }
        return treeList;
    }

    private OrganizationTree toNodeTreeForFullTree(Organization info) {
        OrganizationTree tree = new OrganizationTree();
        tree.setId(info.getId());
        tree.setOrganizationName(info.getOrganizationName());
        tree.setDeptLevel(info.getDeptLevel());
        tree.setDeptType(info.getDeptType());
        tree.setParentID(info.getParentID());
        tree.setMemo(info.getMemo());
        tree.setCreateOperatorID(info.getCreateOperatorID());
        tree.setCreateTime(info.getCreateTime());
        tree.setUpdateOperatorID(info.getUpdateOperatorID());
        tree.setUpdateTime(info.getUpdateTime());
        return tree;
    }


    private List<OrganizationTree> toTree(List<OrganizationTree> treeList) {
        List<OrganizationTree> dtoList = new ArrayList<OrganizationTree>();
        Map<String, OrganizationTree> map = new HashMap<String, OrganizationTree>();
        if (null == treeList || treeList.size() == 0) {
            return dtoList;
        }
        // 遍历NodeTreeList 依次放入map
        for (OrganizationTree tree : treeList) {
            map.put(tree.getId(), tree);
        }
        // 遍历NodeTreeList与map对比
        for (OrganizationTree tree : treeList) {
            if (map.containsKey(tree.getParentID())) {
                // 获取父节点
                OrganizationTree Organization = map.get(tree.getParentID());
                List<OrganizationTree> children = Organization.getChildren();
                if (null == children) {
                    children = new ArrayList<OrganizationTree>();
                }
                children.add(tree);
                //  Organization.setIsParent(true);
                Organization.setChildren(children);

            } else {
                dtoList.add(tree);
            }
        }
        return dtoList;
    }


    /**
     * 查询所有的部门的
     *
     * @param params
     * @return
     */
    @Override
    public List<OrganizationVO> findAllDepartment(OrganizationQuery params) {
        QueryWrapper<Organization> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("CreateTime");
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());

        if (StringUtil.isNotEmpty(params.getId())) {
            queryWrapper.eq("ID", params.getId());
        }

        if (StringUtil.isNotEmpty(params.getParentID())) {
            queryWrapper.eq("ParentID", params.getParentID());
        } else {
            queryWrapper.eq("DeptType", 0);
        }

        if (null != params.getDeptType()) {
            queryWrapper.eq("DeptType", params.getDeptType());
        }
        List<Organization> list = this.service.list(queryWrapper);
        List<OrganizationVO> autlist = new ArrayList<>();
        OrganizationVO vo = null;
        for (Organization aut : list) {
            vo = new OrganizationVO();
            vo.setId(aut.getId());
            vo.setOrganizationName(aut.getOrganizationName());
            vo.setDeptLevel(aut.getDeptLevel());
            vo.setDeptType(aut.getDeptType());
            vo.setParentID(aut.getParentID());
            vo.setMemo(aut.getMemo());
            vo.setCreateOperatorID(aut.getCreateOperatorID());
            vo.setCreateOperatorName(aut.getCreateOperatorName());
            vo.setCreateTime(aut.getCreateTime());
            autlist.add(vo);
        }
        return autlist;
    }

}
