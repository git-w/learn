package com.elite.learn.systemManage.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统管理-管理员表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
public class AccountDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 用户登录名
     */
    private String LoginName;

    /**
     * 用户真实姓名
     */
    private String AccountName;

    /**
     * 用户密码
     */
    private String LoginPassword;

    /**
     * 账号状态 0:正常/启用 1:冻结/禁用
     */
    private Integer IsStatus;

    /**
     * 头像路径
     */
    private String Photo;

    /**
     * 员工编号
     */
    private String AccountCode;

    /**
     * 角色ID
     */
    private String RoleID;

    /**
     * 用户单独设置权限
     */
    private String AccountAuthority;

    /**
     * 邮箱
     */
    private String Email;

    /**
     * 手机
     */
    private String Phone;

    /**
     * 性别 0:未知 1:男 2:女
     */
    private Integer Sex;

    /**
     * 部门id
     */
    private String DepartmentID;

    /**
     * 住址
     */
    private String Address;

    /**
     * 备注
     */
    private String Memo;


    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    private Long CreateTime;

    /**
     * 更新者ID
     */
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    private String UpdateOperatorName;

    /**
     * 更新时间
     */
    private Long UpdateTime;


    /**
     * 管理员类型 0:超级管理员 1普通管理员
     */
    private Integer Type;

    /**
     * 额外参数1
     */
    private String ExtraParams1;

    /**
     * 额外参数2
     */
    private String ExtraParams2;

    /**
     * 额外参数3
     */
    private String ExtraParams3;

    /**
     * 头像路径-长路径
     */
    private String PhotoPath;

    /**
     * 部门名称
     */
    private String DepartmentName;


}
