package com.elite.learn.systemManage.service;

import com.elite.learn.systemManage.entity.Organization;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统管理-组织架构表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
public interface IOrganizationService extends IService<Organization> {

}
