package com.elite.learn.systemManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.elite.learn.systemManage.dto.MenuDTO;
import com.elite.learn.systemManage.entity.Account;
import com.elite.learn.systemManage.entity.Menu;
import com.elite.learn.systemManage.mapper.MenuMapper;
import com.elite.learn.systemManage.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统管理-菜单表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {


    /**
     * 查看左侧菜单
     *
     * @param queryWrapper
     * @return
     */
    @Override
    public List<MenuDTO> selectLists(@Param(Constants.WRAPPER) Wrapper<Account> queryWrapper) {
        return this.baseMapper.selectLists(queryWrapper);
    }
}
