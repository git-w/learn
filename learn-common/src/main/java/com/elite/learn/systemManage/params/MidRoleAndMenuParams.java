package com.elite.learn.systemManage.params;


import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统管理-角色和菜单中间表
 * </p>
 *
 * @author: leroy
 * @since 2020-11-24
 */
@Data
public class MidRoleAndMenuParams extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 角色ID
     */
    private String RoleID;

    /**
     * 菜单ID
     */
    private String MenuID;

    /**
     * 排除的按钮(多个时用逗号分隔)
     */
    private String ExcludeButton;



}
