package com.elite.learn.systemManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.tree.NodeTree;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IAccountServiceBLL;
import com.elite.learn.systemManage.bll.IRoleServiceBLL;
import com.elite.learn.systemManage.contants.SystemManageExceptionCodeEnum;
import com.elite.learn.systemManage.dto.MenuDTO;
import com.elite.learn.systemManage.dto.RoleDTO;
import com.elite.learn.systemManage.entity.MidRoleAndMenu;
import com.elite.learn.systemManage.entity.Role;
import com.elite.learn.systemManage.params.RoleParams;
import com.elite.learn.systemManage.service.IMidRoleAndMenuService;
import com.elite.learn.systemManage.service.IRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class RoleServiceBLLImpl implements IRoleServiceBLL {

    @Resource
    private IRoleService service;

    @Resource
    private IMidRoleAndMenuService midRoleAndMenuService;//菜单和角色中间表*/

    @Resource
    private IAccountServiceBLL accountServiceBLL;

    /**
     * 查询角色详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public Role get(String id) {
        return this.service.getById(id);
    }


    /**
     * 添加后台管理员
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public String save(String operUserId, Role bean) {
        String id = IdUtils.simpleUUID();
        Role info = new Role();
        info.setId(id);

        //角色名称
        if (!StringUtil.isNotEmpty(bean.getRoleName())) {
            throw new CommonException(SystemManageExceptionCodeEnum.RoleNameParamsError.code, SystemManageExceptionCodeEnum.RoleNameParamsError.msg);
        }

        //部门
        if (!StringUtil.isNotEmpty(bean.getDepartmentID())) {
            throw new CommonException(SystemManageExceptionCodeEnum.DepartmentIDParamsError.code, SystemManageExceptionCodeEnum.DepartmentIDParamsError.msg);
        }

        if (StringUtil.notBlank(bean.getMemo())) {
            info.setMemo(bean.getMemo());
        }
        info.setIsUpdate(0);
        info.setRoleName(bean.getRoleName());
        info.setCreateTime(System.currentTimeMillis());
        info.setCreateOperatorID(operUserId);
        boolean flag = this.service.save(info);
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code, BasePojectExceptionCodeEnum.SaveFail.msg);
        }
        return info.getId();
    }


    /**
     * 添加后台管理员
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public String saveAndMenu(String operUserId, RoleParams bean) {
        String id = IdUtils.simpleUUID();
        Role info = new Role();
        info.setId(id);
        info.setCode(bean.getCode());//角色编号
        info.setMemo(bean.getMemo());
        //所有菜单id
        if (StringUtil.isNotEmpty(bean.getMenuIdList())) {
            String[] idsArr = bean.getMenuIdList().split(",");
            boolean flag = true;
            MidRoleAndMenu midInfo = null;
            List<MidRoleAndMenu> midInfoList = new ArrayList<>();
            for (String menuId : idsArr) {
                midInfo = new MidRoleAndMenu();
                // 添加中间表
                midInfo.setId(IdUtils.simpleUUID());
                midInfo.setMenuID(menuId);
                midInfo.setRoleID(id);
                midInfoList.add(midInfo);
            }
            if (midInfoList.size() > 0) {
                boolean result = this.midRoleAndMenuService.saveBatch(midInfoList);
                if (!result) {
                    flag = false;
                }
            }
        }

        info.setIsUpdate(0);
        //账号状态 0:正常/启用 1:不启用
        if (null != bean.getIsStatus()) {
            info.setIsStatus(bean.getIsStatus());
        } else {
            // 默认为正常启用
            info.setIsStatus(0);
        }
        info.setRoleName(bean.getRoleName());
        info.setIsSuperTube(1);
        info.setDepartmentID(bean.getDepartmentID());
        info.setCreateTime(System.currentTimeMillis());
        // info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateOperatorID(operUserId);
        boolean flag = this.service.save(info);
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code, BasePojectExceptionCodeEnum.SaveFail.msg);
        }
        return info.getId();
    }


    /**
     * 修改
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public Boolean updateAndMenu(String operUserId, RoleParams bean) {
        boolean flag = true;
        if (StringUtil.isEmpty(bean.getId())) {
            throw new CommonException(SystemManageExceptionCodeEnum.IdParamsError.code, SystemManageExceptionCodeEnum.IdParamsError.msg);
        }
        Role info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //查看角色是否可以编辑删除 是否可以编辑 0是 1否
        /*if (null == info.getIsUpdate() && info.getIsUpdate() == 1) {
            //     throw new CommonException(BasePojectExceptionCodeEnum.IsUpdateFail.code, BasePojectExceptionCodeEnum.IsUpdateFail.msg);
        }*/
        Role infoNew = new Role();
        infoNew.setId(info.getId());
        //角色名称
        if (StringUtil.notBlank(bean.getCode())) {
            infoNew.setCode(bean.getCode());
        }
        //角色名称
        if (StringUtil.notBlank(bean.getRoleName())) {
            infoNew.setRoleName(bean.getRoleName());
        }
        //角色名称
        if (StringUtil.notBlank(bean.getDepartmentID())) {
            infoNew.setDepartmentID(bean.getDepartmentID());
        }
        //账号状态 0:正常/启用 1:不启用
        if (null != bean.getIsStatus()) {
            infoNew.setIsStatus(bean.getIsStatus());
        }
        if (StringUtil.notBlank(bean.getMemo())) {
            infoNew.setMemo(bean.getMemo());
        }
        infoNew.setUpdateTime(System.currentTimeMillis());
        infoNew.setUpdateOperatorID(operUserId);
        //删除所有的权限
        QueryWrapper<MidRoleAndMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("RoleID", infoNew.getId());
        this.midRoleAndMenuService.remove(queryWrapper);
        //插入所有新的菜单权限
        if (StringUtil.isNotEmpty(bean.getMenuIdList())) {
            String[] idsArr = bean.getMenuIdList().split(",");
            MidRoleAndMenu midInfo = null;
            List<MidRoleAndMenu> midInfoList = new ArrayList<>();
            for (String menuId : idsArr) {
                midInfo = new MidRoleAndMenu();
                // 添加中间表
                midInfo.setMenuID(menuId);
                midInfo.setRoleID(infoNew.getId());
                midInfoList.add(midInfo);
            }
            boolean result = this.midRoleAndMenuService.saveBatch(midInfoList);
            if (!result) {
                flag = false;
            }
        }

        boolean result = this.service.updateById(infoNew);
        // 查询是否修改成功
        if (!result) {
            flag = false;
        }
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //更新角色权限
        this.accountServiceBLL.updateRoleMenuList(operUserId);
        return flag;
    }


    /**
     * 查看角色详情
     *
     * @param Id
     * @return
     */
    @Override
    public RoleDTO getInfo(String Id) {
        /*  Role info = this.get(Id);*/
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("sr.CreateTime");
        queryWrapper.eq("sr.IsDelete", DeleteType.NOTDELETE.getCode());
        queryWrapper.like("sr.ID", Id);
        RoleDTO info = this.service.selectInfo(queryWrapper);
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        // 查看角色是否启用
        if (null == info.getIsStatus() && info.getIsStatus() == 0) {
            throw new CommonException(BasePojectExceptionCodeEnum.RoleNotEnableFail.code, BasePojectExceptionCodeEnum.RoleNotEnableFail.msg);
        }
        RoleDTO dto = new RoleDTO();
        dto.setId(info.getId());
        dto.setRoleName(info.getRoleName());
        dto.setIsStatus(info.getIsStatus());
        dto.setIsSuperTube(info.getIsSuperTube());
        dto.setCode(info.getCode());
        dto.setIsUpdate(info.getIsUpdate());
        dto.setMemo(info.getMemo());
        dto.setCreateOperatorID(info.getCreateOperatorID());
        dto.setCreateOperatorName(info.getCreateOperatorName());
        dto.setCreateTime(info.getCreateTime());
        //删除所有的权限
        QueryWrapper<MidRoleAndMenu> queryWrapperMid = new QueryWrapper<>();
        queryWrapperMid.eq("ram.RoleID", Id);
        List<MenuDTO> list = this.midRoleAndMenuService.selectLists(queryWrapperMid);
        // Menu menu = new Menu();
        //  List<Menu> listAll = iMenuServiceBLL.findAll(menu);//所有的
        List<NodeTree> treeList = new ArrayList<NodeTree>();
        if (list != null && list.size() > 0) {
            List<NodeTree> infoList = toNodeTreeList(list);
            treeList = toMenuTree(infoList);
        }
        dto.setTreeList(treeList);
        return dto;
    }


    /**
     * 转成 treeList
     *
     * @param
     * @return
     */
    private List<NodeTree> toNodeTreeList(List<MenuDTO> list) {
        List<NodeTree> treeList = new ArrayList<NodeTree>();

        if (null == list || list.size() == 0) {
            return treeList;
        }
        // 遍历列表 转成NodeTree对象
        for (MenuDTO menu : list) {
            if (null == menu) {
                continue;
            }
            NodeTree NodeTree = toNodeTreeForFullTree(menu, list);
            treeList.add(NodeTree);
        }
        return treeList;
    }


    private List<NodeTree> toMenuTree(List<NodeTree> NodeTreeList) {
        List<NodeTree> dtoList = new ArrayList<NodeTree>();
        Map<String, NodeTree> map = new HashMap<String, NodeTree>();
        if (null == NodeTreeList || NodeTreeList.size() == 0) {
            return dtoList;
        }
        // 遍历NodeTreeList 依次放入map
        for (NodeTree NodeTree : NodeTreeList) {
            map.put(NodeTree.getId(), NodeTree);
        }
        // 遍历NodeTreeList与map对比
        for (NodeTree NodeTree : NodeTreeList) {
            if (map.containsKey(NodeTree.getParentID())) {
                //if(NodeTree.getAttributes().get("MenuType"))
                // 获取父节点
                NodeTree parent = map.get(NodeTree.getParentID());
                List<NodeTree> children = parent.getChildren();
                if (null == children) {
                    children = new ArrayList<NodeTree>();
                }
                children.add(NodeTree);
                parent.setIsParent(true);
                parent.setChildren(children);

            } else {
                dtoList.add(NodeTree);
            }
        }
        return dtoList;
    }


    private NodeTree toNodeTreeForFullTree(MenuDTO menu, List<MenuDTO> list) {
        NodeTree info = new NodeTree();
        info.setId(menu.getId());
        info.setMenuName(menu.getMenuName());
        info.setParentID(menu.getParentID());


        Map<String, String> attr = new HashMap<String, String>();
        // 遍历列表 转成NodeTree对象
        for (MenuDTO dto : list) {
            if (dto.getId().equals(menu.getId())) {
                //NodeTree.setChecked(true);
                attr.put("Checked", "true");
                break;
            }
        }
        attr.put("MenuName", menu.getMenuName());
        attr.put("Icon", menu.getIcon());
        if (StringUtil.isNotEmpty(menu.getIcon())) {
            info.setIconPath(FileUploadUtil.getImgRootPath() + menu.getIcon());
            attr.put("IconPath", FileUploadUtil.getImgRootPath() + menu.getIcon());
        } else {
            attr.put("IconPath", "");
        }

        attr.put("MenuCode", menu.getMenuCode());
        attr.put("MenuLevel", menu.getMenuLevel().toString());
        attr.put("ParentCode", menu.getParentCode());
        attr.put("ParentID", menu.getParentID());
        if (null != menu.getSort()) {
            attr.put("Sort", menu.getSort().toString());
        } else {
            attr.put("Sort", null);
        }

        attr.put("MenuType", menu.getMenuType().toString());
        if (null != menu.getButtonType()) {
            attr.put("ButtonType", menu.getButtonType().toString());
        } else {
            attr.put("ButtonType", null);
        }
        attr.put("Url", menu.getUrl());
        attr.put("extraParams1", menu.getExtraParams1());
        attr.put("extraParams2", menu.getExtraParams2());
        attr.put("extraParams3", menu.getExtraParams3());
        attr.put("memo", menu.getMemo());
        info.setAttributes(attr);
        return info;
    }


    /**
     * 修改
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean update(String operUserId, Role bean) {
        Role info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        Role infoNew = new Role();
        infoNew.setId(info.getId());

        //角色名称
        if (StringUtil.notBlank(bean.getRoleName())) {
            infoNew.setRoleName(bean.getRoleName());
        }

        if (StringUtil.notBlank(bean.getMemo())) {
            infoNew.setMemo(bean.getMemo());
        }
        infoNew.setUpdateTime(System.currentTimeMillis());
        infoNew.setUpdateOperatorID(operUserId);

        return this.service.updateById(infoNew);


    }


    /**
     * 批量删除角色
     *
     * @param operUserId 操作人员标识
     * @param ids        对象标识
     * @return
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        String[] idsArr = ids.split(",");
        for (String id : idsArr) {
            Role info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            Role infoDel = new Role();
            //查看角色是否可以编辑删除 是否可以编辑 0是 1否
            if (null == info.getIsUpdate() && info.getIsUpdate() == 1) {
                //   throw new CommonException(BasePojectExceptionCodeEnum.IsUpdateFail.code, BasePojectExceptionCodeEnum.IsUpdateFail.msg);
                flag = false;
                continue;
            }
            infoDel.setId(info.getId());
            // 删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            infoDel.setDeleteTime(System.currentTimeMillis());
            infoDel.setDeleteOperatorID(operUserId);
            boolean result = this.service.updateById(infoDel);
            // 查看角色是否删除成功
            if (!result) {
                flag = false;
            }
        }

        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }

    @Override
    public void addCache(Role bean) {


    }

    @Override
    public void updateCache(Role oldBean, Role newBean) {


    }

    @Override
    public void removeCache(Role bean) {


    }

    @Override
    public void initCache() {


    }

    @Override
    public List<Role> findAll(Role bean) {
        return null;
    }

    /**
     * 分页查询角色
     *
     * @param params
     * @return
     */
    @Override
    public PageResult<RoleDTO> getList(RoleParams params) {
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("sr.CreateTime");
        queryWrapper.eq("sr.IsDelete", DeleteType.NOTDELETE.getCode());
        if (StringUtil.isNotEmpty(params.getDepartmentID())) {
            queryWrapper.eq("sr.DepartmentID", params.getDepartmentID());
        }

        if (StringUtil.isNotEmpty(params.getRoleName())) {
            queryWrapper.like("sr.RoleName", params.getRoleName());
        }
        Page<RoleDTO> AccountPage = new Page<RoleDTO>(params.getPageNum(), params.getPageSize());
        IPage<RoleDTO> page = this.service.selectLists(AccountPage, queryWrapper);
        // 总记录数
        return new PageResult<>(page);
    }


    /**
     * 查看所有的角色
     *
     * @param params
     * @return
     */
    @Override
    public List<RoleDTO> findAll(RoleParams params) {
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("CreateTime");
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        //创建者id
    /*    if (StringUtil.isNotEmpty(params.getCreateOperatorID())) {
            queryWrapper.eq("CreateOperatorID", params.getCreateOperatorID());
        }
*/
        //创建者id
        if (StringUtil.isNotEmpty(params.getDepartmentID())) {
            queryWrapper.eq("DepartmentID", params.getDepartmentID());
        }
        List<Role> list = this.service.list(queryWrapper);
        List<RoleDTO> rolelist = new ArrayList<>();
        RoleDTO dto = null;
        for (Role info : list) {
            dto = new RoleDTO();
            dto.setId(info.getId());
            dto.setRoleName(info.getRoleName());
            dto.setIsUpdate(info.getIsUpdate());
            dto.setMemo(info.getMemo());
            dto.setIsSuperTube(info.getIsSuperTube());
            dto.setDepartmentID(info.getDeleteOperatorID());
            dto.setCreateOperatorID(info.getCreateOperatorID());
            dto.setCreateOperatorName(info.getCreateOperatorName());
            dto.setCreateTime(info.getCreateTime());
            rolelist.add(dto);
        }
        return rolelist;
    }

}
