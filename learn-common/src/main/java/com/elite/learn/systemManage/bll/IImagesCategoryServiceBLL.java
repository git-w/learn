package com.elite.learn.systemManage.bll;


import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.systemManage.dto.ImagesCategoryDTO;
import com.elite.learn.systemManage.entity.ImagesCategory;
import com.elite.learn.systemManage.query.ImagesCategoryQuery;
import com.elite.learn.systemManage.vo.ImagesCategoryVO;

import java.util.List;

public interface IImagesCategoryServiceBLL extends ICommonServiceBLL<ImagesCategory, ImagesCategoryDTO, ImagesCategoryVO, ImagesCategoryQuery> {


    /**
     * 查询全部
     *
     * @return
     */
    List<ImagesCategoryVO> findAll();

}