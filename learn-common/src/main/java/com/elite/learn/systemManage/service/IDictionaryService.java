package com.elite.learn.systemManage.service;

import com.elite.learn.systemManage.entity.Dictionary;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-26
 */
public interface IDictionaryService extends IService<Dictionary> {


	/**
	 * 查询字典详情
	 * @param parentKey
	 * @return
	 */
	List<Dictionary> getDictionaryInfoByParentCode(String parentKey);

}
