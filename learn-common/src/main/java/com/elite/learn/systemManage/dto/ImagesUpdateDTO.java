package com.elite.learn.systemManage.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 素材库--图片
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Data
public class ImagesUpdateDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @NotBlank(message = "id,参数为空")
    private String ids;

    /**
     * @NotEmpty 用在集合类上面
     * 加了@NotEmpty的String类、Collection、Map、数组，是不能为null或者长度为0的(String Collection Map的isEmpty()方法)
     * @NotBlank只用于String,不能为null且trim()之后size>0
     * @NotNull:不能为null，但可以为empty,没有Size的约束
     * 分类Id
     */
    @NotBlank(message = "分类id,参数为空")
    private String CategoryID;



}
