package com.elite.learn.systemManage.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Title DiscountVo
 * @Package com.elite.learn.systemManage.vo
 * @Description
 * @Author Axe
 * @Date 2021-10-11 2:51 下午
 */
@Data
public class DiscountVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;

    /**
     * 折扣名称
     */
    private String discountName;

    /**
     * 是否区分周末：0-不区分；1-区分
     */
    private Integer isWeekend;

    /**
     * 是否区分节假日：0-不区分；1-区分
     */
    private Integer isHoliday;

    /**
     * 平日折扣
     */
    private BigDecimal singleDiscount;

    /**
     * 周六折扣
     */
    private BigDecimal saturdayDiscount;

    /**
     * 周日折扣
     */
    private BigDecimal sundayDiscount;

    /**
     * 节假日折扣
     */
    private BigDecimal holidayDiscount;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;
}
