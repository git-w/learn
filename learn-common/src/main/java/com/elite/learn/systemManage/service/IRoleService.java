package com.elite.learn.systemManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.systemManage.dto.RoleDTO;
import com.elite.learn.systemManage.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统管理-角色管理表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
public interface IRoleService extends IService<Role> {


    /**
     * 管理员分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<RoleDTO> selectLists(Page<RoleDTO> page, @Param(Constants.WRAPPER) Wrapper<Role> queryWrapper);


    /**
     * 用户详情
     *
     * @param queryWrapper
     * @return
     */
    RoleDTO selectInfo(@Param(Constants.WRAPPER) Wrapper<Role> queryWrapper);
}
