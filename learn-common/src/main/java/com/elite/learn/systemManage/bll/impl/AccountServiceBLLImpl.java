package com.elite.learn.systemManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.cacheKeyUtils.CaptchaKeyUtils;
import com.elite.learn.common.core.exception.CaptchaException;
import com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.type.DeleteType;
import com.elite.learn.common.core.type.StateType;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.tree.NodeTree;
import com.elite.learn.common.utils.encrypt.md5.Md5Util;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.redis.RedisCache;
import com.elite.learn.common.utils.redis.RedisUtil;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.common.utils.string.StringUtils;
import com.elite.learn.logManage.bll.ILogAccountLoginServiceBLL;
import com.elite.learn.systemManage.bll.IAccountServiceBLL;
import com.elite.learn.systemManage.bll.IRoleServiceBLL;
import com.elite.learn.systemManage.contants.SystemManageExceptionCodeEnum;
import com.elite.learn.systemManage.dto.AccountDTO;
import com.elite.learn.systemManage.dto.MenuDTO;
import com.elite.learn.systemManage.entity.Account;
import com.elite.learn.systemManage.entity.Role;
import com.elite.learn.systemManage.params.AccountParams;
import com.elite.learn.systemManage.params.LoginParams;
import com.elite.learn.systemManage.params.ResetPasswordParams;
import com.elite.learn.systemManage.service.IAccountService;
import com.elite.learn.systemManage.service.IMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AccountServiceBLLImpl implements IAccountServiceBLL {

    @Autowired
    private RedisCache redisCache;

    @Resource
    private IAccountService service;
    //系统管理-菜单表 服务类
    @Resource
    private IMenuService menuService;
    //系统管理-角色管理表
    @Resource
    private IRoleServiceBLL roleServiceBLL;
    //缓存工具类
    @Resource
    RedisUtil redisUtil;
    @Resource
    IAccountServiceBLL accountServiceBLL;
    //登录日志记录
    @Resource
    private ILogAccountLoginServiceBLL logAccountLoginServiceBLL;

    /**
     * 查看管理员详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public Account get(String id) {
        if (StringUtil.isNotEmpty(id)) {
            Account info = this.service.getById(id);
            return info;
        }
        return null;
    }


    /**
     * 添加 管理员
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public String save(String operUserId, Account bean) {
        // 获取uuid
        String id = IdUtils.simpleUUID();
        Account info = new Account();
        info.setId(id);

        // 判断 用户登录名
        if (!StringUtil.notBlank(bean.getLoginName())) {
            throw new CommonException(BasePojectExceptionCodeEnum.LoginNameParamsError.code, BasePojectExceptionCodeEnum.LoginNameParamsError.msg);
        }
        // 判断 用户真实姓名
        if (!StringUtil.notBlank(bean.getAccountName())) {
            throw new CommonException(SystemManageExceptionCodeEnum.AccountNameParamsError.code, SystemManageExceptionCodeEnum.AccountNameParamsError.msg);
        }
        // 判断 用户手机号是否为空
        if (!StringUtil.notBlank(bean.getPhone())) {
            throw new CommonException(SystemManageExceptionCodeEnum.PhoneParamsError.code, SystemManageExceptionCodeEnum.PhoneParamsError.msg);
        }

        // 判断 管理员角色id是否为空
        if (!StringUtil.notBlank(bean.getRoleID())) {
            throw new CommonException(SystemManageExceptionCodeEnum.RoleIDParamsError.code, SystemManageExceptionCodeEnum.RoleIDParamsError.msg);
        }

        // 部门id
        if (!StringUtil.notBlank(bean.getDepartmentID())) {
            throw new CommonException(SystemManageExceptionCodeEnum.DepartmentIDParamsError.code, SystemManageExceptionCodeEnum.DepartmentIDParamsError.msg);
        }

        // 密码
        if (!StringUtil.notBlank(bean.getLoginPassword())) {
            throw new CommonException(BasePojectExceptionCodeEnum.LoginPasswordParamsError.code, BasePojectExceptionCodeEnum.LoginPasswordParamsError.msg);
        }

        //查询用户登录名是否已经存在
        boolean isFlagName = this.isLoginName(bean.getLoginName());
        if (!isFlagName) {
            throw new CommonException(SystemManageExceptionCodeEnum.RepeatYhdlm.code, SystemManageExceptionCodeEnum.RepeatYhdlm.msg);
        }

        info.setLoginName(bean.getLoginName()); //用户登录名
        info.setAccountName(bean.getAccountName());//用户真实姓名
        info.setPhoto(bean.getPhoto());//头像路径
        info.setAccountCode(bean.getAccountCode());//员工编号
        info.setRoleID(bean.getRoleID());//角色ID
        info.setAccountAuthority(bean.getAccountAuthority());//用户单独设置权限
        info.setEmail(bean.getEmail());//邮箱
        info.setPhone(bean.getPhone());//电话
        info.setType(1);
        info.setAddress(bean.getAddress());//住址
        info.setDepartmentID(bean.getDepartmentID());
        // 给旧密码加密两次
        Md5Util md5 = Md5Util.getInstance();
        String pwd = md5.getLongToken(bean.getLoginPassword() != null ? bean.getLoginPassword() : "");
        String pwdTwo = md5.getLongToken(pwd);
        info.setLoginPassword(pwdTwo);
        if (null != bean.getSex()) {
            info.setSex(bean.getSex());//性别 0:未知 1:男 2:女
        } else {
            info.setSex(0);//性别 0:未知 1:男 2:女
        }
        // 备注
        if (StringUtil.isNotEmpty(bean.getMemo())) {
            info.setMemo(bean.getMemo().trim());
        }

        //账号状态 0:正常 1:冻结
        if (null != bean.getIsStatus()) {
            info.setIsStatus(bean.getIsStatus());
        } else {
            info.setIsStatus(0);
        }

        info.setIsDelete(DeleteType.NOTDELETE.getCode());//查询没有被删除的
        info.setCreateTime(System.currentTimeMillis());//创建时间
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateOperatorID(operUserId);

        boolean flag = this.service.save(info);
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code, BasePojectExceptionCodeEnum.SaveFail.msg);
        }
        return info.getId();
    }


    /**
     * 修改管理员信息
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean update(String operUserId, Account bean) {
        // 查看修改信息详情
        Account info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        Account infoNew = new Account();
        infoNew.setId(info.getId());
        // 用户真实姓名
        if (!StringUtil.isEmpty(bean.getAccountName())) {
            infoNew.setAccountName(bean.getAccountName());
        }

        // 头像路径
        if (!StringUtil.isEmpty(bean.getPhoto())) {
            infoNew.setPhoto(bean.getPhoto());
        }

        //邮箱
        if (!StringUtil.isEmpty(bean.getEmail())) {
            infoNew.setEmail(bean.getEmail());
        }

        // RoleID 角色ID
        if (!StringUtil.isEmpty(bean.getRoleID())) {
            infoNew.setRoleID(bean.getRoleID());
        }

        // RoleID 角色ID
        if (!StringUtil.isEmpty(bean.getDepartmentID())) {
            infoNew.setDepartmentID(bean.getDepartmentID());
        }

        //用户单独设置权限
        if (!StringUtil.isEmpty(bean.getAccountAuthority())) {
            infoNew.setAccountAuthority(bean.getAccountAuthority());
        }

        // 手机
        if (StringUtil.notBlank(bean.getPhone())) {
            infoNew.setPhone(bean.getPhone());
        }
        // 性别 0:未知 1:男 2:女
        if (null != bean.getSex()) {
            infoNew.setSex(bean.getSex());
        }

        // 住址
        if (StringUtil.notBlank(bean.getAddress())) {
            infoNew.setAddress(bean.getAddress());
        }

        //备注
        if (StringUtil.notBlank(bean.getMemo())) {
            infoNew.setMemo(bean.getMemo());
        }

        // 修改时间
        infoNew.setUpdateTime(System.currentTimeMillis());
        // 修改人
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateOperatorID(operUserId);

        return this.service.updateById(infoNew);

    }


    /**
     * 删除管理员信息
     *
     * @param operUserId 操作人员标识
     * @param ids        对象标识
     * @return
     */
    @Override
    public boolean remove(String operUserId, String ids) {

        String[] idsArr = ids.split(",");
        boolean flag = true;
        for (String id : idsArr) {
            // 查看 数据系那个IQ那个
            Account info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            Account infoDel = new Account();
            infoDel.setId(info.getId());
            // 删除状态
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());
            infoDel.setDeleteTime(System.currentTimeMillis());
            infoDel.setDeleteOperatorID(operUserId);
            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;

            }
        }
        if (!flag) {
            // 修改失败
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }

    @Override
    public void addCache(Account bean) {


    }

    @Override
    public void updateCache(Account oldBean, Account newBean) {


    }

    @Override
    public void removeCache(Account bean) {


    }

    @Override
    public void initCache() {


    }

    @Override
    public List<Account> findAll(Account bean) {
        return null;
    }


    /**
     * 分页查询管理员
     *
     * @param params
     * @return
     */
    @Override
    public PageResult<AccountDTO> getList(AccountParams params) {
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("sa.CreateTime");
        queryWrapper.eq("sa.IsDelete", DeleteType.NOTDELETE.getCode());
        // 模糊搜索 用户登录名
        if (StringUtil.notBlank(params.getLoginName())) {
            queryWrapper.like("sa.LoginName", "%" + params.getLoginName() + "%");
        }
        // 模糊搜索 用户真实姓名
        if (StringUtil.notBlank(params.getAccountName())) {
            queryWrapper.like("AccountName", "%" + params.getAccountName() + "%");
        }

        if (StringUtil.notBlank(params.getDepartmentID())) {
            queryWrapper.like("sa.DepartmentID", "%" + params.getDepartmentID() + "%");
        }
        if (StringUtil.notBlank(params.getRoleID())) {
            queryWrapper.eq("sa.RoleID", params.getRoleID());
        }
        // 搜索 Phone 用户手机号
        if (StringUtil.notBlank(params.getPhone())) {
            queryWrapper.eq("sa.Phone", params.getPhone());
        }
        Page<AccountDTO> AccountPage = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<AccountDTO> page = this.service.selectLists(AccountPage, queryWrapper);
        if (page.getRecords() != null && page.getRecords().size() > 0) {
            for (AccountDTO bean : page.getRecords()) {
                if (StringUtil.isNotEmpty(bean.getPhoto())) {
                    bean.setPhotoPath(FileUploadUtil.getImgRootPath() + bean.getPhoto());
                }
            }
        }

        return new PageResult<>(page);
    }


    /**
     * 查看详情信息
     *
     * @param params
     * @return
     */
    @Override
    public AccountDTO getInfo(IDParams params) {
        if (!StringUtil.isNotEmpty(params.getId())) {
            throw new CommonException(SystemManageExceptionCodeEnum.IdParamsError.code, SystemManageExceptionCodeEnum.IdParamsError.msg);
        }
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sa.IsDelete", DeleteType.NOTDELETE.getCode());
        // 根据用户id查看用户详情
        if (StringUtil.notBlank(params.getId())) {
            queryWrapper.eq("sa.ID", params.getId());
        }
        AccountDTO info = this.service.selectInfo(queryWrapper);
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        if (StringUtil.isNotEmpty(info.getPhoto())) {
            info.setPhotoPath(FileUploadUtil.getImgRootPath() + info.getPhoto());
        }
        return info;
    }


    /**
     * 登录
     *
     * @param params 登录名
     * @return
     */
    @Override
    public String login(LoginParams params) {
        String verifyKey = CaptchaKeyUtils.getKey(params.getToken());
        String captcha = redisCache.getCacheObject(verifyKey);
        // 先删除缓存数据
        //redisCache.deleteObject(verifyKey);
        if (StringUtils.isNotEmpty(captcha)) {
            log.info("JwtRequestFilter .text=" + captcha);
            if (!Objects.equals(captcha, params.getCode())) {
                log.info("JwtRequestFilter .验证码正确");
                throw new CaptchaException("验证码错误");
            }
        } else {
            throw new CaptchaException("验证码已经过期");
        }



        // 用户验证
      /*  Authentication authentication =  authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(params.getUsername(), params.getPassword()));
*/
        Md5Util md5 = Md5Util.getInstance();
        String pwd = md5.getLongToken(params.getPassword());

        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        queryWrapper.eq("LoginName", params.getUsername().trim());
        Account info = this.service.getOne(queryWrapper);

        if (Objects.nonNull(info)) {
            if (!info.getLoginPassword().equals(pwd)) {
                // 密码错误
                logAccountLoginServiceBLL.save(info.getId(),"登录失败，密码错误",1,params.getPassword());
                throw new CommonException(SystemManageExceptionCodeEnum.PasswordError.code, SystemManageExceptionCodeEnum.PasswordError.msg);
            }
            if (info.getIsStatus() == 1) {
                logAccountLoginServiceBLL.save(info.getId(),"登录失败，账号冻结",1,params.getPassword());
                // 账号冻结
                throw new CommonException(SystemManageExceptionCodeEnum.AccountNoUse.code, SystemManageExceptionCodeEnum.AccountNoUse.msg);
            }
            // 生成jwt，并放置到请求头中
            //String jwt = jwtUtils.getToken(info.getLoginName(), info.getId(), accountServiceBLL.getRoleMenuList(info.getId()));
            logAccountLoginServiceBLL.save(info.getId(),"登录成功",0,params.getPassword());
            return null;
        } else {
            throw new CommonException(SystemManageExceptionCodeEnum.LoginNameError.code, SystemManageExceptionCodeEnum.LoginNameError.msg);
        }
    }


    /**
     * 用户重置修改密码
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean updatePassword(String operUserId, ResetPasswordParams bean) {
        boolean flag = true;
        // 给旧密码加密两次
        Md5Util md5 = Md5Util.getInstance();
        String pwd = md5.getLongToken(bean.getLoginPassword() != null ? bean.getLoginPassword() : "");
        String pwdTwo = md5.getLongToken(pwd);
        //查询用户输入旧密码是否正确
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ID", operUserId);
        queryWrapper.eq("LoginPassword", pwdTwo);
        Account info = this.service.getOne(queryWrapper);
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        Account infoNew = new Account();
        // 新密码进行md5两次加密
        String NewLoginPassword = md5.getLongToken(bean.getNewLoginPassword());
        infoNew.setLoginPassword(md5.getLongToken(NewLoginPassword));
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());
        infoNew.setUpdateOperatorID(operUserId);
        infoNew.setId(info.getId());
        boolean result = this.service.updateById(infoNew);
        // 判断是否修改成功
        if (!result) {
            flag = false;
        }
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     * 修改用户状态是否启用禁用
     *
     * @param operUserId 操作人员标识
     * @param params
     * @return
     */
    @Override
    public boolean updateState(String operUserId, AccountParams params) {
        boolean flag = true;
        Account info = this.get(params.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        Account infoNew = new Account();
        infoNew.setId(info.getId());
        //账号状态 0:正常 1:冻结
        if (Objects.nonNull(params.getIsStatus())) {
            infoNew.setIsStatus(params.getIsStatus());
        }
        infoNew.setUpdateTime(System.currentTimeMillis());
        infoNew.setUpdateOperatorID(operUserId);
        info.setUpdateOperatorName(params.getUpdateOperatorName());

        boolean result = this.service.updateById(infoNew);
        // 判断是否修改成功
        if (!result) {
            flag = false;
        }
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     * @return boolean    返回类型
     * @Description: TODO(查询用户名是否已经存在) 
     * @date 2020年3月13日 下午1:02:31 
     * @version V1.0   
     */
    public boolean isLoginName(String LoginName) {
        boolean flag = true;
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        queryWrapper.eq("LoginName", LoginName.trim());
        int count = this.service.count(queryWrapper);
        if (count > 0) {
            flag = false;
            return flag;
        }
        return flag;
    }


    /**
     * 转换实体对象到DTO
     *
     * @param info
     * @return
     */
    public AccountDTO getTransitionDTO(Account info) {
        AccountDTO dto = new AccountDTO();
        dto.setId(info.getId());
        dto.setLoginName(info.getLoginName()); //用户登录名
        dto.setAccountName(info.getAccountName());//用户真实姓名
        dto.setLoginPassword(info.getLoginPassword());
        dto.setPhoto(info.getPhoto());//头像路径
        dto.setAccountCode(info.getAccountCode());//员工编号
        dto.setRoleID(info.getRoleID());//角色ID
        dto.setAccountAuthority(info.getAccountAuthority());//用户单独设置权限
        dto.setEmail(info.getEmail());//邮箱
        dto.setPhone(info.getPhone());//电话
        dto.setAddress(info.getAddress());//住址
        if (StringUtil.isNotEmpty(info.getPhoto())) {
            dto.setPhotoPath(FileUploadUtil.getImgRootPath() + info.getPhoto());
        }
        dto.setMemo(info.getMemo());//备注
        dto.setIsStatus(info.getIsStatus());//账号状态 0:正常/启用 1:冻结/禁用
        dto.setSex(info.getSex());//性别 0:未知 1:男 2:女
        dto.setCreateOperatorID(info.getCreateOperatorID());
        dto.setCreateOperatorName(info.getCreateOperatorName());
        dto.setCreateTime(info.getCreateTime());
        dto.setUpdateOperatorID(info.getUpdateOperatorID());
        dto.setUpdateOperatorName(info.getUpdateOperatorName());
        dto.setType(info.getType());
        dto.setUpdateTime(info.getUpdateTime());
        dto.setExtraParams1(info.getExtraParams1());
        dto.setExtraParams2(info.getExtraParams2());
        dto.setExtraParams3(info.getExtraParams3());
        dto.setDepartmentID(info.getDepartmentID());
        return dto;
    }


    /**
     * 获取用户权限
     *
     * @param operUserId
     * @return
     */
    @Override
    public List<NodeTree> getMenuList(String operUserId) {
        List<NodeTree> treeList = new ArrayList<NodeTree>();

        // 查看用户是否存在
        Account info = this.get(operUserId);
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        if (!StringUtil.isNotEmpty(info.getRoleID())) {
            //用户没有权限
            throw new CommonException(BasePojectExceptionCodeEnum.RoleIdParamsError.code, BasePojectExceptionCodeEnum.RoleIdParamsError.msg);
        }

        //账号状态 0:正常/启用 1:不启用
        Role roleInfo = this.roleServiceBLL.get(info.getRoleID());
        if (Objects.isNull(roleInfo)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        //角色未启用
        if (null == roleInfo.getIsStatus() && roleInfo.getIsStatus() == 1) {
            throw new CommonException(BasePojectExceptionCodeEnum.RoleNotEnableFail.code, BasePojectExceptionCodeEnum.RoleNotEnableFail.msg);
        }
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("m.Sort");
        queryWrapper.eq("m.IsDelete", DeleteType.NOTDELETE.getCode());
        queryWrapper.eq("m.IsState", StateType.ISSTATE.getCode());
        queryWrapper.eq("ac.ID", info.getId());
        List<MenuDTO> menuList = this.menuService.selectLists(queryWrapper);
        if (menuList != null && menuList.size() > 0) {
            List<NodeTree> NodeTreeList = toNodeTreeList(menuList);
            treeList = this.toMenuTree(NodeTreeList);
        }
        return treeList;
    }


    /**
     * 转成 treeList
     *
     * @param
     * @return
     */
    private List<NodeTree> toNodeTreeList(List<MenuDTO> list) {
        List<NodeTree> treeList = new ArrayList<NodeTree>();
        if (null == list || list.size() == 0) {
            return treeList;
        }
        // 遍历列表 转成NodeTree对象
        for (MenuDTO menu : list) {
            //菜单对象不等于空的话
            if (null == menu) {
                continue;
            }
            NodeTree NodeTree = toNodeTreeForFullTree(menu);
            treeList.add(NodeTree);
        }
        return treeList;
    }


    /**
     * 转换
     *
     * @param NodeTreeList
     * @return
     */
    private List<NodeTree> toMenuTree(List<NodeTree> NodeTreeList) {
        List<NodeTree> dtoList = new ArrayList<>();
        Map<String, NodeTree> map = new HashMap<>();
        // 判断菜单是否返回空
        if (null == NodeTreeList || NodeTreeList.size() == 0) {
            return dtoList;
        }
        // 遍历NodeTreeList 依次放入map
        for (NodeTree bean : NodeTreeList) {
            map.put(bean.getId(), bean);
        }
        // 遍历NodeTreeList与map对比
        for (NodeTree bean : NodeTreeList) {
            if (map.containsKey(bean.getParentID())) {
                //if(NodeTree.getAttributes().get("MenuType"))
                // 获取父节点
                NodeTree parent = map.get(bean.getParentID());
                List<NodeTree> children = parent.getChildren();
                if (null == children) {
                    children = new ArrayList<NodeTree>();
                }
                children.add(bean);
                parent.setIsParent(true);
                parent.setChildren(children);

            } else {
                dtoList.add(bean);
            }
        }
        List<NodeTree> dtoListNew = new ArrayList<NodeTree>();
        // 遍历NodeTreeList与map对比
        for (NodeTree bean : dtoList) {
            if (bean.getMenuLevel() == 1) {
                dtoListNew.add(bean);
            }
        }
        return dtoListNew;
    }


    /**
     * 菜单集合转换成Map
     *
     * @param menu
     * @return
     */
    private NodeTree toNodeTreeForFullTree(MenuDTO menu) {
        NodeTree bean = new NodeTree();
        bean.setId(menu.getId());
        bean.setMenuName(menu.getMenuName());
        bean.setParentID(menu.getParentID());
        bean.setMenuLevel(menu.getMenuLevel());
        Map<String, String> attr = new HashMap<String, String>();
        //查看头像是否存在
        if (StringUtil.isNotEmpty(menu.getIcon())) {
            bean.setIconPath(FileUploadUtil.getImgRootPath() + menu.getIcon());
            attr.put("IconUrl", FileUploadUtil.getImgRootPath() + menu.getIcon());
        }
        attr.put("MenuName", menu.getMenuName());
        attr.put("Icon", menu.getIcon());
        attr.put("MenuCode", menu.getMenuCode());
        attr.put("MenuLevel", menu.getMenuLevel().toString());
        attr.put("ParentCode", menu.getParentCode());
        attr.put("ParentID", menu.getParentID());
        attr.put("MenuType", menu.getMenuType().toString());
        if (null != menu.getButtonType()) {
            attr.put("ButtonType", menu.getButtonType().toString());
        } else {
            attr.put("ButtonType", null);
        }
        attr.put("Url", menu.getUrl());
        attr.put("PagePath", menu.getPagePath());
        attr.put("memo", menu.getMemo());
        bean.setAttributes(attr);
        return bean;
    }


    /**
     * 验证手机号码是否存在
     *
     * @param MobilePhone
     * @return boolean
     */
    public boolean isMobilePhoneExistNotNull(String MobilePhone) {
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        queryWrapper.eq("Phone", MobilePhone);
        Integer count = this.service.count(queryWrapper);
        if (count > 0) {
            return true;
        }
        return false;
    }


    /**
     * 登录-用户名登录
     *
     * @param loginName 登录名
     * @return
     */
    @Override
    public AccountDTO getLoginName(String loginName) {

        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        queryWrapper.eq("LoginName", loginName.trim());
        Account info = this.service.getOne(queryWrapper);
        if (Objects.nonNull(info)) {
            return this.getTransitionDTO(info);
        }
        return null;
    }


    /**
     * 获取所有的权限
     *
     * @param operUserId
     * @return
     */
    @Override
    public String getRoleMenuList(String operUserId) {
        //  ROLE_admin,ROLE_normal,sys:user:list,....
        String authority = "";
        List<String> treeList = new ArrayList<>();
        if (redisUtil.hasKey("GrantedAuthority:" + operUserId)) {
            authority = (String) redisUtil.get("GrantedAuthority:" + operUserId);
            return authority;
        }
        Account info = this.get(operUserId);
        if (Objects.nonNull(info)) {//当用户存在的时候

            if (StringUtil.isNotEmpty(info.getRoleID())) {
                //账号状态 0:正常/启用 1:不启用
                Role roleInfo = this.roleServiceBLL.get(info.getRoleID());
                //角色未启用
                if (Objects.nonNull(roleInfo) && Objects.nonNull(roleInfo.getIsStatus()) && roleInfo.getIsStatus() != 1) {
                    QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
                    queryWrapper.orderByAsc("m.Sort");
                    queryWrapper.eq("m.IsDelete", DeleteType.NOTDELETE.getCode());
                    queryWrapper.eq("m.IsState", StateType.ISSTATE.getCode());
                    queryWrapper.eq("ac.ID", info.getId());
                    List<MenuDTO> menuList = this.menuService.selectLists(queryWrapper);
                    if (menuList != null && menuList.size() > 0) {
                        // 遍历列表 转成NodeTree对象
                        for (MenuDTO menu : menuList) {
                            treeList.add(menu.getUrl());
                        }
                        //获取角色权限
                        String roleCodes = "ROLE_" + roleInfo.getCode();
                        authority = roleCodes.concat(",");
                        // 获取具体方法权限
                        String menuPerms = menuList.stream().map(m -> m.getUrl()).collect(Collectors.joining(","));
                        authority = authority.concat(menuPerms);
                        redisUtil.set("GrantedAuthority:" + operUserId, authority, 60 * 60);
                    }
                }
            }
        }
        return authority;
    }


    /**
     * 获取所有的权限
     *
     * @param operUserId
     * @return
     */
    @Override
    public String updateRoleMenuList(String operUserId) {
        //  ROLE_admin,ROLE_normal,sys:user:list,....
        String authority = "";
        List<String> treeList = new ArrayList<>();
        Account info = this.get(operUserId);
        if (Objects.nonNull(info)) {//当用户存在的时候

            if (StringUtil.isNotEmpty(info.getRoleID())) {
                //账号状态 0:正常/启用 1:不启用
                Role roleInfo = this.roleServiceBLL.get(info.getRoleID());
                //角色未启用
                if (Objects.nonNull(roleInfo) && Objects.nonNull(roleInfo.getIsStatus()) && roleInfo.getIsStatus() != 1) {
                    QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
                    queryWrapper.orderByAsc("m.Sort");
                    queryWrapper.eq("m.IsDelete", DeleteType.NOTDELETE.getCode());
                    queryWrapper.eq("m.IsState", StateType.ISSTATE.getCode());
                    queryWrapper.eq("ac.ID", info.getId());
                    List<MenuDTO> menuList = menuService.selectLists(queryWrapper);
                    if (menuList != null && menuList.size() > 0) {
                        // 遍历列表 转成NodeTree对象
                        for (MenuDTO menu : menuList) {
                            treeList.add(menu.getUrl());
                        }
                        //获取角色权限
                        String roleCodes = "ROLE_" + roleInfo.getCode();
                        authority = roleCodes.concat(",");
                        // 获取具体方法权限
                        String menuPerms = menuList.stream().map(m -> m.getUrl()).collect(Collectors.joining(","));
                        authority = authority.concat(menuPerms);
                        redisUtil.set("GrantedAuthority:" + operUserId, authority, 60 * 60);
                    }

                }
            }
        }
        return authority;
    }
}