package com.elite.learn.systemManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.systemManage.dto.AccountDTO;
import com.elite.learn.systemManage.entity.Account;
import com.elite.learn.systemManage.mapper.AccountMapper;
import com.elite.learn.systemManage.service.IAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统管理-管理员表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {


    /**
     * 分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */


    /**
     * 管理员分页查询
     * @author: leroy
     * @date 2022/1/14 15:45
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<AccountDTO> selectLists(Page<AccountDTO> page, @Param(Constants.WRAPPER) Wrapper<Account> queryWrapper) {
        page.setRecords(this.baseMapper.selectLists(page, queryWrapper));
        return page;
    }

    /**
     * 查看用户详情
     *
     * @param queryWrapper
     * @return
     */
    @Override
    public AccountDTO selectInfo(Wrapper<Account> queryWrapper) {
        return this.baseMapper.selectInfo(queryWrapper);
    }
}
