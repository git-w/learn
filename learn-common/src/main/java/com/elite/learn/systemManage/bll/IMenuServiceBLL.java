package com.elite.learn.systemManage.bll;/**
 * @description:
 * @projectName:ymstudy
 * @see:com.elite.learn.systemManage.bll
 * @author: leroy
 * @createTime:2021/4/18 2:19
 * @version:1.0
 */

import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.IServiceCommonBLL;
import com.elite.learn.common.tree.NodeTree;
import com.elite.learn.systemManage.entity.Menu;
import com.elite.learn.systemManage.params.MenuParams;

import java.util.List;

/**
 *
 * @Title  :MenuServiceBLL.java
 * @Package: com.elite.learn.systemManage.bll
 * @Description:
 * @author: leroy
 * @data: 2021/4/18 2:19 
 */
public interface IMenuServiceBLL extends IServiceCommonBLL<Menu> {

    boolean saveAllMenu();


    /**
     * 超级管理员列表(角色设置菜单时，获取全量菜单)
     *
     * @param params
     * @return
     * @throws
     */
    List<NodeTree> getTree(MenuParams params);


    /**
     * 普通管理员列表(角色设置菜单时，获取全量菜单)
     *
     * @param params
     * @return
     * @throws
     */
    List<NodeTree> getAccountTree(MenuParams params);


    /**
     * 列表（前端根据角色获取菜单列表）
     *
     * @param params
     * @return
     * @throws
     */
    List<NodeTree> getMenuList(String operUserId,MenuParams params);


    /**
     * 多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateMultiState(String operUserId, BaseParams params);
}