package com.elite.learn.systemManage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Dictionary implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典项编号
     */
    @TableId(value = "d_key", type = IdType.UUID)
    private String dKey;

    /**
     * 字典项的值
     */
    private String dValue;

    /**
     * 字典项名称
     */
    private String dName;

    /**
     * 字典项上级编号
     */
    private String parentKey;

    /**
     * 是否可见，不在字典列表中展现（不能被选中）。但是逻辑上是存在的。
     */
    private Integer visible;

    /**
     * 字典项说明
     */
    private String description;


}
