package com.elite.learn.systemManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统管理-角色和菜单中间表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_mid_role_and_menu")
public class MidRoleAndMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "ID", type = IdType.UUID)
    private String id;

    /**
     * 角色ID
     */
    @TableField("RoleID")
    private String RoleID;

    /**
     * 菜单ID
     */
    @TableField("MenuID")
    private String MenuID;

    /**
     * 排除的按钮(多个时用逗号分隔)
     */
    @TableField("ExcludeButton")
    private String ExcludeButton;


}
