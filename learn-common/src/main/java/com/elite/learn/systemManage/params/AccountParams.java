package com.elite.learn.systemManage.params;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统管理-管理员表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
public class AccountParams extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 用户登录名
     */
    private String LoginName;

    /**
     * 用户真实姓名
     */
    private String AccountName;

    /**
     * 用户密码
     */
    private String LoginPassword;

    /**
     * 账号状态 0:正常/启用 1:冻结/禁用
     */
    private Integer IsStatus;

    /**
     * 头像路径
     */
    private String Photo;

    /**
     * 员工编号
     */
    private String AccountCode;

    /**
     * 角色ID
     */
    private String RoleID;

    /**
     * 用户单独设置权限
     */
    private String AccountAuthority;

    /**
     * 邮箱
     */
    private String Email;

    /**
     * 手机
     */
    private String Phone;

    /**
     * 性别 0:未知 1:男 2:女
     */
    private Integer Sex;

    /**
     * 部门id
     */
    private String DepartmentID;

    /**
     * 住址
     */
    private String Address;

    /**
     * 备注
     */
    private String Memo;

    /**
     * 新密码
     */
    private String NewLoginPassword;

    /**
     * 更新用户名称
     */
    private String UpdateOperatorName;

    /**
     * 更新用户ID
     */
    private String UpdateOperatorID;

}
