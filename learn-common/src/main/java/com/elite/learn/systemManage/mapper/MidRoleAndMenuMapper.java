package com.elite.learn.systemManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.elite.learn.systemManage.dto.MenuDTO;
import com.elite.learn.systemManage.entity.MidRoleAndMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统管理-角色和菜单中间表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
public interface MidRoleAndMenuMapper extends BaseMapper<MidRoleAndMenu> {


    /**
     * 自定义分页查询
     *
     * @param queryWrapper
     * @return
     */
    List<MenuDTO> selectLists(@Param(Constants.WRAPPER) Wrapper<MidRoleAndMenu> queryWrapper);
}
