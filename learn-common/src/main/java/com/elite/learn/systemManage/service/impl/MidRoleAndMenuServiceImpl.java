package com.elite.learn.systemManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.elite.learn.systemManage.dto.MenuDTO;
import com.elite.learn.systemManage.entity.MidRoleAndMenu;
import com.elite.learn.systemManage.mapper.MidRoleAndMenuMapper;
import com.elite.learn.systemManage.service.IMidRoleAndMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统管理-角色和菜单中间表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Service
public class MidRoleAndMenuServiceImpl extends ServiceImpl<MidRoleAndMenuMapper, MidRoleAndMenu> implements IMidRoleAndMenuService {


    /**
     * 查询左侧菜单权限
     *
     * @param queryWrapper
     * @return
     */
    @Override
    public List<MenuDTO> selectLists(@Param(Constants.WRAPPER) Wrapper<MidRoleAndMenu> queryWrapper) {
        return this.baseMapper.selectLists(queryWrapper);
    }
}
