package com.elite.learn.systemManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IImagesServiceBLL;
import com.elite.learn.systemManage.dto.ImagesDTO;
import com.elite.learn.systemManage.dto.ImagesUpdateDTO;
import com.elite.learn.systemManage.entity.Images;
import com.elite.learn.systemManage.query.ImagesQuery;
import com.elite.learn.systemManage.service.IImagesService;
import com.elite.learn.systemManage.vo.ImagesVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ImagesServiceBLLImpl implements IImagesServiceBLL {


    @Resource
    private IImagesService service;


    /**
     * 查看菜单详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public Images get(String id) {
        return this.service.getById(id);
    }


    @Override
    public ImagesVO getInfo(String id) {
        return null;
    }


    /**
     * 添加图片
     *
     * @param operUserId 操作人员标识
     * @param bean       图片详情对象
     * @return
     */
    @Override
    public String save(String operUserId, ImagesDTO bean) {
        String id = IdUtils.simpleUUID();
        Images info = new Images();
        info.setId(id);
        info.setCategoryID(bean.getCategoryID());
        info.setImgName(bean.getImgName());
        info.setImgUrl(bean.getImgUrl());
        info.setImgType(bean.getImgType());
        info.setImgHeight(bean.getImgHeight());
        info.setImgWidth(bean.getImgWidth());
        info.setImgSize(bean.getImgSize());
        info.setMemo(bean.getMemo());//备注
        info.setCreateTime(System.currentTimeMillis());
        info.setCreateOperatorID(operUserId);
        boolean flag = this.service.save(info);
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code, BasePojectExceptionCodeEnum.SaveFail.msg);
        }
        return info.getId();
    }


    /**
     * 修改菜单
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */

    @Override
    public boolean update(String operUserId, ImagesDTO bean) {

        // 查询菜单是否存在
        Images infImage = this.get(bean.getId());
        // 如果菜单不存在的话直接抛出异常
        if (Objects.isNull(infImage)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        // new一个新的对象 接受修改的值
        Images info = new Images();
        info.setId(bean.getId());

        if (StringUtil.notBlank(bean.getCategoryID())) {
            info.setCategoryID(bean.getCategoryID());
        }
        if (StringUtil.notBlank(bean.getImgName())) {
            info.setImgName(bean.getImgName());
        }
        if (StringUtil.notBlank(bean.getImgUrl())) {
            info.setImgUrl(bean.getImgUrl());
        }
        if (StringUtil.notBlank(bean.getImgType())) {
            info.setImgType(bean.getImgType());
        }
        if (bean.getImgHeight() != null) {
            info.setImgHeight(bean.getImgHeight());
        }
        if (bean.getImgWidth() != null) {
            info.setImgWidth(bean.getImgWidth());
        }
        if (bean.getImgSize() != null) {
            info.setImgSize(bean.getImgSize());
        }
        // 链接地址 修改
        if (StringUtil.isNotEmpty(bean.getMemo())) {
            info.setMemo(bean.getMemo());
        }
        info.setUpdateTime(System.currentTimeMillis());
        info.setUpdateOperatorID(operUserId);
        return this.service.updateById(info);

    }


    /**
     * 批量插入
     *
     * @param operUserId 操作人员标识
     * @param beans      对象集合
     * @return
     */
    @Override
    @Transactional
    public boolean saveBatch(String operUserId, List<ImagesDTO> beans) {
        boolean flag = true;
        List<Images> params = new ArrayList<>();
        if (Objects.isNull(beans) && beans.size() > 0) {
            throw new CommonException(BasePojectExceptionCodeEnum.ParamsError.code,
                    BasePojectExceptionCodeEnum.ParamsError.msg);
        }
        for (ImagesDTO bean : beans) {
            String id = IdUtils.simpleUUID();
            Images info = new Images();
            info.setId(id);
            info.setCategoryID(bean.getCategoryID());
            info.setImgName(bean.getImgName());
            info.setImgUrl(bean.getImgUrl());
            info.setImgType(bean.getImgType());
            info.setImgHeight(bean.getImgHeight());
            info.setImgWidth(bean.getImgWidth());
            info.setImgSize(bean.getImgSize());
            info.setMemo(bean.getMemo());//备注
            info.setCreateTime(System.currentTimeMillis());
            info.setCreateOperatorID(operUserId);
            params.add(info);
        }
        boolean batch = this.service.saveBatch(params);
        if (!batch) {
            flag = false;
        }
        return flag;
    }


    /**
     * 删除 菜单
     *
     * @param operUserId 操作人员标识
     * @param ids        对象标识
     * @return
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        boolean flag = true;
        String[] idsArr = ids.split(",");
            for (String id : idsArr) {
            Images info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            Images infoNew = new Images();
            // 删除状态
            infoNew.setIsDelete(DeleteType.ISDELETE.getCode());
            infoNew.setDeleteTime(System.currentTimeMillis());
            infoNew.setDeleteOperatorID(operUserId);
            infoNew.setId(info.getId());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        // 判断 菜单是否删除成功 如果删除成功直接抛出异常
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }


    /**
     * 查询全部
     * @param bean
     * @return
     */
    public List<ImagesVO> findAll(ImagesQuery bean) {
        QueryWrapper<Images> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CategoryID", bean.getCategoryID());
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        List<ImagesVO> list = this.service.findAll(queryWrapper);
        return list;
    }


    /**
     * 查询分页
     * @param params
     * @return
     */
    @Override
    public PageResult<ImagesVO> getList(ImagesQuery params) {

        QueryWrapper<Images> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("im.IsDelete", DeleteType.NOTDELETE.getCode());
        // 根据分组查询
        if (StringUtil.notBlank(params.getCategoryID())) {
            queryWrapper.eq("im.CategoryID", params.getCategoryID());
        }
        // 根据图片名称筛选
        if (StringUtil.notBlank(params.getImgName())) {
            queryWrapper.like("im.ImgName", "%" + params.getImgName() + "%");
        }
        Page<ImagesVO> pageVo = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<ImagesVO> page = this.service.selectLists(pageVo, queryWrapper);
        List<ImagesVO> result = page.getRecords();
        for (ImagesVO info : result) {
            info.setImgUrlPath(FileUploadUtil.getImgRootPath() + info.getImgUrl());
        }
        return new PageResult<>(page);
    }




    /**
     * 批量修改素材库图片分类
     *
     * @param params
     * @return
     */
    @Override
    public boolean updateMultiCategory(String operUserId, ImagesUpdateDTO params) {
        boolean flag = true;
        String ids = params.getIds();
        String[] idArr = ids.split(",");
        Images infoNew = null;
        for (String id : idArr) {
            Images info = this.get(id.trim());
            if (Objects.isNull(info)) {
                continue;
            }
            infoNew = new Images();
            infoNew.setId(id.trim());
            infoNew.setCategoryID(params.getCategoryID());
            infoNew.setUpdateOperatorID(operUserId);
            infoNew.setUpdateTime(System.currentTimeMillis());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        // 判断 菜单是否删除成功 如果删除成功直接抛出异常
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code,
                    BasePojectExceptionCodeEnum.UpdateFail.msg);
        }
        return flag;
    }


 
}
