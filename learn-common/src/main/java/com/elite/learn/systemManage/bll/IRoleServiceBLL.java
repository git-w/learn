package com.elite.learn.systemManage.bll;

import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.service.IServiceCommonBLL;
import com.elite.learn.systemManage.dto.RoleDTO;
import com.elite.learn.systemManage.entity.Role;
import com.elite.learn.systemManage.params.RoleParams;

import java.util.List;

/**
 * @Title :RoleServiceBLL.java
 * @Package: com.elite.learn.systemManage.bll
 * @Description:
 * @author: leroy
 * @data: 2021/4/18 2:21
 */
public interface IRoleServiceBLL extends IServiceCommonBLL<Role> {


    /**
     * 分页查询菜单
     *
     * @param params
     * @return
     */
    PageResult<RoleDTO> getList(RoleParams params);


    /**
     * 添加角色和菜单
     *
     * @param operUserId
     * @param bean
     * @return
     */
    String saveAndMenu(String operUserId, RoleParams bean);


    /**
     * 修改角色和菜单
     *
     * @param operUserId
     * @param bean
     * @return
     */
    Boolean updateAndMenu(String operUserId, RoleParams bean);


    /**
     * 返回角色菜单详请
     *
     * @param Id
     * @return
     */
    RoleDTO getInfo(String Id);


    /**
     * 查询所有的角色
     *
     * @param params
     * @return
     */
    List<RoleDTO> findAll(RoleParams params);
}