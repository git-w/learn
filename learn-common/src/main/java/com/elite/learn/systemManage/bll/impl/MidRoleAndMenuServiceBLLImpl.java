package com.elite.learn.systemManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IMidRoleAndMenuServiceBLL;
import com.elite.learn.systemManage.dto.MidRoleAndMenuDTO;
import com.elite.learn.systemManage.entity.MidRoleAndMenu;
import com.elite.learn.systemManage.params.MidRoleAndMenuParams;
import com.elite.learn.systemManage.service.IMidRoleAndMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class MidRoleAndMenuServiceBLLImpl implements IMidRoleAndMenuServiceBLL {


    @Resource
    private IMidRoleAndMenuService service;


    /**
     * 查看菜单详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public MidRoleAndMenu get(String id) {
        if (StringUtil.isNotEmpty(id)) {
            return this.service.getById(id);
        }
        return null;
    }


    /**
     * 添加菜单
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public String save(String operUserId, MidRoleAndMenu bean) {
        return null;
    }


    /**
     * 修改菜单
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean update(String operUserId, MidRoleAndMenu bean) {
        return false;
    }


    /**
     * 删除 菜单
     *
     * @param operUserId 操作人员标识
     * @param ids        对象标识
     * @return
     */
    @Override
    public boolean remove(String operUserId, String ids) {

        return false;
    }


    @Override
    public void addCache(MidRoleAndMenu bean) {

    }


    @Override
    public void updateCache(MidRoleAndMenu oldBean, MidRoleAndMenu newBean) {

    }


    @Override
    public void removeCache(MidRoleAndMenu bean) {


    }


    @Override
    public void initCache() {


    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2022/1/14 15:35
     * @update
     * @updateTime
     */
    public PageResult<MidRoleAndMenuDTO> getList(MidRoleAndMenuParams params) {

        QueryWrapper<MidRoleAndMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("CreateTime");

        Page<MidRoleAndMenu> AccountPage = new Page<MidRoleAndMenu>(params.getPageNum(), params.getPageSize());
        IPage<MidRoleAndMenu> page = this.service.page(AccountPage, queryWrapper);
        // 总记录数

        List<MidRoleAndMenuDTO> listDTO = new ArrayList<MidRoleAndMenuDTO>();
        if (page.getRecords() != null && page.getRecords().size() > 0) {
            MidRoleAndMenuDTO dto = null;
            for (MidRoleAndMenu bean : page.getRecords()) {
                dto = new MidRoleAndMenuDTO();
                dto.setRoleID(bean.getRoleID());
                dto.setMenuID(bean.getMenuID());
                listDTO.add(dto);
            }
        }
        return new PageResult(listDTO, (int) page.getTotal(), (int) page.getSize(), (int) page.getCurrent());
    }


    /**
     * 查看所有的
     */
    @Override
    public List<MidRoleAndMenu> findAll(MidRoleAndMenu bean) {
        QueryWrapper<MidRoleAndMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("CreateTime");
        queryWrapper.eq("isDelete", DeleteType.NOTDELETE.getCode());
        List<MidRoleAndMenu> list = this.service.list(queryWrapper);
        List<MidRoleAndMenu> autlist = new ArrayList<>();
        MidRoleAndMenu dto = null;
        for (MidRoleAndMenu aut : list) {
            dto = new MidRoleAndMenu();
            dto.setId(aut.getId());
            autlist.add(dto);
        }
        return autlist;
    }


}
