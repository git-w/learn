package com.elite.learn.systemManage.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 用户登录对象
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Getter
@Setter
public class LoginParams {

    private static final long serialVersionUID = 1L;


    /**
     * 用户登录名
     */
    @NotBlank(message = "登录名不能为空")
    @ApiModelProperty("登录名")
    private String username;


    /**
     * 用户密码
     */
    @NotBlank(message = "密码不能为空")
    @ApiModelProperty("密码")
    private String password;

    /**
     * 验证码
     */
    @NotBlank(message = "验证码不能为空")
    @ApiModelProperty("验证码")
    private String code;

    /**
     * 唯一标识
     */
    @NotBlank(message = "唯一标识不能为空")
    @ApiModelProperty("唯一标识")
    private String token;


}
