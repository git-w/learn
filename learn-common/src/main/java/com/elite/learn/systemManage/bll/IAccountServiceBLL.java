package com.elite.learn.systemManage.bll;

import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.service.IServiceCommonBLL;
import com.elite.learn.common.tree.NodeTree;
import com.elite.learn.systemManage.dto.AccountDTO;
import com.elite.learn.systemManage.entity.Account;
import com.elite.learn.systemManage.params.AccountParams;
import com.elite.learn.systemManage.params.LoginParams;
import com.elite.learn.systemManage.params.ResetPasswordParams;

import java.util.List;

/**
 *
 * @Title  :AccountServiceBLL.java
 * @Package: com.elite.learn.systemManage.bll
 * @Description:
 * @author: leroy
 * @date
 */
public interface IAccountServiceBLL extends IServiceCommonBLL<Account> {


    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    PageResult<AccountDTO> getList(AccountParams params);


    /**
     * 查看管理员详情
     *
     * @param params
     * @return
     */
    AccountDTO getInfo(IDParams params);


    /**
     * 登录
     * @param params
     * @return
     */
    String login(LoginParams params);


    /**
     * 重置密码
     *
     * @param operUserId 操作人员标识
     * @param bean 对象
     * @return
     */
    boolean updatePassword(String operUserId, ResetPasswordParams bean);


    /**
     * 修改用户状态是否启用禁用
     * @param params
     * @return
     */
    boolean updateState(String operUserId,AccountParams params);


    /**
     * 列表（前端根据角色获取菜单列表）
     * @param operUserId
     * @return
     */
    List<NodeTree> getMenuList(String operUserId);


    /**
     * 登录--用户名登录
     * @param loginName
     * @return
     */
    AccountDTO getLoginName(String loginName);


    /**
     * 列表（前端根据角色获取菜单列表）
     * @param operUserId
     * @return
     */
    String getRoleMenuList(String operUserId);


    /**
     * 更新redis里面的角色条件
     * @param operUserId
     * @return
     */
    String updateRoleMenuList(String operUserId);
}