package com.elite.learn.systemManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.systemManage.entity.Images;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.systemManage.vo.ImagesVO;

import java.util.List;

/**
 * <p>
 * 素材库--图片 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
public interface IImagesService extends IService<Images> {


    /**
     * 获取分页信息
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    IPage<ImagesVO> selectLists(Page<ImagesVO> page, Wrapper<Images> queryWrapper);


    /**
     * 获取同一分类下所有的对象
     *
     * @param queryWrapper
     * @return
     */
    List<ImagesVO> findAll(Wrapper<Images> queryWrapper);
}
