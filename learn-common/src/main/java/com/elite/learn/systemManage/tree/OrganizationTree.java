package com.elite.learn.systemManage.tree;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 *
 * @Title  :OrganizationTree.java  
 * @Package: com.elite.learn.systemManage.tree
 * @Description:
 * @author: leroy
 * @data: 2021/5/21 16:09 
 */
@Data
public class OrganizationTree {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 组织名称
     */
    private String OrganizationName;

    /**
     * 组织层级 0:总公司(内置) 1:总公司的部门或下属公司  依次类推...
     */
    private Integer DeptLevel;

    /**
     * 组织区分 0:总公司(内置) 1:总公司的部门  2:总公司下属的公司 3:下属公司的部门
     */
    private Integer DeptType;

    /**
     * 父标识
     */
    private String ParentID;

    /**
     * 备注
     */
    private String Memo;





    /**
     * 子节点
     */
    private List<OrganizationTree> Children;

    /**
     * 属性
     */
    private Map<String, String> Attributes;


    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    private Long CreateTime;

    /**
     * 更新者ID
     */
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    private String UpdateOperatorName;

    /**
     * 更新时间
     */
    private Long UpdateTime;

    /**
     * 额外参数1
     */
    private String ExtraParams1;

    /**
     * 额外参数2
     */
    private String ExtraParams2;

    /**
     * 额外参数3
     */
    private String ExtraParams3;
}