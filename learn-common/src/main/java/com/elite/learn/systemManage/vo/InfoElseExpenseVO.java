package com.elite.learn.systemManage.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Title InfoElseExpenseVo
 * @Package com.elite.learn.systemManage.vo
 * @Description
 * @author: leroy
 * @Date 2021-07-08 11:03
 */
@Data
public class InfoElseExpenseVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 消费名称
     */
    private String ExpenseName;

    /**
     * 分类ID
     */
    private String ClassifyID;

    /**
     * 分类名称
     */
    private String ClassifyName;

    /**
     * 价格
     */
    private BigDecimal Price;

    /**
     * 是否启用 0是 1否
     */
    private Integer IsState;

    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    private Long CreateTime;

}
