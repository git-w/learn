package com.elite.learn.systemManage.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Title InfoBreakfastVo
 * @Package com.elite.learn.systemManage.vo
 * @Description
 * @author: leroy
 * @Date 2021-07-07 17:21
 */
@Data
public class InfoBreakfastVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 早餐名称
     */
    private String BreakfastName;

    /**
     * 早餐类型：0: 房费含早 1:单份
     */
    private Integer BreakfastType;

    /**
     * 价格
     */
    private BigDecimal Price;

    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    private Long CreateTime;
}
