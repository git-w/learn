package com.elite.learn.systemManage.mapper;

import com.elite.learn.systemManage.entity.Dictionary;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-05-26
 */

public interface DictionaryMapper extends BaseMapper<Dictionary> {


	List<Dictionary> getDictionaryInfoByParentCode(@Param(value = "parentKey") String parentKey);
}
