package com.elite.learn.systemManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统管理-组织架构表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_organization")
public class Organization implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "ID", type = IdType.UUID)
    private String id;

    /**
     * 组织名称
     */
    @TableField("OrganizationName")
    private String OrganizationName;

    /**
     * 组织层级 0:总公司(内置) 1:总公司的部门或下属公司  依次类推...
     */
    @TableField("DeptLevel")
    private Integer DeptLevel;

    /**
     * 组织区分 0:总公司(内置) 1:总公司的部门  2:总公司下属的公司 3:下属公司的部门
     */
    @TableField("DeptType")
    private Integer DeptType;

    /**
     * 父标识
     */
    @TableField("ParentID")
    private String ParentID;

    /**
     * 备注
     */
    @TableField("Memo")
    private String Memo;

    /**
     * 删除标识 0:未删除 1:删除
     */
    @TableField("IsDelete")
    private Integer IsDelete;

    /**
     * 创建者ID
     */
    @TableField("CreateOperatorID")
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    @TableField("CreateOperatorName")
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    @TableField("CreateTime")
    private Long CreateTime;

    /**
     * 更新者ID
     */
    @TableField("UpdateOperatorID")
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    @TableField("UpdateOperatorName")
    private String UpdateOperatorName;

    /**
     * 更新时间
     */
    @TableField("UpdateTime")
    private Long UpdateTime;

    /**
     * 删除者ID
     */
    @TableField("DeleteOperatorID")
    private String DeleteOperatorID;

    /**
     * 删除者名称
     */
    @TableField("DeleteOperatorName")
    private String DeleteOperatorName;

    /**
     * 删除时间
     */
    @TableField("DeleteTime")
    private Long DeleteTime;

    /**
     * 额外参数1
     */
    @TableField("ExtraParams1")
    private String ExtraParams1;

    /**
     * 额外参数2
     */
    @TableField("ExtraParams2")
    private String ExtraParams2;

    /**
     * 额外参数3
     */
    @TableField("ExtraParams3")
    private String ExtraParams3;

    /**
     * 额外参数4
     */
    @TableField("ExtraParams4")
    private String ExtraParams4;

    /**
     * 额外参数5
     */
    @TableField("ExtraParams5")
    private String ExtraParams5;


}
