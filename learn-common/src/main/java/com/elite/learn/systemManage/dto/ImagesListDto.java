package com.elite.learn.systemManage.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 素材库--图片
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Data
public class ImagesListDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private List<ImagesDTO> imageList;



}
