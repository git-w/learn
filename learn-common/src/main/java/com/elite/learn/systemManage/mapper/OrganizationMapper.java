package com.elite.learn.systemManage.mapper;

import com.elite.learn.systemManage.entity.Organization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统管理-组织架构表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
public interface OrganizationMapper extends BaseMapper<Organization> {

}
