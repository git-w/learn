package com.elite.learn.systemManage.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.elite.learn.common.tree.NodeTree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 系统管理-角色管理表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
public class RoleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 角色名称
     */
    private String RoleName;

    /**
     * 是否可以编辑 0-是   1-否
     */
    private Integer IsUpdate;

    /**
     * 账号状态 0:正常/启用 1:不启用
     */
    private Integer IsStatus;

    /**
     * 是否是超管 0-是 1-否
     */
    private Integer IsSuperTube;

    /**
     * 部门id
     */
    private String DepartmentID;

    /**
     * 备注
     */
    private String Memo;



    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    private Long CreateTime;

    /**
     * 更新者ID
     */
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    private String UpdateOperatorName;

    /**
     * 更新时间
     */
    private Long UpdateTime;


    /**
     * 额外参数1
     */
    private String ExtraParams1;

    /**
     * 额外参数2
     */
    private String ExtraParams2;

    /**
     * 额外参数3
     */
    private String ExtraParams3;


    /**
     * 部门id
     */
    private String DepartmentName;
    /**
     * 角色编码
     */
    private String Code;

    /**
     * 权限数结构
     */
    private List<NodeTree> treeList;
}
