package com.elite.learn.systemManage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @Title StewardVo
 * @Package com.elite.learn.systemManage.vo
 * @Description
 * @author: leroy
 * @Date 2021-07-14 09:48
 */
@Data
public class StewardVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 管家姓名
     */
    private String name;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 是否启用 0是 1否
     */
    private Integer isState;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;
}
