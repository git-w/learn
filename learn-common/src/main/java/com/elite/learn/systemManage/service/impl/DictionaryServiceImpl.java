package com.elite.learn.systemManage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.systemManage.entity.Dictionary;
import com.elite.learn.systemManage.mapper.DictionaryMapper;
import com.elite.learn.systemManage.service.IDictionaryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-26
 */
@Service
public class DictionaryServiceImpl extends ServiceImpl<DictionaryMapper, Dictionary> implements IDictionaryService {


	@Resource
	private DictionaryMapper dictionaryMapper;


	/**
	 * 查询字典详情
	 * @author: leroy
	 * @date 2022/1/14 15:46
	 * @param parentKey
	 * @return null
	 * @update
	 * @updateTime
	 */
	@Override
	public List<Dictionary> getDictionaryInfoByParentCode(String parentKey) {
		return dictionaryMapper.getDictionaryInfoByParentCode(parentKey);
	}

}
