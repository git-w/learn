package com.elite.learn.systemManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统管理-管理员表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_account")
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.UUID)
    private String id;

    /**
     * 用户登录名
     */
    @TableField("LoginName")
    private String LoginName;

    /**
     * 用户真实姓名
     */
    @TableField("AccountName")
    private String AccountName;

    /**
     * 用户密码
     */
    @TableField("LoginPassword")
    private String LoginPassword;

    /**
     * 账号状态 0:正常/启用 1:冻结/禁用
     */
    @TableField("IsStatus")
    private Integer IsStatus;

    /**
     * 头像路径
     */
    @TableField("Photo")
    private String Photo;

    /**
     * 员工编号
     */
    @TableField("AccountCode")
    private String AccountCode;

    /**
     * 角色ID
     */
    @TableField("RoleID")
    private String RoleID;

    /**
     * 用户单独设置权限
     */
    @TableField("AccountAuthority")
    private String AccountAuthority;

    /**
     * 邮箱
     */
    @TableField("Email")
    private String Email;

    /**
     * 手机
     */
    @TableField("Phone")
    private String Phone;

    /**
     * 性别 0:未知 1:男 2:女
     */
    @TableField("Sex")
    private Integer Sex;

    /**
     * 部门id
     */
    @TableField("DepartmentID")
    private String DepartmentID;

    /**
     * 住址
     */
    @TableField("Address")
    private String Address;

    /**
     * 备注
     */
    @TableField("Memo")
    private String Memo;

    /**
     * 删除标识 0:未删除 1:删除
     */
    @TableField("IsDelete")
    private Integer IsDelete;

    /**
     * 创建者ID
     */
    @TableField("CreateOperatorID")
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    @TableField("CreateOperatorName")
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    @TableField("CreateTime")
    private Long CreateTime;

    /**
     * 更新者ID
     */
    @TableField("UpdateOperatorID")
    private String UpdateOperatorID;

    /**
     * 更新者名称
     */
    @TableField("UpdateOperatorName")
    private String UpdateOperatorName;

    /**
     * 更新时间
     */
    @TableField("UpdateTime")
    private Long UpdateTime;

    /**
     * 删除者ID
     */
    @TableField("DeleteOperatorID")
    private String DeleteOperatorID;

    /**
     * 删除者名称
     */
    @TableField("DeleteOperatorName")
    private String DeleteOperatorName;

    /**
     * 删除时间
     */
    @TableField("DeleteTime")
    private Long DeleteTime;

    /**
     * 管理员类型 0:超级管理员 1普通管理员
     */
    @TableField("Type")
    private Integer Type;

    /**
     * 额外参数1
     */
    @TableField("ExtraParams1")
    private String ExtraParams1;

    /**
     * 额外参数2
     */
    @TableField("ExtraParams2")
    private String ExtraParams2;

    /**
     * 额外参数3
     */
    @TableField("ExtraParams3")
    private String ExtraParams3;


}
