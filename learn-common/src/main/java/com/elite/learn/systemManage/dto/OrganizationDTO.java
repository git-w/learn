package com.elite.learn.systemManage.dto;

import lombok.Data;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class OrganizationDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * 组织名称
     */
    @NotBlank(message = "组织名称,参数为空")
    private String OrganizationName;

    /**
     * 组织层级 0:总公司(内置) 1:总公司的部门或下属公司  依次类推...
     */
    @NotNull(message = "组织层级,参数为空")
    private Integer DeptLevel;

    /**
     * 组织区分 0:总公司(内置) 1:总公司的部门  2:总公司下属的公司 3:下属公司的部门
     */
    @NotNull(message = "组织区分,参数为空")
    private Integer DeptType;

    /**
     * 父标识
     */
    private String ParentID;

    /**
     * 备注
     */
    private String Memo;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;
}
