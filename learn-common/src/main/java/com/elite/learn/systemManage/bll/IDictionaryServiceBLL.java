package com.elite.learn.systemManage.bll;

import java.util.List;

import com.elite.learn.common.core.service.IServiceCommonBLL;
import com.elite.learn.systemManage.entity.Dictionary;

public interface IDictionaryServiceBLL extends IServiceCommonBLL<Dictionary> {


	/**
	 * 查询字典详情
	 * @param parentKey
	 * @return
	 */
	List<Dictionary> getDictionaryInfoByParentCode(String parentKey);

}
