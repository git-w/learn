package com.elite.learn.systemManage.params;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 *
 * @Title  :ResetPasswordParams.java  
 * @Package: com.elite.learn.systemManage.params
 * @Description:
 * @author: leroy
 * @data: 2021/6/1 16:19 
 */
@Data
public class ResetPasswordParams implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户密码
     */
    @NotBlank(message = "旧密码,参数为空")
    private String LoginPassword;

    /**
     * 新密码
     */
    @NotBlank(message = "新密码,参数为空")
    private String NewLoginPassword;

    /**
     * 更新用户名称
     */
    private String UpdateOperatorName;

    /**
     * 更新用户ID
     */
    private String UpdateOperatorID;
}