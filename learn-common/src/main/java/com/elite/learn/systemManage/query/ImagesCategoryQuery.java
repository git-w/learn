package com.elite.learn.systemManage.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 素材库--分类表
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Data
public class ImagesCategoryQuery implements Serializable {


    private static final long serialVersionUID = 1L;


    /**
     * 分类名称
     */
    private String CategoryName;




}
