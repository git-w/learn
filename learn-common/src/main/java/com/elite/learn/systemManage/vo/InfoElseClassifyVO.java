package com.elite.learn.systemManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Title InfoElseClassifyVo
 * @Package com.elite.learn.systemManage.vo
 * @Description
 * @author: leroy
 * @Date 2021-07-08 11:01
 */
@Data
public class InfoElseClassifyVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 分类名称
     */
    private String ClassifyName;

    /**
     * 是否启用 0是 1否
     */
    private Integer IsState;

    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建者名称
     */
    private String CreateOperatorName;

    /**
     * 创建时间
     */
    private Long CreateTime;
}
