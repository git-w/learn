package com.elite.learn.systemManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.systemManage.dto.AccountDTO;
import com.elite.learn.systemManage.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统管理-管理员表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
public interface AccountMapper extends BaseMapper<Account> {


    /**
     * 自定义分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    List<AccountDTO> selectLists(Page<AccountDTO> page, @Param(Constants.WRAPPER) Wrapper<Account> queryWrapper);


    /**
     * 用户详情
     *
     * @param queryWrapper
     * @return
     */
    AccountDTO selectInfo(@Param(Constants.WRAPPER) Wrapper<Account> queryWrapper);
}
