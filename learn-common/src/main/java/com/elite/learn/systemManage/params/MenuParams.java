package com.elite.learn.systemManage.params;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 系统管理-菜单表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
public class MenuParams extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 菜单名称
     */
    private String MenuName;

    /**
     * 菜单或按钮icon
     */
    private String Icon;

    /**
     * 菜单标识
     */
    private String MenuCode;

    /**
     * 菜单层级 1:一级菜单 2:二级菜单 依次类推...
     */
    private Integer MenuLevel;

    /**
     * 父菜单标识
     */
    private String ParentCode;

    /**
     * 父菜单ID
     */
    private String ParentID;

    /**
     * 排序号 数据越高排序越大 0是最低
     */
    private Integer Sort;

    /**
     * 类型 0-菜单 1-按钮
     */
    private Integer MenuType;

    /**
     * 是否启用 0-是 1-否
     */
    private Integer IsState;

    /**
     * 按钮区分 1：增改  2：查询 3：删除
     */
    private Integer ButtonType;

    /**
     * 链接地址
     */
    private String Url;

    /**
     * 备注
     */
    private String Memo;

    /**
     * 角色id
     */
    private String RoleId;
    /**
     * 更新者名称
     */
    private String UpdateOperatorName;

    /**
     * 菜单参数集合
     */
    private List<MenuParams> MenuList;
}
