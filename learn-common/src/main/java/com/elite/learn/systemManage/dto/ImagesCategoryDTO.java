package com.elite.learn.systemManage.dto;

import lombok.Data;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 素材库--分类表
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Data
public class ImagesCategoryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * 分类名称
     */
    @NotBlank(message = "分类名称,参数为空")
    private String CategoryName;

    /**
     * 备注
     */
    private String Memo;

    /**
     * 是否是其他 0-是 1-否
     */
    private Integer IsRests;


}
