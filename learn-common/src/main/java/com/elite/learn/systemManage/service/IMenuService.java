package com.elite.learn.systemManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.elite.learn.systemManage.dto.MenuDTO;
import com.elite.learn.systemManage.entity.Account;
import com.elite.learn.systemManage.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统管理-菜单表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
public interface IMenuService extends IService<Menu> {


    /**
     * 查询左侧权限
     *
     * @param queryWrapper
     * @return
     */
    List<MenuDTO> selectLists(@Param(Constants.WRAPPER) Wrapper<Account> queryWrapper);
}
