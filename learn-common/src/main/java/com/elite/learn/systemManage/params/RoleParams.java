package com.elite.learn.systemManage.params;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 系统管理-角色管理表
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Data
public class RoleParams  extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */

    private String id;

    /**
     * 角色名称
     */
    @NotBlank(message = "角色名称,参数为空")
    private String RoleName;
    /**
     * 角色名称
     */
    @NotBlank(message = "角色编码,参数为空")
    private String Code;

    /**
     * 是否可以编辑 0-是   1-否
     */
    private Integer IsUpdate;

    /**
     * 账号状态 0:正常/启用 1:不启用
     */
    private Integer IsStatus;

    /**
     * 是否是超管 0-是 1-否
     */
    private Integer IsSuperTube;

    /**
     * 部门id
     */
    @NotBlank(message = "部门id,参数为空")
    private String DepartmentID;

    /**
     * 备注
     */
    private String Memo;
    /**
     * 菜单id
     */
    @NotBlank(message = "菜单集合,参数为空")
    private String MenuIdList;


}
