package com.elite.learn.systemManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.systemManage.entity.Images;
import com.elite.learn.systemManage.mapper.ImagesMapper;
import com.elite.learn.systemManage.service.IImagesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.systemManage.vo.ImagesVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 素材库--图片 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Service
public class ImagesServiceImpl extends ServiceImpl<ImagesMapper, Images> implements IImagesService {


    /**
     * 查询分页
     * @author: leroy
     * @date 2022/1/14 15:47
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<ImagesVO> selectLists(Page<ImagesVO> page, Wrapper<Images> queryWrapper) {
        page.setRecords(this.baseMapper.selectLists(page, queryWrapper));
        return page;
    }


    /**
     * 查询全部
     * @author: leroy
     * @date 2022/1/14 15:47
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public List<ImagesVO> findAll(Wrapper<Images> queryWrapper) {
        return baseMapper.findAll(queryWrapper);
    }
}
