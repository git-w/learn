package com.elite.learn.systemManage.contants;

public enum SystemManageExceptionCodeEnum {

    SaveSuccess(100000, "保存成功"),
    UpdateFail(100000, "编辑成功"),
    DeleteSuccess(100000, "删除成功"),
    SelectSuccess(100000, "查询成功"),
    LoginNameError(100003, "用户名输入错误"),
    PasswordError(100003, "密码输入错误"),
    MenuNameParamsError(100001, "菜单名称,参数为空或者参数错误"),

    MenuCodeParamsError(100001, "菜单标识参数为空!"),
    MenuLevelParamsError(100001, "菜单层级参数为空!"),
    MenuTypeParamsError(100001, "类型参数为空!"),
    RoleNameParamsError(100001, "菜单层级参数为空!"),
    IsStateParamsError(100001,"状态，参数为空或者参数错误!"),
    LoginNameParamsError(100001, "登录名参数为空!"),
    AccountNameParamsError(100001,"用户真实姓名参数为空！"),
    LoginPasswordParamsError(100001, "密码参数为空！"),
    RoleIdParamsError(100009, "该管理员权限不足"),


    IdParamsError(100001, "id参数为空"),
    DepartmentIDParamsError(100001, "部门id参数为空"),
    RoleIDParamsError(100001, "角色ID参数为空"),
    PhoneParamsError(100001, "手机参数为空"),


    ParentIDParamsError(100001, "组织区分参数为空"),
    ParamsError(100001, "参数为空"),
    GetFail(100002,"请先为该房间设置保洁人员！"),
    AccountNoUse(100004, "账号已停用,请联系管理员!"),
    RepeatYhdlm(100004, "用户名存在重复"),
    AlreadyExist(100004, "该渠道已存在，不能重复添加"),
    RoleNotEnableFail(100010,"角色未启用"),
    SaveFail(100005,"保存失败"),
    DeleteFail(100005,"删除失败"),
    SelectFail(100005,"查询失败"),

    AuthorityError(100001, "没有权限");
    public Integer code;
    public String msg;

    private SystemManageExceptionCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
