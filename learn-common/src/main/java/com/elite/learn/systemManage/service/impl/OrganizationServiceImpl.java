package com.elite.learn.systemManage.service.impl;

import com.elite.learn.systemManage.entity.Organization;
import com.elite.learn.systemManage.mapper.OrganizationMapper;
import com.elite.learn.systemManage.service.IOrganizationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统管理-组织架构表 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
@Service
public class OrganizationServiceImpl extends ServiceImpl<OrganizationMapper, Organization> implements IOrganizationService {

}
