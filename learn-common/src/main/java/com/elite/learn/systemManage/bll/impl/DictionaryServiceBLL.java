package com.elite.learn.systemManage.bll.impl;

import com.elite.learn.systemManage.bll.IDictionaryServiceBLL;
import com.elite.learn.systemManage.entity.Dictionary;
import com.elite.learn.systemManage.service.IDictionaryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DictionaryServiceBLL implements IDictionaryServiceBLL{


	@Resource
	private IDictionaryService service;
	
	@Override
	public Dictionary get(String id) {

		return null;
	}

	@Override
	public String save(String operUserId, Dictionary bean) {

		return null;
	}

	@Override
	public boolean update(String operUserId, Dictionary bean) {

		return false;
	}

	@Override
	public boolean remove(String operUserId, String ids) {

		return false;
	}

	@Override
	public void addCache(Dictionary bean) {

		
	}

	@Override
	public void updateCache(Dictionary oldBean, Dictionary newBean) {

		
	}

	@Override
	public void removeCache(Dictionary bean) {

		
	}

	@Override
	public void initCache() {

		
	}

	@Override
	public List<Dictionary> findAll(Dictionary bean) {

		return null;
	}


	/**
	 * 查询字典详情
	 * @author: leroy
	 * @date 2022/1/14 15:20
	 * @param parentKey
	 * @return null
	 * @update
	 * @updateTime
	 */
	@Override
	public List<Dictionary> getDictionaryInfoByParentCode(String parentKey) {
		return this.service.getDictionaryInfoByParentCode(parentKey);
	}

}
