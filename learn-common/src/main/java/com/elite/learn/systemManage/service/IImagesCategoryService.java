package com.elite.learn.systemManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.elite.learn.systemManage.entity.ImagesCategory;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.systemManage.vo.ImagesCategoryVO;

import java.util.List;

/**
 * <p>
 * 素材库--分类表 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
public interface IImagesCategoryService extends IService<ImagesCategory> {


    /**
     * 查询全部
     *
     * @return
     */
    List<ImagesCategoryVO> findAll(Wrapper<ImagesCategory> queryWrapper);
}
