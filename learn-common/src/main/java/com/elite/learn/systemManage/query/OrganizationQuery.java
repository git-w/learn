package com.elite.learn.systemManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

@Data
public class OrganizationQuery  extends PageParams implements Serializable {


	private static final long serialVersionUID = 1L;


	/**
	 * 编号
	 */
	private String id;

	/**
	 * 组织名称
	 */
	private String OrganizationName;

	/**
	 * 组织层级 0:总公司(内置) 1:总公司的部门或下属公司  依次类推...
	 */
	private Integer DeptLevel;

	/**
	 * 组织区分 0:总公司(内置) 1:总公司的部门  2:总公司下属的公司 3:下属公司的部门
	 */
	private Integer DeptType;

	/**
	 * 父标识
	 */
	private String ParentID;



}
