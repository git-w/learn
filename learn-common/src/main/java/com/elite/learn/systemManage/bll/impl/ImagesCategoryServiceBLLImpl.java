package com.elite.learn.systemManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IImagesCategoryServiceBLL;
import com.elite.learn.systemManage.dto.ImagesCategoryDTO;
import com.elite.learn.systemManage.entity.Images;
import com.elite.learn.systemManage.entity.ImagesCategory;
import com.elite.learn.systemManage.query.ImagesCategoryQuery;
import com.elite.learn.systemManage.service.IImagesCategoryService;
import com.elite.learn.systemManage.service.IImagesService;
import com.elite.learn.systemManage.vo.ImagesCategoryVO;
import com.elite.learn.systemManage.vo.ImagesVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
public class ImagesCategoryServiceBLLImpl implements IImagesCategoryServiceBLL {


    @Resource
    private IImagesCategoryService service;
    // 素材库--图片 服务类
    @Resource
    private IImagesService imageService;


    /**
     * 查看图片分类详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public ImagesCategory get(String id) {
        if (StringUtil.isNotEmpty(id)) {
            return this.service.getById(id);
        }
        return null;
    }


    @Override
    public ImagesCategoryVO getInfo(String id) {
        return null;
    }


    /**
     * 添加图片分类
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public String save(String operUserId, ImagesCategoryDTO bean) {
        String id = IdUtils.simpleUUID();
        ImagesCategory info = new ImagesCategory();
        info.setId(id);
        info.setCategoryName(bean.getCategoryName());
        info.setIsRests(0);
        info.setMemo(bean.getMemo());//备注
        info.setCreateTime(System.currentTimeMillis());
        info.setCreateOperatorID(operUserId);
        boolean flag = this.service.save(info);
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SaveFail.code,
                    BasePojectExceptionCodeEnum.SaveFail.msg);
        }
        return info.getId();
    }


    /**
     * 修改图片分类
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    @Override
    public boolean update(String operUserId, ImagesCategoryDTO bean) {

        // 查询菜单是否存在
        ImagesCategory info = this.get(bean.getId());
        if (info.getIsRests() == 1) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        // 如果菜单不存在的话直接抛出异常
        if (Objects.isNull(info)) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        // new一个新的对象 接受修改的值
        ImagesCategory infoNew = new ImagesCategory();
        infoNew.setId(bean.getId());
        // 修改分组名
        if (StringUtil.notBlank(bean.getCategoryName())) {
            infoNew.setCategoryName(bean.getCategoryName());
        }
        // 链接地址 修改
        if (StringUtil.isNotEmpty(bean.getMemo())) {
            infoNew.setMemo(bean.getMemo());
        }

        infoNew.setUpdateTime(System.currentTimeMillis());
        infoNew.setUpdateOperatorID(operUserId);

        return   this.service.updateById(infoNew);

    }


    /**
     * 删除 菜单
     *
     * @param operUserId 操作人员标识
     * @param ids        对象标识
     * @return
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        String[] idsArr = ids.split(",");
        boolean flag = true;
        for (String id : idsArr) {
            ImagesCategory info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            if (info.getIsRests() == 1) {
                continue;
            }
            ImagesCategory infoNew = new ImagesCategory();
            infoNew.setId(info.getId());
            // 删除状态
            infoNew.setIsDelete(DeleteType.ISDELETE.getCode());
            infoNew.setDeleteTime(System.currentTimeMillis());
            infoNew.setDeleteOperatorID(operUserId);
            boolean result = this.service.updateById(infoNew);
            // 修改分类下所属的素材
            if (result) {
                QueryWrapper<Images> wrapper = new QueryWrapper<>();
                wrapper.eq("CategoryID", id);
                wrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
                List<ImagesVO> list = this.imageService.findAll(wrapper);
                for (ImagesVO bean : list) {
                    Images infoImages = new Images();
                    infoImages.setId(bean.getId());
                    infoImages.setCreateTime(System.currentTimeMillis());
                    infoImages.setCreateOperatorID(operUserId);
                    // 其他类型id
                    infoImages.setCategoryID("df300210e48446c3a6b1f7c8762513a3");
                    this.imageService.updateById(infoImages);
                }
            }
            if (!result) {
                flag = false;
            }
        }
        // 判断 菜单是否删除成功 如果删除成功直接抛出异常
        if (!flag) {
            throw new CommonException(BasePojectExceptionCodeEnum.SelectFail.code,
                    BasePojectExceptionCodeEnum.SelectFail.msg);
        }
        return flag;
    }

    @Override
    public List<ImagesCategoryVO> findAll(ImagesCategoryQuery bean) {
        return null;
    }


    @Override
    public PageResult<ImagesCategoryVO> getList(ImagesCategoryQuery params) {
        return null;
    }


    /**
     * 查询全部
     *
     * @return
     */
    @Override
    public List<ImagesCategoryVO> findAll() {
        QueryWrapper<ImagesCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("IsDelete", DeleteType.NOTDELETE.getCode());
        queryWrapper.orderByDesc("IsRests");
        List<ImagesCategoryVO> list = this.service.findAll(queryWrapper);
        if (Objects.nonNull(list)) {
            return list;
        }
        return null;
    }
}
