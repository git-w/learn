package com.elite.learn.systemManage.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 素材库--图片
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Data
public class ImagesQuery  extends PageParams implements Serializable {


    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "ID", type = IdType.UUID)
    private String id;

    /**
     * 分类Id
     */
    private String CategoryID;

    /**
     * 图片名称
     */
    private String ImgName;



}
