package com.elite.learn.systemManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.systemManage.dto.RoleDTO;
import com.elite.learn.systemManage.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统管理-角色管理表 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-05-21
 */
public interface RoleMapper extends BaseMapper<Role> {


    /**
     * 自定义分页查询
     *
     * @param page
     * @param queryWrapper
     * @return
     */
    List<RoleDTO> selectLists(Page<RoleDTO> page, @Param(Constants.WRAPPER) Wrapper<Role> queryWrapper);


    /**
     * 用户详情
     *
     * @param queryWrapper
     * @return
     */
    RoleDTO selectInfo(@Param(Constants.WRAPPER) Wrapper<Role> queryWrapper);
}
