package com.elite.learn.systemManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 素材库--图片
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_images")
public class Images implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "ID", type = IdType.UUID)
    private String id;

    /**
     * 分类Id
     */
    @TableField("CategoryID")
    private String CategoryID;

    /**
     * 图片名称
     */
    @TableField("ImgName")
    private String ImgName;

    /**
     * 图片地址Url
     */
    @TableField("ImgUrl")
    private String ImgUrl;

    /**
     * 文件内容类型
     */
    @TableField("ImgType")
    private String ImgType;

    /**
     * 图片高 px
     */
    @TableField("ImgHeight")
    private Integer ImgHeight;

    /**
     * 图片宽 px
     */
    @TableField("ImgWidth")
    private Integer ImgWidth;

    /**
     * 图片文件大小 字节
     */
    @TableField("ImgSize")
    private Integer ImgSize;

    /**
     * 备注
     */
    @TableField("Memo")
    private String Memo;

    /**
     * 删除标识 0:未删除 1:删除
     */
    @TableField("IsDelete")
    private Integer IsDelete;

    /**
     * 创建者ID
     */
    @TableField("CreateOperatorID")
    private String CreateOperatorID;

    /**
     * 创建时间
     */
    @TableField("CreateTime")
    private Long CreateTime;

    /**
     * 更新者ID
     */
    @TableField("UpdateOperatorID")
    private String UpdateOperatorID;

    /**
     * 更新时间
     */
    @TableField("UpdateTime")
    private Long UpdateTime;

    /**
     * 删除者ID
     */
    @TableField("DeleteOperatorID")
    private String DeleteOperatorID;

    /**
     * 删除时间
     */
    @TableField("DeleteTime")
    private Long DeleteTime;

    /**
     * 额外参数1
     */
    @TableField("ExtraParams1")
    private String ExtraParams1;


}
