package com.elite.learn.systemManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 素材库--分类表
 * </p>
 *
 * @author: leroy
 * @since 2021-06-07
 */
@Data
public class ImagesCategoryVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 分类名称
     */
    private String CategoryName;

    /**
     * 备注
     */
    private String Memo;

    /**
     * 是否是其他 0-是 1-否
     */
    private Integer IsRests;


    /**
     * 创建者ID
     */
    private String CreateOperatorID;

    /**
     * 创建时间
     */
    private Long CreateTime;

    /**
     * 更新者ID
     */
    private String UpdateOperatorID;

    /**
     * 更新时间
     */
    private Long UpdateTime;


}
