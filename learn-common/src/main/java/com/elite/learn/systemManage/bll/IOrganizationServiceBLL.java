package com.elite.learn.systemManage.bll;

import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.systemManage.dto.OrganizationDTO;
import com.elite.learn.systemManage.entity.Organization;
import com.elite.learn.systemManage.query.OrganizationQuery;
import com.elite.learn.systemManage.tree.OrganizationTree;
import com.elite.learn.systemManage.vo.OrganizationVO;

import java.util.List;

public interface IOrganizationServiceBLL extends ICommonServiceBLL<Organization, OrganizationDTO, OrganizationVO, OrganizationQuery> {


    /**
     * 组织架构树
     *
     * @param params
     * @return
     */
    List<OrganizationTree> getTree(OrganizationQuery params);


    /**
     * 查询所有的部门
     *
     * @param params
     * @return
     */
    List<OrganizationVO> findAllDepartment(OrganizationQuery params);



}
