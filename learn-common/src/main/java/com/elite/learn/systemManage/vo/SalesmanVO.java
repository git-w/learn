package com.elite.learn.systemManage.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Title SalesmanVo
 * @Package com.elite.learn.systemManage.vo
 * @Description
 * @author: leroy
 * @Date 2021-07-19 09:04
 */
@Data
public class SalesmanVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 销售员姓名
     */
    private String salesmanName;

    /**
     * 联系电话
     */
    private String salesmanPhone;

    /**
     * 是否启用 0是 1否
     */
    private Integer isState;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;


}
