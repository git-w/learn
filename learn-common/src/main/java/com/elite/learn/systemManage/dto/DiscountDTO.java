package com.elite.learn.systemManage.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Title DiscountDto
 * @Package com.elite.learn.systemManage.dto
 * @Description
 * @Author Axe
 * @Date 2021-10-11 2:51 下午
 */
@Data
public class DiscountDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.UUID)
    @NotBlank(message = "主键id不能为空",groups = Update.class)
    private String id;

    /**
     * 折扣名称
     */
    @NotBlank(message = "折扣名称不能为空")
    private String discountName;

    /**
     * 是否区分周末：0-不区分；1-区分
     */
    private Integer isWeekend;

    /**
     * 是否区分节假日：0-不区分；1-区分
     */
    private Integer isHoliday;

    /**
     * 平日折扣
     */
    @NotNull(message = "平日折扣不能为空")
    private BigDecimal singleDiscount;

    /**
     * 周六折扣
     */
    private BigDecimal saturdayDiscount;

    /**
     * 周日折扣
     */
    private BigDecimal sundayDiscount;

    /**
     * 节假日折扣
     */
    private BigDecimal holidayDiscount;

    /**
     * 创建者ID
     */
    private String createOperatorId;

    /**
     * 创建者名称
     */
    private String createOperatorName;


    /**
     * 更新者名称
     */
    private String updateOperatorName;
}
