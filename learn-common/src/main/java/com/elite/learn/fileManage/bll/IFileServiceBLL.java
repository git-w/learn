package com.elite.learn.fileManage.bll;

import com.elite.learn.fileManage.vo.FileVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author: leroy
 * @Description:
 * @create 2020-12-01-10:28
 */
public interface IFileServiceBLL {


    /**
     * @param file: 上传的文件
     * @Description: 单个文件上传（图片、视频音频、文本文档）
     * @author: leroy
     * @Date: 2020/12/1 10:34
     * @return: FileDTO类
     **/
    FileVO uploadFile(MultipartFile file);


    /**
     * @param files: 上传的多个文件
     * @Description: 单个文件上传（图片、视频音频、文本文档）
     * @author: leroy
     * @Date: 2020/12/1 10:34
     * @return: FileDTO类
     **/
    List<FileVO> uploadMultiFile(MultipartFile[] files);
    List<FileVO> uploadMultiFile2(MultipartFile[] files, String print);

}
