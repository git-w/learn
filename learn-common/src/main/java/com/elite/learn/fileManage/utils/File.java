package com.elite.learn.fileManage.utils;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.fileManage.contants.FileExceptionCodeEnum;
import com.elite.learn.fileManage.entity.FastDFSFile;
import com.elite.learn.fileManage.vo.FileVO;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;

/**
 * @author mlhstart
 * @Description:
 * @create 2020-12-01-19:25
 */
public class File {

    /**
     * @param file: 上传的文件
     * @Description: 单个文件上传（图片、视频音频、文本文档）
     * @author: leroy
     * @Date: 2020/12/2 9:20
     * @return: FileDTO类
     **/
    public static FileVO uploadFile(MultipartFile file) {

        //判断文件是否为空
        if (file.isEmpty()) {
            throw new CommonException(BasePojectExceptionCodeEnum.ParamsError.code, BasePojectExceptionCodeEnum.ParamsError.msg);
        }

        //获取文件字节数组
        byte[] bytes = new byte[0];
        try {
            bytes = file.getBytes();//文件本身的字节数组
        } catch (IOException e) {
            throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
        }
        //获取文件名称
        String originalFilename = file.getOriginalFilename();//原来的文件名
        //获取文件扩展名
        String filenameExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        //获取文件大小
        long size = file.getSize();
        //获取文件内容类型
        String contentType = file.getContentType();


        //封装文件
        FastDFSFile fastdfsfile = new FastDFSFile(originalFilename, bytes, filenameExtension);
        //图片上传
        String[] upload = FastDFSClient.upload(fastdfsfile);

        if (null != upload && upload.length > 0) {
            //创建DTO用于信息显示
            FileVO dto = new FileVO();


            //文件字节大小
            dto.setSize((long) size);
            //获取文件名称
            dto.setFileName(originalFilename);
            //文件内容类型
            dto.setFileContentType(contentType);
            //文件短路径
            dto.setImgPath(upload[0] + "/" + upload[1]);
            //文件长路径
            dto.setImgPathUrl(FileUploadUtil.getImgRootPath() + "/" + upload[0] + "/" + upload[1]);

            return dto;
        }
        return null;
    }

    /**
     * @param ids: 一个或多个文件短路径，用逗号分隔
     * @Description: 文件删除
     * @author: leroy
     * @Date: 2020/12/2 9:14
     * @return: boolean
     **/
    public static boolean deleteFile(String ids) {
        //判断路径是否为空
        if (StringUtils.isEmpty(ids)) {
            throw new CommonException(BasePojectExceptionCodeEnum.ParamsError.code, BasePojectExceptionCodeEnum.ParamsError.msg);
        }

        //截取路径
        String[] imgPathsArr = ids.split(",");

        int flag = 1;

        for (String imgPath : imgPathsArr) {

            //判断路径是否合法
            if (imgPath.indexOf("/") == -1) {
                throw new CommonException(FileExceptionCodeEnum.ParamsTypeError.code, FileExceptionCodeEnum.ParamsTypeError.msg);
            }
            //获取文件组名
            String groupName = imgPath.substring(0, imgPath.indexOf("/"));
            //获取远程的文件名
            String remoteFileName = imgPath.substring(groupName.length() + 1, imgPath.length());
            //删除文件
            flag = FastDFSClient.deleteFile(groupName, remoteFileName);
        }

        if (Objects.nonNull(flag)) {
            throw new CommonException(BasePojectExceptionCodeEnum.DeleteFail.code, BasePojectExceptionCodeEnum.DeleteFail.msg);
        }
        return true;
    }
}
