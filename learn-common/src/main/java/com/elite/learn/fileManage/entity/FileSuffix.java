package com.elite.learn.fileManage.entity;

/**
 * @author: leroy
 * @Description: 文件后缀名
 * @create 2020-11-30-9:20
 */
public class FileSuffix {

    /**
     * 图片后缀名
     */
    public static final String IMAGE_SUFFIX = "bmp,tiff,gif,jpeg,pdf,png,jpg";
    /**
     * 视频音频后缀名
     */
    public static final String VIDEO_SUFFIX = "avi,wmv,flash,mp4,mp3,wma,avi,rm,rmvb,flv,mpg,mov,mkv";
    /**
     * 文本文档后缀名
     */
    public static final String DOCUMENT_SUFFIX = "doc,docx,xls,xlsx,rtf,ppt,pptx,wps,txt";
    private FileSuffix() {

    }
}
