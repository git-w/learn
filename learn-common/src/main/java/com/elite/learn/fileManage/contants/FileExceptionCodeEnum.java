package com.elite.learn.fileManage.contants;

/**
 * @Description: 文件上传异常   枚举类
 * @author: leroy
 * @Date: 2020/12/2 13:51
 **/
public enum FileExceptionCodeEnum {


    ParamsError(100001, "参数为空"),
    UpdateSuccess(100000, "上传成功"),
    UpdateFail(100005, "上传失败"),
    FileTypeError(100001, "文件类型错误"),
    DeleteSuccess(100000, "删除成功"),
    DeleteFail(100005, "删除失败"),
    ParamsTypeError(100001, "参数类型错误"),
    ParamsBigError(100001, "参数个数超过9个"),
    ;

    public Integer code;
    public String msg;

    private FileExceptionCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
