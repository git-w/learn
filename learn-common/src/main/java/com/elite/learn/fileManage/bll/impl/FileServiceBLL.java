package com.elite.learn.fileManage.bll.impl;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.fileManage.bll.IFileServiceBLL;
import com.elite.learn.fileManage.utils.FileUtils;
import com.elite.learn.fileManage.vo.FileVO;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * @author: leroy
 * @Description:
 * @create 2020-12-01-10:27
 */
@Service
public class FileServiceBLL implements IFileServiceBLL {

    /**
     * @param file: 上传的文件
     * @Description: 单个文件上传
     * @author: leroy
     * @Date: 2020/12/1 10:34
     * @return: FileDTO类
     **/
    @Override
    public FileVO uploadFile(MultipartFile file) {
        //判断文件是否为空
        if (file.isEmpty()) {
            throw new CommonException(BasePojectExceptionCodeEnum.ParamsError.code, BasePojectExceptionCodeEnum.ParamsError.msg);
        }

        //上传文件
        FileVO dto = FileUtils.uploadFile(file);

        if (null == dto) {
            throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
        }

        //如果为图片类型，加入图片的宽和高
        try {
            //读取文件，转化成输入流(返回一个InputStream以从中读取文件的内容)
            InputStream inputStream = file.getInputStream();
            //获取图片信息
            BufferedImage image = ImageIO.read(inputStream);

            //获取图片的宽
            Integer width = image.getWidth();
            //获取图片的高
            Integer height = image.getHeight();

            //设置图片的宽
            dto.setWidth(width);
            //设置图片的高
            dto.setHeight(height);

            return dto;
        } catch (Exception e) {
            return dto;
        }
    }

    /**
     * @param files: 上传的文件
     * @Description: 多个文件上传
     * @author: leroy
     * @Date: 2020/12/1 10:34
     * @return: FileDTO类
     **/
    @Override
    public List<FileVO> uploadMultiFile(MultipartFile[] files) {

        //判断文件是否为空
    /*    if(files.isEmpty()){
            throw new CommonException(BasePojectExceptionCodeEnum.ParamsError.code,BasePojectExceptionCodeEnum.ParamsError.msg);
        }
*/
        //新创建一个listDto用于存储查询回来的对象
        List<FileVO> listDTO = new ArrayList<>();

        for (MultipartFile file : files) {

            //上传文件
            FileVO dto = FileUtils.uploadFile(file);

            if (null == dto) {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
            }

            //如果为图片类型，加入图片的宽和高
            try {
                //读取文件，转化成输入流(返回一个InputStream以从中读取文件的内容)
                InputStream inputStream = file.getInputStream();
                //获取图片信息
                BufferedImage image = ImageIO.read(inputStream);

                //获取图片的宽
                Integer width = image.getWidth();
                //获取图片的高
                Integer height = image.getHeight();

                //设置图片的宽
                dto.setWidth(width);
                //设置图片的高
                dto.setHeight(height);

            } catch (Exception e) {

            }

            listDTO.add(dto);
        }

        return listDTO;

    }

    /**
     * @param files: 上传的文件
     * @Description: 多个文件上传--上传水印
     * @author: leroy
     * @Date: 2020/12/1 10:34
     * @return: FileDTO类
     **/
    @Override
    public List<FileVO> uploadMultiFile2(MultipartFile[] files, String print) {

        //判断文件是否为空
    /*    if(files.isEmpty()){
            throw new CommonException(BasePojectExceptionCodeEnum.ParamsError.code,BasePojectExceptionCodeEnum.ParamsError.msg);
        }
*/
        //新创建一个listDto用于存储查询回来的对象
        List<FileVO> listDTO = new ArrayList<>();

        for (MultipartFile file : files) {

            //上传文件
            FileVO dto = FileUtils.uploadFile2(file,print);

            if (null == dto) {
                throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
            }

            //如果为图片类型，加入图片的宽和高
            try {
                //读取文件，转化成输入流(返回一个InputStream以从中读取文件的内容)
                InputStream inputStream = file.getInputStream();
                //获取图片信息
                BufferedImage image = ImageIO.read(inputStream);

                //获取图片的宽
                Integer width = image.getWidth();
                //获取图片的高
                Integer height = image.getHeight();

                //设置图片的宽
                dto.setWidth(width);
                //设置图片的高
                dto.setHeight(height);

            } catch (Exception e) {

            }

            listDTO.add(dto);
        }

        return listDTO;

    }
}
