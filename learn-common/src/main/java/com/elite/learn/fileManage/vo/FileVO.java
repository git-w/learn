package com.elite.learn.fileManage.vo;


import lombok.Data;

import java.io.Serializable;

/**
 * @author: leroy
 * @Description:
 * @create 2020-11-27-10:25
 */
@Data
public class FileVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 图片宽 px
     */
    private Integer Width;

    /**
     * 图片高 px
     */
    private Integer Height;

    /**
     * 文件字节大小
     */
    private Long Size;

    /**
     * 文件短路径
     */
    private String ImgPath;

    /**
     * 文件长路径
     */
    private String ImgPathUrl;

    /**
     * 文件名称
     */
    private String FileName;

    /**
     * 文件内容类型
     */
    private String FileContentType;
}
