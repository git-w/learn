package com.elite.learn.fileManage.utils;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.fileManage.contants.FileExceptionCodeEnum;
import com.elite.learn.fileManage.entity.FastDFSFile;
import com.elite.learn.fileManage.vo.FileVO;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/**
 * @author: leroy
 * @Description:
 * @create 2020-12-01-19:25
 */
public class FileUtils {
    private final static String HOME_PATH = FileUtils.class.getResource("/").getPath();
    private final static String FONTS_FILE = "static/fonts/";

    /**
     * @param file: 上传的文件
     * @Description: 单个文件上传（图片、视频音频、文本文档）
     * @author: leroy
     * @Date: 2020/12/2 9:20
     * @return: FileDTO类
     **/
    public static FileVO uploadFile(MultipartFile file) {

        //判断文件是否为空
        if (file.isEmpty()) {
            throw new CommonException(BasePojectExceptionCodeEnum.ParamsError.code, BasePojectExceptionCodeEnum.ParamsError.msg);
        }

        //获取文件字节数组
        byte[] bytes = new byte[0];
        try {
            bytes = file.getBytes();//文件本身的字节数组
        } catch (IOException e) {
            throw new CommonException(BasePojectExceptionCodeEnum.UpdateFail.code, BasePojectExceptionCodeEnum.UpdateFail.msg);
        }
        //获取文件名称
        String originalFilename = file.getOriginalFilename();//原来的文件名
        //获取文件扩展名
        String filenameExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        //获取文件大小
        long size = file.getSize();
        //获取文件内容类型
        String contentType = file.getContentType();

        //封装文件
        FastDFSFile fastdfsfile = new FastDFSFile(originalFilename, bytes, filenameExtension);
        //图片上传
        String[] upload = FastDFSClient.upload(fastdfsfile);

        if (null != upload && upload.length > 0) {
            //创建DTO用于信息显示
            FileVO dto = new FileVO();


            //文件字节大小
            dto.setSize((long) size);
            //获取文件名称
            dto.setFileName(originalFilename);
            //文件内容类型
            dto.setFileContentType(contentType);
            //文件短路径
            dto.setImgPath(upload[0] + "/" + upload[1]);
            //文件长路径
            dto.setImgPathUrl(FileUploadUtil.getImgRootPath() + "/" + upload[0] + "/" + upload[1]);

            return dto;
        }
        return null;
    }

    /**
     *  引入自定义的字体
     * @param fontPath 字体存放路径（项目运行的服务器）
     * @param fontStyle 字体样式(是否加粗)
     * @param fontSize  字体大小
     * @return
     * @throws IOException
     */
    public static Font getFont(String fontPath,int fontStyle, float fontSize) throws IOException {
        String fontUrl = "";
        if(fontStyle==Font.BOLD) {
            fontUrl = HOME_PATH+fontPath+"msyhbd.ttc";
        }else {
            fontUrl = HOME_PATH+fontPath+"msyh.ttc";
        }
        java.io.File file = FileUtil.file(fontUrl);
//        File file = new File(fontUrl);
        //1.判断文件上级目录是否存在
        String parentPath=file.getParent();
        java.io.File parentFile = new java.io.File(parentPath);
        if(!parentFile.exists()) {//创建文件夹
            parentFile.mkdirs();
        }
        if(!file.exists()) {//如果文件不存在，则需要从阿里云下载到本地路径
            System.out.println("字体不存在===>字体位置："+HOME_PATH+fontPath);
//            if(fontStyle==Font.BOLD) {
//                HttpUtil.downloadFile("https://grandpictest.xxx.xxx.com/common/font/msyhbd.ttc",fontUrl);
//            }else {
//                HttpUtil.downloadFile("https://grandpictest.xxx.xxx.com/common/font/msyh.ttc",fontUrl);
//            }
        }
        Font font = null;
        try{
            font = Font.createFont(Font.TRUETYPE_FONT, file);
            font = font.deriveFont(fontStyle, fontSize);
        }catch (Exception e){
            return null;
        }
        return font;
    }

    /**
     * @param file: 上传的文件
     * @Description: 单个文件上传（图片、视频音频、文本文档）
     * @author: leroy
     * @Date: 2020/12/2 9:20
     * @return: FileDTO类
     **/
    public static FileVO uploadFile2(MultipartFile file, String print) {

        //判断文件是否为空
        if (file.isEmpty()) {
            throw new CommonException(BasePojectExceptionCodeEnum.ParamsError.code, BasePojectExceptionCodeEnum.ParamsError.msg);
        }
        //获取文件扩展名
        String filenameExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        byte[] bytes = new byte[0];
        try{
            InputStream srcStream  = file.getInputStream();
            Image image = ImgUtil.read(srcStream);//源图片
            Font font = getFont(FONTS_FILE,Font.BOLD, 20);
            Image desImage = ImgUtil.pressText(image,
                    print,Color.WHITE,
                    font, //字体
                    0, //x坐标修正值。 默认在中间，偏移量相对于中间偏移
                    0, //y坐标修正值。 默认在中间，偏移量相对于中间偏移
                    0.8f//透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
            );
            bytes = ImgUtil.toBytes(desImage,filenameExtension.toLowerCase(Locale.ROOT));
        }catch (IOException ex){
            ex.printStackTrace();
        }
        //获取文件名称
        String originalFilename = file.getOriginalFilename();//原来的文件名
        //获取文件大小
        long size = file.getSize();
        //获取文件内容类型
        String contentType = file.getContentType();

        //封装文件
        FastDFSFile fastdfsfile = new FastDFSFile(originalFilename, bytes, filenameExtension);
        //图片上传
        String[] upload = FastDFSClient.upload(fastdfsfile);

        if (null != upload && upload.length > 0) {
            //创建DTO用于信息显示
            FileVO dto = new FileVO();


            //文件字节大小
            dto.setSize((long) size);
            //获取文件名称
            dto.setFileName(originalFilename);
            //文件内容类型
            dto.setFileContentType(contentType);
            //文件短路径
            dto.setImgPath(upload[0] + "/" + upload[1]);
            //文件长路径
            dto.setImgPathUrl(FileUploadUtil.getImgRootPath() + "/" + upload[0] + "/" + upload[1]);

            return dto;
        }
        return null;
    }

    /**
     * @param ids: 一个或多个文件短路径，用逗号分隔
     * @Description: 文件删除
     * @author: leroy
     * @Date: 2020/12/2 9:14
     * @return: boolean
     **/
    public static boolean deleteFile(String ids) {
        //判断路径是否为空
        if (StringUtils.isEmpty(ids)) {
            throw new CommonException(BasePojectExceptionCodeEnum.ParamsError.code, BasePojectExceptionCodeEnum.ParamsError.msg);
        }

        //截取路径
        String[] imgPathsArr = ids.split(",");

        int flag = 1;

        for (String imgPath : imgPathsArr) {

            //判断路径是否合法
            if (imgPath.indexOf("/") == -1) {
                throw new CommonException(FileExceptionCodeEnum.ParamsTypeError.code, FileExceptionCodeEnum.ParamsTypeError.msg);
            }
            //获取文件组名
            String groupName = imgPath.substring(0, imgPath.indexOf("/"));
            //获取远程的文件名
            String remoteFileName = imgPath.substring(groupName.length() + 1, imgPath.length());
            //删除文件
            flag = FastDFSClient.deleteFile(groupName, remoteFileName);
        }

        if (flag != 0) {
            throw new CommonException(BasePojectExceptionCodeEnum.DeleteFail.code, BasePojectExceptionCodeEnum.DeleteFail.msg);
        }
        return true;
    }
}
