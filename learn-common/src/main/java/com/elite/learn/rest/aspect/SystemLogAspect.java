package com.elite.learn.rest.aspect;

import com.elite.learn.common.utils.ip.RequestUtil;
import com.elite.learn.rest.annotation.SystemLog;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 自定义操作日志切面处理类
 */
@Aspect
@Component
public class SystemLogAspect {

    private static Logger logger = LoggerFactory.getLogger(SystemLogAspect.class);

    /**
     * 操作数据库
     */
   /* @Resource
    private ISysLogsServiceBLL sysLogsServiceBLL;*/




    /*private SysLogs adminLog = new SysLogs();*/


    /*定义切点
     *  Controller层切点 注解拦截
     */
    @Pointcut("@annotation(com.elite.learn.rest.annotation.SystemLog)")
    public void logPointCut() {
    }

    private void clearObj() {
      /*  adminLog = null;
        System.gc();
        adminLog = new SysLogs();*/
    }

    /*前置切面*/
    @Before(value = "logPointCut()")
    public void around(JoinPoint point) {

        // this.clearObj();
        //  adminLog = null;
        System.gc();
        //   adminLog = new SysLogs();
        logger.info("调用日志监控");
        /*从切面值入点获取植入点方法*/
        MethodSignature signature = (MethodSignature) point.getSignature();
        /*获取切入点方法*/
        Method method = signature.getMethod();
        /*获取方法上的值*/
        SystemLog SystemLog = method.getAnnotation(SystemLog.class);
        /*保存操作事件*/
        if (SystemLog != null) {
            String operation = SystemLog.operation();
            /*保存日志类型*/
            // adminLog.setOperation(operation);
            String type = SystemLog.type();
            //  adminLog.setType(Integer.valueOf(type));
            String methodName = SystemLog.methodName();
            //  adminLog.setMethod(methodName);
            /*打印*/
            logger.info("操作事件 :" + operation);
        }
//        String token = request.getHeader("Token");
//        String uid = request.getHeader("uid");
        /*if (StringUtil.notBlank(token) && StringUtil.notBlank(uid)) {
            try {
             //   AccountDTO account = JwtHelperUtil.unsign(request.getHeader("Token"), AccountDTO.class);
              *//*  if (Objects.nonNull(account)) {
                   *//**//* adminLog.setCreateOperatorName(account.getLoginName());
                    adminLog.setCreateOperatorID(account.getId());*//**//*
                }*//*
            } catch (Exception e) {
            }


        }*/
        /*获取请求体内容*/
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String requestUri = request.getRequestURI();/*获取请求地址*/
        String requestMethod = request.getMethod();/*获取请求方式*/
        // String remoteAddr1 = request.getRemoteAddr();/*获取请求IP*/
        String remoteAddr = this.getIpAddress(request);
//        logger.info(remoteAddr1);
//        System.out.println(remoteAddr1 + "处理前的ip-----------" + remoteAddr + "处理后的ip");
        /*存请求地址，请求方式，请求IP*/
        RequestUtil.getIP(request);
        //  adminLog.setIpAddr(remoteAddr);
        logger.info("客户端IP为：" + remoteAddr);
        //  adminLog.setStr1(requestUri);
        logger.info("请求路径为：" + requestUri);
        // adminLog.setStr2(requestMethod);
        logger.info("请求方式为：" + requestMethod);
        /*获取参数*/
        Object[] args = point.getArgs();
        if (args != null) {
            for (Object obj : args) {
                /*   System.out.println("传递的参数" + obj);*/
                String params = obj.toString();
                /*      System.out.println("传递的参数" + params);*/
                logger.info("请求参数为：" + params);
                /*保存请求参数*/
                // adminLog.setParams(params);
            }
        }
        // adminLog.setExceptionLog("无异常");
    }

    /*异常切面*/
    @AfterThrowing(pointcut = "logPointCut()", throwing = "ex")
    public void exception(Exception ex) {
       /* adminLog.setExceptionLog(ex.getMessage());
        adminLog.setStr3("Err");
        adminLog.setCreateTime(System.currentTimeMillis());*/
        //  sysLogsServiceBLL.saveLog(adminLog);
    }

    /*后置切面*/
    @AfterReturning(returning = "rvt", pointcut = "logPointCut()")
    public void after(Object rvt) {
        //adminLog.setResultParams(rvt.toString());
        //  adminLog.setCreateTime(System.currentTimeMillis());
        // sysLogsServiceBLL.saveLog(adminLog);
        logger.info("返回结果为：" + rvt.toString());
    }

    //ip处理工具类
    public String getIpAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
                //根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();

                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                ipAddress = inet.getHostAddress();
            }
        }
        //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ipAddress != null && ipAddress.length() > 15) { //"***.***.***.***".length() = 15
            if (ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
        }
        return ipAddress;
    }

}

