package com.elite.learn.rest.annotation;/**
 * @description:
 * @projectName:learn
 * @see:com.elite.learn.rest.annotation
 * @author: leroy
 * @createTime:2020/7/14 16:09
 * @version:1.0
 */

import java.lang.annotation.*;

/**
 *
 * @Title  :SystemLog.java
 * @Package: com.elite.learn.rest.annotation
 * @Description:type 添加-1  修改-2 删除-3 列表-4 详情-5
 * @author: leroy
 * @data: 2020/7/14 16:09
 *
 */
@Target({ ElementType.PARAMETER,ElementType.METHOD }) //注解放置的目标位置,METHOD是可注解在方法级别上
@Retention(RetentionPolicy.RUNTIME) //注解在哪个阶段执行
@Documented//生成文档
public @interface SystemLog {
    /** 操作事件     */
    String operation() default "";
    /** 日志名称 */
    String methodName();
    /** 日志类型 */
    String type();//1新增2修改3删除4查看
    //    @SystemLog(methodName="用户管理",operation = "列表",typeName = "list",type="4")
    //    @SystemLog(methodName="用户管理",operation = "删除",typeName = "delete",type="3")
    //    @SystemLog(methodName="用户管理",operation = "修改",typeName = "update",type="2")
    //   @SystemLog(methodName="用户管理",operation = "添加",typeName = "save",type="1")
    //   @SystemLog(methodName="用户管理",operation = "详情",typeName = "info",type="5")
}