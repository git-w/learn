package com.elite.learn.rest.annotation;

import com.elite.learn.common.core.type.OperationType;

import java.lang.annotation.*;

/**
 *
 * @Title  :WebLogAspect.java  
 * @Package: com.elite.learn.rest.aspect
 * @Description:
 * @author: leroy
 * @data: 2020/11/6 13:59 
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface WebLog {
    /**
     * 方法描述,可使用占位符获取参数:{{tel}}
     */
    String detail() default "";

    /**
     * 操作类型(enum):主要是select,insert,update,delete
     */
    OperationType operationType() default OperationType.UNKNOWN;
}