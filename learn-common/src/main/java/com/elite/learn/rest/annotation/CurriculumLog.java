package com.elite.learn.rest.annotation;
import java.lang.annotation.*;

/**
 *
 * @Title  :SystemLog.java
 * @Package: com.elite.learn.rest.annotation
 * @Description:type 添加-1  修改-2 删除-3 列表-4 详情-5
 * @author: leroy
 * @data: 2020/7/14 16:09
 *
 */
@Target({ ElementType.PARAMETER,ElementType.METHOD }) //注解放置的目标位置,METHOD是可注解在方法级别上
@Retention(RetentionPolicy.RUNTIME) //注解在哪个阶段执行
@Documented//生成文档
public @interface CurriculumLog {
    /** 课程类型 */
    String type();//1推荐课程 2课程 3安全培训课程
}