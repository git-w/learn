package com.elite.learn.rest.aspect;

import com.elite.learn.rest.annotation.CurriculumLog;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
/**
 * 自定义操作日志切面处理类
 */
@Aspect
@Component
public class CurriculumLogAspect {

    private static Logger logger = LoggerFactory.getLogger(CurriculumLogAspect.class);

    /**
     * 操作数据库
     */
/*    @Resource
    private IResourcesServiceBLL resourcesServiceBLL;

    @Resource
    private IResourcesPublicServiceBLL resourcesPublicServiceBLL;*/
    @Resource
    private HttpServletRequest request;




    /*定义切点
     *  Controller层切点 注解拦截
     */
    @Pointcut("@annotation(com.elite.learn.rest.annotation.CurriculumLog)")
    public void logPointCut() {
    }



    /*前置切面*/
    @Before(value = "logPointCut()")
    public void around(JoinPoint point) {


        logger.info("调用日志监控");
        /*从切面值入点获取植入点方法*/
        MethodSignature signature = (MethodSignature) point.getSignature();
        /*获取切入点方法*/
        Method method = signature.getMethod();
        /*获取方法上的值*/
        CurriculumLog SystemLog = method.getAnnotation(CurriculumLog.class);
        /*保存操作事件*/
        if (SystemLog != null) {
            String type = SystemLog.type();
            /*获取参数*/
            Object[] args = point.getArgs();
            if (args != null) {
                for (Object obj : args) {
                    /*   System.out.println("传递的参数" + obj);*/
                    String params = obj.toString();
                    /*      System.out.println("传递的参数" + params);*/
                    logger.info("请求参数为：" + params);
                    if(type.equals("2")){

                    }else{
                      //  resourcesServiceBLL.RecordNumberOfViewers(params);
                    }
                }
            }

        }


    }





    //ip处理工具类
    public String getIpAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
                //根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                ipAddress = inet.getHostAddress();
            }
        }
        //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ipAddress != null && ipAddress.length() > 15) { //"***.***.***.***".length() = 15
            if (ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
        }
        return ipAddress;
    }

}

