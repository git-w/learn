package com.elite.learn.operationManage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商城首页轮播图
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_info_banner")
public class Banner implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * banner图名称
     */
    private String bannerName;

    /**
     * 图片存放路径（短）
     */
    private String imgPath;

    /**
     * 排序 数值越大越靠前
     */
    private Integer sort;

    /**
     * 开始显示时间
     */
    private Long startTime;

    /**
     * 结束显示时间
     */
    private Long endTime;

    /**
     * 跳转类型的id 或者图片跳转url
     */
    private String businessId;

    /**
     * 跳转类型：0：不跳转,1-跳转URL,2-客户列表,3-会员卡列表;4-视频详情;5-客户详情,6-会员卡详情;7-活动列表,8-活动详情,
     */
    @TableField("Type")
    private Integer Type;

    /**
     * 投放位置 0-首页
     */
    private Integer putSite;

    /**
     * 备注
     */
    private String memo;

    /**
     *  是否时间段显示 0是 1否
     */
    private Integer isTime;

    /**
     * 是否上线 0是 1否
     */
    private Integer isState;

    /**
     * 删除标识 0:未删除 1:已删除
     */
    private Integer isDelete;

    /**
     * 创建者ID（反馈者）
     */
    private String createOperatorid;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 删除者ID
     */
    private String deleteOperatorId;

    /**
     * 删除者名称
     */
    private String deleteOperatorName;

    /**
     * 删除时间
     */
    private Long deleteTime;

    /**
     * 额外参数1
     */
    private String extraParams1;


}
