package com.elite.learn.operationManage.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elite.learn.operationManage.entity.Banner;
import com.elite.learn.operationManage.mapper.BannerMapper;
import com.elite.learn.operationManage.service.IBannerService;
import com.elite.learn.operationManage.vo.BannerVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商城首页轮播图 服务实现类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
@Service
public class BannerServiceImpl extends ServiceImpl<BannerMapper, Banner> implements IBannerService {


    /**
     * 详情
     * @author: leroy
     * @date 2022/1/14 15:13
     * @param id
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public BannerVO getInfo(String id) {
        return this.baseMapper.getInfo(id);
    }


    /**
     * 分页
     * @author: leroy
     * @date 2022/1/14 15:13
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     */
    @Override
    public IPage<BannerVO> getLists(Page<BannerVO> pageVo, @Param(Constants.WRAPPER) Wrapper<Banner> queryWrapper) {
        pageVo.setRecords(this.baseMapper.getLists(pageVo, queryWrapper));
        return pageVo;
    }


    /**
     * 请求全部
     * @param queryWrapper
     * @return
     */
    @Override
    public List<BannerVO> findAll(@Param(Constants.WRAPPER) Wrapper<Banner> queryWrapper) {
        return this.baseMapper.findAll(queryWrapper);
    }
}
