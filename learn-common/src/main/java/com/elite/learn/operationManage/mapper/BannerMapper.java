package com.elite.learn.operationManage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.operationManage.entity.Banner;
import com.elite.learn.operationManage.vo.BannerVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商城首页轮播图 Mapper 接口
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
public interface BannerMapper extends BaseMapper<Banner> {
    /*
     *
     * @author: leroy
     * @date 2021/11/30 11:43
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    BannerVO getInfo(String id);


    /*
     *
     * @author: leroy
     * @date 2021/11/30 11:45
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<BannerVO> getLists(Page<BannerVO> pageVo, @Param(Constants.WRAPPER) Wrapper<Banner> queryWrapper);

    /**
     *
     * @author: leroy
     * @date 2021/11/30 11:45
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    List<BannerVO> findAll( @Param(Constants.WRAPPER) Wrapper<Banner> queryWrapper);

}
