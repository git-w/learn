package com.elite.learn.operationManage.bll;


import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.service.ICommonServiceBLL;
import com.elite.learn.operationManage.dto.BannerDTO;
import com.elite.learn.operationManage.entity.Banner;
import com.elite.learn.operationManage.query.BannerQuery;
import com.elite.learn.operationManage.vo.BannerVO;

public interface IBannerServiceBLL extends ICommonServiceBLL<Banner, BannerDTO, BannerVO, BannerQuery> {


    /**
     * 多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    boolean updateMultiIsState(String operUserId, BaseParams params);


}
