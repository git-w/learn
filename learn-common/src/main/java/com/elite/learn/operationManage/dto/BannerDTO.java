package com.elite.learn.operationManage.dto;

import lombok.Data;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 商城首页轮播图
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
@Data
public class BannerDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @NotBlank(groups = Update.class,message = "id参数不能为空")
    private String id;

    /**
     * banner图名称
     */
    @NotBlank(message = "banner图名称，参数为空")
    private String bannerName;

    /**
     * 图片存放路径（短）
     */
    @NotBlank(message = "封面图，参数为空")
    private String imgPath;

    /**
     * 排序 数值越大越靠前
     */
    private Integer sort;

    /**
     * 开始显示时间
     */
    private Long startTime;

    /**
     * 结束显示时间
     */
    private Long endTime;

    /**
     * 跳转类型的id 或者图片跳转url
     */
    private String businessId;

    /**
     * 跳转类型：0：不跳转,1-跳转URL,2-客户列表,3-会员卡列表;4-视频详情;5-客户详情,6-会员卡详情;7-活动列表,8-活动详情,
     */
    private Integer Type;

    /**
     * 投放位置 0-首页
     */
    private Integer putSite;

    /**
     * 备注
     */
    private String memo;

    /**
     * 是否时间段显示 0是 1否
     */
    private Integer isTime;

    /**
     * 是否上线 0是 1否
     */
    private Integer isState;


    /**
     * 创建者ID（反馈者）
     */
    private String createOperatorid;

    /**
     * 创建者名称
     */
    private String createOperatorName;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者ID
     */
    private String updateOperatorId;

    /**
     * 更新者名称
     */
    private String updateOperatorName;

    /**
     * 更新时间
     */
    private Long updateTime;


    /**
     * 额外参数1
     */
    private String extraParams1;


}
