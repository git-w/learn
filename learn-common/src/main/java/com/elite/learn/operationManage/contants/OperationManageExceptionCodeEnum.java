package com.elite.learn.operationManage.contants;

public enum OperationManageExceptionCodeEnum {

    TypeParamsError(100001,"类型参数为空"),
    ImgPathIsParamsError(100001, "图片短路径为空"),
    SaveSuccess(100000, "保存成功"),
    SelectSuccess(100000, "查询成功"),
    DeleteSuccess(100000, "删除成功"),
    UpdateSuccess(100000, "修改成功"),
    IdParamsError(100001, "id编号，参数为空"),

    SaveFail(100005, "保存失败"),
    DeleteFail(100005, "删除失败"),
    SelectFail(100005, "查询失败"),
    UpdateFail(100005, "修改失败");

    public Integer code;
    public String msg;

    private OperationManageExceptionCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
