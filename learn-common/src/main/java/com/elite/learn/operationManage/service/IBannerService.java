package com.elite.learn.operationManage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elite.learn.operationManage.entity.Banner;
import com.elite.learn.operationManage.vo.BannerVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商城首页轮播图 服务类
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
public interface IBannerService extends IService<Banner> {


    /**
     *
     * @author: leroy
     * @date 2021/11/30 11:41
     * @param id
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    BannerVO getInfo(String id);


    /**
     *
     * @author: leroy
     * @date 2021/11/30 11:42
     * @param pageVo
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    IPage<BannerVO> getLists(Page<BannerVO> pageVo, @Param(Constants.WRAPPER) Wrapper<Banner> queryWrapper);


    /**
     *
     * @author: leroy
     * @date 2021/11/30 11:42
     * @param queryWrapper
     * @return null
     * @update
     * @updateTime
     * 全部
     */
    List<BannerVO> findAll(@Param(Constants.WRAPPER) Wrapper<Banner> queryWrapper);

}
