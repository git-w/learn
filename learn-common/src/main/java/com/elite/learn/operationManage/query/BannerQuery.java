package com.elite.learn.operationManage.query;

import com.elite.learn.common.core.params.PageParams;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 商城首页轮播图
 * </p>
 *
 * @author: leroy
 * @since 2021-12-07
 */
@Data
public class BannerQuery extends PageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 是否时间段显示 0是 1否
     */
    private Integer isTime;
    /**
     * banner图名称
     */
    private String bannerName;


    /**
     * 开始显示时间
     */
    private Long startTime;

    /**
     * 结束显示时间
     */
    private Long endTime;

    /**
     * 跳转类型的id 或者图片跳转url
     */
    private String businessId;

    /**
     * 跳转类型的id 或者图片跳转url
     */
    private Integer IsState;
    /**
     * 投放位置 0-首页
     */
    private Integer putSite;

}
