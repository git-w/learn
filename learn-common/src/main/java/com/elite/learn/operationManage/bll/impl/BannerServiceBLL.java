package com.elite.learn.operationManage.bll.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.type.DeleteType;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.utils.date.CommonHelperUtil;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.operationManage.bll.IBannerServiceBLL;
import com.elite.learn.operationManage.contants.OperationManageExceptionCodeEnum;
import com.elite.learn.operationManage.dto.BannerDTO;
import com.elite.learn.operationManage.entity.Banner;
import com.elite.learn.operationManage.query.BannerQuery;
import com.elite.learn.operationManage.service.IBannerService;
import com.elite.learn.operationManage.vo.BannerVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 会员卡信息表
 * @Title: InfoBannerServiceBLL
 * @Package com.elite.learn.userManage.bll.impl
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/29 14:12
 */
@Service
public class BannerServiceBLL implements IBannerServiceBLL {

    @Resource
    private IBannerService service;


    /*
     *
     * @author: leroy
     * @date 2021/11/29 15:30
     * @param null
     * @return null
     * @update
     * @updateTime
     * 获取对象id
     */
    @Override
    public Banner get(String id) {
        return this.service.getById(id);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 15:32
     * @param null
     * @return null
     * @update
     * @updateTime
     * 详情
     */
    @Override
    public BannerVO getInfo(String id) {
        BannerVO info = this.service.getInfo(id);
        if (Objects.nonNull(info) && StringUtil.notBlank(info.getImgPath())) {
            //拼接主图长地址
            info.setImgPathUrl(FileUploadUtil.getImgRootPath() + info.getImgPath());
        }
        return info;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 15:34
     * @param null
     * @return null
     * @update
     * @updateTime
     * 添加
     */
    @Override
    public String save(String operUserId, BannerDTO bean) {
        //创建实体
        Banner info = new Banner();
        //创建用户生成的id
        info.setId(IdUtils.simpleUUID());
        // 设置banner图名称
        info.setBannerName(bean.getBannerName());
        // 设置排序
        info.setSort(bean.getSort());
        // 设置开始时间
        info.setStartTime(bean.getStartTime());
        // 设置结束时间
        info.setEndTime(bean.getEndTime());
        // 设置状态,默认上线
        if (bean.getIsState() != null) {
            info.setIsState(bean.getIsState());
        } else {
            info.setIsState(0);
        }
        // 设置删除状态
        info.setIsDelete(DeleteType.NOTDELETE.getCode());
        // 如果选择的在线时间不在当前时间内则自动设置为下线状态
        if (1 != bean.getIsTime()) {
            if (CommonHelperUtil.isEffectiveDate(new Date(), CommonHelperUtil.getDateLong(bean.getStartTime()), CommonHelperUtil.getDateLong(bean.getEndTime()))) {
                info.setIsState(0);
            } else {
                info.setIsState(1);
            }
        }
        // 设置banner图路径
        if (StringUtil.notBlank(bean.getImgPath())) {
            info.setImgPath(bean.getImgPath());
        } else {
            throw new CommonException(OperationManageExceptionCodeEnum.ImgPathIsParamsError.code,
                    OperationManageExceptionCodeEnum.ImgPathIsParamsError.msg);
        }
        // 设置跳转类型
        if (bean.getType() != null) {
            info.setType(bean.getType());
        } else {
            throw new CommonException(OperationManageExceptionCodeEnum.TypeParamsError.code,
                    OperationManageExceptionCodeEnum.TypeParamsError.msg);
        }
        // 投放位置
        info.setPutSite(bean.getPutSite());
        // 设置跳转类型的id 或者图片跳转url
        info.setBusinessId(bean.getBusinessId());

        info.setMemo(bean.getMemo());
        // 设置创建者信息
        info.setCreateOperatorid(operUserId);
        info.setCreateOperatorName(bean.getCreateOperatorName());
        info.setCreateTime(System.currentTimeMillis());
        //是否时间段
        info.setIsTime(bean.getIsTime());
        boolean flag = this.service.save(info);
        return flag ? info.getId() : null;
    }


    /**
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/11/29 16:00
     * @update
     * @updateTime 更新
     */
    @Override
    public boolean update(String operUserId, BannerDTO bean) {
        // 判断通过ID获取的对象是否为空
        Banner info = this.get(bean.getId());
        if (Objects.isNull(info)) {
            throw new CommonException(OperationManageExceptionCodeEnum.SelectFail.code,
                    OperationManageExceptionCodeEnum.SelectFail.msg);
        }

        //创建实体
        Banner infoNew = new Banner();
        //创建用户生成的id
        if (StringUtil.notBlank(bean.getId())) {
            infoNew.setId(bean.getId());
        }
        // 修改标题
        if (StringUtil.notBlank(bean.getBannerName())) {
            infoNew.setBannerName(bean.getBannerName());
        }
        // 修改排序
        if (Objects.nonNull(bean.getSort())) {
            infoNew.setSort(bean.getSort());
        }
        // 修改时间
        if (Objects.nonNull(bean.getStartTime())) {
            infoNew.setStartTime(bean.getStartTime());
        }
        if (Objects.nonNull(bean.getEndTime())) {
            infoNew.setEndTime(bean.getEndTime());
        }

        // 修改状态
        if (Objects.nonNull(bean.getIsState())) {
            infoNew.setIsState(bean.getIsState());
        }
        // 修改图片
        if (StringUtil.notBlank(bean.getImgPath())) {
            infoNew.setImgPath(bean.getImgPath());
        }
        // 修改跳转类型的id
        if (StringUtil.notBlank(bean.getBusinessId())) {
            infoNew.setBusinessId(bean.getBusinessId());
        }

        // 修改跳转类型
        if (Objects.nonNull(bean.getType())) {
            infoNew.setType(bean.getType());
        }
        // 投放位置
        if (Objects.nonNull(bean.getPutSite())) {
            infoNew.setPutSite(bean.getPutSite());
        }
        //修改备注
        if (Objects.nonNull(bean.getMemo())) {
            infoNew.setMemo(bean.getMemo());
        }
        //是否时间段显示
        if (Objects.nonNull(bean.getIsTime())) {
            infoNew.setIsTime(bean.getIsTime());
        }
        // 设置修改者信息
        infoNew.setUpdateOperatorId(operUserId);
        infoNew.setUpdateOperatorName(bean.getUpdateOperatorName());
        infoNew.setUpdateTime(System.currentTimeMillis());
        return this.service.updateById(infoNew);
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 16:17
     * @param null
     * @return null
     * @update
     * @updateTime
     * 删除
     */
    @Override
    public boolean remove(String operUserId, String ids) {
        Banner infoDel = null;
        boolean flag = true;
        //每个id已逗号分割
        String[] idsArr = ids.split(",");
        //遍历id并判断是否为空
        for (String id : idsArr) {
            Banner info = this.get(id);
            if (Objects.isNull(info)) {
                continue;
            }
            infoDel = new Banner();
            //设置删除状态
            infoDel.setId(info.getId());
            infoDel.setDeleteOperatorId(operUserId);
            infoDel.setDeleteOperatorName(info.getDeleteOperatorName());
            infoDel.setDeleteTime(System.currentTimeMillis());
            //设置状态为删除
            infoDel.setIsDelete(DeleteType.ISDELETE.getCode());

            boolean result = this.service.updateById(infoDel);
            if (!result) {
                flag = false;
            }
            if (!flag) {
                //修改失败
                throw new CommonException(OperationManageExceptionCodeEnum.SelectFail.code,
                        OperationManageExceptionCodeEnum.SelectFail.msg);
            }
        }

        return flag;
    }



    /**
     * 全部
     *
     * @param params
     * @return
     */
    @Override
    public List<BannerVO> findAll(BannerQuery params) {
        QueryWrapper<Banner> queryWrapper = new QueryWrapper<Banner>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        if (Objects.nonNull(params.getIsState())) {
            queryWrapper.eq("is_state", params.getIsState());
        }
        if (Objects.nonNull(params.getPutSite())) {
            queryWrapper.eq("put_site", params.getPutSite());
        }
        if (Objects.nonNull(params.getIsTime())) {
            queryWrapper.eq("is_time", params.getIsTime());
        }
        queryWrapper.orderByDesc("sort,create_time");
        List<BannerVO> list = service.findAll(queryWrapper);
       //有查询结果
        if (null != list && list.size() > 0) {
            for (BannerVO info : list) {
                if (StringUtil.notBlank(info.getImgPath())) {
                    //拼接主图长地址
                    info.setImgPathUrl(FileUploadUtil.getImgRootPath() + info.getImgPath());
                }

            }
        }
        return list;
    }


    /*
     *
     * @author: leroy
     * @date 2021/11/29 16:21
     * @param null
     * @return null
     * @update
     * @updateTime
     * 分页
     */
    @Override
    public PageResult<BannerVO> getList(BannerQuery params) {
        QueryWrapper<Banner> queryWrapper = new QueryWrapper<Banner>();
        queryWrapper.eq("is_delete", DeleteType.NOTDELETE.getCode());
        if (Objects.nonNull(params.getIsState())) {
            queryWrapper.eq("is_state", params.getIsState());
        }
        queryWrapper.orderByDesc("create_time");
        Page<BannerVO> pageVo = new Page<>(params.getPageNum(), params.getPageSize());
        IPage<BannerVO> page = service.getLists(pageVo, queryWrapper);
        //有查询结果
        if (null != page.getRecords() && page.getRecords().size() > 0) {
            for (BannerVO info : page.getRecords()) {
                if (StringUtil.notBlank(info.getImgPath())) {
                    //拼接主图长地址
                    info.setImgPathUrl(FileUploadUtil.getImgRootPath() + info.getImgPath());
                }

            }
        }

        return new PageResult<>(page);
    }


    /**
     * 会员卡多选修改状态
     *
     * @param operUserId
     * @param params
     * @return
     */
    @Override
    public boolean updateMultiIsState(String operUserId, BaseParams params) {
        boolean flag = true;
        String ids = params.getIds();
        String[] idArr = ids.split(",");
        Banner infoNew = null;
        for (String id : idArr) {
            Banner info = this.get(id.trim());
            if (Objects.isNull(info)) {
                continue;
            }
            infoNew = new Banner();
            infoNew.setId(id.trim());
            infoNew.setIsState(params.getIsState());
            infoNew.setUpdateOperatorId(operUserId);
            infoNew.setUpdateOperatorName(params.getUpdateOperatorName());
            infoNew.setUpdateTime(System.currentTimeMillis());
            boolean result = this.service.updateById(infoNew);
            if (!result) {
                flag = false;
            }
        }
        // 判断 菜单是否删除成功 如果删除成功直接抛出异常
        if (!flag) {
            throw new CommonException(OperationManageExceptionCodeEnum.UpdateFail.code,
                    OperationManageExceptionCodeEnum.UpdateFail.msg);
        }
        return flag;
    }


}