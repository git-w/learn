package com.elite.learn.common.utils.http.http;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * @Title :EasySendHttpUtils.java
 * @Package: com.elite.learn.common.utils.httpAndAddress.http
 * @Description:
 * @author: leroy
 * @data: 2020/5/6 17:08
 */
public class EasySendHttpUtil {

    private static class SingleTonHolder{

        private static RestTemplate restTemplate = new RestTemplate();
    }

    private EasySendHttpUtil() {}

    /**
     * 单例实例
     * @return
     */
    public static RestTemplate getInstance(){

        return SingleTonHolder.restTemplate;
    }

    /**
     * post请求 不加密
     * @param requestUrl 访问连接
     * @param data 传输参数json
     * @return
     */
    public static String post(String requestUrl, String data ){
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Content-Type","application/json; charset=utf-8");

            HttpEntity<String> httpEntity = new HttpEntity<>(data, httpHeaders);
            return EasySendHttpUtil.getInstance().postForObject(requestUrl,httpEntity,String.class);
        } catch (Exception e) {
            e.printStackTrace();
            return "请求失败";
        }
    }

    /**
     * get根据url获取对象
     */
    public static String get(String url) {
        try {
            return EasySendHttpUtil.getInstance().getForObject(url, String.class, new Object[] {});
        } catch (RestClientException e) {
            e.printStackTrace();
            return "请求失败";
        }
    }
}
