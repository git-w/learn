package com.elite.learn.common.utils.encrypt.des;

public class DecryptionUserUtil {

	private String key; //钥匙
	private String appsercet; //密钥串
	private String enSercet; //加密密钥串
	private String userId;//用户id


	/**
	 * @param key 钥匙
	 * @param appsercet 密钥串
	 * @param enSercet 加密密钥串
	 */
	public DecryptionUserUtil(String key,String appsercet,String enSercet){
		this.key = key;
		this.appsercet = appsercet;
		this.enSercet = enSercet;
		init();
		//TODO 
	}


	/**
	 * AOYOU 验证
	 * @description  m
	 * @author: leroy
	 * @created 2014-6-17 下午07:20:27
	 */
	private void init(){
		
		if(key!=null && !"".equals(key) && enSercet!=null && !"".equals(enSercet)){
			DESEncryptUtil des = new DESEncryptUtil();
			try {
				String ser = des.decryption(enSercet, key);
				String[] serArry = ser.split(",");
				if(serArry != null && serArry.length > 0){

//					String uid = null;
//					if(CookieTool.getCookie(ServletActionContext.getRequest(), "hxnovo_ec_uid") != null){
						if(serArry[0] != null && !"".equals(serArry[0])){
							this.userId = new String(serArry[0]);
						}
//					}else{
//						this.userId = null;
//					}
					//
					
				}
			} catch (Exception e) {
//				e.printStackTrace();
			}
		}
	}


	/**
	 * 红星验证
	 * @description  
	 * @author: leroy
	 * @created 2014-6-17 下午07:20:12
	 */
	private void init2(){/*
		
		if(key!=null && !"".equals(key) && enSercet!=null && !"".equals(enSercet)){
			DESEncryptUtil des = new DESEncryptUtil();
			try {

				String ser = des.decryption(enSercet, key);
				String[] serArry = ser.split(",");
				
				String uid = null;
				if(CookieTool.getCookie(ServletActionContext.getRequest(), "leroy_hdt_uid") != null){
					uid = CookieTool.getCookie(ServletActionContext.getRequest(), "leroy_hdt_uid");

					if(serArry != null && serArry.length == 3){
						if(serArry[0] != null && !"".equals(serArry[0])){
							this.userId = new Integer(serArry[0]);
						}

						if(serArry[1] != null && !"".equals(serArry[1])){
							this.shopId = new Integer(serArry[1]);
						}
						if(appsercet != null && !"".equals(appsercet)){
							if(appsercet.equals(serArry[2])){
								this.isSercetTrue = true;
							}else{
								this.isSercetTrue = false;
							}
						}
					}
					
//					System.out.println("userId-Y-"+uid);
					this.isSercetTrue = true;
				}else{
//					System.out.println("userId-N-"+uid);
					this.isSercetTrue = false;
					
//////					taobao 平台需要放开以下注释
//					if(serArry != null && serArry.length == 3){
//						if(serArry[0] != null && !"".equals(serArry[0])){
//							this.userId = new Integer(serArry[0]);
//						}
//
//						if(serArry[1] != null && !"".equals(serArry[1])){
//							this.shopId = new Integer(serArry[1]);
//						}
//						if(appsercet != null && !"".equals(appsercet)){
//							if(appsercet.equals(serArry[2])){
//								this.isSercetTrue = true;
//							}else{
//								this.isSercetTrue = false;
//							}
//						}
//					}
				}
			} catch (Exception e) {
//				e.printStackTrace();
			}
		}
	*/}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getAppsercet() {
		return appsercet;
	}

	public void setAppsercet(String appsercet) {
		this.appsercet = appsercet;
	}

	public String getEnSercet() {
		return enSercet;
	}

	public void setEnSercet(String enSercet) {
		this.enSercet = enSercet;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
