package com.elite.learn.common.core.exception;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import lombok.Data;

/**
 * @version V1.0 
 * @author: leroy  
 * @ClassName: Exception 
 * @Description: 公共异常处理
 * @date 2020年3月12日 下午10:56:55 
 */
@Data
public class CommonException extends RuntimeException {
    /**
     * 状态码
     */
    private Integer errCode = BasePojectExceptionCodeEnum.Unknown_Exception.code;

    public CommonException() {
        super(BasePojectExceptionCodeEnum.Unknown_Exception.msg);
    }

    public CommonException(Integer code, String message) {
        super(message);
        this.errCode = code;
    }

    public CommonException(String message) {
        super(message);
        this.errCode = BasePojectExceptionCodeEnum.ERROR_CODE.code;
    }
}
