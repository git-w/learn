package com.elite.learn.common.utils.string;

import java.util.Objects;

public class ReplaceStringUtil {


    /**
     * 定义所有常量
     */
    public static final String EMPTY = "";
    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
 
    /**
     * @Description 字符串向左截取
     * @author ShengLiu
     * @date 2018/7/4
     * @param str
     * @param len
     * @return java.lang.String
     */
    public static String left(String str, int len) {
        if (Objects.isNull(str)) {
            return null;
        }
        if (len < ZERO) {
            return EMPTY;
        }
        if (str.length() <= len) {
            return str;
        }
        return str.substring(ZERO, len);
 
    }


    /**
     * @Description 字符串向右截取
     * @author ShengLiu
     * @date 2018/7/4
     * @param str
     * @param len
     * @return java.lang.String
     */
    public static String right(String str, int len) {
        if (str==null) {
            return null;
        }
        if (len < ZERO) {
            return EMPTY;
        }
        if (str.length() <= len) {
            return str;
        }
        return str.substring(str.length() - len);
    }


    /**
     * @Description 根据不同名字的长度返回不同的显示数据
     * @author ShengLiu
     * @date 2018/7/4
     * @param str
     * @return java.lang.String
     */
    public static String checkNameLength(String str){
        if(str==null){
            return null;
        }
        if(str.length() == ONE) {
            return str;
        }else if(str.length() == TWO){
            return ReplaceStringUtil.left(str, ONE)+ "*" ;
        }else if(str.length() == THREE){
            return ReplaceStringUtil.left(str, ONE) + "*" + ReplaceStringUtil.right(str, ONE);
        }else if(str.length() == FOUR){
            return ReplaceStringUtil.left(str, ONE) + "**" + ReplaceStringUtil.right(str, ONE);
        }
        return str;
    }
 
    /**
     * 测试
     */
    public static void main(String[] args) {
        System.out.println("名字: " + ReplaceStringUtil.checkNameLength("海"));
        System.out.println("名字: " + ReplaceStringUtil.checkNameLength("{贼王"));
        System.out.println("名字: " + ReplaceStringUtil.checkNameLength("海贼王"));
        System.out.println("名字: " + ReplaceStringUtil.checkNameLength("大海贼王"));
        System.out.println("手机号: " + ReplaceStringUtil.left("15838883888", THREE) + "*****" + ReplaceStringUtil.right("15838883888", FOUR));
        System.out.println("信用卡号16位: " + ReplaceStringUtil.left("1234567891011121", FOUR) + "*****" + ReplaceStringUtil.right("1234567891011121", FOUR));
        System.out.println("银行卡号19位: " + ReplaceStringUtil.left("1234567891011121314", FOUR) + "*****" + ReplaceStringUtil.right("1234567891011121314", FOUR));


}
}