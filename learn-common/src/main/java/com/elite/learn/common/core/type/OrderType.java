package com.elite.learn.common.core.type;

/**
 * @author wei-pc
 */
public enum OrderType {
    /**
     * 0会员卡  1活动
     * 订单类型
     */
    VIP(0, "会员卡"),
    ACTIVITY(1, "活动"),
    /**
     * 订单状态 0-取消订单 1-未支付/待支付 2-已支付/待确认使用、3-已核销/已使用/完成
     * 4-申请退款 5-退款中， 6-退款完成 7-超时未支付取消的已关闭
     */

    CANCLE(0, "取消订单"),
    UNPAY(1, "未支付/待支付"),
    PAY(2, "已支付/待确认使用"),
    WAITSEND(3, "已核销/已使用/完成"),
    RETURNING(4, "申请退款"),
    RETURN(5, "退款中"),
    WRITEOFF(6, "退款完成"),
    WAITUSE(7, "超时未支付取消的已关闭"),
    COMPLETE(5, "完成"),

    /**
     * 付款方式 0.支付宝、1.微信、2.现金、3.钱包余额、4.银行卡、5.会员卡
     */
    ZhiFuBao(0, "支付宝"),
    WeiXin(1, "微信"),
    XianJin(2, "现金"),
    Yue(3, "钱包"),
    Bank(4, "银行卡"),
    Mianfei(6, "免费"),
    MemberCard(5, "会员卡");

    private int type;
    private String note;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    /**
     * 初始化
     *
     * @param type
     * @param note
     */
    OrderType(int type, String note) {
        this.type = type;
        this.note = note;
    }
}
