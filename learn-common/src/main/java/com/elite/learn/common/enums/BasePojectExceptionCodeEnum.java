package com.elite.learn.common.enums;

public enum BasePojectExceptionCodeEnum {

    SUCCESS(100000, "请求成功"),
    LOGINERROR(-1, "用户未登录"),
    ERROR_CODE(100001, "登录失败"),


    Unknown_Exception(-1, "未知异常"),
    Insufficient_IAuthority(403, "用户没有权限"),
    USER_NOT_FOUND(100001, "没有找到此用户"),
    USERNAME_NOT_BLANK(100002, "用户名不能为空"),
    USERNAME_EXIST(100003, "用户名已经存在"),
    USERTYPE_ERROR(100031, "用户类型不正确"),
    RoleIdParamsError(100009, "该管理员权限不足"),
    DEVICE_ID_EMPTY(100052, "设备ID:deviceId不能为空"),
    DELETE_CONNECT_ERROR(100053, "删除connect出错"),


    SaveSuccess(100000, "保存成功"),

    FileUploadingSuccess(100000, "上传成功"),
    EnrollSuccess(100000, "注册成功"),
    SelectSuccess(100000, "查询成功"),
    DeleteSuccess(100000, "删除成功"),
    TypeError(100001, "类型为空"),
    UpdateSuccess(100000, "修改成功"),
    CheckInSuccess(100000, "签到成功"),
    OperatingSuccess(100000, "操作成功！"),
    FileUploadSuccess(100000, "文件上传成功"),
    TaskTypeError(100000, "参数为空"),

    FileUploadingSectionError(100000, "部分上传失败"),
    TokenDataReadError(100001, "token数据解析异常,请检查code是否过期！"),
    FileNameError(100001, "文件名称异常！"),
    TokenIsNull(100001, "token为空！"),
    TokenCheckIsNull(100001, "接口code校验为空！"),
    JobCodeNotFound(100001, "工号未找到"),
    NotSininRepeat(100001, "不可重复签到"),
    NotDtaSin(100003, "查询无数据"),
    FileUploadFail(100001, "文件上传失败"),
    TimeDontExceed(100001, "逾期时间不合法"),
    TextMessageSendingFail(100001, "短信发送失败"),
    FileFormatError(100001, "文件格式错误！"),
    LoginNameParamsError(100001, "登录名参数为空"),
    pwdParamsError(100001, "密码参数为空"),
    ParamsError(100001, "参数为空"),
    IDParamsError(100001, "id参数为空"),
    ParamsErrorNum(100001, "参数不合法"),
    ClassifyIDParamsError(100001, "分类参数为空"),
    DataIDParamsError(100001, "安全培训数据id参数为空"),
    DingDingResponseStrIsNull(100001, "钉钉响应体为空"),

    FileUploadingFfailed(100001, "上传错误"),
    FileParamsError(100001, "文件为空"),
    UnitFileFormatError(100001, "文件格式错误"),
    ResourcesIDParamsError(100001, "资源id参数为空"),
    IdError(100001, "id参数为空"),
    IdHaveError(100001, "id参数错误"),
    RCETaskTypeError(100001, "任务类型不可更改"),
    WebsiteConfigurationError(100001, "没有数据，请先添加"),
    LoginPasswordParamsError(100001, "密码参数为空"),
    UpdateNULLError(100001, "请输入修改内容的正确参数"),
    TypeMobError(100001, "协议类型为空"),
    SaveFail(100005, "保存失败"),
    OperationError(100005, "操作失败！"),
    DeleteFail(100005, "删除失败"),
    SelectFail(100005, "查询失败"),

    RoleNotEnableFail(100010,"角色未启用"),
    DataError(100001, "数据异常"),
    FinishSuccess(100000, "提交成功"),
    FinishFail(100005, "提交失败"),
    CloseSuccess(100000, "关闭成功"),
    CloseFail(100005, "关闭失败"),
    UpdateFail(100005, "修改失败"),

    AuthorizationFail(100005, "授权失败"),
    ServiceException(300404, "服务异常");
    public Integer code;
    public String msg;

    BasePojectExceptionCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
