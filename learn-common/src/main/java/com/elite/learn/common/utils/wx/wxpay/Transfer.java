/**
 * 
 */
package com.elite.learn.common.utils.wx.wxpay;

/**
 * @description:
 * @author sun.li
 * @data 2017-5-24下午05:58:48
 */
public class Transfer {
	private String mch_appid;// 公众号id
	private String mchid;// 商户号
	private String device_info;// 设备号 非必填
	private String nonce_str;// 随机字符
	private String sign;// 签名
	private String partner_trade_no;// 商户订单号
	private String openid;// 用户openid
	private String check_name;// 校验用户姓名选项 收款用户真实姓名。如果check_name设置为FORCE_CHECK，则必填用户真实姓名
	private String re_user_name;// 收款用户姓名 非必填
	private int amount;// 企业付款金额，单位为分
    private String desc;// 企业付款描述信息 
	private String spbill_create_ip;// Ip地址
	/**
	 * @return the mch_appid
	 */
	public String getMch_appid() {
		return mch_appid;
	}
	/**
	 * @param mchAppid the mch_appid to set
	 */
	public void setMch_appid(String mchAppid) {
		mch_appid = mchAppid;
	}
	/**
	 * @return the mchid
	 */
	public String getMchid() {
		return mchid;
	}
	/**
	 * @param mchid the mchid to set
	 */
	public void setMchid(String mchid) {
		this.mchid = mchid;
	}
	/**
	 * @return the device_info
	 */
	public String getDevice_info() {
		return device_info;
	}
	/**
	 * @param deviceInfo the device_info to set
	 */
	public void setDevice_info(String deviceInfo) {
		device_info = deviceInfo;
	}
	/**
	 * @return the nonce_str
	 */
	public String getNonce_str() {
		return nonce_str;
	}
	/**
	 * @param nonceStr the nonce_str to set
	 */
	public void setNonce_str(String nonceStr) {
		nonce_str = nonceStr;
	}
	/**
	 * @return the sign
	 */
	public String getSign() {
		return sign;
	}
	/**
	 * @param sign the sign to set
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}
	/**
	 * @return the partner_trade_no
	 */
	public String getPartner_trade_no() {
		return partner_trade_no;
	}
	/**
	 * @param partnerTradeNo the partner_trade_no to set
	 */
	public void setPartner_trade_no(String partnerTradeNo) {
		partner_trade_no = partnerTradeNo;
	}
	/**
	 * @return the openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * @param openid the openid to set
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * @return the check_name
	 */
	public String getCheck_name() {
		return check_name;
	}
	/**
	 * @param checkName the check_name to set
	 */
	public void setCheck_name(String checkName) {
		check_name = checkName;
	}
	/**
	 * @return the re_user_name
	 */
	public String getRe_user_name() {
		return re_user_name;
	}
	/**
	 * @param reUserName the re_user_name to set
	 */
	public void setRe_user_name(String reUserName) {
		re_user_name = reUserName;
	}
	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}
	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	/**
	 * @return the spbill_create_ip
	 */
	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}
	/**
	 * @param spbillCreateIp the spbill_create_ip to set
	 */
	public void setSpbill_create_ip(String spbillCreateIp) {
		spbill_create_ip = spbillCreateIp;
	}
    
}
