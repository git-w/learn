package com.elite.learn.common.utils.id;

import com.elite.learn.common.utils.string.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description: 身份证工具类
 * @Title: IDCardUtil
 * @Package com.elite.learn.common.utils.IdUtils
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2022/1/8 15:54
 */
public class IDCardUtil {


    /**
     * 15位身份证号
     */
    private static final Integer FIFTEEN_ID_CARD = 15;

    /**
     * 18位身份证号
     */
    private static final Integer EIGHTEEN_ID_CARD = 18;

    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 根据身份证号获取性别
     *
     * @param IDCard
     * @return
     */
    public static Integer getSex(String IDCard) {
        //用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
        Integer sex = 0;
        if (StringUtils.isNotBlank(IDCard)) {
            //15位身份证
            if (IDCard.length() == FIFTEEN_ID_CARD) {
                //判断是男是女   男为奇数 女为偶数
                if (Integer.parseInt(IDCard.substring(14, 15)) % 2 == 0) {
                    sex = 2;
                } else {
                    sex = 1;
                }
                //18位身份证
            } else if (IDCard.length() == EIGHTEEN_ID_CARD) {
                //判断是男是女   男为奇数 女为偶数
                if (Integer.parseInt(IDCard.substring(16, 17)) % 2 == 0) {
                    sex = 2;
                } else {
                    sex = 1;
                }
            }
        }
        return sex;
    }

    /**
     * 根据身份证获取年龄
     **/
    private static Integer getAge(String IDCard) {
        Integer age = 0;
        Date date = new Date();
        if (StringUtils.isNotBlank(IDCard)) {
            //15位身份证号
            if (IDCard.length() == FIFTEEN_ID_CARD) {
                //身份证上的年份(15位身份证为1980年前的)
                String uyear = "19" + IDCard.substring(6, 8);
                //身份证上的月份
                String uyue = IDCard.substring(8, 10);
                //当前年份
                String fyear = format.format(date).substring(0, 4);
                //当前月份
                String fyue = format.format(date).substring(5, 7);
                if (Integer.parseInt(uyue) <= Integer.parseInt(fyue)) {
                    age = Integer.parseInt(fyear) - Integer.parseInt(uyear) + 1;
                    //当前用户还没过生日
                } else {
                    age = Integer.parseInt(fyear) - Integer.parseInt(uyear);
                }

                //18位身份证号
            } else if (IDCard.length() == EIGHTEEN_ID_CARD) {
                //身份证上的年份
                String year = IDCard.substring(6).substring(0, 4);
                //身份证上的月份
                String yue = IDCard.substring(10).substring(0, 2);
                //当前年份
                String fyear = format.format(date).substring(0, 4);
                //当前月份
                String fyue = format.format(date).substring(5, 7);
                //当前月份大于用户出生的月份表示已过生日
                if (Integer.parseInt(yue) <= Integer.parseInt(fyear)) {
                    age = Integer.parseInt(fyear) - Integer.parseInt(year) + 1;
                }
                //当前用户还没过生日
                else {
                    age = Integer.parseInt(fyear) - Integer.parseInt(year);
                }
            }
        }
        return age;
    }


    /**
     *获取出生日期格式 yyyy年MM月dd日
     */
    public static  String getBirthday(String IDCard){
        String birthday="";
        String year="";
        String month="";
        String day="";
        if (StringUtils.isNotBlank(IDCard)){
            //15位身份证号
            if (IDCard.length()==FIFTEEN_ID_CARD){
                // 身份证上的年份(15位身份证为1980年前的)
                year ="19"+IDCard.substring(6,8);
                //身份证上的月份
                month = IDCard.substring(8,10);
                //身份证上的日期
                day = IDCard.substring(10,12);
                //18位身份证号
            }else  if (IDCard.length()==EIGHTEEN_ID_CARD){
                //身份证上的年份
                year  =IDCard.substring(6).substring(0,4);
                //身份证上的月份
                month = IDCard.substring(10).substring(0,2);
                //身份证上的日期
                day=IDCard.substring(12).substring(0,2);
            }
            birthday =year+"-"+month+"-"+day;
        }
        return birthday;
    }

    public static void main(String[] args) {


        System.out.println("===================="+ getBirthday("231223199402250038"));
    }
}