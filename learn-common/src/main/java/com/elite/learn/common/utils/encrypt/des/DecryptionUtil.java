package com.elite.learn.common.utils.encrypt.des;


/**
 * @description: 针对的加密方式来解密 userId+ , +密钥串
 * @version 1.0
 * @company：leroy
 * @createDate 2013-3-27;下午06:07:47
 */
public class DecryptionUtil {

	private String key; //钥匙
	private String appsercet; //密钥串
	private String enSercet; //加密密钥串
	private String token;//解密后的uuid
	private String time;//解密后的时间戳
	private String userId;//用户id
	private String username;//用户名称
	private String otherUid; // 第三方用户id
	/**
	 * @param key 钥匙
	 * @param appsercet 密钥串
	 * @param enSercet 加密密钥串
	 */
	public DecryptionUtil(String key,String appsercet,String enSercet){
		this.key = key;
		this.appsercet = appsercet;
		this.enSercet = enSercet;
		init();
		//TODO 
	}

	/**
	 * AOYOU 验证
	 * @description  m
	 * @author: leroy
	 * @created 2014-6-17 下午07:20:27
	 */
	private void init(){

		if(key!=null && !"".equals(key) && enSercet!=null && !"".equals(enSercet)){
			DESEncryptUtil des = new DESEncryptUtil();
			try {
				String ser = des.decryption(enSercet, key);
				String[] serArry = ser.split(",");
				if(serArry != null && serArry.length > 1){

//					String uid = null;
//					if(CookieTool.getCookie(ServletActionContext.getRequest(), "hxnovo_ec_uid") != null){
						if(serArry.length>1&&serArry[0] != null && !"".equals(serArry[0])){
							this.token = new String(serArry[0]);
						}
						if(serArry.length>=2&&serArry[1] != null && !"".equals(serArry[1])){
							this.time = new String(serArry[1]);
						}
						if(serArry.length>=3&&serArry[2] != null && !"".equals(serArry[2])){
							this.userId = new String(serArry[2]);
						}
						if(serArry.length>=4&&serArry[3] != null && !"".equals(serArry[3])){
							this.username = new String(serArry[3]);
						}
						if(serArry.length>=5&&serArry[4] != null && !"".equals(serArry[4])){
							this.otherUid = new String(serArry[4]);
						}
//					}else{
//						this.userId = null;
//					}
					//
					
				}
			} catch (Exception e) {
//				e.printStackTrace();
			}
		}
	}

	/**
	 * 红星验证
	 * @description  
	 * @author: leroy
	 * @created 2014-6-17 下午07:20:12
	 */
	private void init2(){/*
		
		if(key!=null && !"".equals(key) && enSercet!=null && !"".equals(enSercet)){
			DESEncryptUtil des = new DESEncryptUtil();
			try {

				String ser = des.decryption(enSercet, key);
				String[] serArry = ser.split(",");
				
				String uid = null;
				if(CookieTool.getCookie(ServletActionContext.getRequest(), "leroy_hdt_uid") != null){
					uid = CookieTool.getCookie(ServletActionContext.getRequest(), "leroy_hdt_uid");

					if(serArry != null && serArry.length == 3){
						if(serArry[0] != null && !"".equals(serArry[0])){
							this.userId = new Integer(serArry[0]);
						}

						if(serArry[1] != null && !"".equals(serArry[1])){
							this.shopId = new Integer(serArry[1]);
						}
						if(appsercet != null && !"".equals(appsercet)){
							if(appsercet.equals(serArry[2])){
								this.isSercetTrue = true;
							}else{
								this.isSercetTrue = false;
							}
						}
					}
					
//					System.out.println("userId-Y-"+uid);
					this.isSercetTrue = true;
				}else{
//					System.out.println("userId-N-"+uid);
					this.isSercetTrue = false;
					
//////					taobao 平台需要放开以下注释
//					if(serArry != null && serArry.length == 3){
//						if(serArry[0] != null && !"".equals(serArry[0])){
//							this.userId = new Integer(serArry[0]);
//						}
//
//						if(serArry[1] != null && !"".equals(serArry[1])){
//							this.shopId = new Integer(serArry[1]);
//						}
//						if(appsercet != null && !"".equals(appsercet)){
//							if(appsercet.equals(serArry[2])){
//								this.isSercetTrue = true;
//							}else{
//								this.isSercetTrue = false;
//							}
//						}
//					}
				}
			} catch (Exception e) {
//				e.printStackTrace();
			}
		}
	*/}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getAppsercet() {
		return appsercet;
	}

	public void setAppsercet(String appsercet) {
		this.appsercet = appsercet;
	}

	public String getEnSercet() {
		return enSercet;
	}

	public void setEnSercet(String enSercet) {
		this.enSercet = enSercet;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOtherUid() {
		return otherUid;
	}

	public void setOtherUid(String otherUid) {
		this.otherUid = otherUid;
	}
	
}
