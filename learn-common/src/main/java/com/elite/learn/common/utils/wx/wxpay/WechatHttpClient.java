package com.elite.learn.common.utils.wx.wxpay;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.elite.learn.common.utils.encrypt.md5.Md5Util;
import com.elite.learn.common.utils.http.http.PropertyUtil;


/**
 * 退款
 * @author: leroy
 * @date 2019年7月9日 下午3:46:08
 */
public class WechatHttpClient {
	
	/**
     * 统一下单生成签名
     * @author: leroy 2017年8月14日11:28:52
     */
    public static String createWechatPayOrderSign(WechatPay pay){
        StringBuffer sign = new StringBuffer();
        sign.append("appid=").append(pay.getAppid());
        sign.append("&body=").append(pay.getBody());
        sign.append("&mch_id=").append(pay.getMch_id());
        sign.append("&nonce_str=").append(pay.getNonce_str());
        sign.append("&notify_url=").append(pay.getNotify_url());
        /*leroy2017年8月16日14:32:34 修 S*/
        if("JSAPI".equals(pay.getTrade_type())){
        	sign.append("&openid=").append(pay.getOpenid());
        }
        /*leroy2017年8月16日14:32:34 修 E*/
        sign.append("&out_trade_no=").append(pay.getOut_trade_no());
        sign.append("&spbill_create_ip=").append(pay.getSpbill_create_ip());
        sign.append("&total_fee=").append(pay.getTotal_fee());
        sign.append("&trade_type=").append(pay.getTrade_type());
        sign.append("&key=").append(PropertyUtil.getValue("/wxpay.properties", "APIKEY"));
        System.out.println("计算签名参数"+sign.toString());
        return Md5Util.getInstance().getLongToken(sign.toString()).toUpperCase();
    }
    /**
     * 微信公众号内支付发起支付时参数的签名计算
     * @param pay
     * @return
     * @author: leroy 2017年8月16日12:03:06
     */
    public static String createWechatPayJsOrderSign(WechatPayJs pay){
        StringBuffer sign = new StringBuffer();
        sign.append("appId=").append(pay.getAppId());
        sign.append("&nonceStr=").append(pay.getNonceStr());
        sign.append("&package=").append(pay.getPay_package());
        sign.append("&signType=").append(pay.getSignType());
        sign.append("&timeStamp=").append(pay.getTimeStamp());
        sign.append("&key=").append(PropertyUtil.getValue("/wxpay.properties", "APIKEY"));
        System.out.println("jsapi签名参数："+sign.toString());
        return Md5Util.getInstance().getLongToken(sign.toString()).toUpperCase();
    }

    /**
     * 发送请求
     */
    public static String ssl(String url,String data){
        StringBuffer message = new StringBuffer();
        try {
            String mchId = PropertyUtil.getValue("/wxpay.properties", "MCHId");
            KeyStore keyStore  = KeyStore.getInstance("PKCS12");
            String certFilePath= PropertyUtil.getCertsPath("/wxpay.properties");
            FileInputStream instream = new FileInputStream(new File(certFilePath));
            keyStore.load(instream, mchId.toCharArray());
            SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, mchId.toCharArray()).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1" }, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
            CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
            HttpPost httpost = new HttpPost(url);	
            httpost.addHeader("Connection", "keep-alive");
            httpost.addHeader("Accept", "*/*");
            httpost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            httpost.addHeader("Host", "api.mch.weixin.qq.com");
            httpost.addHeader("X-Requested-With", "XMLHttpRequest");
            httpost.addHeader("Cache-Control", "max-age=0");
            httpost.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
            httpost.setEntity(new StringEntity(data, "UTF-8"));
            CloseableHttpResponse response = httpclient.execute(httpost);
            try {
                HttpEntity entity = response.getEntity();
              //  System.out.println(response.getStatusLine());
                
                if (entity != null) {
                  //  System.out.println("Response content length: " + entity.getContentLength());
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent(),"UTF-8"));
                   // System.out.println(bufferedReader);
                    String text;
                    while ((text = bufferedReader.readLine()) != null) {
                        message.append(text);
                    }
                    
                }
                EntityUtils.consume(entity);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                response.close();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } 
      //  System.out.println("111"+message.toString());
        return message.toString();
    }

    /**
     * 发起微信支付接口调用
     * url接口rul
     * data参数
     * @author: leroy 2017年8月14日11:48:07
     */
    public static String paySsl(String url,String data){
        StringBuffer message = new StringBuffer();
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httpost = new HttpPost(url);
            httpost.addHeader("Connection", "keep-alive");
            httpost.addHeader("Accept", "*/*");
            httpost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            httpost.addHeader("Host", "api.mch.weixin.qq.com");
            httpost.addHeader("X-Requested-With", "XMLHttpRequest");
            httpost.addHeader("Cache-Control", "max-age=0");
            httpost.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
            httpost.setEntity(new StringEntity(data, "UTF-8"));
           // System.out.println("executing request" + httpost.getRequestLine());
            CloseableHttpResponse response = httpclient.execute(httpost);
            try {
                HttpEntity entity = response.getEntity();
                System.out.println("----------------------------------------");
             //   System.out.println(response.getStatusLine());
                if (entity != null) {
                //    System.out.println("Response content length: " + entity.getContentLength());
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent(),"UTF-8"));
                    String text;
                    while ((text = bufferedReader.readLine()) != null) {
                        message.append(text);
                    }
                }
                EntityUtils.consume(entity);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                response.close();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } 
        return message.toString();
    }
    
    /**
     * 设置企业付款参数
     * @param checkName 是否校验用户名 
     * @param ReUserName 用户真实姓名 选填 若checkName 为FORCE_CHECK 则必填
     * @param deviceInfo 设备号 选填
     * @param desc 转账说明
     * @return
     */
    public static Transfer setTransfer(String checkName,String ReUserName,String deviceInfo, String desc ){
    	Transfer transfer = new Transfer();
    	try{
    		Date date = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    		String str = sdf.format(date);
    		transfer.setNonce_str(str);
    		transfer.setMchid(PropertyUtil.getValue("/wxpay.properties", "MCHId"));
    		transfer.setPartner_trade_no(PropertyUtil.getValue("/wxpay.properties", "MCHId") + str);
    		transfer.setMch_appid(PropertyUtil.getValue("/wxpay.properties", "APPID"));
    		transfer.setDesc(desc);
    		transfer.setCheck_name(checkName);
    		if("FORCE_CHECK".equals(checkName)){
    			transfer.setRe_user_name(ReUserName);
    		}
    		if(null!=deviceInfo){
    			transfer.setDevice_info(deviceInfo);
    		}
    		transfer.setSpbill_create_ip("8.8.8.8");
    	}catch (Exception e) {
			e.getStackTrace();
		}
        return transfer;
    }
    
    /**
     * 扫码支付微信支付发起参数类设置
     * @param body 交易内容
     * @param outTradeNo 订单号
     * @param totalFee 订单金额
     * @author: leroy 2017年8月14日11:28:24
     * @return
     */
    public static WechatPay setWechatPay(String body,String outTradeNo,int totalFee,String tradeType,String openId,String marking){
    	WechatPay pay = new WechatPay();
    	try{
    		Date date = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    		String str = sdf.format(date);
    		pay.setNonce_str(str);
    		pay.setMch_id(PropertyUtil.getValue("/wxpay.properties", "MCHId"));
    		pay.setBody(body); 
    		if(marking!=null && marking.equals("1")){
    			pay.setAppid(PropertyUtil.getValue("/wxpay.properties", "APPIDS"));
    		}else{
    			pay.setAppid(PropertyUtil.getValue("/wxpay.properties", "APPID"));
    		}
    		pay.setNotify_url(PropertyUtil.getValue("/wxpay.properties", "NOTIFYURL"));
    		pay.setTotal_fee(totalFee);
    		pay.setOut_trade_no(outTradeNo) ;
    	
    		/*leroy2017年8月16日14:27:45 修 S*/
    		if("JSAPI".equals(tradeType)){
    			pay.setOpenid(openId);
    		}
    		pay.setTrade_type(tradeType);
    		/*leroy2017年8月16日14:27:45 修 E*/
    		pay.setSpbill_create_ip("8.8.8.8");
    		
    	}catch (Exception e) {
			e.getStackTrace();
		}
        return pay;
    }
    /**
     * 
     * @param signType
     * @param pay_package
     * @return
     * @author: leroy 2017年8月16日12:04:38
     */
    public static WechatPayJs setWechatPayJs(String signType,String pay_package,String marking){
    	WechatPayJs pay = new WechatPayJs();
    	try{
    		Date date = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    		String str = sdf.format(date);
    		pay.setTimeStamp(String.valueOf(date.getTime()/1000));
    		if(marking!=null && marking.equals("1")){
    			pay.setAppId(PropertyUtil.getValue("/wxpay.properties", "APPIDS"));
    		}else{
    			pay.setAppId(PropertyUtil.getValue("/wxpay.properties", "APPID"));
    		}
    		pay.setNonceStr(str);
    		pay.setPay_package(pay_package);
    		pay.setSignType(signType) ;
    		pay.setPaySign(createWechatPayJsOrderSign(pay));
    		System.out.println("jsapi签名："+createWechatPayJsOrderSign(pay));
    	}catch (Exception e) {
			e.getStackTrace();
		}
        return pay;
    }
    /**
     * 设置发送红包的参数
     * @param actName
     * @param wishing
     * @param remark
     * @return
     */
    public static SendRedPack setRedPack(String actName,String wishing,String remark){
    	SendRedPack sendRedPack = new SendRedPack();
    	try{
    		Date date = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    		String str = sdf.format(date);
    		sendRedPack.setNonce_str(str);
    		sendRedPack.setMch_id(PropertyUtil.getValue("/wxpay.properties", "MCHId"));
    		sendRedPack.setMch_billno(PropertyUtil.getValue("/wxpay.properties", "MCHId") + str);
    		sendRedPack.setWxappid(PropertyUtil.getValue("/wxpay.properties", "APPID"));
    		sendRedPack.setSend_name(PropertyUtil.getValue("/wxpay.properties", "SENDNAME"));
    		sendRedPack.setTotal_num(1);
    		sendRedPack.setAct_name(actName);
    		sendRedPack.setWishing(wishing);
    		sendRedPack.setRemark(remark);
    		sendRedPack.setClient_ip("8.8.8.8");
    	}catch (Exception e) {
			e.getStackTrace();
		}
        return sendRedPack;
    }

    
    /**
     * 发起微信支付获取支付需要生成二维码的url
     * @param pay
     * @return
     * @throws Exception
     * @author: leroy 2017年8月14日11:40:38
     */
    public static Map<String, String> unifiedOrder(WechatPay pay) throws Exception{
        String sign = createWechatPayOrderSign(pay);
        System.out.println("签名："+sign);
        pay.setSign(sign);
     //   System.out.println(sign);
        XMLUtil xmlUtil= new XMLUtil();
        xmlUtil.xstream().alias("xml", pay.getClass());
        String xml = xmlUtil.xstream().toXML(pay);
        System.out.println("请求xml："+xml);
        String response = ssl(PropertyUtil.getValue("/wxpay.properties", "PAYURL"), xml);
        Map<String, String> map = xmlUtil.parseXml(response);
        return map;
    }
    
    
    
    /**
     * 获取外网IP地址
     * @return
     */
    public static String getV4IP(){
    	String ip = "";
    	String chinaz = "http://ip.chinaz.com";
    	
    	StringBuilder inputLine = new StringBuilder();
    	String read = "";
    	URL url = null;
    	HttpURLConnection urlConnection = null;
    	BufferedReader in = null;
    	try {
    		url = new URL(chinaz);
    		urlConnection = (HttpURLConnection) url.openConnection();
    	    in = new BufferedReader( new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
    		while((read=in.readLine())!=null){
    			inputLine.append(read+"\r\n");
    		}
    		//System.out.println(inputLine.toString());
    	} catch (MalformedURLException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}finally{
    		if(in!=null){
    			try {
    				in.close();
    			} catch (IOException e) {
    				//  Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
    	}
    	Pattern p = Pattern.compile("\\<dd class\\=\"fz24\">(.*?)\\<\\/dd>");
    	Matcher m = p.matcher(inputLine.toString());
    	if(m.find()){
    		String ipstr = m.group(1);
    		ip = ipstr;
    		//System.out.println(ipstr);
    	}
    	return ip;
    }

    /**
     * 小程序
     * @param body 交易内容
     * @param outTradeNo 订单号
     * @param totalFee 订单金额
     * @author: leroy 2017年8月14日11:28:24
     * @return
     */
    public static WechatPay setWxWechatPay(String body,String outTradeNo,int totalFee,String openId){
    	WechatPay pay = new WechatPay();
    	try{
    		Date date = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    		String str = sdf.format(date);
    		pay.setNonce_str(str);
    		pay.setMch_id(PropertyUtil.getValue("/wxpay.properties", "MCHId"));
    		pay.setBody(body); 
    		pay.setAppid(PropertyUtil.getValue("/wxpay.properties", "APPID"));
    		pay.setNotify_url(PropertyUtil.getValue("/wxpay.properties", "NOTIFYURL"));
    		pay.setTotal_fee(totalFee);
    		pay.setOut_trade_no(outTradeNo) ;
    	pay.setTrade_type("JSAPI");
    		pay.setOpenid(openId);
    		pay.setSpbill_create_ip("8.8.8.8");
    		
    	}catch (Exception e) {
			e.getStackTrace();
		}
        return pay;
    }
    /**
     * 
     * @param signType
     * @param pay_package
     * @return
     * @author 小程序 leroy2017年8月16日12:04:38
     */
   public static WechatPayJs setWxWechatPayJs(String signType,String pay_package){
    	WechatPayJs pay = new WechatPayJs();
    	try{
    		Date date = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    		String str = sdf.format(date);
    		pay.setTimeStamp(String.valueOf(date.getTime()/1000));
    		pay.setAppId(PropertyUtil.getValue("/wxpay.properties", "APPID"));
    		pay.setNonceStr(str);
    		pay.setPay_package(pay_package);
    		pay.setSignType(signType) ;
    		pay.setPaySign(createWechatPayJsOrderSign(pay));
    		System.out.println("jsapi签名："+createWechatPayJsOrderSign(pay));
    	}catch (Exception e) {
    		e.getStackTrace();
    	}
    	return pay;
    }
    
    /**
     * 小程序发起微信支付获取支付需要生成二维码的url
     * @param pay
     * @return
     * @throws Exception
     * @author: leroy 2017年8月14日11:40:38
     */
   public static Map<String, String> unifiedWxOrder(WechatPay pay) throws Exception{
        String sign = createWechatPayOrderSign(pay);
        System.out.println("签名："+sign);
        pay.setSign(sign);
        XMLUtil xmlUtil= new XMLUtil();
        xmlUtil.xstream().alias("xml", pay.getClass());
        String xml = xmlUtil.xstream().toXML(pay);
        System.out.println("请求xml："+xml);
        String response = wxSsl(PropertyUtil.getValue("/wxpay.properties", "PAYURL"), xml);
        if(response!=null){
        Map<String, String> map = xmlUtil.parseXml(response);
        return map;}
        return null;
    }
    
    /**
     * 发送请求
     */
	private static final String ENCODING = "UTF-8";
    public static String wxSsl(String url,String data){
        StringBuffer message = new StringBuffer();
        try {
            String mchId = PropertyUtil.getValue("/wxpay.properties", "MCHId");//spe.getStr3();
            KeyStore keyStore  = KeyStore.getInstance("PKCS12");
            String certFilePath= PropertyUtil.getValue("/wxpay.properties", "certs.path.linux");//spe.geturlPathPay();
            //String certFilePath="D:"+spe.getUrl();
           FileInputStream instream = new FileInputStream(new File(certFilePath));
            keyStore.load(instream, mchId.toCharArray());
            SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, mchId.toCharArray()).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1" }, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
            CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
            HttpPost httpost = new HttpPost(url);	
            httpost.addHeader("Connection", "keep-alive");
            httpost.addHeader("Accept", "*/*");
            httpost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            httpost.addHeader("Host", "api.mch.weixin.qq.com");
            httpost.addHeader("X-Requested-With", "XMLHttpRequest");
            httpost.addHeader("Cache-Control", "max-age=0");
            httpost.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
            httpost.setEntity(new StringEntity(data, "UTF-8"));
            CloseableHttpResponse response = httpclient.execute(httpost);
            try {
                HttpEntity entity = response.getEntity();
               System.out.println(response.getStatusLine());
                
                if (entity != null) {
                    System.out.println("Response content length: " + entity.getContentLength());
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent(),"UTF-8"));
                    System.out.println(bufferedReader);
                    String text;
                    while ((text = bufferedReader.readLine()) != null) {
                        message.append(text);
                    }
                    
                }
                EntityUtils.consume(entity);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                response.close();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } 
        System.out.println("111"+message.toString());
        return message.toString();
    }
    
    
    /**
	 * 
	 * @Description: xml输入流转化成map
	 * @param @param inputStream
	 * @param @return   
	 * @return Map  
	 * @throws
	 * @author dale.dai
	 * @date 2016-3-23
	 */
	private Map xml2Map(InputStream inputStream){
		Map map = new HashMap();
		try {
		InputStreamReader isr;
			isr = new InputStreamReader(inputStream,ENCODING);
		
		String sCurrentLine="";
		String sTotalString= "";
		BufferedReader reader= new BufferedReader(isr);
		while ((sCurrentLine = reader.readLine())!=null) {
			sTotalString += sCurrentLine+"\r\n";
		}
		InputStream stream = new ByteArrayInputStream(sTotalString.getBytes(ENCODING));
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(stream);
		Element root = doc.getRootElement();
		List list = root.getChildren();
		Iterator it =list.iterator();
		while (it.hasNext()) {
			Element e = (Element) it.next();
			String k =e.getName();
			String v="";
			List childen = e.getChildren();
			if(childen.isEmpty()){
				v=e.getTextNormalize();
			}else{
				v=getChildrenText(childen);
			}
			map.put(k, v);
		}
		inputStream.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}finally{
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return map;
	}
	/**
	 * 
	 * @Description: 获取子节点xml
	 * @param @param children
	 * @param @return   
	 * @return String  
	 * @throws
	 * @author dale.dai
	 * @date 2016-3-23
	 */
	public String getChildrenText(List children){
		StringBuffer buffer = new StringBuffer();
		if(!children.isEmpty()){
			Iterator it = children.iterator();
			while (it.hasNext()) {
				Element e = (Element) it.next();
				String name = e.getName();
				String value = e.getTextNormalize();
				List list = e.getChildren();
				buffer.append("<"+name+">");
				if(!list.isEmpty()){
					buffer.append(getChildrenText(list));
				}
				buffer.append(value);
				buffer.append("<"+name+">");
			}
		}
		return buffer.toString();
	}
    
   

    
    /**
     * 申请退款生成签名
     * @author: leroy 2017年8月14日11:28:52
     */
    public static String createWechatFoundOrderSign(WechatRefund pay){
        StringBuffer sign = new StringBuffer();
        sign.append("appid=").append(pay.getAppid());
        //sign.append("&body=").append(pay.getBody());
        sign.append("&mch_id=").append(pay.getMch_id());
        sign.append("&nonce_str=").append(pay.getNonce_str());
        sign.append("&notify_url=").append(pay.getNotify_url());
        sign.append("&out_refund_no=").append(pay.getOut_refund_no());
        sign.append("&out_trade_no=").append(pay.getOut_trade_no());
        if(StringUtils.isNotBlank(pay.getRefund_account())){
        	sign.append("&refund_account=").append(pay.getRefund_account());
        }        
        sign.append("&refund_fee=").append(pay.getRefund_fee());
        
        //sign.append("&spbill_create_ip=").append(pay.getSpbill_create_ip());
        sign.append("&total_fee=").append(pay.getTotal_fee());
        sign.append("&key=").append(PropertyUtil.getValue("/wxpay.properties", "APIKEY"));
        System.out.println("计算签名参数"+sign.toString());
        return Md5Util.getInstance().getLongToken(sign.toString()).toUpperCase();
    }
    /**
     * 
    * @Title: WxRefund 
    * @Description: TODO(小程序退款) 
    * @param @param pay
    * @param @param spe
    * @param @return
    * @param @throws Exception    入参
    * @return Map<String,String>    返回类型
    * @author: leroy    
    * @throws
    * @date 2020年3月28日 上午10:43:19 
    * @version V1.0   
     */
  public static Map<String, String> WxRefund(WechatRefund pay) throws Exception{
        String sign = createWechatFoundOrderSign(pay);
        System.out.println("签名："+sign);
        pay.setSign(sign);
       
        XMLUtil xmlUtil= new XMLUtil();
        xmlUtil.xstream().alias("xml", pay.getClass());
        String xml = xmlUtil.xstream().toXML(pay);
        System.out.println("请求xml："+xml);
        String response = wxSsl(PropertyUtil.getValue("/wxpay.properties", "WXREFUND"), xml);
        Map<String, String> map = xmlUtil.parseXml(response);
        return map;
    }
    
    /**
     * @param outRefundNo 退款单号	
     * @param outTradeNo 订单号
     * @param totalFee 付款金额
     * @param refundFee 退款金额
     * @param     refundDesc 若商户传入，会在下发给用户的退款消息中体现退款原因 注意：若订单退款金额≤1元，且属于部分退款，则不会在退款消息中体现退款原因
     * @param refundAccount 退款资金来源  仅针对老资金流商户使用 REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款（默认使用未结算资金退款）REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款
     * @param spe
     * @return
     */
   public static WechatRefund setWechatRefund(String outRefundNo,String outTradeNo,int totalFee,int refundFee, String refundDesc,String refundAccount){
    	WechatRefund pay = new WechatRefund();
    	try{
    		Date date = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    		String str = sdf.format(date);
    		pay.setNonce_str(str);
    		pay.setMch_id(PropertyUtil.getValue("/wxpay.properties", "MCHId"));
    		pay.setOut_refund_no(outRefundNo);
    		pay.setAppid(PropertyUtil.getValue("/wxpay.properties", "APPID"));
    		//小程序回调地址
    		pay.setNotify_url(PropertyUtil.getValue("/wxpay.properties", "REFUNDNOTIFYURL"));
    		pay.setTotal_fee(totalFee);
    		pay.setRefund_fee(refundFee);
    		pay.setOut_trade_no(outTradeNo) ;
    		pay.setRefund_desc(refundAccount);
    		pay.setRefund_account(null);
    	}catch (Exception e) {
			e.getStackTrace();
		}
        return pay;
    }
    /**
     * 调用退款接口时的签名计算
     * @param userId 用户id
     * @param orderId 订单id
     * @param spe
     * @return
     */
    public static String createGetWxPayRefundSign(String repairId,String userId,String orderId){
        StringBuffer sign = new StringBuffer();
        sign.append("appid=").append(PropertyUtil.getValue("/wxpay.properties", "APPID"));
        sign.append("&mch_id=").append(PropertyUtil.getValue("/wxpay.properties", "MCHId"));
        sign.append("&orderId=").append(orderId);
        sign.append("&userId=").append(userId);
        sign.append("&repairId=").append(repairId);
        sign.append("&key=").append(PropertyUtil.getValue("/wxpay.properties", "APIKEY"));
        System.out.println("计算签名参数"+sign.toString());
        return Md5Util.getInstance().getLongToken(sign.toString()).toUpperCase();
    }
    
    
    
	public static String AdviceSign(Map<String, String> map,String appsercet){
		String sign = null;
		try {
			String pam="";
			if(map!=null&& map.size()>0 && appsercet!=null && !"".equals(appsercet)){
				Collection<String> keyset = map.keySet();
				List<String> list = new ArrayList<String>(keyset);
				Collections.sort(list);
				for (int i = 0; i < list.size(); i++) {
					if(!list.get(i).equals("sign")){
						if(i==0){
							pam+=list.get(i)+"="+map.get(list.get(i));
						}else {
							pam+="&"+list.get(i)+"="+map.get(list.get(i));
						}
					}
				}
				if(StringUtils.isBlank(appsercet)){
					appsercet= PropertyUtil.getValue("/wxpay.properties", "APIKEY");
				}
				pam=pam+"&key="+appsercet;
				System.out.println(pam.trim());
				sign =Md5Util.getInstance().getLongToken(pam).toUpperCase();
				System.out.println("支付通知签名"+sign.length()+"~"+sign);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sign;
	}
    
	
	
	 /**
	  * 转账需要的参数
       appid 商户账号appid
        mch_id 微信支付商户号
        partner_trade_no
        nonce_str
        openId
        checkName
        amount
        spbill_create_ip
        desc
     */
    /*public static Transfers setWechatTransferPaymen(String outTradeNo,int totalFee,BoSpecialUser spe,BigDecimal userOrder,String time){
    	Transfers pay = new Transfers();
    	try{
    		pay.setNonce_str(RandCharsUtils.getRandomString(16));
    		pay.setMchid(spe.getStr3());
    		pay.setMch_appid(spe.getStr1());
    		pay.setPartner_trade_no(outTradeNo);
    		pay.setOpenid(spe.getWxShrofAccNum());
    		pay.setRe_user_name(spe.getWxAuthPayee());
    		pay.setCheck_name("FORCE_CHECK");//是否校验用户姓名 NO_CHECK：不校验真实姓名  FORCE_CHECK：强校验真实姓名
    		//pay.setNotify_url(PropertyTool.getValue("/wxpay.properties", "REFUNDNOTIFYURL"));
    		pay.setAmount(totalFee);
    		pay.setSpbill_create_ip("8.8.8.8");
    		pay.setDesc("已确认收货订单到款。订单号"+outTradeNo+"，订单金额"+userOrder+"元");
    		
    	}catch (Exception e) {
			e.getStackTrace();
		}
        return pay;
    }*/
/*    
    
    *//**
     * @Title  : WechatHttpClient.java
     * @Package: com.leroy.wxpay
     * @author : leroy
     * @version: V1.0
     * @data   : 2019年8月21日下午10:49:32
     * @Description: 订单转账
     *//*
    public static Map<String, String> WxTransfers(Transfers pay,BoSpecialUser spe) throws Exception{
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	  parameters.put("mch_appid", pay.getMch_appid());
          parameters.put("mchid", pay.getMchid());
          parameters.put("partner_trade_no", pay.getPartner_trade_no());
          parameters.put("nonce_str", pay.getNonce_str());
          parameters.put("openid", pay.getOpenid());
          parameters.put("check_name", pay.getCheck_name());
          parameters.put("amount", pay.getAmount());
          parameters.put("re_user_name", spe.getWxAuthPayee());
          parameters.put("spbill_create_ip", pay.getSpbill_create_ip());
          parameters.put("desc", pay.getDesc());
    	String sign = createSign(pay,spe);
        System.out.println("签名："+sign);
        pay.setSign(sign);
        XMLUtil xmlUtil= new XMLUtil();
        xmlUtil.xstream().alias("xml", pay.getClass());
        String xml = xmlUtil.xstream().toXML(pay);
        String response = wxSsl(PropertyTool.getValue("/wxpay.properties", "TRANSFER"), xml,spe);
        Map<String, String> map = xmlUtil.parseXml(response);
        return map;
    }
    *//**
     * 微信支付签名算法sign
     * @param characterEncoding
     * @param parameters
     * @return
     *//*
    @SuppressWarnings("rawtypes")
    public static String createSign(Transfers pay,BoSpecialUser spe){
    	StringBuffer sign = new StringBuffer();
        sign.append("amount=").append(pay.getAmount());
        sign.append("&check_name=").append(pay.getCheck_name());
        sign.append("&desc=").append(pay.getDesc());
        sign.append("&mch_appid=").append(pay.getMch_appid());
        sign.append("&mchid=").append(pay.getMchid());
        sign.append("&nonce_str=").append( pay.getNonce_str());
        sign.append("&openid=").append(pay.getOpenid());
        sign.append("&partner_trade_no=").append(pay.getPartner_trade_no());
        sign.append("&re_user_name=").append(spe.getWxAuthPayee());
        sign.append("&spbill_create_ip=").append( pay.getSpbill_create_ip());
        sign.append("&key=").append(spe.getStr2());
        System.out.println(sign);
        String sign1 =Md5Util.getInstance().getLongToken(sign.toString()).toUpperCase();
        return sign1;
    }*/

}
