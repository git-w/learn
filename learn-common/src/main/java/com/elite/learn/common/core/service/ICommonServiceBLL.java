package com.elite.learn.common.core.service;


import com.elite.learn.common.core.page.PageResult;

import java.util.List;

/**
 * 通用业务接口
 *
 * @author: leroy
 * @date 2019年9月8日
 */
public interface ICommonServiceBLL<T, DTO, VO, Query> {

    /**
     * 获取对象
     *
     * @param id 主键
     * @return
     */
    T get(String id);


    /**
     * 获取对象
     *
     * @param id 主键
     * @return
     */
    VO getInfo(String id);

    /**
     * 保存
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    String save(String operUserId, DTO bean);


    /**
     * 更新
     *
     * @param operUserId 操作人员标识
     * @param bean       对象
     * @return
     */
    boolean update(String operUserId, DTO bean);

    /**
     * 删除
     *
     * @param operUserId 操作人员标识
     * @param ids        对象标识
     * @return
     */
    boolean remove(String operUserId, String ids);

    /**
     * 获取所有
     */
    List<VO> findAll(Query bean);


    /**
     * 分页查询数据
     * @param params
     */
    PageResult<VO> getList(Query params);


}
