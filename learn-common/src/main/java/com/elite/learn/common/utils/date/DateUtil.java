package com.elite.learn.common.utils.date;


import com.elite.learn.common.utils.string.StringUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil {
	private static final String DEFAULT_FORMAT= "yyyy-MM-dd";
	private static final String DATE_FORMAT="yyyy-MM-dd HH:mm:ss";
	
	
	public static Date beforeOneHourToNowDate() { 
		/*Date date=new Date(); 
		String lasttime;
          SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
          String str_date=sdf.format(date);
          System.out.println(str_date);
         // date.setMonth(date.getMonth()-1);//月
          lasttime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
          date.setHours(date.getHours()-1);//小时
          System.out.println(lasttime);*/


		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) - 1);// 让日期加1
		System.out.println(calendar.get(Calendar.DATE));// 加1之后的日期Top
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS").format(calendar.getTime()));

		Date date = null;
		try {
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS");
			date = format1.parse(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS").format(calendar.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}


	public static int compareToDate(Date date1, Date date2) {
		if (Objects.nonNull(date1) && Objects.nonNull(date2)) {
			return date1.compareTo(date2);
		}
		return -3;
	}

	public static String formatDate(Date date, String format) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}


	/**
	 * 时间格式页面显示 time参数格式2012-01-01 11
	 */
	public static String parseResultTime(String time) {
		String t = null;
		if (StringUtil.notBlank(time) && time.length() == 13) {
			int s = Integer.parseInt(time.substring(11, 13));
			switch (s) {
				case 0:
					t = "00:00 -- 01:00";
					break;
				case 1:
					t = "01:00 -- 02:00";
					break;
				case 2:
					t = "02:00 -- 03:00";
					break;
				case 3:
					t = "03:00 -- 04:00";
					break;
				case 4:
					t = "04:00 -- 05:00";
					break;
				case 5:
					t = "05:00 -- 06:00";
					break;
				case 6:
					t = "06:00 -- 07:00";
					break;
				case 7:
					t = "07:00 -- 08:00";
					break;
				case 8:
					t = "08:00 -- 09:00";
					break;
				case 9:
					t = "09:00 -- 10:00";
					break;
				case 10:
					t = "10:00 -- 11:00";
					break;
				case 11:
					t = "11:00 -- 12:00";
					break;
				case 12:
					t = "12:00 -- 13:00";
					break;
				case 13:
					t = "13:00 -- 14:00";
					break;
				case 14:
					t = "14:00 -- 15:00";
					break;
				case 15:
					t = "15:00 -- 16:00";
					break;
				case 16:
					t = "16:00 -- 17:00";
					break;
				case 17:
					t = "17:00 -- 18:00";
					break;
				case 18:
					t = "18:00 -- 19:00";
					break;
				case 19:
					t = "19:00 -- 20:00";
					break;
				case 20:
					t = "20:00 -- 21:00";
					break;
				case 21:
					t = "21:00 -- 22:00";
					break;
				case 22:
					t = "22:00 -- 23:00";
					break;
				case 23:
					t = "23:00 -- 24:00";
					break;
			}
		}
		return t;
	}


	/**
	 * 将String类型的时间格式转化成date类型
	 * */
	public static Date stringToDate(String time,String mat){
		SimpleDateFormat format = new SimpleDateFormat(mat);
		Date date = null;
		try {
			date = format.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}


	/**
	 * 把时间类型为String的转行成毫秒
	 */
	public static long getMillionSecondLong(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long millionSeconds = 0;
		try {
			millionSeconds = sdf.parse(str).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millionSeconds;
	}


	/**
	 * 把时间类型为String的转行成毫秒
	 */
	public static long getMillionSecond(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long millionSeconds = 0;
		try {
			millionSeconds = sdf.parse(str).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millionSeconds;
	}


	/**
	 * 把时间类型为String的转行成毫秒
	 * */
	public static long getMillionSecond(String str,String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		long millionSeconds = 0;
		try {
			millionSeconds = sdf.parse(str).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return millionSeconds;
	}


	/**
	 * 将毫秒转成时间类型的格式
	 */
	public static String longToString(long millionSeconds) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(millionSeconds);
	}


	/**
	 * 获取今天的起始时间
	 */
	public static Date getStartTime() {
		Calendar todayStart = new GregorianCalendar();
		todayStart.set(Calendar.HOUR_OF_DAY, 0);
		todayStart.set(Calendar.MINUTE, 0);
		todayStart.set(Calendar.SECOND, 0);
		todayStart.set(Calendar.MILLISECOND, 0);
		return todayStart.getTime();
	}


	/**
	 * 获取今天的最终时间
	 */
	public static Date getEndTime() {
		Calendar todayEnd = new GregorianCalendar();
		todayEnd.set(Calendar.HOUR_OF_DAY, 23);
		todayEnd.set(Calendar.MINUTE, 59);
		todayEnd.set(Calendar.SECOND, 59);
		todayEnd.set(Calendar.MILLISECOND, 999);
		return todayEnd.getTime();
	}


	/*
	 * 获取明天时间
	 */
	public static Date nextDay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		//cal.add(cal.DATE, 1);
		return cal.getTime();
	}


	public static Date getNextDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		date = calendar.getTime();
		return date;
	}


	/*
	 * 获取昨天的时间
	 */
	public static Date yesterday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(cal.DATE, -1);
		return cal.getTime();
	}


	/*
	 * 获取前天的时间
	 */
	public static Date beforeYesterday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(cal.DATE, -2);
		return cal.getTime();
	}


	public static Date lastDayOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, calendar
				.getActualMaximum(Calendar.DAY_OF_MONTH));
		return calendar.getTime();
	}


	public static Date firstDayOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, calendar
				.getActualMinimum(Calendar.DAY_OF_MONTH));
		return calendar.getTime();
	}


	/**
	 * 将毫秒转成时间类型的格式
	 *
	 * @param millionSeconds long 毫秒数】
	 * @param format         格式化格式 类似yyyy-MM-dd HH:mm:ss
	 */
	public static String longToString(long millionSeconds, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(millionSeconds);
	}


	/**
	 * 字符串转换为java.util.Date<br>
	 * 支持格式为 yyyy.MM.dd G 'at' hh:mm:ss z 如 '2002-1-1 AD at 22:10:59 PSD'<br>
	 * yy/MM/dd HH:mm:ss 如 '2002/1/1 17:55:00'<br>
	 * yy/MM/dd HH:mm:ss pm 如 '2002/1/1 17:55:00 pm'<br>
	 * yy-MM-dd HH:mm:ss 如 '2002-1-1 17:55:00' <br>
	 * yy-MM-dd HH:mm:ss am 如 '2002-1-1 17:55:00 am' <br>
	 *
	 * @param time String 字符串<br>
	 * @return Date 日期<br>
	 */
	public static Date stringToDate(String time) {
		SimpleDateFormat formatter;
		int tempPos = time.indexOf("AD");
		time = time.trim();
		formatter = new SimpleDateFormat("yyyy.MM.dd G 'at' hh:mm:ss z");
		if (tempPos > -1) {
			time = time.substring(0, tempPos) +
					"公元" + time.substring(tempPos + "AD".length());//china
			formatter = new SimpleDateFormat("yyyy.MM.dd G 'at' hh:mm:ss z");
		}
		tempPos = time.indexOf("-");
		if (tempPos > -1 && (time.indexOf(" ") < 0)) {
			formatter = new SimpleDateFormat("yyyyMMddHHmmssZ");
		} else if ((time.indexOf("/") > -1) && (time.indexOf(" ") > -1)) {
			formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		} else if ((time.indexOf("-") > -1) && (time.indexOf(" ") > -1)) {
			formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		} else if ((time.indexOf("/") > -1) && (time.indexOf("am") > -1) || (time.indexOf("pm") > -1)) {
			formatter = new SimpleDateFormat("yyyy-MM-dd KK:mm:ss a");
		} else if ((time.indexOf("-") > -1) && (time.indexOf("am") > -1) || (time.indexOf("pm") > -1)) {
			formatter = new SimpleDateFormat("yyyy-MM-dd KK:mm:ss a");
		}
		ParsePosition pos = new ParsePosition(0);
		Date ctime = formatter.parse(time, pos);

		return ctime;
	}


	/**
	 * 将java.util.Date 格式转换为字符串格式'yyyy-MM-dd<br>
	 *
	 * @param time Date 日期<br>
	 * @return String   字符串<br>
	 */
	public static String dateToString(Date time) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String ctime = formatter.format(time);
		return ctime;
	}


	/**
	 * 将java.util.Date 格式转换为字符串格式<br>
	 *
	 * @param time   Date 日期<br>
	 * @param format String 格式化的格式
	 * @return String   字符串<br>
	 */
	public static String dateToString(Date time, String format) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat(format);
		String ctime = formatter.format(time);
		return ctime;
	}


	/**
	 * 根据param获得当前是星期
	 * data类型和format类型必须相同
	 */
	public static String getWeek(String data, String format) {
		Date d = stringToDate(data, format);
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("E");
		String ctime = formatter.format(d);
		return ctime;
	}


	/**
	 * 指定data类型yyyy-MM-dd
	 */
	public static String getWeek(String data) {
		Date d = stringToDate(data, "yyyy-MM-dd");
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("E");
		String ctime = formatter.format(d);
		return ctime;
	}


	/**
	 * 获取当前的星期
	 */
	public static String getWeek() {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("E");
		String ctime = formatter.format(new Date());
		return ctime;
	}


	/**
	 * 将long类型转换成Date类型
	 */
	public static Date longToDate(long l) {
		return new Date(l);
	}


	/**
	 * 将String转string
	 */
	public static String formatString(String date) {
		StringBuffer birthDay = new StringBuffer();
		if (StringUtil.isEmpty(date)) {
			if (date.length() == 4) {
				String year = date.substring(0, 4);
				birthDay.append(year + "年");
			} else if (date.length() == 7) {
				String year = date.substring(0, 4);
				String month = date.substring(5, 7);
				birthDay.append(year + "年" + month + "月");
			} else if (date.length() >= 10) {
				String year = date.substring(0, 4);
				String month = date.substring(5, 7);
				String day = date.substring(8, 10);
				birthDay.append(year + "年" + month + "月" + day + "日");
			}
			return birthDay.toString();
		}
		return null;
	}


	/**
	 * 获得当前是这周的第几天
	 * date类型和format格式必须相同
	 */
	public static int getDayOfWeek(String date, String format) {
		Date d = stringToDate(date, format);
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		int temp = c.get(Calendar.DAY_OF_WEEK);
		if (temp == 1) {
			return 7;
		} else {
			return temp - 1;
		}
	}


	/**
	 * 获得当前是这周的第几天
	 * 指定date类型yyyy-MM-dd
	 */
	public static int getDayOfWeek(String date) {
		Date d = stringToDate(date, "yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		int temp = c.get(Calendar.DAY_OF_WEEK);
		if (temp == 1) {
			return 7;
		} else {
			return temp - 1;
		}
	}


	/**
	 * 获得本周一的日期
	 */
	public static Date getDayOfMonday() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.WEEK_OF_MONTH, 0);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return cal.getTime();
	}


	/**
	 * 获得本周日的日期
	 */
	public static Date getDayOfWeekSunday() {
		/*Calendar c = Calendar.getInstance();
		 c.add(Calendar.WEEK_OF_MONTH, -1); // 减去一个星期
		 int d = c.get(Calendar.DAY_OF_WEEK)-1;// 上个星期的今天是第几天,星期天是1,所以要减去1
		 c.add(Calendar.DAY_OF_WEEK, 7 - d);// 添加余下的天数
		 return c.getTime();*/
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.WEEK_OF_MONTH, 1);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return cal.getTime();
	}


	/**
	 * 获得上周日的日期
	 */
	public static Date getBeforDayOfWeek() {
		/*Calendar c = Calendar.getInstance();
		 c.add(Calendar.WEEK_OF_MONTH, -1); // 减去一个星期
		 int d = c.get(Calendar.DAY_OF_WEEK)-1;// 上个星期的今天是第几天,星期天是1,所以要减去1
		 c.add(Calendar.DAY_OF_WEEK, 7 - d);// 添加余下的天数
		 return c.getTime();*/
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.WEEK_OF_MONTH, 0);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return cal.getTime();
	}


	/**
	 * 获得上周一的日期
	 */
	public static Date getBeforDayOfMonday() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.WEEK_OF_MONTH, -1);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return cal.getTime();
	}


	/**
	 * 获得本月的第一天
	 */
	public static Date getDayOfMonthFirst() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 0);
		c.set(Calendar.DATE, 1);
		c.roll(Calendar.DATE, 0);
		return c.getTime();
	}


	/**
	 * 获得本月的最后一天
	 */
	public static Date getDayOfMonth() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 1);
		c.set(Calendar.DATE, 1);
		c.roll(Calendar.DATE, 0);
		return c.getTime();
	}


	/**
	 * 获得上月的第一天
	 */
	public static Date getBeforDayOfMonthFirst() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -1);
		c.set(Calendar.DATE, 1);
		c.roll(Calendar.DATE, 0);
		return c.getTime();
	}


	/**
	 * 获得上月的最后一天
	 */
	public static Date getBeforDayOfMonthEnd() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 0);
		c.set(Calendar.DATE, 0);
		c.roll(Calendar.DATE, 0);
		return c.getTime();
		  /*Calendar c = Calendar.getInstance(); 
		  c.add(Calendar.MONTH,-1);
		  c.set(Calendar.DATE, 1);
		  c.roll(Calendar.DATE, -1);
		  return c.getTime();*/
	}


	/**
	 * 获得今年的第一天
	 */
	public static Date getDayOfYear() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, 0);//加一个年
		c.set(Calendar.DAY_OF_YEAR, 1);
		return c.getTime();
	}


	/**
	 * 获得今年的最后一天
	 */
	public static Date getDayOfYearEnd() {
		/*Calendar c = Calendar.getInstance();   
	    c.add(Calendar.YEAR,0);//加一个年   
	    c.set(Calendar.DAY_OF_YEAR, 1);   
	    c.roll(Calendar.DAY_OF_YEAR, -1);
	    return c.getTime();*/
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, 1);//加一个年
		c.set(Calendar.DAY_OF_YEAR, 1);
		return c.getTime();
	}


	/**
	 * 获得去年的第一天
	 */
	public static Date getDayOfYearBefor() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -1);//减一个年
		c.set(Calendar.DAY_OF_YEAR, 1);
		return c.getTime();
	}


	/**
	 * 获得去年的最后一天
	 */
	public static Date getDayOfYearLast() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, 0);//减一个年
		c.set(Calendar.DAY_OF_YEAR, 1);
		return c.getTime();
		/*Calendar c = Calendar.getInstance();   
	    c.add(Calendar.YEAR,-1);//加一个年   
	    c.set(Calendar.DAY_OF_YEAR, 1);   
	    c.roll(Calendar.DAY_OF_YEAR, -1);
	    return c.getTime();*/
	}

	/**
	 * 时间格式，返回2012-07-09 00:00:00
	 */
	public static Date getDateOfStart(Date timeValue) {
		String data = DateUtil.dateToString(timeValue);
		long tMillion = DateUtil.getMillionSecond(data);
		String time = DateUtil.longToString(tMillion);
		Date tdate = DateUtil.stringToDate(time);
		return tdate;
	}


	public static int getDaysOfTheMonth(Date date) {//获取当月天数
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date); // 要计算你想要的月份，改变这里即可
		int days = rightNow.getActualMaximum(Calendar.DAY_OF_MONTH);
		return days;
	}


	/**
	 * 获得指定月份的第一天
	 *
	 * @param monthValue int 如果monthValue=1 返回的是1月的第一天
	 */
	public static Date getDayOfMonthFirst(int monthValue) {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, monthValue - 1);
		calendar.set(Calendar.DATE, 1);
		return calendar.getTime();
	}


	/**
	 * 获得指定月份的第一天
	 *
	 * @param monthValue int 如果monthValue=1 返回的是2月的第一天
	 */
	public static Date getDayOfMonthEnd(int monthValue) {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, monthValue);
		calendar.set(Calendar.DATE, 1);
		return calendar.getTime();
	}


	/**
	 * 获得指定年的第一天
	 *
	 * @param year int
	 * @return 当前年的第一天
	 */
	public static Date getDayOfYearFirst(int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.DATE, 1);
		return calendar.getTime();
	}

	/**
	 * 获得指定年的最后一天
	 * @param year int 
	 * @return 当前年的第一天
	 *
	 * */
	public static Date getDayOfYearEnd(int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year + 1);
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.DATE, 1);
		return calendar.getTime();
	}


	public static int getCurrentYear(){
		Calendar   calendar   =   Calendar.getInstance();
		return calendar.get(Calendar.YEAR);
	}


	/**
	 * 获得昨天的日期
	 */
	public static Date getYesterday() {
		Date d = new Date();
		d.setTime(d.getTime() - 24 * 60 * 60 * 1000);
		return d;
	}


	public static void main(String args[]) {
		System.out.println("_____________________=" + getMillionSecondLong("1994-02-25"));
//		System.out.println(dateToString(getYesterday()));
//		Calendar cal = Calendar.getInstance();
		//	System.out.println("一个月前日期："+getMonthAgoL(s'sss));
		System.out.println(findDates(1603900800000L, 1604073600000L));
	}


	public static List<String> findDates(Long startDate, Long endDate) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy_MM");
		try {
			Date start = new Date(startDate);
			Date end = new Date(endDate);
			System.out.println(start);
			System.out.println(end);
			/*Date start= df.format(starts);
			Date end= df.format(ends);*/
			List<String> Ldetas = new ArrayList<>();
			//Ldetas.add(startDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(start);
			Calendar calEnd = Calendar.getInstance();
			calEnd.setTime(end);
			while (end.after(calendar.getTime())) {
				calendar.add(Calendar.DAY_OF_MONTH, 1);
				Ldetas.add(df.format(calendar.getTime()));
			}
			HashSet h = new HashSet(Ldetas);
			Ldetas.clear();
			Ldetas.addAll(h);
			System.out.println(Ldetas);
			Date today = new Date();
			List<String> list = new ArrayList<>();
			if (Objects.nonNull(Ldetas) && Ldetas.size() > 0) {
				for (String lde : Ldetas) {
					Date dateD = df.parse(lde); //将字符串转换为 date 类型  Debug：Sun Nov 11 00:00:00 CST 2018
					boolean flag = dateD.getTime() >= today.getTime();
					if (!flag) {
						list.add(lde);
					}
				}
			}
			return list;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	//当前日期后两天
	public static Date afterNDay(int n, Date date) {
		Calendar c = Calendar.getInstance();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		c.setTime(date);
		c.add(Calendar.DATE, n);
		Date d2 = c.getTime();
		//String   s=df.format(d2);
		return d2;
	}


	/**
	 * @description: 得到一个月前的日期，从当天算起
	 * @createDate 2012-11-9;下午03:03:40
	 * @author: leroy
	 */
	public static Date getMonthAgo() {
		Calendar cal = Calendar.getInstance();
		cal.add(cal.MONTH, -1);
		return cal.getTime();
	}


	public static Date parseDate(String dateStr) {
		return parseDate(dateStr, DEFAULT_FORMAT);
	}


	public static Date parseDate(String dateStr, String format) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.parse(dateStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public static String parseHourDate(String dateStr) {
		Date d = parseDate(dateStr, DEFAULT_FORMAT);
		return dateToString(d, DATE_FORMAT);
	}


	//不同时区的日期转换   zoneNum表示时差
	public static Date parseTimeZone(String dateStr, Integer zoneNum) {
		String newDateStr = dateStr.substring(0, 10) + " " + dateStr.substring(11, 19);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date mydate;
		try {
			mydate = df.parse(newDateStr);
			return new Date(mydate.getTime() + (long) zoneNum * 60 * 60 * 1000);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	//得到多少天后的日期     day天数
	public static Date parseLaterDate(String dateStr, Long day) {
		String newDateStr = dateStr.substring(0, 10) + " " + dateStr.substring(11, 19);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date mydate;
		try {
			mydate = df.parse(newDateStr);
			return new Date(mydate.getTime() + day * 24 * 60 * 60 * 1000);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * 根据秒  算出当前的加上秒的时间
	 * @description  
	 * @author: leroy
	 * @created 2014-12-5 下午04:06:59
	 * @version 1.0
	 * @param L 秒
	 * @return
	 */
	public static String getDate(Long L){
		long a=L*1000;
		long F=System.currentTimeMillis()+a;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		return sdf.format(F);
	}


	/**
	 * 获取两个时间的差值
	 * @param d1
	 * @param d2
	 * @return 以秒为单位
	 */
	public static Long getTimeDiff(Date d1,Date d2){
	    long diff = d1.getTime() - d2.getTime();
	    long s = diff / 1000;	
	    return s;
	}
	
	
	
	/**
     * 获取今天的起始时间
	 */
	public static Date getStartTimeAppoint(Integer month){
		Date dt = new Date();
		int iDay = dt.getDate();
		Calendar  todayStart = new GregorianCalendar(); 
		todayStart.setTime(new Date());
		todayStart.set(Calendar.DAY_OF_MONTH,iDay+month);
        todayStart.set(Calendar.HOUR_OF_DAY, 0);  
        todayStart.set(Calendar.MINUTE, 0);  
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);  
        return todayStart.getTime();  
    }


	/**
     * 获取今天的最终时间
	 */
	public static Date getEndTimeAppoint(Integer month){ 
		Date dt = new Date();
		int iDay = dt.getDate();
	        Calendar todayEnd = new GregorianCalendar();  
	        todayEnd.set(Calendar.DAY_OF_MONTH,iDay+month);
	        todayEnd.set(Calendar.HOUR_OF_DAY, 23);  
	        todayEnd.set(Calendar.MINUTE, 59);  
	        todayEnd.set(Calendar.SECOND, 59);  
	        todayEnd.set(Calendar.MILLISECOND, 999);  
	        return todayEnd.getTime();  
	 } 


	 /*
    获取当前时间之前或之后几分钟 minute
    */
    public static String getTimeByMinute(int minute) {

        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.MINUTE, minute);

        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());

    }


	//获取当天结束时间
	public static Date getEndTime(Date date) {
		Calendar dateEnd = Calendar.getInstance();
		dateEnd.setTime(date);
		dateEnd.set(Calendar.HOUR_OF_DAY, 23);
		dateEnd.set(Calendar.MINUTE, 59);
		dateEnd.set(Calendar.SECOND, 59);
		return dateEnd.getTime();
	}


	//获取当天开始时间
	public static Date getStartTime(Date date) {
		Calendar dateStart = Calendar.getInstance();
		dateStart.setTime(date);
		dateStart.set(Calendar.HOUR_OF_DAY, 0);
		dateStart.set(Calendar.MINUTE, 0);
		dateStart.set(Calendar.SECOND, 0);
		return dateStart.getTime();
	}

}
