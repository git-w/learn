package com.elite.learn.common.core.page;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class PageResult<T> {
    public static final int NUMBERS_PER_PAGE = 10;

    public static final int NUMBERS_PAGE_NUM = 1;
    /**
     * 一页显示的记录数
     */
    @ApiModelProperty("每页记录数 默认10条")
    private int pageSize = NUMBERS_PER_PAGE;
    /**
     * 记录总数
     */
    @ApiModelProperty("总记录数")
    private int totalRows;
    /**
     * 记录总页码
     */
    @ApiModelProperty("总页数")
    private int totalPages;

    /**
     * 当前页码
     */
    @ApiModelProperty("当前页数 默认第一页")
    private int pageNum = NUMBERS_PAGE_NUM;
    /**
     * 结果集存放List
     */
    @ApiModelProperty("列表数据")
    private List<T> resultList;


    /**
     * 计算总页数
     */
    public void setTotalPages() {
        if (totalRows % pageSize == 0) {
            this.totalPages = totalRows / pageSize;
        } else {
            this.totalPages = (totalRows / pageSize) + 1;
        }
    }

    /**
     * 分页
     */
    public PageResult(IPage<T> page) {
        this.resultList = page.getRecords();
        this.totalRows = (int) page.getTotal();
        this.pageSize = (int) page.getSize();
        this.pageNum = (int) page.getCurrent();
        this.totalPages = (int) page.getPages();
    }

    /**
     * 分页
     *
     * @param list       列表数据
     * @param totalCount 总记录数
     * @param pageSize   每页记录数
     * @param currPage   当前页数
     */
    public PageResult(List<T> list, int totalCount, int pageSize, int currPage) {

        this.resultList = list;
        this.totalRows = totalCount;
        this.pageSize = pageSize;
        this.pageNum = currPage;
        this.totalPages = (int) Math.ceil((double) totalCount / pageSize);
    }

    /**
     * 判断列表是否存在数据
     *
     * @return
     */
    public boolean isNotEmptyList() {
        return CollectionUtil.isNotEmpty(this.resultList);
    }


    public void setTotalRows(long totalRows) {
        this.totalRows = (int) totalRows;
    }

    public int getTotalPages() {
        setTotalPages();
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<T> getResultList() {
        return resultList;
    }

    public void setResultList(List<T> resultList) {
        this.resultList = resultList;
    }


}
