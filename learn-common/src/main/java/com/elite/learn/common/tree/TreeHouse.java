package com.elite.learn.common.tree;

import java.util.List;

import lombok.Data;

@Data
public class TreeHouse {
	private static final long serialVersionUID = 1L;

    private String id;

    private String lableName;

	// 子节点
	private List<TreeHouse> children;

}
