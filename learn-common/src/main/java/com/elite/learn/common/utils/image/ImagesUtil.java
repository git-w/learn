
package com.elite.learn.common.utils.image;

import com.elite.learn.common.utils.file.FileUploadUtil;

/** 
* @ClassName: Image 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author: leroy  
* @date 2020年3月16日 下午11:27:30 
* @version V1.0 
*/
public class ImagesUtil {
	/**
	 * 图片短路径
	 */
	private String img;
	/**
	 * 上传图片长路径
	 */
	private String imgPath;
	
	
	
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getImgPath() {
		if(null!=imgPath&&!"".equals(imgPath)){
			return FileUploadUtil.getImgRootPath()+"/"+imgPath;
		}
			
		return null;
	}
	public void setImgPath(String imgPath) {
		
		this.imgPath = imgPath;
	}
	
	
	
}
