package com.elite.learn.common.utils.image;


import com.elite.learn.common.utils.string.SubStringUtil;

import javax.imageio.ImageIO;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConvertUrlUtil {

	private String url;
	private byte[] pic;
	private String txt;
	public String getTxt() {
		return txt;
	}

	public void setTxt(String txt) {
		this.txt = txt;
	}

	public byte[] getPic() {
		return pic;
	}

	public void setPic(byte[] pic) {
		this.pic = pic;
	}

	public ConvertUrlUtil(String url) {
		this.url = url;
	}


	/**
	 * 将图片URl转换为字节流
	 * 
	 * @author: leroy
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static byte[] image(String url) {
		URL u;
		HttpURLConnection conn;
		ByteArrayOutputStream outStream = null;
		InputStream inStream = null;
		boolean falg = true;
		while (falg) {
			try {
				u = new URL(url);
				conn = (HttpURLConnection) u.openConnection();
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(800 * 10);
				conn.setReadTimeout(800 * 10);
				ImageIO.read(u);
				inStream = conn.getInputStream();
				outStream = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = inStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, len);
				}
				falg=false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("转换出错，重新转换中...");
				falg=true;
			} finally {
				try {
					inStream.close();
					outStream.close();
				} catch (IOException e1) {
					falg=true;
				}
			}
		}
		return outStream.toByteArray();
	}
	/**
	 * 将文本文件URl转换为字节流
	 * 编码转换
	 * @author: leroy
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static String txt(String url) {
		URL u;
		HttpURLConnection conn;
		InputStream inStream = null;
		BufferedReader bfr = null;
		boolean falg = true;
		String Byte=null;
		while (falg) {
			try {
				u = new URL(url);
				conn = (HttpURLConnection) u.openConnection();
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(800 * 10);
				conn.setReadTimeout(800 * 10);
				ImageIO.read(u);
				inStream = conn.getInputStream();
				bfr= new BufferedReader(new InputStreamReader(inStream,"UTF-8"));													
				StringBuffer s=new StringBuffer();
				String buffer = null;
				while ((buffer = bfr.readLine()) != null) {
					s.append(buffer);
				}
				Byte= SubStringUtil.quBiaoQian(s.toString());
				falg=false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("转换出错，重新转换中...");
				falg=true;
			} finally {
				try {
					inStream.close();
					bfr.close();
				} catch (IOException e1) {
					falg=true;
				}
			}
		}
		return Byte;
	}
	
	public byte[] imageByte() {
		this.setPic(image(url));
		return this.getPic();
	}
	public String txtByte(){
		this.setTxt(txt(url));
		return this.getTxt();
	}
}
