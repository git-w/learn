
package com.elite.learn.common.core.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


/**
 *  
 *
 * @version V1.0 
 * @author: leroy  
 * @ClassName: PageParams 
 * @Description: 拼接查询条件公共类 
 * @date 2020年3月11日 下午10:46:08 
 */
@Getter
@Setter
public class PageVOParams<T> extends PageParams implements Serializable {

    @ApiModelProperty("查询条件")
    private T query;


    public PageVOParams(T query, Integer pageNum, Integer pageSize) {
       // super(pageNum, pageSize);
        this.query = query;
    }

    public PageVOParams(T query) {
      //  super(10, 1);
        this.query = query;
    }

}