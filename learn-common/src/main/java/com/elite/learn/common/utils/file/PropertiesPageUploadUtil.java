package com.elite.learn.common.utils.file;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesPageUploadUtil {
	
	private Properties prop ;
	
	private static PropertiesPageUploadUtil propertiesUtil ;
	
	private static final String defaultFileName = "/application.properties";

	public static PropertiesPageUploadUtil getDefaultInstance(){
		if(propertiesUtil==null){
			propertiesUtil = new PropertiesPageUploadUtil();
		}
		return propertiesUtil;
	}

	public PropertiesPageUploadUtil(){
		init(defaultFileName);
	}

	public PropertiesPageUploadUtil(String fileName){
		init(fileName);
	}

	private void init(String resourcePath) {
		prop = new Properties();
		try {
			InputStream in = this.getClass().getResourceAsStream(resourcePath);
			//LoadProp loadProp = new LoadProp(); 
			//InputStream in = loadProp.getClass().getResourceAsStream("/config/a.properties"); 
			prop.load(in);
		/*	Set keys = prop.keySet();
			for (Object obj : prop.keySet()) {
				properties.put(obj.toString(), prop.getProperty(obj.toString()));
			}*/
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getValue(String key){
		return prop.getProperty(key)==null?"":prop.getProperty(key);
	}
	
	public void setValue(String key,String value){
		prop.setProperty(key, value);
	}
	
}
