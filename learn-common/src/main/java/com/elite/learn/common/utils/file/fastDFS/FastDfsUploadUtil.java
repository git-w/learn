package com.elite.learn.common.utils.file.fastDFS;

import com.elite.learn.common.utils.file.PropertiesPageUploadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @Title :FastDfsUpload.java
 * @Package: com.elite.learn.common.utils.file.fastDFS
 * @Description: 上传文件到fastDFS
 * @author: leroy
 * @Date: 2020/10/31 13:21
 */
public class FastDfsUploadUtil {

    private static Logger logger = LoggerFactory.getLogger(FastDfsUploadUtil.class);

    private static PropertiesPageUploadUtil util = PropertiesPageUploadUtil.getDefaultInstance();

    /**
     * @Description: TODO
     * @author: leroy
     * @data: 2020/5/27 11:03
     * @version: V1.0
     * @url: 保存文件到fastDFS
     * @param:
     */
    public static String saveFile(MultipartFile multipartFile) throws IOException {return null;}/*{
        String path = null;
        String fileName = multipartFile.getOriginalFilename();
        System.out.println(fileName + "：文件开始上传" + new Date(System.currentTimeMillis()).toLocaleString());
        String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
        byte[] file_buff = null;
        InputStream inputStream = multipartFile.getInputStream();
        if (inputStream != null) {
            int len1 = inputStream.available();
            file_buff = new byte[len1];
            inputStream.read(file_buff);
        }
        inputStream.close();
        FastDFSFile file = new FastDFSFile(fileName, file_buff, ext);
        try {
            byte[] content = file.getContent();
            path = FastDFSClient.fileUpload(content, ext);  //upload to fastdfs
            System.out.println("文件名称:" + fileName + "     文件地址：" + util.getValue("fastDFS.static.local") + path);
        } catch (Exception e) {
            logger.error("upload file Exception!", e);
        }
        return util.getValue("fastDFS.static.local") + path;
    }*/

    public static MultipartFile fileToMultipartFile(File file) throws Exception {
     /*   FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));
        return multipartFile;*/
     return null;
    }


//    //获取流文件
//    public void inputStreamToFile(InputStream ins, File file) {
//        try {
//            OutputStream os = null;
//            Properties props = System.getProperties(); //获得系统属性集
//            String osName = props.getProperty("os.name");
//            PropertiesPageUpload util = PropertiesPageUpload.getDefaultInstance();
//            if (osName.contains("Windows")) {
//                if (!new File(util.getValue("windows.video.temp")).exists()) {
//                    new File(util.getValue("windows.video.temp")).mkdirs();
//                }
//                os = new FileOutputStream(util.getValue("windows.video.temp") + file);
//            } else {
//                if (!new File(util.getValue("linux.video.temp")).exists()) {
//                    new File(util.getValue("linux.video.temp")).mkdirs();
//                }
//                os = new FileOutputStream(util.getValue("linux.video.temp") + file);
//            }
//            int bytesRead = 0;
//            byte[] buffer = new byte[8192];
//            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
//                os.write(buffer, 0, bytesRead);
//            }
//            os.close();
//            ins.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    /**
     * @Description: TODO
     * @author: leroy
     * @data: 2020/5/27 11:03
     * @version: V1.0
     * @url: 获取视频信息 -- 时长
     * @param:
     */
//    private Map<String, Object> getVideoFileInfo(File file) {
//        File source = file;
//        Encoder encoder = new Encoder();
//        FileChannel fc = null;
//        String size = "";
//        HashMap<String, Object> resultMap = new HashMap<>();
//        try {
//            Properties props = System.getProperties(); //获得系统属性集
//            String osName = props.getProperty("os.name");
//
//            if (osName.contains("Windows")) {
//                source = new File(util.getValue("windows.video.temp") + source);
//            } else {
//                source = new File(util.getValue("linux.video.temp") + source);
//            }
//            MultimediaInfo m = encoder.getInfo(source);
//            long ls = m.getDuration();
//            long minute = (long) Math.floor(ls / 1000 / 60);
//            long second = (((ls - (minute * 60)) / 1000) - (minute * 60));
//            //视频帧宽高
//            System.out.println("此视频格式为:" + m.getFormat());
//            FileInputStream fis = new FileInputStream(source);
//            fc = fis.getChannel();
//            BigDecimal fileSize = new BigDecimal(fc.size());
//            size = fileSize.divide(new BigDecimal(1048576), 2, RoundingMode.HALF_UP).toString();
//            System.out.println("此视频大小为" + size);
//            resultMap.put("timeStr", minute + "分钟" + (second == -1 ? 0 : second) + "秒");
//            resultMap.put("time", ls);
//            resultMap.put("size", size);
//            resultMap.put("format", m.getFormat());
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (null != fc) {
//                try {
//                    fc.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return resultMap;
//    }

}