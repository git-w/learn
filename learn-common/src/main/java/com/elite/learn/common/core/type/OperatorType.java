package com.elite.learn.common.core.type;

/**
 * @Title :AnswerType.java
 * @Package: com.elite.learn.common.entity.type
 * @Description: 操作人类别
 * @author: leroy
 * @data: 2020/5/15 21:03
 */
public enum OperatorType {

    /**
     * 其他
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端
     */
    MOBILE;


}
