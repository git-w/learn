package com.elite.learn.common.utils.date;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @Title :DateCalculation.java
 * @Package: com.elite.learn.common.utils.date
 * @Description: Date时间做加减运算
 * @author: leroy
 * @data: 2020/5/15 21:28
 */
@Slf4j
public class DateCalculationUtil {
    public static void main(String[] args) {
        log.info("一周7天--------------="+additionDate(System.currentTimeMillis(),7));
        log.info("一月30天--------------="+additionDate(System.currentTimeMillis(),30));
        log.info("一季90天--------------="+additionDate(System.currentTimeMillis(),90));
        log.info("一年365天--------------="+additionDate(System.currentTimeMillis(),365));
    }


    /**
         * @Description: TODO
         * @author: leroy
         * @data: 2020/5/15 21:29
         * @version: V1.0
         * @url: 时间戳  + 天数  返回时间戳
         * @param:
         */
    public static Long additionDate(Long longTime,Integer num){
        Long time=(long)(num) * 24 * 60 * 60 * 1000;
        log.info("--------------="+longTime + time);
        log.info("--------------="+new Date(longTime +time));
        return new Date(longTime + time).getTime();
    }


    /**
     * @Description: TODO
     * @author: leroy
     * @data: 2020/5/15 21:29
     * @version: V1.0
     * @url: 时间戳  - 天数  返回时间戳
     * @param:
     */
    public static Long subtractionDate(Long longTime,Integer num){
        return new Date(longTime - num * 24 * 60 * 60 * 1000).getTime();
    }

    /**
         * @Description: TODO
         * @author: leroy
         * @data: 2020/5/16 10:46
         * @version: V1.0
         * @url: 时间戳  +  几周  =  下几周的周一的时间戳
         * @param:
         */
    public static Long addWeekDate(Long longTime,Integer num){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(longTime));
        int dayOfWeek = calendar1.get(Calendar.DAY_OF_WEEK) - 1;
        if(dayOfWeek <= 0){
            dayOfWeek = 7;
        }
        int offset1 = 1 - dayOfWeek;
        calendar1.add(Calendar.DATE, offset1 + 7+(num*7));
        Date time = calendar1.getTime();
        return time.getTime();
    }

    /**
     * @Description: TODO
     * @author: leroy
     * @data: 2020/5/16 10:46
     * @version: V1.0
     * @url: 时间戳  +  几月  =  下几月的一号的时间戳
     * @param:
     */
    public static Long addMonthDate(Long longTime,Integer num){
        Date d = new Date(longTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(d);
        gc.add(2, (1*num)+1);//2代表月份，1代表在当前的日期添加一个月
        gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE));
        Date date = new Date(gc.getTime().getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH,1);
        calendar.add(Calendar.MONTH, 0);
        return calendar.getTime().getTime();
    }

    /**
     * @Description: TODO
     * @author: leroy
     * @data: 2020/5/16 10:46
     * @version: V1.0
     * @url: 时间戳  +  几季度  =  下几个季度的第一个月的一号的时间戳
     * @param:
     */
    public static Long addSeasonDate(Long longTime,Integer num){
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        GregorianCalendar gc = new GregorianCalendar();
        String format = sdf.format(new Date(longTime));
        try {
            switch (format){
                case "01":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-01-", "-01-")).getTime()).getTime();
                    break;
                case "02":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-02-", "-01-")).getTime()).getTime();
                    break;
                case "03":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-03-", "-01-")).getTime()).getTime();
                    break;
                case "04":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-04-", "-04-")).getTime()).getTime();
                    break;
                case "05":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-05-", "-04-")).getTime()).getTime();
                    break;
                case "06":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-06-", "-04-")).getTime()).getTime();
                    break;
                case "07":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-07-", "-07-")).getTime()).getTime();
                    break;
                case "08":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-08-", "-07-")).getTime()).getTime();
                    break;
                case "09":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-09-", "-07-")).getTime()).getTime();
                    break;
                case "010":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-10-", "-10-")).getTime()).getTime();
                    break;
                case "11":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-11-", "-10-")).getTime()).getTime();
                    break;
                case "12":
                    longTime = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date(longTime)).replace("-12-", "-10-")).getTime()).getTime();
                    break;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date d = new Date(longTime);
        gc.setTime(d);
        gc.add(2, (num*3)+3);//2代表月份，1代表在当前的日期添加一个月
        gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE));
        Date date = new Date(gc.getTime().getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH,1);
        calendar.add(Calendar.MONTH, 0);
        return calendar.getTime().getTime();
    }


    public static Long addYearDate(Long longTime, Integer num) {
        Date d = new Date(longTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(d);
        gc.add(2, (12*num)+12);//2代表月份，1代表在当前的日期添加一个月
        gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE));
        Date date = new Date(gc.getTime().getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH,1);
        calendar.add(Calendar.MONTH, 0);
        return calendar.getTime().getTime();
    }
}
