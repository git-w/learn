package com.elite.learn.common.utils.mail.pojo;
/**
*
* 项目名称：bo-home-common
* 类名称：Mail
* 类描述：
* 创建人：任我行&leroy
* 创建时间：2019年10月17日 上午10:51:36
* 修改人：任我行&leroy
* 修改时间：2019年10月17日 上午10:51:36
* 修改备注： 邮件发送实体类
* @version
* 
*/
public class Mail {

    public static final String ENCODEING = "UTF-8";

    private String host = "smtp.exmail.qq.com"; // 服务器地址

    private String sender = "noreply@ourutec.com"; // 发件人的邮箱

    private String receiver; // 收件人的邮箱

    private String name; // 发件人昵称

    private String username = "noreply@ourutec.com"; // 账号

    private String password = "Qdtc_1235"; // 密码

    private String subject; // 主题

    private String message; // 信息(支持HTML)

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
