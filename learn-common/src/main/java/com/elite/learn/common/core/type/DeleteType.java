package com.elite.learn.common.core.type;

public enum DeleteType {

    NOTDELETE(0,"未删除"),
    ISDELETE(1,"已删除");

    private Integer code;
    private String message;

    DeleteType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
}
