package com.elite.learn.common.utils;/**
 * @description:
 * @projectName:learn
 * @see:com.elite.learn.common.utils
 * @author: leroy
 * @createTime:2020/10/26 20:04
 * @version:1.0
 */

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @Title  :TableNameUtils.java  
 * @Package: com.elite.learn.common.utils
 * @Description:
 * @author: leroy
 * @data: 2020/10/26 20:04 
 */
public class TableNameUtils {

    /**
     * 获取积分表名称
     * @return
     */
    public static String  getIntegralTableName(String TBASENAME) {
        SimpleDateFormat sdyyyy = new SimpleDateFormat("yyyy");
        SimpleDateFormat sdmm = new SimpleDateFormat("MM");

        Date date = new Date();
        String yyyy = sdyyyy.format(date);
        String mm = sdmm.format(date);
        String nmm = getNextMM(mm);

        //表名称
        String tableName = TBASENAME+"_"+yyyy+"_"+mm;
        return tableName;
    }
    /**
     * 得到下一个月
     * @param mm
     * @return
     */
    public static String getNextMM(String mm){
        String nmm = "";
        int imm = Integer.parseInt(mm);
        if(imm>=12){
            nmm = "01";
        }else{
            imm++;
            if(imm>9){
                nmm = ""+imm;
            }
            else{
                nmm = "0"+imm;
            }
        }
        return nmm;
    }




}