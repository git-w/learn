package com.elite.learn.common.core.type;

public enum ShowType {
    HIDE(0,"隐藏"),
    SHOW(1,"显示");

    private Integer code;
    private String message;

    ShowType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
}
