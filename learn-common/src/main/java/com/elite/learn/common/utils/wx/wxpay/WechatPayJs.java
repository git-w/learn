/**
 * 
 */
package com.elite.learn.common.utils.wx.wxpay;

/**
 * @description: 微信扫码支付统一下单参数类
 * @author sun.li2017年8月14日18:30:13 添
 */
public class WechatPayJs {
	private String appId;// 公众账号
	private String timeStamp;//时间戳
	private String nonceStr;// 随机字符串
	private String pay_package;// 订单详情扩展字符串
	private String signType;// 签名方式
	private String paySign;// 签名
	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}
	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	 * @return the nonceStr
	 */
	public String getNonceStr() {
		return nonceStr;
	}
	/**
	 * @param nonceStr the nonceStr to set
	 */
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	/**
	 * @return the pay_package
	 */
	public String getPay_package() {
		return pay_package;
	}
	/**
	 * @param payPackage the pay_package to set
	 */
	public void setPay_package(String payPackage) {
		pay_package = payPackage;
	}
	/**
	 * @return the signType
	 */
	public String getSignType() {
		return signType;
	}
	/**
	 * @param signType the signType to set
	 */
	public void setSignType(String signType) {
		this.signType = signType;
	}
	/**
	 * @return the paySign
	 */
	public String getPaySign() {
		return paySign;
	}
	/**
	 * @param paySign the paySign to set
	 */
	public void setPaySign(String paySign) {
		this.paySign = paySign;
	}

}
