package com.elite.learn.common.core.type;

/**
 * @Title :StateType.java
 * @Package: com.elite.learn.common.entity.type
 * @Description:
 * @author: leroy
 * @data: 2020/4/29 13:18
 */
public enum BaseStateType {

    STATETRUE(0,"启用"),
    STATEFALSE(1,"未启用");

    private Integer code;

    private String message;

    BaseStateType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
}
