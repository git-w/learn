
package com.elite.learn.common.core.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.LinkedHashMap;


/**
 *  
 *
 * @version V1.0 
 * @author: leroy  
 * @ClassName: PageParams 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @date 2020年3月11日 下午10:46:08 
 */
@Data
public class PageParams implements Serializable {

    private static final long serialVersionUID = 5767478893763430122L;
    /**
     * 每页默认数量
     */
    public static int NUMBERS_PER_PAGE = 10;

    /**
     * 默认当前页
     */
    public static int NUMBERS_PAGE_NUM = 1;
    /**
     * 当前页
     */
    @NotNull(message = "条码,参数为空")
    @ApiModelProperty(value = "页码条目", example = "10")
    public Integer pageNum = NUMBERS_PAGE_NUM;

    /**
     * 每页的数量
     */
    @NotNull(message = "页码,参数为空")
    @ApiModelProperty(value = "当前页码", example = "1")
    public Integer pageSize = NUMBERS_PER_PAGE;

  /*  public PageParams(Integer pageNum, Integer pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }
*/
    /**
     * 排序
     */
    private String orderBy;


    /**
     * 排序字段列表，key为排序字段名，value是否是升序
     */
    private LinkedHashMap<String, Boolean> sortMap = new LinkedHashMap<String, Boolean>();

    /**
     * 过滤字段列表，key为过滤字段名，value是过滤值
     */
    private LinkedHashMap<String, Object> filterMap = new LinkedHashMap<String, Object>();

    public LinkedHashMap<String, Boolean> getSortMap() {
        return sortMap;
    }

    public void setSortMap(LinkedHashMap<String, Boolean> sortMap) {
        this.sortMap = sortMap;
    }

    public LinkedHashMap<String, Object> getFilterMap() {
        return filterMap;
    }

    public void setFilterMap(LinkedHashMap<String, Object> filterMap) {
        this.filterMap = filterMap;
    }


    /**
     * 获取过滤值
     *
     * @param key
     * @return
     */
    public Object getFilterValue(String key) {
        return filterMap.get(key);
    }

    /**
     * 添加过滤字段
     *
     * @param key
     * @param value
     */
    public void addFilter(String key, Object value) {
        this.filterMap.put(key, value);
    }


    /**
     * 获取排序条件
     *
     * @return
     */
    public Boolean getSortVale(String key) {
        return sortMap.get(key);
    }

    /**
     * 设置排序条件
     *
     * @param key
     * @param value
     */
    public void addSort(String key, Boolean value) {
        this.sortMap.put(key, value);
    }


}