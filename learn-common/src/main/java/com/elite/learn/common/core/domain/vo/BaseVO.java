package com.elite.learn.common.core.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class BaseVO {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("创建时间")
    private Date createTime;
    @ApiModelProperty("创建人uid")
    private String createUserId;
    @ApiModelProperty("创建人名称")
    private String createUserName;
    @ApiModelProperty("更新时间")
    private Date updateTime;
    @ApiModelProperty("更新人uid")
    private String updateUserId;
    @ApiModelProperty("更新人名称")
    private String updateUserName;
    @ApiModelProperty("状态；1-活跃；2-无效")
    private Integer statusAt;
}
