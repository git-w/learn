package com.elite.learn.common.utils.file;

public class FileUploadUtil {

	
	/**
	* @Title: getImgRootPath 
	* @param @return    入参
	* @return String    返回类型
	* @author: leroy    
	* @date 2020年3月16日 下午4:27:06 
	* 读取文件的路劲
	 */
	public static String getImgRootPath(){
		PropertiesPageUploadUtil util = PropertiesPageUploadUtil.getDefaultInstance();
		return util.getValue("product.img.path");
		
	}


}
