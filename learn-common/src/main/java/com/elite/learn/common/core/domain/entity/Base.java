package com.elite.learn.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class Base {
    @TableId(value = "id", type = IdType.INPUT)
    private String id;
    @TableField("create_time")
    private Date createTime;
    @TableField("create_user_id")
    private String createUserId;
    @TableField("create_user_name")
    private String createUserName;
    @TableField("is_delete")
    @ApiModelProperty("0未删除；1-删除")
    @JsonIgnore
    private Integer isDelete;
    @TableField("update_time")
    private Date updateTime;
    @TableField("update_user_id")
    private String updateUserId;
    @TableField("update_user_name")
    private String updateUserName;
    @TableField("status_at")
    @ApiModelProperty("状态 1:活跃状态 2:无效")
    private Integer statusAt;

    public void init() {
        this.id = IdWorker.getIdStr();
        this.createTime = new Date();
        this.updateTime = new Date();
       // this.statusAt = DBStatus.ACTIVE;
    }
}
