package com.elite.learn.common.utils.http.http;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil {

	private static Properties prop = null;
	private static String fileName = "/application.properties";

	public static String getValue(String theKey) {
		if (prop != null) {
			return prop.getProperty(theKey);
		} else {
			Properties prop = new Properties();
			InputStream is = null;
			try {
				is = PropertyUtil.class.getResourceAsStream(fileName);
				prop.load(is);
				return prop.getProperty(theKey);
			} catch (Exception e) {
				throw new RuntimeException("Can't read properties file");
			} finally {
				try {
					if (is != null)
						is.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static String getValue(String propertiesName, String theKey) {
		Properties prop = new Properties();
		InputStream is = null;
		try {
			is = PropertyUtil.class.getResourceAsStream(propertiesName);
			prop.load(is);
			return prop.getProperty(theKey);
		} catch (Exception e) {
			throw new RuntimeException("Can't read properties file");
		} finally {
			try {
				if (is != null)
					is.close();
			} catch (IOException e) {
			}
		}
	}
	public static String getCertsPath(String propertiesName){
		Properties props=System.getProperties(); //获得系统属性集    
		String osName = props.getProperty("os.name"); 
		Properties prop = new Properties();
		InputStream is = null;
		try {
			is = PropertyUtil.class.getResourceAsStream(propertiesName);
			prop.load(is);
			if(osName.contains("Windows")){
				return prop.getProperty("certs.path.window");
			}else{
				return prop.getProperty("certs.path.linux");
			}
		} catch (Exception e) {
			throw new RuntimeException("Can't read properties file");
		} finally {
			try {
				if (is != null)
					is.close();
			} catch (IOException e) {
			}
		}
	}
}