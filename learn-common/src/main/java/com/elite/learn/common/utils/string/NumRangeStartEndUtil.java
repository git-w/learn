package com.elite.learn.common.utils.string;

/**
 * @Title :NumRangeStartEnd.java
 * @Package: com.elite.learn.common.utils.num
 * @Description:
 * @author: leroy
 * @Date: 2020/10/31 14:36
 */
public class NumRangeStartEndUtil {

    /**
     * @Description: TODO
     * @author: leroy
     * @date: 2020/10/31 11:50
     * @version: V1.0
     * @url: 判断一个数字是否在一个数字范围区间以内
     * @param:
     */
    public static boolean numRangeIsTrue(int startNum, int endNum, int num) {
        boolean flag = false;
        if (startNum <= num && endNum >= num) {
            flag = true;
        }
        return flag;
    }

}