package com.elite.learn.common.core.type;

/**
 * @Title :StateType.java
 * @Package: com.elite.learn.common.entity.type
 * @Description:
 * @author: leroy
 * @data: 2020/4/29 13:18
 */
public enum IsSonType {

    NO(0,"否"),
    YES(1,"是");

    private Integer code;

    private String message;

    IsSonType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
}
