
package com.elite.learn.common.utils.wx.wxpay;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.io.xml.XppDriver;

/**
 * @description:
 * @author Administrator
 * @data 2017-5-24下午06:02:20
 */
public class XMLUtil {
	private static final String ENCODING = "UTF-8";
	/**
	 * 解析字符串（XML）
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> parseXml(String msg)
	throws Exception {
		ResultObj obj = new ResultObj();
		// 将解析结果存储在HashMap中
		//Map<String, String> map = new HashMap<String, String>();
		// 从request中取得输入流
		//InputStream inputStream = new ByteArrayInputStream(msg.getBytes("UTF-8"));
		/*System.out.println(msg);
		analyxml aly = new analyxml();
		Map<String, String> map  = aly.parse(msg);*/
		
		InputStream inputStream=new ByteArrayInputStream(msg.getBytes("UTF-8"));
		
		Map map = xml2Map(inputStream);
		
		
		
		/*// 读取输入流
		SAXReader reader = new SAXReader();
		Document document = reader.read(inputStream);
		// 得到xml根元素
		Element root = document.getRootElement();
		// 得到根元素的所有子节点
		List<Element> elementList = root.elements();
		// 遍历所有子节点
		for (Element e : elementList){
			map.put(e.getName(), e.getText());
		}*/
		
		
		
		
		// 释放资源
		//inputStream.close();
		//inputStream = null;
		return map;
	}
	
	
	
	 /**
		 * 
		 * @Description: xml输入流转化成map
		 * @param @param inputStream
		 * @param @return   
		 * @return Map  
		 * @throws
		 * @author dale.dai
		 * @date 2016-3-23
		 */
		private Map xml2Map(InputStream inputStream){
			Map map = new HashMap();
			try {
			InputStreamReader isr;
				isr = new InputStreamReader(inputStream,ENCODING);
			
			String sCurrentLine="";
			String sTotalString= "";
			BufferedReader reader= new BufferedReader(isr);
			while ((sCurrentLine = reader.readLine())!=null) {
				sTotalString += sCurrentLine+"\r\n";
			}
			InputStream stream = new ByteArrayInputStream(sTotalString.getBytes(ENCODING));
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(stream);
			Element root = doc.getRootElement();
			List list = root.getChildren();
			Iterator it =list.iterator();
			while (it.hasNext()) {
				Element e = (Element) it.next();
				String k =e.getName();
				String v="";
				List childen = e.getChildren();
				if(childen.isEmpty()){
					v=e.getTextNormalize();
				}else{
					v=getChildrenText(childen);
				}
				map.put(k, v);
			}
			inputStream.close();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JDOMException e) {
				e.printStackTrace();
			}finally{
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return map;
		}
		/**
		 * 
		 * @Description: 获取子节点xml
		 * @param @param children
		 * @param @return   
		 * @return String  
		 * @throws
		 * @author dale.dai
		 * @date 2016-3-23
		 */
		public String getChildrenText(List children){
			StringBuffer buffer = new StringBuffer();
			if(!children.isEmpty()){
				Iterator it = children.iterator();
				while (it.hasNext()) {
					Element e = (Element) it.next();
					String name = e.getName();
					String value = e.getTextNormalize();
					List list = e.getChildren();
					buffer.append("<"+name+">");
					if(!list.isEmpty()){
						buffer.append(getChildrenText(list));
					}
					buffer.append(value);
					buffer.append("<"+name+">");
				}
			}
			return buffer.toString();
		}
	    
	
	/**
	 * 扩展xstream，使其支持CDATA块
	 */
	private XStream xstream = new XStream(new XppDriver(new NoNameCoder()) {
		
		@Override
		public HierarchicalStreamWriter createWriter(Writer out) {
			return new PrettyPrintWriter(out) {
				// 对所有xml节点的转换都增加CDATA标记
				boolean cdata = true;
				
				@Override
				@SuppressWarnings("rawtypes")
				public void startNode(String name, Class clazz) {
					super.startNode(name, clazz);
				}
				
				@Override
				public String encodeNode(String name) {
					return name;
				}
				
				
				@Override
				protected void writeText(QuickWriter writer, String text) {
					if (cdata) {
						writer.write("<![CDATA[");
						writer.write(text);
						writer.write("]]>");
					} else {
						writer.write(text);
					}
				}
			};
		}
	});
	
	private XStream inclueUnderlineXstream = new XStream(new DomDriver(null,new XmlFriendlyNameCoder("_-", "_")));
	
	public XStream getXstreamInclueUnderline() {
		return inclueUnderlineXstream;
	}
	public XStream xstream() {
		return xstream;
	}
	     
}
