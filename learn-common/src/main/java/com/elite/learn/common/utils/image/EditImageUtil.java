package com.elite.learn.common.utils.image;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;

/**
 * @Title :EditImage.java
 * @Package: com.elite.learn.common.utils.image
 * @Description: 图片合成  --证书 生成背景
 * @author: leroy
 * @Date: 2020/10/27 16:55
 */
public class EditImageUtil {


    public static void main(String[] args) throws IOException {

    }

    public static void mergeImage(File background, File headImg, URL chapterUrl, String toFileUrl,int y) throws IOException {
        BufferedImage image0 = ImageIO.read(background);
        int backgroundWidth = image0.getWidth();
        BufferedImage headImgIo = ImageIO.read(headImg);
        int headImgWidth = headImgIo.getWidth();
        BufferedImage chapterImgIo = ImageIO.read(chapterUrl);
        //生成画布
        BufferedImage combined = new BufferedImage(image0.getWidth(), image0.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = combined.getGraphics();

        //背景坐标
        g.drawImage(image0, 0, 0, null);

        //头像坐标
        //头像居中： 背景一半的尺寸减去头像一半的尺寸为居中x坐标点
        g.drawImage(headImgIo, ((backgroundWidth/2)-(headImgWidth/2)), y, null);

        //章坐标
        g.drawImage(chapterImgIo, 1020, 2150, null);

        ImageIO.write(combined, "JPG", new File(toFileUrl));
    }


}