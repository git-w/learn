package com.elite.learn.common.utils.string;/**
 * @description:
 * @projectName:learn
 * @see:com.elite.learn.common.utils
 * @author: leroy
 * @createTime:2020/10/10 9:24
 * @version:1.0
 */

import java.util.*;

/**
 *
 * @Title  :ListUniq.java  
 * @Package: com.elite.learn.common.utils
 * @Description:
 * @author: leroy
 * @data: 2020/10/10 9:24 
 */
public class ListUniqUtil {


    public static void main(String[] args) {

        String[] srcArr = {"a", "b", "c", "c", "d", "e", "e", "e", "a"};

        String[] destArr01 = deDuplication01(srcArr);
        System.out.println(Arrays.toString(destArr01));

        String[] destArr02 = deDuplication02(srcArr);
        System.out.println(Arrays.toString(destArr02));

        String[] destArr03 = deDuplication03(srcArr);
        System.out.println(Arrays.toString(destArr03));

        String[] destArr04 = deDuplication04(srcArr);
        System.out.println(Arrays.toString(destArr04));

        String[] destArr05 = deDuplication05(srcArr);
        System.out.println(Arrays.toString(destArr05));


    }

    /**
     * 数组去重 法一
     * <p>
     * 先遍历原数组，然后遍历结束集，通过每个数组的元素和结果集中的元素进行比对，若相同则break。若不相同，则存入结果集。两层循环进行遍历得出最终结果。
     *
     * @param srcArr 原数组
     * @return 目标数组
     */
    private static String[] deDuplication01(String[] srcArr) {
        List<String> list = new ArrayList<>();
        boolean flag;
        for (int i = 0; i < srcArr.length; i++) {// 遍历原数组
            flag = false;
            for (int j = 0; j < list.size(); j++) {// 遍历结果集
                if (srcArr[i].equals(list.get(j))) {
                    flag = true;
                    break;// 重复元素,break
                }
            }
            if (!flag) {
                list.add(srcArr[i]);// 不重复的元素存入结果集
            }
        }

        return (String[]) list.toArray(new String[list.size()]);
    }

    /**
     * 数组去重 法二
     * <p>
     * int indexOf(String str)返回指定子字符串在此字符串中第一次出现处的索引。
     *
     * @param srcArr 原数组
     * @return 目标数组
     */
    private static String[] deDuplication02(String[] srcArr) {
        List<String> list = new ArrayList<>();
        list.add(srcArr[0]);
        for (int i = 1; i < srcArr.length; i++) {
            if (list.toString().indexOf(srcArr[i]) == -1) {
                list.add(srcArr[i]);
            }
        }
        return (String[]) list.toArray(new String[list.size()]);
    }

    /**
     * 数组去重 法三
     * <p>
     * 嵌套循环
     *
     * @param srcArr 原数组
     * @return 目标数组
     */
    private static String[] deDuplication03(String[] srcArr) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < srcArr.length; i++) {
            for (int j = i + 1; j < srcArr.length; j++) {
                if (srcArr[i] == srcArr[j]) {
                    j = ++i;
                }
            }
            list.add(srcArr[i]);
        }
        return (String[]) list.toArray(new String[list.size()]);
    }

    /**
     * 数组去重 法四
     * <p>
     * 先使用java提供的数组排序方法进行排序，然后进行一层for循环，进行相邻数据的比较即可获得最终结果集。
     *
     * @param srcArr 原数组
     * @return 目标数组
     */
    private static String[] deDuplication04(String[] srcArr) {
        Arrays.sort(srcArr);
        List<String> list = new ArrayList<>();
        list.add(srcArr[0]);
        for (int i = 1; i < srcArr.length; i++) {
            if (!srcArr[i].equals(list.get(list.size() - 1))) {
                list.add(srcArr[i]);
            }
        }
        return (String[]) list.toArray(new String[list.size()]);
    }

    /**
     * 数组去重 法五
     * <p>
     * 先使用java提供的数组排序方法进行排序，然后进行一层for循环，进行相邻数据的比较即可获得最终结果集。
     *
     * @param srcArr 原数组
     * @return 目标数组
     */
    private static String[] deDuplication05(String[] srcArr) {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < srcArr.length; i++) {
            set.add(srcArr[i]);
        }
        return (String[]) set.toArray(new String[set.size()]);
    }



    /**
     * 数组去重 法一
     * <p>
     * 先遍历原数组，然后遍历结束集，通过每个数组的元素和结果集中的元素进行比对，若相同则break。若不相同，则存入结果集。两层循环进行遍历得出最终结果。
     *
     * @param srcArr 原数组
     * @return 目标数组
     */
    private static String[] deDuplication(String[] srcArr) {
        List<String> list = new ArrayList<>();
        boolean flag;
        for (int i = 0; i < srcArr.length; i++) {// 遍历原数组
            flag = false;
            for (int j = 0; j < list.size(); j++) {// 遍历结果集
                if (srcArr[i].equals(list.get(j))) {
                    flag = true;
                    break;// 重复元素,break
                }
            }
            if (!flag) {
                list.add(srcArr[i]);// 不重复的元素存入结果集
            }
        }

        return (String[]) list.toArray(new String[list.size()]);
    }


    /**
     * 数组去重 法五
     * <p>
     * 先使用java提供的数组排序方法进行排序，然后进行一层for循环，进行相邻数据的比较即可获得最终结果集。
     *
     * @param srcArr 原数组
     * @return 目标数组
     */
    public static String[] deDuplicationList(List<String> srcArr) {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < srcArr.size(); i++) {
            set.add(srcArr.get(i));
        }
        return (String[]) set.toArray(new String[set.size()]);
    }

}