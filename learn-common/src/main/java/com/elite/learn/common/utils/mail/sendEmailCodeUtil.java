package com.elite.learn.common.utils.mail;


import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sun.mail.util.MailSSLSocketFactory;  

/**
*
* 项目名称：bo-home-common
* 类名称：sendEmailCode
* 类描述：
* 创建人：任我行&leroy
* 创建时间：2019年10月17日 上午10:56:10
* 修改人：任我行&leroy
* 修改时间：2019年10月17日 上午10:56:10
* 修改备注：
* @version
*
*/
public class sendEmailCodeUtil {
    // 邮件发送协议  
    private final static String PROTOCOL = "smtp";  
    // SMTP邮件服务器  
    private final static String HOST = "smtp.qq.com";  
    // SMTP邮件服务器默认端口  
    private final static String PORT = "465";  
    // 是否要求身份认证  
    private final static String IS_AUTH = "true";  
    // 发件人  
    private static String from = "leroy.lei@foxmail.com";  
    // 收件人  
    public static String sslSocketFactory = "javax.net.ssl.SSLSocketFactory";
    // 初始化连接邮件服务器的会话信息  
    private static Properties props = null;  
   
    static {  
        props = new Properties();  
        MailSSLSocketFactory sf;
		try {
			 //SSL加密
             sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            props.setProperty("mail.transport.protocol", PROTOCOL); // 使用的协议（JavaMail规范要求）
            props.put("mail.smtp.ssl.enable", "true");
            props.put("mail.smtp.ssl.socketFactory", sf);
            
            //设置系统属性
            props.setProperty("mail.smtp.host", HOST);
            props.put("mail.smtp.auth", IS_AUTH);
            props.put("mail.smtp.ssl.enable", "true");
          //ssl安全认证
            props.setProperty("mail.smtp.port", PORT);
            //设置socketfactory
            props.setProperty("mail.smtp.socketFactory.class", sslSocketFactory);
            //只处理SSL的连接, 对于非SSL的连接不做处理
            props.setProperty("mail.smtp.socketFactory.fallback", "false");
            props.setProperty("mail.smtp.socketFactory.port", PORT);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
    }  
   
   
   
    /**
     * 向邮件服务器提交认证信息
     */  
    static class MyAuthenticator extends Authenticator {  
    	//账号
        private String username = "leroy.lei@foxmail.com";  
       //授权码
        private String password = "rqbtmzuaksevbffd";  
        public MyAuthenticator() {  
            super();  
        }  
   
        public MyAuthenticator(String username, String password) {  
            super();  
            this.username = username;  
            this.password = password;  
        }  
        @Override  
        protected PasswordAuthentication getPasswordAuthentication() {  
            return new PasswordAuthentication(username, password);  
        }
    } 
    
    
    public static boolean sendEmails(String title,String content) throws Exception {
    	// 1.创建一个程序与邮件服务器会话对象 Session  解决linux上发送邮件失败问题，需要下面的配置
    	String sendTo[] = { "1072600118@qq.com", "872459456@qq.com","807267726@qq.com"};// leroy  丽姐 子龙  
    	 InternetAddress[] sendTo1 = new InternetAddress[sendTo.length];  
         for (int i = 0; i < sendTo.length; i++) {  
             System.out.println("发送到:" + sendTo[i]);  
             sendTo1[i] = new InternetAddress(sendTo[i]);  
         }  
        // 创建Session实例对象  
        Session session1 = Session.getInstance(props, new MyAuthenticator());  
        System.out.println(props);
        // 创建MimeMessage实例对象  
        MimeMessage message = new MimeMessage(session1);  
        // 设置邮件主题  
        message.setSubject(title);  
        // 设置发送人  
        message.setFrom(new InternetAddress(from));  
        // 设置发送时间  
        message.setSentDate(new Date());  
        // 设置收件人  addRecipientT0
        message.setRecipients(RecipientType.TO, sendTo1);  
        // 设置html内容为邮件正文，指定MIME类型为text/html类型，并指定字符编码为gbk  
       // message.setContent("<div style='width: 600px;margin: 0 auto'><h3 style='color:#003E64; text-align:center; '>内燃机注册验证码</h3><p style=''>尊敬的用户您好：</p><p style='text-indent: 2em'>您在注册内燃机账号，此次的验证码是："+code+",有效期10分钟!如果过期请重新获取。</p><p style='text-align: right; color:#003E64; font-size: 20px;'>中国内燃机学会</p></div>","text/html;charset=utf-8");  
        //html代码部分
        //整封邮件的MINE消息体
        MimeMultipart msgMultipart = new MimeMultipart("mixed");//混合的组合关系
        //设置邮件的MINE消息体
        message.setContent(msgMultipart);
        //html代码部分
        MimeBodyPart htmlPart = new MimeBodyPart();
        msgMultipart.addBodyPart(htmlPart);
        //html代码
        htmlPart.setContent(content, "text/html;charset=utf-8");
      //设置自定义发件人昵称 
        String nick="订单通知"; 
        try { 
            nick=javax.mail.internet.MimeUtility.encodeText("订单通知"); 
        } catch (UnsupportedEncodingException e) { 
            e.printStackTrace(); 
        }
        message.setFrom(new InternetAddress(nick+" <"+from+">"));
        // 保存并生成最终的邮件内容  
        message.saveChanges();  
        // 发送邮件  
        try {
            Transport.send(message);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } 
      
    }  
    
    public static void main(String[] args) throws Exception {
    	sendEmails( "有新用户","新用户登录"+new Date());
    }
}
