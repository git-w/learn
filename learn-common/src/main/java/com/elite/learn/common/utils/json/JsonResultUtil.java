package com.elite.learn.common.utils.json;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * JSON回应类
 *
 * @author: leroy
 * @date 2019/11/28
 */
public class JsonResultUtil implements Serializable {
    // 错误码
    private Integer code = HttpStatus.OK.value();

    // 提示语
    private String msg = "操作成功";

    // 返回对象
    private Object data;

    public Integer getCode() {
        return this.code;
    }

    public void setCode(final Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(final String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(final Object data) {
        this.data = data;
    }

    /**
     * 构造函数
     */
    public JsonResultUtil() {
    }

    public JsonResultUtil(String msg) {
        this.msg = msg;
    }

    public JsonResultUtil(Object data) {
        this.data = data;
    }

    public JsonResultUtil(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public JsonResultUtil(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static JsonResultUtil success() {
        return new JsonResultUtil();
    }

    public static JsonResultUtil success(String msg) {
        return new JsonResultUtil(msg);
    }

    public JsonResultUtil success(Object data) {
        return new JsonResultUtil(HttpStatus.OK.value(), msg, data);
    }

    public static JsonResultUtil success(String msg, Object data) {
        return new JsonResultUtil(HttpStatus.OK.value(), msg, data);
    }

    public static JsonResultUtil error() {
        return new JsonResultUtil(-1, "操作失败");
    }

    public static JsonResultUtil error(String msg) {
        return new JsonResultUtil(-1, msg);
    }

    public static JsonResultUtil error(Integer code, String msg) {
        return new JsonResultUtil(code, msg);
    }

    public static JsonResultUtil error(Integer code, String msg, Object data) {
        return new JsonResultUtil(code, msg, data);
    }

/*
    public static JsonResultUtil error(ErrorCode errorCode) {
        return new JsonResultUtil(errorCode.getCode(), errorCode.getMsg());
    }
*/

    public static JsonResultUtil error(HttpStatus httpStatus, String msg, Object data) {
        return new JsonResultUtil(httpStatus.value(), msg, data);
    }

    public Object error(HttpStatus httpStatus, String msg) {
        this.code = httpStatus.value();
        this.msg = msg;
        return this;
    }

}
