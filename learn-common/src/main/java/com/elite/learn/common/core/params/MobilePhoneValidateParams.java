package com.elite.learn.common.core.params;/**
 * @description:
 * @projectName:learn
 * @see:com.elite.learn.common.entity.params
 * @author: leroy
 * @createTime:2020/10/10 23:06
 * @version:1.0
 */

import lombok.Data;

/**
 *
 * @Title  :MobilePhoneValidateParams.java  
 * @Package: com.elite.learn.common.entity.params
 * @Description:
 * @author: leroy
 * @data: 2020/10/10 23:06 
 */
@Data
public class MobilePhoneValidateParams {

    /**
     * 手机号
     */
    private String MobilePhone;
    /**
     * 验证码
     */
    private String Code;

}