package com.elite.learn.common.utils.image.contants;

public enum FileManageExceptionCodeEnumUtil {

	SaveSuccess(100000,"上传成功"),
	ParamsError(100001,"上传文件为空"),
	SaveFail(100005,"上传失败");
	
	public Integer code;
	public String msg;
	
	private FileManageExceptionCodeEnumUtil(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}
}
