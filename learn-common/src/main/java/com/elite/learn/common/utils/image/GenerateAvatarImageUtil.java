package com.elite.learn.common.utils.image;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;

/**
 * @Title :GenerateAvatarImage.java
 * @Package: com.elite.learn.common.utils.image
 * @Description: 将图片设裁剪为圆形--证书  加工头像
 * @author: leroy
 * @Date: 2020/10/29 17:12
 */
public class GenerateAvatarImageUtil {

    public static String gerRound(String url, String target, String fileName) throws Exception {

//        String url = "https://img-blog.csdn.net/20180529211243786?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3plbmdyZW55dWFu/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/7";
        BufferedImage avatarImage = ImageIO.read(new URL(url));
        int width = 200;
        // 透明底的图片
        BufferedImage formatAvatarImage = new BufferedImage(width, width, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics = formatAvatarImage.createGraphics();
        //把图片切成一个圓
        {
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //留一个像素的空白区域，这个很重要，画圆的时候把这个覆盖
            int border = 1;
            //图片是一个圆型
            Ellipse2D.Double shape = new Ellipse2D.Double(border, border, width - border * 2, width - border * 2);
            //需要保留的区域
            graphics.setClip(shape);
            graphics.drawImage(avatarImage, border, border, width - border * 2, width - border * 2, null);
            graphics.dispose();
        }
//        //在圆图外面再画一个圆
        {
            //新创建一个graphics，这样画的圆不会有锯齿
            graphics = formatAvatarImage.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            int border = 1;
            //画笔是4.5个像素，BasicStroke的使用可以查看下面的参考文档

            //使画笔时基本会像外延伸一定像素，具体可以自己使用的时候测试
            Stroke s = new BasicStroke(4.5F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
            graphics.setStroke(s);
            graphics.setColor(Color.WHITE);
            graphics.drawOval(border, border, width - border * 2, width - border * 2);
            graphics.dispose();
        }
        {
            try (OutputStream os = new FileOutputStream(target + fileName)) {
                ImageIO.write(formatAvatarImage, "PNG", os);
            }
        }
        return target + fileName;
    }


    public static BufferedImage resize(int faceWidth, BufferedImage srcImg) throws IOException {
        //构建新的图片200
        BufferedImage resizedImg = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
        //将原图放大或缩小后画下来:并且保持png图片放大或缩小后背景色是透明的而不是黑色
        Graphics2D resizedG = resizedImg.createGraphics();
        resizedImg = resizedG.getDeviceConfiguration().createCompatibleImage(200, 200, Transparency.TRANSLUCENT);
        resizedG.dispose();
        resizedG = resizedImg.createGraphics();
        Image from = srcImg.getScaledInstance(200, 200, srcImg.SCALE_AREA_AVERAGING);
        resizedG.drawImage(from, 200, 200, null);
        resizedG.dispose();

        return resizedImg;
    }



}