package com.elite.learn.common.utils.string;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**        
 * Title: 字符串工具类    
 * @author: leroy
 * @created 2017年7月4日 下午5:15:29    
 */      
public class StringUtil {
	  
	/**     
	 * @description 给定字符串是否为空或空串
	 * @author: leroy
	 * @created 2017年7月4日 下午5:15:46     
	 * @param str
	 * @return     
	 */
	public static boolean isNotEmpty(String str) {
		if (str != null && str.length() != 0) {
			return true;
		}
		return false;
	}

	/**     
	 * @description 给定字符串是否为空或空串
	 * @author: leroy
	 * @created 2017年7月4日 下午5:15:46     
	 * @param str
	 * @return     
	 */
	public static boolean isEmpty(String str) {
		if (str != null && str.length() != 0) {
			return false;
		}
		return true;
	}


	/**
	 * 字符串不为 null 而且不为  "" 时返回 true
	 */
	public static boolean notBlank(String str) {
		return str == null || "".equals(str.trim()) || "null".equals(str.trim()) ? false : true;
	}
	
	/**
	 * @description: 字符串转换成数组
	 * @param str 字符串
	 * @param joinStr 拆分字符串 如 "," or ";"
	 * @return 数组
	 * @author: leroy
	 * @createDate 2013-5-24;上午09:59:53
	 */
	public static String[] string2Array(String str , String joinStr){
		if(notBlank(str)){
			return str.split(joinStr);
		}
		return null;
	}
	 
	/**
	 * @description: 以分号拼接的字符串转换成数组
	 * @param str
	 * @return
	 * @author: leroy
	 * @createDate 2013-5-24;上午10:03:33
	 */
	public static String[] string2ArrayBySemicolon(String str){
		return string2Array(str, ";");
	}
	
	/**
	 * @description: 以逗号拼接的字符串转换成数组
	 * @param str
	 * @return
	 * @author: leroy
	 * @createDate 2013-5-24;上午10:04:16
	 */
	public static String[] string2ArrayByComma(String str){
		return string2Array(str, ",");
	}
	
	/**
	 * @description: 以冒号拼接的字符串转换成数组
	 * @param str
	 * @return
	 * @author: leroy
	 * @createDate 2013-5-24;上午10:05:06
	 */
	public static String[] string2ArrayByColon(String str){
		return string2Array(str, ":");
	}
	
	/**
	 * @description: 数组转换成字符串,
	 * @param args 数组
	 * @param joinStr 拼接字符串 如 "," or ";"
	 * @return 字符串
	 * @author: leroy
	 * @createDate 2013-5-7;下午08:15:02
	 */
	public static String array2String(String args[],String joinStr){
		StringBuilder msg = new StringBuilder();
		if(args != null && args.length > 0){
			for(int i=0; i<args.length; i++){
				if(i != 0){
					msg.append(joinStr);
				}
				msg.append(args[i]);
			}
		}
		return msg.toString();
	}
	/**
	 * @description: 数组转换成字符串,
	 * @param args 数组
	 * @param joinStr 拼接字符串 如 "," or ";"
	 * @return 字符串
	 * @author: leroy
	 * @createDate 2013-5-7;下午08:15:02
	 */
	public static String array2char(char[] args[],String joinStr){
		StringBuilder msg = new StringBuilder();
		if(args != null && args.length > 0){
			for(int i=0; i<args.length; i++){
				if(i != 0){
					msg.append(joinStr);
				}
				msg.append(args[i]);
			}
		}
		return msg.toString();
	}
	
	/**
	 * @description: 数组转换成字符串，以分号拼接
	 * @param args
	 * @return
	 * @author: leroy
	 * @createDate 2013-5-8;上午10:07:32
	 */
	public static String array2StringBySemicolon(String args[]){
		return array2String(args, ";");
	}
	
	/**
	 * @description: 数组转换成字符串，以逗号拼接
	 * @param args
	 * @return
	 * @author: leroy
	 * @createDate 2013-5-8;上午10:07:32
	 */
	public static String array2StringByComma(String args[]){
		return array2String(args, ",");
	}
	                     
	/**
	 * @description: 数组转换成字符串，以逗号拼接
	 * @param args
	 * @return
	 * @author: leroy
	 * @createDate 2013-5-8;上午10:07:32
	 */
	public static String array2charByComma(char[] args[]){
		return array2char(args, ",");
	}
	/**
	 * @description: 判断数据类型
	 * @param <T>
	 * @param t
	 * @return
	 * @author: leroy
	 * @createDate 2013-3-21;下午02:54:13
	 */
	public static <T> String getType(T t){ 
		if(t != null){
			 if(t instanceof String){ 
			        return "string"; 
			    }else if(t instanceof Integer){ 
			        return "int"; 
			   }else if(t instanceof Long){ 
			        return "long"; 
			   }else if(t instanceof Boolean){ 
			        return "boolean"; 
			   }else if(t instanceof Double){
			      return "double";
			    }else{
			    	return "do not know"; 
			    }
			        
			}else{
				return null;
			}
		}
	
		/** 
		 * @description: 
		 * @param start
		 * @param end
		 * @return >= start && <=end 的随机数 end需大于start 否则返回 0
		 * @company：leroy
		 * @author: leroy
		 * @createDate 2014-2-19;下午03:50:41
		 */
		public static int getRandom(int start,int end){
			if(start > end || start < 0 || end < 0){
				return 0;
			}
			return (int)(Math.random()*(end-start+1))+start;
		}
		
		/** 
	     * JSON字符串特殊字符处理，比如：“\A1;1300” 
	     * @param s 
	     * @return String 
	     */  
	    public static String string2Json(String s) {        
	        StringBuffer sb = new StringBuffer();
	        if(!notBlank(s)){
	        	return "";
	        }
	        for (int i=0; i<s.length(); i++) { 
	            char c = s.charAt(i);    
	             switch (c){ 
	             case '\"':        
	                 sb.append("\\\"");        
	                 break;        
	             case '\\':        
	                 sb.append("\\\\");        
	                 break;        
	             case '/':        
	                 sb.append("\\/");        
	                 break;        
	             case '\b':        
	                 sb.append("\\b");        
	                 break;        
	             case '\f':        
	                 sb.append("\\f");        
	                 break;        
	             case '\n':        
	                 sb.append("\\n");        
	                 break;        
	             case '\r':        
	                 sb.append("\\r");        
	                 break;        
	             case '\t':        
	                 sb.append("\\t");        
	                 break;        
	             default:        
	                 sb.append(c);     
	             }  
	         }      
	        return sb.toString();     
        }  
	    
	    /** 
	     * @description: 返回20位随机字符串
	     * @return
	     * @company：leroy
	     * @author: leroy
	     * @createDate 2014-4-2;下午02:49:23
	     */
	    public static String createRandomCode(){
	    	SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmss");
	    	return RandomStringUtils.randomAlphanumeric(10) + sdf.format(new Date());
	    }
	    
		/** 
		 * @description: 截取字符串
		 * @param str
		 * @param subSLength
		 * @return
		 * @company：leroy
		 * @author: leroy
		 * @createDate 2014-1-18;上午11:16:36
		 */
		public static String subString(String str,int subSLength){
			
			int tempSubLength = subSLength;
			String subStr=str;
			int subStrByetsL;
			try {
				subStrByetsL = str.getBytes("GBK").length;
				
				while (subStrByetsL > tempSubLength){    
		            int subSLengthTemp = --subSLength;  
		            subStr = str.substring(0, subSLengthTemp>str.length() ? str.length() : subSLengthTemp);    
		            subStrByetsL = subStr.getBytes("GBK").length;  
		        } 
				
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			return subStr;

		}

}
