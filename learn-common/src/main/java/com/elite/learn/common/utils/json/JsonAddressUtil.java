package com.elite.learn.common.utils.json;

public class JsonAddressUtil {
	private Integer Id;
	private String userId;
	private String provinceId;
	private String cityId;
	private String address;
	private String conact;
	private String mobile;
	private String phone;
	private String postcode;
	private String acquiesce; 
	private String email; 
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getConact() {
		return conact;
	}
	public void setConact(String conact) {
		this.conact = conact;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getAcquiesce() {
		return acquiesce;
	}
	public void setAcquiesce(String acquiesce) {
		this.acquiesce = acquiesce;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
