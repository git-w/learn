package com.elite.learn.common.core.exception;

import com.elite.learn.common.core.result.R;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.utils.string.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;


/**
 * Title: 全局异常处理切面 Description: 利用 @ControllerAdvice + @ExceptionHandler
 * 组合处理Controller层RuntimeException异常
 *
 * @author: leroy
 * @created 2017年7月4日 下午4:29:07
 */
@RestControllerAdvice
@ResponseBody
@Slf4j
public class ExceptionAspect {


    /**
     * 400 错误 无法读取json
     *
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public R handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        log.error("无法读取JSON...", e);
        return R.error(HttpStatus.BAD_REQUEST.value(), "无法读取JSON");
    }


    /**
     * 500
     *
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R handleValidationException(MethodArgumentNotValidException e) {
        log.error("参数验证异常...", e);
        BindingResult result = e.getBindingResult();
        ObjectError objectError = result.getAllErrors().stream().findFirst().get();
        log.error("参数验证异常...", objectError.getDefaultMessage());
        return R.error(BasePojectExceptionCodeEnum.ERROR_CODE.code, objectError.getDefaultMessage());
    }


    /**
     * Assert异常
     *
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = IllegalArgumentException.class)
    public R handler(IllegalArgumentException e) {
        log.error("Assert异常：----------------{}", e.getMessage());
        return R.error(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }


    /**
     * 运行时异常
     * @param e
     * @return
     */
/*
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = RuntimeException.class)
    public R handler(RuntimeException e) {
        String message=e.getMessage();
        if(StringUtil.isEmpty(e.getMessage())){
            message="运行时异常!";
        }
        log.error("运行时异常：----------------{}", e.getMessage());
        return PojectBaseQuickException.error(500,message);
    }
*/


    /**
     * 404
     *
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public R handlerNotFoundException(NoHandlerFoundException e) {
        log.error("请求的资源不可用", e);
        return R.error(HttpStatus.NOT_FOUND.value(), "请求的资源不可用");
    }


    /**
     * 405
     *
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public R handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        log.error("不合法的请求方法", e);
        return R.error(HttpStatus.METHOD_NOT_ALLOWED.value(), "不合法的请求方法");
    }


    /**
     * 415-Unsupported Media Type.HttpMediaTypeNotSupportedException是ServletException的子类，需要Serlet API支持
     */
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler({HttpMediaTypeNotSupportedException.class})
    public R handleHttpMediaTypeNotSupportedException(Exception e) {
        log.error("内容类型不支持...", e);
        return R.error(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), "内容类型不支持");
    }


    /**
     * 500 - Internal Server Error
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public R handleException(Exception e) {
        String message = "内部服务错误";
        if (StringUtil.isNotEmpty(e.getMessage())) {
            message = e.getMessage();
        }
        //判断是否是权限不足
        if (e instanceof AccessDeniedException) {
            log.error("权限不足...", e);
            return R.error(BasePojectExceptionCodeEnum.Insufficient_IAuthority);
        }
        log.error("内部服务错误...", e);
        return R.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
    }

    /**
     * 权限不足
     *
     * @param e
     * @return
     */
    @ExceptionHandler(AccessDeniedException.class)
    public R handleAuthorizationException(AccessDeniedException e) {
        log.error("权限不足...", e.getMessage());
        return R.error(BasePojectExceptionCodeEnum.Insufficient_IAuthority);
    }

    /**
     * ，在成功验证用户身份后 LoginModule，可以确定该用户的帐号已到期
     *
     * @param e
     * @return
     */
    @ExceptionHandler(AccountExpiredException.class)
    public R handleAccountExpiredException(AccountExpiredException e) {
        log.error("账号过期...", e.getMessage());
        log.error(e.getMessage(), e);
        return R.error(e.getMessage());
    }

    /**
     * 用户不存在
     *
     * @param e
     * @return
     */
    @ExceptionHandler(UsernameNotFoundException.class)
    public R handleUsernameNotFoundException(UsernameNotFoundException e) {
        log.error("用户不存在...", e.getMessage());
        log.error(e.getMessage(), e);
        return R.error(e.getMessage());
    }

    /**
     * 自定义异常
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = CommonException.class)
    public R domainExceptionHandler(HttpServletRequest req, CommonException e) {
        e.printStackTrace();
        log.error("自定义异常...", e);
        log.error("自定义异常...", e.getMessage());
        return R.error(e.getErrCode(), e.getMessage());
    }

    /**
     * 工具类异常
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = UtilException.class)
    public R utilException(HttpServletRequest req, CommonException e) {
        e.printStackTrace();
        log.error("工具类异常...", e);
        log.error("工具类异常...", e.getMessage());
        return R.error(e.getErrCode(), e.getMessage());
    }

}
