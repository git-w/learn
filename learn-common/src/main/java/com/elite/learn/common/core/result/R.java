package com.elite.learn.common.core.result;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.utils.string.StringUtil;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.ibatis.exceptions.TooManyResultsException;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


@AllArgsConstructor //会生成一个包含所有变量的构造方法，默认生成的方法是 public 的
@Builder
@Accessors(chain = true)
@Getter
@Setter
public class R<T> implements Serializable {
    /**
     * 提示信息
     */
    protected String message;
    /**
     * 状态码
     */
    protected Integer code;
    /**
     * 是否成功
     */
    protected boolean success;
    /**
     * 数据
     */
    protected T data;

    public static R error() {
        return error(500, "未知异常，请联系管理员");
    }

    public static R error(String msg) {
        return error(500, msg);
    }

    public static R error(int code, String msg) {
        R r = new R();
        r.code = code;
        r.message = msg;
        return r;
    }

    public static R error(BasePojectExceptionCodeEnum code) {
        return error(code.code, code.msg);
    }

    /**
     * 未登录错误
     *
     * @param code
     * @return
     */
    public static R loginError(BasePojectExceptionCodeEnum code) {
        R r = new R();
        r.code = code.code;
        r.message = code.msg;
        r.success = false;
        r.data = null;
        return r;
    }

    public R() {
        this.code = 0;
        this.success = true;
        this.data = null;
    }


    public R(String message) {
        this.message = message;
    }

    public static R<?> ok() {
        return R.ok(BasePojectExceptionCodeEnum.SUCCESS);
    }

    public R<Map<String, Object>> put(String key, Object value) {
        HashMap<String, Object> map;
        if (this.data == null) {
            map = new HashMap<>();
        } else {
            map = (HashMap<String, Object>) this.data;
            map.put(key, value);
        }
        this.data = (T) map;
        return (R<Map<String, Object>>) this;
    }

    public R(Integer code, String message) {
        this.code = code;
        this.message = message;
    }


    public R(boolean success, String message) {
        this.success = success;
        this.message = message;
    }


    public R(Integer code, boolean success, String message, T data) {
        this.code = code;
        this.success = success;
        this.message = message;
        this.data = data;

    }

    public static <K> R<K> ok(K obj) {
        R<K> r = new R<>();
        r.data = obj;
        return r;
    }

    public R<T> R(BasePojectExceptionCodeEnum code) {
        R r = new R<>();
        r.code = code.code;
        r.success = true;
        r.message = code.msg;
        r.data = "";
        return r;

    }

    public R<T> R(BasePojectExceptionCodeEnum code, T data) {
        R<T> r = new R<>();
        r.code = code.code;
        r.success = true;
        r.message = code.msg;
        r.data = data;
        return r;

    }

    public static <K> R<K> ok(BasePojectExceptionCodeEnum code, K obj) {
        R<K> r = new R<>();
        r.code = code.code;
        r.success = true;
        r.message = code.msg;
        r.data = obj;
        return r;
    }

    public static <K> R<K> ok(String message) {
        R<K> r = new R<>();
        r.code = BasePojectExceptionCodeEnum.SUCCESS.code;
        r.success = true;
        r.message = message;
        return r;
    }

    public static <K> R<K> ok(String message, K obj) {
        R<K> r = new R<>();
        r.code = BasePojectExceptionCodeEnum.SUCCESS.code;
        r.success = true;
        r.message = message;
        r.data = obj;
        return r;
    }

    public static <K> R<K> ok(Integer code, String message, K obj) {
        R<K> r = new R<>();
        r.code = code;
        r.success = true;
        r.message = message;
        r.data = obj;
        return r;
    }

    public static <K> R<K> error(Integer code, String message, K obj) {
        R<K> r = new R<>();
        r.code = code;
        r.success = true;
        r.message = message;
        r.data = obj;
        return r;
    }

    public R(Exception ex) {
        this.code = -1;
        this.success = false;
        if (ex.getMessage() == null) {
            this.message = "参数不正确或不完整";
        } else if (ex.getCause() instanceof SQLException) {
            this.message = "服务器异常：执行数据库语句错误";
        } else if (ex.getCause() instanceof TooManyResultsException) {
            this.message = "应该查询出一条结果,但查询出多条结果.";
        } else {
            setMessage(ex.getMessage());
        }
    }

    public void setMessage(String message) {
        if (StringUtil.isNotEmpty(message)) {
            message = message.replace("\"", "'").replace('\r', '\0');
        }
        this.message = message;
    }


    @Override
    public String toString() {
        StringBuffer sbJson = new StringBuffer("{\"code\":");
        sbJson.append(this.code)
                .append(",\"message\":\"")
                .append(this.message)
                .append("\",")
                .append("\"success\":")
                .append(this.success)
                .append("}");
        return sbJson.toString();

    }

}
