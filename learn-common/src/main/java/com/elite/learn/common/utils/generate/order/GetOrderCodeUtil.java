package com.elite.learn.common.utils.generate.order;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description: 生成订单号
 * @data: 2020/3/23 16:12
 */
public class GetOrderCodeUtil {

    /**
     * @Description: 生成各个类型订单号
     * @author: leroy
     * @data: 2020/3/23 16:14
     * @param: MS民宿
     */
    public static String getOrderCodeByStr(String orderStr) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
        String verificationCode = String.valueOf((int) ((Math.random() * 9 + 1) * 1000));
        return orderStr + df.format(new Date()) + verificationCode;
    }

}
