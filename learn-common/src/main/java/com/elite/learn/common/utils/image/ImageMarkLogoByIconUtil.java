package com.elite.learn.common.utils.image;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageMarkLogoByIconUtil {
   

	public static void main(String[] args) {
			Font font = new Font("宋体", Font.PLAIN, 14);
			// 原图位置, 输出图片位置, 水印文字颜色, 水印文字
			// new MarkText4J().mark("eguidMarkText2.jpg", "eguidMarkText2.jpg", "水印效果测试", font, Color.ORANGE, 0, 14);
			// 增加图片水印
			//new MarkText4J().mark("eguidMarkText2.jpg", "eguid.jpg", "eguidMarkText3.jpg", 40, 20, 0, 14);

	}
	public void testImage(){
        markImageByText("hello world","D:/1.jpg","D:2.jpg",45,new Color(0,0,0),"JPG");
    }
 
 
    /**
     * 给图片添加水印文字、可设置水印文字的旋转角度
     * @param logoText 要写入的文字
     * @param srcImgPath 源图片路径
     * @param newImagePath 新图片路径
     * @param degree 旋转角度
     * @param color  字体颜色
     * @param formaName 图片后缀
     */
    public static void markImageByText(String logoText, String srcImgPath,String newImagePath,Integer degree,Color color,String formaName) {
        InputStream is = null;
        OutputStream os = null;
        try {
            // 1、源图片
            Image srcImg = ImageIO.read(new File(srcImgPath));
            BufferedImage buffImg = new BufferedImage(srcImg.getWidth(null),srcImg.getHeight(null), BufferedImage.TYPE_INT_RGB);
            // 2、得到画笔对象
            Graphics2D g = buffImg.createGraphics();
            // 3、设置对线段的锯齿状边缘处理
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.drawImage(srcImg.getScaledInstance(srcImg.getWidth(null), srcImg.getHeight(null), Image.SCALE_SMOOTH), 0, 0, null);
            // 4、设置水印旋转
            if (null != degree) {
                g.rotate(Math.toRadians(degree),  buffImg.getWidth()/2,buffImg.getHeight() /2);
            }
            // 5、设置水印文字颜色
            g.setColor(color);
            // 6、设置水印文字Font
            g.setFont(new Font("宋体", Font.BOLD, buffImg.getHeight() /2));
            // 7、设置水印文字透明度
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, 0.15f));
            // 8、第一参数->设置的内容，后面两个参数->文字在图片上的坐标位置(x,y)
            g.drawString(logoText,  buffImg.getWidth()/2 , buffImg.getHeight()/2);
            // 9、释放资源
            g.dispose();
            // 10、生成图片
            os = new FileOutputStream(newImagePath);
            ImageIO.write(buffImg, formaName, os);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != is)
                    is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (null != os)
                    os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
 
    
    
    /**
	 * 给图片增加文字水印
	 * 
	 * @param imgPath
	 *            -要添加水印的图片路径
	 * @param outImgPath
	 *            -输出路径
	 * @param text-文字
	 * @param font
	 *            -字体
	 * @param color
	 *            -颜色
	 * @param x
	 *            -文字位于当前图片的横坐标
	 * @param y
	 *            -文字位于当前图片的竖坐标
	 */
	public void mark(String imgPath, String outImgPath, String text, Font font, Color color, int x, int y) {
		try {
			// 读取原图片信息
			File imgFile = null;
			Image img = null;
			if (imgPath != null) {
				imgFile = new File(imgPath);
			}
			if (imgFile != null && imgFile.exists() && imgFile.isFile() && imgFile.canRead()) {
				img = ImageIO.read(imgFile);
			}
			int imgWidth = img.getWidth(null);
			int imgHeight = img.getHeight(null);
			// 加水印
			BufferedImage bufImg = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);
			mark(bufImg, img, text, font, color, x, y);
			// 输出图片
			FileOutputStream outImgStream = new FileOutputStream(outImgPath);
			ImageIO.write(bufImg, "jpg", outImgStream);
			outImgStream.flush();
			outImgStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 给图片增加图片水印
	 * 
	 * @param inputImg
	 *            -源图片，要添加水印的图片
	 * @param markImg
	 *            - 水印图片
	 * @param outputImg
	 *            -输出图片(可以是源图片)
	 * @param width
	 *            - 水印图片宽度
	 * @param height
	 *            -水印图片高度
	 * @param x
	 *            -横坐标，相对于源图片
	 * @param y
	 *            -纵坐标，同上
	 */
	public void mark(String inputImg, String markImg, OutputStream os, int width, int height, int x, int y) {
		// 读取原图片信息
		File inputImgFile = null;
		File markImgFile = null;
		Image img = null;
		Image mark = null;
		try {
			if (inputImg != null && markImg != null) {
				inputImgFile = new File(inputImg);
				markImgFile = new File(markImg);
			}
			if (inputImgFile != null && inputImgFile.exists() && inputImgFile.isFile() && inputImgFile.canRead()) {
 
				img = ImageIO.read(inputImgFile);
 
			}
			if (markImgFile != null && markImgFile.exists() && markImgFile.isFile() && markImgFile.canRead()) {
 
				mark = ImageIO.read(markImgFile);
 
			}
			int imgWidth = img.getWidth(null);
			int imgHeight = img.getHeight(null);
			BufferedImage bufImg = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);
			mark(bufImg, img, mark, width, height, x, y);
			//FileOutputStream outImgStream = new FileOutputStream(outputImg);
			ImageIO.write(bufImg, "jpg", os);
			os.flush();
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// 加文字水印
	public void mark(BufferedImage bufImg, Image img, String text, Font font, Color color, int x, int y) {
		Graphics2D g = bufImg.createGraphics();
		g.drawImage(img, 0, 0, bufImg.getWidth(), bufImg.getHeight(), null);
		g.setColor(color);
		g.setFont(font);
		g.drawString(text, x, y);
		g.dispose();
	}

	// 加图片水印
		public void mark(BufferedImage bufImg, Image img, Image markImg, int width, int height, int x, int y) {
			Graphics2D g = bufImg.createGraphics();
			g.drawImage(img, 0, 0, bufImg.getWidth(), bufImg.getHeight(), null);
			g.drawImage(markImg, x, y, width, height, null);
			g.dispose();
		}
		
		
		
		
}
