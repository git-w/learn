package com.elite.learn.common.core.type;

/**
 * @Title :StateType.java
 * @Package: com.elite.learn.common.entity.type
 * @Description:
 * @author: leroy
 * @data: 2020/4/29 13:18
 */
public enum UserStateType {

    STATETRUE(0,"正常"),
    STATEERROR(1,"冻结"),
    STATEFALSE(2,"未启用");

    private Integer code;

    private String message;

    UserStateType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
}
