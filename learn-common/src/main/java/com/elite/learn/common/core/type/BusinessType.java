package com.elite.learn.common.core.type;

/**
 * @Title :UserType.java
 * @Package: com.elite.learn.common.entity.type
 * @Description: 业务操作类别
 * @author: leroy
 * @data: 2020/4/29 14:32
 */
public enum  BusinessType{
    /**
     * 其他
     */
    OTHER,

    /**
     * 新增
     */
    INSERT,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE,

    /**
     * 强退
     */
    FORCE,

    /**
     * 更新状态
     */
    STATUS,

    /**
     * 清空数据
     */
    CLEAN;


}
