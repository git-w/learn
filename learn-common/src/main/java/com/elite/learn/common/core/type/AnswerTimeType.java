package com.elite.learn.common.core.type;

public enum AnswerTimeType {
    DAY(0,"日"),
    WEEK(1,"周"),
    MONTH(2,"月"),
    SEASON(3,"季度"),
    YEAR(4,"年");

    private Integer code;

    private String message;

    AnswerTimeType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
}
