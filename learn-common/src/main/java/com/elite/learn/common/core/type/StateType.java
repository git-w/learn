package com.elite.learn.common.core.type;

public enum StateType {
    ISSTATE(0, "是"),
    NOTSTATE(1, "否");

    private Integer code;
    private String message;

    StateType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
}
