package com.elite.learn.common.utils.http.http;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

@Slf4j
public class HttpClientTool {
	private static final String APPLICATION_JSON = "application/json";
	private static final String CONTENT_TYPE_TEXT_JSON = "text/json";

	public static String doHttpGet(String path) {

		HttpClient client = new HttpClient();
		GetMethod  getMethod = new GetMethod(path);

		try {

			int statusCode = client.executeMethod(getMethod);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: "
						+ getMethod.getStatusLine());
				return "statusCode"+statusCode;
			}
			//读取内容 
			byte[] responseBody = getMethod.getResponseBody();
			String resString = new String(responseBody,Charset.forName("GBK"));;
			//处理内容
			return resString;
		}catch (HttpException e) {
			//发生致命的异常，可能是协议不对或者返回的内容有问�?
			e.printStackTrace();
			return "Please check your provided http address!";
		}catch (IOException e) {
			//发生网络异常
			e.printStackTrace();
			return "发生网络异常";
		} finally {
			//释放连接
			getMethod.releaseConnection();
		}
	}
	/**
	 * 参数为字符串
	 * @param url
	 * @param postData
	 * @return
	 */
	public static String doHttpPost(String url, String postData) {
		String data = null;
		try {
			URL dataUrl = new URL(url);
			HttpURLConnection con = (HttpURLConnection) dataUrl.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Proxy-Connection", "Keep-Alive");
			con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			con.setConnectTimeout(5*1000);
			con.setReadTimeout(5*1000);
			con.setDoOutput(true);
			con.setDoInput(true);
			OutputStream os = con.getOutputStream();
			DataOutputStream dos = new DataOutputStream(os);
			dos.write(postData.getBytes("UTF-8"));
			dos.flush();
			dos.close();
			InputStream is = con.getInputStream();
			DataInputStream dis = new DataInputStream(is); 
			byte d[] = new byte[dis.available()];
			dis.read(d);
			data = new String(d, "UTF-8");
			con.disconnect();
		} catch (Exception ex) { 
			ex.printStackTrace();
			return "failure";
		}
		return data;
	}
	/**
	 * 参数为字符串
	 * @param url
	 * @param postData
	 * @return
	 */
	public static String doHttpGet(String url, String postData) {
		String data = null;
		try {
			URL dataUrl = new URL(url);
			HttpURLConnection con = (HttpURLConnection) dataUrl.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Proxy-Connection", "Keep-Alive");
			
			con.setConnectTimeout(5*1000);
			con.setReadTimeout(5*1000);
			con.setDoOutput(true);
			con.setDoInput(true);
			if(postData!=null){
				OutputStream os = con.getOutputStream();
				DataOutputStream dos = new DataOutputStream(os);
				dos.write(postData.getBytes());
				dos.flush();
				dos.close();
			}
			InputStream is = con.getInputStream();
			DataInputStream dis = new DataInputStream(is); 
			StringBuffer desc = new StringBuffer();
			byte[] a = new byte[1024]; 
			int len = 0; 
			while((len = dis.read(a))>0){
				desc.append(new String(a,0,len,"utf-8"));
			}
			data = desc.toString();
			con.disconnect();
		} catch (Exception ex) { 
			ex.printStackTrace();
			return "failure";
		}
		return data;
	}
	/**
	 * 参数为字符串
	 * @param url
	 * @param postData
	 * @return
	 */
	public static String doHttpPostWhile(String url, String postData) {
		String data = null;
		try {
			URL dataUrl = new URL(url);
			HttpURLConnection con = (HttpURLConnection) dataUrl.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			con.setRequestProperty("Content-Length", String.valueOf(postData.getBytes().length));
			con.setConnectTimeout(5*1000);
			con.setReadTimeout(5*1000);
			con.setDoOutput(true);
			con.setDoInput(true);
			if(postData!=null){
				OutputStream os = con.getOutputStream();
				DataOutputStream dos = new DataOutputStream(os);
				dos.write(postData.getBytes());
				dos.flush();
				dos.close();
			}
			
			InputStream is = con.getInputStream();
			DataInputStream dis = new DataInputStream(is); 
			StringBuffer desc = new StringBuffer();
			byte[] a = new byte[9999]; 
			int len = 0; 
			while((len = dis.read(a))>0){
				desc.append(new String(a,0,len,"utf-8"));
			}
			data = desc.toString();
			con.disconnect();
		} catch (Exception ex) { 
			ex.printStackTrace();
			return "failure";
		}
		return data;
	}
	/**
	 * 参数为字节流
	 * @param url
	 * @param postData
	 * @return
	 */
	public static String doHttpPost(String url, byte[] postData){
		String data = null;
		try {
			URL dataUrl = new URL(url);
			HttpURLConnection con = (HttpURLConnection) dataUrl.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Proxy-Connection", "Keep-Alive");
			con.setConnectTimeout(5*1000);
			con.setReadTimeout(5*1000);
			con.setDoOutput(true);
			con.setDoInput(true);
			OutputStream os = con.getOutputStream();
			DataOutputStream dos = new DataOutputStream(os);
			dos.write(postData);
			dos.flush();
			dos.close();
			InputStream is = con.getInputStream();
			DataInputStream dis = new DataInputStream(is);
			byte d[] = new byte[dis.available()];
			dis.read(d);
			data = new String(d, "UTF-8") ;
			con.disconnect();
		} catch (Exception ex) {
			ex.printStackTrace();
			return "failure";
		}

		return data;
	}
	/**
	 * 循环从流中取出数据
	 * @param is
	 * @return
	 */
	public static String convertStreamToString(InputStream is) {      
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));      
		StringBuilder sb = new StringBuilder();      

		String line = null;      
		try {      
			while ((line = reader.readLine()) != null) {  
				sb.append(line + "\n");      
			}      
		} catch (IOException e) {      
			e.printStackTrace();      
		} finally {      
			try {      
				is.close();      
			} catch (IOException e) {      
				e.printStackTrace();      
			}      
		}      
		return sb.toString();  
	}
	/**
	 * 数组传参
	 * @param urlAddress
	 * @param params
	 * @return
	 */
	public static String httpPost(String urlAddress,String []params){
		URL url = null;
		HttpURLConnection con  =null;
		BufferedReader in = null;
		StringBuffer result = new StringBuffer();
		try {
			url = new URL(urlAddress);
			con  = (HttpURLConnection) url.openConnection();
			con.setUseCaches(false);
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			String paramsTemp = "";
			for(String param:params){
				if(param!=null&&!"".equals(param.trim())){
					paramsTemp+="&"+param;
				}
			}
			byte[] b = paramsTemp.getBytes();
			con.getOutputStream().write(b, 0, b.length);
			con.getOutputStream().flush();
			con.getOutputStream().close();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			while (true) {
				String line = in.readLine();
				if (line == null) {
					break;
				}
				else {
					result.append(line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if(in!=null){
					in.close();
				}
				if(con!=null){
					con.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result.toString();
	}

	/**
	 * map传参
	 * @param urlAddress
	 * @param paramMap
	 * @return
	 */
	public static String httpPost(String urlAddress,Map<String, String> paramMap){
		if(paramMap==null){
			paramMap = new HashMap<String, String>();
		}
		String [] params = new String[paramMap.size()];
		int i = 0;
		for(String paramKey:paramMap.keySet()){
			String param = paramKey+"="+paramMap.get(paramMap);
			params[i] = param;
			i++;
		}
		return httpPost(urlAddress, params);
	}

	/**]
	 * 
	 * @param urlAddress
	 * @param paramList
	 * @return
	 */
	public static String httpPost(String urlAddress,List<String> paramList){
		if(paramList==null){
			paramList = new ArrayList<String>();
		}
		return httpPost(urlAddress, paramList.toArray(new String[0]));
	} 

	/**
	 * 网址:http://javacrazyer.iteye.com/blog/1840093
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public String testGetRequest(String url) throws IllegalStateException, IOException {  
		HttpClient client = new HttpClient();  
		StringBuilder sb = new StringBuilder();  
		InputStream ins = null;  
		// Create a method instance.  
		GetMethod method = new GetMethod(url);  
		// Provide custom retry handler is necessary  
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,  
				new DefaultHttpMethodRetryHandler(3, false));  
		try {  
			// Execute the method.  
			int statusCode = client.executeMethod(method);  
			if (statusCode == HttpStatus.SC_OK) {  
				ins = method.getResponseBodyAsStream();  
				byte[] b = new byte[1024];  
				int r_len = 0;  
				while ((r_len = ins.read(b)) > 0) {  
					sb.append(new String(b, 0, r_len, method  
							.getResponseCharSet()));  
				}  
			} else {  
				System.err.println("Response Code: " + statusCode);  
			}  
		} catch (HttpException e) {  
			System.err.println("Fatal protocol violation: " + e.getMessage());  
		} catch (IOException e) {  
			System.err.println("Fatal transport error: " + e.getMessage());
		} finally {
			method.releaseConnection();
			if (ins != null) {
				ins.close();  
			}  
		}  
		return  sb.toString();
	}  

	/**
	 * post提交数据
	 * @param url
	 * @param parms
	 * @throws HttpException
	 * @throws IOException
	 */
	public void testPostRequest(String url,String[] parms) throws HttpException, IOException {  
		HttpClient client = new HttpClient();  
		PostMethod method = new PostMethod("url");  
		method.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=gb2312");  
		NameValuePair[] param = { new NameValuePair("age", parms[0]),
				new NameValuePair("name", parms[1]), };
		method.setRequestBody(param);  
		int statusCode = client.executeMethod(method);  
		method.releaseConnection();  
	}
	/**
	 * 本地图片上传到微信
	 * @param f  图片
	 * @param strUrl  需要处理信息的url
	 * @return
	 */
	public static String getImgURL(File f,String strUrl) {
		String message = null;
		try {
			URL url = new URL(strUrl);
			HttpURLConnection http = (HttpURLConnection) url.openConnection();

			http.setRequestMethod("POST");
			http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);
			System.setProperty("sun.net.client.defaultConnectTimeout", "30000");// 连接超时30秒
			System.setProperty("sun.net.client.defaultReadTimeout", "30000"); // 读取超时30秒

			http.connect();
			OutputStream os = http.getOutputStream();
			InputStream iss=new FileInputStream(f);
			byte[] b=new byte[1024];
			int a=-1;
			
			while((a = iss.read(b) )!= -1)
			{
				os.write(b,0,a);
			}
			iss.close();
			log.info("上传完毕");
			os.flush();
			os.close();

			InputStream is = http.getInputStream();
			int size = is.available();
			byte[] jsonBytes = new byte[size];
			is.read(jsonBytes);
			message = new String(jsonBytes, "UTF-8");
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return message;
	}
	

}