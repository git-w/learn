package com.elite.learn.common.core.type;/**
 * @description:
 * @projectName:learn
 * @see:com.elite.learn.rest.util
 * @author: leroy
 * @createTime:2020/11/6 14:06
 * @version:1.0
 */

/**
 *
 * @Title  :OperationType.java  
 * @Package: com.elite.learn.rest.util
 * @Description:
 * @author: leroy
 * @data: 2020/11/6 14:06 
 */
public enum  OperationType {
    /**
     * 操作类型
     */
    UNKNOWN("unknown"),
    DELETE("delete"),
    SELECT("select"),
    UPDATE("update"),
    SAVE("save");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    OperationType(String s) {
        this.value = s;
    }
}