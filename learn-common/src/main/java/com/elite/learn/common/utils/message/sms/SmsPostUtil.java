
package com.elite.learn.common.utils.message.sms;



import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

/**
 * 短信工具类
 *
 */
public class SmsPostUtil {
	public static final String tempUserName = "LTAI55JHKeVdPAnk";
	public static final String tempUserPasswd = "DwSehKKAGTxKQSA2ZIyP3MnI0SzsQj";
	public static final String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
	public static final String alitemplateContent="SMS_167964838";//用户登录注册的
	public static final  String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）

	/**
	 *
	 * @Title  : SmsPostTool.java
	 * @Package: com.leroy.sms.action
	 * @author : leroy
	 * @version: V1.0
	 * @data   : 2019年6月16日下午3:12:01
	 * @Description:  阿里云短信发送
	 */
	public static String sendNoteByAliYuns( String tempPhoneAttr ,String code,String templateContents){
		String backData = "接口调用异常,请核实数据是否正确。";
		if(tempUserName!= null && tempUserPasswd!= null && tempUserPasswd.length()>0 && tempPhoneAttr!= null && tempPhoneAttr.length()>0
				&& templateContents!= null && templateContents.length()>0 ){
			// 可自助调整超时时间
			System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
			System.setProperty("sun.net.client.defaultReadTimeout", "10000");
			try {//请求失败这里会抛ClientException异常手动添加的异常处理
				// 初始化acsClient,暂不支持region化
				IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", tempUserName, tempUserPasswd);
				DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
				IAcsClient acsClient = new DefaultAcsClient(profile);
				SendSmsRequest request = new SendSmsRequest(); //组装请求对象
				//使用post提交
				request.setMethod(MethodType.POST);
				//必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
				request.setPhoneNumbers(tempPhoneAttr);//替换自己手机号
				//必填:短信签名-可在短信控制台中找到
				request.setSignName("阳煤");//替换自己短信签名
				//必填:短信模板-可在短信控制台中找到
				request.setTemplateCode(templateContents);//替换自己短信模板ID阿里云上的
				//可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
				//友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
				request.setTemplateParam("{\"code\":\""+code+"\"}");//这里参数用户发送提醒类可以自义定参数
				//可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
				//request.setSmsUpExtendCode("90997");
				//可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
				//request.setOutId("learn");
				//请求失败这里会抛ClientException异常
				SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
				if(sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
					//请求成功
					backData=sendSmsResponse.getCode()+","+sendSmsResponse.getMessage()+","+sendSmsResponse.getBizId();
				}else{
					backData=sendSmsResponse.getCode()+","+sendSmsResponse.getMessage();
				}
			} catch (ServerException e) {
				e.printStackTrace();
			} catch (ClientException e) {
				e.printStackTrace();
			}
		}
		return backData;
	}

	public static void main(String[] args) {
		String s = sendNoteByAliYuns("15652330299", "666" ,"SMS_167964838");
		System.out.println(s);
	}

}