package com.elite.learn.common.tree;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @Title :NodeTree.java
 * @Package: com.elite.learn.common.tree
 * @Description:
 * @author: leroy
 * @data: 2021/5/21 14:42
 */
@Data
public class NodeTree implements Serializable {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 7344223482661293008L;

    /**
     * 树形节点ID
     */
    private String id;

    /**
     * 菜单层级 1:一级菜单 2:二级菜单 依次类推...
     */
    private Integer menuLevel;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 节点状态 tree 表示不是叶子节点，false表示是叶子节点
     */
    private Boolean isParent;

    /**
     * 父节点id
     */
    private String ParentID;

    /**
     * 节点图标
     */
    private String iconPath;

    /**
     * 子节点
     */
    private List<NodeTree> children;

    /**
     * 属性
     */
    private Map<String, String> attributes;


    /**
     * 菜单名称
     */
    private String url;


}