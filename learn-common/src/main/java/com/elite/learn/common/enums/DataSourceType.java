package com.elite.learn.common.enums;

/**
 * 数据源
 * 
 * @author leroy
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
