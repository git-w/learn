package com.elite.learn.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 对象工具类
 *
 * @author: leroy
 * @since 2019-10-23
 */
public class ObjectUtils {

    /**
     * 判断对象是否全部为空
     *
     * @param obj
     * @return
     */
    public static boolean allfieldIsNUll(Object obj) {
        try {
            for (Field field : obj.getClass().getDeclaredFields()) {
                field.setAccessible(true);// 把私有属性公有化
                Object object = field.get(obj);
                if (object instanceof CharSequence) {
                    if (!StringUtils.isEmpty((String) object)) {
                        return false;
                    }
                } else if (object instanceof String[]) {
                    if (((String[]) object).length > 0) {
                        return false;
                    }
                } else {
                    if (null != object) {
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    /**
     * 移除Map集合中值为空的对象
     *
     * @param map
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> mapRemoveWithNullByRecursion(Map<String, Object> map) {
        Set<Entry<String, Object>> set = map.entrySet();
        Iterator<Entry<String, Object>> it = set.iterator();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        while (it.hasNext()) {
            Entry<String, Object> en = it.next();
            if (!(en.getValue() instanceof Map)) {
                if ((null == en.getValue()) || "".equals(en.getValue())) {
                    it.remove();
                }
            } else {
                resultMap = (Map<String, Object>) en.getValue();
                mapRemoveWithNullByRecursion(resultMap);
            }
        }
        return map;
    }
}
