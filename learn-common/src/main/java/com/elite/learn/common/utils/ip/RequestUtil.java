package com.elite.learn.common.utils.ip;

import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;


public class RequestUtil {

    /**
     * 获取访客IP地址
     *
     * @return 访客IP地址
     */
    public static String getIP(HttpServletRequest request) {
        if (request != null) {
            String ip = request.getHeader("x-forwarded-for");
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
                if (ip.equals("127.0.0.1")) {
                    //根据网卡取本机配置的IP
                    InetAddress inet = null;
                    try {
                        inet = InetAddress.getLocalHost();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ip = inet.getHostAddress();
                }
            }
            // 多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
            if (ip != null && ip.length() > 15) {
                if (ip.indexOf(",") > 0) {
                    ip = ip.substring(0, ip.indexOf(","));
                }
            }
            return ip;
        }
        return null;
    }

    /**
     * 获取访问URL
     *
     * @return 访问URL
     */
    public static String getVisitURL(HttpServletRequest request) {
        return request.getRequestURL().toString();
    }

    /**
     * 过滤掉一些特殊字�?
     *
     * @param decodeKeywords
     * @return
     */
    public static String replaceStr(String decodeKeywords) {
        decodeKeywords = decodeKeywords.replace("\"", "\\\"");
        decodeKeywords = decodeKeywords.replace("\n", "\\r\\n");
        decodeKeywords = decodeKeywords.replace("\r", "\\r\\n");
        decodeKeywords = decodeKeywords.replace("\r\n", "\\r\\n");
        decodeKeywords = decodeKeywords.replace("\t", "\\t");
        decodeKeywords = decodeKeywords.replace("\\", "\\");
        decodeKeywords = decodeKeywords.replace("\b", "\\b");
        return decodeKeywords;

    }
}