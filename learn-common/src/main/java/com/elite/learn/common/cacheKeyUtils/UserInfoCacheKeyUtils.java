package com.elite.learn.common.cacheKeyUtils;

import com.elite.learn.common.utils.redis.InnoPlatformConstantsUtil;

/** 
* @ClassName: AccountCacheKeyUtils 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author: leroy  
* @date 2020年3月12日 下午1:15:29 
* @version V1.0 
*/
public class UserInfoCacheKeyUtils {

	private static final String PRE = InnoPlatformConstantsUtil.CACHE_PREX + "USERINFO_";

	/**
	 * 对象缓存key
	 * 
	 * @param id
	 * @return
	 */
	public static String getKey(String id) {
		return PRE + id;
	}


}
