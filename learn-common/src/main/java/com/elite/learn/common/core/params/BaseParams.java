package com.elite.learn.common.core.params;

import lombok.Data;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *  
 *
 * @author: leroy  
 * @version V1.0 
 * @ClassName: BaseParams 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @date 2020年3月11日 下午10:31:43 
 */
@Data
public class BaseParams implements Serializable {

    private static final long serialVersionUID = 3211622965273775061L;

    /**
     * 主键id数组
     */
    @NotBlank(message = "主键id数组,参数为空")
    @NotBlank(groups = Update.class,message = "主键id数组,参数为空")
    private String ids;


    /**
     * 状态值
     */
    private Integer IsState;
    /**
     * 是否启用 0-是 1-否
     */
    @NotNull(groups = Update.class,message = "是否启用,参数为空")
    private Integer isEnabled;

    /**
     * 修改人名称
     */
    private String UpdateOperatorName;
    /**
     * 是否推荐 0是 1否
     */
    private Integer IsType;

    /**
     * 审核状态：0-待审核，1-审核通过 2-审核不通过
     */
    private Integer isAudit;
}
