package com.elite.learn.common.utils.image;


import com.elite.learn.common.utils.string.StringUtil;
import sun.font.FontDesignMetrics;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @Title :WaterMarkUtils.java
 * @Package: com.elite.learn.common.utils.image
 * @Description:利用Java代码给图片加文字 -- 证书 增加文字
 * @author: leroy
 * @Date: 2020/10/30 11:07
 */
public class WaterMarkUtil {

    /**
     * @param srcImgPath       源图片路径
     * @param tarImgPath       保存的图片路径
     * @param waterMarkContent 内容
     * @param markContentColor 水印颜色
     * @param font             水印字体
     * @param rightTrue        是否右边对齐
     * @param x                横向距离
     * @param y                纵向距离
     */
    public void addWaterMark(
            String srcImgPath,
            String tarImgPath,
            String waterMarkContent,
            Color markContentColor,
            boolean rightTrue,
            Font font,
            int y,
            Integer... x
    ) {
        try {
            // 读取原图片信息
            File srcImgFile = new File(srcImgPath);//得到文件
            Image srcImg = ImageIO.read(srcImgFile);//文件转化为图片
            int srcImgWidth = srcImg.getWidth(null);//获取图片的宽
            int srcImgHeight = srcImg.getHeight(null);//获取图片的高
            // 加水印
            BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufImg.createGraphics();
            //背景图宽度的一半是中间线
            //文字长度的一半是偏移量
            //那么 让文字居中的位置就是
            //中间线的宽度减去偏移量的一半
            //文字的偏移量需要通过字体大小来计算
            g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
            g.setColor(markContentColor); //根据图片的背景设置水印颜色
            g.setFont(font);              //设置字体
            //设置水印的坐标

            //是否从右往左对齐
            if (rightTrue) {
                int xSize = srcImgWidth - x[0] - getWordWidth(font, waterMarkContent);
                g.drawString(waterMarkContent, xSize, y);  //画出水印
            } else {
                if (x.length != 0) {
                    g.drawString(waterMarkContent, x[0], y);  //画出水印
                } else {
                    g.drawString(waterMarkContent, ((srcImgWidth / 2) - (getWordWidth(font, waterMarkContent) / 2)), y);  //画出水印
                }
            }

            g.dispose();
            // 输出图片
            FileOutputStream outImgStream = new FileOutputStream(tarImgPath);
            ImageIO.write(bufImg, "jpg", outImgStream);
            outImgStream.flush();
            outImgStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @Description: TODO
     * @author: leroy
     * @date: 2020/10/30 15:05
     * @version: V1.0
     * @url: 获取文字在图片中占用的px
     * @param:
     */
    public int getWordWidth(Font font, String content) {
        FontDesignMetrics metrics = FontDesignMetrics.getMetrics(font);
        int width = 0;
        for (int i = 0; i < content.length(); i++) {
            width += metrics.charWidth(content.charAt(i));
        }
        return width;
    }


    /**
     * @Description: TODO
     * @author: leroy
     * @date: 2020/10/30 16:28
     * @version: V1.0
     * @url: 一行最多十九个字  将一大段文字拆成19个19个的
     * @param:
     */
    public static List<String> subString(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        if (StringUtil.notBlank(str)) {
            if (str.length() <= 19) {
                arrayList.add(str);
            }
            if (str.length() > 19) {
                Double floorNum = Math.floor(str.length() / 19);
                Integer index = floorNum.intValue() + 1;
                for (Integer i = 0; i < index; i++) {
                    if (i == index - 1) {
                        arrayList.add(str.substring(i * 19, str.length()));
                    } else {
                        arrayList.add(str.substring(i * 19, ((i + 1) * 19) - 1));
                    }
                }
            }
        }
        return arrayList;
    }

}