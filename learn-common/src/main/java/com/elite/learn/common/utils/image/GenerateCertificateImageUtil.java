package com.elite.learn.common.utils.image;

/**
 * @Title :GenerateCertificateImage.java
 * @Package: com.elite.learn.common.utils.image
 * @Description: 生成证书图片
 * @author: leroy
 * @Date: 2020/10/30 17:37
 */
public class GenerateCertificateImageUtil {}/*{

//    public static String path = "/Volumes/Files/uploadImage/";
    public static String path = "/temp/CertificateImage/";

    *//**
     * @Description: TODO
     * @author: leroy
     * @date: 2020/10/31 10:53
     * @version: V1.0
     * @url:
     * @param: type
     * @param: userInfo
     * @param: certificate
     * @param: exam
     * @param: sourceNum 分数
     *//*
    public static String getImage(Integer styleType, UserInfoDTO userInfo, Certificate certificate, String examName, BigDecimal sourceNum, String content, String stateStr) throws Exception {
        String toFileUrl = path + "/success/" + userInfo.getId() + "---" + certificate.getId() + ".jpg";
        //1 生成圆形头像
        GenerateAvatarImage.gerRound(DeRepeatUrl.repeatStayOneUrl(userInfo.getPhoto()),
                path + "/userRoundHead/",
                userInfo.getId() + "_certificateHeadPhoto.png");
        String styleImage = "";
        if (certificate.getStyleType().equals(1)) {
            styleImage = "/style/style_one.png";
        }
        if (certificate.getStyleType().equals(2)) {
            styleImage = "/style/style_two.png";
        }
        if (certificate.getStyleType().equals(3)) {
            styleImage = "/style/style_three.png";
        }
        //2 生成证书样式背景
        File background = new File(path + styleImage);
        File headImg = new File(path + "/userRoundHead/" + userInfo.getId() + "_certificateHeadPhoto.png");
        URL chapterImgUrl = new URL(DeRepeatUrl.repeatStayOneUrl(certificate.getSeal()));
        int y = 0;
        if (styleType.equals(1)) {
            y = 638;
        }
        if (styleType.equals(2)) {
            y = 720;
        }
        if (styleType.equals(3)) {
            y = 638;
        }

        //合成证书样式盖章图片
        EditImage.mergeImage(background, headImg, chapterImgUrl, toFileUrl, y);
        if (styleType.equals(2)) {
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    "证书编号：" + certificate.getCode(),
                    new Color(102, 102, 102),
                    false,
                    new Font("CN Heavy Bold",
                            Font.PLAIN, 40),
                    630
            );
        } else {
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    "证书编号：" + certificate.getCode(),
                    new Color(102, 102, 102),
                    false,
                    new Font("CN Heavy Bold",
                            Font.PLAIN, 40),
                    320
            );
        }


        new WaterMarkUtils().addWaterMark(
                toFileUrl,
                toFileUrl,
                certificate.getName(),
                new Color(161, 135, 106, 250),
                false,
                new Font("CN Heavy Bold",
                        Font.PLAIN, 58),
                565
        );

        if (styleType.equals(2)) {
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    userInfo.getNickName(),
                    new Color(51, 51, 51),
                    false,
                    new Font("CN Heavy Bold",
                            Font.PLAIN, 58),
                    1000
            );
        } else {
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    userInfo.getNickName(),
                    new Color(51, 51, 51),
                    false,
                    new Font("CN Heavy Bold",
                            Font.PLAIN, 58),
                    924
            );
        }


        if (styleType.equals(2) || styleType.equals(3)) {
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    "在 《" + examName + "》 获得了",
                    new Color(51, 51, 51),
                    false,
                    new Font("CN Heavy Bold",
                            Font.PLAIN, 40),
                    1172
            );
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    sourceNum.intValue() + "",
                    new Color(51, 51, 51),
                    false,
                    new Font("CN Heavy Bold",
                            Font.BOLD, 88),
                    1320
            );
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    "分",
                    new Color(51, 51, 51),
                    false,
                    new Font("CN Heavy Bold",
                            Font.PLAIN, 40),
                    1320,
                    825
            );
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    stateStr,
                    new Color(51, 51, 51),
                    false, new Font("CN Heavy Bold",
                            Font.BOLD, 56),
                    1540
            );
        } else {

            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    "在 《" + examName + "》 获得了",
                    new Color(161, 135, 106, 250),
                    false,
                    new Font("CN Heavy Bold",
                            Font.PLAIN, 40),
                    1172
            );
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    sourceNum.intValue() + "",
                    new Color(161, 135, 106, 250),
                    false,
                    new Font("CN Heavy Bold",
                            Font.BOLD, 88),
                    1285
            );
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    "分",
                    new Color(161, 135, 106, 250),
                    false,
                    new Font("CN Heavy Bold",
                            Font.PLAIN, 40),
                    1280,
                    825
            );
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    stateStr,
                    new Color(161, 135, 106, 250),
                    false, new Font("CN Heavy Bold",
                            Font.BOLD, 56),
                    1500
            );
        }

        List<String> strList = WaterMarkUtils.subString(content);
        int cellHeight = 80;//行高
        for (int i = 0; i < strList.size(); i++) {
            new WaterMarkUtils().addWaterMark(
                    toFileUrl,
                    toFileUrl,
                    strList.get(i),
                    new Color(102, 102, 102, 225),
                    false, new Font("CN Heavy Bold",
                            Font.PLAIN, 56),
                    1730 + (cellHeight * (i + 1))
            );
        }

        new WaterMarkUtils().addWaterMark(
                toFileUrl,
                toFileUrl,
                certificate.getSignature(),
                new Color(102, 102, 102, 200),
                true,
                new Font("CN Heavy Bold",
                        Font.BOLD, 40),
                2250,
                210
        );

        new WaterMarkUtils().addWaterMark(
                toFileUrl,
                toFileUrl,
                getNowDayDate(),
                new Color(51, 51, 51, 180),
                true,
                new Font("CN Heavy Bold",
                        Font.BOLD, 40),
                2336,
                210
        );
        return toFileUrl;
    }

    public static String getNowDayDate() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.YEAR) + "年" + ((now.get(Calendar.MONTH) + 1)) + "月" + now.get(Calendar.DAY_OF_MONTH) + "日";
    }


    public static void main(String[] args) {
//        try {
//            getImage(
//                    1,
//                    new UserInfo()
//                            .setId("6666")
//                            .setPhoto("https://wx.qlogo.cn/mmopen/vi_32/ianplBgG1grJT5iaYiaQmVfWop7wzxN3PlQYdU7qPdpAgueja7FLw28rgUOoBuibibDwzIFegJdbeHrJntbIh0ic8iaAA/132"),
//                    new Certificate()
//                            .setId("777")
//                            .setStyleType(1),
//                    null,
//                    "100");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

}*/