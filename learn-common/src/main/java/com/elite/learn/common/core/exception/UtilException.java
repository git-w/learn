package com.elite.learn.common.core.exception;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import lombok.Data;

/**
 *  
 *
 * @author: leroy  
 * @version V1.0 
 * @ClassName: Exception 
 * @Description: 工具类异常
 * @date 2020年3月12日 下午10:56:55 
 */
@Data
public class UtilException extends RuntimeException {
    /**
     * 状态码
     */
    private Integer errCode = BasePojectExceptionCodeEnum.Unknown_Exception.code;

    public UtilException() {
        super(BasePojectExceptionCodeEnum.Unknown_Exception.msg);
    }

    public UtilException(Integer code, String message) {
        super(message);
        this.errCode = code;
    }
    public UtilException(String message) {
        super(message);
        this.errCode = BasePojectExceptionCodeEnum.ERROR_CODE.code;
    }
}
