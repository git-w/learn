package com.elite.learn.common.cacheKeyUtils;

import com.elite.learn.common.utils.redis.InnoPlatformConstantsUtil;

/**
 * @Title :CaptchaKeyUtils.java
 * @Package: com.elite.learn.common.cacheKeyUtils
 * @Description: <p></p>
 * @update:
 * @author: ；leroy
 * @data: 2023年03月20日 11:37
 */
public class CaptchaKeyUtils {
    private static final String PRE = InnoPlatformConstantsUtil.CACHE_PREX + "CAPTCHA_";

    /**
     * 对象缓存key
     *
     * @param id
     * @return
     */
    public static String getKey(String id) {
        return PRE + id;
    }
}