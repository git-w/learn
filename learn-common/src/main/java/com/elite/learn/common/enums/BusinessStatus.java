package com.elite.learn.common.enums;

/**
 * 操作状态
 * 
 * @author leroy
 *
 */
public enum BusinessStatus {
	/**
	 * 成功
	 */
	SUCCESS,

	/**
	 * 失败
	 */
	FAIL,
}
