package com.elite.learn.common.core.params;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/** 
* @ClassName: IDParams 
* @Description: 工具类
* @author: leroy  
* @date 2020年3月11日 下午10:47:27 
* @version V1.0 
*/
@Data
public class IDParams implements Serializable {

	private static final long serialVersionUID = -7751934719801156622L;
	/**
	 * 主键id
	 */
	@NotBlank(message = "主键id,参数为空")
	private String id;

}
