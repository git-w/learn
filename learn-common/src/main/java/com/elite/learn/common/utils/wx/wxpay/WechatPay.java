/**
 * 
 */
package com.elite.learn.common.utils.wx.wxpay;

/**
 * @description: 微信扫码支付统一下单参数类
 * @author sun.li2017年8月16日11:47:10 添
 */
public class WechatPay {
	private String appid;// 公众账号
	private String body;// 商品描述
	private String mch_id;// 商户号
	private String nonce_str;// 随机字符串
	private String notify_url;// 通知url
	private String sign;// 签名
	private String out_trade_no;// 商户订单号
	private String spbill_create_ip;//终端IP
	private int total_fee;// 付款金额 单位：分
	private String trade_type;// 交易类型
	/*sun.li2017年8月16日14:26:30 修 S*/
	private String openid;//openId公众号内调用js发起支付用
	/*sun.li2017年8月16日14:26:30 修 E*/
	public String getNonce_str() {
		return nonce_str;
	}
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getMch_id() {
		return mch_id;
	}
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notifyUrl) {
		notify_url = notifyUrl;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String outTradeNo) {
		out_trade_no = outTradeNo;
	}
	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}
	public void setSpbill_create_ip(String spbillCreateIp) {
		spbill_create_ip = spbillCreateIp;
	}
	public int getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(int totalFee) {
		total_fee = totalFee;
	}
	public String getTrade_type() {
		return trade_type;
	}
	public void setTrade_type(String tradeType) {
		trade_type = tradeType;
	}
	/*sun.li2017年8月16日14:26:30 修 S*/
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/*sun.li2017年8月16日14:26:30 修 E*/
}
