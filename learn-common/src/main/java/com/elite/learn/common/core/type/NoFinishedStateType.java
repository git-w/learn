package com.elite.learn.common.core.type;

/**
 * @Title :NoFinishedStateType.java
 * @Package: com.elite.learn.common.entity.type
 * @Description:
 * @author: leroy
 * @data: 2020/5/15 21:47
 */
public enum  NoFinishedStateType {
    NORMAL(0,"正常"),
    BEOVERDUE(1,"逾期"),
    OVER(2,"结束");

    private Integer code;
    private String message;

    NoFinishedStateType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
}
