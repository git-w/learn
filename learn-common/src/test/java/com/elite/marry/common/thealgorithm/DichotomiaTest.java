package com.elite.learn.common.thealgorithm;
/**
 *
 * @Title  :DichotomiaTest.java  
 * @Package: com.elite.learn.common.thealgorithm
 * @Description:<p>
 *     又叫折半查找，要求待查找的序列有序。每次取中间位置的值与待查关键字比较，如果中间位置
 * 的值比待查关键字大，则在前半部分循环这个查找的过程，如果中间位置的值比待查关键字小，
 * 则在后半部分循环这个查找的过程。直到查找到了为止，否则序列中没有待查的关键字。
 * </p>
 * @author: leroy
 * @data: 2021/12/31 13:34 
 */
public class DichotomiaTest {
}