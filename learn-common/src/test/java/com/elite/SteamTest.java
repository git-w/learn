package com.elite;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title： SteamTest
 * @Package： com.elite
 * @Description
 * @Author： Charon
 * @Date： 2021-08-18 16:40
 */
public class SteamTest {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");

        list.stream().forEach(str -> {
            System.out.println(str);
        });

    }
}
