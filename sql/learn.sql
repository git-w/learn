/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 8.0.19 : Database - learn
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`learn` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;

/*Table structure for table `dictionary` */

DROP TABLE IF EXISTS `dictionary`;

CREATE TABLE `dictionary` (
  `d_key` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '字典项编号',
  `d_value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典项的值',
  `d_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典项名称',
  `parent_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典项上级编号',
  `visible` int NOT NULL DEFAULT '1' COMMENT '是否可见，不在字典列表中展现（不能被选中）。但是逻辑上是存在的。',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字典项说明',
  PRIMARY KEY (`d_key`) USING BTREE,
  KEY `dictionary_I1` (`parent_key`) USING BTREE,
  KEY `dictionary_I2` (`d_value`,`parent_key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='字典表';

/*Data for the table `dictionary` */

insert  into `dictionary`(`d_key`,`d_value`,`d_name`,`parent_key`,`visible`,`description`) values ('1000','房源户型','房源户型','0',1,'房源户型'),('100001','1','一居','1000',1,NULL),('100002','2','两居','1000',1,NULL),('100003','3','三居','1000',1,NULL),('100004','4','四居','1000',1,NULL),('1001','入住要求','入住要求','0',1,NULL),('100101','1','接待儿童','1001',1,NULL),('100102','2','接待老人','1001',1,NULL),('100103','3','接待外宾','1001',1,NULL),('100104','4','允许吸烟','1001',1,NULL),('100105','5','允许带宠物','1001',1,NULL),('100106','6','允许做饭','1001',1,NULL),('1002','核心设施','核心设施','0',1,NULL),('100201','1','窗户','1002',1,NULL),('100202','2','热水器','1002',1,NULL),('100203','3','空调','1002',1,NULL),('100204','4','无线WIFI','1002',1,NULL),('100205','5','电视','1002',1,NULL),('100206','6','独立卫浴','1002',1,NULL),('100207','7','一次性用品','1002',1,NULL),('100208','8','冰箱','1002',1,NULL),('100209','9','洗衣机','1002',1,NULL),('1003','居家','居家','0',1,NULL),('100301','1','暖气','1003',1,NULL),('100302','2','新风','1003',1,NULL),('100303','3','沙发','1003',1,NULL),('1004','休闲','休闲','0',1,NULL),('100401','1','投影仪','1004',1,NULL),('100402','2','麻将机','1004',1,NULL),('1005','卫浴','卫浴','0',1,NULL),('100501','1','浴巾','1005',1,NULL),('100502','2','浴缸','1005',1,NULL),('100503','3','洗发水','1005',1,NULL),('100504','4','电吹风','1005',1,NULL),('100505','5','烘干机','1005',1,NULL),('1006','餐厨','餐厨','0',1,NULL),('100601','1','燃气灶','1006',1,NULL),('100602','2','餐具','1006',1,NULL),('100603','3','电热水壶','1006',1,NULL),('100604','4','刀具','1006',1,NULL),('100605','5','餐桌','1006',1,NULL),('1007','儿童设施','儿童设施','0',1,NULL),('100701','1','儿童玩具','1007',1,NULL),('100702','2','儿童桌椅','1007',1,NULL),('100703','3','儿童防护设施','1007',1,NULL),('100704','4','儿童帐篷','1007',1,NULL),('1008','其他服务','其他服务','0',1,NULL),('100801','1','电瓶车接送','1008',1,NULL),('100802','2','行李寄存','1008',1,NULL);

/*Table structure for table `t_activity_order_time` */

DROP TABLE IF EXISTS `t_activity_order_time`;

CREATE TABLE `t_activity_order_time` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '订单时间区间表 主键ID',
  `activity_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动id',
  `activity_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动名称',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户id',
  `classify_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '分类id',
  `order_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '关联订单id',
  `order_num` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单号',
  `to_day_price` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '当日价格',
  `to_day_time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '当日时间',
  `activity_date` date DEFAULT NULL COMMENT '活动日期',
  `join_date` date DEFAULT NULL COMMENT '参与日期',
  `is_use` int DEFAULT '0' COMMENT '0未使用  1已使用  2已过期',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:已删除',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `delete_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段1',
  `extra_params2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段2',
  `extra_params3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段3',
  `extra_params4` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段4',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='活动订单时间区间表模块';

/*Data for the table `t_activity_order_time` */

/*Table structure for table `t_activity_order_user` */

DROP TABLE IF EXISTS `t_activity_order_user`;

CREATE TABLE `t_activity_order_user` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '活动用户中间表 线下管理 主键ID',
  `activity_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '活动ID',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `order_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '订单ID',
  `is_state` int NOT NULL COMMENT '是否签到 0未签到 1已签到',
  `sign_in_time` bigint DEFAULT NULL COMMENT '签到时间',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '名字',
  `pin_codes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证号',
  `mobile_phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号',
  `clothes_size` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '衣服尺码',
  `sex` int DEFAULT NULL COMMENT '1男  2女',
  `age` int DEFAULT NULL COMMENT '年龄',
  `answer_one` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '问题1的答案',
  `answer_two` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '问题2的答案',
  `answer_three` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '问题3的答案',
  `answer_four` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '问题4的答案',
  `answer_five` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '问题5的答案',
  `answer_six` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '问题6的答案',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建者购买时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `delete_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段1',
  `extra_params2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段2',
  `extra_params3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段3',
  `extra_params4` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段4',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='活动用户中间表';

/*Data for the table `t_activity_order_user` */

/*Table structure for table `t_activity_price` */

DROP TABLE IF EXISTS `t_activity_price`;

CREATE TABLE `t_activity_price` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '活动时间价格区间表 主键ID',
  `activity_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动id',
  `start_time` bigint DEFAULT NULL COMMENT '开始时间',
  `end_time` bigint DEFAULT NULL COMMENT '结束时间',
  `vip_discount` decimal(10,2) DEFAULT '0.00' COMMENT '会员折扣',
  `vip_price` decimal(10,2) DEFAULT '0.00' COMMENT '会员价格',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `stock` int NOT NULL DEFAULT '0' COMMENT '库存',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `delete_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段1',
  `extra_params2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段2',
  `extra_params3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段3',
  `extra_params4` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段4',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='活动时间价格区间表';

/*Data for the table `t_activity_price` */

insert  into `t_activity_price`(`id`,`activity_id`,`start_time`,`end_time`,`vip_discount`,`vip_price`,`price`,`stock`,`create_operator_id`,`create_operator_name`,`create_time`,`update_operator_id`,`update_operator_name`,`update_time`,`is_delete`,`delete_operator_id`,`delete_operator_name`,`delete_time`,`extra_params1`,`extra_params2`,`extra_params3`,`extra_params4`) values ('8622c13e471047928fe46ee7f6ec75f3','cb83283ea60d49f789d41e3f168f43de',1644595200000,1644768000000,'0.00','0.01','0.02',97,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640946876392,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8c93fb1542af4063a4545e81cdf609e2','b4a2cc98683e48aa8e32fbe355a87c28',1643472000000,1645459200000,'0.00','111.00','1.00',3,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641797930305,NULL,NULL,NULL,1,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1641798414223,NULL,NULL,NULL,NULL),('8f9bffc38852455eb622695b3dd9492a','b4a2cc98683e48aa8e32fbe355a87c28',1643558400000,1643644800000,'0.00','3.00','1000.00',2,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641797180130,NULL,NULL,NULL,1,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1641798343846,NULL,NULL,NULL,NULL);

/*Table structure for table `t_info_activity` */

DROP TABLE IF EXISTS `t_info_activity`;

CREATE TABLE `t_info_activity` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '活动主键ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '活动名称',
  `msg` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动简介',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '活动描述(富文本)',
  `booking_information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '预定须知',
  `pictures` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动封面',
  `share_it_pictures` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '分享图片',
  `banner` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '活动轮播图最多九张',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动地址',
  `img_path_list` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '图片存放路径（短），多图用,隔开',
  `longitude` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '经度',
  `latitude` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '纬度',
  `strat_time` bigint DEFAULT NULL COMMENT '活动开始时间',
  `end_time` bigint DEFAULT NULL COMMENT '活动结束时间',
  `is_free` int DEFAULT '0' COMMENT '是否免费：0-免费；1-不免费',
  `price` decimal(8,2) DEFAULT '0.00' COMMENT '活动报名费',
  `activity_user_count` int DEFAULT NULL COMMENT '活动名额',
  `category_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '活动分类',
  `is_state` int DEFAULT NULL COMMENT '状态 0在线 1下线',
  `pay_start_time` bigint DEFAULT NULL COMMENT '售卖开始时间',
  `pay_end_time` bigint DEFAULT NULL COMMENT '售卖结束时间',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `delete_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '额外参数1',
  `extra_params2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '额外参数2',
  `extra_params3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '额外参数3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='订单表---活动表';

/*Data for the table `t_info_activity` */

insert  into `t_info_activity`(`id`,`name`,`msg`,`content`,`booking_information`,`pictures`,`share_it_pictures`,`banner`,`address`,`img_path_list`,`longitude`,`latitude`,`strat_time`,`end_time`,`is_free`,`price`,`activity_user_count`,`category_id`,`is_state`,`pay_start_time`,`pay_end_time`,`is_delete`,`create_operator_id`,`create_operator_name`,`create_time`,`update_operator_id`,`update_operator_name`,`update_time`,`delete_operator_id`,`delete_operator_name`,`delete_time`,`extra_params1`,`extra_params2`,`extra_params3`) values ('99ef075900094a1c9c9806bc7ba2fb54','新注册用户会员福利领取',NULL,'<p>learn新注册用户可领取三天免费会员福利！</p>','新注册用户福利领取','group1/M00/00/7A/rBH812HFbUmALxoaAAUtUMAI1Aw991.png','group1/M00/00/7A/rBH812HFbUmALxoaAAUtUMAI1Aw991.png',NULL,'北京市顺义区双丰街道金蝶软件园(龙苑北街)','group1/M00/00/7A/rBH812HFbm-AVkR8AAuV-Xv84-4156.png,','116.665218','40.161842',1640275200000,1656518400000,0,'0.00',999,'a329118fd75645dda2fbd19e39aeaa17',0,1640275200000,1654012800000,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640326776104,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640328981962,NULL,NULL,NULL,NULL,NULL,NULL),('cb83283ea60d49f789d41e3f168f43de','learn',NULL,'<p>1.专业红娘帮您匹配教你谈恋爱</p><p>2.全程跟踪为你解决各种的问题</p><p>3.拒绝婚托助情感之路保驾护航</p>','learn同城相亲会','group1/M00/00/7A/rBH812HFa4OAAWesAAJWBihk4WE956.png','group1/M00/00/7A/rBH812HFa4OAAWesAAJWBihk4WE956.png',NULL,'北京市顺义区双丰街道金蝶软件园(龙苑北街)','group1/M00/00/7A/rBH812HFbKGAIe-CAAsyL131gPg982.png,','116.665028','40.161869',1644595200000,1644768000000,1,'0.01',100,'4f7ea271986341f2bfaa04fbaa98a12a',0,1640275200000,1644681600000,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640326355582,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640946831438,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_info_activity_cate` */

DROP TABLE IF EXISTS `t_info_activity_cate`;

CREATE TABLE `t_info_activity_cate` (
  `id` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `cate_name` varchar(30) NOT NULL COMMENT '分类名称',
  `parent_id` char(32) DEFAULT NULL COMMENT '父分类ID',
  `sort` int DEFAULT '0' COMMENT '排序 数值越大越靠前',
  `memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `is_state` int DEFAULT '0' COMMENT '是否上线 0是 1否',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `create_operator_id` char(32) DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `delete_operator_id` char(32) DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动分类表';

/*Data for the table `t_info_activity_cate` */

insert  into `t_info_activity_cate`(`id`,`cate_name`,`parent_id`,`sort`,`memo`,`is_state`,`is_delete`,`create_operator_id`,`create_operator_name`,`create_time`,`update_operator_id`,`update_operator_name`,`update_time`,`delete_operator_id`,`delete_operator_name`,`delete_time`,`extra_params1`) values ('4f7ea271986341f2bfaa04fbaa98a12a','相亲会',NULL,20,'',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640325966802,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641795591359,NULL,NULL,NULL,NULL),('a329118fd75645dda2fbd19e39aeaa17','会员福利',NULL,19,'新注册用户免费领取会员福利',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640326448694,'fe3cc2241d3f40e38a788a77fcd98581','admin',1644818811019,NULL,NULL,NULL,NULL);

/*Table structure for table `t_info_banner` */

DROP TABLE IF EXISTS `t_info_banner`;

CREATE TABLE `t_info_banner` (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '编号',
  `banner_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'banner图名称',
  `img_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图片存放路径（短）',
  `sort` int DEFAULT '0' COMMENT '排序 数值越大越靠前',
  `start_time` bigint DEFAULT NULL COMMENT '开始显示时间',
  `end_time` bigint DEFAULT NULL COMMENT '结束显示时间',
  `business_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '跳转类型的id 或者图片跳转url',
  `Type` int DEFAULT '0' COMMENT '跳转类型：0：不跳转,1-跳转URL,2-客户列表,3-会员卡列表;4-视频详情;5-客户详情,6-会员卡详情;7-活动列表,8-活动详情,',
  `put_site` int DEFAULT '0' COMMENT '投放位置 0-首页',
  `memo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `is_time` int NOT NULL DEFAULT '0' COMMENT ' 是否时间段显示 0是 1否',
  `is_state` int DEFAULT NULL COMMENT '是否上线 0是 1否',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:已删除',
  `create_operatorid` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者ID（反馈者）',
  `create_operator_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `delete_operator_id` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '额外参数1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='商城首页轮播图';

/*Data for the table `t_info_banner` */

insert  into `t_info_banner`(`id`,`banner_name`,`img_path`,`sort`,`start_time`,`end_time`,`business_id`,`Type`,`put_site`,`memo`,`is_time`,`is_state`,`is_delete`,`create_operatorid`,`create_operator_name`,`create_time`,`update_operator_id`,`update_operator_name`,`update_time`,`delete_operator_id`,`delete_operator_name`,`delete_time`,`extra_params1`) values ('93d7c3e96a2c481e833d8461aa03ee25','首页banner图','group1/M00/00/7A/rBH812HFbKGAIe-CAAsyL131gPg982.png',0,NULL,NULL,'',0,0,NULL,1,0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640325841268,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640328851726,NULL,NULL,NULL,NULL),('ad1f3384d6f14dceafc65697f6a0065f','首图','group1/M00/00/7A/rBH812HFbS2AZF4GAAP4FRlREZQ274.png',0,NULL,NULL,'',0,0,NULL,1,0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640325872775,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640328839700,NULL,NULL,NULL,NULL),('d3fadd05c28241d4b992a84ec3293c7d','首页banner图1','group1/M00/00/7A/rBH812HFbm-AVkR8AAuV-Xv84-4156.png',0,NULL,NULL,'',0,0,NULL,1,0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640325899103,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640328827202,NULL,NULL,NULL,NULL),('d707b30cf01846c7a78cff9ba86b1f8d','新人福利','group1/M00/00/7A/rBH812HFhSGAOM57AAIaXWa1CCk498.png',0,NULL,NULL,'9408868cd79c4b558854d5cb426d5706',9,0,NULL,1,0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640335160609,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_info_blacklist` */

DROP TABLE IF EXISTS `t_info_blacklist`;

CREATE TABLE `t_info_blacklist` (
  `ID` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `UserName` varchar(100) DEFAULT NULL COMMENT '用户名称',
  `ModilePhone` varchar(64) DEFAULT NULL COMMENT '手机号',
  `UpdateOperatorID` char(32) DEFAULT NULL COMMENT '更新者ID',
  `UpdateOperatorName` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `CreateTime` bigint DEFAULT NULL COMMENT '添加时间',
  `UpdateTime` bigint DEFAULT NULL COMMENT '更新时间',
  `IsDelete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `DeleteOperatorID` char(32) DEFAULT NULL COMMENT '删除者ID',
  `DeleteOperatorName` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `DeleteTime` bigint DEFAULT NULL COMMENT '删除时间',
  `Memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `ExtraParams1` varchar(100) DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用',
  `ExtraParams2` varchar(100) DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用',
  `ExtraParams3` varchar(100) DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用',
  `CreateOperatorID` char(32) DEFAULT NULL COMMENT '创建者ID',
  `CreateOperatorName` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='民宿预定-黑名单表';

/*Data for the table `t_info_blacklist` */

/*Table structure for table `t_info_feedback` */

DROP TABLE IF EXISTS `t_info_feedback`;

CREATE TABLE `t_info_feedback` (
  `id` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '用户昵称',
  `mobile_phone` int DEFAULT NULL COMMENT '联系方式',
  `complaints_time` bigint DEFAULT NULL COMMENT '投诉时间',
  `content` text COMMENT '投诉内容',
  `img_path_list` varchar(500) NOT NULL DEFAULT '' COMMENT '图片存放路径（短），多图用,隔开',
  `satis_faction` int DEFAULT '0' COMMENT '满意度 0非常满意 1满意 2一般 3不满意',
  `cate_id` char(32) DEFAULT '' COMMENT '分类id',
  `handing_content` text COMMENT '处理结果',
  `memo` varchar(150) DEFAULT NULL COMMENT '备注',
  `is_state` int DEFAULT NULL COMMENT '处理状态 0:未处理 1:已处理',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:已删除',
  `create_operatorid` char(32) DEFAULT NULL COMMENT '创建者ID（反馈者）',
  `create_operator_name` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `delete_operator_id` char(32) DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  `extra_Params2` varchar(100) DEFAULT NULL COMMENT '额外参数2',
  `extra_Params3` varchar(100) DEFAULT NULL COMMENT '额外参数3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='意见反馈表';

/*Data for the table `t_info_feedback` */

/*Table structure for table `t_info_feedback_cate` */

DROP TABLE IF EXISTS `t_info_feedback_cate`;

CREATE TABLE `t_info_feedback_cate` (
  `id` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `cate_name` varchar(30) NOT NULL COMMENT '分类名称',
  `parent_id` char(32) DEFAULT NULL COMMENT '父分类ID',
  `sort` int DEFAULT '0' COMMENT '排序 数值越大越靠前',
  `memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `is_state` int DEFAULT '0' COMMENT '是否上线 0是 1否',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `create_operator_id` char(32) DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `delete_operator_id` char(32) DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户意见反馈分类表';

/*Data for the table `t_info_feedback_cate` */

insert  into `t_info_feedback_cate`(`id`,`cate_name`,`parent_id`,`sort`,`memo`,`is_state`,`is_delete`,`create_operator_id`,`create_operator_name`,`create_time`,`update_operator_id`,`update_operator_name`,`update_time`,`delete_operator_id`,`delete_operator_name`,`delete_time`,`extra_params1`) values ('21684de39cd147c291823e75a0dacb53','其他异常','',3,'其他异常',1,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639703005628,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641801946227,NULL,NULL,NULL,NULL),('9d3e5869e45f4f6eb0c2f61c42131d21','产品建议','',0,'产品建议',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638841410167,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638841528872,NULL,NULL,NULL,NULL),('b259dbc6038247698f1252758ee733df','支付问题','',1,'支付问题',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638842251336,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638846221037,NULL,NULL,NULL,NULL),('e7f80199206c4f569dbf5cb60322a177','功能异常','',2,'功能异常',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638842275201,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640230561733,NULL,NULL,NULL,NULL);

/*Table structure for table `t_info_group` */

DROP TABLE IF EXISTS `t_info_group`;

CREATE TABLE `t_info_group` (
  `ID` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `GroupName` varchar(50) DEFAULT NULL COMMENT '团队姓名',
  `UserName` varchar(100) DEFAULT NULL COMMENT '用户注册时使用个的帐号',
  `ModilePhone` varchar(64) DEFAULT NULL COMMENT '手机号',
  `TotalOrderNum` int DEFAULT '0' COMMENT '订单总数量',
  `Members` text COMMENT '成员',
  `UpdateOperatorID` char(32) DEFAULT NULL COMMENT '更新者ID',
  `UpdateOperatorName` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `CreateTime` bigint DEFAULT NULL COMMENT '添加时间',
  `UpdateTime` bigint DEFAULT NULL COMMENT '更新时间',
  `IsDelete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `DeleteOperatorID` char(32) DEFAULT NULL COMMENT '删除者ID',
  `DeleteOperatorName` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `DeleteTime` bigint DEFAULT NULL COMMENT '删除时间',
  `Memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `ExtraParams1` varchar(100) DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用',
  `ExtraParams2` varchar(100) DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用',
  `ExtraParams3` varchar(100) DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用',
  `CreateOperatorID` char(32) DEFAULT NULL COMMENT '创建者ID',
  `CreateOperatorName` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='民宿预定-团队客户表';

/*Data for the table `t_info_group` */

/*Table structure for table `t_info_images` */

DROP TABLE IF EXISTS `t_info_images`;

CREATE TABLE `t_info_images` (
  `ID` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '编号',
  `CategoryID` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '分类Id',
  `ImgName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '图片名称',
  `ImgUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '图片地址Url',
  `ImgType` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件内容类型',
  `ImgHeight` int DEFAULT '0' COMMENT '图片高 px',
  `ImgWidth` int DEFAULT '0' COMMENT '图片宽 px',
  `ImgSize` int DEFAULT '0' COMMENT '图片文件大小 字节',
  `Memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `IsDelete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `CreateOperatorID` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `CreateTime` bigint DEFAULT NULL COMMENT '创建时间',
  `UpdateOperatorID` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `UpdateTime` bigint DEFAULT NULL COMMENT '更新时间',
  `DeleteOperatorID` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `DeleteTime` bigint DEFAULT NULL COMMENT '删除时间',
  `ExtraParams1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '额外参数1',
  PRIMARY KEY (`ID`),
  KEY `CategoryID` (`CategoryID`,`IsDelete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='素材库--图片';

/*Data for the table `t_info_images` */

insert  into `t_info_images`(`ID`,`CategoryID`,`ImgName`,`ImgUrl`,`ImgType`,`ImgHeight`,`ImgWidth`,`ImgSize`,`Memo`,`IsDelete`,`CreateOperatorID`,`CreateTime`,`UpdateOperatorID`,`UpdateTime`,`DeleteOperatorID`,`DeleteTime`,`ExtraParams1`) values ('08f0d7e06bf24cf7aa3124952d013b66','df300210e48446c3a6b1f7c8762513a3','3fff450e5fbaf61ac040bb3c446e411.jpg','group1/M00/00/7D/rBH812IYUKOASYaCAAamBCdnL-A293.jpg','image/jpeg',1706,1279,435716,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645760676514,NULL,NULL,NULL,NULL,NULL),('0a53aeae9fd041cca2468e5a2f48f90e','815d3c46fd2f456a800e9f3125fa53db','消息.png','group1/M00/00/7B/rBH812HJLzmAVwpkAAAPQxBslaY827.png','image/png',200,200,3907,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640574778275,NULL,NULL,NULL,NULL,NULL),('0d9ae4234a3b4e078e2196ca22733439','df300210e48446c3a6b1f7c8762513a3','e0839096d79e7b9a4c05625d61cedb8.jpg','group1/M00/00/7D/rBH812IYOLGALZHXAAdMMkVg3x8047.jpg','image/jpeg',1920,1080,478258,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645754546222,NULL,NULL,NULL,NULL,NULL),('164351515c7b4eddbedad81f49b1a64f','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812IxQfiAC8q5AA0UCgXVL5s157.PNG','image/png',693,676,857098,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647395320843,NULL,NULL,NULL,NULL,NULL),('17f9b881bcf64969bdcfdad1d4c378ac','815d3c46fd2f456a800e9f3125fa53db','会员卡管理.png','group1/M00/00/7B/rBH812HJLzmAbjDmAAANMSSd1W4389.png','image/png',200,200,3377,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640574778274,NULL,NULL,NULL,NULL,NULL),('1c1e6defcc984c10927939d191b748ef','df300210e48446c3a6b1f7c8762513a3','766d9267fc21c7c54b0cfa8dc54019c.jpg','group1/M00/00/7D/rBH812IS-FqAH2VzAANbAqdTu00304.jpg','image/jpeg',1706,1280,219906,NULL,0,'09090c4e6d9b4e1f8065cb22cd5cb4dd',1645410394832,NULL,NULL,NULL,NULL,NULL),('22e1fea165d94080a07ffef274779da3','815d3c46fd2f456a800e9f3125fa53db','活动.png','group1/M00/00/7B/rBH812HJLzmACpn4AAATSBMNzfA037.png','image/png',200,200,4936,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640574778274,NULL,NULL,NULL,NULL,NULL),('23b6bae2708942bdaa14d417ca511321','793750dc0b684634a5b2ff5b98852aeb','learn会员卡次卡.jpg','group1/M00/00/7D/rBH812IPV06AKAeYAABOgW1sxzc718.jpg','image/jpeg',136,339,20097,NULL,0,'09090c4e6d9b4e1f8065cb22cd5cb4dd',1645172558180,NULL,NULL,NULL,NULL,NULL),('2413f06b8b714220a00004a913064b01','df300210e48446c3a6b1f7c8762513a3','c77ad0c110980a15ce9a49f581b6633.jpg','group1/M00/00/7D/rBH812IYPRyAEX80AAGFLpbL5Ac564.jpg','image/jpeg',1920,1080,99630,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645755677022,NULL,NULL,NULL,NULL,NULL),('25cc92fc8db64e4594aa2617f58ec175','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7D/rBH812IMkDOALDJVABEkh_9N_AM791.PNG','image/png',780,590,1123463,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1644990515564,NULL,NULL,NULL,NULL,NULL),('2779ad69e2bb439883a8e02b134a2b88','df300210e48446c3a6b1f7c8762513a3','c0da9472002e497e7bb1b13792aac37.jpg','group1/M00/00/7E/rBH812Iz4NuAaD9YAAEeq1L0eGc047.jpg','image/jpeg',709,690,73387,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647567067746,NULL,NULL,NULL,NULL,NULL),('2a97d2c873eb4a9d93f75cec94f9a3bc','df300210e48446c3a6b1f7c8762513a3','35887d5804d8710a746e850dcb5613c.jpg','group1/M00/00/7D/rBH812IYbfGAfJXCAAKCBfuiV6s108.jpg','image/jpeg',1280,958,164357,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645768177740,NULL,NULL,NULL,NULL,NULL),('2f1e511ef6c943a791c9367192d0b961','c33b5cf4b34c42f3ad533da8cdbe30b4','默认标题_头像_2022-02-14+17_28_06.jpg','group1/M00/00/7D/rBH812ILIMOAWqR_AACRfuyejME298.jpg','image/jpeg',400,400,37246,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1644896453973,NULL,NULL,NULL,NULL,NULL),('2f3fe7c10491492397f45010b6b952c3','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812Iq29yAMLWHAAiFX2H2U5M262.PNG','image/png',676,682,558431,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646975964317,NULL,NULL,NULL,NULL,NULL),('3762fb3cd49646c8a01bad808b97ab20','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812IgbyCAGahmAATX9R-AohU622.PNG','image/png',415,349,317429,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646292768593,NULL,NULL,NULL,NULL,NULL),('392bc5ceb78a42cc991632cd4c7cd6dd','df300210e48446c3a6b1f7c8762513a3','7667dbc9e7757ae2112327a591b8b61.jpg','group1/M00/00/7E/rBH812IcKvaAIh5zAASZE4ESeMk925.jpg','image/jpeg',1052,947,301331,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646013177616,NULL,NULL,NULL,NULL,NULL),('39fa40cf91f34549b145cda8dcc3a72d','df300210e48446c3a6b1f7c8762513a3','7ddf867d94788449a01c7d486a8d144.jpg','group1/M00/00/7D/rBH812IXF_SAVH1iAAG414rF19s824.jpg','image/jpeg',720,1440,112855,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645680629267,NULL,NULL,NULL,NULL,NULL),('3b69bc4f5e86490ca4b7494f55efcffb','c33b5cf4b34c42f3ad533da8cdbe30b4','默认标题_头像_2022-02-14+17_28_54.jpg','group1/M00/00/7D/rBH812ILIMOABBLTAAJQeSeszcE985.jpg','image/jpeg',400,400,151673,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1644896453973,NULL,NULL,NULL,NULL,NULL),('3e6b51d23cc848afa881209f20bc8ced','815d3c46fd2f456a800e9f3125fa53db','内容管理.png','group1/M00/00/7B/rBH812HJLzmALXgnAAASIwZj68s635.png','image/png',200,200,4643,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640574778274,NULL,NULL,NULL,NULL,NULL),('424a5cefbc3f49c6987072051a9b40e1','df300210e48446c3a6b1f7c8762513a3','f4d1a72000091691d6091fbd6a1305a.jpg','group1/M00/00/7D/rBH812ILTrSABFB6AAF7K9m3LnY064.jpg','image/jpeg',960,960,97067,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1644908212218,NULL,NULL,NULL,NULL,NULL),('45f7a50e3fe048f99d073afcd4f21a4e','95f71c1f6b094906a5968f25e83dbbb9','我们的故事.png','group1/M00/00/7A/rBH812HFbEaARsgoAAvnKujl-MY175.png','image/png',500,900,780074,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640328263090,NULL,NULL,NULL,NULL,NULL),('4d28eedf517c4112b7fd197541621784','df300210e48446c3a6b1f7c8762513a3','4.PNG','group1/M00/00/7E/rBH812Iqs8qAIDUsAAUgFyMJwiE408.PNG','image/png',410,397,335895,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646965708349,NULL,NULL,NULL,NULL,NULL),('4f59235d065b4cdfbe2e79bdf4cc0ce7','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7D/rBH812IPPpmAKMb2AArqfizMXik829.PNG','image/png',574,578,715390,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645166237441,NULL,NULL,NULL,NULL,NULL),('57a9078b6ac843e08aa2953f9e29847a','95f71c1f6b094906a5968f25e83dbbb9','白首之约封面.png','group1/M00/00/7A/rBH812HFa76APluxAAgIbiZ8wuc112.png','image/png',500,900,526446,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640328126633,NULL,NULL,NULL,NULL,NULL),('5c1a97e98a184cb0a7e6165dc61fbe99','df300210e48446c3a6b1f7c8762513a3','72528d82fd3bdb209669b428d5d8f57.jpg','group1/M00/00/7D/rBH812INv6aASf8aAAJiHu3p-Sw521.jpg','image/jpeg',1613,1080,156190,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645068199008,NULL,NULL,NULL,NULL,NULL),('5ca113f808784210a8e652ebe85eaa9e','df300210e48446c3a6b1f7c8762513a3','7ce69804a3ae5621b4f255a16b8f5dc.jpg','group1/M00/00/7E/rBH812IcKvSAJ43zAAU6fvyTKfM136.jpg','image/jpeg',1600,1200,342654,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646013177616,NULL,NULL,NULL,NULL,NULL),('60c04dba03c2490b8201e94e6c93e61a','df300210e48446c3a6b1f7c8762513a3','2bc3a859333c35668103602a9c4564c.jpg','group1/M00/00/7E/rBH812Iq_RSAeR62AAFnUTmDcus046.jpg','image/jpeg',1440,1080,91985,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646984469396,NULL,NULL,NULL,NULL,NULL),('66e66211289a469a8d88608bc59e7e5f','95f71c1f6b094906a5968f25e83dbbb9','浪漫的事.png','group1/M00/00/7A/rBH812HFa9OAY4y4AAQUrQ-znxo796.png','image/png',500,900,267437,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640328147708,NULL,NULL,NULL,NULL,NULL),('6846b92919494136bdf67c290555ada4','793750dc0b684634a5b2ff5b98852aeb','免费会员.png','group1/M00/00/7A/rBH812HFhSGAOM57AAIaXWa1CCk498.png','image/png',500,900,137821,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640334625970,NULL,NULL,NULL,NULL,NULL),('68a5eabe3ad44d0988d151e91d4c79f8','df300210e48446c3a6b1f7c8762513a3','a23124cfdfdf5abbdc23111db6eef2b.jpg','group1/M00/00/7D/rBH812ILZ1yAYMOnAAFSsmS57OM804.jpg','image/jpeg',960,959,86706,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1644914524664,NULL,NULL,NULL,NULL,NULL),('6b5de876b2d74320b9b46a6c430c06d7','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812IwTsaAfSrPAAEtS3fphnw577.PNG','image/png',640,673,77131,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647333062317,NULL,NULL,NULL,NULL,NULL),('70a5e00cf05446cfa47e7140033ff3f7','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812Iy4wiAZGMiAAeXXmDp-Vw010.PNG','image/png',641,647,497502,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647502088228,NULL,NULL,NULL,NULL,NULL),('73af6668b33d4cb690f1b7a7215e49ef','df300210e48446c3a6b1f7c8762513a3','377d6fcf1bf09310700e9ac3f5278de.jpg','group1/M00/00/7D/rBH812ILUeyACbztAAC0TsS9740872.jpg','image/jpeg',864,864,46158,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1644909036306,NULL,NULL,NULL,NULL,NULL),('744a3f53075348b793a10f1d0067107f','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7D/rBH812IPYh6AfIIGAA0ZfrGewZA377.PNG','image/png',631,621,858494,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645175326577,NULL,NULL,NULL,NULL,NULL),('7468922ccdb84c27a00fdc809134041d','df300210e48446c3a6b1f7c8762513a3','7f6b7cc93cf54ed7f362c08f2dce4b1.jpg','group1/M00/00/7D/rBH812IUeUmAG3EtAALrkY4ZG2Y125.jpg','image/jpeg',1706,1279,191377,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645508938102,NULL,NULL,NULL,NULL,NULL),('780a32e3f86e4f5fa3dbeae2c89e7b96','df300210e48446c3a6b1f7c8762513a3','aa016abdf536ea44876d4b93a343ab8.jpg','group1/M00/00/7D/rBH812INv6aATED3AACUMe9dkz4251.jpg','image/jpeg',960,960,37937,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645068199008,NULL,NULL,NULL,NULL,NULL),('78d8841f53874f5f92feb3ff4e4b289c','aaf85523038c4fa298bebf1abd0b8cd7','learn首页banner图.png','group1/M00/00/7A/rBH812HFbS2AZF4GAAP4FRlREZQ274.png','image/png',500,900,260117,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640328495512,NULL,NULL,NULL,NULL,NULL),('7be79bead5cd415a9f6a53742ff2fa7f','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812IqvgqAKgoUAAqKSVSb60o729.PNG','image/png',580,582,690761,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646968330567,NULL,NULL,NULL,NULL,NULL),('85d39a43abd445c8b07fdbb96d1d1a26','df300210e48446c3a6b1f7c8762513a3','2bc3a859333c35668103602a9c4564c.jpg','group1/M00/00/7E/rBH812Iq_XiAEGFWAAFnUTmDcus384.jpg','image/jpeg',1440,1080,91985,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646984568848,NULL,NULL,NULL,NULL,NULL),('884a8b12050d4dde9ce3d11aa78e2c9e','c33b5cf4b34c42f3ad533da8cdbe30b4','捕获.PNG','group1/M00/00/7E/rBH812IqtgSAEapDAAqKSVSb60o759.PNG','image/png',580,582,690761,NULL,1,'24f9e20bfc61413eb2cabcbd5379a0bc',1646966276801,NULL,NULL,'24f9e20bfc61413eb2cabcbd5379a0bc',1646966302392,NULL),('8b41e0191cd34053a8dc20466e7f95e3','df300210e48446c3a6b1f7c8762513a3','8e3054f58aa8bd33dcfbc3828eaf62a.jpg','group1/M00/00/7D/rBH812IUcjSAO1VOAACgwSqRHR0304.jpg','image/jpeg',800,600,41153,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645507125081,NULL,NULL,NULL,NULL,NULL),('8bb4b28c722842879a6e13a93d884bee','df300210e48446c3a6b1f7c8762513a3','rB8_RmDurT-APwK3AAB6UB-A7ks045.jpg','group1/M00/00/7D/rBH812IKMuKAFwVIAAB6UBXYfLk555.jpg','image/jpeg',800,800,31312,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1644835554354,NULL,NULL,NULL,NULL,NULL),('91e470f0bbef4090bc55763490013cdb','df300210e48446c3a6b1f7c8762513a3','0d426e7f33ebbce1a94d7b9fe72e3a4.jpg','group1/M00/00/7E/rBH812Iq9FGAb3qfAADN0CuKnyk125.jpg','image/jpeg',1247,685,52688,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646982225560,NULL,NULL,NULL,NULL,NULL),('9297d7f9e288498aa5c163baf70e9cdb','df300210e48446c3a6b1f7c8762513a3','459ecb57c2cd1910d789a4b1400f4d0.jpg','group1/M00/00/7E/rBH812IcQ7mAF6zoAAEHtprEYp8384.jpg','image/jpeg',695,696,67510,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646019514433,NULL,NULL,NULL,NULL,NULL),('95a84bf0e5b6487dae08d05b372776ce','df300210e48446c3a6b1f7c8762513a3','fda71398f3b2f06cc33388cf9a4e7e8.jpg','group1/M00/00/7E/rBH812IxRNOASO7KAACZknmtH9k579.jpg','image/jpeg',667,500,39314,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647396051820,NULL,NULL,NULL,NULL,NULL),('98b4f95d4afc412cb030e38b3a6d2028','df300210e48446c3a6b1f7c8762513a3','015b975bb0aa8f5bb39f72fff36f8cd.jpg','group1/M00/00/7D/rBH812IcKvGAVvlhAAItivS4g58928.jpg','image/jpeg',1136,640,142730,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646013177616,NULL,NULL,NULL,NULL,NULL),('9b83c00115ee40f3bc9fdb1da10bc9a3','815d3c46fd2f456a800e9f3125fa53db','系统设置.png','group1/M00/00/7B/rBH812HJLzmAKKPxAAAfgg-5jc8812.png','image/png',200,211,8066,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640574778275,NULL,NULL,NULL,NULL,NULL),('9d7bd6d9b42144c5ac3541daae20b84c','df300210e48446c3a6b1f7c8762513a3','32f6bc8b96d94881967ffae58397c43.png','group1/M00/00/7D/rBH812IYNamALGzKAA2iSYJO190689.png','image/png',686,690,893513,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645753769727,NULL,NULL,NULL,NULL,NULL),('9fad09e1b2804194832a6d51c5f73561','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812IurtqAAaQpAAsIUkke0dU463.PNG','image/png',594,648,723026,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647226586959,NULL,NULL,NULL,NULL,NULL),('9fc43f4719ce4f298e27d2c6fae8907c','95f71c1f6b094906a5968f25e83dbbb9','learn首页banner图.png','group1/M00/00/7A/rBH812HFgtaAWNZ7AAP4FRlREZQ603.png','image/png',500,900,260117,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640334038366,NULL,NULL,NULL,NULL,NULL),('a3ff05c11cd348a4bc528fea9fd09bd9','815d3c46fd2f456a800e9f3125fa53db','日常运营.png','group1/M00/00/7B/rBH812HJLzmAL9QFAAACfD7XM48242.png','image/png',48,48,636,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640574778274,NULL,NULL,NULL,NULL,NULL),('a6f3eb7f3416478c827ecb2e84ec20b4','df300210e48446c3a6b1f7c8762513a3','fdbf0bad43e9ec4373e0852e1cd2b27.jpg','group1/M00/00/7E/rBH812Iz4a2ALz1pAAxUtMSDGqw505.jpg','image/jpeg',1442,1080,808116,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647567277456,NULL,NULL,NULL,NULL,NULL),('a904fcd8dc364d2eba91272e2d0f5af3','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812IurpuAUU1iAAsIUkke0dU991.PNG','image/png',594,648,723026,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647226525860,NULL,NULL,NULL,NULL,NULL),('a99d7fb4bdfe408798d891f97eadde90','793750dc0b684634a5b2ff5b98852aeb','learn会员卡.jpg','group1/M00/00/7D/rBH812IPTj6AUU6pAAH6ktyy0q0699.jpg','image/jpeg',638,1063,129682,NULL,0,'09090c4e6d9b4e1f8065cb22cd5cb4dd',1645170238711,NULL,NULL,NULL,NULL,NULL),('afb3355fd3b54ab89771c4909e18756e','df300210e48446c3a6b1f7c8762513a3','9b891fe4b73ef5253ddcbb861824c27.jpg','group1/M00/00/7E/rBH812Iy9vKAWc0kAAT4g2CIQ1c961.jpg','image/jpeg',1706,1279,325763,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647507187175,NULL,NULL,NULL,NULL,NULL),('b03e9f138c894e6d83e5aa4c3ce72fa5','df300210e48446c3a6b1f7c8762513a3','微信图片_20220316101605.jpg','group1/M00/00/7E/rBH812IxST6AGiIFAAIcGU67Ewo556.jpg','image/jpeg',1239,1239,138265,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647397183442,NULL,NULL,NULL,NULL,NULL),('c0618709082f4db8879581aee47f7fc0','38a6173cddd541b4ae922d290acf6cf6','鹊桥会活动封面.png','group1/M00/00/7A/rBH812HFa4OAAWesAAJWBihk4WE956.png','image/png',500,900,153094,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640328068008,NULL,NULL,NULL,NULL,NULL),('c4c0eab21e06426781ec1464b312fa2d','815d3c46fd2f456a800e9f3125fa53db','视频案例.png','group1/M00/00/7B/rBH812HJLzmATFtBAAAT1uaO9NQ916.png','image/png',200,200,5078,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640574778275,NULL,NULL,NULL,NULL,NULL),('c536642465054312b338236c38bcdced','df300210e48446c3a6b1f7c8762513a3','132.jpg','group1/M00/00/7E/rBH812IxR2qADp1hAAAhKFkfMDY822.jpg','image/jpeg',132,132,8488,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647396715018,NULL,NULL,NULL,NULL,NULL),('c8153a6a45754a99bfcec045c2bc56e4','c33b5cf4b34c42f3ad533da8cdbe30b4','捕获.PNG','group1/M00/00/7E/rBH812Iqtb2ARzZcAAqKSVSb60o493.PNG','image/png',580,582,690761,NULL,1,'24f9e20bfc61413eb2cabcbd5379a0bc',1646966205914,NULL,NULL,'24f9e20bfc61413eb2cabcbd5379a0bc',1646966306485,NULL),('d1944740111a42e4bddd9de337f08d9c','df300210e48446c3a6b1f7c8762513a3','b02f6940b1591c5a30694dde3a7642d.jpg','group1/M00/00/7E/rBH812IwRumAK8FtAA_ootsYqLo263.jpg','image/jpeg',1706,1279,1042594,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647331050012,NULL,NULL,NULL,NULL,NULL),('d4f17b4f53c3475893125e2f20659fe7','df300210e48446c3a6b1f7c8762513a3','微信图片_20220307171545.jpg','group1/M00/00/7E/rBH812IlzdiAQr_tAAo4UoKNcww076.jpg','image/jpeg',1339,1079,669778,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1646644696481,NULL,NULL,NULL,NULL,NULL),('d5254a13d15146eab671f4426c660673','df300210e48446c3a6b1f7c8762513a3','c663d7cf9e789cd450015019a5f8d93.jpg','group1/M00/00/7E/rBH812IcbdqAEX67AArcqQy7aCo017.jpg','image/jpeg',1731,1279,711849,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646030298529,NULL,NULL,NULL,NULL,NULL),('d990dc8d718143749acdd7b49ed66c30','df300210e48446c3a6b1f7c8762513a3','35887d5804d8710a746e850dcb5613c.jpg','group1/M00/00/7D/rBH812IYbd2ABZV-AAKCBfuiV6s788.jpg','image/jpeg',1280,958,164357,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645768157742,NULL,NULL,NULL,NULL,NULL),('db95097b97cb49c18349d692f3ab9ce1','c33b5cf4b34c42f3ad533da8cdbe30b4','头像1.jpg','group1/M00/00/7D/rBH812ILIMWAfoAEAALPV9iQgf0527.jpg','image/jpeg',400,400,184151,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1644896453973,NULL,NULL,NULL,NULL,NULL),('dcff41ee218b43649d231e63cf16991e','aaf85523038c4fa298bebf1abd0b8cd7','learn首页1.png','group1/M00/00/7A/rBH812HFbm-AVkR8AAuV-Xv84-4156.png','image/png',1080,1920,759289,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640328815744,NULL,NULL,NULL,NULL,NULL),('e0b1327aa3b6492dad66899579ce1fee','df300210e48446c3a6b1f7c8762513a3','132.jpg','group1/M00/00/7E/rBH812IxRzaAE1UjAAAhKFkfMDY310.jpg','image/jpeg',132,132,8488,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647396663495,NULL,NULL,NULL,NULL,NULL),('e314f6dffc414fa5aec9d573cf102192','38a6173cddd541b4ae922d290acf6cf6','learn图.png','group1/M00/00/7A/rBH812HFbUmALxoaAAUtUMAI1Aw991.png','image/png',1080,1920,339280,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640328521589,NULL,NULL,NULL,NULL,NULL),('e6a39b9777784255b1e222b700ff263a','df300210e48446c3a6b1f7c8762513a3','de766ce0392dfe1c31572d55aa057c8.jpg','group1/M00/00/7E/rBH812IcKvmAOPqmAAZpbLrQC50000.jpg','image/jpeg',1920,1080,420204,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646013177616,NULL,NULL,NULL,NULL,NULL),('e6fd147bddcd448c86d4e6d1bbbe788b','38a6173cddd541b4ae922d290acf6cf6','浪漫的事.png','group1/M00/00/7A/rBH812HFa4KAUqaDAAQUrQ-znxo540.png','image/png',500,900,267437,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640328068008,NULL,NULL,NULL,NULL,NULL),('e8d92961ab294e7eab67cce0c4c82343','df300210e48446c3a6b1f7c8762513a3','f9abb114e186b32d14b9846c1f7e227.jpg','group1/M00/00/7E/rBH812IcKxGAD-zLAAPkcEgGVpA437.jpg','image/jpeg',1472,828,255088,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646013201495,NULL,NULL,NULL,NULL,NULL),('edea8228a8534a87be2d32b994411be7','df300210e48446c3a6b1f7c8762513a3','b633a4f8362513d03505d1d07f799df.jpg','group1/M00/00/7E/rBH812IcbdWAdo5FAAvehlIOKTY363.jpg','image/jpeg',1799,1279,777862,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646030298529,NULL,NULL,NULL,NULL,NULL),('edf46fa8a2e04bcaaaba8ddb724c65b1','df300210e48446c3a6b1f7c8762513a3','27d2a1f3b1997c22e0833db3d49a291.jpg','group1/M00/00/7D/rBH812INvWCACD6KAAD2xrZOKoA119.jpg','image/jpeg',1440,1080,63174,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1645067616507,NULL,NULL,NULL,NULL,NULL),('eec8660c5e6a46fabcb46217494c761b','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812IxXnyAOwlaAAqJGddsHUY294.PNG','image/png',631,616,690457,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647402621186,NULL,NULL,NULL,NULL,NULL),('f1e2cee55f46410cbc442e13ab10d09b','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812Iy7J6AK2zqAAdmkMkYT3w319.PNG','image/png',626,611,485008,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647504542525,NULL,NULL,NULL,NULL,NULL),('f2c4044f9c504303a2857a04696e54b6','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7D/rBH812IManCAYXdvAAJEMsLVqTw428.PNG','image/png',241,260,148530,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1644980849268,NULL,NULL,NULL,NULL,NULL),('f4d06c25a7684db8802d7c11e46a039f','aaf85523038c4fa298bebf1abd0b8cd7','小图首页.png','group1/M00/00/7A/rBH812HFbKGAIe-CAAsyL131gPg982.png','image/png',607,1080,733743,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640328353248,NULL,NULL,NULL,NULL,NULL),('f56dbb667ac1476da6dde3b4e30224e4','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812Iz4kOAO67vAAO2FziY_ao203.PNG','image/png',697,686,243223,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647567428073,NULL,NULL,NULL,NULL,NULL),('f9e34021c3da4c86901374719e47af7d','32b8cb714e8e44018175849a453fc20c','红娘牵线.png','group1/M00/00/7A/rBH812HFiQeAaxvSAAB5OzhFZMc082.png','image/png',400,400,31035,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640335624285,NULL,NULL,NULL,NULL,NULL),('fae28c4aedb645c98a0155b89ec07f14','df300210e48446c3a6b1f7c8762513a3','c74b749e4032ca00eb959b9cd86e47f.jpg','group1/M00/00/7E/rBH812IdggCAGJucAAkUihtvBSk900.jpg','image/jpeg',2270,1279,595082,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1646100992945,NULL,NULL,NULL,NULL,NULL),('fb5fa905dddb40c98cbab2e8b2f19460','df300210e48446c3a6b1f7c8762513a3','捕获.PNG','group1/M00/00/7E/rBH812I0Tb-AMtrOAAN21aC_uqc170.PNG','image/png',324,328,227029,NULL,0,'24f9e20bfc61413eb2cabcbd5379a0bc',1647594943253,NULL,NULL,NULL,NULL,NULL),('fd190045032340d1b6319418a949bf06','815d3c46fd2f456a800e9f3125fa53db','用户.png','group1/M00/00/7B/rBH812HJLzqADvMQAAAEg9zq668463.png','image/png',48,48,1155,NULL,0,'fe3cc2241d3f40e38a788a77fcd98581',1640574778275,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_info_images_category` */

DROP TABLE IF EXISTS `t_info_images_category`;

CREATE TABLE `t_info_images_category` (
  `ID` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '编号',
  `CategoryName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '分类名称',
  `Memo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `IsRests` int DEFAULT '0' COMMENT '是否是其他 0-是 1-否',
  `IsDelete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `CreateOperatorID` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者ID',
  `CreateTime` bigint DEFAULT NULL COMMENT '创建时间',
  `UpdateOperatorID` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者ID',
  `UpdateTime` bigint DEFAULT NULL COMMENT '更新时间',
  `DeleteOperatorID` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '删除者ID',
  `DeleteTime` bigint DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='素材库--分类表';

/*Data for the table `t_info_images_category` */

insert  into `t_info_images_category`(`ID`,`CategoryName`,`Memo`,`IsRests`,`IsDelete`,`CreateOperatorID`,`CreateTime`,`UpdateOperatorID`,`UpdateTime`,`DeleteOperatorID`,`DeleteTime`) values ('32b8cb714e8e44018175849a453fc20c','icon',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581',1640335614954,NULL,NULL,NULL,NULL),('38a6173cddd541b4ae922d290acf6cf6','鹊桥会活动封面',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581',1640322742444,NULL,NULL,NULL,NULL),('793750dc0b684634a5b2ff5b98852aeb','会员卡封面',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581',1640334606970,NULL,NULL,NULL,NULL),('815d3c46fd2f456a800e9f3125fa53db','菜单',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581',1640315963453,NULL,NULL,NULL,NULL),('95f71c1f6b094906a5968f25e83dbbb9','白首约视频封面',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581',1640318464173,NULL,NULL,NULL,NULL),('aaf85523038c4fa298bebf1abd0b8cd7','banner图',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581',1640316663413,NULL,NULL,NULL,NULL),('c33b5cf4b34c42f3ad533da8cdbe30b4','头像',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581',1644831274742,NULL,NULL,NULL,NULL),('df300210e48446c3a6b1f7c8762513a3','其他',NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_info_member_card` */

DROP TABLE IF EXISTS `t_info_member_card`;

CREATE TABLE `t_info_member_card` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员卡名称',
  `subtitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员卡副标题',
  `img_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品主图',
  `price` decimal(11,2) DEFAULT '0.00' COMMENT '价格',
  `level` tinyint DEFAULT NULL COMMENT '会员级别 数字越大级别越大',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '会员卡使用说明',
  `introduction` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员卡简介',
  `is_infinite` int NOT NULL COMMENT '库存是否无限 0—是 1-否',
  `inventory_count` int DEFAULT '0' COMMENT '库存数量',
  `valid_time_unit` tinyint NOT NULL COMMENT '有效时长单位，1-周，2-月，3-季，4-年',
  `valid_time` int NOT NULL COMMENT '有效时长(周期)，表示几周，几个月',
  `start_time` datetime DEFAULT NULL COMMENT '有效期开始日期',
  `end_time` datetime DEFAULT NULL COMMENT '有效期结束日期',
  `is_enabled` int DEFAULT '0' COMMENT '是否启用 0-是 1-否',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '备注',
  `memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `delete_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段',
  `is_free` int DEFAULT '0' COMMENT '是否免费领取 0-是 1-否',
  `get_num` int DEFAULT NULL COMMENT '领取次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='会员卡信息表';

/*Data for the table `t_info_member_card` */

insert  into `t_info_member_card`(`id`,`name`,`subtitle`,`img_url`,`price`,`level`,`content`,`introduction`,`is_infinite`,`inventory_count`,`valid_time_unit`,`valid_time`,`start_time`,`end_time`,`is_enabled`,`remark`,`memo`,`create_time`,`create_operator_id`,`create_operator_name`,`update_time`,`update_operator_id`,`update_operator_name`,`is_delete`,`delete_operator_id`,`delete_operator_name`,`delete_time`,`extra_params1`,`is_free`,`get_num`) values ('10fa2b7c76cb4f8f8d84e69f08fabb6d','牵线次卡','','group1/M00/00/7D/rBH812IPTj6AUU6pAAH6ktyy0q0699.jpg','2.90',NULL,'<p>凭此卡可享鹊桥牵线三次，有效期1个月。</p>','凭此卡可享鹊桥牵线三次，与心仪的TA极速牵线，成就美好姻缘！',0,0,2,1,'2022-01-31 10:00:00','2023-02-01 09:59:59',0,NULL,NULL,1645175001458,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,1645175163387,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,1,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,1645408306176,NULL,1,NULL),('493e49d262444e79b01b806eb2ffef62','鹊桥牵线次卡','','group1/M00/00/7D/rBH812IPV06AKAeYAABOgW1sxzc718.jpg','2.90',NULL,'<p>此卡有效期1年</p><p>凭此卡可享鹊桥牵线三次，极速牵线你心仪的TA，成就美好姻缘！</p>','凭此卡可享鹊桥牵线三次，极速牵线你心仪的TA，成就美好姻缘！',0,0,4,1,'2022-01-31 10:00:00','2023-02-01 09:59:59',0,NULL,NULL,1645170736060,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,1645172571325,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,1,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,1645408274000,NULL,1,NULL),('6abcbe5e2a8a4374bdc9368c57ad3e6b','新人福利','','group1/M00/00/7A/rBH812HFhSGAOM57AAIaXWa1CCk498.png','0.00',NULL,'<p>凭此卡可享三天VIP会员权益</p><p>参加相关线下活动</p>','凭此卡可享受三天VIP会员权益',0,0,5,3,'2022-01-31 10:00:00','2023-02-01 09:59:59',0,NULL,NULL,1645170938067,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,1),('8f585714666e4d0298ec02872d5f5bb4','测试数据（请勿点击）','','group1/M00/00/7D/rBH812IPV06AKAeYAABOgW1sxzc718.jpg','100.00',NULL,'<p>测试数据（请勿点击）</p>','测试数据（请勿点击）',0,0,1,1,'2022-02-20 10:00:00','2022-02-28 09:59:59',1,NULL,NULL,1645408515278,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1645408550774,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1645408556191,NULL,1,NULL),('9408868cd79c4b558854d5cb426d5706','新人福利','','group1/M00/00/7A/rBH812HFhSGAOM57AAIaXWa1CCk498.png','0.00',NULL,'<p>凭此卡可享受三天VIP会员权益</p><p>参加相关线下活动</p>','凭此卡可享受三天VIP会员权益',0,0,5,3,'2022-01-31 10:00:00','2022-12-31 09:59:59',0,NULL,NULL,1640317474981,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1645170104172,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,1,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,1645408342422,NULL,0,1),('a843143eb90a4ef3bbc85a99237d1845','牵线次卡','','group1/M00/00/7D/rBH812IPTj6AUU6pAAH6ktyy0q0699.jpg','2.90',NULL,'<p>凭此卡可享鹊桥牵线三次，有效期1个月</p>','凭此卡可享受鹊桥牵线三次，与心仪的TA极速牵线，成就美好姻缘！',0,0,2,1,'2022-01-31 10:00:00','2023-02-01 09:59:59',0,NULL,NULL,1645175515899,'09090c4e6d9b4e1f8065cb22cd5cb4dd',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,1,NULL);

/*Table structure for table `t_info_member_card_config` */

DROP TABLE IF EXISTS `t_info_member_card_config`;

CREATE TABLE `t_info_member_card_config` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键ID',
  `card_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '关联会员卡ID',
  `is_type` tinyint DEFAULT NULL COMMENT '关联类型 0-红娘牵线',
  `others_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '关联ID',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片地址',
  `count` int DEFAULT NULL COMMENT '数量',
  `time_interval` int DEFAULT NULL COMMENT '间隔天数',
  `valid_time` int DEFAULT NULL COMMENT '有效天数',
  `is_enabled` int DEFAULT '0' COMMENT '是否启用 0-是 1-否',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '备注',
  `memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `delete_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='会员卡配置表';

/*Data for the table `t_info_member_card_config` */

insert  into `t_info_member_card_config`(`id`,`card_id`,`is_type`,`others_id`,`img_url`,`count`,`time_interval`,`valid_time`,`is_enabled`,`remark`,`memo`,`create_time`,`create_operator_id`,`create_operator_name`,`update_time`,`update_operator_id`,`update_operator_name`,`is_delete`,`delete_operator_id`,`delete_operator_name`,`delete_time`) values ('1a1f91f215a74994ae5934ec73ec35e8','9408868cd79c4b558854d5cb426d5706',0,'','group1/M00/00/7A/rBH812HFiQeAaxvSAAB5OzhFZMc082.png',1,NULL,NULL,0,'',NULL,1640317529042,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640335661890,'fe3cc2241d3f40e38a788a77fcd98581','admin',0,NULL,NULL,NULL),('1d13e1a03b724e59935860ab01fa9e3c','493e49d262444e79b01b806eb2ffef62',0,'','group1/M00/00/7A/rBH812HFiQeAaxvSAAB5OzhFZMc082.png',3,NULL,NULL,0,'',NULL,1645171012787,'09090c4e6d9b4e1f8065cb22cd5cb4dd','13021215093',NULL,NULL,NULL,0,NULL,NULL,NULL),('303bb02d6e5d4d4e830efbffeb8a3890','3cabfb4e734d479dabf86e984efe80bd',0,'','group1/M00/00/7A/rBH812HFiQeAaxvSAAB5OzhFZMc082.png',1,NULL,NULL,1,'',NULL,1641806106770,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641806261908,'fe3cc2241d3f40e38a788a77fcd98581','admin',1,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1641806289887),('4b27df8da8314c66a59da83bcc9345b2','10fa2b7c76cb4f8f8d84e69f08fabb6d',0,'','group1/M00/00/7A/rBH812HFiQeAaxvSAAB5OzhFZMc082.png',3,NULL,NULL,0,'',NULL,1645175019095,'09090c4e6d9b4e1f8065cb22cd5cb4dd','13021215093',NULL,NULL,NULL,0,NULL,NULL,NULL),('916c6d9a18c14a358c9c28fa964dbc44','6abcbe5e2a8a4374bdc9368c57ad3e6b',0,'','group1/M00/00/7A/rBH812HFiQeAaxvSAAB5OzhFZMc082.png',3,NULL,NULL,0,'',NULL,1645171057295,'09090c4e6d9b4e1f8065cb22cd5cb4dd','13021215093',NULL,NULL,NULL,0,NULL,NULL,NULL),('c02203c781364a65a73d4f7d4cebbf9e','a843143eb90a4ef3bbc85a99237d1845',0,'','group1/M00/00/7A/rBH812HFiQeAaxvSAAB5OzhFZMc082.png',3,NULL,NULL,0,'',NULL,1645175531094,'09090c4e6d9b4e1f8065cb22cd5cb4dd','13021215093',NULL,NULL,NULL,0,NULL,NULL,NULL);

/*Table structure for table `t_info_message` */

DROP TABLE IF EXISTS `t_info_message`;

CREATE TABLE `t_info_message` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '主键id',
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '内容',
  `is_undo` int DEFAULT '0' COMMENT '是否撤销 0:未撤销 1:撤销',
  `undo_time` bigint DEFAULT NULL COMMENT '撤销时间',
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '系统消息',
  `is_user_all` int DEFAULT NULL COMMENT '接受者 是否全部用户 0-通知全部用户 1-部分用户',
  `is_message` int DEFAULT NULL COMMENT '信息类型 0-公告 1-个人信息',
  `is_type` char(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '消息类型:system-系统消息 order-订单消息',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发布者',
  `create_time` bigint DEFAULT NULL COMMENT '发布时间',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '额外参数1',
  `extra_Params2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '额外参数2',
  `extra_Params3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '额外参数3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统消息-通告表';

/*Data for the table `t_info_message` */

insert  into `t_info_message`(`id`,`title`,`content`,`is_undo`,`undo_time`,`message`,`is_user_all`,`is_message`,`is_type`,`create_operator_id`,`create_time`,`extra_params1`,`extra_Params2`,`extra_Params3`) values ('0b6a40584fd9482f9739fdcd3504fcb9','新用户注册成功','欢迎使用learn小程序，快去寻找属于你的情缘吧！',0,NULL,NULL,1,1,'4','fe3cc2241d3f40e38a788a77fcd98581',1640654019297,NULL,NULL,NULL),('2f1e7261cccc4add87cd6d6aa6493a05','免费会员卡领取成功','您已免费领取会员卡，此会员卡有效期为3天。可以使用1次红娘牵线和免费参加一次活动。',0,NULL,NULL,1,1,'5','fe3cc2241d3f40e38a788a77fcd98581',1640654074539,NULL,NULL,NULL),('3c00ff986f084e86a1c0317e1f9c9576','线下预约','对您感兴趣的用户向您发出了邀请',0,NULL,NULL,1,0,'0','fe3cc2241d3f40e38a788a77fcd98581',1642670516071,NULL,NULL,NULL),('490295fc45024c3ab242243cce39ee14','审核失败','审核失败，请点击我的-认证中心查看失败原因并重新实名认证！',0,NULL,NULL,1,1,'2','fe3cc2241d3f40e38a788a77fcd98581',1640654103899,NULL,NULL,NULL),('90a1c967f9984bf3a7bc7168b222385f','会员卡购买成功','会员卡购买成功，可点击VIP会员标签查看您购买会员的信息！',0,NULL,NULL,1,1,'3','fe3cc2241d3f40e38a788a77fcd98581',1640654045205,NULL,NULL,NULL),('b67c30228c51459bab73b988eb83a0f8','活动取消','265464',1,NULL,NULL,1,0,'0','fe3cc2241d3f40e38a788a77fcd98581',1641259563866,NULL,NULL,NULL),('e6ffbf6658eb43e99b980852bdfe3df6','审核通过','审核通过，其他用户可在同城和推荐看到您的信息！',0,NULL,NULL,1,1,'1','fe3cc2241d3f40e38a788a77fcd98581',1640653686879,NULL,NULL,NULL);

/*Table structure for table `t_info_order_card` */

DROP TABLE IF EXISTS `t_info_order_card`;

CREATE TABLE `t_info_order_card` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '编号',
  `order_num` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单编号',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `order_status` int DEFAULT NULL COMMENT '0-取消订单  1-未支付、2-支付/待出库、3-出库/待发货、4-已发货 5-完成 6-申请退款 7-退款中 8-退款完成 9-超时未支付取消的',
  `total_money` decimal(8,2) DEFAULT NULL COMMENT '收款总金额',
  `real_cost` decimal(10,2) DEFAULT NULL COMMENT '实付金额(元)',
  `pay_mode` int DEFAULT NULL COMMENT ' 1微信支付',
  `Pay_status` int DEFAULT NULL COMMENT '支付状态，0未支付，1已支付，2已支付定金',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `order_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单备注',
  `pay_time` datetime DEFAULT NULL COMMENT '订单支付时间',
  `pay_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单支付创建者ID',
  `finish_time` datetime DEFAULT NULL COMMENT '完成时间',
  `finish_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '完成创建者ID',
  `out_store_Time` datetime DEFAULT NULL COMMENT '订单申请退货时间',
  `deliver_time` datetime DEFAULT NULL COMMENT '订单发货时间',
  `deliver_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单发货创建者ID',
  `cancel_time` datetime DEFAULT NULL COMMENT '订单取消时间',
  `order_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '0' COMMENT '0.正常订单',
  `memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `delete_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='订单表---会员卡订单表';

/*Data for the table `t_info_order_card` */

/*Table structure for table `t_info_report` */

DROP TABLE IF EXISTS `t_info_report`;

CREATE TABLE `t_info_report` (
  `id` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `user_id` varchar(32) DEFAULT '' COMMENT '用户id',
  `cate_id` char(32) DEFAULT '' COMMENT '分类id',
  `name` varchar(50) DEFAULT NULL COMMENT '用户昵称',
  `mobile_phone` int DEFAULT NULL COMMENT '联系方式',
  `complaints_time` bigint DEFAULT NULL COMMENT '举报时间',
  `content` text COMMENT '举报内容',
  `img_path_list` varchar(500) NOT NULL DEFAULT '' COMMENT '图片存放路径（短），多图用,隔开',
  `satis_faction` int DEFAULT '0' COMMENT '满意度 0非常满意 1满意 2一般 3不满意',
  `memo` varchar(150) DEFAULT NULL COMMENT '备注',
  `handing_content` text COMMENT '处理结果',
  `is_state` int DEFAULT NULL COMMENT '处理状态 0:未处理 1:已处理',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:已删除',
  `create_operatorid` char(32) DEFAULT NULL COMMENT '创建者ID（反馈者）',
  `create_operator_name` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `delete_operator_id` char(32) DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  `extra_Params2` varchar(100) DEFAULT NULL COMMENT '额外参数2',
  `extra_Params3` varchar(100) DEFAULT NULL COMMENT '额外参数3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='举报反馈表';

/*Data for the table `t_info_report` */

insert  into `t_info_report`(`id`,`user_id`,`cate_id`,`name`,`mobile_phone`,`complaints_time`,`content`,`img_path_list`,`satis_faction`,`memo`,`handing_content`,`is_state`,`is_delete`,`create_operatorid`,`create_operator_name`,`create_time`,`update_operator_id`,`update_operator_name`,`update_time`,`delete_operator_id`,`delete_operator_name`,`delete_time`,`extra_params1`,`extra_Params2`,`extra_Params3`) values ('11279916c12948bd97dca4ef554eadab','8980bd541e6545e993dd6bbd62506dc4','7b373b84fd0947daaef217f81dd487c9','leroy',NULL,NULL,'推销','group1/M00/00/7B/rBH812Hbpv-AcVWUAAS3fRhpYgU353.jpg',0,NULL,'双击666',1,0,'72f3e5e7cf8e46a2ab6eea2460538a3c','九亿少女的梦',1641785088044,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641786751820,NULL,NULL,NULL,NULL,NULL,NULL),('203fc61339504ab6b55030f95b3ed136','72f3e5e7cf8e46a2ab6eea2460538a3c','048c26cb4d944e7d94d6d5abc949bcfe','九亿少女的梦',NULL,NULL,'内容不真实','',0,NULL,'知道了',1,0,'8980bd541e6545e993dd6bbd62506dc4','leroy',1640746874819,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641790736193,NULL,NULL,NULL,NULL,NULL,NULL),('446f173abf814cf5bfbaa6fb8cff26b2','','1','张',1121445,0,'','',0,NULL,NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1644920504211,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','');

/*Table structure for table `t_info_report_cate` */

DROP TABLE IF EXISTS `t_info_report_cate`;

CREATE TABLE `t_info_report_cate` (
  `id` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `cate_name` varchar(30) NOT NULL COMMENT '分类名称',
  `sort` int DEFAULT '0' COMMENT '排序 数值越大越靠前',
  `memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `is_state` int DEFAULT '0' COMMENT '是否上线 0是 1否',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `create_operator_id` char(32) DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `delete_operator_id` char(32) DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户举报反馈分类表';

/*Data for the table `t_info_report_cate` */

insert  into `t_info_report_cate`(`id`,`cate_name`,`sort`,`memo`,`is_state`,`is_delete`,`create_operator_id`,`create_operator_name`,`create_time`,`update_operator_id`,`update_operator_name`,`update_time`,`delete_operator_id`,`delete_operator_name`,`delete_time`,`extra_params1`) values ('048c26cb4d944e7d94d6d5abc949bcfe','垃圾广告',1,'垃圾广告',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638840175768,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641786154484,NULL,NULL,NULL,NULL),('195f773454a6498388cd10640f8b78b0','欺诈骗钱',2,'欺诈骗钱',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638840186010,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7b373b84fd0947daaef217f81dd487c9','推销',4,'推销',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638840244870,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('975067e5f69d4d1db99baede7af24646','涉政涉恐',3,'涉政涉恐',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638840201899,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('bf3c57ea2ee841b98ba6aad52dae2f4c','其他',5,'其他',1,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638840254509,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641786120827,NULL,NULL,NULL,NULL),('d340e69a5c9944f59a11cf54c8c85531','骚扰谩骂',0,'骚扰谩骂',0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638840165127,'fe3cc2241d3f40e38a788a77fcd98581','admin',1641786168628,NULL,NULL,NULL,NULL);

/*Table structure for table `t_info_user` */

DROP TABLE IF EXISTS `t_info_user`;

CREATE TABLE `t_info_user` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '编号',
  `true_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '真实姓名',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邮箱',
  `birthday` bigint DEFAULT NULL COMMENT '生日',
  `pin_codes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证号',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户住址',
  `native_place` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '籍贯',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户注册时使用个的帐号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户的昵称',
  `headimgurl` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户头像',
  `modile_phone` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号',
  `routine_openid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '小程序唯一身份ID',
  `sex` tinyint DEFAULT '0' COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `city` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在城市',
  `language` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户的语言，简体中文为zh_CN',
  `province` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在省份',
  `country` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在国家',
  `unionid` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段',
  `openid` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户的标识，对当前公众号唯一',
  `other_uid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '第三方平台返回的user_id',
  `total_integral` decimal(10,2) DEFAULT '0.00' COMMENT '累计总积分',
  `integral` decimal(10,2) DEFAULT '0.00' COMMENT '累计剩余积分',
  `money` decimal(8,2) DEFAULT '0.00' COMMENT '账户金额',
  `total_money` decimal(8,2) DEFAULT '0.00' COMMENT '总账户金额',
  `register_time` datetime DEFAULT NULL COMMENT '注册时间',
  `register_ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '注册ip',
  `login_time` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  `login_ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '登录ip',
  `is_state` int DEFAULT '1' COMMENT '是否上下线 0-是 1-否',
  `is_audit` int DEFAULT '1' COMMENT '是否已审核：0-是，1-否',
  `is_type` int DEFAULT '0' COMMENT '用户类型（0:无 ）',
  `level` int DEFAULT '1' COMMENT '用户等级 1-游客（普通用户）2-会员vip',
  `consume_amount` decimal(8,2) DEFAULT '0.00' COMMENT '累计消费金额',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `delete_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `extra_params2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `extra_params3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `views_num` int DEFAULT '0' COMMENT '浏览量',
  `img_photo_wall_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '照片墙面',
  `wechat_number` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `t_info_user` */

/*Table structure for table `t_info_user_addition` */

DROP TABLE IF EXISTS `t_info_user_addition`;

CREATE TABLE `t_info_user_addition` (
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `tags` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户标签，多个英文逗号隔开',
  `pin_codes_pros_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证正面照片地址',
  `pin_codes_cons_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证反面照片地址',
  `pin_codes_hand_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手持身份证',
  `stature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身高-身高范围：145-250',
  `education_background` int DEFAULT NULL COMMENT '学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士',
  `marital_status` int DEFAULT NULL COMMENT '婚姻状况：0-未婚、1-离异、2-丧偶',
  `children_status` int DEFAULT NULL COMMENT '子女状况：0-无子女、1-有子女',
  `province_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在省份名称',
  `city_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在城市名称',
  `province_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在省份id',
  `city_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在城市id',
  `housing_status` int DEFAULT NULL COMMENT '住房情况：0-已购房、1-租房、2-有需要购房、3-其他',
  `car_status` int DEFAULT NULL COMMENT '购车情况：0-已购车、1-有需要购车、2-其他',
  `introduce` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像、独白、语音独白在上传/修改时需要审核',
  `introduce_voice_url` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '语音独白最长60秒，最短5秒',
  `duration` int DEFAULT NULL COMMENT '语音时长',
  `cover_img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户封面图',
  `weight` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='用户信息---附加表';

/*Data for the table `t_info_user_addition` */

/*Table structure for table `t_info_user_audit` */

DROP TABLE IF EXISTS `t_info_user_audit`;

CREATE TABLE `t_info_user_audit` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '编号',
  `is_audit` int DEFAULT '1' COMMENT '审核状态：0-待审核，1-审核通过 2-审核不通过',
  `pin_codes_pros_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证正面照片地址',
  `pin_codes_cons_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证反面照片地址',
  `pin_codes_hand_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手持身份证',
  `introduce` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像、独白、语音独白在上传/修改时需要审核',
  `headimgurl` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户头像',
  `original_pin_codes_pros_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证正面照片地址',
  `original_pin_codes_cons_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证反面照片地址',
  `original_pin_codes_hand_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手持身份证',
  `original_introduce` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像、独白、语音独白在上传/修改时需要审核',
  `original_headimgurl` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户头像',
  `reason` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '错误原因',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `audit_time` bigint DEFAULT NULL COMMENT '更新时间',
  `audit_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `audit_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `pin_codes` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身份证',
  `true_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='用户模块---用户信息审核表';

/*Data for the table `t_info_user_audit` */

/*Table structure for table `t_info_user_mate_selection` */

DROP TABLE IF EXISTS `t_info_user_mate_selection`;

CREATE TABLE `t_info_user_mate_selection` (
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `age_optional_range` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '年龄可选范围：18-99（最小年龄选不限、最多年龄选30，则显示：30岁及以下）',
  `height_optional_range` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '身高可选范围：145-250（最低选160cm、最高选不限，则显示：160cm及以上）',
  `education_background` int DEFAULT NULL COMMENT '学历：0-高中及以下、1-大专、2-本科、3-双学位、4-硕士、5-博士',
  `marital_status` int DEFAULT NULL COMMENT '婚姻状况：0-未婚、1-离异、2-丧偶',
  `children_status` int DEFAULT NULL COMMENT '子女状况：0-无子女、1-有子女',
  `province_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在省份名称',
  `city_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在城市名称',
  `province_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在省份id',
  `city_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户所在城市id',
  `housing_status` int DEFAULT NULL COMMENT '住房情况：0-已购房、1-租房、2-有需要购房、3-其他',
  `car_status` int DEFAULT NULL COMMENT '购车情况：0-已购车、1-有需要购车、2-其他'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='用户信息---附加表';

/*Data for the table `t_info_user_mate_selection` */

/*Table structure for table `t_info_user_message` */

DROP TABLE IF EXISTS `t_info_user_message`;

CREATE TABLE `t_info_user_message` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '编号',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `message_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息id',
  `is_read` int DEFAULT '0' COMMENT '用户 0:未读 1:已读',
  `read_time` bigint DEFAULT NULL COMMENT '阅读时间',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '额外参数1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统消息-用户消息表';

/*Data for the table `t_info_user_message` */

/*Table structure for table `t_info_visitors_or_browse` */

DROP TABLE IF EXISTS `t_info_visitors_or_browse`;

CREATE TABLE `t_info_visitors_or_browse` (
  `user_id` char(32) DEFAULT NULL COMMENT '用户id',
  `create_operator_id` char(32) DEFAULT NULL COMMENT '创建者ID',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='浏览访客';

/*Data for the table `t_info_visitors_or_browse` */

/*Table structure for table `t_info_whitehead_about` */

DROP TABLE IF EXISTS `t_info_whitehead_about`;

CREATE TABLE `t_info_whitehead_about` (
  `id` char(32) NOT NULL COMMENT '主键id',
  `card_id` char(32) NOT NULL DEFAULT '' COMMENT '分类id  ',
  `user_id` char(32) NOT NULL DEFAULT '' COMMENT '用户ID',
  `name` char(32) NOT NULL DEFAULT '' COMMENT '名称',
  `the_cover` varchar(100) DEFAULT NULL COMMENT '封面',
  `url` char(100) DEFAULT NULL COMMENT 'url',
  `memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `is_state` int DEFAULT NULL COMMENT '状态 0:上线 1:下线',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:已删除',
  `create_operatorid` char(32) DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `delete_operator_id` char(32) DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='白首约表';

/*Data for the table `t_info_whitehead_about` */

insert  into `t_info_whitehead_about`(`id`,`card_id`,`user_id`,`name`,`the_cover`,`url`,`memo`,`is_state`,`is_delete`,`create_operatorid`,`create_operator_name`,`create_time`,`update_operator_id`,`update_operator_name`,`update_time`,`delete_operator_id`,`delete_time`,`extra_params1`) values ('d28806f860da449ebc879fb06af747be','','','白首之约','group1/M00/00/7A/rBH812HFa76APluxAAgIbiZ8wuc112.png','https://www.rhg.wbzlkj.cn/upload/pinfo/13.mov',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640333990698,NULL,NULL,NULL,NULL,NULL,NULL),('d6edf9afbed84d6f8dfdf779c8aa93bf','','','浪漫的事','group1/M00/00/7A/rBH812HFgtaAWNZ7AAP4FRlREZQ603.png','https://www.rhg.wbzlkj.cn/upload/pinfo/14.mp4',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640334134828,NULL,NULL,1641794778999,NULL,NULL,NULL),('de0d163d51b84822bc8d58fb4f0f5d22','','','我们的故事','group1/M00/00/7A/rBH812HFbEaARsgoAAvnKujl-MY175.png','https://www.rhg.wbzlkj.cn/upload/pinfo/12.MP4',NULL,0,0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640333890257,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640333967015,NULL,NULL,NULL);

/*Table structure for table `t_log_account_login` */

DROP TABLE IF EXISTS `t_log_account_login`;

CREATE TABLE `t_log_account_login` (
  `AccountId` varchar(32) NOT NULL COMMENT '登录者id',
  `LoginTime` bigint DEFAULT NULL COMMENT '创建时间',
  `BrowserDetails` varchar(4000) DEFAULT NULL COMMENT '浏览器信息',
  `IpAddr` varchar(20) NOT NULL COMMENT '登陆ip',
  `Message` varchar(255) DEFAULT NULL COMMENT '登录信息',
  `IsStatus` int DEFAULT NULL COMMENT '登录状态 0-成功 1-失败',
  `Password` varchar(100) DEFAULT NULL COMMENT '登录密码'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统管理-管理员登录日志';

/*Data for the table `t_log_account_login` */

/*Table structure for table `t_log_user_browse` */

DROP TABLE IF EXISTS `t_log_user_browse`;

CREATE TABLE `t_log_user_browse` (
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='用户日志记录---用户浏览记录和最近访客';

/*Data for the table `t_log_user_browse` */

/*Table structure for table `t_log_user_collect` */

DROP TABLE IF EXISTS `t_log_user_collect`;

CREATE TABLE `t_log_user_collect` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '收藏表 主键ID',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `collect_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '收藏ID',
  `is_type` int NOT NULL COMMENT '收藏类型 0—活动 1-情缘',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='用户收藏表';

/*Data for the table `t_log_user_collect` */

/*Table structure for table `t_log_user_login` */

DROP TABLE IF EXISTS `t_log_user_login`;

CREATE TABLE `t_log_user_login` (
  `ID` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `UserId` char(32) NOT NULL COMMENT '用户id',
  `IpAddr` varchar(50) NOT NULL COMMENT '登陆ip',
  `IpLocation` varchar(100) DEFAULT NULL COMMENT 'ip所在地',
  `CreateTime` bigint DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小程序用户登录日志表';

/*Data for the table `t_log_user_login` */

/*Table structure for table `t_log_user_member_card` */

DROP TABLE IF EXISTS `t_log_user_member_card`;

CREATE TABLE `t_log_user_member_card` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键ID',
  `card_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '来源任务ID 如：用户会员卡ID   ',
  `card_config_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '来源任务ID 如：用户会员卡ID  ',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '用户ID',
  `is_type` tinyint DEFAULT NULL COMMENT '关联类型 0-红娘牵线',
  `others_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '关联ID',
  `count` int DEFAULT NULL COMMENT '领取数量',
  `memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `mid_card_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `mid_card_config_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='使用会员卡权益日志表';

/*Data for the table `t_log_user_member_card` */

/*Table structure for table `t_log_user_pay` */

DROP TABLE IF EXISTS `t_log_user_pay`;

CREATE TABLE `t_log_user_pay` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '编号',
  `order_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '关联订单id',
  `order_num` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单号',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `pay_type` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '支付方式 1.微信扫码2.微信公众号内支付3.微信h5支付4.其他',
  `order_type` int DEFAULT '0' COMMENT '订单类型 0-其他 1-活动 2-班车 3-商城  4-外卖 5-酒店/民宿 6-会员卡  8店铺',
  `app_sign` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '签名',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `pay_money` int DEFAULT NULL COMMENT '发生的金额 单位 分',
  `is_status` int DEFAULT '0' COMMENT '状态 1.未支付2.支付成功',
  `reason` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '错误原因',
  `pay_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '交易号',
  `end_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '支付完成时间',
  `open_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户对微信公众号的唯一标识',
  `mch_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商户号',
  `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'appid',
  `code_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信支付url',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='支付日志记录表---用户支付记录表';

/*Data for the table `t_log_user_pay` */

/*Table structure for table `t_log_user_refund_pay` */

DROP TABLE IF EXISTS `t_log_user_refund_pay`;

CREATE TABLE `t_log_user_refund_pay` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '编号',
  `order_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '关联订单id',
  `order_num` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单号',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `pay_type` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '退款方式 5.小程序支付6.其他',
  `app_sign` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '签名',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `refund_money` int DEFAULT NULL COMMENT '退款的金额 单位 分',
  `status` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '1' COMMENT '状态 1.未退款2.已退款',
  `reason` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '错误原因',
  `pay_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '交易号',
  `time_end` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '退款完成时间',
  `refund_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '退款原因',
  `mch_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商户号',
  `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'appid',
  `refund_pay_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信退款单号',
  `extra_params1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用。',
  `extra_params2` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用。',
  `extra_params3` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用。',
  `extra_params4` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用。',
  `extra_params5` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段，如果占用，请备注做什么用。',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='支付日志记录表---用户退款支付记录表';

/*Data for the table `t_log_user_refund_pay` */

/*Table structure for table `t_mid_role_and_menu` */

DROP TABLE IF EXISTS `t_mid_role_and_menu`;

CREATE TABLE `t_mid_role_and_menu` (
  `ID` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `RoleID` char(32) DEFAULT NULL COMMENT '角色ID',
  `MenuID` char(32) DEFAULT NULL COMMENT '菜单ID',
  `ExcludeButton` varchar(100) DEFAULT NULL COMMENT '排除的按钮(多个时用逗号分隔)',
  PRIMARY KEY (`ID`),
  KEY `RoleID` (`RoleID`),
  KEY `MenuID` (`MenuID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统管理-角色和菜单中间表';

/*Data for the table `t_mid_role_and_menu` */

insert  into `t_mid_role_and_menu`(`ID`,`RoleID`,`MenuID`,`ExcludeButton`) values ('01f4a669487882ced6f660470becdc94','90b3384c28044e85aab06960e77afbc5','d2ce4141b7ff4ffba3cc0bc435a3733f',NULL),('027e6139dd132fcc85c73e8e5e1e804b','b31b5c862daa4684bedb12e890310128','f39a41b01e3e4d9c81b091fa6bbb9920',NULL),('0285e8a56b08e5a067db098cba4e7098','e647b3b72e4140db9b47d8dd69f208dc','bf82c05e336d4663acf5b0340929ef93',NULL),('03583fd90602789bbae45b4850d05e40','b31b5c862daa4684bedb12e890310128','6de1487abb3342f6ab0e81dd6b940b1c',NULL),('042d7abccd754728bd2315e61aa372b1','db7bad475f114048bb2e87dd487a1308','dacbc5e6536d406d958433ed85770974',NULL),('04bda63296e9a7d9f2a8d5f87ca5bdb8','90b3384c28044e85aab06960e77afbc5','0117ad66f9444c9e83870450c7de2b38',NULL),('052d9e53f5c9b4f2a3ef37c5e0012c63','90b3384c28044e85aab06960e77afbc5','4cf91156d7194791a51b8ab1a4f53383',NULL),('06886352e535406fb7e5ebea9fb047d8','b31b5c862daa4684bedb12e890310128','18dfdfcc9a874c9cb2b4b7ff81a78ebd',NULL),('070b782fa578d56e4bc0f1eb8f8aa64e','e647b3b72e4140db9b47d8dd69f208dc','4eec02eeb3874a84ae2e10b54b7148c3',NULL),('079895013b1145d382e3a7e16f92771c','db7bad475f114048bb2e87dd487a1308','4b0bcf5e655b419386c16048434026e2',NULL),('07fc23db3b49f8eb702cb0ad3578f7ea','b31b5c862daa4684bedb12e890310128','95e6788bc7804d599049ea585030cad3',NULL),('087bcfc15a160b71b941cd508abd7dbf','90b3384c28044e85aab06960e77afbc5','7bdb5ff442b64bcfa12847d2fffc3115',NULL),('0914b1cc7cde406abbe394c233c20517','db7bad475f114048bb2e87dd487a1308','e8ca082a7e0a47c6a2649a320fdf9ee0',NULL),('0950f27426069fceeb0e9f8acc00a93e','90b3384c28044e85aab06960e77afbc5','28f44231b0614e20824d0a524b215130',NULL),('0a0db0c4fa85c03ea04dd14b08efb648','e647b3b72e4140db9b47d8dd69f208dc','56a5884d370f4b28b510ba0717443263',NULL),('0a6b77f25564bf80ca38a277094848e2','e647b3b72e4140db9b47d8dd69f208dc','e466151a5345485f847fc77e9b00cf29',NULL),('0b013ae49d4fde52427d01e3aa779de1','b31b5c862daa4684bedb12e890310128','b973f15a93ae4c1583b2cbdd18bdb09a',NULL),('0c607d6c2b68b37fb678ae6c73510471','b31b5c862daa4684bedb12e890310128','de85eb3426334d088d068e0e08f15521',NULL),('0d0e3ede4c40c817a4451414ef33a77b','e647b3b72e4140db9b47d8dd69f208dc','4ba7e74502a840359a2c066105428731',NULL),('0d1672e34601ef8cdf9bdef1182d7d3c','b31b5c862daa4684bedb12e890310128','99dae66b98d14cc7813a33480282e9f1',NULL),('0d3a79f24ebc7d15963ebf61c4f434b3','90b3384c28044e85aab06960e77afbc5','dbd88ca037344af397bff479bfcd57b1',NULL),('0d3b238191b00a551c1d6d872f64a148','e647b3b72e4140db9b47d8dd69f208dc','7f6ef9b8b11d42088aa3f7d3820111a2',NULL),('0dc27464c93341d89b09b699bdee4853','db7bad475f114048bb2e87dd487a1308','865f617073c6457f851d3902e616345f',NULL),('10285a99c39ad8d48ba2293d9c72fd40','90b3384c28044e85aab06960e77afbc5','e6e40771579b45f4aab138b53febdf78',NULL),('1104305b091ea922135314b9e1ea6465','90b3384c28044e85aab06960e77afbc5','9fbb1fcf032a4acabb6fb93b965c4b7a',NULL),('12fae60b36e3cf0c5aff7a2e789e2eed','e647b3b72e4140db9b47d8dd69f208dc','8d091467450d4838b78ef2913978b235',NULL),('1313e87ca0f1a6fa89e101c261e31f1a','b31b5c862daa4684bedb12e890310128','0bd7300ca6934ae6b98bed985e9d9877',NULL),('133bb1634f024aca9092bf1c9674f990','db7bad475f114048bb2e87dd487a1308','3eb43a130f384b7c9525b91c5c4e597b',NULL),('14f1d86aa0914476a5911b0729ae7626','db7bad475f114048bb2e87dd487a1308','4cf91156d7194791a51b8ab1a4f53383',NULL),('16374dddf9e6fd2b1a4c1ecb65de5ae9','b31b5c862daa4684bedb12e890310128','dad7ae49b0a6494187842c6f1dd14875',NULL),('16fc21519d664de48a0dc9e1bac0ccec','db7bad475f114048bb2e87dd487a1308','0657931e4c50460eb913b73cf2f2fe24',NULL),('17703d28e27dcbcae1c60d39fedde379','e647b3b72e4140db9b47d8dd69f208dc','f07f6807086e43429848ac9a2b1890d4',NULL),('18bbf13506ff1ea40f44a4f07c3bfe99','b31b5c862daa4684bedb12e890310128','1954e32551094452a9b844a514418275',NULL),('18e52174f20e167500f832cd65a3fdad','e647b3b72e4140db9b47d8dd69f208dc','5af51ebd276f4bc695b988e09cf1d4be',NULL),('19062729aa414483a061fc2d0f2b07a6','db7bad475f114048bb2e87dd487a1308','af08e5a43d5c435fa0dfc501ebb8aac8',NULL),('199035ca4d7c40718af6ca51b976e390','b31b5c862daa4684bedb12e890310128','9fbb1fcf032a4acabb6fb93b965c4b7a',NULL),('1ad5e77f778779afd43eeba91c3c33a8','e647b3b72e4140db9b47d8dd69f208dc','9da7daf3976744199bee3b0fad1f45c9',NULL),('1ae7fe5837f94b7ea94a27a1c7baf8ea','db7bad475f114048bb2e87dd487a1308','6faadb095bb8415b8864963e19acfb0d',NULL),('1d38798cfde3d88ca904e04405e11511','90b3384c28044e85aab06960e77afbc5','51b4c86fe59747c6829d9d856a4d142a',NULL),('1e56af4334dc6c4cfe151720de28f79d','b31b5c862daa4684bedb12e890310128','f57de605937046dba426695dd2fd3feb',NULL),('1fa17c53001bf3177ee2fdf727170ed8','e647b3b72e4140db9b47d8dd69f208dc','0657931e4c50460eb913b73cf2f2fe24',NULL),('202bcf99fa2b4ce7824e05161b693fba','db7bad475f114048bb2e87dd487a1308','684b2b6e78e148ba959f60d153e6664b',NULL),('2036110a700d3bb40596ce057a902412','b31b5c862daa4684bedb12e890310128','1bcf1ef5f8984692aaea5784f0df9742',NULL),('20dea3b7d8f9ccb292b50333facb5f77','b31b5c862daa4684bedb12e890310128','cf2f5fbd7320465aa8f85cea2d7e14ed',NULL),('218bcfab6cb1126cbabdea8622dd1592','e647b3b72e4140db9b47d8dd69f208dc','0fc4f8a072b04a88abb0848c69d1f883',NULL),('2302ea58524966475c510936f5858e3d','b31b5c862daa4684bedb12e890310128','68906112da4a42038bad1660ebb8484c',NULL),('23c07c01e9cae35646110a0167468d51','e647b3b72e4140db9b47d8dd69f208dc','d7ca61dabca4468985d137f34c3fe32f',NULL),('25201d9cc8f01e0063d8b0510fcd90e8','e647b3b72e4140db9b47d8dd69f208dc','bf406095ff2841598826912a6064cc28',NULL),('25fe423ae5a0558e9013daca4429e68c','e647b3b72e4140db9b47d8dd69f208dc','8232c9ee8ae74a818e4a6a4be79c9627',NULL),('27c707fcc97ec2d871784e3f236a2f5c','b31b5c862daa4684bedb12e890310128','4b0bcf5e655b419386c16048434026e2',NULL),('285ef40b6f4fd735eade9a370e9a2c76','b31b5c862daa4684bedb12e890310128','342eda268eb14af4b8270098ecff1fc0',NULL),('28f8eb2df0c394cedd9839aa385da3f9','e647b3b72e4140db9b47d8dd69f208dc','e6e40771579b45f4aab138b53febdf78',NULL),('2c2c32132bbf981b1a2f686011963f7a','b31b5c862daa4684bedb12e890310128','532a8dfdc1a84b019a82e6264cf25221',NULL),('2c91d163c3055baae2a004f41937a443','b31b5c862daa4684bedb12e890310128','4b28b089e98d49258754f4998bd48244',NULL),('2ea21fb6ce43cf86720b10031702785e','e647b3b72e4140db9b47d8dd69f208dc','532a8dfdc1a84b019a82e6264cf25221',NULL),('2fc1b60c653a4497b62c9aaffc9901be','db7bad475f114048bb2e87dd487a1308','e466151a5345485f847fc77e9b00cf29',NULL),('2fe35246f1eb02204ad133290871fe16','e647b3b72e4140db9b47d8dd69f208dc','4b0bcf5e655b419386c16048434026e2',NULL),('30b7cb1a76cf8da94f8f472c3c1f9500','e647b3b72e4140db9b47d8dd69f208dc','e8d9507fb59a48ceaa0dda75b8f2b5ca',NULL),('3130319b50284d2fa1dc146abd973a07','db7bad475f114048bb2e87dd487a1308','4ba7e74502a840359a2c066105428731',NULL),('315458790c674c24b8e71b72a63835d4','db7bad475f114048bb2e87dd487a1308','7f6ef9b8b11d42088aa3f7d3820111a2',NULL),('3154dd76f0005ca1ce769c370d6c680e','e647b3b72e4140db9b47d8dd69f208dc','15cf3bb15d3d41c099ddba422eb91215',NULL),('3164d2446c138f7ccfc1698c71998475','e647b3b72e4140db9b47d8dd69f208dc','865f617073c6457f851d3902e616345f',NULL),('31c0017542999c5a97ffce6afff2e546','b31b5c862daa4684bedb12e890310128','d2ce4141b7ff4ffba3cc0bc435a3733f',NULL),('326376f331b644e69991ed5934891127','db7bad475f114048bb2e87dd487a1308','6dabd809142f47bb87b2a44d4b39b39f',NULL),('33a275a2eb1f464e86722ab6c9b51f2c','db7bad475f114048bb2e87dd487a1308','bf406095ff2841598826912a6064cc28',NULL),('341c43a69c8f4b279be9ffe49482de00','db7bad475f114048bb2e87dd487a1308','9fbb1fcf032a4acabb6fb93b965c4b7a',NULL),('34833dc7c9e36dcafe3541d36af17058','b31b5c862daa4684bedb12e890310128','f1bd9dd5aaa1442db3fa66ad472896b3',NULL),('35cd3ea08066a7b77a2f2594e6821eb9','b31b5c862daa4684bedb12e890310128','7bdb5ff442b64bcfa12847d2fffc3115',NULL),('36e634e8685e4951b93cbebc4391d0e9','db7bad475f114048bb2e87dd487a1308','bf82c05e336d4663acf5b0340929ef93',NULL),('36eaa94b501f461d9f55beff09132c75','db7bad475f114048bb2e87dd487a1308','f07f6807086e43429848ac9a2b1890d4',NULL),('379e29285d0937dc14e595c7e1ea8279','e647b3b72e4140db9b47d8dd69f208dc','61241c6baa96488b844a1ff4d184e574',NULL),('39784e3243797bc3369444c38d7ef887','b31b5c862daa4684bedb12e890310128','4eec02eeb3874a84ae2e10b54b7148c3',NULL),('3aa3e7e197e5aa18e984c87f773a5acf','b31b5c862daa4684bedb12e890310128','0fc4f8a072b04a88abb0848c69d1f883',NULL),('3abe342607c444debd8c79fba5e5ecb0','db7bad475f114048bb2e87dd487a1308','f39a41b01e3e4d9c81b091fa6bbb9920',NULL),('3b2b4dc14a6c1e10a8c27625cf0a61f3','e647b3b72e4140db9b47d8dd69f208dc','e039905820ab4fb1999088a41c777f76',NULL),('3c19ad3aae4e7f206df309a7de385a3e','e647b3b72e4140db9b47d8dd69f208dc','d2ce4141b7ff4ffba3cc0bc435a3733f',NULL),('3ec4342f199c8256cf0186467cf08f5b','90b3384c28044e85aab06960e77afbc5','dad7ae49b0a6494187842c6f1dd14875',NULL),('3f66d29472de4f00ab7f7f161e6f58c5','db7bad475f114048bb2e87dd487a1308','dad7ae49b0a6494187842c6f1dd14875',NULL),('3fc9e6888f9138fd18f3661257986fcb','e647b3b72e4140db9b47d8dd69f208dc','cca70c2320c24337a802e5a71900b4b9',NULL),('402ded08013b421d97641ba04faaf8de','db7bad475f114048bb2e87dd487a1308','4b28b089e98d49258754f4998bd48244',NULL),('41a03877cee7f678294372532483573c','b31b5c862daa4684bedb12e890310128','28f44231b0614e20824d0a524b215130',NULL),('41b29a10b0cb446ea297e590193f6e83','db7bad475f114048bb2e87dd487a1308','342eda268eb14af4b8270098ecff1fc0',NULL),('43212a8c8237b992ee92171c1befe21f','90b3384c28044e85aab06960e77afbc5','56a5884d370f4b28b510ba0717443263',NULL),('438f12fc10c70de617b0793b29be3444','90b3384c28044e85aab06960e77afbc5','6dabd809142f47bb87b2a44d4b39b39f',NULL),('43b96dc7bd0c4c1a9f7a1c8048d67ad8','db7bad475f114048bb2e87dd487a1308','6ecdb6afcc7947d6af9dcb02b8f7549d',NULL),('43e7f200fc5bb494abd757f792726141','90b3384c28044e85aab06960e77afbc5','1bcf1ef5f8984692aaea5784f0df9742',NULL),('4486c70345916ba749ee310c3b574b79','e647b3b72e4140db9b47d8dd69f208dc','684b2b6e78e148ba959f60d153e6664b',NULL),('44c19eac0e572b20caa758b72cb2f3f9','90b3384c28044e85aab06960e77afbc5','b731520663d84373949993408471f42e',NULL),('451e7009d76458cc99d8a8af48e9535a','b31b5c862daa4684bedb12e890310128','e466151a5345485f847fc77e9b00cf29',NULL),('46c736608f454af884e10f60a635968e','db7bad475f114048bb2e87dd487a1308','d2ce4141b7ff4ffba3cc0bc435a3733f',NULL),('46de27bb6211466eb0eec2defd13739b','db7bad475f114048bb2e87dd487a1308','5af51ebd276f4bc695b988e09cf1d4be',NULL),('46e29a82dbea4fcb9272200e9ec45286','db7bad475f114048bb2e87dd487a1308','1aabaeeba3a04ffea1264a91a87e8287',NULL),('4755b2a9b877627226ee6407944c3257','e647b3b72e4140db9b47d8dd69f208dc','3eb43a130f384b7c9525b91c5c4e597b',NULL),('4a4b75cd8a3d83221dc0de642e47e014','e647b3b72e4140db9b47d8dd69f208dc','95e6788bc7804d599049ea585030cad3',NULL),('4ae6f4e5548ebfadab35109707667f51','b31b5c862daa4684bedb12e890310128','15cf3bb15d3d41c099ddba422eb91215',NULL),('4d08e0159ed58461dc09a9dd3b89fadb','b31b5c862daa4684bedb12e890310128','0117ad66f9444c9e83870450c7de2b38',NULL),('4f66465027d244e7946a7bcdbcdb8852','db7bad475f114048bb2e87dd487a1308','7366d770533847fd8a3d8b629c343ca4',NULL),('507bb5addf92090b1393e30dfe63045d','e647b3b72e4140db9b47d8dd69f208dc','bad841be9c794a1d80d64e56076cd906',NULL),('50b8a3548186966d04f6512cf47c3a36','b31b5c862daa4684bedb12e890310128','acbf59c3dd6b421486adde17e153ed63',NULL),('520d7e60bc9d84209f6ca93af03e9d20','90b3384c28044e85aab06960e77afbc5','a60d8becabe94226b01aabe8a0823ab1',NULL),('54086b68dcdc4621a08b63209215511a','db7bad475f114048bb2e87dd487a1308','b973f15a93ae4c1583b2cbdd18bdb09a',NULL),('5483f36df6a07080a9777a9e130ac2d5','b31b5c862daa4684bedb12e890310128','81ed5443ac07488b8fd66a4c26d52550',NULL),('5489f4f13f5a401799b3f7d073866384','db7bad475f114048bb2e87dd487a1308','4eec02eeb3874a84ae2e10b54b7148c3',NULL),('5515a2d6f32451cfad5d5e1f10fc19ac','e647b3b72e4140db9b47d8dd69f208dc','e8ca082a7e0a47c6a2649a320fdf9ee0',NULL),('5576b225b4490f259e553f73b5a6f06c','b31b5c862daa4684bedb12e890310128','42b836f7164d4fa9a5fbecc4d6a88c80',NULL),('571851946378c5257611f45d4968c5e6','90b3384c28044e85aab06960e77afbc5','b554be9d62244cbbb97201a787aca996',NULL),('57c7058e22c92a5b1bc4a799878e156e','b31b5c862daa4684bedb12e890310128','6faadb095bb8415b8864963e19acfb0d',NULL),('57fddb8aa7be4479fc3dd80b41cf2844','90b3384c28044e85aab06960e77afbc5','72bce1c91e9045d69838e988bfa5bc7d',NULL),('5911bdfe58f6c28338a31d5d1db0672e','b31b5c862daa4684bedb12e890310128','e8ca082a7e0a47c6a2649a320fdf9ee0',NULL),('59ae0544e0c0c3a5676c7541c84e0c87','e647b3b72e4140db9b47d8dd69f208dc','de85eb3426334d088d068e0e08f15521',NULL),('59faf261c51b776ed98e78a0731980eb','90b3384c28044e85aab06960e77afbc5','bad841be9c794a1d80d64e56076cd906',NULL),('5bdca302caa14f789bd34c17209f5c1b','db7bad475f114048bb2e87dd487a1308','9da7daf3976744199bee3b0fad1f45c9',NULL),('5cfc3162af04b3004c396f12e09edda6','b31b5c862daa4684bedb12e890310128','0fd5aa2753834ebb88b5de52474d5ca0',NULL),('5d93355a116d4f6f69ec718b83ea383f','90b3384c28044e85aab06960e77afbc5','033b4d93d3364fa7ad7a5f2023673c6b',NULL),('5dac95091ec962eb338cccfa211e35ed','b31b5c862daa4684bedb12e890310128','7f6ef9b8b11d42088aa3f7d3820111a2',NULL),('5daff6a1e9f79dd23ec7da7fec2676b7','b31b5c862daa4684bedb12e890310128','ed7408dd87ac4054956e7dbc54a9b47d',NULL),('5dfed220cd5e6c1beb2354455b2da009','e647b3b72e4140db9b47d8dd69f208dc','2f4c6807a165410385b5ce1438e4af41',NULL),('5efd32f733d94916a8bc61cac41b53cb','db7bad475f114048bb2e87dd487a1308','cee60507074345c6a806affaee2711e9',NULL),('5f9b653ec8d2298e4e8f3440d46c9a35','e647b3b72e4140db9b47d8dd69f208dc','689a1f346d3444669aa5806a0d2f3aea',NULL),('5ffa28e1e82b489eb169e36eae180b66','db7bad475f114048bb2e87dd487a1308','1bcf1ef5f8984692aaea5784f0df9742',NULL),('60fa60967c7f87aa91c6d9b71fb4e593','e647b3b72e4140db9b47d8dd69f208dc','514e68f98eae410580251ba885810309',NULL),('629fb0c01ce4e3eb24c5d98b7bb10e20','90b3384c28044e85aab06960e77afbc5','4b0bcf5e655b419386c16048434026e2',NULL),('637a289092b11ba08222e6a60ccf0c54','e647b3b72e4140db9b47d8dd69f208dc','eafe28b01b6f4d67b95f64dd89e5d363',NULL),('63b10bc2feff413d813369638561dac1','db7bad475f114048bb2e87dd487a1308','d7ca61dabca4468985d137f34c3fe32f',NULL),('6512d5ba4181d3d2d810864f0b623bed','b31b5c862daa4684bedb12e890310128','af08e5a43d5c435fa0dfc501ebb8aac8',NULL),('656b672d7a429e7052be115f25f3e0fb','e647b3b72e4140db9b47d8dd69f208dc','4cf91156d7194791a51b8ab1a4f53383',NULL),('65d2bb2286e443f1aba2141b9ff34af0','db7bad475f114048bb2e87dd487a1308','0fc4f8a072b04a88abb0848c69d1f883',NULL),('65e0e210aa230126a68185dbd574952e','b31b5c862daa4684bedb12e890310128','5af51ebd276f4bc695b988e09cf1d4be',NULL),('6615c384979d413db86e71267671fcc4','db7bad475f114048bb2e87dd487a1308','72bce1c91e9045d69838e988bfa5bc7d',NULL),('66508878f29264aafb7edf2b08594cb9','90b3384c28044e85aab06960e77afbc5','1aabaeeba3a04ffea1264a91a87e8287',NULL),('675618330baa90d29e526fd1f57583ad','b31b5c862daa4684bedb12e890310128','eafe28b01b6f4d67b95f64dd89e5d363',NULL),('67b055f2fbff40e685037fa1f27f5ae1','db7bad475f114048bb2e87dd487a1308','eafe28b01b6f4d67b95f64dd89e5d363',NULL),('683c87532901b0ecd733d57606ec99cb','b31b5c862daa4684bedb12e890310128','547a043ef4e04aae9613f5d89b39522b',NULL),('688e78a28a1bc5bfe805b1a6e961cf7f','e647b3b72e4140db9b47d8dd69f208dc','79f1f4a01c3b429c993fe2669f18134b',NULL),('694e818c92ff48be83a9f68226ecefb9','db7bad475f114048bb2e87dd487a1308','1954e32551094452a9b844a514418275',NULL),('697bdfb34560f0c55686d1ab122d5ed9','e647b3b72e4140db9b47d8dd69f208dc','0117ad66f9444c9e83870450c7de2b38',NULL),('699f3a52dcd607d491a679a85b242819','e647b3b72e4140db9b47d8dd69f208dc','81ed5443ac07488b8fd66a4c26d52550',NULL),('6b3350099cde41eda71a678c84ad8e2c','db7bad475f114048bb2e87dd487a1308','dc698fde21ae454c84999946c71cb150',NULL),('6bc80c006d2e13e3e4070aea72e14200','b31b5c862daa4684bedb12e890310128','331b9d69edbc4d94b10fe231e02c761f',NULL),('6cc37436a9f099254ce475d74d09e514','e647b3b72e4140db9b47d8dd69f208dc','dacbc5e6536d406d958433ed85770974',NULL),('6db7ccd51f2e9a90be725a19452d5bf5','b31b5c862daa4684bedb12e890310128','bad841be9c794a1d80d64e56076cd906',NULL),('6dfb473c91e6a9164e558833b5bfa27a','b31b5c862daa4684bedb12e890310128','3033d7be2f094b99aa71ac7142cdf918',NULL),('6e32a1725a28145a10152b7fd156bbe8','90b3384c28044e85aab06960e77afbc5','730c8076493746c38cc90842f18936dc',NULL),('6ed0e4a567651d924a9c7cb61908033a','90b3384c28044e85aab06960e77afbc5','cf2f5fbd7320465aa8f85cea2d7e14ed',NULL),('6ed672f15bceb2e73bf9f4164da29f4b','b31b5c862daa4684bedb12e890310128','dacbc5e6536d406d958433ed85770974',NULL),('6f700ef0c090300624186c31d352f16e','e647b3b72e4140db9b47d8dd69f208dc','6de1487abb3342f6ab0e81dd6b940b1c',NULL),('711c9ea470206389941846105678431f','e647b3b72e4140db9b47d8dd69f208dc','6dabd809142f47bb87b2a44d4b39b39f',NULL),('727f738a2ca1b9acfecf2470d2f6b56e','b31b5c862daa4684bedb12e890310128','cca70c2320c24337a802e5a71900b4b9',NULL),('72a6eec33cc883ffe95a7e87ae4260ba','e647b3b72e4140db9b47d8dd69f208dc','1954e32551094452a9b844a514418275',NULL),('72f1461d921a3093d9f179c9e9ee0e7e','90b3384c28044e85aab06960e77afbc5','6de1487abb3342f6ab0e81dd6b940b1c',NULL),('730923138967484c820ab20467840818','db7bad475f114048bb2e87dd487a1308','547a043ef4e04aae9613f5d89b39522b',NULL),('741652ec2e4ce5624655ac43b86cab8c','e647b3b72e4140db9b47d8dd69f208dc','6faadb095bb8415b8864963e19acfb0d',NULL),('746fd02cfe09cd3935ec576359560166','e647b3b72e4140db9b47d8dd69f208dc','e16dd93c3b6f4d0bad4959fe9ebcad94',NULL),('756e8503469f372aefa998f4390d030f','90b3384c28044e85aab06960e77afbc5','81ed5443ac07488b8fd66a4c26d52550',NULL),('7678cec97174d52277600ab83c126ac9','e647b3b72e4140db9b47d8dd69f208dc','51b4c86fe59747c6829d9d856a4d142a',NULL),('76e95eb5300b69fe7ac29f5c01daf361','e647b3b72e4140db9b47d8dd69f208dc','547a043ef4e04aae9613f5d89b39522b',NULL),('797142e8b3845240c7b3928cfb62e531','90b3384c28044e85aab06960e77afbc5','2f4c6807a165410385b5ce1438e4af41',NULL),('797fed143d6b4304b042b46610bf097f','db7bad475f114048bb2e87dd487a1308','79f1f4a01c3b429c993fe2669f18134b',NULL),('7994ea1c32534dc29e75480c2f5c6ec2','db7bad475f114048bb2e87dd487a1308','2281779d4f2a417fa4a986ee938b22b1',NULL),('799b0575f61997c324895a60d09f7a1e','e647b3b72e4140db9b47d8dd69f208dc','7366d770533847fd8a3d8b629c343ca4',NULL),('7bfa6b44e7e52208c1b3d18031dab0f4','e647b3b72e4140db9b47d8dd69f208dc','f57de605937046dba426695dd2fd3feb',NULL),('7e4983b919f44897a02edddedf653a8b','db7bad475f114048bb2e87dd487a1308','e8d9507fb59a48ceaa0dda75b8f2b5ca',NULL),('7e531449b90d4c448f063f3e611d21ae','db7bad475f114048bb2e87dd487a1308','a60d8becabe94226b01aabe8a0823ab1',NULL),('7e9aa3dbb6e679e895c7082a2da07135','b31b5c862daa4684bedb12e890310128','e16dd93c3b6f4d0bad4959fe9ebcad94',NULL),('7f851007865a4fd4bbc9cd78c4de2a53','db7bad475f114048bb2e87dd487a1308','68906112da4a42038bad1660ebb8484c',NULL),('7fb53f9a4498f28ebd3ddebb85f44bfc','90b3384c28044e85aab06960e77afbc5','4ba7e74502a840359a2c066105428731',NULL),('80116d2da7974e6089264142d0f987bc','db7bad475f114048bb2e87dd487a1308','15cf3bb15d3d41c099ddba422eb91215',NULL),('80daab095e39d8911eacd34736314329','e647b3b72e4140db9b47d8dd69f208dc','10d5a3b9165e4951b4d4cf937ea03325',NULL),('811ffd18ff8fb10b9b5012999aea304b','90b3384c28044e85aab06960e77afbc5','4875d4539cdc4340abf6bb6183bb8f90',NULL),('8149ab9742a8480fdb0420a395ca36ee','90b3384c28044e85aab06960e77afbc5','7366d770533847fd8a3d8b629c343ca4',NULL),('81a1c3ce271c6dfc49813b0d2897ae68','e647b3b72e4140db9b47d8dd69f208dc','5b96dbb0d80244bbb7a0036efa7cac9c',NULL),('81a79b52211268056ae1910cee437cf7','90b3384c28044e85aab06960e77afbc5','e466151a5345485f847fc77e9b00cf29',NULL),('81edfe5b0195315c4201f8b2d1dd5616','b31b5c862daa4684bedb12e890310128','684b2b6e78e148ba959f60d153e6664b',NULL),('8223587cf011465f82379ca549d7f3ae','db7bad475f114048bb2e87dd487a1308','10d5a3b9165e4951b4d4cf937ea03325',NULL),('8253442dadec715d31b09709812121b7','90b3384c28044e85aab06960e77afbc5','b973f15a93ae4c1583b2cbdd18bdb09a',NULL),('8296e601f61b53248628d01eb086535f','e647b3b72e4140db9b47d8dd69f208dc','af08e5a43d5c435fa0dfc501ebb8aac8',NULL),('8414391fa51f3448186912e8ae95a0c5','e647b3b72e4140db9b47d8dd69f208dc','730c8076493746c38cc90842f18936dc',NULL),('8456fb47983ed8f7404e791be5cac80b','e647b3b72e4140db9b47d8dd69f208dc','2281779d4f2a417fa4a986ee938b22b1',NULL),('851f55e5cf714487a8b4bd428409f663','db7bad475f114048bb2e87dd487a1308','abf248dad56d40839c73f8644fd4a61f',NULL),('855dae29a3dbd6017f377c695613a171','b31b5c862daa4684bedb12e890310128','865f617073c6457f851d3902e616345f',NULL),('86bcff05e5352330feb8a4febc307da2','90b3384c28044e85aab06960e77afbc5','15cf3bb15d3d41c099ddba422eb91215',NULL),('89867dd9deb5c1211b84a37ea654fc82','90b3384c28044e85aab06960e77afbc5','f39a41b01e3e4d9c81b091fa6bbb9920',NULL),('89ce64948d59ed894751e05f8ef941aa','b31b5c862daa4684bedb12e890310128','b731520663d84373949993408471f42e',NULL),('89e67f7f0511754b6b4229b4404874f5','b31b5c862daa4684bedb12e890310128','7366d770533847fd8a3d8b629c343ca4',NULL),('8a58bc41336a4aefb89a464d1c9dc119','db7bad475f114048bb2e87dd487a1308','b554be9d62244cbbb97201a787aca996',NULL),('8e0e619d7b494550b70ad1aa3e6a0974','db7bad475f114048bb2e87dd487a1308','689a1f346d3444669aa5806a0d2f3aea',NULL),('8f86442e5a06db7a7c1b17b657cfd6c2','90b3384c28044e85aab06960e77afbc5','dacbc5e6536d406d958433ed85770974',NULL),('8fdb746aae3263640d758b01c3c2a740','90b3384c28044e85aab06960e77afbc5','684b2b6e78e148ba959f60d153e6664b',NULL),('8feb2da1817f199dd6a9f476daa9abd9','90b3384c28044e85aab06960e77afbc5','e8d9507fb59a48ceaa0dda75b8f2b5ca',NULL),('9037551ab75e4ef58a1fba983c6c3099','db7bad475f114048bb2e87dd487a1308','e039905820ab4fb1999088a41c777f76',NULL),('907c3e844ea644579d07b6f4c125e646','db7bad475f114048bb2e87dd487a1308','28f44231b0614e20824d0a524b215130',NULL),('92df364eb26922f050709c835eb7994b','e647b3b72e4140db9b47d8dd69f208dc','0fd5aa2753834ebb88b5de52474d5ca0',NULL),('930dc629dbe247cd96f08e5d8723e2a7','90b3384c28044e85aab06960e77afbc5','6faadb095bb8415b8864963e19acfb0d',NULL),('932557e670b7d4ffefd8268deef0406e','b31b5c862daa4684bedb12e890310128','1aabaeeba3a04ffea1264a91a87e8287',NULL),('93f6bac5f1cd896ae56309a73f89b9aa','b31b5c862daa4684bedb12e890310128','6ecdb6afcc7947d6af9dcb02b8f7549d',NULL),('9412619b9232413e8a90e12918510fdf','db7bad475f114048bb2e87dd487a1308','2f4c6807a165410385b5ce1438e4af41',NULL),('95d232af8f1b610bd0f1fa85b2660313','90b3384c28044e85aab06960e77afbc5','514e68f98eae410580251ba885810309',NULL),('95e66617256f4aea81663ac4df32f075','db7bad475f114048bb2e87dd487a1308','924218c0d78f4ea8b98ca2d5a527b16c',NULL),('972c4f075bb246dbadce98acfc74214d','db7bad475f114048bb2e87dd487a1308','56a5884d370f4b28b510ba0717443263',NULL),('97b082b8d5fec705f46622ed5c36add4','b31b5c862daa4684bedb12e890310128','9da7daf3976744199bee3b0fad1f45c9',NULL),('9961dfd08300c4ae9f9327b775c3054c','e647b3b72e4140db9b47d8dd69f208dc','b731520663d84373949993408471f42e',NULL),('997649e2e48e157619237a8cd7fbd6aa','b31b5c862daa4684bedb12e890310128','e039905820ab4fb1999088a41c777f76',NULL),('9a78bf84ca956e72fa1866f5b39a4cf9','90b3384c28044e85aab06960e77afbc5','5af51ebd276f4bc695b988e09cf1d4be',NULL),('9a8a8551fa8b67644b359dec108daf7f','e647b3b72e4140db9b47d8dd69f208dc','331b9d69edbc4d94b10fe231e02c761f',NULL),('9bfcbf83190ba182804218c828918067','b31b5c862daa4684bedb12e890310128','56a5884d370f4b28b510ba0717443263',NULL),('9c646f756ffc41949939c0b1e5f6818e','db7bad475f114048bb2e87dd487a1308','7bdb5ff442b64bcfa12847d2fffc3115',NULL),('9e9bb8ce0dd80278dee0dc9402c655b8','b31b5c862daa4684bedb12e890310128','f07f6807086e43429848ac9a2b1890d4',NULL),('9ea73cffdef140fb9cfeffd9462433e7','db7bad475f114048bb2e87dd487a1308','51b4c86fe59747c6829d9d856a4d142a',NULL),('9ee7af4413f94b5791efefd42a8c9466','db7bad475f114048bb2e87dd487a1308','b731520663d84373949993408471f42e',NULL),('a0059c174c324b05aec8dedf43cf5f3b','db7bad475f114048bb2e87dd487a1308','bad841be9c794a1d80d64e56076cd906',NULL),('a04de51d18619e283f0b0baa18a8208c','90b3384c28044e85aab06960e77afbc5','42b836f7164d4fa9a5fbecc4d6a88c80',NULL),('a083f4e67681219afe44e41d486e3e7e','90b3384c28044e85aab06960e77afbc5','99dae66b98d14cc7813a33480282e9f1',NULL),('a187e9e3d888b8394e9018745d2ad7ff','90b3384c28044e85aab06960e77afbc5','af08e5a43d5c435fa0dfc501ebb8aac8',NULL),('a28903cf2347e961aad54d87bda5b583','b31b5c862daa4684bedb12e890310128','10d5a3b9165e4951b4d4cf937ea03325',NULL),('a2935b4e7a1752cd22ae16053e95607b','b31b5c862daa4684bedb12e890310128','72bce1c91e9045d69838e988bfa5bc7d',NULL),('a4794b7b7d70274e46153cbe8fb7eabf','e647b3b72e4140db9b47d8dd69f208dc','acbf59c3dd6b421486adde17e153ed63',NULL),('a534ecf4c2235c65788b41fecef2c82f','e647b3b72e4140db9b47d8dd69f208dc','9fbb1fcf032a4acabb6fb93b965c4b7a',NULL),('a5e0d801a6664bc48261629814232b77','db7bad475f114048bb2e87dd487a1308','cca70c2320c24337a802e5a71900b4b9',NULL),('a663ad0ad6c88076a744cd8950387905','e647b3b72e4140db9b47d8dd69f208dc','f39a41b01e3e4d9c81b091fa6bbb9920',NULL),('a6e3bb4f900091feb289d29aa424471b','e647b3b72e4140db9b47d8dd69f208dc','18dfdfcc9a874c9cb2b4b7ff81a78ebd',NULL),('a6f6e752120844cf9db82d64a2b7e722','db7bad475f114048bb2e87dd487a1308','e16dd93c3b6f4d0bad4959fe9ebcad94',NULL),('a7bb65c030614d31aab7c3823fa6c950','db7bad475f114048bb2e87dd487a1308','95e6788bc7804d599049ea585030cad3',NULL),('a826761d0e2d5b1072ba8392830603a6','b31b5c862daa4684bedb12e890310128','bf82c05e336d4663acf5b0340929ef93',NULL),('a97e55ae67072a45c8f5e2d82dcd37eb','e647b3b72e4140db9b47d8dd69f208dc','72bce1c91e9045d69838e988bfa5bc7d',NULL),('aaf34e4fccbb5c35df880878ca9dad9b','b31b5c862daa4684bedb12e890310128','8232c9ee8ae74a818e4a6a4be79c9627',NULL),('ad0473098ff5398672dc7f542855ba89','e647b3b72e4140db9b47d8dd69f208dc','0bd7300ca6934ae6b98bed985e9d9877',NULL),('ad24f76983e3bbbbe98e245d4d3e384c','b31b5c862daa4684bedb12e890310128','61241c6baa96488b844a1ff4d184e574',NULL),('ae0fdce652cd018072d087dbf2e95faf','b31b5c862daa4684bedb12e890310128','e6e40771579b45f4aab138b53febdf78',NULL),('aea5c477bf636c16648ca699ed531b7d','e647b3b72e4140db9b47d8dd69f208dc','1bcf1ef5f8984692aaea5784f0df9742',NULL),('b04ca5244fce396f1f4c825ebc766835','90b3384c28044e85aab06960e77afbc5','924218c0d78f4ea8b98ca2d5a527b16c',NULL),('b061bfad293439bf1a633ef2c017c62d','e647b3b72e4140db9b47d8dd69f208dc','f1bd9dd5aaa1442db3fa66ad472896b3',NULL),('b107c14d5a29425e3a3f4f3e58298c0d','e647b3b72e4140db9b47d8dd69f208dc','4875d4539cdc4340abf6bb6183bb8f90',NULL),('b26f674493db43bf8373eead8b82a905','db7bad475f114048bb2e87dd487a1308','033b4d93d3364fa7ad7a5f2023673c6b',NULL),('b2c6838a461b4260bd1b76a2c314359b','db7bad475f114048bb2e87dd487a1308','0117ad66f9444c9e83870450c7de2b38',NULL),('b307d1b5a834493f8eea7582f470437d','db7bad475f114048bb2e87dd487a1308','0fd5aa2753834ebb88b5de52474d5ca0',NULL),('b30e1657a3a24bd1861c982c5d48d60c','db7bad475f114048bb2e87dd487a1308','42b836f7164d4fa9a5fbecc4d6a88c80',NULL),('b35fb901b3154ccbae7b14127ec4aaa7','db7bad475f114048bb2e87dd487a1308','730c8076493746c38cc90842f18936dc',NULL),('b3841668a263dff13b27c216220a57dd','90b3384c28044e85aab06960e77afbc5','e039905820ab4fb1999088a41c777f76',NULL),('b3b3baeba564fd9ce77b20923a4973d2','e647b3b72e4140db9b47d8dd69f208dc','cf2f5fbd7320465aa8f85cea2d7e14ed',NULL),('b5cadfa442533e86eb243b20010331c4','b31b5c862daa4684bedb12e890310128','730c8076493746c38cc90842f18936dc',NULL),('b5f04aa689699dd2e5548f2dec3223e7','90b3384c28044e85aab06960e77afbc5','342eda268eb14af4b8270098ecff1fc0',NULL),('b809d77df86b500370b6bfd7ea1b42fa','90b3384c28044e85aab06960e77afbc5','f57de605937046dba426695dd2fd3feb',NULL),('ba29307072865485a15c99498e5ddd7f','e647b3b72e4140db9b47d8dd69f208dc','99dae66b98d14cc7813a33480282e9f1',NULL),('ba2e22b25e34c7fa33b3584a76fe1467','b31b5c862daa4684bedb12e890310128','689a1f346d3444669aa5806a0d2f3aea',NULL),('ba5f2f066bbc68064973b6ee2a83775f','90b3384c28044e85aab06960e77afbc5','532a8dfdc1a84b019a82e6264cf25221',NULL),('bbbee0384591b3d08a995a51b1aaa49a','b31b5c862daa4684bedb12e890310128','dc698fde21ae454c84999946c71cb150',NULL),('bd2fb773dba7f37b9ee3ecfa33a26299','90b3384c28044e85aab06960e77afbc5','1954e32551094452a9b844a514418275',NULL),('bd7cc0c7520acedd00ffb2c7ba0ba338','b31b5c862daa4684bedb12e890310128','2f4c6807a165410385b5ce1438e4af41',NULL),('bdf6b64c3c553a3374574db9a6d817d8','e647b3b72e4140db9b47d8dd69f208dc','3033d7be2f094b99aa71ac7142cdf918',NULL),('c018927487962514f0792b2c9e3d771e','b31b5c862daa4684bedb12e890310128','a60d8becabe94226b01aabe8a0823ab1',NULL),('c01fa56a95b1ae99999dee63d744a638','e647b3b72e4140db9b47d8dd69f208dc','cb28f213e495438dab2e750843aba6be',NULL),('c0a2e6687bad48cebd0a60285801762e','db7bad475f114048bb2e87dd487a1308','8232c9ee8ae74a818e4a6a4be79c9627',NULL),('c1d616bc24594a51aeb54d18bcbd0682','db7bad475f114048bb2e87dd487a1308','8d091467450d4838b78ef2913978b235',NULL),('c2cc9675b5ca4a6f81fd5aac54e58cbb','db7bad475f114048bb2e87dd487a1308','6de1487abb3342f6ab0e81dd6b940b1c',NULL),('c301eb2d3ce2425a6d30d7172e29f7ed','90b3384c28044e85aab06960e77afbc5','547a043ef4e04aae9613f5d89b39522b',NULL),('c43e5c0580e38cb420952f53d1f1ce5b','b31b5c862daa4684bedb12e890310128','4875d4539cdc4340abf6bb6183bb8f90',NULL),('c6c4ec86945144df32c0632e1242fca3','e647b3b72e4140db9b47d8dd69f208dc','28f44231b0614e20824d0a524b215130',NULL),('c6e1c3113de549ad903855f1edd15d03','db7bad475f114048bb2e87dd487a1308','81ed5443ac07488b8fd66a4c26d52550',NULL),('c7006e06e6714be4b4c6217f662b2f40','db7bad475f114048bb2e87dd487a1308','f1bd9dd5aaa1442db3fa66ad472896b3',NULL),('c70e0dfa5fbc410dbb41c7e86dcf1a90','db7bad475f114048bb2e87dd487a1308','532a8dfdc1a84b019a82e6264cf25221',NULL),('ca0cbb6c3cf5c3c108251403a9134d36','b31b5c862daa4684bedb12e890310128','4cf91156d7194791a51b8ab1a4f53383',NULL),('ca0e8da8494340ee5396cdd65a4e23e5','b31b5c862daa4684bedb12e890310128','514e68f98eae410580251ba885810309',NULL),('cbaa3b431304489365af9bc4fe0e896b','b31b5c862daa4684bedb12e890310128','abf248dad56d40839c73f8644fd4a61f',NULL),('cd3f25fbedcd2711babba4127820b097','e647b3b72e4140db9b47d8dd69f208dc','a60d8becabe94226b01aabe8a0823ab1',NULL),('ce5e2ae92a9900faa2709c7e090e669f','90b3384c28044e85aab06960e77afbc5','dc698fde21ae454c84999946c71cb150',NULL),('ce887aeaa361458d9aad68dfcb5c0362','db7bad475f114048bb2e87dd487a1308','acbf59c3dd6b421486adde17e153ed63',NULL),('d137da8bfda555fce11c7e617f192a38','e647b3b72e4140db9b47d8dd69f208dc','033b4d93d3364fa7ad7a5f2023673c6b',NULL),('d162f6207c0148bbbebb9f49d0fb5a81','db7bad475f114048bb2e87dd487a1308','99dae66b98d14cc7813a33480282e9f1',NULL),('d20768a54ca948ac84720b6ac9162acd','db7bad475f114048bb2e87dd487a1308','dbd88ca037344af397bff479bfcd57b1',NULL),('d2c9764e19c97c609eb44dea730f363e','b31b5c862daa4684bedb12e890310128','6dabd809142f47bb87b2a44d4b39b39f',NULL),('d372c0a0b4cf4ab9b7fb78d7a51a6798','db7bad475f114048bb2e87dd487a1308','331b9d69edbc4d94b10fe231e02c761f',NULL),('d429accf12dbfdc917c1f8b8a1a19cbd','b31b5c862daa4684bedb12e890310128','4ba7e74502a840359a2c066105428731',NULL),('d46b98631d6c896cf8e72aa85977ae0e','e647b3b72e4140db9b47d8dd69f208dc','dad7ae49b0a6494187842c6f1dd14875',NULL),('d5f5d82264ac5c02d979b2471332d62e','b31b5c862daa4684bedb12e890310128','924218c0d78f4ea8b98ca2d5a527b16c',NULL),('d68c720f3cb440a282c1d012b4e76a9d','db7bad475f114048bb2e87dd487a1308','5b96dbb0d80244bbb7a0036efa7cac9c',NULL),('d84ef805434b464dae96b9d3490be8ec','db7bad475f114048bb2e87dd487a1308','f57de605937046dba426695dd2fd3feb',NULL),('d97fd250341d4f97b27e5f78997d4092','db7bad475f114048bb2e87dd487a1308','cb28f213e495438dab2e750843aba6be',NULL),('dc8f32c1a6c2035af0458220844893f4','90b3384c28044e85aab06960e77afbc5','e16dd93c3b6f4d0bad4959fe9ebcad94',NULL),('dcb05757b5eb475c8bfeb6a1a745a7a3','db7bad475f114048bb2e87dd487a1308','3033d7be2f094b99aa71ac7142cdf918',NULL),('dd3614e06d3ea50d8929132a1cfcf8a0','b31b5c862daa4684bedb12e890310128','bf406095ff2841598826912a6064cc28',NULL),('dda2fbe7a087ed47f10ed4ebfc347f02','e647b3b72e4140db9b47d8dd69f208dc','cee60507074345c6a806affaee2711e9',NULL),('de516f067fd2143578fbbb22ded8ed1b','b31b5c862daa4684bedb12e890310128','5b96dbb0d80244bbb7a0036efa7cac9c',NULL),('e080b0eebf0add10a078e883bf2bce91','e647b3b72e4140db9b47d8dd69f208dc','dbd88ca037344af397bff479bfcd57b1',NULL),('e10a429b0b13dccdbcf0679bcdcf2a50','e647b3b72e4140db9b47d8dd69f208dc','b973f15a93ae4c1583b2cbdd18bdb09a',NULL),('e1662f6aa0c867a12421fe2129b88a13','b31b5c862daa4684bedb12e890310128','51b4c86fe59747c6829d9d856a4d142a',NULL),('e2ac8b3cc370c651d9f2d547a6ff2851','b31b5c862daa4684bedb12e890310128','e8d9507fb59a48ceaa0dda75b8f2b5ca',NULL),('e2e5a6c8555690865480383230ae4e7d','b31b5c862daa4684bedb12e890310128','8d091467450d4838b78ef2913978b235',NULL),('e3dc99681964183742e0be7f7545aaa3','b31b5c862daa4684bedb12e890310128','2281779d4f2a417fa4a986ee938b22b1',NULL),('e4242856b98ecbf86d8e489e79159b93','b31b5c862daa4684bedb12e890310128','cee60507074345c6a806affaee2711e9',NULL),('e5465c040fbe3d68f931f42f961775fa','90b3384c28044e85aab06960e77afbc5','4eec02eeb3874a84ae2e10b54b7148c3',NULL),('e5696f09fcc46c74be37992eb6729a54','b31b5c862daa4684bedb12e890310128','b554be9d62244cbbb97201a787aca996',NULL),('e5af242a8dbb45fdbdcc02cc71ba4307','db7bad475f114048bb2e87dd487a1308','4875d4539cdc4340abf6bb6183bb8f90',NULL),('e6207e845c1e4ad8b7c33f82c39ddca4','db7bad475f114048bb2e87dd487a1308','cf2f5fbd7320465aa8f85cea2d7e14ed',NULL),('e6ebe0b277ae305dd610e2386eff5b9f','e647b3b72e4140db9b47d8dd69f208dc','ed7408dd87ac4054956e7dbc54a9b47d',NULL),('e8c722c67718a2960daab7de53efde0f','b31b5c862daa4684bedb12e890310128','dbd88ca037344af397bff479bfcd57b1',NULL),('ea2480edd7946e6670dccb22deb86011','90b3384c28044e85aab06960e77afbc5','18dfdfcc9a874c9cb2b4b7ff81a78ebd',NULL),('ead0742eba6c4e08aa024db02f0397e9','db7bad475f114048bb2e87dd487a1308','0bd7300ca6934ae6b98bed985e9d9877',NULL),('eaea8b35dd32269f881846e5a9745cc0','90b3384c28044e85aab06960e77afbc5','61241c6baa96488b844a1ff4d184e574',NULL),('eb64b63d523b4263a9f73908b833b1a6','db7bad475f114048bb2e87dd487a1308','e6e40771579b45f4aab138b53febdf78',NULL),('ebd8eda44520a76c87643d40c9156c18','b31b5c862daa4684bedb12e890310128','033b4d93d3364fa7ad7a5f2023673c6b',NULL),('ebdd9ca2b1593ae30a31e8ee82967ab5','e647b3b72e4140db9b47d8dd69f208dc','7bdb5ff442b64bcfa12847d2fffc3115',NULL),('ec80509a6d6397993734d48d9feb4dc9','90b3384c28044e85aab06960e77afbc5','9da7daf3976744199bee3b0fad1f45c9',NULL),('ecb98ebeca323eb793a16517457e5539','e647b3b72e4140db9b47d8dd69f208dc','b554be9d62244cbbb97201a787aca996',NULL),('ee44ae0df53f378e597bdc2b78c26cb8','b31b5c862daa4684bedb12e890310128','0657931e4c50460eb913b73cf2f2fe24',NULL),('ee8d45f94edb4e9db811039a0c73e79a','db7bad475f114048bb2e87dd487a1308','61241c6baa96488b844a1ff4d184e574',NULL),('eed2b3b693e942dcb8857af5d5ff8f7e','db7bad475f114048bb2e87dd487a1308','ed7408dd87ac4054956e7dbc54a9b47d',NULL),('efff3bf82fc13cc27d29d08e24a96eae','90b3384c28044e85aab06960e77afbc5','e8ca082a7e0a47c6a2649a320fdf9ee0',NULL),('f0bc2bae0015eb50e6f8df90905a623b','e647b3b72e4140db9b47d8dd69f208dc','924218c0d78f4ea8b98ca2d5a527b16c',NULL),('f0cb5660db24a64e40b2aaa2940369b1','e647b3b72e4140db9b47d8dd69f208dc','42b836f7164d4fa9a5fbecc4d6a88c80',NULL),('f4e6df0752c242908ec24b9c8a6d2862','db7bad475f114048bb2e87dd487a1308','514e68f98eae410580251ba885810309',NULL),('f5bc3598b3a953097a3fe265e0992d2e','e647b3b72e4140db9b47d8dd69f208dc','4b28b089e98d49258754f4998bd48244',NULL),('f5d27044c3a82e88374e684ec1e27eca','90b3384c28044e85aab06960e77afbc5','331b9d69edbc4d94b10fe231e02c761f',NULL),('f5d66df4a0fcdc16ccd521387f0c9635','e647b3b72e4140db9b47d8dd69f208dc','dc698fde21ae454c84999946c71cb150',NULL),('f754d618e6283dc3691c68dbd32b1cad','e647b3b72e4140db9b47d8dd69f208dc','342eda268eb14af4b8270098ecff1fc0',NULL),('f8848eb8bc2a15042d0c1e413cbc4111','b31b5c862daa4684bedb12e890310128','79f1f4a01c3b429c993fe2669f18134b',NULL),('f935ba80043ad3c1247a35dcfd39b26d','90b3384c28044e85aab06960e77afbc5','d7ca61dabca4468985d137f34c3fe32f',NULL),('fb53fe1565ed192c901e2d0070cb85a1','b31b5c862daa4684bedb12e890310128','cb28f213e495438dab2e750843aba6be',NULL),('fbacacdfc75be25431cb4977f9da32d3','b31b5c862daa4684bedb12e890310128','3eb43a130f384b7c9525b91c5c4e597b',NULL),('fc3f2d1e82a5454096448abc486385f7','db7bad475f114048bb2e87dd487a1308','18dfdfcc9a874c9cb2b4b7ff81a78ebd',NULL),('fd209d0b9e984058345aefa8360dfdc6','e647b3b72e4140db9b47d8dd69f208dc','6ecdb6afcc7947d6af9dcb02b8f7549d',NULL),('fd779e0dbb0c8a42a2d8a8a1295d888d','e647b3b72e4140db9b47d8dd69f208dc','1aabaeeba3a04ffea1264a91a87e8287',NULL),('fe1432aa96e3890caa18c428a4175cef','90b3384c28044e85aab06960e77afbc5','acbf59c3dd6b421486adde17e153ed63',NULL),('fe1f72ff2bc9bb6c11041635717cb481','b31b5c862daa4684bedb12e890310128','d7ca61dabca4468985d137f34c3fe32f',NULL),('fe2fa012984bd4598522b69ea3a46053','90b3384c28044e85aab06960e77afbc5','f1bd9dd5aaa1442db3fa66ad472896b3',NULL),('fe87a180f9a049c0b2deff74cf2c8ff5','db7bad475f114048bb2e87dd487a1308','de85eb3426334d088d068e0e08f15521',NULL),('ff095ee97124f2cfbbffd6e85b802e1b','90b3384c28044e85aab06960e77afbc5','0fc4f8a072b04a88abb0848c69d1f883',NULL),('ffc8154ff1556f9a4ff1fea8c376deaf','e647b3b72e4140db9b47d8dd69f208dc','68906112da4a42038bad1660ebb8484c',NULL);

/*Table structure for table `t_mid_user_member_card` */

DROP TABLE IF EXISTS `t_mid_user_member_card`;

CREATE TABLE `t_mid_user_member_card` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键ID',
  `card_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '关联会员卡ID',
  `card_num` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员卡编号',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `start_time` bigint DEFAULT NULL COMMENT '有效期开始日期',
  `end_time` bigint DEFAULT NULL COMMENT '有效期结束日期',
  `order_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '关联订单id',
  `order_num` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单号',
  `is_status` tinyint DEFAULT NULL COMMENT '会员卡状态 0-是 1-否',
  `memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='会员卡和用户中间表';

/*Data for the table `t_mid_user_member_card` */

/*Table structure for table `t_mid_user_member_card_rights` */

DROP TABLE IF EXISTS `t_mid_user_member_card_rights`;

CREATE TABLE `t_mid_user_member_card_rights` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键ID',
  `mid_user_card_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '会员卡id',
  `card_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '会员卡id',
  `config_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '会员卡权益配置id',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID',
  `count` int DEFAULT NULL COMMENT '数量',
  `is_status` tinyint DEFAULT NULL COMMENT '会员卡状态 0-正常 1-失效 2-使用 3-过期',
  `batchnum` int DEFAULT NULL COMMENT '批次',
  `valid_days` int DEFAULT NULL COMMENT '有效天数',
  `start_time` bigint DEFAULT NULL COMMENT '有效开始时间',
  `end_time` bigint DEFAULT NULL COMMENT '有效结束时间',
  `memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` bigint DEFAULT NULL COMMENT '添加时间',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `is_type` int DEFAULT NULL COMMENT '关联类型 0-红娘牵线',
  `total_count` int DEFAULT NULL COMMENT '总数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='用户注册会员权益表';

/*Data for the table `t_mid_user_member_card_rights` */

/*Table structure for table `t_order_activity` */

DROP TABLE IF EXISTS `t_order_activity`;

CREATE TABLE `t_order_activity` (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '活动售卖订单表主键ID',
  `activity_price_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动库存ID',
  `activity_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动ID',
  `pay_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '付款用户ID',
  `order_num` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单号',
  `pay_time` bigint DEFAULT NULL COMMENT '付款时间',
  `act_cost` decimal(10,2) DEFAULT NULL COMMENT '活动付款金额',
  `real_cost` decimal(10,2) DEFAULT NULL COMMENT '实际付款金额',
  `discount_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '优惠类别  对应会员卡',
  `card_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '会员卡id',
  `card_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '会员卡号',
  `discount_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '折扣id',
  `discount_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '折扣',
  `payment_method` int(1) unsigned zerofill DEFAULT NULL COMMENT '支付方式 0微信 1会员卡',
  `pay_state` int DEFAULT NULL COMMENT '是否支付成功 0未成功 1已成功',
  `refund_start_time` bigint DEFAULT NULL COMMENT '退款发起时间',
  `refund_end_time` bigint DEFAULT NULL COMMENT '退款成功时间',
  `activity_people_number` int DEFAULT '0' COMMENT '活动人数',
  `is_order_status` int(1) unsigned zerofill DEFAULT NULL COMMENT '订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款  7已退款',
  `refund_to_describe` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '退款描述',
  `memo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `order_activity_date` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '下单活动日期的拼接',
  `sign_in_time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '签到时间',
  `is_sign_in` int DEFAULT NULL COMMENT '0未签到  1签到',
  `is_user_type` int DEFAULT NULL COMMENT '类型  2业主  1访客',
  `create_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者ID',
  `create_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者名称',
  `create_time` bigint DEFAULT NULL COMMENT '创建时间',
  `update_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者ID',
  `update_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者名称',
  `update_time` bigint DEFAULT NULL COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `delete_operator_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者ID',
  `delete_operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除者名称',
  `delete_time` bigint DEFAULT NULL COMMENT '删除时间',
  `extra_params1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段1',
  `extra_params2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段2',
  `extra_params3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段3',
  `extra_params4` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备用字段4',
  `refund_order_status` int(1) unsigned zerofill DEFAULT NULL COMMENT '申请退款时候订单状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='活动售卖订单表';

/*Data for the table `t_order_activity` */

/*Table structure for table `t_sys_account` */

DROP TABLE IF EXISTS `t_sys_account`;

CREATE TABLE `t_sys_account` (
  `ID` char(32) NOT NULL DEFAULT '' COMMENT '主键',
  `LoginName` char(50) DEFAULT NULL COMMENT '用户登录名',
  `AccountName` varchar(50) DEFAULT NULL COMMENT '用户真实姓名',
  `LoginPassword` char(255) DEFAULT NULL COMMENT '用户密码',
  `IsStatus` int DEFAULT '0' COMMENT '账号状态 0:正常/启用 1:冻结/禁用',
  `Photo` varchar(100) DEFAULT NULL COMMENT '头像路径',
  `AccountCode` varchar(50) DEFAULT NULL COMMENT '员工编号',
  `RoleID` char(32) DEFAULT '' COMMENT '角色ID',
  `AccountAuthority` text COMMENT '用户单独设置权限',
  `Email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `Phone` varchar(50) DEFAULT NULL COMMENT '手机',
  `Sex` int DEFAULT '0' COMMENT '性别 0:未知 1:男 2:女',
  `DepartmentID` varchar(500) DEFAULT NULL COMMENT '部门id',
  `Address` varchar(100) DEFAULT NULL COMMENT '住址',
  `Memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `IsDelete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `CreateOperatorID` char(32) DEFAULT NULL COMMENT '创建者ID',
  `CreateOperatorName` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `CreateTime` bigint DEFAULT NULL COMMENT '创建时间',
  `UpdateOperatorID` char(32) DEFAULT NULL COMMENT '更新者ID',
  `UpdateOperatorName` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `UpdateTime` bigint DEFAULT NULL COMMENT '更新时间',
  `DeleteOperatorID` char(32) DEFAULT NULL COMMENT '删除者ID',
  `DeleteOperatorName` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `DeleteTime` bigint DEFAULT NULL COMMENT '删除时间',
  `Type` int DEFAULT '1' COMMENT '管理员类型 0:超级管理员 1普通管理员',
  `ExtraParams1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  `ExtraParams2` varchar(100) DEFAULT NULL COMMENT '额外参数2',
  `ExtraParams3` varchar(100) DEFAULT NULL COMMENT '额外参数3',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统管理-管理员表';

/*Data for the table `t_sys_account` */

insert  into `t_sys_account`(`ID`,`LoginName`,`AccountName`,`LoginPassword`,`IsStatus`,`Photo`,`AccountCode`,`RoleID`,`AccountAuthority`,`Email`,`Phone`,`Sex`,`DepartmentID`,`Address`,`Memo`,`IsDelete`,`CreateOperatorID`,`CreateOperatorName`,`CreateTime`,`UpdateOperatorID`,`UpdateOperatorName`,`UpdateTime`,`DeleteOperatorID`,`DeleteOperatorName`,`DeleteTime`,`Type`,`ExtraParams1`,`ExtraParams2`,`ExtraParams3`) values ('fe3cc2241d3f40e38a788a77fcd98581','admin','admin','4e95d0753e2ee0f676ae5abc41cb7e7b',0,'group1/M00/00/6F/rBH812EtigCAHFHyAABLMCGeJ74546.png','','b31b5c862daa4684bedb12e890310128',NULL,'xxx@163.com1','15901203132',0,'3423e59327614684af27fefa0bb581d2','',NULL,0,'550e1bafe077ff0b0b67f4e32f29d751','admin',1621848136977,'fe3cc2241d3f40e38a788a77fcd98581','admin',1645078218991,NULL,NULL,NULL,0,NULL,NULL,NULL);

/*Table structure for table `t_sys_menu` */

DROP TABLE IF EXISTS `t_sys_menu`;

CREATE TABLE `t_sys_menu` (
  `ID` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `MenuName` varchar(100) DEFAULT NULL COMMENT '菜单名称',
  `Icon` varchar(200) DEFAULT NULL COMMENT '菜单或按钮icon',
  `MenuCode` char(100) DEFAULT NULL COMMENT '菜单标识',
  `MenuLevel` int DEFAULT NULL COMMENT '菜单层级 1:一级菜单 2:二级菜单 依次类推...',
  `ParentCode` char(32) DEFAULT NULL COMMENT '父菜单标识',
  `ParentID` char(32) DEFAULT NULL COMMENT '父菜单ID',
  `Sort` int DEFAULT '0' COMMENT '排序号 数据越高排序越大 0是最低',
  `MenuType` int DEFAULT '0' COMMENT '类型 0-菜单 1-按钮',
  `IsState` int DEFAULT '0' COMMENT '是否启用 0-是 1-否',
  `ButtonType` int DEFAULT '0' COMMENT '按钮区分 1：增改  2：查询 3：删除',
  `Url` varchar(200) DEFAULT NULL COMMENT '链接地址',
  `PagePath` varchar(200) DEFAULT NULL COMMENT '页面路由',
  `Memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `IsDelete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `CreateOperatorID` char(32) DEFAULT NULL COMMENT '创建者ID',
  `CreateOperatorName` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `CreateTime` bigint DEFAULT NULL COMMENT '创建时间',
  `UpdateOperatorID` char(32) DEFAULT NULL COMMENT '更新者ID',
  `UpdateOperatorName` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `UpdateTime` bigint DEFAULT NULL COMMENT '更新时间',
  `DeleteOperatorID` char(32) DEFAULT NULL COMMENT '删除者ID',
  `DeleteOperatorName` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `DeleteTime` bigint DEFAULT NULL COMMENT '删除时间',
  `ExtraParams1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  `ExtraParams2` varchar(100) DEFAULT NULL COMMENT '额外参数2',
  `ExtraParams3` varchar(100) DEFAULT NULL COMMENT '额外参数3',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统管理-菜单表';

/*Data for the table `t_sys_menu` */

insert  into `t_sys_menu`(`ID`,`MenuName`,`Icon`,`MenuCode`,`MenuLevel`,`ParentCode`,`ParentID`,`Sort`,`MenuType`,`IsState`,`ButtonType`,`Url`,`PagePath`,`Memo`,`IsDelete`,`CreateOperatorID`,`CreateOperatorName`,`CreateTime`,`UpdateOperatorID`,`UpdateOperatorName`,`UpdateTime`,`DeleteOperatorID`,`DeleteOperatorName`,`DeleteTime`,`ExtraParams1`,`ExtraParams2`,`ExtraParams3`) values ('0117ad66f9444c9e83870450c7de2b38','删除','','delete',4,NULL,'28f44231b0614e20824d0a524b215130',1,1,0,3,'v1:user:feedbackcate:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031245427,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('017090c7ed094c2ebdd8706f8008bd3b','修改','','update',3,NULL,'7f88ae69b83b497cb5aebb4f927ba029',3,1,0,4,'v1:rooms:consumable:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626413869126,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('01712084a7f242adb86960a9f4b235c9','详情','','info',3,NULL,'4134b1a0ec6b43e4a5300668426876b7',2,1,0,2,'v1:order:order:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625016737027,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('02a7ba106a174dbbbff013385b7b4a30','删除','','delete',4,NULL,'aaff788d0f3644b9870678460e691d90',1,1,0,3,'v1:user:user:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623225284225,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('02d8433603974e0787e90e34ddfba75d','详情','','info',3,NULL,'56bf141c668e49668e7b3e99a0fa8ef5',2,1,0,2,'v1:sys:channel:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626831121794,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('033b4d93d3364fa7ad7a5f2023673c6b','内容管理','','caseManage',2,NULL,'1bcf1ef5f8984692aaea5784f0df9742',0,0,0,0,'','/caseVideo/caseManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638517455551,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('034d9a198d874b5baff04f1b10460799','渠道手续费','','procedure',2,NULL,'68906112da4a42038bad1660ebb8484c',4,0,0,0,'','/system/procedure','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1628141761462,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412055437,NULL,NULL,NULL),('0492d760325c4f6f90f74a7b56234d57','详情','','info',3,NULL,'a12896ba791a409f9f25a27299326ecd',2,1,0,2,'v1:sys:role:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410283400,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0657931e4c50460eb913b73cf2f2fe24','角色管理','','authority',2,NULL,'68906112da4a42038bad1660ebb8484c',1,0,0,0,'','/system/authority','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623421590,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625647724224,NULL,NULL,NULL,NULL,NULL,NULL),('0694482838274008a2afcf3b25380927','房源档案','','record',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',0,0,0,0,'','/houseCollocation/record','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625731543430,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('07aa3372bd7e4bb4a9fd0900539b6997','详情','','info',3,NULL,'d49f38e3d7fe49beb03bb93ce5b78746',2,1,0,2,'v1:house:trusteeship:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626844904503,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('080f2e1502054cbbb075e9774e6634fe','部门管理','','organizatioon',2,NULL,'bd770fef9c304b46be16954aacd4359b',4,0,0,0,'','/system/organizatioon','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410078584,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('09447f67a7824e4080a05b8c8de6b823','添加','','add',3,NULL,'3d478012b51c472d97571647e35aaab6',0,1,0,1,'v1:house:source:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288399981,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('09923210b5564f9b87cee339e19956ed','详情','','info',3,NULL,'9de96dcd1251461bb122e6640c97addb',2,1,0,2,'v1:house:source:getListToOwnerStay','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1632883131502,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0abfc4ab4dda487692d3d57c1f3328f3','房源管理','','housingManage',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',1,2,0,0,'','/housingManage','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622769812518,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1623288247104,NULL,NULL,NULL),('0af28d52df2a4621a78be954265b86c0','包房表','','channelRoomsTable',3,NULL,'70b022dcfdc645338c750e1a4aa9c3dc',2,0,0,0,'','/channelRoomsTable','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624503712664,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0b7cc705df084002bd8bfb9a25524e6a','早餐管理','','breakFast',2,NULL,'68906112da4a42038bad1660ebb8484c',6,0,0,0,'','/system/breakFast','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625795244647,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625798365086,NULL,NULL,NULL),('0ba2ee73b3564a46afb17105ca91d837','黑名单','','blacklist',3,NULL,'431903c381114040a2e4fb1f21f0c0c4',2,0,0,0,'','/userManage/blacklist','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227464065,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0bd7300ca6934ae6b98bed985e9d9877','修改','','update',3,NULL,'0657931e4c50460eb913b73cf2f2fe24',3,1,0,4,'v1:sys:role:update','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623561412,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0c701b2acd3a4c78ba63a223a0dbde46','用户','','userManage',2,NULL,'a4d73aa0bdc84efc804dccdf994ed5e2',1,0,0,0,'','/home/userManage','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623224746690,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1623224764939,NULL,NULL,NULL),('0d13021d34164905890ce3f5cd457560','详情','','info',3,NULL,'8221389f1ade47a4847ae8e6243488de',2,1,0,2,'v1:house:policy:getInfo','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1630318666477,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0d95e2c84945437b8fd53839e72b4407','房源管理','','house',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',0,2,0,0,'','/homestayRentOrSale/house','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625730850837,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625731502409,NULL,NULL,NULL),('0e68e9f3ff0747449abed5f02f79651d','用户管理','','user',2,NULL,'57638abb63c24b9590f9ae3e29bff89a',2,2,0,0,'','/homestayRentOrSale/user','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734478688,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0f1250ecd92742bdb9564a12dc96eb9d','预订须知','','reservePolicy',2,NULL,'57638abb63c24b9590f9ae3e29bff89a',4,0,0,0,'','/homestayRentOrSale/reservePolicy','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1630318714836,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0f66ee9fe4ae4ea380f8545a3b74e1d1','房务','group1/M00/00/73/rBH812Gl0tKAQ3ejAAAZXU8aQRY403.jpg','rooms',1,'1','',4,0,0,0,'','/rooms','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623914588596,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638257444658,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638411927245,NULL,NULL,NULL),('0f73ce2ba80d4cc3b0dbccee07597454','详情','','info',4,NULL,'2357ad803ee34fc7ad162bb4292ad837',2,1,0,2,'v1:user:blacklist:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625735313526,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625738128446,NULL,NULL,NULL,NULL,NULL,NULL),('0f95c40320a64453a4415ec38dd12122','详情','','info',3,NULL,'b5542b1619004195b8c8fd7ded0ee7ea',2,1,0,2,'v1:house:status:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624861337189,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624861363825,NULL,NULL,NULL,NULL,NULL,NULL),('0f9d2fd3ccd448fa8b457b96f5f2c91b','删除','','delete',4,NULL,'70bab8e4cb0d49b6a1962afbfdc7b8b8',1,1,0,3,'v1:house:status:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080805916,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0fc4f8a072b04a88abb0848c69d1f883','详情','','info',3,NULL,'5af51ebd276f4bc695b988e09cf1d4be',2,1,0,2,'v1:card:order:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639372599552,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0fce9f7925ff4c478cdfcc124aa3cb96','删除','','delete',3,NULL,'7f88ae69b83b497cb5aebb4f927ba029',1,1,0,3,'v1:rooms:consumable:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626413856870,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0fd5aa2753834ebb88b5de52474d5ca0','添加','','add',3,NULL,'8d091467450d4838b78ef2913978b235',0,1,0,1,'v1:sys:menu:save','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623132171,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('10d5a3b9165e4951b4d4cf937ea03325','详情','','info',3,NULL,'0657931e4c50460eb913b73cf2f2fe24',2,1,0,2,'v1:sys:role:list','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623521233,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('113b39b262fd477ca308da793f97a7b8','添加','','add',3,NULL,'8ffed7ca28e146eba9d89ca93ff1218e',0,1,0,1,'v1:sys:account:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410307980,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('131261a403704cc99d93ec76256a134e','详情','','info',3,NULL,'1604fae269424584ab0b0491598ab129',2,1,0,2,'v1:sys:room:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626254225715,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('13214987ac6c4ad2a26d55259768c732','添加','','add',3,NULL,'034d9a198d874b5baff04f1b10460799',0,1,0,1,'v1:sys:procedure:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1635304149500,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('13b64c9b7dbb4c95b966f888864b8ea3','添加','','add',3,NULL,'9de96dcd1251461bb122e6640c97addb',0,1,0,1,'v1:house:ownerStay:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1632883108354,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('146c622929cd4c189510da6b3e7639b4','添加','','add',3,NULL,'1604fae269424584ab0b0491598ab129',0,1,0,1,'v1:sys:room:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626254210667,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('14820c277ff6439fa8c2fe3176ea323d','修改','','update',4,NULL,'6767333ea8e044449535d89488b113c3',3,1,0,4,'v1:user:group:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227932858,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('15747571e9a2420bb1e5068b5cb6e995','详情','','info',3,NULL,'7f88ae69b83b497cb5aebb4f927ba029',2,1,0,2,'v1:rooms:repertory:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626660526175,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('15cf3bb15d3d41c099ddba422eb91215','用户管理','','userManage',2,NULL,'51b4c86fe59747c6829d9d856a4d142a',0,0,0,0,'','/user/userManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638422188289,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1604fae269424584ab0b0491598ab129','房型管理','','room',2,NULL,'68906112da4a42038bad1660ebb8484c',8,0,0,0,'','/system/room','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626254191379,'fe3cc2241d3f40e38a788a77fcd98581','admin',1634001153234,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412023251,NULL,NULL,NULL),('16c9696fd231435fa14153e9689306a8','删除','','delete',3,NULL,'0694482838274008a2afcf3b25380927',1,1,0,3,'v1:house:source:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625731870988,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('18dfdfcc9a874c9cb2b4b7ff81a78ebd','添加','','add',3,NULL,'e8ca082a7e0a47c6a2649a320fdf9ee0',0,1,0,1,'v1:sys:images:saveBatch','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288944045,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288978734,NULL,NULL,NULL,NULL,NULL,NULL),('1954e32551094452a9b844a514418275','删除','','delete',3,NULL,'033b4d93d3364fa7ad7a5f2023673c6b',1,1,0,3,'v1:vide:whiteheadabout:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031562018,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1a24f0bac52f41a6b6be13804e40155d','修改','','update',3,NULL,'7f88ae69b83b497cb5aebb4f927ba029',3,1,0,4,'v1:rooms:consumable:updateRepertory','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1627520707830,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1aabaeeba3a04ffea1264a91a87e8287','添加','','add',3,NULL,'033b4d93d3364fa7ad7a5f2023673c6b',0,1,0,1,'v1:vide:whiteheadabout:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031530274,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1bcf1ef5f8984692aaea5784f0df9742','视频案例','group1/M00/00/7B/rBH812HJLzmATFtBAAAT1uaO9NQ916.png','caseVideo',1,'1','',2,0,0,0,'','/caseVideo','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638517174521,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640574820094,NULL,NULL,NULL,NULL,NULL,NULL),('1c1e3b41cf5447d7ab5402267b1a9ab7','修改','','update',4,NULL,'dff51fb853e24032aa832784549d095a',3,1,0,4,'v1:house:dailyPrice:delete','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623062162723,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1ed3a1d1a084450183f030ce7fccc6e7','房态','','housStatus',2,NULL,'a4c1aa15e033431fa5b9583d33761481',0,0,0,0,'','/frontDesk/housStatus','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624666974429,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625653818848,NULL,NULL,NULL,NULL,NULL,NULL),('207d15a9644f48e38eae87c2076697af','详情','','info',3,NULL,'75565354178b4dcfb61a74662cab9fcb',2,1,0,2,'v1:house:status:getAll','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626068029001,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2156385cffb948389e05ad7fb7952516','详情','','info',3,NULL,'d95cd781d7d449ef99ff5503ae7d1e9b',2,1,0,2,'v1:sys:steward:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626258072462,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('220ea8bd6f9042189918b5fce59aadbe','修改','','update',3,NULL,'dc79f41ec1dc442d9237c2d3a75349e8',3,1,0,4,'v1:house:trusteeship:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626850938250,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2281779d4f2a417fa4a986ee938b22b1','系统管理员','','account',2,NULL,'68906112da4a42038bad1660ebb8484c',2,0,0,0,'','/system/account','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622715394534,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625647662685,NULL,NULL,NULL,NULL,NULL,NULL),('2357ad803ee34fc7ad162bb4292ad837','黑名单','','blacklist',3,NULL,'0e68e9f3ff0747449abed5f02f79651d',2,0,0,0,'','/homestayRentOrSale/user/blacklist','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734587002,'fe3cc2241d3f40e38a788a77fcd98581','admin',1627263524779,NULL,NULL,NULL,NULL,NULL,NULL),('25ba986d6b7945a0a1f700c128d0abb5','修改','','update',4,NULL,'0ba2ee73b3564a46afb17105ca91d837',3,1,0,4,'v1:user:blacklist:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227988220,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('27c6a95a18114fcca36231ac7ce70ec5','添加','','add',3,NULL,'0f1250ecd92742bdb9564a12dc96eb9d',0,1,0,1,'v1:house:policy:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1630318729150,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('28f44231b0614e20824d0a524b215130','分类管理','','feedBackCategory',3,NULL,'730c8076493746c38cc90842f18936dc',0,0,0,0,'','/operation/feedBack/feedBackCategory','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409418952,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639034095647,NULL,NULL,NULL,NULL,NULL,NULL),('2a93f41742664c4c9df1cf0f2dba9b6c','任务统计','','taskSchedule',2,NULL,'0f66ee9fe4ae4ea380f8545a3b74e1d1',2,0,0,0,'','/rooms/taskSchedule','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624358427047,'fe3cc2241d3f40e38a788a77fcd98581','admin',1633758751894,NULL,NULL,NULL,NULL,NULL,NULL),('2b23f97bb41747b4833396b375804d0d','删除','','delete',3,NULL,'d95cd781d7d449ef99ff5503ae7d1e9b',1,1,0,3,'v1:sys:steward:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626258087060,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2b4d91771ae14a5aac9400683e0bdcf7','修改','','update',3,NULL,'57f8feaf4b13417599a163d7a4f3a0a4',3,1,0,4,'v1:rooms:cleaner:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626313412110,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2e57155c29c44bb0bcea319f91225262','添加','','add',4,NULL,'dff51fb853e24032aa832784549d095a',0,1,0,1,'v1:house:dailyPrice:save','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623062099431,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623062131369,NULL,NULL,NULL,NULL,NULL,NULL),('2f264a5b0ef546bbb959d896268284a9','修改','','update',3,NULL,'1604fae269424584ab0b0491598ab129',3,1,0,4,'v1:sys:room:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626254263877,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2f4c6807a165410385b5ce1438e4af41','详情','','info',4,NULL,'81ed5443ac07488b8fd66a4c26d52550',2,1,0,2,'v1:user:feedback:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031297412,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2f539f9b500e4f9a93e7156816c0b8b6','删除','','delete',3,NULL,'57c866a96d6b4edb865a5a2842a0ebde',1,1,0,3,'v1:sys:discount:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1634004531752,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('301cde1a256c40f5bf90e6f54ca8b3c9','删除','','delete',3,NULL,'d49f38e3d7fe49beb03bb93ce5b78746',1,1,0,3,'v1:house:trusteeship:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626844918250,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3033d7be2f094b99aa71ac7142cdf918','添加','','add',3,NULL,'3eb43a130f384b7c9525b91c5c4e597b',0,1,0,1,'v1:sys:organizatioon:save','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622622219907,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3137f93c90f14cb58e3956c37fe17cc7','详情','','info',3,NULL,'3e04f37276854caf996f9fddccfa248e',2,1,0,2,'v1:sys:bed:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626256665016,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('329dbe16b1434f5298ff9b60d976cd89','添加','','add',4,NULL,'70bab8e4cb0d49b6a1962afbfdc7b8b8',0,1,0,1,'v1:house:status:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080776730,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('331b9d69edbc4d94b10fe231e02c761f','详情','','info',4,NULL,'f57de605937046dba426695dd2fd3feb',2,1,0,2,'v1:user:report:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031058844,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031092812,NULL,NULL,NULL,NULL,NULL,NULL),('342eda268eb14af4b8270098ecff1fc0','删除','','delete',3,NULL,'684b2b6e78e148ba959f60d153e6664b',1,1,0,3,'v1:card:memberCard:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031667989,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('350543b7b1f7403ebf64cc062c2b4346','删除','','delete',3,NULL,'56bf141c668e49668e7b3e99a0fa8ef5',1,1,0,3,'v1:sys:channel:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626831134325,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3734f8e72a794ddeb08511708da3a628','订单','','housStatus',2,NULL,'a4c1aa15e033431fa5b9583d33761481',1,0,0,0,'','/frontDesk/order','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624667032772,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625654188809,NULL,NULL,NULL,NULL,NULL,NULL),('3bdedf95ddb14302b741ce7cfbfabf33','添加','','add',3,NULL,'d49f38e3d7fe49beb03bb93ce5b78746',0,1,0,1,'v1:house:trusteeship:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626844891608,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3c7ef9101bea4bb1bccf87f53172579c','房东','','landlordManage',2,NULL,'a4d73aa0bdc84efc804dccdf994ed5e2',2,2,0,0,'','/landlordManage','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623287986294,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1627009638439,NULL,NULL,NULL),('3cf8ee6210ec4417aaca61e613ff385a','删除','','delete',3,NULL,'cbacdc2c2df3490b8481b9d5fdbac114',1,1,0,3,'v1:sys:salesman:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626665524249,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3d478012b51c472d97571647e35aaab6','房源管理','','houseManage',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',0,0,0,0,'','/houseManage','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288310078,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625384976512,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625730742791,NULL,NULL,NULL),('3d61eb262fc1495eacd8cfca935b6dee','删除','','delete',3,NULL,'1604fae269424584ab0b0491598ab129',1,1,0,3,'v1:sys:room:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626254243259,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3dfae62545f44756991da6cec535afea','房态','','channelRoomState',3,NULL,'70b022dcfdc645338c750e1a4aa9c3dc',0,0,0,0,'','/channelRoomState','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624503674822,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3e04f37276854caf996f9fddccfa248e','床型管理','','bed',2,NULL,'68906112da4a42038bad1660ebb8484c',7,0,0,0,'','/system/bed','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626256615752,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412033456,NULL,NULL,NULL),('3e0667497231454b85b15179632c1a8e','添加','','add',4,NULL,'c0377b2f3a2e48bdaa5e7ed665a3cb36',0,1,0,1,'v1:house:records:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080760714,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3eb43a130f384b7c9525b91c5c4e597b','部门管理','','organizatioon',2,NULL,'68906112da4a42038bad1660ebb8484c',3,0,0,0,'','/system/organizatioon','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622620580431,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625647671315,NULL,NULL,NULL,NULL,NULL,NULL),('3f6b7410e78a4f17acbe773db0b7865d','修改','','update',3,NULL,'d80e2098db174faf947127cb2ad09545',3,1,0,4,'v1:sys:habitable:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626255567286,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3fa44e82e659475085a83d8027ecc6ee','添加','','add',4,NULL,'aaff788d0f3644b9870678460e691d90',0,1,0,1,'v1:user:user:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623225252553,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4134b1a0ec6b43e4a5300668426876b7','订单中心','','orderCenter',2,NULL,'449b7f0851e94c4ca5accbe20f95a2ba',0,0,0,0,'','/order/orderCenter','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624861264543,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625653342860,NULL,NULL,NULL,NULL,NULL,NULL),('42b836f7164d4fa9a5fbecc4d6a88c80','添加','','add',4,NULL,'28f44231b0614e20824d0a524b215130',0,1,0,1,'v1:user:feedbackcate:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031215889,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('431903c381114040a2e4fb1f21f0c0c4','用户','','userManage',2,NULL,'a4d73aa0bdc84efc804dccdf994ed5e2',2,2,0,0,'','/userManage','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623224794023,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623225427773,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1627009645861,NULL,NULL,NULL),('436c361e98414d9fa238217c3259daee','删除','','delete',3,NULL,'a12896ba791a409f9f25a27299326ecd',1,1,0,3,'v1:sys:role:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410271814,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4407ef400b234d2197fe6d491b26ba80','修改','','update',3,NULL,'8ffed7ca28e146eba9d89ca93ff1218e',3,1,0,4,'v1:sys:account:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410324944,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('446b09b184b846dda2df6cfa233b5cb4','修改','','update',3,NULL,'0b7cc705df084002bd8bfb9a25524e6a',3,1,0,4,'v1:sys:breakfast:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625795433383,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('449b7f0851e94c4ca5accbe20f95a2ba','订单','group1/M00/00/73/rBH812Gl0tGAHNZ9AAAS22CN1Oc099.jpg','order',1,'1','',7,0,0,0,'','order','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624861226494,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638257518116,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638411859989,NULL,NULL,NULL),('4875d4539cdc4340abf6bb6183bb8f90','修改','','update',3,NULL,'a60d8becabe94226b01aabe8a0823ab1',3,1,0,4,'v1:operation:banner:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031167772,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('48eda6c11fa94dbb8a181d11b1b0cf0b','详情','','info',3,NULL,'93ee32e1cb0041aaa8d5ed1ed38c1089',2,1,0,2,'v1:sys:holiday:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623377521680,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4a47d73d732748909efafbfeded3b168','修改','','update',4,NULL,'70bab8e4cb0d49b6a1962afbfdc7b8b8',3,1,0,4,'v1:house:status:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080818153,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4b0bcf5e655b419386c16048434026e2','添加','','add',4,NULL,'81ed5443ac07488b8fd66a4c26d52550',0,1,0,1,'v1:user:feedback:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031278607,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4b28b089e98d49258754f4998bd48244','详情','','info',3,NULL,'689a1f346d3444669aa5806a0d2f3aea',2,1,0,2,'v1:log:userLogin:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638414070873,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4b6b513a18ff47e48232650111c92ef4','添加','','add',3,NULL,'3e04f37276854caf996f9fddccfa248e',0,1,0,1,'v1:sys:bed:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626256634660,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4b805344ba0f47d1827ea9b70ead39b2','详情','','info',3,NULL,'4c8c451d4bc14fa98c8cd39f01742dbc',2,1,0,2,'v1:house:dailyPrice:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288550314,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4ba7e74502a840359a2c066105428731','修改','','update',4,NULL,'28f44231b0614e20824d0a524b215130',3,1,0,4,'v1:user:feedbackcate:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031260380,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4c4e03dd408c40ddb56357677e0739c9','配置渠道','','configChannel',3,NULL,'53706141879242c5b830a45ab6ffa8c0',0,0,0,0,'','/configChannel','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624503050620,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624503790470,NULL,NULL,NULL,NULL,NULL,NULL),('4c8c451d4bc14fa98c8cd39f01742dbc','房价管理','','propertyManage',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',1,0,0,0,'','/propertyManage','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288495308,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625730749042,NULL,NULL,NULL),('4cf91156d7194791a51b8ab1a4f53383','详情','','info',3,NULL,'033b4d93d3364fa7ad7a5f2023673c6b',2,1,0,2,'v1:vide:whiteheadabout:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031548376,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4d5591839e64476296dcf6593d5c48bb','添加','','add',4,NULL,'aabdd886699e47ff8c79fbf3e3d81527',0,1,0,1,'v1:house:source:save','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622769919843,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4dadceaa42ff464c89e24f2842070ef1','详情','','info',4,NULL,'aabdd886699e47ff8c79fbf3e3d81527',2,1,0,2,'v1:house:source:list','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622769966187,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622769983619,NULL,NULL,NULL,NULL,NULL,NULL),('4e9dd406cce4424ab176a211294faacc','日志管理','','cardLog',2,NULL,'e6e40771579b45f4aab138b53febdf78',2,0,0,0,'','/memberCard/cardLog','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638437747399,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638438404339,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1639706552777,NULL,NULL,NULL),('4eec02eeb3874a84ae2e10b54b7148c3','详情','','info',3,NULL,'514e68f98eae410580251ba885810309',2,1,0,2,'v1:activityCate:activityCate:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031383923,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5088856f790c44729918fa86e518dc8a','修改','','update',4,NULL,'2357ad803ee34fc7ad162bb4292ad837',3,1,0,4,'v1:user:blacklist:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625735357145,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('514e68f98eae410580251ba885810309','分类管理','','activityCategory',2,NULL,'6dabd809142f47bb87b2a44d4b39b39f',0,0,0,0,'','/activity/activityCategory','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638423696970,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638432519418,NULL,NULL,NULL,NULL,NULL,NULL),('51b4c86fe59747c6829d9d856a4d142a','用户管理','group1/M00/00/7B/rBH812HJLzqADvMQAAAEg9zq668463.png','user',1,'1','',3,0,0,0,'','/user','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638421730874,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640574829711,NULL,NULL,NULL,NULL,NULL,NULL),('52c7a8ea3ed64f2cb05cd97a3f45ea5f','修改','','update',4,NULL,'9e0b860c1f6245e4bf1ee248b7159bd4',3,1,0,4,'v1:sys:elseExpense:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625811173332,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('532a8dfdc1a84b019a82e6264cf25221','举报管理','','report',2,NULL,'51b4c86fe59747c6829d9d856a4d142a',2,2,0,0,'','/user/report','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638422592001,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639030778995,NULL,NULL,NULL,NULL,NULL,NULL),('53706141879242c5b830a45ab6ffa8c0','渠道管理','','channelManage',2,NULL,'a445a38cf28d4f14bbeb1126eda640a8',0,2,0,0,'','/channelManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624503030522,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('53853a64d57a4e5dba82c0c9cdc7fcb5','删除','','delete',3,NULL,'54379385730e459a867b4c18911da87f',1,1,0,3,'v1:sys:breakfast:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625798050002,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('54198ba6027641a5ba1e5710dcb4dfab','修改','','update',4,NULL,'ea380ddc41d147008176dce03b5d34a5',3,1,0,4,'v1:user:group:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625735462763,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('54379385730e459a867b4c18911da87f','早餐管理','','breakFast',2,NULL,'68906112da4a42038bad1660ebb8484c',11,0,0,0,'','/system/breakFast','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625797774425,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626665474777,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412002579,NULL,NULL,NULL),('547a043ef4e04aae9613f5d89b39522b','修改','','update',4,NULL,'f57de605937046dba426695dd2fd3feb',3,1,0,4,'v1:user:report:dealWithResults','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031080826,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5566d4cae5344bb4b082d174d0e07138','添加','','add',4,NULL,'2357ad803ee34fc7ad162bb4292ad837',0,1,0,1,'v1:user:blacklist:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734996248,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('55a6a9e1c88544fd92be6a97e974dd4c','自营订单','','proprietaryOrder',2,NULL,'57638abb63c24b9590f9ae3e29bff89a',0,0,0,0,'','/proprietaryOrder','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625020688805,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625732517877,NULL,NULL,NULL),('5623208c2a16419a849cdeecaffa3d08','添加','','add',3,NULL,'57c866a96d6b4edb865a5a2842a0ebde',0,1,0,1,'v1:sys:discount:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1634004516773,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('56a5884d370f4b28b510ba0717443263','日常运营','group1/M00/00/7B/rBH812HJLzmAL9QFAAACfD7XM48242.png','operation',1,'1','',0,0,0,0,'','/operation','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409125337,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640574794876,NULL,NULL,NULL,NULL,NULL,NULL),('56bf141c668e49668e7b3e99a0fa8ef5','渠道配置','','channelConfig',2,NULL,'93f5db9524c446a78211d132a2a4243e',0,0,0,0,'','/channel/channelConfig','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626831086019,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('571fd0d7bb7b4961a0df456f9f5459a4','托管人管理','','owner',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',2,0,0,0,'','/houseCollocation/owner','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625731580459,'fe3cc2241d3f40e38a788a77fcd98581','admin',1633937395708,NULL,NULL,NULL,NULL,NULL,NULL),('57638abb63c24b9590f9ae3e29bff89a','民宿租售','group1/M00/00/73/rBH812Gl0tOANVHCAAAWN_wOeBQ845.jpg','homestayRentOrSale',1,'1','',2,0,0,0,'','/homestayRentOrSale','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625020612963,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638257466995,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638411882139,NULL,NULL,NULL),('5790aa568a8544ec9b335b614139288d','详情','','info',3,NULL,'0b7cc705df084002bd8bfb9a25524e6a',2,1,0,2,'v1:sys:breakfast:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625795405596,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('57c866a96d6b4edb865a5a2842a0ebde','折扣管理','','discountManage',2,NULL,'68906112da4a42038bad1660ebb8484c',5,0,0,0,'','/system/discountManage','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1633948554085,'fe3cc2241d3f40e38a788a77fcd98581','admin',1634001112254,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412046518,NULL,NULL,NULL),('57f8feaf4b13417599a163d7a4f3a0a4','保洁员','','cleaner',2,NULL,'0f66ee9fe4ae4ea380f8545a3b74e1d1',0,0,0,0,'','/rooms/cleaner','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623914613274,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626313339087,NULL,NULL,NULL,NULL,NULL,NULL),('592e47ef73714c4c93ec487ec4fe8c00','收益流水','','earnings',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',3,0,0,0,'','/houseCollocation/earnings','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625731599557,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626844878671,NULL,NULL,NULL,NULL,NULL,NULL),('5a38c3b9e0274ebea572a4adeb3e8e4f','删除','','delete',4,NULL,'c7d3d2f1ecab4deda35ec5f0fa4353b9',1,1,0,3,'v1:house:source:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080645394,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5ada274d58844671968b88d928708325','详情','','info',3,NULL,'080f2e1502054cbbb075e9774e6634fe',2,1,0,2,'v1:sys:organizatioon:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410398696,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5af51ebd276f4bc695b988e09cf1d4be','订单管理','','cardOrder',2,NULL,'e6e40771579b45f4aab138b53febdf78',1,0,0,0,'','/card/cardOrder','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638437730296,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640945961663,NULL,NULL,NULL,NULL,NULL,NULL),('5b96dbb0d80244bbb7a0036efa7cac9c','详情','','info',3,NULL,'689a1f346d3444669aa5806a0d2f3aea',2,1,0,2,'v1:log:accountLogin:list','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623330173,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638422412852,NULL,NULL,NULL,NULL,NULL,NULL),('5bc45f127ef1417ca8a6b10ed06bb341','预定管理','','reserve',2,NULL,'57638abb63c24b9590f9ae3e29bff89a',1,2,0,0,'','/homestayRentOrSale/reserve','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734438623,'fe3cc2241d3f40e38a788a77fcd98581','admin',1627886215008,NULL,NULL,NULL,NULL,NULL,NULL),('5c7ce7e364ae49178438777267a0d00d','删除','','delete',3,NULL,'8ffed7ca28e146eba9d89ca93ff1218e',1,1,0,3,'v1:sys:account:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410335631,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5e03bb7eccb9422bb755f1c2048b9069','出租房源','','rentalHousing',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',4,0,1,0,'','/rentalHousing','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623830541748,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625384980989,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625730759855,NULL,NULL,NULL),('5e126ab933db423ab82d9efb9acd1547','添加','','add',3,NULL,'f9577a3dbdb544d4b2e269e323e0ff3c',0,1,0,1,'v1:sys:menu:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410143320,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5e31159c0d134a05b72f0e31bb6b7f5a','业主自住','','ownerOccupied',3,NULL,'3c7ef9101bea4bb1bccf87f53172579c',2,0,0,0,'','/ownerOccupied','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288070367,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5eb19f7b6fcd4a06b2caa7ff47d57f53','修改','','update',3,NULL,'571fd0d7bb7b4961a0df456f9f5459a4',3,1,0,4,'v1:house:owner:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626141628157,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5ed88e4031b8483ebdb30a183d41f64f','修改','','update',4,NULL,'aaff788d0f3644b9870678460e691d90',3,1,0,4,'v1:user:user:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623225297314,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('61241c6baa96488b844a1ff4d184e574','删除','','delete',3,NULL,'a60d8becabe94226b01aabe8a0823ab1',1,1,0,3,'v1:operation:banner:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031150173,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6272f83e5d9a4f37bc8ace7031f91aab','详情','','info',4,NULL,'6767333ea8e044449535d89488b113c3',2,1,0,2,'v1:user:group:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227910059,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('62ffbf09f4f24b7792082463137eba17','详情','','info',4,NULL,'65fb5ee180774764b2317836d6756abf',2,1,0,2,'v1:order:night:getCount','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1627886137195,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('65fb5ee180774764b2317836d6756abf','夜审操作','','nightAudit',3,NULL,'5bc45f127ef1417ca8a6b10ed06bb341',2,0,0,0,'','/homestayRentOrSale/reserve/nightAudit','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626079884959,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('66a975616e164e0681e5fbfb32a4adc6','详情','','info',3,NULL,'cbacdc2c2df3490b8481b9d5fdbac114',2,1,0,2,'v1:sys:salesman:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626665509316,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6767333ea8e044449535d89488b113c3','团队客户','','customerGroup',3,NULL,'431903c381114040a2e4fb1f21f0c0c4',1,0,0,0,'','/userManage/customerGroup','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227425947,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('67d23793fa154e5d912ab520f4640b45','添加','','add',3,NULL,'cbacdc2c2df3490b8481b9d5fdbac114',0,1,0,1,'v1:sys:salesman:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626665495890,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('684b2b6e78e148ba959f60d153e6664b','会员卡管理','','cardManage',2,NULL,'e6e40771579b45f4aab138b53febdf78',0,0,0,0,'','/card/cardManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638437703545,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640945946459,NULL,NULL,NULL,NULL,NULL,NULL),('68906112da4a42038bad1660ebb8484c','系统设置','group1/M00/00/7B/rBH812HJLzmAKKPxAAAfgg-5jc8812.png','system',1,'1','',6,0,0,0,'——','/system','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622620553726,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640574863553,NULL,NULL,NULL,NULL,NULL,NULL),('689a1f346d3444669aa5806a0d2f3aea','日志管理','','adminLog',2,NULL,'68906112da4a42038bad1660ebb8484c',6,0,0,0,'','/system/adminLog','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623245203,'fe3cc2241d3f40e38a788a77fcd98581','admin',1634001134780,NULL,NULL,NULL,NULL,NULL,NULL),('68f9f44d183c4f358998b492ed1017a8','添加','','add',3,NULL,'0694482838274008a2afcf3b25380927',0,1,0,1,'v1:house:source:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625731694846,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625731844452,NULL,NULL,NULL,NULL,NULL,NULL),('6a131fb98064488782128f6b81b78e0d','详情','','info',3,NULL,'0694482838274008a2afcf3b25380927',2,1,0,2,'v1:house:source:getInfo','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625731710858,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625731858869,NULL,NULL,NULL,NULL,NULL,NULL),('6b1e8118dbbf4dda9a7d030ae5a58c24','删除','','delete',4,NULL,'a392e7f27a74454895d4bb677bdca635',1,1,0,3,'v1:sys:elseClassify:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625811071928,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6d60c42d09b24594ab863b53bea374ee','删除','','delete',4,NULL,'9e0b860c1f6245e4bf1ee248b7159bd4',1,1,0,3,'v1:sys:elseExpense:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625811157711,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6dabd809142f47bb87b2a44d4b39b39f','活动管理','group1/M00/00/7B/rBH812HJLzmACpn4AAATSBMNzfA037.png','activity',1,'1','',1,0,0,0,'','/activity','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638423635361,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640574808400,NULL,NULL,NULL,NULL,NULL,NULL),('6dccf38388c14d68bd5f8167f75335f2','添加','','add',3,NULL,'0b7cc705df084002bd8bfb9a25524e6a',0,1,0,1,'v1:sys:breakfast:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625795378992,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6de1487abb3342f6ab0e81dd6b940b1c','删除','','delete',4,NULL,'81ed5443ac07488b8fd66a4c26d52550',1,1,0,3,'v1:user:feedback:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031308514,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6df7520dbdb84a79a619e2206a66cbf3','详情','','info',3,NULL,'0f1250ecd92742bdb9564a12dc96eb9d',2,1,0,2,'v1:house:policy:getInfo','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1630318742955,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6e256d297c054424942cfa1d50a055af','删除','','delete',4,NULL,'c1508aa5629644e09d91f8fed3249e6d',1,1,0,3,'v1:user:user:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734871005,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6ecdb6afcc7947d6af9dcb02b8f7549d','修改','','update',3,NULL,'3eb43a130f384b7c9525b91c5c4e597b',3,1,0,4,'v1:sys:organizatioon:update','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623057056,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6f88c27b260a45008c63e4dce0bf6a42','详情','','info',3,NULL,'2a93f41742664c4c9df1cf0f2dba9b6c',2,1,0,2,'v1:room:clean:getList','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1633947290992,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('6faadb095bb8415b8864963e19acfb0d','详情','','info',3,NULL,'a60d8becabe94226b01aabe8a0823ab1',2,1,0,2,'v1:operation:banner:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031136051,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('70b022dcfdc645338c750e1a4aa9c3dc','渠道后台','','channelBackstage',2,NULL,'a445a38cf28d4f14bbeb1126eda640a8',1,2,0,0,'','/channelBackstage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624503657090,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('70bab8e4cb0d49b6a1962afbfdc7b8b8','房态管理','','status',3,NULL,'8c651cb8174e4839be547661d543ba7d',2,0,0,0,'','/homestayRentOrSale/house/status','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080426812,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('70d4c7c49cfc4c41adbf6232432efeae','渠道包房','','channelRooms',3,NULL,'53706141879242c5b830a45ab6ffa8c0',1,0,0,0,'','/channelRooms','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624503076976,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('70ee7a8e21f8463dbdb75f5b82adfc4e','详情','','info',3,NULL,'3d478012b51c472d97571647e35aaab6',2,1,0,2,'v1:house:source:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288427444,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('716edc59c4fb4766aeae9134ce3b7de9','添加','','add',3,NULL,'b5542b1619004195b8c8fd7ded0ee7ea',0,1,0,1,'v1:house:status:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624861311242,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('71ac09c06e13449180ce615a2c132554','详情','','info',4,NULL,'dff51fb853e24032aa832784549d095a',2,1,0,2,'v1:house:dailyPrice:list','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623062145601,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('72bce1c91e9045d69838e988bfa5bc7d','添加','','add',3,NULL,'cf2f5fbd7320465aa8f85cea2d7e14ed',0,1,0,1,'v1:activity:activity:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031440963,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('730c8076493746c38cc90842f18936dc','意见反馈','','feedBack',2,NULL,'56a5884d370f4b28b510ba0717443263',2,2,0,0,'','/operation/feedBack','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409387602,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7366d770533847fd8a3d8b629c343ca4','修改','','update',3,NULL,'e8ca082a7e0a47c6a2649a320fdf9ee0',3,1,0,4,'v1:sys:images:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623289051117,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('75565354178b4dcfb61a74662cab9fcb','房态方块','','houseStatusBlock',2,NULL,'a4c1aa15e033431fa5b9583d33761481',1,0,0,0,'','/frontDesk/houseStatusBlock','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626067975295,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626068163336,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1626079228180,NULL,NULL,NULL),('755d1e131969484b800c0e52d6c00c6b','添加','','add',3,NULL,'8221389f1ade47a4847ae8e6243488de',0,1,0,1,'v1:house:policy:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1630318652351,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7659f960873f4f2f8ba022a6b8b96dd4','修改','','update',3,NULL,'080f2e1502054cbbb075e9774e6634fe',3,1,0,4,'v1:sys:organizatioon:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410406532,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('77b401f9cef0445c9ee6510b374566da','首页','','homePage',2,NULL,'a4d73aa0bdc84efc804dccdf994ed5e2',0,0,0,0,'','/homePage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623220212270,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623225416445,NULL,NULL,NULL,NULL,NULL,NULL),('77c67073459b4bd1a84f4aadb3630c19','修改','','update',3,NULL,'57c866a96d6b4edb865a5a2842a0ebde',3,1,0,4,'v1:sys:discount:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1634004551026,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('79f1f4a01c3b429c993fe2669f18134b','详情','','info',3,NULL,'3eb43a130f384b7c9525b91c5c4e597b',2,1,0,2,'v1:sys:organizatioon:list','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623025258,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7b6d22cca6da4244a276a7bb707ed9ec','修改','','update',4,NULL,'c0377b2f3a2e48bdaa5e7ed665a3cb36',3,1,0,4,'v1:house:dailyPrice:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080744912,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7bdb5ff442b64bcfa12847d2fffc3115','添加','','add',3,NULL,'514e68f98eae410580251ba885810309',0,1,0,1,'v1:activityCate:activityCate:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031366856,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7c11a12879804b5b99cc17fecbe248e1','修改','','update',3,NULL,'1ed3a1d1a084450183f030ce7fccc6e7',3,1,0,4,'v1:house:status:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625738367629,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7ca04e2bf95e4e1697a8decac7f7b823','删除','','delete',3,NULL,'f9577a3dbdb544d4b2e269e323e0ff3c',1,1,0,3,'v1:sys:menu:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410240686,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7d0084cfab794f5892bd89646c220a5b','删除','','delete',3,NULL,'57f8feaf4b13417599a163d7a4f3a0a4',1,1,0,3,'v1:rooms:cleaner:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626313396286,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7f6ef9b8b11d42088aa3f7d3820111a2','详情','','info',3,NULL,'8d091467450d4838b78ef2913978b235',2,1,0,2,'v1:sys:menu:list','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623156778,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7f88ae69b83b497cb5aebb4f927ba029','易耗品','','consumable',2,NULL,'0f66ee9fe4ae4ea380f8545a3b74e1d1',4,0,0,0,'','/rooms/consumable','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624428040354,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626413704606,NULL,NULL,NULL,NULL,NULL,NULL),('7fad4ea44277401e9f66ccd835dab6ca','详情','','info',3,NULL,'dc79f41ec1dc442d9237c2d3a75349e8',2,1,0,2,'v1:house:trusteeship:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626850914613,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('7ffebe9f761f4573b638f9a40e5775c3','渠道订单','','channelOrders',2,NULL,'57638abb63c24b9590f9ae3e29bff89a',1,0,0,0,'','/channelOrders','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625020738588,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625732525914,NULL,NULL,NULL),('8043d9e2029a411c81cb2bda4268c9b4','删除','','delete',4,NULL,'aabdd886699e47ff8c79fbf3e3d81527',1,1,0,3,'v1:house:source:delete','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622769947013,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('81ed5443ac07488b8fd66a4c26d52550','内容管理','','feedBackManage',3,NULL,'730c8076493746c38cc90842f18936dc',1,0,0,0,'','/operation/feedBack/feedBackManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409478234,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639034106992,NULL,NULL,NULL,NULL,NULL,NULL),('8221389f1ade47a4847ae8e6243488de','退订政策','','exchangePolicy',2,NULL,'57638abb63c24b9590f9ae3e29bff89a',3,0,0,0,'','/homestayRentOrSale/exchangePolicy','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1630318489190,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8232c9ee8ae74a818e4a6a4be79c9627','删除','','delete',3,NULL,'2281779d4f2a417fa4a986ee938b22b1',1,1,0,3,'v1:sys:account:delete','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622716284508,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('82c4437078744eb39a733200003dddce','房间保洁','','houseCleaner',2,NULL,'0f66ee9fe4ae4ea380f8545a3b74e1d1',2,0,0,0,'','/houseCleaner','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624324160136,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1633758702804,NULL,NULL,NULL),('83302f8812444bfdb393d101cd9fad76','渠道订单','','channelOrder',3,NULL,'5bc45f127ef1417ca8a6b10ed06bb341',1,0,0,0,'','/homestayRentOrSale/reserve/channelOrder','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1633677603916,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8341bafcb84b4d5681887c9ed340e64b','夜审操作','','nightAudit',2,NULL,'57638abb63c24b9590f9ae3e29bff89a',3,0,0,0,'','/nightAudit','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625020830907,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625732538731,NULL,NULL,NULL),('83ebc65e790a49ebbc11f24554b4eeb9','修改','','update',3,NULL,'3e04f37276854caf996f9fddccfa248e',3,1,0,4,'v1:sys:bed:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626256699424,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('84cd6fd0b0cb4fa58ea8786fa0672d94','添加','','add',3,NULL,'080f2e1502054cbbb075e9774e6634fe',0,1,0,1,'v1:sys:organizatioon:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410381916,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('865f617073c6457f851d3902e616345f','系统消息','group1/M00/00/7B/rBH812HJLzmAVwpkAAAPQxBslaY827.png','message',1,'1','',7,0,0,0,'','/message','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638525882438,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640653530097,NULL,NULL,NULL,NULL,NULL,NULL),('88a499db6ef847cabc2bc740438c598b','添加','','add',4,NULL,'c7d3d2f1ecab4deda35ec5f0fa4353b9',0,1,0,1,'v1:house:source:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080596008,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('88ef07042f524743a055ee446ce3fcb1','详情','','info',3,NULL,'57c866a96d6b4edb865a5a2842a0ebde',2,1,0,2,'v1:sys:discount:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1634004498533,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8bc4addaae87475093ef4b6df88044b2','修改','','update',3,NULL,'d49f38e3d7fe49beb03bb93ce5b78746',3,1,0,4,'v1:house:trusteeship:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626844937640,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8c651cb8174e4839be547661d543ba7d','房源管理','','house',2,NULL,'57638abb63c24b9590f9ae3e29bff89a',0,2,0,0,'','/homestayRentOrSale/house','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734412564,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8d091467450d4838b78ef2913978b235','菜单管理','','menu',2,NULL,'68906112da4a42038bad1660ebb8484c',0,0,0,0,'','/system/menu','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623108270,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625647643765,NULL,NULL,NULL,NULL,NULL,NULL),('8dacd92a2b1c4a8ab45803ff2cf12061','删除','','delete',4,NULL,'ea380ddc41d147008176dce03b5d34a5',1,1,0,3,'v1:user:group:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625735449642,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8ffed7ca28e146eba9d89ca93ff1218e','系统管理员','','account',2,NULL,'bd770fef9c304b46be16954aacd4359b',2,0,0,0,'','/system/account','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410015523,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('90165cab8fff4ae494c3e11bef07d988','详情','','info',3,NULL,'d80e2098db174faf947127cb2ad09545',2,1,0,2,'v1:sys:habitable:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626255542218,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('91ba9edfcad74d2f939a07c5430709dd','添加','','add',4,NULL,'0ba2ee73b3564a46afb17105ca91d837',0,1,0,1,'v1:user:blacklist:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227949259,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('924218c0d78f4ea8b98ca2d5a527b16c','详情','','info',3,NULL,'cf2f5fbd7320465aa8f85cea2d7e14ed',2,1,0,2,'v1:activity:activity:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031456549,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9284fa0abb0c476aa4dadec023dcbe6f','删除','','delete',3,NULL,'571fd0d7bb7b4961a0df456f9f5459a4',1,1,0,3,'v1:house:owner:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626141647506,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('93ee32e1cb0041aaa8d5ed1ed38c1089','节假日管理','','holiday',2,NULL,'68906112da4a42038bad1660ebb8484c',10,0,0,0,'','/system/holiday','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623377431590,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626671559478,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412007912,NULL,NULL,NULL),('93f5db9524c446a78211d132a2a4243e','线下渠道','group1/M00/00/73/rBH812Gl0tKAR6rsAAAT773djyk677.jpg','channel',1,'1','',5,0,0,0,'','/channel','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626831031115,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638257484572,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638411902781,NULL,NULL,NULL),('94ab6001aa174e6bb53f0bde3d6b372c','删除','','delete',3,NULL,'3d478012b51c472d97571647e35aaab6',1,1,0,3,'v1:house:source:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288441788,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('95e6788bc7804d599049ea585030cad3','详情','','info',3,NULL,'2281779d4f2a417fa4a986ee938b22b1',2,1,0,2,'v1:sys:account:list','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622716272874,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('96a9a9418a904b6389d16d4face46a8c','删除','','delete',3,NULL,'080f2e1502054cbbb075e9774e6634fe',1,1,0,3,'v1:sys:organizatioon:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410389577,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('982c12acada14b588e33f74177512669','推荐用户','','recommendUser',2,NULL,'56a5884d370f4b28b510ba0717443263',1,0,0,0,'','/operation/recommendUser','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409269223,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638526273266,NULL,NULL,NULL),('98977930c9bc4226ac8637f021a5d245','修改','','update',4,NULL,'65fb5ee180774764b2317836d6756abf',3,1,0,4,'v1:order:night:auditor','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1628574771634,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('990f042f1a824405b1ec7849f626e3ff','删除','','delete',3,NULL,'0b7cc705df084002bd8bfb9a25524e6a',1,1,0,3,'v1:sys:breakfast:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625795419896,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('99dae66b98d14cc7813a33480282e9f1','删除','','delete',3,NULL,'514e68f98eae410580251ba885810309',1,1,0,3,'v1:activityCate:activityCate:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031407536,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('99e1fe610ffd4faab7a2978ac6ee2474','详情','','info',4,NULL,'0ba2ee73b3564a46afb17105ca91d837',2,1,0,2,'v1:user:blacklist:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227967210,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623228787173,NULL,NULL,NULL,NULL,NULL,NULL),('9a46981fa3674cd790ac8327aa82bba6','添加','','add',3,NULL,'56bf141c668e49668e7b3e99a0fa8ef5',0,1,0,1,'v1:sys:channel:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626831104752,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9bd036d0888e449b9042453a9841f628','修改','','update',4,NULL,'aabdd886699e47ff8c79fbf3e3d81527',3,1,0,4,'v1:house:source:update','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622770000462,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9c2bb0b2b0ca4c548e544b65bc98f19b','添加','','add',4,NULL,'6767333ea8e044449535d89488b113c3',0,1,0,1,'v1:user:group:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227893402,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9d446fd30f1249df8aee42c3bd6735c5','详情','','info',3,NULL,'7f88ae69b83b497cb5aebb4f927ba029',2,1,0,2,'v1:rooms:consumable:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626413840408,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626413891598,NULL,NULL,NULL,NULL,NULL,NULL),('9da7daf3976744199bee3b0fad1f45c9','详情','','info',4,NULL,'28f44231b0614e20824d0a524b215130',2,1,0,2,'v1:user:feedbackcate:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031232614,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9de96dcd1251461bb122e6640c97addb','业主自住','','ownerOccupied',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',3,0,0,0,'','/houseCollocation/ownerOccupied','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1632796196047,'fe3cc2241d3f40e38a788a77fcd98581','admin',1632880566346,NULL,NULL,NULL,NULL,NULL,NULL),('9e0b860c1f6245e4bf1ee248b7159bd4','其他消费管理','','elseExpense',3,NULL,'e9097901ea434f9c871c21e852b15f93',1,0,0,0,'','/system/elseExpenses/elseExpense','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625810982162,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9fbb1fcf032a4acabb6fb93b965c4b7a','修改','','update',3,NULL,'684b2b6e78e148ba959f60d153e6664b',3,1,0,4,'v1:card:memberCard:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031649894,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a0431ec41107466e98354e72f007d65c','钱包流水','','walletPurse',3,NULL,'3c7ef9101bea4bb1bccf87f53172579c',1,0,0,0,'','/walletPurse','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288051284,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a0a9e5051d1745579d9f10ff65174b03','详情','','info',4,NULL,'c1508aa5629644e09d91f8fed3249e6d',2,1,0,2,'v1:user:user:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734859129,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625738072738,NULL,NULL,NULL,NULL,NULL,NULL),('a12896ba791a409f9f25a27299326ecd','角色管理','','authority',2,NULL,'bd770fef9c304b46be16954aacd4359b',1,0,0,0,'','/system/authority','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409989821,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a12d876159444f1f8046e8cfc05b4322','修改','','update',3,NULL,'d95cd781d7d449ef99ff5503ae7d1e9b',3,1,0,4,'v1:sys:steward:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626258100434,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a29c168f565c472c9d64b746b87264f8','详情','','info',4,NULL,'70bab8e4cb0d49b6a1962afbfdc7b8b8',2,1,0,2,'v1:house:status:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080793572,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a2da2b9bd764420c9099e9d0a4dcbe4c','修改','','update',3,NULL,'b5542b1619004195b8c8fd7ded0ee7ea',3,1,0,4,'v1:house:status:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624861402687,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a312d9fe6fa345fca786a089e941e529','删除','','delete',3,NULL,'034d9a198d874b5baff04f1b10460799',1,1,0,3,'v1:sys:procedure:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1635304168777,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a392e7f27a74454895d4bb677bdca635','其他消费分类','','elseClassify',3,NULL,'e9097901ea434f9c871c21e852b15f93',0,0,0,0,'','/system/elseExpenses/elseClassify','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625810935048,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a3bfacc06aa84b39a37c8e962dbd6f22','删除','','delete',3,NULL,'d80e2098db174faf947127cb2ad09545',1,1,0,3,'v1:sys:habitable:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626255554662,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a445a38cf28d4f14bbeb1126eda640a8','渠道','group1/M00/00/64/rBH812DVN1aAHE7rAAAM_apgC2A196.png','channel',1,'1','',4,0,1,0,'','/channel','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624503008261,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625651424280,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1626830988697,NULL,NULL,NULL),('a4c1aa15e033431fa5b9583d33761481','营业前台','group1/M00/00/73/rBH812Gl0tKAILJIAAAYFzBUQfA502.jpg','frontDesk',1,'1','',6,0,0,0,'','/frontDesk','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624666945492,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638257501574,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638411897500,NULL,NULL,NULL),('a4cbeaedea7649269ac02563b988fe7a','修改','','update',3,NULL,'0694482838274008a2afcf3b25380927',3,1,0,4,'v1:house:source:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625731943645,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a4d73aa0bdc84efc804dccdf994ed5e2','首页','group1/M00/00/73/rBH812Gl0tOAHKBdAAAUGT-SZ6U795.jpg','home',1,'1','',0,0,1,0,'','/home','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623220110978,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638257398076,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638411870372,NULL,NULL,NULL),('a50bc63a472c41a7a94e92ef2c27fd86','详情','','info',4,NULL,'c7d3d2f1ecab4deda35ec5f0fa4353b9',2,1,0,2,'v1:house:source:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080621766,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a60d8becabe94226b01aabe8a0823ab1','banner图管理','','banner',2,NULL,'56a5884d370f4b28b510ba0717443263',0,0,0,0,'','/operation/banner','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409201064,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409279230,NULL,NULL,NULL,NULL,NULL,NULL),('a61b974503eb42c39ecc2cfe71a0a37b','报名管理','','activityApply',2,NULL,'6dabd809142f47bb87b2a44d4b39b39f',2,0,0,0,'','/activity/activityApply','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638423775875,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638432528020,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638439192196,NULL,NULL,NULL),('a6973e34a66040e69db3ba600cb7e0c6','修改','','update',3,NULL,'f9577a3dbdb544d4b2e269e323e0ff3c',3,1,0,4,'v1:sys:menu:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410226262,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a81744eae5f14a48aeb2fa368edc0d11','添加','','add',3,NULL,'a12896ba791a409f9f25a27299326ecd',0,1,0,1,'v1:sys:role:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410259567,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a8cc6323cea04327a7a3a4186ff2ffec','删除','','delete',3,NULL,'3e04f37276854caf996f9fddccfa248e',1,1,0,3,'v1:sys:bed:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626256682933,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a90afd4f115a4c78a2e5cda48a633007','添加','','add',3,NULL,'d95cd781d7d449ef99ff5503ae7d1e9b',0,1,0,1,'v1:sys:steward:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626258058144,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('aabdd886699e47ff8c79fbf3e3d81527','房源管理','','houseManage',3,NULL,'0abfc4ab4dda487692d3d57c1f3328f3',0,0,0,0,'','/houseManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622769841017,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('aaff788d0f3644b9870678460e691d90','个人客户','','individualcClient',3,NULL,'431903c381114040a2e4fb1f21f0c0c4',0,0,0,0,'','/userManage/individualcClient','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623225172648,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623226352364,NULL,NULL,NULL,NULL,NULL,NULL),('abf248dad56d40839c73f8644fd4a61f','订单管理','','activityOrder',2,NULL,'6dabd809142f47bb87b2a44d4b39b39f',3,0,0,0,'','/activity/activityOrder','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638423802032,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640946301396,NULL,NULL,NULL,NULL,NULL,NULL),('aca00d40e77d404885db90da64b36074','删除','','delete',3,NULL,'dc79f41ec1dc442d9237c2d3a75349e8',1,1,0,3,'v1:house:trusteeship:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626850927042,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('acbf59c3dd6b421486adde17e153ed63','详情','','info',3,NULL,'15cf3bb15d3d41c099ddba422eb91215',2,1,0,2,'v1:user:user:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639030881176,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('aef60e304430487d972bd7a7eb7a0089','删除','','delete',3,NULL,'de1a4a1ee28e4cfd8b2e1740c115916d',1,1,0,3,'v1:room:clean:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626337341029,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626412822651,NULL,NULL,NULL,NULL,NULL,NULL),('af08e5a43d5c435fa0dfc501ebb8aac8','分类管理','','reportCategory',3,NULL,'532a8dfdc1a84b019a82e6264cf25221',0,0,0,0,'','/user/report/reportCategory','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638423295663,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639034132503,NULL,NULL,NULL,NULL,NULL,NULL),('afd66627e81049bd9d541db6e51acb93','其他消费','','elseExpense',2,NULL,'68906112da4a42038bad1660ebb8484c',8,0,0,0,'','/system/elseExpense','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625797854231,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625810804941,NULL,NULL,NULL),('b01a859208d946eca619563d8b554ca2','添加','','add',3,NULL,'de1a4a1ee28e4cfd8b2e1740c115916d',0,1,0,1,'v1:room:clean:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626337310286,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b04457e819454dd6a4ac65bc2418bb3a','详情','','info',4,NULL,'c0377b2f3a2e48bdaa5e7ed665a3cb36',2,1,0,2,'v1:house:dailyPrice:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080700530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b05d2ab4f38444d0a3273c13bfa5095f','修改','','update',3,NULL,'4c8c451d4bc14fa98c8cd39f01742dbc',3,1,0,4,'v1:house:dailyPrice:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288566493,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b06caa28aee641a38c7529930eafcaef','房型管理','','genre',3,NULL,'0d95e2c84945437b8fd53839e72b4407',0,0,0,0,'','/homestayRentOrSale/house/genre','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625730888621,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625731497081,NULL,NULL,NULL),('b0701f51a0b847c28cea3c794d9b09a0','添加','','add',3,NULL,'571fd0d7bb7b4961a0df456f9f5459a4',0,1,0,1,'v1:house:owner:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626141605094,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b305213183d4454d8ef0d777dfc6325d','添加','','add',3,NULL,'54379385730e459a867b4c18911da87f',0,1,0,1,'v1:sys:breakfast:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625797922085,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b397e51d38074ec0b3c34f54f298c511','业主管理','','ownerManage',3,NULL,'3c7ef9101bea4bb1bccf87f53172579c',0,0,0,0,'','/ownerManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288015698,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b5542b1619004195b8c8fd7ded0ee7ea','房态管理','','roomStateManage',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',2,0,0,0,'','/roomStateManage','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623805254813,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625730754334,NULL,NULL,NULL),('b554be9d62244cbbb97201a787aca996','修改','','update',3,NULL,'cf2f5fbd7320465aa8f85cea2d7e14ed',3,1,0,4,'v1:activity:activity:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031505406,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b6121a242c1e4e7da01e5e18443318a8','详情','','info',4,NULL,'aaff788d0f3644b9870678460e691d90',2,1,0,2,'v1:user:user:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623225272246,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b63ab7cc14cf4d0ba4c5359ad3df1077','添加','','add',4,NULL,'9e0b860c1f6245e4bf1ee248b7159bd4',0,1,0,1,'v1:sys:elseExpense:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625811112098,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b731520663d84373949993408471f42e','修改','','update',3,NULL,'033b4d93d3364fa7ad7a5f2023673c6b',3,1,0,4,'v1:vide:whiteheadabout:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031575159,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b78b165f303c482182f4fd44e185ff50','删除','','delete',4,NULL,'0ba2ee73b3564a46afb17105ca91d837',1,1,0,3,'v1:user:blacklist:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227976919,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b973f15a93ae4c1583b2cbdd18bdb09a','内容管理','group1/M00/00/7B/rBH812HJLzmALXgnAAASIwZj68s635.png','content',1,'1','',5,0,0,0,'','/content','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288799226,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640574849323,NULL,NULL,NULL,NULL,NULL,NULL),('bad841be9c794a1d80d64e56076cd906','详情','','info',3,NULL,'e8ca082a7e0a47c6a2649a320fdf9ee0',2,1,0,2,'v1:sys:images:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623289003251,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('bb98b74fbe944ca691a27c0a1e175261','详情','','info',3,NULL,'034d9a198d874b5baff04f1b10460799',2,1,0,2,'v1:sys:procedure:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1628141868832,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('bc0f1cc3ed7a417681a9f4efcf0f75a2','详情','','info',3,NULL,'c8e9cabd29634e09bf7a45eb02600157',2,1,0,2,'v1:log:logAccountLogin:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410630124,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('bd770fef9c304b46be16954aacd4359b','系统设置','group1/M00/00/6D/rBH812ETdCyAUwmLAAAfgg-5jc8545.png','system',1,'1','',4,0,0,0,'','/system','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409583765,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638411922295,NULL,NULL,NULL),('bdb37852e6444f48ad1add8c3a1e92e4','修改','','update',3,NULL,'3d478012b51c472d97571647e35aaab6',3,1,0,4,'v1:house:source:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288452321,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('beeb04d7be2147738ccab47fd1934bf3','详情','','info',4,NULL,'9e0b860c1f6245e4bf1ee248b7159bd4',2,1,0,2,'v1:sys:elseExpense:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625811131916,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('bf001ec9af15440bbf3fb3e67cebb1d3','添加','','add',3,NULL,'1ed3a1d1a084450183f030ce7fccc6e7',0,1,0,1,'v1:house:status:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625738381162,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('bf406095ff2841598826912a6064cc28','添加','','add',3,NULL,'0657931e4c50460eb913b73cf2f2fe24',0,1,0,1,'v1:sys:role:save','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623446614,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('bf82c05e336d4663acf5b0340929ef93','删除','','delete',3,NULL,'3eb43a130f384b7c9525b91c5c4e597b',1,1,0,3,'v1:sys:organizatioon:delete','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623040028,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c003994646a54ef39d96c963d6a7d770','修改','','update',3,NULL,'034d9a198d874b5baff04f1b10460799',3,1,0,4,'v1:sys:procedure:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1628141830299,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c0377b2f3a2e48bdaa5e7ed665a3cb36','房价管理','','price',3,NULL,'8c651cb8174e4839be547661d543ba7d',1,0,0,0,'','/homestayRentOrSale/house/price','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080399617,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c1508aa5629644e09d91f8fed3249e6d','个人用户','','singleUser',3,NULL,'0e68e9f3ff0747449abed5f02f79651d',0,0,0,0,'','/homestayRentOrSale/user/singleUser','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734552067,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c234e295e5e14b48a16391b6a058ee15','添加','','add',4,NULL,'a392e7f27a74454895d4bb677bdca635',0,1,0,1,'v1:sys:elseClassify:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625811038734,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c262029e393549969161187efdee5647','详情','','info',3,NULL,'de1a4a1ee28e4cfd8b2e1740c115916d',2,1,0,2,'v1:room:clean:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626337329861,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c4b65c6ebf1b4b8fa8f474cd3dd3541c','详情','','info',3,NULL,'57f8feaf4b13417599a163d7a4f3a0a4',2,1,0,2,'v1:rooms:cleaner:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626313371285,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c7d3d2f1ecab4deda35ec5f0fa4353b9','房型管理','','genre',3,NULL,'8c651cb8174e4839be547661d543ba7d',0,0,0,0,'','/homestayRentOrSale/house/genre','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080334374,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c8e9cabd29634e09bf7a45eb02600157','日志管理','','adminLog',2,NULL,'bd770fef9c304b46be16954aacd4359b',3,0,0,0,'','/system/adminLog','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410045425,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('cb28f213e495438dab2e750843aba6be','删除','','delete',3,NULL,'8d091467450d4838b78ef2913978b235',1,1,0,3,'v1:sys:menu:delete','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623182011,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('cb3c194996354b09b901fa15c11109ac','修改','','update',3,NULL,'54379385730e459a867b4c18911da87f',3,1,0,4,'v1:sys:breakfast:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625798097584,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('cb41234a0a8b4fe08360e85eeb149bc8','自营订单','','monopoly',3,NULL,'5bc45f127ef1417ca8a6b10ed06bb341',0,0,0,0,'','/homestayRentOrSale/reserve/monopoly','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625735696115,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('cbacdc2c2df3490b8481b9d5fdbac114','销售管理','','salesman',2,NULL,'68906112da4a42038bad1660ebb8484c',9,0,0,0,'','/system/salesman','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626665465325,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626671550949,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412013078,NULL,NULL,NULL),('cca70c2320c24337a802e5a71900b4b9','修改','','update',3,NULL,'2281779d4f2a417fa4a986ee938b22b1',3,1,0,4,'v1:sys:account:update','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622716294674,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ce33c60364a3482f9688c76178f8200f','修改','','update',3,NULL,'cbacdc2c2df3490b8481b9d5fdbac114',3,1,0,4,'v1:sys:salesman:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626665540493,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ce5b03689da74ad7ad7c74609995b56e','修改','','update',3,NULL,'a12896ba791a409f9f25a27299326ecd',3,1,0,4,'v1:sys:role:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410293232,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('cee60507074345c6a806affaee2711e9','修改','','update',3,NULL,'8d091467450d4838b78ef2913978b235',3,1,0,4,'v1:sys:menu:update','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623198356,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('cf2f5fbd7320465aa8f85cea2d7e14ed','活动管理','','activityManage',2,NULL,'6dabd809142f47bb87b2a44d4b39b39f',1,0,0,0,'','/activity/activityManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638423731418,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638423738112,NULL,NULL,NULL,NULL,NULL,NULL),('d174f83e071a499aacb2d401ee5d66dc','修改','','update',3,NULL,'93ee32e1cb0041aaa8d5ed1ed38c1089',3,1,0,4,'v1:sys:holiday:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623377487739,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('d195ef1649a944edb2dd95463752d744','详情','','info',3,NULL,'f9577a3dbdb544d4b2e269e323e0ff3c',2,1,0,2,'v1:sys:menu:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410210388,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('d2ce4141b7ff4ffba3cc0bc435a3733f','修改','','update',3,NULL,'514e68f98eae410580251ba885810309',3,1,0,4,'v1:activityCate:activityCate:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031424363,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('d31ed8830e884874ab6d587bb2277e32','详情','','info',3,NULL,'8ffed7ca28e146eba9d89ca93ff1218e',2,1,0,2,'v1:sys:account:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638410316703,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('d49f38e3d7fe49beb03bb93ce5b78746','托管时长','','trusteeship',2,NULL,'e0c209b1aa70457482b7af7a8fa1ac7c',1,0,0,0,'','/houseCollocation/trusteeship','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626844866167,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1626850950486,NULL,NULL,NULL),('d5a8b9db11cf4d7aa3b78a351630d3b9','修改','','update',3,NULL,'56bf141c668e49668e7b3e99a0fa8ef5',3,1,0,4,'v1:sys:channel:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626831146435,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('d7ca61dabca4468985d137f34c3fe32f','添加','','add',4,NULL,'af08e5a43d5c435fa0dfc501ebb8aac8',0,1,0,1,'v1:user:reportcate:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639030928853,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639030996199,NULL,NULL,NULL,NULL,NULL,NULL),('d7cd3f9d3bfe49cf90c128efe3b2d5c3','添加','','add',4,NULL,'c1508aa5629644e09d91f8fed3249e6d',0,1,0,1,'v1:user:user:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734807450,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('d80e2098db174faf947127cb2ad09545','户型管理','','habitable',2,NULL,'68906112da4a42038bad1660ebb8484c',6,0,0,0,'','/system/habitable','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626255458485,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412040417,NULL,NULL,NULL),('d8dd72a9cffc4ee4a64094638d26aee1','添加','','add',3,NULL,'dc79f41ec1dc442d9237c2d3a75349e8',0,1,0,1,'v1:house:trusteeship:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626850900729,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('d95cd781d7d449ef99ff5503ae7d1e9b','管家管理','','steward',2,NULL,'68906112da4a42038bad1660ebb8484c',8,0,0,0,'','/system/steward','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626258003965,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626311227423,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412018373,NULL,NULL,NULL),('dacbc5e6536d406d958433ed85770974','添加','','add',3,NULL,'684b2b6e78e148ba959f60d153e6664b',0,1,0,1,'v1:card:memberCard:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031622384,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('dad7ae49b0a6494187842c6f1dd14875','删除','','delete',4,NULL,'af08e5a43d5c435fa0dfc501ebb8aac8',1,1,0,3,'v1:user:reportcate:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031014288,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('dbd88ca037344af397bff479bfcd57b1','删除','','delete',3,NULL,'e8ca082a7e0a47c6a2649a320fdf9ee0',1,1,0,3,'v1:sys:images:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623289036718,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('dc498737611146f6b56f4db5ba33549d','详情','','info',4,NULL,'ea380ddc41d147008176dce03b5d34a5',2,1,0,2,'v1:user:group:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625735389854,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625738139891,NULL,NULL,NULL,NULL,NULL,NULL),('dc698fde21ae454c84999946c71cb150','添加','','add',3,NULL,'a60d8becabe94226b01aabe8a0823ab1',0,1,0,1,'v1:operation:banner:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031120159,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('dc7217cbff33439dbcbeb3bee4a3b4b3','添加','','add',3,NULL,'57f8feaf4b13417599a163d7a4f3a0a4',0,1,0,1,'v1:rooms:cleaner:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626313357885,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('dc79f41ec1dc442d9237c2d3a75349e8','托管时长','','trusteeship',2,NULL,'68906112da4a42038bad1660ebb8484c',7,0,0,0,'','/system/trusteeship','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626850872664,'fe3cc2241d3f40e38a788a77fcd98581','admin',1634001144726,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638412029070,NULL,NULL,NULL),('de1a4a1ee28e4cfd8b2e1740c115916d','保洁日程','','cleanerSchedule',2,NULL,'0f66ee9fe4ae4ea380f8545a3b74e1d1',1,0,0,0,'','/rooms/cleanerSchedule','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623914639065,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626337292158,NULL,NULL,NULL,NULL,NULL,NULL),('de47e2dccdd94858a2a5986536d9d086','详情','','info',3,NULL,'54379385730e459a867b4c18911da87f',2,1,0,2,'v1:sys:breakfast:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625798147295,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625799446647,NULL,NULL,NULL,NULL,NULL,NULL),('de85eb3426334d088d068e0e08f15521','添加','','add',3,NULL,'2281779d4f2a417fa4a986ee938b22b1',0,1,0,1,'v1:sys:account:save','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622716256149,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('dff51fb853e24032aa832784549d095a','房价管理','','propertyManage',3,NULL,'0abfc4ab4dda487692d3d57c1f3328f3',1,0,0,0,'','/propertyManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623062037534,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e039905820ab4fb1999088a41c777f76','修改','','update',4,NULL,'af08e5a43d5c435fa0dfc501ebb8aac8',3,1,0,4,'v1:user:reportcate:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031035487,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e0c209b1aa70457482b7af7a8fa1ac7c','房源托管','group1/M00/00/73/rBH812Gl0tOABpf5AAATOHVbAYs726.jpg','houseCollocation',1,'1','',1,0,0,0,'','/houseCollocation','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1622769779121,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638257424449,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638411874802,NULL,NULL,NULL),('e0e38b217a7f435e9ceebb61dc8e31d5','添加','','add',3,NULL,'93ee32e1cb0041aaa8d5ed1ed38c1089',0,1,0,1,'v1:sys:holiday:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623377463509,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e16dd93c3b6f4d0bad4959fe9ebcad94','详情','','info',4,NULL,'af08e5a43d5c435fa0dfc501ebb8aac8',2,1,0,2,'v1:user:reportcate:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639030960408,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e2d4c5993ca24bf5ba08a610f2f03847','删除','','delete',4,NULL,'2357ad803ee34fc7ad162bb4292ad837',1,1,0,3,'v1:user:blacklist:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625735338203,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e466151a5345485f847fc77e9b00cf29','审核管理','','userAudit',2,NULL,'51b4c86fe59747c6829d9d856a4d142a',1,0,0,0,'','/user/userAudit','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639030834251,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e6bd8f81f45549ba880030a17386dac1','修改','','update',4,NULL,'c7d3d2f1ecab4deda35ec5f0fa4353b9',3,1,0,4,'v1:house:source:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626080666630,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e6e40771579b45f4aab138b53febdf78','会员卡管理','group1/M00/00/7B/rBH812HJLzmAbjDmAAANMSSd1W4389.png','card',1,'1','',4,0,0,0,'','/card','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638437662334,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640945934000,NULL,NULL,NULL,NULL,NULL,NULL),('e7c4f78ffdba40b0b74f85c3c08edad7','订单','','channelOrders',3,NULL,'70b022dcfdc645338c750e1a4aa9c3dc',1,0,0,0,'','/channelOrders','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624503689405,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625020912774,NULL,NULL,NULL,NULL,NULL,NULL),('e84b0b1a2b34410d9702652a695fbdda','详情','','info',3,NULL,'1ed3a1d1a084450183f030ce7fccc6e7',2,1,0,2,'v1:house:status:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625738329590,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e8ca082a7e0a47c6a2649a320fdf9ee0','素材管理','','material',2,NULL,'b973f15a93ae4c1583b2cbdd18bdb09a',0,0,0,0,'','/content/material','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288881214,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625728716365,NULL,NULL,NULL,NULL,NULL,NULL),('e8d9507fb59a48ceaa0dda75b8f2b5ca','修改','','update',4,NULL,'81ed5443ac07488b8fd66a4c26d52550',3,1,0,4,'v1:user:feedback:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031323764,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e9097901ea434f9c871c21e852b15f93','其他消费','','elseExpenses',2,NULL,'68906112da4a42038bad1660ebb8484c',12,2,0,0,'','/system/elseExpenses','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625810868896,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626665482229,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1638411995032,NULL,NULL,NULL),('ea380ddc41d147008176dce03b5d34a5','团队客户','','customerGroup',3,NULL,'0e68e9f3ff0747449abed5f02f79651d',1,0,0,0,'','/homestayRentOrSale/user/customerGroup','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734612219,'fe3cc2241d3f40e38a788a77fcd98581','admin',1627263534113,NULL,NULL,NULL,NULL,NULL,NULL),('eafe28b01b6f4d67b95f64dd89e5d363','系统消息管理','','systemMessage',2,NULL,'865f617073c6457f851d3902e616345f',0,0,0,0,'','/message/systemMessage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638525925316,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638940134040,NULL,NULL,NULL,NULL,NULL,NULL),('ebd575f943fb4d3eaf4977421355cf72','添加','','add',3,NULL,'4c8c451d4bc14fa98c8cd39f01742dbc',0,1,0,1,'v1:house:records:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623288527220,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ed7408dd87ac4054956e7dbc54a9b47d','删除','','delete',3,NULL,'0657931e4c50460eb913b73cf2f2fe24',1,1,0,3,'v1:sys:role:delete','/','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin1',1622623545602,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f056ab9e12fd4b5aa2d5e8585658382b','详情','','info',3,NULL,'592e47ef73714c4c93ec487ec4fe8c00',2,1,0,2,'v1:house:earnings:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626141726864,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f07f6807086e43429848ac9a2b1890d4','消息通知日志','','messageLog',2,NULL,'865f617073c6457f851d3902e616345f',1,0,0,0,'','/message/messageLog','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638940180150,'fe3cc2241d3f40e38a788a77fcd98581','admin',1640946309024,NULL,NULL,NULL,NULL,NULL,NULL),('f11fcdb790d24ca483c210f24658b855','详情','','info',3,NULL,'571fd0d7bb7b4961a0df456f9f5459a4',2,1,0,2,'v1:house:owner:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626141543298,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626327586396,NULL,NULL,NULL,NULL,NULL,NULL),('f1bd9dd5aaa1442db3fa66ad472896b3','修改','','update',3,NULL,'684b2b6e78e148ba959f60d153e6664b',3,1,0,4,'v1:card:memberCard:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031684500,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f21beb907f684e39b190f3034e07dba5','删除','','delete',3,NULL,'b5542b1619004195b8c8fd7ded0ee7ea',1,1,0,3,'v1:house:status:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1624861384780,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f39a41b01e3e4d9c81b091fa6bbb9920','删除','','delete',3,NULL,'cf2f5fbd7320465aa8f85cea2d7e14ed',1,1,0,3,'v1:activity:activity:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639031479782,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f4c4854fdca347979a21a717015f2164','修改','','update',3,NULL,'de1a4a1ee28e4cfd8b2e1740c115916d',3,1,0,4,'v1:room:clean:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626337354464,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f4df4c1687ee44b098195f3957114d22','添加','','add',4,NULL,'ea380ddc41d147008176dce03b5d34a5',0,1,0,1,'v1:user:group:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625735373508,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f50dc58a741b438587ea699a053f71a8','修改','','update',4,NULL,'a392e7f27a74454895d4bb677bdca635',3,1,0,4,'v1:sys:elseClassify:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625811089131,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f57de605937046dba426695dd2fd3feb','内容管理','','reportManage',3,NULL,'532a8dfdc1a84b019a82e6264cf25221',1,0,0,0,'','/user/report/reportManage','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638423334420,'fe3cc2241d3f40e38a788a77fcd98581','admin',1639034141219,NULL,NULL,NULL,NULL,NULL,NULL),('f60615072855489d926efa42b97f000e','删除','','delete',4,NULL,'6767333ea8e044449535d89488b113c3',1,1,0,3,'v1:user:group:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623227921153,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f82a3816bce14d64a1b9d2a19d239d40','详情','','info',4,NULL,'65fb5ee180774764b2317836d6756abf',2,1,0,2,'v1:order:night:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1627886123762,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f84397e545714f83aeb033f5cd2f17ac','删除','','delete',3,NULL,'93ee32e1cb0041aaa8d5ed1ed38c1089',1,1,0,3,'v1:sys:holiday:delete','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1623377499995,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f9577a3dbdb544d4b2e269e323e0ff3c','菜单管理','','menu',2,NULL,'bd770fef9c304b46be16954aacd4359b',0,0,0,0,'','/system/menu','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1638409965782,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('fa776f439cdd42628c286169127255b2','订单评价','','orderEvaluate',2,NULL,'57638abb63c24b9590f9ae3e29bff89a',2,0,0,0,'','/orderEvaluate','',1,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625020781214,NULL,NULL,NULL,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1625732530442,NULL,NULL,NULL),('fb45981d8c1e44b49c6fe69d3cf13ea1','修改','','update',4,NULL,'c1508aa5629644e09d91f8fed3249e6d',3,1,0,4,'v1:user:user:update','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625734884512,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('fbffd14849a24d47adfcd6578b194ebb','详情','','info',4,NULL,'a392e7f27a74454895d4bb677bdca635',2,1,0,2,'v1:sys:elseClassify:list','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1625811056107,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('fc6c8b49a129403d8b16f063208cb95e','添加','','add',3,NULL,'d80e2098db174faf947127cb2ad09545',0,1,0,1,'v1:sys:habitable:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626255510438,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('fe354e39b65a4b5480ad34afee3ab622','添加','','add',3,NULL,'7f88ae69b83b497cb5aebb4f927ba029',0,1,0,1,'v1:rooms:consumable:save','','',0,'fe3cc2241d3f40e38a788a77fcd98581','admin',1626413802680,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_sys_organization` */

DROP TABLE IF EXISTS `t_sys_organization`;

CREATE TABLE `t_sys_organization` (
  `ID` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `OrganizationName` varchar(20) DEFAULT NULL COMMENT '组织名称',
  `DeptLevel` int NOT NULL COMMENT '组织层级 0:总公司(内置) 1:总公司的部门或下属公司  依次类推...',
  `DeptType` int NOT NULL COMMENT '组织区分 0:总公司(内置) 1:总公司的部门  2:总公司下属的公司 3:下属公司的部门',
  `ParentID` char(32) DEFAULT NULL COMMENT '父标识',
  `Memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `IsDelete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `CreateOperatorID` char(32) DEFAULT NULL COMMENT '创建者ID',
  `CreateOperatorName` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `CreateTime` bigint DEFAULT NULL COMMENT '创建时间',
  `UpdateOperatorID` char(32) DEFAULT NULL COMMENT '更新者ID',
  `UpdateOperatorName` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `UpdateTime` bigint DEFAULT NULL COMMENT '更新时间',
  `DeleteOperatorID` char(32) DEFAULT NULL COMMENT '删除者ID',
  `DeleteOperatorName` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `DeleteTime` bigint DEFAULT NULL COMMENT '删除时间',
  `ExtraParams1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  `ExtraParams2` varchar(100) DEFAULT NULL COMMENT '额外参数2',
  `ExtraParams3` varchar(100) DEFAULT NULL COMMENT '额外参数3',
  `ExtraParams4` varchar(100) DEFAULT NULL COMMENT '额外参数4',
  `ExtraParams5` varchar(100) DEFAULT NULL COMMENT '额外参数5',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统管理-组织架构表';

/*Data for the table `t_sys_organization` */

insert  into `t_sys_organization`(`ID`,`OrganizationName`,`DeptLevel`,`DeptType`,`ParentID`,`Memo`,`IsDelete`,`CreateOperatorID`,`CreateOperatorName`,`CreateTime`,`UpdateOperatorID`,`UpdateOperatorName`,`UpdateTime`,`DeleteOperatorID`,`DeleteOperatorName`,`DeleteTime`,`ExtraParams1`,`ExtraParams2`,`ExtraParams3`,`ExtraParams4`,`ExtraParams5`) values ('1','公司',0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('76555ede47e5490fb1d7639f98c357ba','其他部门',1,1,'1','',0,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1622705283179,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a5a764f8e0b64170a32180a7c84e9c7c','前台',1,1,'1','',0,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1622705224752,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b97b3fd55e1347ea86a3cb84fa9c267f','运营部',1,1,'1','',0,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1622705211013,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('fce2caf438254835b55da8908632d486','技术部门',1,1,'1','备注',0,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1622531975686,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1622705161852,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_sys_role` */

DROP TABLE IF EXISTS `t_sys_role`;

CREATE TABLE `t_sys_role` (
  `ID` char(32) NOT NULL DEFAULT '' COMMENT '编号',
  `RoleName` varchar(20) DEFAULT NULL COMMENT '角色名称',
  `IsUpdate` int DEFAULT '0' COMMENT '是否可以编辑 0-是   1-否',
  `IsStatus` int DEFAULT '0' COMMENT '账号状态 0:正常/启用 1:不启用',
  `IsSuperTube` int DEFAULT '0' COMMENT '是否是超管 0-是 1-否',
  `DepartmentID` char(32) NOT NULL DEFAULT '' COMMENT '部门id',
  `Code` varchar(64) DEFAULT NULL COMMENT '角色编码不能为空',
  `Memo` varchar(100) DEFAULT NULL COMMENT '备注',
  `IsDelete` int DEFAULT '0' COMMENT '删除标识 0:未删除 1:删除',
  `CreateOperatorID` char(32) DEFAULT NULL COMMENT '创建者ID',
  `CreateOperatorName` varchar(50) DEFAULT NULL COMMENT '创建者名称',
  `CreateTime` bigint DEFAULT NULL COMMENT '创建时间',
  `UpdateOperatorID` char(32) DEFAULT NULL COMMENT '更新者ID',
  `UpdateOperatorName` varchar(50) DEFAULT NULL COMMENT '更新者名称',
  `UpdateTime` bigint DEFAULT NULL COMMENT '更新时间',
  `DeleteOperatorID` char(32) DEFAULT NULL COMMENT '删除者ID',
  `DeleteOperatorName` varchar(50) DEFAULT NULL COMMENT '删除者名称',
  `DeleteTime` bigint DEFAULT NULL COMMENT '删除时间',
  `ExtraParams1` varchar(100) DEFAULT NULL COMMENT '额外参数1',
  `ExtraParams2` varchar(100) DEFAULT NULL COMMENT '额外参数2',
  `ExtraParams3` varchar(100) DEFAULT NULL COMMENT '额外参数3',
  PRIMARY KEY (`ID`),
  KEY `IsDelete` (`IsDelete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统管理-角色管理表';

/*Data for the table `t_sys_role` */

insert  into `t_sys_role`(`ID`,`RoleName`,`IsUpdate`,`IsStatus`,`IsSuperTube`,`DepartmentID`,`Code`,`Memo`,`IsDelete`,`CreateOperatorID`,`CreateOperatorName`,`CreateTime`,`UpdateOperatorID`,`UpdateOperatorName`,`UpdateTime`,`DeleteOperatorID`,`DeleteOperatorName`,`DeleteTime`,`ExtraParams1`,`ExtraParams2`,`ExtraParams3`) values ('90b3384c28044e85aab06960e77afbc5','运营1',0,0,1,'b97b3fd55e1347ea86a3cb84fa9c267f','user','',0,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1642670375898,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1644999323269,NULL,NULL,NULL,NULL,NULL,NULL),('b31b5c862daa4684bedb12e890310128','超级管理员',0,0,1,'1','ADMIN','111111111111',0,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1622530426184,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1642562124908,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1622531488213,NULL,NULL,NULL),('db7bad475f114048bb2e87dd487a1308','开发管理员',0,0,1,'fce2caf438254835b55da8908632d486','user','',0,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1641808980038,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e647b3b72e4140db9b47d8dd69f208dc','运营-my',0,0,1,'b97b3fd55e1347ea86a3cb84fa9c267f','user','',0,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1644896724103,'fe3cc2241d3f40e38a788a77fcd98581',NULL,1645002299409,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
