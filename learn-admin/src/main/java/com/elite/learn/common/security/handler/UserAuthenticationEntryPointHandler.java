package com.elite.learn.common.security.handler;

import com.alibaba.fastjson.JSON;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.ServletUtils;
import com.elite.learn.common.utils.string.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用户没有登录时返回给前端的数据
 * 定义认证失败处理类
 * 用来解决匿名用户访问无权限资源时的异常
 * AuthenticationEntryPoint 用来解决匿名用户访问无权限资源时的异常
 * AccessDeineHandler 用来解决认证过的用户访问无权限资源时的异常
 */
@Component
@Slf4j
public class UserAuthenticationEntryPointHandler implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        log.error("UserAuthenticationEntryPointHandler=" + authException.getMessage());
        String msg = StringUtils.format("请求访问：{}，认证失败，无法访问系统资源", request.getRequestURI());
        ServletUtils.renderString(response, JSON.toJSONString(R.error(HttpServletResponse.SC_UNAUTHORIZED, msg)));
    }
}
