package com.elite.learn.common.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;

import java.io.Serializable;

/**
 *
 * @Title  :UserPermissionEvaluator.java  
 * @Package: com.elite.learn.common.security
 * @Description:  <p> 自定义权限注解验证</p>
 * @update:
 * @author: ；leroy  
 * @data: 2023年03月27日 13:09
 */
@Slf4j
@Configuration
public class UserPermissionEvaluator implements PermissionEvaluator {
    /**
     * hasPermission鉴权方法
     * 这里仅仅判断PreAuthorize注解中的权限表达式
     * 实际中可以根据业务需求设计数据库通过targetUrl和permission做更复杂鉴权
     * @Author Sans
     * @CreateTime 2019/10/6 18:25
     * @Param  authentication  用户身份
     * @Param  targetUrl  请求路径
     * @Param  permission 请求路径权限
     * @Return boolean 是否通过
     */
    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        log.info("用户数据authentication：{}",authentication);
        log.info("targetDomainObject：{}",targetDomainObject);
        log.info("permission：{}",permission);
        // 获取用户信息
       /* SelfUserEntity selfUserEntity =(SelfUserEntity) authentication.getPrincipal();
        // 查询用户权限(这里可以将权限放入缓存中提升效率)
        Set<String> permissions = new HashSet<>();
        List<SysMenuEntity> sysMenuEntityList = sysUserService.selectSysMenuByUserId(selfUserEntity.getUserId());
        for (SysMenuEntity sysMenuEntity:sysMenuEntityList) {
            permissions.add(sysMenuEntity.getPermission());
        }*/
        // 权限对比
       /* if (permissions.contains(permission.toString())){
            return true;
        }*/
        return true;
    }


    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        return false;
    }
}