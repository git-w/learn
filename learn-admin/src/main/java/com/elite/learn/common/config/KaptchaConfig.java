package com.elite.learn.common.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * @Title :KaptchaConfig.java
 * @Package: com.elite.learn.common.config
 * @Description: <p></p>
 * @update:
 * @author: ；leroy
 * @data: 2023年03月20日 10:04
 */
@Slf4j
@Component
public class KaptchaConfig {
    @Bean
    public DefaultKaptcha getKaptcheCode() {
        //验证码生成器
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        //配置
        Properties properties = new Properties();
        //是否有边框
        properties.setProperty("kaptcha.border", "yes");
        //设置边框颜色
        properties.setProperty("kaptcha.border.color", "105,179,90");
        //验证码
        properties.setProperty("kaptcha.session.key", "code");
        //验证码文本字符颜色 默认为黑色
        properties.setProperty("kaptcha.textproducer.font.color", "blue");
        //设置字体样式
        properties.setProperty("kaptcha.textproducer.font.names", "宋体,楷体,微软雅黑");
        //字体大小 默认40
        properties.setProperty("kaptcha.textproducer.font.size", "40");
        //验证码文本字符内容范围 默认为abced23456789gfynmnpwx
        properties.setProperty("kaptcha.textproducer.char.string", "");
        //字符长度 默认为5
        properties.setProperty("kaptcha.textproducer.char.length", "5");
        //字符间距 默认为2
        properties.setProperty("kaptcha.textproducer.char.space", "4");
        //验证码图片宽度 默认为200
        properties.setProperty("kaptcha.image.width", "200");
        //验证码图片高度 默认为40
        properties.setProperty("kaptcha.image.height", "80");
        Config config = new Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }

}