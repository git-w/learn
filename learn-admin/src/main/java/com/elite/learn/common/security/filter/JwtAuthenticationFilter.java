package com.elite.learn.common.security.filter;

import cn.hutool.core.util.StrUtil;
import com.elite.learn.common.security.service.UserDetailsServiceImpl;
import com.elite.learn.common.utils.jwt.JwtUtils;
import com.elite.learn.systemManage.bll.IAccountServiceBLL;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * token 校验 过滤器
 * 认证过滤器 拦截请求验证权限
 * 当我们登录成功后，是通过token去访问我们接口的，因此需要自定义一个过滤器，这个过滤器会去获取请求头中的token，对token进行解析取出其中的用户ID,然后根据用户ID去获取缓存中的用户信息，存入到SecurityContextHolder中
 */

@Component
@Slf4j
public class JwtAuthenticationFilter extends GenericFilterBean {

    @Resource
    JwtUtils jwtUtils;//解析token 工具

    @Resource
    UserDetailsServiceImpl userDetailService;//查看用户登录信息
    @Resource
    IAccountServiceBLL accountServiceBLL;
    /* @Resource
     UserDetailsServiceImpl userDetailService;//查看用户登录信息
     @Resource
     IAccountServiceBLL accountServiceBLL;
 */


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        // 获取header里面的token 信息
        String jwt = request.getHeader(jwtUtils.getHeader());
        if (StrUtil.isBlankOrUndefined(jwt)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        Claims claim = jwtUtils.getClaimByToken(jwt);
        if (claim == null) {
            throw new JwtException("token 异常");
        }
        // 判断token是否过期
        if (jwtUtils.isTokenExpired(claim)) {
            throw new JwtException("token已过期");
        }
        // 获取用户登录名称
        String username = claim.getSubject();
        UsernamePasswordAuthenticationToken token
                = new UsernamePasswordAuthenticationToken(username, null, userDetailService.getUserAuthority(claim.getId()));
        SecurityContextHolder.getContext().setAuthentication(token);
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
