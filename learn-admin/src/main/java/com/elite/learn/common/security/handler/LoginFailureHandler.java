package com.elite.learn.common.security.handler;

import com.alibaba.fastjson.JSON;
import com.elite.learn.common.core.exception.CaptchaException;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.utils.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.security.web.authentication.www.NonceExpiredException;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录失败
 */
@Component
@Slf4j
public class LoginFailureHandler implements AuthenticationFailureHandler {
    //登录日志记录
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        String message;
        log.error("【登录失败】LoginFailureHandler.userName=" + exception);
        // 账号过期
        if (exception instanceof AccountExpiredException) {
            log.error("【账号过期】"+exception.getMessage());
            message = "账号过期";
        }
        // 密码错误
        else if (exception instanceof UsernameNotFoundException) {
            log.error("【用户名不存在】"+exception.getMessage());
            message = "用户名不存在";
        }
        // 密码错误
        else if (exception instanceof BadCredentialsException) {
            log.error("【密码错误】"+exception.getMessage());
            message = "密码错误";
        }
        // 密码过期
        else if (exception instanceof CredentialsExpiredException) {
            log.error("【密码过期】"+exception.getMessage());
            message = "密码过期";
        }
        // 账号不可用
        else if (exception instanceof DisabledException) {
            log.error("【账号不可用】"+exception.getMessage());
            message = "账号不可用";
        }
        //账号锁定
        else if (exception instanceof LockedException) {
            log.error("【账号锁定】"+exception.getMessage());
            message = "账号锁定";
        }
        // 用户不存在
        else if (exception instanceof InternalAuthenticationServiceException) {
            log.error("【用户不存在】"+exception.getMessage());
            message = "用户不存在";
        } else if (exception instanceof LockedException) {
            log.error("【账号锁定】"+exception.getMessage());
            //账号锁定
            message = "账号锁定";
        } else if (exception instanceof NonceExpiredException) {
            log.error("【异地登录】"+exception.getMessage());
            //异地登录
            message = "异地登录";
        } else if (exception instanceof SessionAuthenticationException) {
            log.error("【session错误】"+exception.getMessage());
            //session异常
            message = "session错误";
        } else if (exception instanceof CaptchaException) {
            log.error("【验证码异常】"+exception.getMessage());
        //验证码异常
            message = exception.getMessage();
        }  else {
            log.error("【其他未知异常】"+exception.getMessage());
            //其他未知异常
            message = exception.getMessage();
        }
        ServletUtils.renderString(response, JSON.toJSONString(R.error(BasePojectExceptionCodeEnum.ERROR_CODE.code, message)));
    }
}
