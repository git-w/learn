package com.elite.learn.common.config;

import com.elite.learn.common.interceptor.AccessSignAuthInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Title :WebMvcConfig.java
 * @Package: com.elite.learn.common.config
 * @Description:
 * @author: leroy
 * @data: 2021/5/24 13:27
 */
@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {


    /**
     * Description :
     * Group :
     * <p>
     * 实现自定义拦截器只需要3步
     * 1、创建我们自己的拦截器类并实现 HandlerInterceptor 接口。
     * 2、创建一个Java类继承WebMvcConfigurerAdapter，并重写 addInterceptors 方法。
     * 3、实例化我们自定义的拦截器，然后将对像手动添加到拦截器链中（在addInterceptors方法中添加）。
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        log.info("进入拦截器配置------addInterceptors");
        InterceptorRegistration registration = registry.addInterceptor(new AccessSignAuthInterceptor());
        // 拦截配置
        registration.addPathPatterns("/v1/**");
        // 排除配置
        registration.excludePathPatterns("/", "/v1/sys/code/captcha", "/login", "/js/**", "/css/**", "/images/**", "/static/**"
        );
    }


    /**
     * 配置静态访问资源
     * addResoureHandler：指的是对外暴露的访问路径
     * addResourceLocations：指的是内部文件放置的目录
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/");
    }

}