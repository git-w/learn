package com.elite.learn.common.security.handler;

import com.elite.learn.common.core.result.R;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.naming.AuthenticationException;

/**
 * 定义全局异常处理类，处理没有权限、用户名不存在等异常信息，并以json格式向前端响应这些信息
 * 自定义权限不足的权限
 * 定义无权限访问类
 */
@RestControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(AuthenticationException.class)
    public R handleAuthenticationException(AuthenticationException e) {
        return R.error(e.getMessage());
    }


    /**
     * 权限不足
     *
     * @param e
     * @return
     */
    @ExceptionHandler(AccessDeniedException.class)
    public R handleAccessDeniedException(AccessDeniedException e) {
        return R.error(BasePojectExceptionCodeEnum.Insufficient_IAuthority);
    }

}
