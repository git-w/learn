package com.elite.learn.common.utils.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
/**
 * JWT工具类
 * @Author leroy
 */
@Data
@Component
public class JwtUtils {



    // 令牌有效期（默认30分钟）
    @Value("${jwt.expire}")
    private int expire;

    // 令牌秘钥
    @Value("${jwt.secret}")
    private String secret;

    // 令牌自定义标识
    @Value("${jwt.header}")
    private String header;
    // 令牌自定义标识
    @Value("${jwt.tokenPrefix}")
    private String tokenPrefix;

    @Resource
    private HttpServletRequest request;


    /**
     * 生成token
     * 签发-jwt
     *
     * @param username
     * @return
     */
    public String getToken(String username, String id, String roles) {

        Date nowDate = new Date();
        Date expireDate = new Date(nowDate.getTime() + 1000 * expire);
        Map<String, Object> claims = new HashMap<>(8);
        claims.put("CLAIMS_ROLE", roles);
        return Jwts.builder()
                // 自定义属性 放入用户拥有权限
                .setClaims(claims)
                .claim("username", username)
                .setHeaderParam("typ", "JWT")
                // 放入用户名和用户ID
                .setSubject(username)
                .setId(id)
                // 签发时间
                .setIssuedAt(nowDate)
                // 签发者
                .setIssuer("learn")
                // 失效时间
                .setExpiration(expireDate)
                // 签名算法和密钥
                .signWith(SignatureAlgorithm.HS512, secret)//加密
                .compact();
    }


    /**
     * <p>解析jwt<p/>
     *
     * @return
     */
    public Claims getClaimByTokenInfo() {
        String jwt = request.getHeader(header);
        try {
            return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(jwt)
                    .getBody();
        } catch (Exception e) {
            return null;
        }


    }


    /**
     * 解析jwt
     *
     * @param jwt
     * @return
     */
    public Claims getClaimByToken(String jwt) {
        try {
            return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(jwt)
                    .getBody();
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * jwt是否过期
     *
     * @param claims
     * @return
     */
    public boolean isTokenExpired(Claims claims) {
        return claims.getExpiration().before(new Date());
    }


    /**
     * 验证token是否有效，包括签名是否有效、是否过期
     *
     * @param token
     * @return
     */
    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtException("token 异常");
        }
    }



    /**
     * 从token中获取用户名字
     *
     * @param token
     * @return
     */
    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }


    /**
     * 从请求头中获取token
     *
     * @param req
     * @return
     */
    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }


}
