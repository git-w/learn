package com.elite.learn.common.security.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import java.util.Collection;

/**
 * UserDetails实现类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AccountUser implements UserDetails {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 用户登录名
     */
    private  String username;

    /**
     * 用户真实姓名
     */
    private String AccountName;

    /**
     * 邮箱
     */
    private String Email;

    /**
     * 头像路径
     */
    private String Photo;

    /**
     * 头像路径 长路径
     * FileUploadTool.getImgRootPath() + user.getPhoto())
     */
    private String PhotoPath;

    /**
     * 员工编号
     */
    private String AccountCode;

    /**
     * 手机号
     */
    private String Phone;


    /**
     * 地址
     */
    private String Address;

    /**
     * System.currentTimeMillis()
     * 登录时间
     */
    private Long LoginTime;

    /**
     * System.currentTimeMillis()
     * String serviceToken = JwtHelper.sign(dto, 120 * 60 * 60 * 1000);
     */
    private String Authorization;


    private  Collection<? extends GrantedAuthority> authorities;

    private  boolean accountNonExpired;

    private  boolean accountNonLocked;

    private  boolean credentialsNonExpired;

    private  boolean enabled;


    public AccountUser(String userId, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        this(userId, username, password, true, true, true, true, authorities);
    }


    public AccountUser(String userId, String username, String password, boolean enabled, boolean accountNonExpired,
                       boolean credentialsNonExpired, boolean accountNonLocked,
                       Collection<? extends GrantedAuthority> authorities) {
        Assert.isTrue(username != null && !"".equals(username) && password != null,
                "Cannot pass null or empty values to constructor");
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.credentialsNonExpired = credentialsNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.authorities = authorities;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    /**
     * 账户是否过期
     *
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    /**
     * 是否禁用
     *
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    /**
     * 密码是否过期
     *
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    /**
     * 是否启用
     *
     * @return
     */
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}
