package com.elite.learn.common.task;
/**
 *
 * @Title  :TimingTimeSetting.java
 * @Package: com.elite.learn.common.task
 * @Description:
 * @author: leroy
 * @data: 2020/10/20 14:11 
 */
public class TimingTimeSetting {

    /**
     * 每隔5秒执行一次
     */
    public  static final String  FIVE_SECONDS="*/5 * * * * ?";

    /**
     * 每隔1分钟执行一次
     */
    public  static final String  A_MINUTE="0 */1 * * * ?";
    /**
     * 每天23点执行一次
     */
    public  static final String  HOURS_23_POINTS="0 0 23 * * ?";
    /**
     * 每天凌晨1点执行一次
     */
    public  static final String  HOURS_1_POINTS="0 0 1 * * ?";
    /**
     * 每月1号凌晨1点执行一次
     */
    public  static final String  MONTH_1_HOURS_1_POINTS="0 0 1 1 * ?";
    /**
     * 每月最后一天23点执行一次
     */
    public  static final String  MONTH_LAST_HOURS_23_POINTS="0 0 23 L * ?";
    /**
     * 每周星期天凌晨1点实行一次
     */
    public  static final String  SUNDAY_HOURS_1_POINTS="0 0 1 ? * L";
    /**
     * 在26分、29分、33分执行一次
     */
    public  static final String  MINUTES_26_29_33_POINTS="0 26,29,33 * * * ?";

    /**
     * 每天的0点、13点、18点、21点都执行一次
     */
    public  static final String  HOURS_0_13_18_21_POINTS="0 0 0,13,18,21 * * ?";

}