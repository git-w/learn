package com.elite.learn.common.security.handler;

import com.alibaba.fastjson.JSON;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.security.entity.AccountUser;
import com.elite.learn.common.utils.ServletUtils;
import com.elite.learn.common.utils.jwt.JwtUtils;
import com.elite.learn.systemManage.bll.IAccountServiceBLL;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description 登录成功处理类
 * 自定义的登录成功处理器
 * 登录成功后进入
 * 进入这里就代表登录成功校验通过了
 */
@Component
@Slf4j
public class LoginSuccessHandler implements AuthenticationSuccessHandler {
    @Resource
    JwtUtils jwtUtils;
    @Resource
    IAccountServiceBLL accountServiceBLL;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // 请求成功
        AccountUser account = (AccountUser) authentication.getPrincipal();
        // 生成jwt，并放置到请求头中
        String jwt = jwtUtils.getToken(authentication.getName(), account.getUserId(), accountServiceBLL.getRoleMenuList(account.getUserId()));
        response.setHeader(jwtUtils.getHeader(), jwt);
        log.info("LoginSuccessHandler.userName=" + account);
        account.setPassword(null);
        account.setAuthorization(jwt);
        ServletUtils.renderString(response, JSON.toJSONString(R.ok("登录成功", account)));
    }

}
