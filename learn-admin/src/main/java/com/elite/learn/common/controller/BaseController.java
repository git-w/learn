package com.elite.learn.common.controller;

import com.elite.learn.common.utils.jwt.JwtUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 公共的Controller
 */
public class BaseController {

	@Resource
    public HttpServletRequest request;

	// jwt token解密
	@Resource
    public  JwtUtils jwtUtils;

}
