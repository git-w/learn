package com.elite.learn.common.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Title :AccessSignAuthInterceptor.java
 * @Package: com.elite.learn.common.filter
 * @Description: Spring拦截器
 * HandlerInterceptorAdapter需要继承，HandlerInterceptor需要实现
 * 可以作为日志记录和登录校验来使用
 * 建议使用HandlerInterceptorAdapter，因为可以按需进行方法的覆盖。
 * ————————————————
 * 主要为3种方法：
 * preHandle：拦截于请求刚进入时，进行判断，需要boolean返回值，如果返回true将继续执行，如果返回false，将不进行执行。一般用于登录校验。
 * postHandle：拦截于方法成功返回后，视图渲染前，可以对modelAndView进行操作。
 * afterCompletion：拦截于方法成功返回后，视图渲染前，可以进行成功返回的日志记录。
 * ————————————————
 * <p>
 * 简介
 * SpringWebMVC的处理器拦截器，类似于Servlet开发中的过滤器Filter，用于处理器进行预处理和后处理。
 * ————————————————
 * <p>
 * 应用场景
 * 1、日志记录，可以记录请求信息的日志，以便进行信息监控、信息统计等。
 * 2、权限检查：如登陆检测，进入处理器检测是否登陆，如果没有直接返回到登陆页面。
 * 3、性能监控：典型的是慢日志
 * ————————————————
 * <p>
 * 拦截器适配器HandlerInterceptorAdapter
 * 有时候我们可能只需要实现三个回调方法中的某一个，如果实现HandlerInterceptor接口的话，
 * 三个方法必须实现，不管你需不需要，此时spring提供了一个HandlerInterceptorAdapter适配器（种适配器设计模式的实现），
 * 允许我们只实现需要的回调方法。
 * ————————————————
 * <p>
 * 这样在我们业务中比如要记录系统日志，日志肯定是在afterCompletion之后记录的，否则中途失败了，也记录了，那就扯淡了。一定是程序正常跑完后，我们记录下那些对数据库做个增删改的操作日志进数据库。所以我们只需要继承HandlerInterceptorAdapter，并重写afterCompletion一个方法即可，因为preHandle默认是true。
 * 运行流程总结如下：
 * 1、拦截器执行顺序是按照Spring配置文件中定义的顺序而定的。
 * 2、会先按照顺序执行所有拦截器的preHandle方法，一直遇到return false为止，比如第二个preHandle方法是return false，则第三个以及以后所有拦截器都不会执行。若都是return true，则按顺序加载完preHandle方法。
 * 3、然后执行主方法（自己的controller接口），若中间抛出异常，则跟return false效果一致，不会继续执行postHandle，只会倒序执行afterCompletion方法。
 * 4、在主方法执行完业务逻辑（页面还未渲染数据）时，按倒序执行postHandle方法。若第三个拦截器的preHandle方法return false，则会执行第二个和第一个的postHandle方法和afterCompletion（postHandle都执行完才会执行这个，也就是页面渲染完数据后，执行after进行清理工作）方法。（postHandle和afterCompletion都是倒序执行）
 * ————————————————
 * @author: leroy
 * @data: 2021/5/24 13:28
 */
@Slf4j
public class AccessSignAuthInterceptor implements HandlerInterceptor {


    /**
     * 只有返回true才会继续向下执行，返回false取消当前请求
     * <p>
     * <p>
     * 可以在这个方法中进行一些前置初始化操作或者是对当前请求的一个预处理，
     * 也可以在这个方法中进行一些判断来决定请求是否要继续进行下去。
     * 该方法的返回值是布尔值Boolean 类型的，当它返回为false 时，
     * 表示请求结束，后续的Interceptor 和Controller 都不会再执行；
     * 当返回值为true 时就会继续调用下一个Interceptor 的preHandle 方法
     * 如果已经是最后一个Interceptor 的时候就会是调用当前请求的Controller 方法。 postHandle
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     * @author: leroy
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.debug("拦截器执行=========");
        log.debug("请求url:{}", request.getRequestURI());
        log.debug("拦截器结束=========");
        return true;
    }

    /**
     * postHandle 方法，顾名思义就是在当前请求进行处理之后，
     * 也就是Controller 方法调用之后执行，但是它会在DispatcherServlet
     * 进行视图返回渲染之前被调用，所以我们可以在这个方法中对Controller 处理之后的ModelAndView 对象进行操作
     */
    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) {
        log.debug("postHandle:请求后调用");
    }


    /**
     * 该方法将在整个请求结束之后，也就是在DispatcherServlet
     * 渲染了对应的视图之后执行。这个方法的主要作用是用于进行资源清理工作的。
     */
    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex) {
        log.debug("afterCompletion:请求调用完成后回调方法，即在视图渲染完成后回调");
    }


}