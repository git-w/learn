package com.elite.learn.common.security.service;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.common.security.entity.AccountUser;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IAccountServiceBLL;
import com.elite.learn.systemManage.contants.SystemManageExceptionCodeEnum;
import com.elite.learn.systemManage.dto.AccountDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 查询用户是否存在
 */
@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    IAccountServiceBLL accountServiceBLL;

    /**
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.error("UserDetailsServiceImpl-loadUserByUsername-登录名=" + username);

        if (StringUtil.isEmpty(username)) { // 用户名为空的时候
            throw new UsernameNotFoundException(BasePojectExceptionCodeEnum.LoginNameParamsError.msg);
        }
        // 查询用户是否存在
        AccountDTO sysAccount = accountServiceBLL.getLoginName(username);
        if (Objects.isNull(sysAccount)) {//如果用户不存在
            throw new CommonException(SystemManageExceptionCodeEnum.LoginNameError.code, SystemManageExceptionCodeEnum.LoginNameError.msg);
        }

        AccountUser bean = new AccountUser(sysAccount.getId(), sysAccount.getLoginName(), sysAccount.getLoginPassword(), this.getUserAuthority(sysAccount.getId()));
        bean.setAccountName(sysAccount.getAccountName());//用户真实姓名
        bean.setEmail(sysAccount.getEmail());//邮箱
        bean.setPhoto(sysAccount.getPhoto());//头像路径
        if (StringUtil.isNotEmpty(sysAccount.getPhoto())) {
            bean.setPhotoPath(FileUploadUtil.getImgRootPath() + sysAccount.getPhoto());
        }
        bean.setAccountCode(sysAccount.getAccountCode());
        bean.setPhone(sysAccount.getPhone());//手机号
        bean.setAddress(sysAccount.getAddress());//地址
        bean.setLoginTime(System.currentTimeMillis());

        return bean;
    }

    /**
     * 获取用户权限信息（角色、菜单权限）
     *
     * @param id
     * @return
     */
    public List<GrantedAuthority> getUserAuthority(String id) {
        // 角色(ROLE_admin)、菜单操作权限 sys:user:list
        String authority = accountServiceBLL.getRoleMenuList(id);
        log.info("JwtUserDetailsService=" + authority);
        // 存储role字符串链表
        return AuthorityUtils.commaSeparatedStringToAuthorityList(authority);
    }
}
