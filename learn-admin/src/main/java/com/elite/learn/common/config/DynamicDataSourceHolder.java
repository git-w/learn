package com.elite.learn.common.config;

import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 *
 * @Title  :DynamicDataSourceHolder.java  
 * @Package: com.elite.learn.common.config
 * @Description:
 * @author: leroy
 * @data: 2020/9/13 21:38 
 */
@Slf4j
public class DynamicDataSourceHolder {
    private static ThreadLocal<String> contextHolder = new ThreadLocal<String>();
    public static final String DB_MASTER = "master";
    public static final String DB_SLAVE = "slave";

    public static String getDbType() {
        String db = contextHolder.get();
        if (Objects.isNull(db)) {
            db = DB_MASTER;
        }
        return db;
    }

    /**
     * 设置线程的dbType
     *
     * @param str
     */
    public static void setDbType(String str) {
        log.debug("所使用的数据源为：" + str);
        System.out.println("所使用的数据源为：" + str);
        contextHolder.set(str);
    }

    /**
     * 清理连接类型
     */
    public static void clearDBType() {
        contextHolder.remove();
    }
}