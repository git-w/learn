package com.elite.learn.common.security.handler;

import com.alibaba.fastjson.JSON;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.utils.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Title :UserAuthAccessDeniedHandler.java
 * @Package: com.elite.learn.common.security.handler
 * @Description: <p>自定义无权限访问时的处理器
 * 请求配置了权限的访问资源如果没有权限会进入这里面进行处理</p>
 * @update:
 * @author: ；leroy
 * @data: 2023年03月27日 10:54
 */
@Slf4j
@Component
public class UserAuthAccessDeniedHandler implements AccessDeniedHandler {
    /**
     * 暂无权限返回结果
     *
     * @Author Sans
     * @CreateTime 2019/10/3 8:41
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception) {
        ServletUtils.renderString(response, JSON.toJSONString(R.error(BasePojectExceptionCodeEnum.Insufficient_IAuthority)));
    }
}