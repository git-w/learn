package com.elite.learn.common.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.Map;

/**
 *
 * @Title  :DynamicDataSource.java  
 * @Package: com.elite.learn.common.config
 * @Description:
 * @author: leroy
 * @data: 2020/9/13 21:37 
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
   /* @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceHolder.getDbType();
    }*/


    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceHolder.getDbType();
    }



}