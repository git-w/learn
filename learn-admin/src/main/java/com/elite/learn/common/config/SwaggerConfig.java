package com.elite.learn.common.config;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.*;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title :SwaggerConfig.java
 * @Package: com.elite.learn.common.config
 * @Description: <p></p>
 * @update:
 * @author: ；leroy
 * @data: 2023年03月20日 15:15
 */
@Configuration
@EnableOpenApi
public class SwaggerConfig {
    /**
     * 是否开启swagger
     */
    @Value("${swagger.enabled}")
    private boolean enabled;


    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                // ture 启用Swagger3.0， false 禁用（生产环境要禁用）
                //是否启用swagger
                .enable(enabled)
                .select()
                //加了ApiOperation注解的类，才生成接口文档
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .globalRequestParameters(getGlobalRequestParameters())//加入通用入参
                .globalResponses(HttpMethod.POST, getGlobalResonseMessage());//post方法加入通用响应头
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("learn管理端")
                .termsOfServiceUrl("https://gitee.com/ray-ac/learn")
                .contact(new Contact("leroy", "https://gitee.com/ray-ac/learn", "leroy.tankard@gmail.com"))
                .description("learn测试学习")
                .version("版本号：1.0")
                .build();
    }


    //生产通过接口入参
    private List<RequestParameter> getGlobalRequestParameters() {
        List<RequestParameter> parameters = new ArrayList<>();
        parameters.add(new RequestParameterBuilder()
                .name("token")
                .description("token")
                .required(true)
                .in(ParameterType.QUERY)
                .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
                .required(false)
                .build());
        parameters.add(new RequestParameterBuilder()
                .name("udid")
                .description("唯一id")
                .required(true)
                .in(ParameterType.QUERY)
                .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
                .required(false)
                .build());
        parameters.add(new RequestParameterBuilder()
                .name("version")
                .description("客户端的版本号")
                .required(true)
                .in(ParameterType.QUERY)
                .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
                .required(false)
                .build());
        return parameters;
    }

    //生成通用响应信息
    private List<Response> getGlobalResonseMessage() {
        List<Response> responseList = new ArrayList<>();
        responseList.add(new ResponseBuilder()
                .code("404")
                .description("找不到资源")
                .build());
        responseList.add(new ResponseBuilder()
                .code("0")
                .description("成功")
                .build());
        responseList.add(new ResponseBuilder()
                .code("10")
                .description("系统异常")
                .build());
        responseList.add(new ResponseBuilder()
                .code("20")
                .description("参数错误")
                .build());
        responseList.add(new ResponseBuilder()
                .code("30")
                .description("系统异常")
                .build());
        //或者通过枚举类添加
        for (BasePojectExceptionCodeEnum value:BasePojectExceptionCodeEnum.values()) {
            String code = String.valueOf(value.code);
            String message =String.valueOf(value.msg);
                    responseList.add(new ResponseBuilder().code(code).description(message).build());
        }

        return responseList;
    }

}