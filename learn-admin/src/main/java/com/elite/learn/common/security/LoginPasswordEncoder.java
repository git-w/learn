package com.elite.learn.common.security;

import com.elite.learn.common.utils.encrypt.md5.Md5Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 自定义密码加密
 *
 * @Title :LoginPasswordEncoder.java
 * @Package: com.elite.learn.common.security
 * @Description:
 * @author: leroy
 * @data: 2021/5/31 16:06
 */
@Slf4j
public class LoginPasswordEncoder implements PasswordEncoder {

    /**
     * 密码加密
     *
     * @param rawPassword
     * @return
     */
    @Override
    public String encode(CharSequence rawPassword) {
        log.info("LoginPasswordEncoder.rawPassword=" + rawPassword);
        Md5Util md5 = Md5Util.getInstance();
        String pwd = md5.getLongToken(rawPassword != null ? rawPassword.toString() : "");
        return pwd;
    }


    /**
     * 密码对比
     *
     * @param rawPassword     新密码
     * @param encodedPassword 旧密码
     * @return
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        boolean flag = false;
        // 判断密码参数是否为空
        log.info("LoginPasswordEncoder.encodedPassword=" + encodedPassword);
        String password = encode(rawPassword);
        log.info("LoginPasswordEncoder.password=" + password);
        flag = password.equals(encodedPassword);
        // 密码错误--登录失败
        if (!flag) {
            return flag;
        }
        return flag;
    }
}