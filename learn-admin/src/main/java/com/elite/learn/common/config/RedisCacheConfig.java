package com.elite.learn.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

/**
 * @Title :RedisCacheConfig.java
 * @Package: com.elite.learn.common.config
 * @Description:
 * @author: leroy
 * @data: 2020/5/9 15:39
 */
@Component
@Configuration
public class RedisCacheConfig {


    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        //关联
        template.setConnectionFactory(factory);
        //设置key的序列化器
        template.setKeySerializer(new StringRedisSerializer());
        //设置value的序列化器
        template.setValueSerializer(new StringRedisSerializer());

        template.setDefaultSerializer(redisKeySerializer());  //其他的都是之前设置的，设置这个即可，将默认的序列化方式改成StringRedisSerializer
        return template;
    }
    @Bean
    public RedisSerializer<?> redisKeySerializer() {
        return new StringRedisSerializer();
    }

}
