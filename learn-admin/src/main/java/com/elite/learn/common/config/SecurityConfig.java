package com.elite.learn.common.config;

import com.elite.learn.common.security.LoginPasswordEncoder;
import com.elite.learn.common.security.UserPermissionEvaluator;
import com.elite.learn.common.security.filter.JwtAuthenticationFilter;
import com.elite.learn.common.security.handler.*;
import com.elite.learn.common.security.service.UserDetailsServiceImpl;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.Arrays;

/**
 * Spring Security配置类
 *
 * @author: leroy
 * @date 2021
 */
@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig {
    /**
     * 自定义暂无权限处理器
     */
    @Resource
    private UserAuthAccessDeniedHandler userAuthAccessDeniedHandler;
    /**
     * 自定义未登录的处理器
     */
    @Resource
    private UserAuthenticationEntryPointHandler userAuthenticationEntryPointHandler;

    /**
     * 自定义登录成功处理器
     */
    @Resource
    private LoginSuccessHandler loginSuccessHandler;
    /**
     * 自定义登录失败处理器
     */
    @Resource
    private LoginFailureHandler loginFailureHandler;
    /**
     * 自定义注销成功处理器
     */
    @Resource
    private GlobalLogoutSuccessHandler globalLogoutSuccessHandler;

    /**
     * token 校验 过滤器
     */
    @Resource
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    /**
     * 用户userService
     */
    @Resource
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Resource
    private UserPermissionEvaluator userPermissionEvaluator;

    /**
     * 不需要拦截的
     */
    private static final String[] URL_WHITELIST = {
            "/swagger-*", //swagger文档
            "/api/swagger-resources",
            "/v3/**", //swagger文档
            "/doc.html", //swagger文档
            "/webjars/**", //swagger文档
            "/swagger**/**", //swagger文档
            "/v1/file/fastDFS/preview2",
            "/login",
            "/logout",
            "/v1/sys/code/captcha",
            "/common/download**",
            "/druid/**",
            "/favicon.ico"
    };

    /**
     * anyRequest          |   匹配所有请求路径
     * access              |   SpringEl表达式结果为true时可以访问
     * anonymous           |   匿名可以访问
     * denyAll             |   用户不能访问
     * fullyAuthenticated  |   用户完全认证可以访问（非remember-me下自动登录）
     * hasAnyAuthority     |   如果有参数，参数表示权限，则其中任何一个权限可以访问
     * hasAnyRole          |   如果有参数，参数表示角色，则其中任何一个角色可以访问
     * hasAuthority        |   如果有参数，参数表示权限，则其权限可以访问
     * hasIpAddress        |   如果有参数，参数表示IP地址，如果用户IP和参数匹配，则可以访问
     * hasRole             |   如果有参数，参数表示角色，则其角色可以访问
     * permitAll           |   用户可以任意访问
     * rememberMe          |   允许通过remember-me登录的用户访问
     * authenticated       |   用户登录后可访问
     */
    @SneakyThrows
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) {
        log.info("进入授权 =>>>>>>>>>>>>>>>>>>>>>>");
        //csrf().disable()是为了防止csdf攻击的，至于什么是csdf攻击，
        http.formLogin()
                .usernameParameter("username")
                .passwordParameter("password")
                //配置登录成功自定义处理类
                .successHandler(loginSuccessHandler)
                //配置登录失败自定义处理类
                .failureHandler(loginFailureHandler)
                .permitAll()
                //登出方法
                .and()
                .logout()
                .logoutSuccessHandler(globalLogoutSuccessHandler)

                //配置没有权限自定义处理类
                .and()
                .exceptionHandling()
                .accessDeniedHandler(userAuthAccessDeniedHandler)
                .authenticationEntryPoint(userAuthenticationEntryPointHandler)  //未登录是的逻辑处理


                // 基于token，所以不需要session
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                .and()
                .httpBasic()
                /**
                 * antMatchers(HttpMethod.OPTIONS, "/**")，是为了方便后面写前后端分离的时候前端过来的第一次验证请求，
                 * 这样做，会减少这种请求的时间和资源使用。
                 * 配置拦截规则
                 *  过滤请求
                 */

                .and()
                .authorizeRequests()
                .antMatchers(URL_WHITELIST).permitAll()//url白名单
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .anyRequest().authenticated()
                // 禁用缓存
                // 配置自定义的过滤器  验证码过滤器
                .and()
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                // 添加JWT filter
                //csrf().disable()是为了防止csdf攻击的，至于什么是csdf攻击，
                .cors()
                .configurationSource(corsConfigurationSource())
                .and()
                .csrf().disable()
        ;
        return http.build();
    }

    /**
     * 解决跨域
     *
     * @return
     */
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.setAllowedOriginPatterns(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("*"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setMaxAge(Duration.ofHours(1));
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    /**
     * //身份验证管理  基于数据库自定义UserDetailsService
     * //AuthenticationManager是定义Spring Security的过滤器如何执行身份验证的API。
     * // 然后，调用AuthenticationManager的控制器（即Spring Security的Filters实例）
     * // 在SecurityContextHolder上设置返回的Authentication。
     *
     * @return
     * @throws Exception
     */
    @Bean
    public AuthenticationManager authenticationManager(HttpSecurity httpSecurity) throws Exception {
        AuthenticationManager authenticationManager = httpSecurity.getSharedObject(AuthenticationManagerBuilder.class)
                .userDetailsService(userDetailsServiceImpl)

                .passwordEncoder(passwordEncoder())
                .and()
                .build();

        return authenticationManager;
    }


    /**
     * 自定义密码加密
     *
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        //SpringSecurity 提供的一种编码器，我们也可以自己实现PasswordEncoder
        return new LoginPasswordEncoder();
    }

    /**
     * 注入自定义PermissionEvaluator
     */
    @Bean
    public DefaultWebSecurityExpressionHandler userSecurityExpressionHandler() {
        DefaultWebSecurityExpressionHandler handler = new DefaultWebSecurityExpressionHandler();
        handler.setPermissionEvaluator(userPermissionEvaluator);
        return handler;
    }

}
