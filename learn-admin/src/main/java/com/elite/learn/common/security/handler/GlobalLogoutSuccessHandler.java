package com.elite.learn.common.security.handler;

import com.alibaba.fastjson.JSON;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.ServletUtils;
import com.elite.learn.common.utils.jwt.JwtUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * @Description 用户登出类
 */
@Component
public class GlobalLogoutSuccessHandler implements LogoutSuccessHandler {

    @Resource
    JwtUtils jwtUtils;

    /**
     * 用户登出返回结果
     * 这里应该让前端清除掉Token
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if (Objects.nonNull(authentication)) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
        response.setHeader(jwtUtils.getHeader(), "");
        ServletUtils.renderString(response, JSON.toJSONString(R.ok()));
    }
}
