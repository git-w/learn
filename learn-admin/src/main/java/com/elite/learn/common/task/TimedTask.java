package com.elite.learn.common.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时处理
 *
 * @author: leroy
 */
@Slf4j
@Component("alertTask")
public class TimedTask {

    /**
     * 每隔1分钟执行一次
     */
    @Scheduled(cron = "0 */30 * * * ?")
    public void creatTableTask() {
        log.info("——————————————————————————定时任务");

        log.info("——————————————————————————定时任务");
    }


}