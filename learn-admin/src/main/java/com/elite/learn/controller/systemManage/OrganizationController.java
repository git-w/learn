package com.elite.learn.controller.systemManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IOrganizationServiceBLL;
import com.elite.learn.systemManage.dto.OrganizationDTO;
import com.elite.learn.systemManage.entity.Organization;
import com.elite.learn.systemManage.query.OrganizationQuery;
import com.elite.learn.systemManage.vo.OrganizationVO;
import io.jsonwebtoken.Claims;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 系统管理-组织架构表 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2021-01-20
 */
@RestController
@RequestMapping(value = "v1/sys/organizatioon", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class OrganizationController extends BaseController {


    @Resource
    private IOrganizationServiceBLL service;



    /**
     * 保存
     *
     * @param params
     * @return
     */
    @PreAuthorize("hasAuthority('v1:sys:organizatioon:save')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody OrganizationDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setCreateOperatorName(claim.getSubject());
        String data = this.service.save(claim.getId(), params);
        if (StringUtil.isNotEmpty(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }



    /**
     * 修改
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasAuthority('v1:sys:organizatioon:update')")
    @PostMapping("/update")
    public R update(@RequestBody @Validated(Update.class) OrganizationDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.update(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }



    /**
     * 详情
     *
     * @param params
     * @return
     */
    @PreAuthorize("hasAuthority('v1:sys:organizatioon:list')")
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        Organization bean = this.service.get(params.getId());
        if (Objects.nonNull(bean)) {
            OrganizationDTO dto = new OrganizationDTO();
            BeanUtils.copyProperties(bean, dto);
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, dto);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }

    /**
     * 删除
     *
     * @param params
     * @return
     */
    @PreAuthorize("hasAuthority('v1:sys:organizatioon:delete')")
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = service.remove(claim.getId(), params.getIds());
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }




    /**
     * 获取公司下面所有的部门
     *
     * @param params
     * @return
     */
    @PostMapping("/findAllDepartment")
    public R findAllDepartment(@RequestBody OrganizationQuery params) {
        List<OrganizationVO> result = this.service.findAllDepartment(params);
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }




    /**
     * 获取菜单(角色设置菜单权限时，获取全量菜单)
     *
     * @param params
     * @return
     * @throws
     */
    @PostMapping("/getTree")
    public R getTree(@RequestBody OrganizationQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getTree(params));
    }

}

