package com.elite.learn.controller.userManage;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.userManage.bll.impl.UserMateSelectionServiceBLL;
import com.elite.learn.userManage.dto.UserMateSelectionDTO;
import com.elite.learn.userManage.vo.UserMateSelectionVO;
import io.jsonwebtoken.Claims;
import org.apache.ibatis.annotations.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Description: 用户附属表
 * @Title: UserMateSelectionController
 * @Package com.elite.learn.controller.userManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/1 15:52
 */
@RestController
@RequestMapping("v1/user/userMateSerlection")
public class UserMateSelectionController  extends BaseController {

    @Resource
    private UserMateSelectionServiceBLL service;



    /**
     * 修改个人客户
     *
     * @param dto
     * @return
     * @throws
     */
    @PostMapping("/update")
    public R update( @RequestBody @Validated(Update.class) UserMateSelectionDTO dto) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.update(claim.getId(), dto);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }





    /**
     * 查询用户详情
     * @author: leroy
     * @date 2021/8/13 10:14
     * @param query
     * @return com.elite.learn.common.core.result.R
     */

    @PostMapping("/getInfo")
    public R getInfo(@RequestBody @Validated IDParams query) {
        UserMateSelectionVO result = this.service.getInfo(query.getId());
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);

}}