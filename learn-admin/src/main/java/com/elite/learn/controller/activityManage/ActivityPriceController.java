package com.elite.learn.controller.activityManage;

import com.elite.learn.activityManage.bll.IActivityPriceServiceBLL;
import com.elite.learn.activityManage.dto.ActivityPriceDTO;
import com.elite.learn.activityManage.query.ActivityPriceQuery;
import com.elite.learn.activityManage.vo.ActivityPriceVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import io.jsonwebtoken.Claims;
import org.apache.ibatis.annotations.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Description: 活动时间价格区间表
 * @Title: ActivityPriceController
 * @Package com.elite.learn.controller.activityManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/22 13:16
 */

@RestController
@RequestMapping("v1/activity/price")
public class ActivityPriceController extends BaseController {


    @Resource
    private IActivityPriceServiceBLL service;


    /**
     * 详情
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:29
     * @update
     * @updateTime
     */
    @PostMapping("/save")
    public R getInfo(@Validated @RequestBody ActivityPriceDTO params) {
        /**
         * 获取用户信息
         * 判断返回值是否为空
         * 如果不为空，返回保存成功状态
         */
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setCreateOperatorName(claim.getSubject());
        String data = this.service.save(claim.getId(), params);
        if (StringUtil.isNotEmpty(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess);
        }
        return R.ok(BasePojectExceptionCodeEnum.SaveFail, params);

    }


    /**
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:33
     * @update
     * @updateTime 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody @Validated(Update.class) ActivityPriceDTO bean) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        bean.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.update(claim.getId(), bean);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 删除
     */
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        //判断boolean
        if (flag) {
            //如果true，返回删除成功状态
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }


    /**
     * 详情
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:29
     * @update
     * @updateTime
     */
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        ActivityPriceVO result = this.service.getInfo(params.getId());
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);

        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:20
     * @update
     * @updateTime
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody ActivityPriceQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }


}