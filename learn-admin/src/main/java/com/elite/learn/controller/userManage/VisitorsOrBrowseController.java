package com.elite.learn.controller.userManage;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.result.R;
import com.elite.learn.userManage.bll.IVisitorsOrBrowseServiceBLL;
import com.elite.learn.userManage.query.VisitorsOrBrowseQuery;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description: 浏览访客
 * @Title: VisitorsOrBrowseController
 * @Package com.elite.learn.controller.userManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/15 10:37
 */
@RestController
@RequestMapping("v1/user/visitorsOrBrowse")
public class VisitorsOrBrowseController extends BaseController {


    @Resource
    private  IVisitorsOrBrowseServiceBLL service;


    /**
     * 访客
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/15 10:50
     * @update
     * @updateTime
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody VisitorsOrBrowseQuery params) {
        params.setUserId(params.getUserId());
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));

    }

    /**
     * 被浏览的用户
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/15 10:51
     * @update
     * @updateTime
     */
    @PostMapping("/getListBrowse")
    public R getListBrowse(@Validated @RequestBody VisitorsOrBrowseQuery params) {
        params.setCreateOperatorId(params.getUserId());
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getListBrowse(params));
    }
}