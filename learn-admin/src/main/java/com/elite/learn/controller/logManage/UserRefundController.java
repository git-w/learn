package com.elite.learn.controller.logManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.result.R;
import com.elite.learn.logManage.bll.IUserRefundPayServiceBLL;
import com.elite.learn.logManage.query.UserRefundPayQuery;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 订单退款记录表 前端控制器
 * </p>
 *
 * @author: leroy
 * @date 2021/7/15 13:49
 */
@RestController
@RequestMapping("/v1/log/userRefund")
public class UserRefundController {
    @Resource
    private IUserRefundPayServiceBLL service;
    /*
    @Resource
    private HttpServletRequest request;
    */

    /**
     * <p>
     * 列表
     * </p>
     *
     * @param params
     * @return com.elite.learn.tools.base.common.rest.response.R
     * @author: leroy
     * @date 2021/7/15 14:19
     */
    @PostMapping("/getList")
    public R getList(@RequestBody UserRefundPayQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));

    }
}
