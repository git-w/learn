package com.elite.learn.controller.cardManage;


import com.elite.learn.cardManage.bll.IUserMemberCardServiceBLL;
import com.elite.learn.cardManage.query.LogUserMemberCardQuery;
import com.elite.learn.cardManage.vo.UserMemberCardVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 后台管理员登陆日志 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2020-11-24
 */
@RestController
@RequestMapping("v1/log/userMemberCard")
public class LogUserMemberCardController {

    @Resource
    private IUserMemberCardServiceBLL service;



    /**
     * 查看用户登录日志
     *
     * @param params
     * @return
     */
    @PostMapping("/getList")
    //@PreAuthorize("hasAuthority('v1:log:userLogin:list')")
    public R getList(@Validated @RequestBody LogUserMemberCardQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }




    /**
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 详情
     */
    @PostMapping("/getVIPInfo")
    public R getVIPInfo(@Validated @RequestBody IDParams params) {
        UserMemberCardVO data = this.service.getVIPInfo(params.getId());
        if (Objects.nonNull(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }
}

