package com.elite.learn.controller.systemManage;

import com.elite.learn.common.cacheKeyUtils.LoginAccountCacheKeyUtils;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.tree.NodeTree;
import com.elite.learn.common.utils.ObjectUtils;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IAccountServiceBLL;
import com.elite.learn.systemManage.dto.AccountDTO;
import com.elite.learn.systemManage.entity.Account;
import com.elite.learn.systemManage.params.AccountParams;
import com.elite.learn.systemManage.params.ResetPasswordParams;
import io.jsonwebtoken.Claims;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 系统管理-管理员表 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2020-11-24
 */
@RestController
@RequestMapping("/v1/sys/account")
public class AccountController extends BaseController {

    @Resource
    private IAccountServiceBLL service;




    /**
     * 添加管理员
     *
     * @param params
     * @return
     */
    @PostMapping("/save")
    @PreAuthorize("hasAuthority('v1:sys:account:save')")
    public R save(@RequestBody Account params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        if (ObjectUtils.allfieldIsNUll(params)) {
            return R.error(BasePojectExceptionCodeEnum.ParamsError);
        }
        params.setCreateOperatorName(claim.getSubject());
        String data = this.service.save(claim.getId(), params);
        if (StringUtil.isNotEmpty(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }




    /**
     * 删除管理员
     *
     * @param params
     * @return
     */
    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('v1:sys:account:delete')")
    public R delete(@RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        if (ObjectUtils.allfieldIsNUll(params.getIds())) {
            return R.error(BasePojectExceptionCodeEnum.ParamsError);
        }
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }



    /**
     * 修改管理员信息
     *
     * @param params
     * @return
     */
    @PostMapping("/update")
    @PreAuthorize("hasAuthority('v1:sys:account:update')")
    public R update(@RequestBody Account params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        if (ObjectUtils.allfieldIsNUll(params)) {
            return R.error(BasePojectExceptionCodeEnum.ParamsError);
        }
        params.setUpdateOperatorName(claim.getSubject());
        params.setUpdateOperatorID(claim.getId());
        boolean flag = this.service.update(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }




    /**
     * 查看管理员详情
     *
     * @param params
     * @return
     */
    @PostMapping("/getInfo")
    @PreAuthorize("hasAuthority('v1:sys:account:list')")
    public R getInfo(@RequestBody IDParams params) {
        if (ObjectUtils.allfieldIsNUll(params)) {
            return R.error(BasePojectExceptionCodeEnum.ParamsError);
        }
        if (!StringUtil.isNotEmpty(params.getId())) {
            return R.error(BasePojectExceptionCodeEnum.ParamsError);
        }
        AccountDTO result = this.service.getInfo(params);
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }



    /**
     * 管理员分页
     *
     * @param params
     * @return
     */

    @PostMapping("/getList")
    @PreAuthorize("hasAuthority('v1:sys:account:list')")
    public R getList(@RequestBody AccountParams params) {
        PageResult<AccountDTO> result = null;
        result = this.service.getList(params);
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }



    /**
     * @Description: TODO
     * @author: leroy
     * @data: 2020/4/30 13:29
     * @version: V1.0
     * @url: 修改管理员状态是否启用不启用
     * @param:
     */
    @PostMapping("/updateState")
    @PreAuthorize("hasAuthority('v1:sys:account:update')")
    public R updateState(@RequestBody AccountParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setUpdateOperatorName(claim.getSubject());
        params.setUpdateOperatorID(claim.getId());
        boolean flag = this.service.updateState(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        } else {
            return R.error(BasePojectExceptionCodeEnum.UpdateFail);
        }
    }




    /**
     * @param @param  params
     * @param @return    入参
     * @return R    返回类型
     * @Title: resetPassword
     * @Description: TODO(修改密码用户输入旧密码换取新的密码) 
     * @author: leroy    
     * @date 2020年3月13日 上午11:31:10 
     * @version V1.0   
     */
    @RequestMapping(value = "/resetPassword")
    public R resetPassword(@Validated @RequestBody ResetPasswordParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setUpdateOperatorName(claim.getSubject());
        params.setUpdateOperatorID(claim.getId());
        boolean flag = this.service.updatePassword(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        } else {
            return R.error(BasePojectExceptionCodeEnum.UpdateFail);
        }
    }



    /**
     * 前端用户获取权限节点
     *
     * @return
     */
    @PostMapping("/getMenuList")
    public R getMenuList() {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        //如果是超管的话就看到所有的
        List<NodeTree> infoList = this.service.getMenuList(claim.getId());
        if (Objects.nonNull(infoList)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, infoList);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * @Description: TODO
     * @author: leroy
     * @data: 2020/4/30 13:29
     * @version: V1.0
     * @url: 管理员详情接口
     * @param:
     */
    @PostMapping("/getAdministratorInfo")
    public R getAdministratorInfo() {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        IDParams params = new IDParams();
        params.setId(claim.getId());
        AccountDTO result = this.service.getInfo(params);
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);

    }




    /**
     * 生产token   
     */
    protected Map<String, Object> getLoginMap(AccountDTO user) {
        AccountDTO dto = new AccountDTO();
        BeanUtils.copyProperties(user, dto);

        Map<String, Object> claims = new HashMap<String, Object>();
        // 登录名
        claims.put("ID", user.getId().trim());
        // 登录名
        claims.put("LoginName", user.getLoginName().trim());
        // 性别 0:未知 1:男 2:女
        claims.put("Sex", user.getSex());
        // 用户真实姓名
        claims.put("AccountName", user.getAccountName());
        // 用户真实姓名
        claims.put("DepartmentName", user.getDepartmentName());

        // 邮箱
        claims.put("Email", user.getEmail());

        // 头像路径
        claims.put("Photo", user.getPhoto());


        if (StringUtil.isNotEmpty(user.getPhoto())) {
            // 头像路径
            claims.put("PhotoPath", FileUploadUtil.getImgRootPath() + user.getPhoto());
        } else {
            claims.put("PhotoPath", null);
        }

        // 员工编号
        claims.put("AccountCode", user.getAccountCode());
        // 手机
        claims.put("Phone", user.getPhone());
        // 地址
        claims.put("Address", user.getAddress());
        Long loginTime = System.currentTimeMillis();
        // 登录时间
        claims.put("LoginTime", loginTime);
        //dto.setLoginTime(loginTime);
        //claims.put("uuid", Md5Util.getInstance().getLongToken(user.getId().trim() + "" + loginTime));
        // serviceToken有效期8个小时
       // String serviceToken = JwtHelperUtil.sign(dto, 120 * 60 * 60 * 1000);
        //保存redis 有效时8小时
        String key = LoginAccountCacheKeyUtils.getKey(user.getId().trim());
        //redisUtils.set(key, serviceToken, 8 * 60 * 60 * 1000);
        //  System.out.println(redisUtils.get(key));
       // claims.put("serviceToken", serviceToken);
        return claims;
    }

}

