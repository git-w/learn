package com.elite.learn.controller.logManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.result.R;
import com.elite.learn.logManage.bll.IUserPayServiceBLL;
import com.elite.learn.logManage.query.UserPayQuery;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 订单支付记录表 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2021-07-15
 */
@RestController
@RequestMapping("/v1/log/userPay")
public class UserPayController {
    @Resource
    private IUserPayServiceBLL service;

    /**
     * <p>
     * 列表
     * </p>
     *
     * @param params
     * @return com.elite.learn.tools.base.common.rest.response.R
     * @author: leroy
     * @date 2021/7/15 14:19
     */
    @PostMapping("/getList")
    public R getList(@RequestBody UserPayQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));

    }
}

