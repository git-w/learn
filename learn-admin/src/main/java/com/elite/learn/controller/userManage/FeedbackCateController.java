package com.elite.learn.controller.userManage;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IFeedbackCateServiceBLL;
import com.elite.learn.userManage.dto.FeedbackCateDTO;
import com.elite.learn.userManage.query.FeedbackCateQuery;
import com.elite.learn.userManage.vo.FeedbackCateVO;
import io.jsonwebtoken.Claims;
import org.apache.ibatis.annotations.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Description: 用户意见反馈分类表
 * @Package com.elite.learn.controller.userManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/26 15:17
 */
@RestController
@RequestMapping("v1/user/feedbackcate")
public class FeedbackCateController extends BaseController {


    @Resource
    private IFeedbackCateServiceBLL service;


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:33
     * @update
     * @updateTime 添加
     */
    @PostMapping("/save")
    public R save(@Validated @RequestBody FeedbackCateDTO params) {
        /**
         * 获取用户信息
         * 判断返回值是否为空
         * 如果不为空，返回保存成功状态
         */
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setCreateOperatorName(claim.getSubject());
        String data = this.service.save(claim.getId(), params);
        if (StringUtil.isNotEmpty(data)) {

            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);

        }
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }


    /**
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:33
     * @update
     * @updateTime 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody @Validated(Update.class) FeedbackCateDTO bean) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        bean.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.update(claim.getId(), bean);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 删除
     */
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        Claims claim = jwtUtils.getClaimByTokenInfo();

        boolean flag = this.service.remove(claim.getId(), params.getIds());

        //判断boolean
        if (flag) {
            //如果true，返回删除成功状态
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 分页
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody FeedbackCateQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 详情
     */
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        FeedbackCateVO result = this.service.getInfo(params.getId());
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * @param
     * @return null
     * @author: leroy
     * @date 2021/12/6 16:52
     * @update
     * @updateTime
     */
    @PostMapping("/findAll")
    public R findAll(FeedbackCateQuery query) {
        // 判断list是否为空
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.findAll(query));
    }


    /**
     * 批量上下线管理
     *
     * @param params
     * @return
     */
    @PostMapping("/updateMultiIsEnabled")
    public R updateMultiCategory(@RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.updateMultiIsEnabled(claim.getId(), params);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }
}