package com.elite.learn.controller.userManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IGroupServiceBLL;
import com.elite.learn.userManage.dto.GroupDTO;
import com.elite.learn.userManage.entity.Group;
import com.elite.learn.userManage.query.GroupQuery;
import com.elite.learn.userManage.vo.GroupVO;
import io.jsonwebtoken.Claims;
import org.apache.ibatis.annotations.Update;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 用户-客户管理--团队客户 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2020-11-24
 */
@RestController
@RequestMapping("v1/user/group")
public class GroupController extends BaseController {
    @Resource
    private IGroupServiceBLL service;


    /**
     * 添加团队客户
     * 权限只有超管有
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasAuthority('v1:user:group:save')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody GroupDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setCreateOperatorName(claim.getSubject());
        String data = this.service.save(claim.getId(), params);
        if (StringUtil.isNotEmpty(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }


    /**
     * 修改团队客户
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasAuthority('v1:user:group:update')")
    @PostMapping("/update")
    public R update(@RequestBody @Validated(Update.class) GroupDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.update(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 详情
     *
     * @param params
     * @return
     */
    @PreAuthorize("hasAuthority('v1:user:group:list')")
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        GroupVO result = this.service.getInfo(params.getId());
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 删除
     *
     * @param params
     * @return
     */
    @PreAuthorize("hasAuthority('v1:user:group:delete')")
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }


    /**
     * 获取团队客户(前端根据权限获取团队客户)
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasAuthority('v1:user:group:list')")
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody GroupQuery params) {
        PageResult<GroupVO> result = this.service.getList(params);
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 获取团队客户(前端根据权限获取团队客户)
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasAuthority('v1:user:group:list')")
    @PostMapping("/findAll")
    public R findAll(@Validated @RequestBody GroupQuery params) {
        List<Group> result = service.findAll2(params);
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }

}

