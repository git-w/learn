package com.elite.learn.controller.userManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IUserAdditionServiceBLL;
import com.elite.learn.userManage.bll.IUserMateSelectionServiceBLL;
import com.elite.learn.userManage.bll.IUserServiceBLL;
import com.elite.learn.userManage.dto.UserAdditionDTO;
import com.elite.learn.userManage.dto.UserDTO;
import com.elite.learn.userManage.dto.UserMateSelectionDTO;
import com.elite.learn.userManage.query.UserQuery;
import com.elite.learn.userManage.vo.UserVO;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import org.apache.ibatis.annotations.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 用户-客户管理 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2020-11-24
 */
@Api(tags = "用户管理")
@RestController
@RequestMapping("v1/user/user")
public class UserController extends BaseController {
    @Resource
    private IUserServiceBLL service;
    @Resource
    private IUserAdditionServiceBLL userAdditionServiceBLL;
    @Resource
    private IUserMateSelectionServiceBLL userMateSelectionServiceBLL;

    /**
     * 添加个人客户
     * 权限只有超管有
     *
     * @param params
     * @return
     * @throws
     */
    @PostMapping("/save")
    public R save(@Validated @RequestBody UserDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        String data = this.service.save(claim.getId(), params);
        if (StringUtil.isNotEmpty(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }


    /**
     * 修改个人客户
     *
     * @param params
     * @return
     * @throws
     */
    @PostMapping("/update")
    public R update(@RequestBody @Validated(Update.class) UserDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.update(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 查询用户详情
     *
     * @param query
     * @return com.elite.learn.common.core.result.R
     * @author: leroy
     * @date 2021/8/13 10:14
     */
    @PostMapping("/getInfo")
    public R getInfo(@RequestBody @Validated IDParams query) {
        UserVO result = this.service.getInfo(query.getId());
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);

    }


    /**
     * 删除
     *
     * @param params
     * @return
     */
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }


    /**
     * 获取个人客户(前端根据权限获取个人客户)
     *
     * @param params
     * @return
     * @throws
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody UserQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }


    /**
     * 批量上下线管理
     *
     * @param params
     * @return
     */
    @PostMapping("/updateMultiIsEnabled")
    public R updateMultiCategory(@RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.updateMultiIsEnabled(claim.getId(), params);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 批量上下线管理
     *
     * @param params
     * @return
     */
    @PostMapping("/updateMultiIsType")
    public R updateMultiIsType(@RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.updateMultiIsType(claim.getId(), params);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 修改用户信息
     *
     * @return
     */
    @RequestMapping(value = "/updateUserAddition", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public R updateUserAddition(@RequestBody UserAdditionDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.userAdditionServiceBLL.update(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 修改用户信息
     *
     * @return
     */
    @RequestMapping(value = "/updateUserMateSelection", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public R updateUserMateSelection(@RequestBody UserMateSelectionDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.userMateSelectionServiceBLL.update(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }
}

