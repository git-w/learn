package com.elite.learn.controller.userManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.userManage.bll.IBlacklistServiceBLL;
import com.elite.learn.userManage.dto.BlacklistDTO;
import com.elite.learn.userManage.query.BlacklistQuery;
import com.elite.learn.userManage.vo.BlacklistVO;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Update;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 用户-客户管理--黑名单 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2020-11-24
 */
@Api(tags = "黑名单管理")
@RestController
@RequestMapping("v1/user/blacklist")
public class BlacklistController extends BaseController {
    @Resource
    private IBlacklistServiceBLL service;


    /**
     * 添加团队客户
     * 权限只有超管有
     *
     * @param params
     * @return
     * @throws
     */
    @ApiOperation("添加用户")
    @PreAuthorize("hasAuthority('v1:user:blacklist:save')")
    @PostMapping("/save")
    public R save(@Validated @RequestBody BlacklistDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setCreateOperatorName(claim.getSubject());
        String data = this.service.save(claim.getId(), params);
        if (StringUtil.isNotEmpty(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }


    /**
     * 修改团队客户
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasAuthority('v1:user:blacklist:update')")
    @PostMapping("/update")
    public R update(@RequestBody @Validated(Update.class) BlacklistDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.update(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 详情
     *
     * @param params
     * @return
     */
    @PreAuthorize("hasAuthority('v1:user:blacklist:list')")
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        BlacklistVO data = this.service.getInfo(params.getId());
        if (Objects.nonNull(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 删除
     *
     * @param params
     * @return
     */
    @PreAuthorize("hasAuthority('v1:user:blacklist:delete')")
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }


    /**
     * 获取团队客户(前端根据权限获取团队客户)
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasAuthority('v1:user:blacklist:list')")
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody BlacklistQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }


}

