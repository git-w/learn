package com.elite.learn.controller.cardManage;

import com.elite.learn.cardManage.bll.IMemberCardConfigServiceBLL;
import com.elite.learn.cardManage.dto.MemberCardConfigDTO;
import com.elite.learn.cardManage.query.MemberCardConfigQuery;
import com.elite.learn.cardManage.vo.MemberCardConfigVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import io.jsonwebtoken.Claims;
import org.apache.ibatis.annotations.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Description: 会员卡配置表
 * @Title: InfoMemberCardConfigController
 * @Package com.elite.learn.controller.userManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/29 17:16
 */

@RestController
@RequestMapping("v1/card/memberCardConfig")
public class MemberCardConfigController extends BaseController {

    @Resource
    private IMemberCardConfigServiceBLL service;


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:33
     * @update
     * @updateTime 添加
     */
    @PostMapping("/save")
    public R save(@Validated @RequestBody MemberCardConfigDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setCreateOperatorName(claim.getSubject());
        String data = this.service.save(claim.getId(), params);
        //判断返回值是否为空
        if (StringUtil.isNotEmpty(data)) {
            //如果不为空，返回保存成功状态
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess);
        }
        //如果为空，返回保存失败状态
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }


    /**
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:33
     * @update
     * @updateTime 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody @Validated(Update.class) MemberCardConfigDTO bean) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        bean.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.update(claim.getId(), bean);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 删除
     */
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        //判断boolean
        if (flag) {
            //如果true，返回删除成功状态
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }



    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 分页
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody MemberCardConfigQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }



    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 详情
     */
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        MemberCardConfigVO result = this.service.getInfo(params.getId());
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 批量修改会员卡上线线管理
     *
     * @param params
     * @return
     */
    @PostMapping("/updateMultiIsEnabled")
    public R updateMultiCategory(@Validated(Update.class) @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.updateMultiIsEnabled(claim.getId(), params);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }
}