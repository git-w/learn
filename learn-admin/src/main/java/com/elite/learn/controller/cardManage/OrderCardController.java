package com.elite.learn.controller.cardManage;

import com.elite.learn.cardManage.bll.IOrderCardServiceBLL;
import com.elite.learn.cardManage.query.OrderCardQuery;
import com.elite.learn.cardManage.vo.OrderCardVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Description: 会员卡信息表
 * @Title: InfoOrderCardController
 * @Package com.elite.learn.controller.userManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/29 17:09
 */

@RestController
@RequestMapping("v1/card/order")
public class OrderCardController extends BaseController {


    @Resource
    private IOrderCardServiceBLL service;

    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 分页
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody OrderCardQuery params) {
        //是否启用 0-是 1-否
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }



    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 详情
     */
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        OrderCardVO data = this.service.getInfo(params.getId());
        if (Objects.nonNull(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


}