package com.elite.learn.controller.systemManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IImagesCategoryServiceBLL;
import com.elite.learn.systemManage.dto.ImagesCategoryDTO;
import io.jsonwebtoken.Claims;
import org.apache.ibatis.annotations.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 素材库分类表 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2021-01-26
 */
@RestController
@RequestMapping("/v1/sys/imagesCategory")
public class ImagesCategoryController extends BaseController {

    @Resource
    private IImagesCategoryServiceBLL service;




    /**
     * 添加素材库分类
     *
     * @param params
     * @return
     */
    @PostMapping("/save")
    public R save(@Validated @RequestBody ImagesCategoryDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        String data = this.service.save(claim.getId(), params);
        //判断返回值是否为空
        if (StringUtil.isNotEmpty(data)) {
            //如果不为空，返回保存成功状态
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        //如果为空，返回保存失败状态
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }




    /**
     * 删除素材库分类
     *
     * @param params
     * @return
     */
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        //判断boolean
        if (flag) {
            //如果true，返回删除成功状态
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }




    /**
     * 修改素材库分类
     *
     * @param bean
     * @return
     */
    @PostMapping("/update")
    public R update(@RequestBody @Validated(Update.class) ImagesCategoryDTO bean) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.update(claim.getId(), bean);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }




    /**
     * 查询全部素材库分类
     *
     * @return
     */
    @PostMapping("/findAll")
    public R findAll() {
        // 判断list是否为空
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.findAll());
    }
}

