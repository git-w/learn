package com.elite.learn.controller.messageCenterManage;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.messageManage.bll.IMessageServiceBLL;
import com.elite.learn.messageManage.dto.MessageDto;
import com.elite.learn.messageManage.query.MessageQuery;
import io.jsonwebtoken.Claims;
import org.apache.ibatis.annotations.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description: 通告表
 * @Title: MessageController
 * @Package com.elite.learn.controller.messageCenterManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/18 13:56
 */

@RestController
@RequestMapping("v1/messageCenter/message")
public class MessageController extends BaseController {


    @Resource
    private IMessageServiceBLL service;



    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:33
     * @update
     * @updateTime 更新
     */
    @PostMapping("/update")
    public R update( @Validated(Update.class)  @RequestBody MessageDto params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();

        boolean flag = this.service.update(claim.getId(), params);

        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);

    }



    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:33
     * @update
     * @updateTime 保存
     */
    @PostMapping("/save")
    public R save(@Validated @RequestBody MessageDto params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setCreateOperatorId(params.getId());
        String data = this.service.save(claim.getId(), params);
        //判断返回值是否为空
        if (StringUtil.isNotEmpty(data)) {
            //如果不为空，返回保存成功状态

            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        //如果为空，返回保存失败状态

        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }



    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 分页
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody MessageQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }



}