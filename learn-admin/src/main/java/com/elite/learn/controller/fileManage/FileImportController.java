package com.elite.learn.controller.fileManage;

import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @Title  :FileImportController.java
 * @Package: com.controller
 * @Description: 模板下载
 * @author: leroy
 * @data: 2019-5-12下午10:12:46
 */
@RestController
@SuppressWarnings("resource")
public class FileImportController {/*
	@Resource
	private BoProductCateService boProductCateService;
	@Resource
	private BoProductService boProductService;
	@Resource
	private BoProductTagService boProductTagService;
	
	private static final Logger log = Logger.getLogger(FileImportController.class);

	*//**
	 * 用户下载表格模板
	 * 
	 * @param request
	 * @param response
	 * @return wp
	 * @throws Exception
	 *//*
	@SuppressWarnings("deprecation")
	@RequestMapping("/messages/download-excel")
	public String xiazai(HttpServletRequest request, HttpServletResponse response) {
		// 创建工作手册
		HSSFWorkbook workbook = new HSSFWorkbook();
		// 创建sheet
		HSSFSheet sheet = workbook.createSheet("敏感词信息");
		//设置边框:
//		HSSFCellStyle setBorder = workbook.createCellStyle();
//		setBorder.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
//		setBorder.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
//		setBorder.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
//		setBorder.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
//		//设置居中:
//		setBorder.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
		//设置字体:
		HSSFFont font = workbook.createFont();
		font.setFontName("黑体");
		font.setFontHeightInPoints((short) 18);//设置字体大小
		HSSFFont font2 = workbook.createFont();
		font2.setFontName("仿宋_GB2312");
		font2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示
		font2.setFontHeightInPoints((short) 12);
		setBorder.setFont(font);//选择需要用到的字体格式
		//设置自动换行:
		//setBorder.setWrapText(true);//设置自动换行
		// 创建sheet里面第一行表头信息
		HSSFRow rowFirst = sheet.createRow(0);
		sheet.setColumnWidth(0, 20 * 256);
		sheet.setColumnWidth(1, 20 * 256);
		rowFirst.createCell(0).setCellType(HSSFCell.CELL_TYPE_STRING);
		rowFirst.createCell(1).setCellType(HSSFCell.CELL_TYPE_STRING);
		//rowFirst.createCell(2).setCellType(HSSFCell.CELL_TYPE_STRING);
		//rowFirst.createCell(3).setCellType(HSSFCell.CELL_TYPE_STRING);
		rowFirst.createCell(0).setCellValue("敏感词名称");
		rowFirst.createCell(1).setCellValue("敏感词分类");
		//rowFirst.createCell(3).setCellValue("邮箱");
		
		// 把内存中表格下载到本地
		try {
			// 指定下载文件名称
			String filename = "敏感词模板.xls";
			// 下载文件名称有中文，出现乱码问题
			
			 * 火狐浏览器 和 非火狐浏览器（ie和其他） 原理：如果中文，把中文进行编码操作，浏览器进行存储 火狐采用 base64 ie采用
			 * url编码
			 * 实现思想：获取到当前访问浏览器类型 请求信息里面有头信息 User-Agent
			 * 根据不同的浏览器进行不同的编码
			 * 
			 
			String agent = request.getHeader("User-Agent");
			filename = encodeDownloadFilename(filename, agent);
			
			ServletOutputStream out = response.getOutputStream();
			// 下载到excel表格里面设置信息
			response.setContentType("application/vnd.ms-excel");
			// 设置头信息
			response.setHeader("content-Disposition", "attachment;filename=" + filename);
			// 写入表格信息
			workbook.write(out);
		} catch (Exception e) {
			
		}
		
		return null;
	}
	
	public static String encodeDownloadFilename(String filename, String agent) throws IOException {
		if (agent.contains("Firefox")) { //火狐浏览器
			filename = "=?UTF-8?B?" + new Base64().encode(filename.getBytes("utf-8")) + "?=";
		} else { // IE及其他浏览器
			filename = URLEncoder.encode(filename, "utf-8");
			filename = filename.replace("+", " ");
		}
		return filename;
	}
	
	*//**
	 * @Package: com.controller
	 * @Description: TODO
	 * @author: leroy
	 * @data: 2019-4-18上午12:10:26
	 * @version: V1.0
	 * @throws Exception 
	 * @url:品牌批量导入
	 *//*
	@RequestMapping(value = "/brandImport",method = RequestMethod.POST)
	@ResponseBody
	public  R brandImport(@RequestParam(value="filename") MultipartFile file,
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		log.info("BoSensitiveController ......batchimport() start");
		//判断文件是否为空
		if(file==null) return  BusinessException.FILENULL;
		//获取文件名
		String name=file.getOriginalFilename();
		//进一步判断文件是否为空（即判断其大小是否为0或其名称是否为null）
		long size=file.getSize();
		if(name==null || ("").equals(name) && size==0)return  BusinessException.FILENULL;
		InputStream in =null; 
		List<List<Object>> listob = null; 
		in = file.getInputStream(); 
		listob = new ImportExcelUtil().getBankListByExcel(in,file.getOriginalFilename()); 
		in.close(); 
		List<String> strErroor=new ArrayList<String>();
		//该处可调用service相应方法进行数据保存到数据库中，现只对数据输出 
		BoBrand boBrand = new BoBrand();
		for (int i = 0; i < listob.size(); i++) {
			if(i!=0){
				List<Object> lo = listob.get(i); 
				//判断类别是否为空，如果为空则不添加
				if(lo.get(0)!=null&&!"".equals(lo.get(0))&&lo.get(1)!=null&&!"".equals(lo.get(1))&&lo.get(2)!=null&&!"".equals(lo.get(2))){
					//判断分离是否为空
					boBrand.setBrandName(lo.get(0).toString());  // 品牌名称
					boBrand.setId(Integer.valueOf(lo.get(1).toString()));  // 品牌id
					boBrand.setExp1(lo.get(2).toString());  // 产品编号
					boBrand.setShopId(33);  // 绑定店铺id
					boBrandService.addBrand(boBrand);
					
				}else{
					strErroor.add("行"+i+"品牌名称："+lo.get(0)+",品牌编号："+lo.get(1)+",产品编号："+lo.get(2));
				}
			}
			
		}
		log.info("BoSensitiveController ......batchimport() end");
		return new BusinessException().getObject(strErroor);
	}
	
	
	*//**
	 * @Package: com.controller
	 * @Description: TODO
	 * @author: leroy
	 * @data: 2019-4-18上午12:10:26
	 * @version: V1.0
	 * @throws Exception 
	 * @url:分类批量导入
	 *//*
	@RequestMapping(value = "/classifyImport",method = RequestMethod.POST)
	@ResponseBody
	public  R classifyImport(@RequestParam(value="filename") MultipartFile file,
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		log.info("BoSensitiveController ......batchimport() start");
		//判断文件是否为空
		if(file==null) return  BusinessException.FILENULL;;
		//获取文件名
		String name=file.getOriginalFilename();
		//进一步判断文件是否为空（即判断其大小是否为0或其名称是否为null）
		long size=file.getSize();
		if(name==null || ("").equals(name) && size==0) return  BusinessException.FILENULL;
		InputStream in =null; 
		List<List<Object>> listob = null; 
		in = file.getInputStream(); 
		listob = new ImportExcelUtil().getBankListByExcel(in,file.getOriginalFilename()); 
		in.close(); 
		List<String> strErroor=new ArrayList<String>();
		BoProductCate boProductCate = new BoProductCate();
		//该处可调用service相应方法进行数据保存到数据库中，现只对数据输出 
		for (int i = 0; i < listob.size(); i++) {
			if(i!=0){
				List<Object> lo = listob.get(i); 
				//判断类别是否为空，如果为空则不添加
				if(lo.get(0)!=null&&!"".equals(lo.get(0))&&lo.get(1)!=null&&!"".equals(lo.get(1))){
					//判断分离是否为空
					boProductCate.setCateName(lo.get(0).toString()); //分类名称
					boProductCate.setId(Integer.valueOf(lo.get(1).toString()));
					// 排序
					boProductCate.setCateSort(1);
					// 几级分类
					boProductCate.setGrade(1);
					// 分类的父id
					boProductCate.setParCateId(0L);
					// 分类编号
					boProductCate.setCateId(IdUtils.genItemId());
					boProductCateService.addProductCate(boProductCate);
				}else{
					strErroor.add("行"+i+"分类名称："+lo.get(0)+",分类编号："+lo.get(1));
				}
			}
		}
		log.info("BoSensitiveController ......batchimport() end");
		return new BusinessException().getObject(strErroor);
	}
	
	
	
	*//**
	 * @Package: com.controller
	 * @Description: 导入商品
	 * @author: leroy
	 * @data: 2019-4-18上午12:10:26
	 * @version: V1.0
	 * @throws Exception 
	 *//*
	@RequestMapping(value = "/productImport",method = RequestMethod.POST)
	@ResponseBody
	public  R productImport(@RequestParam(value="filename") MultipartFile file,
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		log.info("BoSensitiveController ......productImport() start");
		//判断文件是否为空
		if(file==null) return  BusinessException.FILENULL;
		//获取文件名
		String name=file.getOriginalFilename();
		//进一步判断文件是否为空（即判断其大小是否为0或其名称是否为null）
		long size=file.getSize();
		if(name==null || ("").equals(name) && size==0) return  BusinessException.FILENULL;
		InputStream in =null; 
		List<List<Object>> listob = null; 
		in = file.getInputStream(); 
		listob = new ImportExcelUtil().getBankListByExcel(in,file.getOriginalFilename()); 
		in.close(); 
		List<String> strErroor=new ArrayList<String>();
		//该处可调用service相应方法进行数据保存到数据库中，现只对数据输出 
		for (int i = 0; i < 101; i++) {
			if(i!=0){
				List<Object> lo = listob.get(i); 
				//判断类别是否为空，如果为空则不添加
				if(lo.get(0)!=null&&!"".equals(lo.get(0))&&lo.get(1)!=null&&!"".equals(lo.get(1))){
					
					if(lo.get(0)!=null&&!"".equals(lo.get(0))){
						ProductDetailsWithBLOBs Product=new ProductDetailsWithBLOBs();
					Product.setId(Long.valueOf(lo.get(0).toString()));
					Product.setProductTag(lo.get(2).toString());
				//	productService.updateProductDetails(Product);
					}
					
					BoProductDetailsWithBLOBs product=new  BoProductDetailsWithBLOBs();
					//产品编号
					//店铺id
					product.setShopId(33);
					product.setProductCode(lo.get(2).toString());
					product=boProductService.queryProductDetails(product);
					if(product==null){
						 product=new  BoProductDetailsWithBLOBs();
						product.setProductCode(lo.get(2).toString());
						product.setShopId(33);
					}else if(lo.get(2).toString().equals("SY0003")){
						
					}else{
						if(lo.size()>=8&&lo.get(6).toString()!=null&&!lo.get(6).toString().equals("")&&!lo.get(6).toString().equals("null")){
							SaveProductImg(product.getId(),lo.get(6).toString());//保存商品图片
							}else{
								continue;
							}
						//进入下次循环
						//continue;
					}
					product.setCreateTime(new Date());//添加时间
					product.setProductImg4("0");
					
					
					//分类编号  
					//先去查商品的编号  再把编号set到商品表类别里面
					BoProductCate Cate= boProductCateService.getBoProductCate(Integer.valueOf(lo.get(0).toString()));
					if(Cate!=null){
						//getQueryBoProductCate
						product.setProductCate(Cate.getCateId());
					}
					//品牌编号
					BoBrand boBrand = new BoBrand();
					BoBrand bs =boBrandService.getBrand(Integer.valueOf(lo.get(1).toString()));
					System.out.println(Integer.valueOf(lo.get(1).toString()));
					if(bs!=null){
					boBrand.setExp1(bs.getExp1());  // 品牌id
					boBrand.setShopId(product.getShopId());
					BoBrand boBrand1 =boBrandService.getQueryBrand(boBrand);
					if(boBrand1!=null){
					product.setProductBrand(Long.valueOf(boBrand1.getId()));
					}}
					//市场价
					if(lo.get(3)!=null&&!lo.get(3).toString().equals("")&&!lo.get(3).toString().equals("null")&&!"".equals(lo.get(3).toString().trim())){
						System.out.println("--"+lo.get(3));
						product.setOriginalPrice(new BigDecimal(lo.get(3).toString()));
					}else{
						product.setOriginalPrice(new BigDecimal(0));
					}
					//售价
					if(lo.get(4)!=null&&!lo.get(4).toString().equals("")&&!lo.get(4).toString().equals("null")){
						product.setSortPrice(new BigDecimal(lo.get(4).toString()));
					}else{
						product.setSortPrice(new BigDecimal(0));
					}
					
					
					//产品名称
					if(lo.get(5)!=null&&!lo.get(5).toString().equals("")&&!lo.get(5).toString().equals("null")){
					product.setProductName(lo.get(5).toString());
					}
					
					
					
					//产品图片编号
					
					//产品规格
					
					//产品参数详情
					if(lo.size()>=8&&lo.get(8).toString()!=null&&!lo.get(8).toString().equals("")&&!lo.get(8).toString().equals("null")){
						product.setProductTag(lo.get(8).toString());}
					
					
					
				//	boProductService.SaveProductDetails(product);
					
					if(product.getProductTag()!=null){
					SaveProductTag(product.getId(), product);}
					
					if(lo.size()>=8&&lo.get(6).toString()!=null&&!lo.get(6).toString().equals("")&&!lo.get(6).toString().equals("null")){
					SaveProductImg(product.getId(),lo.get(6).toString());//保存商品图片
					}
					if(product.getConvertScore()!=null){
						product.setPrice(new BigDecimal(product.getConvertScore()));
					}else {
						product.setPrice(new BigDecimal(0));
					}
					
					product.setPhoneDetail(productDetailsPhone);
					productDetailsService.wxSaveProductDetails(product,wxGetAttributeParams(1));
					wxSaveProductDetails(productId);//保存商品详细信息
					wxSaveProductDetails_phone(productId);//保存手机端商品详细信息
					
					wxSaveDrawImg(productId);
					
					
				}else{
					strErroor.add("行"+i+"分类名称："+lo.get(0)+",分类编号："+lo.get(1));
				}
			}
		}
		log.info("BoSensitiveController ......batchimport() end");
		return new BusinessException().getObject(strErroor);
	}
	
	
	*//**7
	 * @Title  : FileImportController.java
	 * @Package: com.controller
	 * @author : leroy
	 * @version: V1.0
	 * @data   : 2019年6月18日上午11:08:31
	 * @Description: 保存商品标签
	 *//*
	private void SaveProductTag(Long productId,BoProductDetails product) throws Exception{
		product.setId(productId);
		boProductTagService.SaveProductTag(product, product.getProductTag());
	}
	
	*//**
	 * 保存商品图片
	 * *//*
	private void SaveProductImg(Long pid,String listImg){
		// 45 * 45 small 小，侧栏、边栏等特殊位置使用
		// 120 * 120 mid 中等，产品列表页使用
		// 300 * 300 big 大，产品详细页使用
		// 800 * 800 large 最大，产品放大查看时使用
		for(int i=0;i<5;i++){
			delProImg(BoProductDetails.getPinfoDir(pid),i);//删除图片
		}
		List<String> result = Arrays.asList(listImg.split("、"));
		if(result!=null&&result.size()>0){
			int i=0;
			for(String Str:result){
				String productImg=null;
				if(Integer.valueOf(Str.trim())>=25){
					productImg="D:\\imgs\\"+Str+".jpg";
				}else{
					productImg="D:\\imgs\\"+Str+".png";
				}
				File file =new File(productImg);
				String filePath = BoProductDetails.getPinfoDir(pid);
				ImageScale.copyItemImgFile(file,filePath,i);
				i++;
			}
		}
	}
	
	
	*//**
	 * 删除图片
	 * *//*
	public void delProImg(String filePath,Integer num){
		File largeFile = new File(filePath+ "/large/pimg-"+num+".jpg");
		largeFile.delete();
		
		File smallFile = new File(filePath+ "/small/pimg-"+num+".jpg");
		smallFile.delete();
		
		File midFile = new File(filePath+ "/mid/pimg-"+num+".jpg");
		midFile.delete();
		
		File bigFile = new File(filePath+ "/big/pimg-"+num+".jpg");
		bigFile.delete();
	}
	

*/}
