package com.elite.learn.controller.logManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.result.R;
import com.elite.learn.logManage.bll.ILogAccountLoginServiceBLL;
import com.elite.learn.logManage.query.LogAccountLoginQuery;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 后台管理员登陆日志 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2020-11-24
 */
@RestController
@RequestMapping("v1/log/accountLogin")
public class LogAccountLoginController {

    @Resource
    private ILogAccountLoginServiceBLL service;


    /**
     * 查看用户登录日志
     *
     * @param params
     * @return
     */
    @PostMapping("/getList")
    //@PreAuthorize("hasAuthority('v1:log:accountLogin:list')")
    public R getList(@RequestBody @Validated LogAccountLoginQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }
}

