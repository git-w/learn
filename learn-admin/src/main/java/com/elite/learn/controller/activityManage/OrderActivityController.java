package com.elite.learn.controller.activityManage;

import com.elite.learn.activityManage.bll.IOrderActivityServiceBLL;
import com.elite.learn.activityManage.dto.ApplicationDrawbackOrderDTO;
import com.elite.learn.activityManage.dto.ConfirmOrderDTO;
import com.elite.learn.activityManage.dto.OrderActivityDTO;
import com.elite.learn.activityManage.query.OrderActivityQuery;
import com.elite.learn.activityManage.vo.OrderActivityVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.type.OrderType;
import com.elite.learn.common.core.result.R;
import io.jsonwebtoken.Claims;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Description: 活动售卖订单表
 * @Title: OrderActivityController
 * @Package com.elite.learn.controller.activityManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/22 13:26
 */
@RestController
@RequestMapping("v1/activity/order")
public class OrderActivityController extends BaseController {


    @Resource
    private IOrderActivityServiceBLL service;


    /**
     * 拒绝退款
     * 订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款 7已退款
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:26
     * @update
     * @updateTime
     */
    @PostMapping("/refuseRefundOrder")
    public R refuseRefundOrder(@RequestBody @Validated ApplicationDrawbackOrderDTO bean) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        OrderActivityDTO params = new OrderActivityDTO();
        params.setId(bean.getId());
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.refuseRefundOrder(claim.getId(), params);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 确定退款
     * 订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款 7已退款
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:26
     * @update
     * @updateTime
     */
    @PostMapping("/confirmRefundOrder")
    public R confirmRefundOrder(@RequestBody @Validated ApplicationDrawbackOrderDTO bean) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        OrderActivityDTO params = new OrderActivityDTO();
        params.setId(bean.getId());
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.confirmRefundOrder(claim.getId(), params);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 确认订单
     * 订单状态 0-其他 1-待付款 2-已付款 3-已取消 4-已完成 6-待退款 7已退款
     *
     * @param bean
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:26
     * @update
     * @updateTime
     */
    @PostMapping("/confirmOrder")
    public R confirmOrder(@RequestBody @Validated ConfirmOrderDTO bean) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        OrderActivityDTO params = new OrderActivityDTO();
        params.setId(bean.getId());
        params.setIsOrderStatus(OrderType.WAITSEND.getType());
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.update(claim.getId(), params);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 详情
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:29
     * @update
     * @updateTime
     */
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        OrderActivityVO result = this.service.getInfo(params.getId());
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);

        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:20
     * @update
     * @updateTime
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody OrderActivityQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }
}