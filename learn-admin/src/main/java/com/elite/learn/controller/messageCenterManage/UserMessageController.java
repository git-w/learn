package com.elite.learn.controller.messageCenterManage;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.result.R;
import com.elite.learn.messageManage.bll.IUserMessageServiceBLL;
import com.elite.learn.messageManage.contants.MessageCenterManageExceptionCodeEnum;
import com.elite.learn.messageManage.dto.UserMessageDto;
import com.elite.learn.messageManage.query.UserMessageQuery;
import io.jsonwebtoken.Claims;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Description: 通告表
 * @Title: MessageController
 * @Package com.elite.learn.controller.messageCenterManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/18 13:56
 */

@RestController
@RequestMapping("v1/messageCenter/userMessage")
public class UserMessageController extends BaseController {


    @Resource
    private IUserMessageServiceBLL service;


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:33
     * @update
     * @updateTime 保存
     */
    @PostMapping("/save")
    public R save(@Validated @RequestBody UserMessageDto params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        String data = this.service.save(claim.getId(), params);
        //判断返回值是否为空
        if (Objects.nonNull(data)) {
            //如果不为空，返回保存成功状态
            return R.ok(MessageCenterManageExceptionCodeEnum.SaveSuccess.code,
                    MessageCenterManageExceptionCodeEnum.SaveSuccess.msg, data);
        }
        //如果为空，返回保存失败状态
        return R.error(MessageCenterManageExceptionCodeEnum.SaveFail.code,
                MessageCenterManageExceptionCodeEnum.SaveFail.msg);
    }


    /**
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 分页
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody UserMessageQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }


}