package com.elite.learn.controller.systemManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.ObjectUtils;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IRoleServiceBLL;
import com.elite.learn.systemManage.dto.RoleDTO;
import com.elite.learn.systemManage.params.RoleParams;
import io.jsonwebtoken.Claims;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 系统管理-角色管理表 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2020-11-24
 */
@RestController
@RequestMapping("v1/sys/role")
public class RoleController extends BaseController {

    @Resource
    private IRoleServiceBLL service;

    /**
     * 添加角色
     *
     * @param params
     * @return
     * @throws
     */
    @PostMapping("/save")
    //@PreAuthorize("hasAuthority('v1:sys:role:save')")
    public R save(@Validated @RequestBody RoleParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        String data = this.service.saveAndMenu(claim.getId(), params);
        if (StringUtil.isNotEmpty(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }


    /**
     * 修改角色
     *
     * @param params
     * @return
     * @throws
     */
    //  @PreAuthorize("hasAuthority('v1:sys:role:update')")
    @PostMapping("/update")
    public R update(@RequestBody RoleParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        if (ObjectUtils.allfieldIsNUll(params)) {

            return R.error(BasePojectExceptionCodeEnum.ParamsError);
        }
        boolean flag = this.service.updateAndMenu(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 详情
     *
     * @param params
     * @return
     */
    // @PreAuthorize("hasAuthority('v1:sys:role:list')")
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        RoleDTO result = this.service.getInfo(params.getId());
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 删除
     *
     * @param params
     * @return
     */
    //  @PreAuthorize("hasAuthority('v1:sys:role:delete')")
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }


    /**
     * @Description: TODO
     * @author: leroy
     * @data: 2020/4/30 13:29
     * @version: V1.0
     * @url: 列表
     * @param:
     */
    @PostMapping("/getList")
    // @PreAuthorize("hasAuthority('v1:sys:role:list')")
    public R getList(@RequestBody RoleParams params) {
        PageResult<RoleDTO> result = this.service.getList(params);
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        } else {
            return R.error(BasePojectExceptionCodeEnum.SelectFail);
        }
    }


    /**
     * 查看所有的角色
     *
     * @param params
     * @return
     */
    @PostMapping("/findAll")
    public R findAll(@RequestBody RoleParams params) {
        List<RoleDTO> result = this.service.findAll(params);
        if (Objects.nonNull(result)) {

            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        } else {
            return R.error(BasePojectExceptionCodeEnum.SelectFail);
        }
    }

}

