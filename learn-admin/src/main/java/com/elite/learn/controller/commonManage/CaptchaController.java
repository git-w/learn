package com.elite.learn.controller.commonManage;


import com.elite.learn.common.cacheKeyUtils.CaptchaKeyUtils;
import com.elite.learn.common.contants.Constants;
import com.elite.learn.common.utils.id.uuid.IdUtils;
import com.elite.learn.common.utils.redis.RedisCache;
import com.google.code.kaptcha.Producer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.BASE64Encoder;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Title :CaptchaController.java
 * @Package: com.controller
 * @Description: 验证码操作处理
 * @author: leroy
 * @data: 2019-5-12下午10:13:24
 */

@Slf4j
@RestController
@RequestMapping(value = "v1/sys/code")
public class CaptchaController {
    @Autowired
    private RedisCache redisCache;
    @Resource
    private Producer producer;

    /**
     * @Title :getVerifiCodeController.java
     * @Package: com.controller
     * @author: leroy
     * @data: 2019-4-26上午10:40:55
     * @version: V1.0
     * @url: 生成验证码
     */
    @PostMapping(value = "/captcha")
    public Map<String, String> getVerifiCode() throws IOException {
        // 生成文字验证码
        String text = producer.createText();
        // 生成图片验证码
        ByteArrayOutputStream outputStream = null;
        BufferedImage image = producer.createImage(text);

        outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        //获取uuid唯一标识
        String uuid = IdUtils.simpleUUID();
        //创建map对象
        Map<String, String> map = new HashMap<>();
        //把值和验证码保存到redis
        map.put("token", uuid);
        map.put("images", encoder.encode(outputStream.toByteArray()));
        //保存redis 有效时5分钟
        redisCache.setCacheObject(CaptchaKeyUtils.getKey(uuid), text, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        return map;
    }

}
