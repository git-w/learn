package com.elite.learn.controller.systemManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.tree.NodeTree;
import com.elite.learn.common.utils.ObjectUtils;
import com.elite.learn.common.utils.file.FileUploadUtil;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IAccountServiceBLL;
import com.elite.learn.systemManage.bll.IMenuServiceBLL;
import com.elite.learn.systemManage.dto.MenuDTO;
import com.elite.learn.systemManage.entity.Account;
import com.elite.learn.systemManage.entity.Menu;
import com.elite.learn.systemManage.params.MenuParams;
import io.jsonwebtoken.Claims;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 系统管理-菜单表 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2020-11-24
 */
@RestController
@RequestMapping("v1/sys/menu")
public class MenuController extends BaseController {
    @Resource
    private IMenuServiceBLL service;
    @Resource
    private IAccountServiceBLL accountServiceBLL;


    /**
     * 添加菜单
     * 权限只有超管有
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/save")
    public R save(@RequestBody Menu params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        if (ObjectUtils.allfieldIsNUll(params)) {
            return R.error(BasePojectExceptionCodeEnum.ParamsError);

        }
        params.setCreateOperatorName(claim.getSubject());
        String data = this.service.save(claim.getId(), params);
        if (StringUtil.isNotEmpty(data)) {
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }


    /**
     * 修改菜单
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/update")
    public R update(@RequestBody Menu params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        if (ObjectUtils.allfieldIsNUll(params)) {
            return R.error(BasePojectExceptionCodeEnum.ParamsError);

        }
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.update(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 获取菜单(角色设置菜单权限时，获取全量菜单)
     *
     * @param params
     * @return
     * @throws
     */
    @PostMapping("/getTree")
    public R getTree(@RequestBody MenuParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        Account account = accountServiceBLL.get(claim.getId());
        if (Objects.isNull(account)) {
            R.loginError(BasePojectExceptionCodeEnum.LOGINERROR);
        }
        List<NodeTree> result = null;
        //  管理员类型 0:超级管理员 1普通管理员
        if (null != account.getType() && account.getType() == 0) {
            result = this.service.getTree(params);
        } else {
            // 查看用户角色权限是否为空
            if (!StringUtil.isNotEmpty(account.getRoleID())) {
                return R.error(BasePojectExceptionCodeEnum.SelectFail.code, BasePojectExceptionCodeEnum.SelectFail.msg);
            }
            params.setRoleId(account.getRoleID());
            //分开数据 查看是否超级管理员 如果不是超级管理员则分来数据查询
            result = this.service.getAccountTree(params);
        }
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 详情
     *
     * @param params
     * @return
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        Menu bean = this.service.get(params.getId());
        if (Objects.nonNull(bean)) {
            MenuDTO dto = new MenuDTO();
            dto.setId(bean.getId());
            dto.setMenuName(bean.getMenuName());
            dto.setMenuCode(bean.getMenuCode());//菜单或按钮icon
            dto.setMenuLevel(bean.getMenuLevel());//菜单层级 1:一级菜单 2:二级菜单 依次类推...
            dto.setParentCode(bean.getParentCode());//父菜单标识
            dto.setParentID(bean.getParentID());//父菜单ID
            dto.setSort(bean.getSort());//排序号
            dto.setMenuType(bean.getMenuType());//类型 0-菜单 1-按钮
            dto.setButtonType(bean.getButtonType());//按钮区分 1：增改 2：查询 3：删除
            dto.setIsState(bean.getIsState());//是否启用 0是 1否
            dto.setUrl(bean.getUrl());
            dto.setExtraParams1(bean.getExtraParams1());
            dto.setExtraParams2(bean.getExtraParams2());
            dto.setIcon(bean.getIcon());//菜单标识
            if (StringUtil.isNotEmpty(bean.getIcon())) {
                dto.setIconPath(FileUploadUtil.getImgRootPath() + bean.getIcon());
            }
            dto.setMemo(bean.getMemo());
            dto.setPagePath(bean.getPagePath());
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, dto);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 删除
     *
     * @param params
     * @return
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }


    /**
     * 修改菜单状态
     * isState   是否启用 0-是 1-否
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/updateMultipleState")
    public R updateMultipleState(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setUpdateOperatorName(claim.getSubject());
        boolean flag = this.service.updateMultiState(claim.getId(), params);
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * 获取菜单(前端根据权限获取菜单)
     *
     * @param params
     * @return
     * @throws
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/getMenuList")
    public R getMenuList(@RequestBody MenuParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        List<NodeTree> result = null;
        result = this.service.getMenuList(claim.getId(), params);
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


}

