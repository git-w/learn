package com.elite.learn.controller.activityManage;

import com.elite.learn.activityManage.bll.IActivityOrderTimeServiceBLL;
import com.elite.learn.activityManage.query.ActivityOrderTimeQuery;
import com.elite.learn.activityManage.vo.ActivityOrderTimeVO;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.params.IDParams;
import com.elite.learn.common.core.result.R;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Description: 活动订单时间表
 * @Title: ActivityOrderTimeController
 * @Package com.elite.learn.controller.activityManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/12/22 11:33
 */
@RestController
@RequestMapping("v1/activity/activityOrderTime")
public class ActivityOrderTimeController extends BaseController {

    @Resource
    private IActivityOrderTimeServiceBLL service;


    /**
     * 详情
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:29
     * @update
     * @updateTime
     */
    @PostMapping("/getInfo")
    public R getInfo(@Validated @RequestBody IDParams params) {
        ActivityOrderTimeVO result = this.service.getInfo(params.getId());
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);

        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }


    /**
     * 分页
     *
     * @param params
     * @return null
     * @author: leroy
     * @date 2021/12/22 11:20
     * @update
     * @updateTime
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody ActivityOrderTimeQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));
    }


}