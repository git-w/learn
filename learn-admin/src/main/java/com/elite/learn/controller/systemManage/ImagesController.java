package com.elite.learn.controller.systemManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.page.PageResult;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.string.StringUtil;
import com.elite.learn.systemManage.bll.IImagesServiceBLL;
import com.elite.learn.systemManage.dto.ImagesDTO;
import com.elite.learn.systemManage.dto.ImagesListDto;
import com.elite.learn.systemManage.dto.ImagesUpdateDTO;
import com.elite.learn.systemManage.query.ImagesQuery;
import com.elite.learn.systemManage.vo.ImagesVO;
import io.jsonwebtoken.Claims;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 图片 前端控制器
 * </p>
 *
 * @author: leroy
 * @since 2021-01-26
 */
@RestController
@RequestMapping("/v1/sys/images")
public class ImagesController extends BaseController {

    @Resource
    private IImagesServiceBLL service;




    /**
     * 添加素材库图
     *
     * @param params
     * @return
     */
    @PostMapping("/save")
    public R save(@Validated @RequestBody ImagesDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        String data = this.service.save(claim.getId(), params);
        //判断返回值是否为空
        if (StringUtil.isNotEmpty(data)) {
            //如果不为空，返回保存成功状态
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess, data);
        }
        //如果为空，返回保存失败状态
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }




    /**
     * 批量插入
     *
     * @param
     * @return
     */
    @PostMapping("/saveBatch")
    public R saveBatch(@Validated @RequestBody ImagesListDto list) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.saveBatch(claim.getId(), list.getImageList());
        //判断返回值是否为空
        if (flag) {
            //如果不为空，返回保存成功状态
            return R.ok(BasePojectExceptionCodeEnum.SaveSuccess);
        }
        //如果为空，返回保存失败状态
        return R.error(BasePojectExceptionCodeEnum.SaveFail);
    }




    /**
     * 删除素材库
     *
     * @param params
     * @return
     */
    @PostMapping("/delete")
    public R delete(@Validated @RequestBody BaseParams params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.remove(claim.getId(), params.getIds());
        //判断boolean
        if (flag) {
            //如果true，返回删除成功状态
            R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }




    /**
     * 修改素材库图片
     *
     * @param bean
     * @return
     */
    @PostMapping("/update")
    public R update(@RequestBody ImagesDTO bean) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.update(claim.getId(), bean);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }




    /**
     * 批量修改素材库图片的分类
     *
     * @param params
     * @return
     */
    @PostMapping("/updateMultiCategory")
    public R updateMultiCategory(@Validated @RequestBody ImagesUpdateDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        boolean flag = this.service.updateMultiCategory(claim.getId(), params);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }




    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody ImagesQuery params) {
        PageResult<ImagesVO> result = this.service.getList(params);
        if (Objects.nonNull(result)) {
            return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, result);
        }
        return R.error(BasePojectExceptionCodeEnum.SelectFail);
    }
}

