package com.elite.learn.controller.systemManage;

import com.elite.learn.common.core.result.R;
import com.elite.learn.systemManage.bll.IAccountServiceBLL;
import com.elite.learn.systemManage.params.LoginParams;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 *
 * @Title  :LoginController.java  
 * @Package: com.elite.learn.controller.systemManage
 * @Description:  <p>登录验证</p>
 * @update:
 * @author: ；leroy  
 * @data: 2023年03月26日 16:02
 */
@RestController
public class LoginController {
    @Resource
    private IAccountServiceBLL service;

    /**
     * 用户登录
     *
     * @param params
     * @return
     */
    @PostMapping("/login")
    public R login(@RequestBody LoginParams params) {
        //查看用户是否存在
        String  token = service.login(params);
        return R.ok("登录成功", token);
    }
}