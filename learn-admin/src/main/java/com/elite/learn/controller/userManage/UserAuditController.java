package com.elite.learn.controller.userManage;

import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.controller.BaseController;
import com.elite.learn.common.core.result.R;
import com.elite.learn.userManage.bll.IUserAuditServiceBLL;
import com.elite.learn.userManage.dto.UserAuditDTO;
import com.elite.learn.userManage.query.UserAuditQuery;
import io.jsonwebtoken.Claims;
import org.apache.ibatis.annotations.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description: 举报反馈表
 * @Title: InfoUserAuditController
 * @Package com.elite.learn.controller.userManage
 * @author: leroy
 * @Copyright 版权归elite所有
 * @CreateTime: 2021/11/26 15:24
 */


@RestController
@RequestMapping("v1/user/userAudit")
public class UserAuditController extends BaseController {
    @Resource
    private IUserAuditServiceBLL service;


    /**
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 分页
     */
    @PostMapping("/getList")
    public R getList(@Validated @RequestBody UserAuditQuery params) {
        return R.ok(BasePojectExceptionCodeEnum.SelectSuccess, this.service.getList(params));

    }



    /**
     * @author: leroy
     * @date 2021/11/26 15:32
     * @update
     * @updateTime 分页
     */
    @PostMapping("/updateIsAudit")
    public R updateIsAudit(@Validated(Update.class) @RequestBody UserAuditDTO params) {
        // 获取用户信息
        Claims claim = jwtUtils.getClaimByTokenInfo();
        params.setAuditOperatorName(claim.getSubject());
        boolean flag = this.service.updateIsAudit(claim.getId(), params);
        //判断boolean
        if (flag) {
            //如果true，返回修改成功状态
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess, flag);
        }
        //如果false，返回修改失败状态
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


}