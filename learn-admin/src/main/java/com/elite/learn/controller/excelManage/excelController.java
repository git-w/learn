package com.elite.learn.controller.excelManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.result.R;
import com.elite.learn.common.utils.ip.RequestUtil;
import com.elite.learn.excelManage.util.ExcelUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description: 考试考试任务Controller
 * @author: leroy
 * @data: 2020/9/21
 * @version: V1.0
 * @url:
 * @param:
 */
@RestController
@RequestMapping("v1/excel")
public class excelController {

    @Resource
    private HttpServletRequest request;


    private static final Logger log = LogManager.getLogger(excelController.class);

    /**
     * 导入
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/readExcel", method = RequestMethod.POST)
    public R readExcel(@RequestParam("file") MultipartFile file, @RequestParam String companyID, @RequestParam String departmentID, @RequestParam String workIds) {
        boolean flag = true;
        String name = file.getOriginalFilename();
        if (name.length() < 6 || !name.substring(name.length() - 5).equals(".xlsx")) {
            List<Object> li = new ArrayList<>();
            li.add("文件格式错误");
            return R.error(BasePojectExceptionCodeEnum.UnitFileFormatError);
        }
        //文件是否为空
        if (file.isEmpty()) {
            return R.error(BasePojectExceptionCodeEnum.FileParamsError);

        }
        try {
            InputStream inputStream = file.getInputStream();
            List<List<Object>> list = ExcelUtils.getCourseListByExcel(inputStream, file.getOriginalFilename());
            inputStream.close();


            String ip = RequestUtil.getIP(request);

        } catch (Exception e) {
            return R.error(BasePojectExceptionCodeEnum.FileUploadingFfailed);
        }
        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.FileUploadingSuccess);

        } else {
            return R.error(BasePojectExceptionCodeEnum.FileUploadingSectionError);

        }

    }


    //解析Excel日期格式
    public static String ExcelDoubleToDate(String strDate) {
        if (strDate.length() == 5) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date tDate = DoubleToDate(Double.parseDouble(strDate));
                return sdf.format(tDate);
            } catch (Exception e) {
                e.printStackTrace();
                return strDate;
            }
        }
        return strDate;
    }

    //解析Excel日期格式
    public static Date DoubleToDate(Double dVal) {
        Date tDate = new Date();
        long localOffset = tDate.getTimezoneOffset() * 60000; //系统时区偏移 1900/1/1 到 1970/1/1 的 25569 天
        tDate.setTime((long) ((dVal - 25569) * 24 * 3600 * 1000 + localOffset));

        return tDate;
    }


}
