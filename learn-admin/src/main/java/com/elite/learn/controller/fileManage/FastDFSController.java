package com.elite.learn.controller.fileManage;


import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.params.BaseParams;
import com.elite.learn.common.core.result.R;
import  com.elite.learn.common.core.exception.CommonException;
import com.elite.learn.fileManage.bll.impl.FileServiceBLL;
import com.elite.learn.fileManage.contants.FileExceptionCodeEnum;
import com.elite.learn.fileManage.entity.FileSuffix;
import com.elite.learn.fileManage.utils.FileUtils;
import com.elite.learn.fileManage.vo.FileVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

//import com.artofsolving.jodconverter.DocumentConverter;
//import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
//import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
//import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;

/**
 * @Title :FastDFSController.java
 * @Package: com.elite.train.controller.systemManage
 * @Description:
 * @author: leroy
 * @data: 2020/5/26 11:48
 */
@Slf4j
@RestController
@RequestMapping("v1/file/fastDFS")
public class FastDFSController {


    @Resource
    private FileServiceBLL service;

    /**
     * 根据地址获得数据的字节流
     *
     * @param strUrl 网络连接地址
     * @return
     */
    public static InputStream getImageFromNetByUrl(String strUrl) {
        try {
            URL url = new URL(strUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5 * 1000);
            InputStream inStream = conn.getInputStream();//通过输入流获取图片数据
            //byte[] btImg = readInputStream(inStream);//得到图片的二进制数据
            return inStream;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 转换文件成pdf
     *
     * @param fromFileInputStream:
     * @throws IOException
     */
//    public String file2pdf(InputStream fromFileInputStream, String toFilePath,String type) throws IOException {
//        Date date = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//        String timesuffix = sdf.format(date);
//        String docFileName = null;
//        String htmFileName = null;
//        if("doc".equals(type)){
//            docFileName = "doc_" + timesuffix + ".doc";
//            htmFileName = "doc_" + timesuffix + ".pdf";
//        }else if("docx".equals(type)){
//            docFileName = "docx_" + timesuffix + ".docx";
//            htmFileName = "docx_" + timesuffix + ".pdf";
//        }else if("xls".equals(type)){
//            docFileName = "xls_" + timesuffix + ".xls";
//            htmFileName = "xls_" + timesuffix + ".pdf";
//        }else if("ppt".equals(type)){
//            docFileName = "ppt_" + timesuffix + ".ppt";
//            htmFileName = "ppt_" + timesuffix + ".pdf";
//        }else{
//            return null;
//        }
//
//        File htmlOutputFile = new File(toFilePath + File.separatorChar + htmFileName);
//        File docInputFile = new File(toFilePath + File.separatorChar + docFileName);
//        if (htmlOutputFile.exists())
//            htmlOutputFile.delete();
//        htmlOutputFile.createNewFile();
//        if (docInputFile.exists())
//            docInputFile.delete();
//        docInputFile.createNewFile();
//        /**
//         * 由fromFileInputStream构建输入文件
//         */
//        try {
//            OutputStream os = new FileOutputStream(docInputFile);
//            int bytesRead = 0;
//            byte[] buffer = new byte[1024 * 8];
//            while ((bytesRead = fromFileInputStream.read(buffer)) != -1) {
//                os.write(buffer, 0, bytesRead);
//            }
//
//            os.close();
//            fromFileInputStream.close();
//        } catch (IOException e) {
//        }
//
//        OpenOfficeConnection connection = new SocketOpenOfficeConnection("8.140.167.46",8100);
//        try {
//            connection.connect();
//        } catch (ConnectException e) {
//            System.err.println("文件转换出错，请检查OpenOffice服务是否启动。");
//        }
//        // convert
//        DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
//        converter.convert(docInputFile, htmlOutputFile);
//        connection.disconnect();
//        // 转换完之后删除word文件
//        docInputFile.delete();
//        return htmFileName;
//    }

    //在线预览
//    @RequestMapping(value = "/preview2",method = RequestMethod.GET)
//    public void findPdf( HttpServletResponse response, String filePath) throws IOException{
//        response.setContentType("application/pdf");
//        //filePath = "http://8.140.167.46:30004/group1/M00/00/1F/rBwdg2FRkw2AW20UADa7PeMmJEs922.pdf";
//        //FileInputStream in = (FileInputStream)this.getImageFromNetByUrl(filePath);
//        FileInputStream in = new FileInputStream(new File("H:\\"+filePath));
//        OutputStream out = response.getOutputStream();
//        byte[] b = new byte[512];
//        while ((in.read(b))!=-1) {
//            out.write(b);
//        }
//        out.flush();
//        in.close();
//        out.close();
//    }

    /**
     * @param file: 上次文件
     * @Description: 单个图片上传
     * @author: leroy
     * @Date: 2020/11/27 17:42
     * @return: 成功返回上传成功状态码+FileDTO类,否则返回上传失败状态码
     **/
    @RequestMapping(value = "/uploadImage")
    public R uploadImage(@RequestPart("file") MultipartFile file) {
        //获取文件扩展名
        String filenameExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        //判断是否为图片类型
        if (!(FileSuffix.IMAGE_SUFFIX.indexOf(filenameExtension) > -1)) {
            throw new CommonException(FileExceptionCodeEnum.FileTypeError.code, FileExceptionCodeEnum.FileTypeError.msg);
        }

        FileVO vo = service.uploadFile(file);

        if (Objects.nonNull(vo)) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess, vo);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }

    /**
     * @param file:
     * @Description: 单个视频音频上传
     * @author: leroy
     * @Date: 2020/11/27 17:42
     * @return: 成功返回上传成功状态码+FileDTO类,否则返回上传失败状态码
     **/
    @RequestMapping(value = "/uploadVideo")
    public R uploadVideo(@RequestPart("file") MultipartFile file) {
        //获取文件扩展名
        String filenameExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        //判断是否为视频音频类型
        if (!(FileSuffix.VIDEO_SUFFIX.indexOf(filenameExtension) > -1)) {
            throw new CommonException(FileExceptionCodeEnum.FileTypeError.code, FileExceptionCodeEnum.FileTypeError.msg);
        }

        FileVO vo = service.uploadFile(file);

        if (Objects.nonNull(vo)) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess, vo);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }

    /**
     * @param file:
     * @Description: 单个文本文档上传
     * @author: leroy
     * @Date: 2020/11/27 17:42
     * @return: 成功返回上传成功状态码+FileDTO类,否则返回上传失败状态码
     **/
    @RequestMapping(value = "/uploadDocument")
    public R uploadDocument(@RequestPart("file") MultipartFile file) {

        //获取文件扩展名
        String filenameExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        //判断是否为文本文档类型
        if (!(FileSuffix.DOCUMENT_SUFFIX.indexOf(filenameExtension) > -1)) {
            throw new CommonException(FileExceptionCodeEnum.FileTypeError.code, FileExceptionCodeEnum.FileTypeError.msg);
        }

        FileVO vo = service.uploadFile(file);

        if (Objects.nonNull(vo)) {
            return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess, vo);
        }
        return R.error(BasePojectExceptionCodeEnum.UpdateFail);
    }


    /**
     * @param params: 多个文件短路径,中间用逗号分隔
     * @Description: 单个文件删除(图片 、 视频音频 、 文本文档)
     * @author: leroy
     * @Date: 2020/11/27 14:45
     * @return: 成功返回删除成功状态码, 否则返回删除失败状态码
     **/
    @RequestMapping(value = "/deleteFile")
    public R deleteFile(@RequestBody BaseParams params) {
        //获取删除的ids
        String ids = params.getIds();

        boolean flag = FileUtils.deleteFile(ids);

        if (flag) {
            return R.ok(BasePojectExceptionCodeEnum.DeleteSuccess);
        }
        return R.error(BasePojectExceptionCodeEnum.DeleteFail);
    }


    /**
     * @param files:
     * @Description: 多张图片上传（限制9张图片）
     * @author: leroy
     * @Date: 2020/11/30 10:24
     * @return: 成功返回上传成功状态码+FileDTO类集合,否则返回上传失败状态码
     **/
    @RequestMapping(value = "/uploadMultiFile")
    public R uploadMultiFile(@RequestPart("files") MultipartFile[] files) {

        //判断上传的图片数量是否大于9个
        if (files.length > 9) {
            return R.error(FileExceptionCodeEnum.ParamsBigError.code, FileExceptionCodeEnum.ParamsBigError.msg);
        }
        //新创建一个listDto用于存储查询回来的对象
        List<FileVO> listVo = service.uploadMultiFile(files);

        return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess, listVo);
    }

    /**
     * @param files:
     * @Description: 多张图片上传（限制9张图片）--添加水印
     * @author: leroy
     * @Date: 2020/11/30 10:24
     * @return: 成功返回上传成功状态码+FileDTO类集合,否则返回上传失败状态码
     **/
    @RequestMapping(value = "/uploadMultiFilePrint")
    public R uploadMultiFilePrint(@RequestPart("files") MultipartFile[] files, @RequestPart("print") String print, @RequestPart("isFlag") String isFlag) {

        //判断上传的图片数量是否大于9个
        if (files.length > 9) {
            return R.error(FileExceptionCodeEnum.ParamsBigError.code, FileExceptionCodeEnum.ParamsBigError.msg);
        }

        if ("0".equals(isFlag)) {//不加水印
            print = "";
        }
        try {
            print = URLDecoder.decode(print, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //System.out.println("222222222222222222:"+print);
        //新创建一个listDto用于存储查询回来的对象
        List<FileVO> listVo = service.uploadMultiFile2(files, print);

        return R.ok(BasePojectExceptionCodeEnum.UpdateSuccess, listVo);
    }

}
