package com.elite.learn.controller.systemManage;

import com.alibaba.fastjson.JSONObject;
import com.elite.learn.common.enums.BasePojectExceptionCodeEnum;
import com.elite.learn.common.core.result.R;
import com.elite.learn.systemManage.bll.IDictionaryServiceBLL;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("v1/sys/dictionary")
public class DictionaryController  {
	@Resource
	private IDictionaryServiceBLL dictionaryService;

	/**
	 * 获取数据字典信息通过父编码
	 * @return
	 */
	@PostMapping("/getDictionaryInfoByParentCode")
	public R getDictionaryInfoByParentCode(@RequestBody JSONObject json){
		R result = new R();
		try {
			result.setData(this.dictionaryService.getDictionaryInfoByParentCode(json.getString("parentKey"))); 
		}catch(Exception e) {
			return R.error(BasePojectExceptionCodeEnum.SelectFail);
		}
		return result;
	}
	
	
}
