package com.elite;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.google.gson.Gson;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@ComponentScan(value = "com.elite.*")
@MapperScan(value = "com.elite.learn.*.mapper*")
@SpringBootApplication(exclude = {DruidDataSourceAutoConfigure.class,SecurityAutoConfiguration.class})
@EnableScheduling
@EnableWebMvc
public class Admin {

    public static void main(String[] args) {
        SpringApplication.run(Admin.class, args);
        System.out.println("(*^▽^*)  learn管理系统启动成功！   (*^▽^*)  \n");
    }

    @Bean
    Gson gson() {
        return new Gson();
    }


}
