# learn SpringBoot学习

## 简介

* 管理后台的 Vue2 版本采用 [vue-element-admin](https://github.com/PanJiaChen/vue-element-admin) 
* 管理后台的移动端采用小程序！
* 后端采用 Spring Boot 多模块架构、MySQL + MyBatis Plus、Redis + Redisson
* 数据库可使用 MySQL
* 权限认证使用 Spring Security & Token & Redis
* 支持加载动态权限菜单，按钮级别权限控制，本地缓存提升性能


### 系统功能

| 功能    | 描述                              |
|-------|---------------------------------|
| 用户管理  | 用户是系统操作者，该功能主要完成系统用户配置          |
| 角色管理  | 角色菜单权限分配、设置角色按机构进行数据范围权限划分      |
| 菜单管理  | 配置系统菜单、操作权限、按钮权限标识等，本地缓存提供性能    |
| 部门管理  | 配置系统组织机构（公司、部门、小组），树结构展现支持数据权限  |
| 岗位管理  | 配置系统用户所属担任职务                    |
| 字典管理  | 对系统中经常使用的一些较为固定的数据进行维护          |
| 操作日志  | 系统正常操作日志记录和查询，集成 Swagger 生成日志内容 |
| 登录日志  | 系统登录日志记录查询，包含登录异常               |
| 错误码管理 | 系统所有错误码的管理，可在线修改错误提示，无需重启服务     |
|  敏感词  | 配置系统敏感词，支持标签分组                  |

## 技术栈

## 后台项目模块

| 项目                                                                       | 说明  |
|--------------------------------------------------------------------------|--------------------|
| `learn-parent`                                                        | Maven 依赖版本管理  |
| `learn-common`                                                        | 基础设施的 Module 模块  |
| `learn-admin`                                                         | 管理端接口    |



### 前端项目模块


| 项目                                                                       | 说明  |
|--------------------------------------------------------------------------|--------------------|
| `vue-learn-admin`                                                        | 基于 Vue2 + element-ui 实现的管理后台  |

### 框架

| 框架                                                                                          | 说明               | 版本          | 学习指南                                                           |
|---------------------------------------------------------------------------------------------|------------------|-------------|----------------------------------------------------------------|
| [Spring Boot](https://spring.io/projects/spring-boot)                                       | 应用开发框架           | 2.7.8       | [文档](https://github.com/YunaiV/SpringBoot-Labs)                |
| [MySQL](https://www.mysql.com/cn/)                                                          | 数据库服务器           | 5.7 / 8.0+  |                                                                |
| [Druid](https://github.com/alibaba/druid)                                                   | JDBC 连接池、监控组件    | 1.2.15      | [文档](http://www.iocoder.cn/Spring-Boot/datasource-pool/?yudao) |
| [MyBatis Plus](https://mp.baomidou.com/)                                                    | MyBatis 增强工具包    | 3.5.3.1     | [文档](http://www.iocoder.cn/Spring-Boot/MyBatis/?yudao)         |
| [Dynamic Datasource](https://dynamic-datasource.com/)                                       | 动态数据源            | 3.6.1       | [文档](http://www.iocoder.cn/Spring-Boot/datasource-pool/?yudao) |
| [Redis](https://redis.io/)                                                                  | key-value 数据库    | 5.0 / 6.0   |                                                                |
| [Spring MVC](https://github.com/spring-projects/spring-framework/tree/master/spring-webmvc) | MVC 框架           | 5.3.24      | [文档](http://www.iocoder.cn/SpringMVC/MVC/?yudao)               |
| [Spring Security](https://github.com/spring-projects/spring-security)                       | Spring 安全框架      | 5.7.6       | [文档](http://www.iocoder.cn/Spring-Boot/Spring-Security/?yudao) |


#### 安装教程

1、通过git下载源码  
2、创建数据库learn，数据库编码为UTF-8 
3、下载redis   windows版下载地址：https://github.com/MicrosoftArchive/redis/releases
        

>         * 把压缩包解压到一个文件夹
>         * 进入到解压到的文件里面，在文件夹地址栏输入cmd，回车进入到命令提示符里  
>         * 输入命令redis-server redis.windows.conf  
>         * 部署Redis在windows下的服务
>         * 先关闭上一个窗口，再到文件夹地址栏里输入命令redis-server --service-install redis.windows.conf
>         * 在windows搜索框里搜索服务，或者按win+r键，打开运行，在里面输入services.msc打开
>         * 然后随便点击一下，按一下键盘的r进行搜索，然后我们找到Redis的服务进行启动
>         * Redis使用：
>         * 卸载服务：redis-server --service-uninstall
>         * 开启服务：redis-server --service-start
>         * 停止服务：redis-server --service-stop
>         * 进入到解压到的文件里面，在地址栏里输入cmd打开命令提示符，依次输入命令
>         * redis-cli.exe
>         * set name hello
>         * get name
>         * keys *

3、执行sql/learn.sql文件，初始化数据  
4、修改application.yml，更新MySQL账号和密码  
5、修改application.yml,更改Redis连接信息  
6、IDEA运行App.java，则可启动项目 
7、账号默认：admin 密码：123456
8、Swagger管理端访问地址：http://localhost:30000/admin/swagger-ui/index.html
9、Swagger用户端访问地址：http://localhost:30001/web/swagger-ui/index.html


